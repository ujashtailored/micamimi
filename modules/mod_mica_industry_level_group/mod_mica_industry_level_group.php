<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_mica_industry_level_group
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the latest functions only once
JLoader::register('ModMicaIndustryLevelGroupHelper', __DIR__ . '/helper.php');

$industryLevelGroups = ModMicaIndustryLevelGroupHelper::getMicaIndustryLevelGroup();
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_mica_industry_level_group', $params->get('layout', 'default'));
