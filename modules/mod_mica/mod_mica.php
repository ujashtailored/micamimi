<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app     = JFactory::getApplication('site');
$session = JFactory::getSession();
$db      = JFactory::getDBO();
$user    = JFactory::getUser();

$itemid = $app->input->request->get('Itemid', 0, 'int');
//$state      = JRequest::getVar('state','');
//$district   = JRequest::getVar('district','');
//$town       = JRequest::getVar('town','');
//$m_type     = JRequest::getVar('m_type','');
//$attributes = JRequest::getVar('attributes',array());

$db_table = $app->input->request->get('db_table');
$m_type   = $session->get('m_type');
//$new_name         = JRequest::getVar('new_name',array());
//$custom_attribute = JRequest::getVar('custom_attribute',array());
$activeworkspace = $session->get('activeworkspace');

function getattributelist($db_table){
	$db = JFactory::getDBO();
	/*if($zoom == 5){
		$db_table = "State";
	}else if($zoom > 5 && $zoom <= 7){
		$db_table = "District";
	}else{
		$db_table = "Town";
	}*/

	$userAttr = userSelectedAttr($db_table);
	//print '<pre>'; print_r($userAttr);exit;
	if($userAttr == -2){
		return -2;
	}

	$str       = "";
	$todisplay = array();

	$query = "SELECT  * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
		INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
		WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
			AND ".$db->quoteName('grp.publish')." = ".$db->quote(1);
	$db->setQuery($query);
	$list = $db->loadAssocList();

	$change_group_name = 1;
	$icon              = array();
	foreach($list as $each){
		if($change_group_name == 1){
			$change_group_name = 0;
		}

		$each['field'] = str_replace("Rural_","",$each['field']);
		$each['field'] = str_replace("Urban_","",$each['field']);
		$each['field'] = str_replace("Total_","",$each['field']);

		$todisplay[$each['group']][] = $each['field'];
		$icon[$each['group']]        = $each['icon'];
		$todisplay[$each['group']][] = $each['field'];
		$groupid[]                   = $each['groupid'];
	}

	foreach($todisplay as $eachgrp => $vals){
		$todisplay[$eachgrp] = array_unique($todisplay[$eachgrp]);
	}
	return array($todisplay,$groupid,$icon);
}

function userSelectedAttr($db_table){
	$db   = JFactory::getDBO();
	$user = JFactory::getUser();
	$where		=

	$query = "SELECT plan_id FROM ".$db->quoteName('#__osmembership_subscribers')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($user->id);
	$db->setQuery($query);
	$planid = $db->loadResult();

	$typeQry = "SELECT ".$db->quoteName('attribute')."
		FROM ".$db->quoteName('#__mica_user_attribute')."
		WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid)."
			AND ".$db->quoteName('dbtable')." LIKE ".$db->quote($db_table);
	$db->setQuery($typeQry);
	$row  = $db->loadResult();
	$attr = explode(",", $row[0]);

	return $attr;
}


function displaysttributes($grparray){
	$app     = JFactory::getApplication('site');
	$session = JFactory::getSession();

	$todisplay = $grparray[0];
	$grpid     = $grparray[1];
	$icon      = $grparray[2];

	$str      = "";
	$userAttr = $session->get('attributes');	//print '<pre>'; print_r($userAttr);
	$userAttr = explode(",",$userAttr);
	$popup    = 3;
	$i        = 0;

	foreach($todisplay as $key => $val){
		if(($i%2)==0){
			$applyclass="clear";
		}else{
			$applyclass="";
		}

		$img="";
		if($icon[$key]!=""){
			$img='<img src="'.JUri::base().'components/com_mica/images/'.$icon[$key].'"/>';
		}

		$str .='<li class="'.$applyclass.'"><a href="javascript:void(0)" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'block\';document.getElementById(\'fade\').style.display=\'block\'"><div class="leftgrpimg">'.$img.'</div><div class="rightgrptext">'.$key;//'.$grpid[count($val)+1].'"
		$popup++;
		$chk_count   = 0;
		$chk_count1  = 0;
		$unchk_count = 0;

				/* if($key=='Financial Services'){
				print '<pre>'; echo $key; echo '=====>'; print_r($val);
				echo '=========<br/>';
						print_r($userAttr);
				}
				 if($key=='Score'){echo '<br/>@@@@@@@@@@@@@@@@@@<br/>';
				print '<pre>'; echo $key; echo '=====>'; print_r($val);
				echo '=========<br/>';
						print_r($userAttr);
				}*/
		foreach($val as $eachval){
			if(strstr($app->input->request->get('sessionattributes'),$eachval)){
				$chk_count1++;
			}
		}
		/*foreach($userAttr as $eachattr){
			if(in_array($eachattr,$val)){
				$chk_count++;
			}
		}
		$str.= " (<b>".$chk_count."</b> / ".count($val).")</div></a></li>;";*/
		$str.= " (<b>".$chk_count1."</b> / ".count($val).")</div></a></li>";
		$i++;
	}
	//$str .="</ul>";
	echo $str;
}
$listtoshow = getattributelist($db_table);//echo "in mod";exit;

?>

<div class="mod_mica">

			<div class="contentblock">
				<div class="contenthead">Workspace</div>
				<div class="blockcontent">
					<ul class="allworkspace">
					    <?php $id = $user->id;
						if($user->id == 0){
							return -1;
						}

						$query="SELECT name, id, data,is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($user->id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1);
						$db->setQuery($query);
						$result = $db->loadAssocList();

						$str               ="";
						$activeworkspace   = $app->input->request->get('activeworkspace');
						$selected          = "";
						$activeprofilename = $eachresult['name'];
						$i                 = 1;
						foreach($result as $eachresult){
							if(($i%2)==0){
								$clear="clear";
							}else{
								$clear="";
							}

							if($activeworkspace == $eachresult['id']){
								$selected          = "selected";
								$edit              = "Edit/Delete";
								$activeprofilename = $eachresult['name'];

								echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('Active Workspace :<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
								$onclick="document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
								echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
								$active_text ="<span style='font-weight:normal;'>(Active)<span>";
								$active_text ="";

							}else{
								//$arguments = $eachresult['id'].",".unserialize($eachresult['data'])['dataof'];
								$selected    ="";$edit="Select";
								$onclick     ="changeWorkspace(".$eachresult['id'].",'".unserialize($eachresult['data'])['dataof']."')";
								$active_text ="";
							}

							$str .= '
								<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
										<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
										<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'
						}
						echo $str; ?>
					</ul>
				</div>
			</div>


				<?php /* ? >
				< ?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){? >
					<input type="button" name="new" class="readon button" id="deleteworkspace"  value="Delete" />
				< ?php }? >
				<input type="button" name="new" class="readon button" id="cancelupdate"  value="Cancel" onclick="JQuery('#thematiceditoption').css('display','block');JQuery('.thematiceditoption').css('display','none');" />
				</div>
			</div><?php */ ?>
	<script type="text/javascript">

	function changeWorkspace(value, type){
	jQuery("#main_loader").show();
	if(value == 0){
		jQuery('.createneworkspace').css({'display':'block'});
		jQuery('.createnewworkspace').css({'display':''});
		jQuery('.thematiceditoption').css('display','block');
	}else{
		if(type == "District"){
			jQuery.ajax({
	            url: "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid=" + value + "&tmpl=component",
	            method: 'GET',
	            success: function(data) {
	                result = jQuery.parseJSON(data);

	                jQuery("#workspacceedit").load(location.href + " #workspacceedit>*", "");

	                jQuery('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
	                    getAttribute_v2(5, "district");

	                    if (typeof(preselected) != "undefined") {
	                        displaycombo_v2();
	                    }
	                });

	                jQuery("#activeworkspacename").html(result[0].name);
	                jQuery("#workspacceedit").hide();
	                jQuery("#fade").hide();
	                jQuery("#default").show();
	                window.location = "index.php?option=com_mica&view=micafront&Itemid=302";
	                //update form fields  as per selected workspace id
	                setTimeout(function() {
	                    jQuery('.scrollbar-inner').scrollbar();
	                    //getDataNew();
	                }, 1000);
	            }
	        });
		}

		if(type == "villages" || type == "Villages")
		{
			jQuery.ajax({
				url: "index.php?option=com_mica&task=villageshowresults.loadWorkspace&workspaceid="+value + "&view=villageshowresults&tmpl=component",
				method: 'GET',
				success: function(data)
				{
					result = jQuery.parseJSON(data);

					jQuery("#workspacceedit").load(location.href + " #workspacceedit>*", "");
					jQuery('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
	                    getAttribute(9, "district");

	                    if (typeof(preselected) != "undefined") {
	                        displaycombo();
	                    }
	                });
	                jQuery("#activeworkspacename").html(result[0].name);
	                jQuery("#workspacceedit").hide();
	                jQuery("#fade").hide();
	                jQuery("#default").show();

	                window.location = "index.php?option=com_mica&view=villagefront&Itemid=388";

	                setTimeout(function() {
	                    jQuery('.scrollbar-inner').scrollbar();
	                    //getDataNew();
	                }, 1000);
	            }
	        });
		}

		if(type == "villageSummary"){
			jQuery.ajax({
				url: "index.php?option=com_mica&task=summeryresults.loadWorkspace&workspaceid="+value + "&view=summaryshowresults&tmpl=component",
				method: 'GET',
				success: function(data)
				{
					result = jQuery.parseJSON(data);

					jQuery("#workspacceedit").load(location.href + " #workspacceedit>*", "");
					jQuery('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
	                        getAttribute(9, "district");

	                        if (typeof(preselected) != "undefined") {
	                            displaycombo();
	                        }
	                    });
	 				jQuery("#activeworkspacename").html(result[0].name);
	                jQuery("#workspacceedit").hide();
	                jQuery("#fade").hide();
	               	jQuery("#default").show();

	               	window.location = "index.php?option=com_mica&view=summeryfront&Itemid=398";

	               	setTimeout(function () {
	               		jQuery('.scrollbar-inner').scrollbar();
	               	}, 1000);
	            }
	        });
		}
	}
}
	</script>
