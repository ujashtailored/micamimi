<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  User.Syncmicauser
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

/**
 * Syncmicauser User plugin
 *
 * @since  1.5
 */
class PlgUserSyncmicauser extends JPlugin
{
	/**
	 * Application object
	 *
	 * @var    JApplicationCms
	 * @since  3.2
	 */
	protected $app;

	/**
	 * Database object
	 *
	 * @var    JDatabaseDriver
	 * @since  3.2
	 */
	protected $db;

	/**
	 * Remove all sessions for the user name
	 *
	 * Method is called after user data is deleted from the database
	 *
	 * @param   array    $user     Holds the user data
	 * @param   boolean  $success  True if user was successfully stored in the database
	 * @param   string   $msg      Message
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function onUserAfterDelete($user, $success, $msg)
	{
		if (!$success){
			return false;
		}

		$this->SyncMicaUsers();
		return true;
	}

	/**
	 * Utility method to act on a user after it has been saved.
	 *
	 * This method sends a registration email to new users created in the backend.
	 *
	 * @param   array    $user     Holds the new user data.
	 * @param   boolean  $isnew    True if a new user is stored.
	 * @param   boolean  $success  True if user was successfully stored in the database.
	 * @param   string   $msg      Message.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function onUserAfterSave($user, $isnew, $success, $msg){
		$this->SyncMicaUsers();

		$CREATEDCHILD_SQL = "SELECT created_userid FROM ".$this->db->quoteName('#__mica_adduser')." WHERE ".$this->db->quoteName('login_userid')." = ".$this->db->quote($user['id']);
		$this->db->setQuery($CREATEDCHILD_SQL);
		$createdChild = $this->db->loadColumn();
		
		foreach ($createdChild as $key => $each_createdchild) {
			$UPDATE_CHILD_IPS_SQL = " UPDATE ".$this->db->quoteName('#__users')."
				SET  ".$this->db->quoteName('ipaddress')." = ".$this->db->quote($user['ipaddress'])."
				WHERE ".$this->db->quoteName('id')." = ".$this->db->quote($each_createdchild);
			$this->db->setQuery($UPDATE_CHILD_IPS_SQL);
			$this->db->execute();
		}
	}


	/**
	 * Edited -Mrunal -30/05/2017 -A function to remove unliked relations of mica users.
	 */
	protected function SyncMicaUsers(){
		$this->db->setQuery(" DELETE FROM ".$this->db->quoteName('#__mica_adduser')." WHERE  ".$this->db->quoteName('created_userid')." NOT IN ( SELECT id FROM ".$this->db->quoteName('#__users')." ) OR ".$this->db->quoteName('login_userid')." 	NOT IN ( SELECT id FROM ".$this->db->quoteName('#__users')." ) ");
		$this->db->execute();
	}


	/**
	 * This method should handle any login logic and report back to the subject
	 *
	 * @param   array  $user     Holds the user data
	 * @param   array  $options  Array holding options (remember, autoregister, group)
	 *
	 * @return  boolean  True on success
	 *
	 * @since   1.5
	 */
	public function onUserLogin($user, $options = array())
	{
		$app = JFactory::getApplication();
		

		if ($app->isClient('administrator')){
			return true;
		}

		$instance = $this->_getUser($user, $options);
		
		/*print "<pre>";
		print_r($instance);
		exit;*/



		// If _getUser returned an error, then pass it back.
		if ($instance instanceof Exception){
			return true;
		}

		if (!file_exists(JPATH_ROOT . '/components/com_osmembership/osmembership.php')){
			return true;
		}

		require_once(JPATH_SITE.'/components/com_osmembership/helper/subscription.php');
		/*$plans = OSMembershipHelperSubscription::getActiveMembershipPlans($instance->id);
		unset($plans[0]);
		sort($plans);*/
		$db  = JFactory::getDbo();

		$db->setQuery ( "SELECT login_userid FROM ".$db->quoteName('#__mica_adduser')." WHERE ".$db->quoteName('created_userid')." = ".$db->quote($instance->id));
		$parent_userid = (int) $db->loadResult();
		
		$membership_userid = ($parent_userid > 0 ) ? $parent_userid : $instance->id;

		$SQL = "SELECT * FROM ".$db->quoteName('#__osmembership_subscribers')."
			WHERE ".$db->quoteName('user_id')." = ".$db->quote($membership_userid)."
			ORDER BY ".$db->quoteName('to_date')." DESC ";
		$db->setQuery($SQL);
		$rows = $db->loadObjectList();

		

		if (count($rows) > 0) {
			$plans = OSMembershipHelperSubscription::getActiveMembershipPlans($membership_userid);
			unset($plans[0]);
			sort($plans);
			if (count($plans) > 0 ) {
				//Is having active subscription.
				return true;
			}elseif ($parent_userid > 0 ) {
				//No active usbscription for child login.
				$this->app->redirect(JRoute::_('index.php', false), JText::_('YOUR_SUBSCRIPTION_EXPIRED'), 'error');
				return false;
			}

			$rows = $rows[0];//The first membership in index is latest one.
			if ($rows->published == 0) {
				//Plan Pending.
				$this->app->redirect(JRoute::_('index.php?option=com_users&view=registration&layout=pending', false));
			}elseif($rows->published == 2){
				//Plan Expired.
				$this->app->redirect(JRoute::_('index.php?option=com_users&view=registration&layout=expired&mica_user='.$rows->profile_id, false));
			}
			return false;
		}else{
			// Not having Any Plans
			$this->app->redirect(JRoute::_('index.php?option=com_users&view=registration&layout=subscribe', false));
			return false;
		}

		return true;
	}

	/**
	 * This method will return a user object
	 *
	 * If options['autoregister'] is true, if the user doesn't exist yet they will be created
	 *
	 * @param   array  $user     Holds the user data.
	 * @param   array  $options  Array holding options (remember, autoregister, group).
	 *
	 * @return  JUser
	 *
	 * @since   1.5
	 */
	protected function _getUser($user, $options = array())
	{
		$instance = JUser::getInstance();
		$id = (int) JUserHelper::getUserId($user['username']);

		if ($id)
		{
			$instance->load($id);

			return $instance;
		}

		// TODO : move this out of the plugin
		$config = JComponentHelper::getParams('com_users');

		// Hard coded default to match the default value from com_users.
		$defaultUserGroup = $config->get('new_usertype', 2);

		$instance->id = 0;
		$instance->name = $user['fullname'];
		$instance->username = $user['username'];
		$instance->password_clear = $user['password_clear'];

		// Result should contain an email (check).
		$instance->email = $user['email'];
		$instance->groups = array($defaultUserGroup);

		// If autoregister is set let's register the user
		$autoregister = isset($options['autoregister']) ? $options['autoregister'] : $this->params->get('autoregister', 1);

		if ($autoregister)
		{
			if (!$instance->save())
			{
				JLog::add('Error in autoregistration for user ' . $user['username'] . '.', JLog::WARNING, 'error');
			}
		}
		else
		{
			// No existing user and autoregister off, this is a temporary user.
			$instance->set('tmp_user', true);
		}

		return $instance;
	}

}
