<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.Autologin
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Joomla! Autologin plugin.
 *
 * @since  1.5
 */
class PlgSystemAutologin extends JPlugin
{
	/**
	 * Application object.
	 *
	 * @var    JApplicationCms
	 * @since  3.3
	 */
	protected $app;

	/**
	 * Database object.
	 *
	 * @var    JApplicationCms
	 * @since  3.3
	 */
	protected $db;

	/**
	 * Constructor.
	 *
	 * @param   object  &$subject  The object to observe.
	 * @param   array   $config    An optional associative array of configuration settings.
	 *
	 * @since   1.5
	 */
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);

		// Get the application if not done by JPlugin. This may happen during upgrades from Joomla 2.5.
		if (!$this->app)
		{
			$this->app = JFactory::getApplication();
		}

		$this->db = JFactory::getDbo();
	}

	/**
	 * Listener for the `onAfterInitialise` event
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function onAfterInitialise()
	{

		//$ipaddress = $_SERVER["REMOTE_ADDR"];
		///echo $_SERVER["REMOTE_ADDR"];
		//if($_SERVER["REMOTE_ADDR"] == "122.170.14.145")
		//{

		
			$currentPath = JURI::current();
			$url         = JURI::root()."administrator";
			$pos         = strpos($currentPath,$url);

			$task        = $this->app->input->get('task','','raw');
			$option      = $this->app->input->get('option','','raw');
			$username    = $this->app->input->get('username','','raw');

			$status      = "";
			$user        = JFactory::getUser();
			//$ipaddress = $_SERVER["REMOTE_ADDR"];

			if($pos !== 0 && $option == "com_users" && $task == "user.login") {
				$query  = "SELECT ipaddress FROM ".$this->db->quoteName('#__users')." WHERE ".$this->db->quoteName('username')." = ".$this->db->quote($username);
				$this->db->setQuery($query);
				$status = $this->db->loadResult();
				
			} else {
				$status = "Approved";
			}

			/*if($_SERVER["REMOTE_ADDR"] == "122.170.14.145"){
				echo "posr::_" . $pos."---";
			}*/
			$flag = 0;
			
			if($pos !== 0 && $option != "com_users" && $task != "user.login" && $status != "") {
				$ipaddress = $_SERVER["REMOTE_ADDR"];
				//$ipaddress = "122.170.14.145";


				/*$query = "SELECT u.id FROM #__users as u, #__acctexp_subscr as s WHERE u.id = s.userid
						  AND s.status = 'Active' AND u.ipaddress like '%".$ipaddress."%'";*/

				$query = "SELECT u.id FROM ".$this->db->quoteName('#__users')." as u
					WHERE ".$this->db->quoteName('u.block')." = ".$this->db->quote(0)."
						AND ".$this->db->quoteName('u.ipaddress')." LIKE ".$this->db->quote("%".$ipaddress."%");
				$this->db->setQuery($query);
				$results = $this->db->loadColumn();

				/*print_r($results);
				die();

				die("ere12121r");*/
				if(count($results) > 0) {
					$counter = 0;
					$info    = array();
					$z=0;
					foreach($results as $r) {

						$findActive = "SELECT userid, username
							FROM ".$this->db->quoteName('#__session')."
							WHERE ".$this->db->quoteName('userid')." = ".$this->db->quote($r)."
								AND ".$this->db->quoteName('guest')." <> 1";
						$this->db->setQuery($findActive);
						$res = $this->db->loadObjectList();

						/*if($_SERVER["REMOTE_ADDR"] == "122.170.14.145"){
							echo $findActive."<br />";
							print "<pre>";
							print_r($res);
						}*/

						$counter++;
						
						if(count($res) == 1) {
							
							if(!$user->guest) {
								$flag = 0;
								/*if($_SERVER["REMOTE_ADDR"] == "122.170.14.145"){
									echo $user->id . ": flag = 0";
								}*/
								break;
							}

						} else if(count($res) == 0) {
							$flag = 1;

							/*if($_SERVER["REMOTE_ADDR"] == "122.170.14.145"){
								echo "flag = 1";
							}*/

							$getInfo = "SELECT id, username, name, email FROM ".$this->db->quoteName('#__users')." WHERE ".$this->db->quoteName('id')." = ".$this->db->quote($r);
							$this->db->setQuery($getInfo);
							$ans = $this->db->loadObject();
							

							$info['userid']   = $ans->id;
							$info['username'] = $ans->username;
							$info['name']     = $ans->name;
							$info['email']    = $ans->email;
							break;
						}
						$z++;
					}
					
					if($flag == 1) {

						/*jimport('joomla.filesystem.file');
						$s = JFactory::getSession(); 
						$milliseconds1 = round(microtime(true) * 1000);
						$myFile = $z."__before_login.txt";
						$start = microtime(true);
						$message =  'User Name: ' . $info['username'] . ' - before_session_id = '.$s->getId() . ' - time: '.$start;

						JFile::write($myFile, $message);*/


						$user     = JFactory::getUser();
						//$homepage = $this->app->getMenu()->getDefault();
						$options  = array(
							'remember' => false
							//'return'   => 'index.php?Itemid='.$homepage->id
						);

						$this->app->login( array('username' => $info['username'] , 'mica_autologin' => 1), $options);

						/*jimport('joomla.filesystem.file');
						$s = JFactory::getSession(); 
						$myFile = $z."__after_login.txt";
						//$milliseconds = round(microtime(true) * 1000);
						$end = microtime(true);
						$exectuin_time = ($end - $start);
						echo $message =  'User Name: ' . $info['username'] . 'after_session_id = '.$s->getId() . ' - time: '.$end. 'Exeution Time'.$exectuin_time.' Secs       Exeution Time'.($exectuin_time*1000).' MilliSecs';
						JFile::write($myFile, $message);*/
						
						/*if($_SERVER["REMOTE_ADDR"] == "122.170.14.145"){
							echo "flg 1 exit";	
						}*/

						$this->app->redirect("index.php");exit;
						//exit;
					}

					if($flag == 0 && $user->id == 0) {
						
						/*if($_SERVER["REMOTE_ADDR"] == "122.170.14.145"){
							echo "flg 0 exit";
						}*/
							
						$this->app->enqueueMessage("You have reached the limit of the maximum users. To get the access for MIMI please contact your Admin.", 'message');
					}
				}
			} 
			else 
			{
				
				if($pos !== 0) {
					$ipaddress = $_SERVER["REMOTE_ADDR"];
					$query = "SELECT u.id FROM ".$this->db->quoteName('#__users')." as u
						INNER JOIN ".$this->db->quoteName('#__osmembership_subscribers')." as s
							ON ".$this->db->quoteName('u.id')." = ".$this->db->quoteName('s.user_id')."
								AND  s.published =1 and ".$this->db->quoteName('s.plan_subscription_status')." = ".$this->db->quote(1)."
						WHERE ".$this->db->quoteName('u.ipaddress')." LIKE ".$this->db->quote("%".$ipaddress."%");
					
					

					$this->db->setQuery($query);
					$results = $this->db->loadColumn();
					
					$resultsStr = implode(',',$results);

					$qry = "SELECT count(userid)
						FROM ".$this->db->quoteName('#__session')."
						WHERE ".$this->db->quoteName('userid')." IN (".$resultsStr.")";
					$this->db->setQuery($qry);
					try 
					{
						$qryCount = $this->db->loadResult();
						/* print_r($qryCount);
						die(11);*/				
					} catch (Exception $e) {

						$qryCount = NULL;
					}
					
					$user = JFactory::getUser();
					if(count($qryCount) > 0) {
						
						if($_POST['task'] == "user.logout" && $user->id <> 0) {

							// Prepare the logout options.
							$options = array(
								'clientid' => $this->app->get('shared_session', '0') ? null : 0,
							);
							// Perform the log out.
							$error = $this->app->logout(null, $options);
							$this->app->redirect(JUri::base().'logout.html');exit;
							return false;
						} else {

							$_POST['task']     = "";
							$_POST['option']   = "";
							$_POST['username'] = "";
							$_POST['password'] = "";
							$this->app->enqueueMessage(JText::_("You are already logged in."));
							$this->app->redirect("index.php");
						}
					}
				}
			}
		//}
	}
}
