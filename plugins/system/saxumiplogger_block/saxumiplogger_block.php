<?php
/**
 * $Id: saxumiplogger_block.php 18 2013-12-19 12:26:36Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.plugin.plugin');

class plgSystemSaxumiplogger_block extends JPlugin {

	function onAfterInitialise()
	{
		$mainframe = JFactory::getApplication();
		$session = JFactory::getSession();
		$db = JFactory::getDBO();
				
		$sql = $db->getQuery(true)
			->select('ip')
			->from('#__saxum_iplogger_block');
		$db->setQuery($sql);
		$iptable = $db->loadColumn();

		require_once JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'controller.php';

		$ip=SaxumiploggerController::getip();
		$err=SaxumiploggerController::ipinlist($iptable,$ip);
		
		if ($err) {
			JError::raiseError('403', JText::_( 'JERROR_ALERTNOAUTHOR' ));
		}
		else {
			return true;
		}
	}

}
