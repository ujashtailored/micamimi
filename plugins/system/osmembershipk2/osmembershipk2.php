<?php
/**
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2012 - 2017 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die;

use Joomla\Registry\Registry;

if (!file_exists(JPATH_ROOT . '/components/com_osmembership/osmembership.php'))
{
	return;
}

class plgSystemOSMembershipk2 extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);

		$this->canRun = file_exists(JPATH_ROOT . '/components/com_k2/k2.php');
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_osmembership/table');
	}

	/**
	 * Render settings form
	 *
	 * @param OSMembershipTablePlan $row
	 *
	 * @return array
	 */
	public function onEditSubscriptionPlan($row)
	{
		if (!$this->canRun)
		{
			return;
		}

		ob_start();
		$this->drawSettingForm($row);
		$form = ob_get_clean();

		return array('title' => JText::_('PLG_OSMEMBERSHIP_K2_ITEMS_RESTRICTION_SETTINGS'),
		             'form'  => $form,
		);
	}

	/**
	 * Store setting into database
	 *
	 * @param PlanOsMembership $row
	 * @param Boolean          $isNew true if create new plan, false if edit
	 */
	public function onAfterSaveSubscriptionPlan($context, $row, $data, $isNew)
	{
		if (!$this->canRun)
		{
			return;
		}

		$db         = JFactory::getDbo();
		$query      = $db->getQuery(true);
		$planId     = $row->id;
		$articleIds = $data['k2_article_ids'];

		if (!$isNew)
		{
			$query->delete('#__osmembership_k2items')->where('plan_id=' . (int) $planId);
			$db->setQuery($query);
			$db->execute();
		}

		if (!empty($articleIds))
		{
			$articleIds = explode(',', $articleIds);

			for ($i = 0; $i < count($articleIds); $i++)
			{
				$articleId = $articleIds[$i];
				$query->clear()
					->insert('#__osmembership_k2items')
					->columns('plan_id, article_id')
					->values("$row->id,$articleId");
				$db->setQuery($query);
				$db->execute();
			}
		}

		if (isset($data['k2_item_categories']))
		{
			$selectedCategories = $data['k2_item_categories'];
		}
		else
		{
			$selectedCategories = array();
		}

		$params = new Registry($row->params);
		$params->set('k2_item_categories', implode(',', $selectedCategories));
		$row->params = $params->toString();
		$row->store();
	}

	/**
	 * Display form allows users to change setting for this subscription plan
	 *
	 * @param object $row
	 */
	private function drawSettingForm($row)
	{
		//Get categories
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id, name')
			->from('#__k2_categories')
			->where('published = 1');
		$db->setQuery($query);
		$categories = $db->loadObjectList('id');

		if (!count($categories))
		{
			return;
		}

		$categoryIds = array_keys($categories);
		$query->clear()
			->select('id, title, catid')
			->from('#__k2_items')
			->where('`published` = 1')
			->where('catid IN (' . implode(',', $categoryIds) . ')');
		$db->setQuery($query);
		$rowArticles = $db->loadObjectList();

		if (!count($rowArticles))
		{
			return;
		}

		$articles = array();

		foreach ($rowArticles as $rowArticle)
		{
			$articles[$rowArticle->catid][] = $rowArticle;
		}

		//Get plans articles
		$query->clear()
			->select('article_id')
			->from('#__osmembership_k2items')
			->where('plan_id=' . (int) $row->id);
		$db->setQuery($query);
		$planArticles = $db->loadColumn();

		$params             = new Registry($row->params);
		$selectedCategories = explode(',', $params->get('k2_item_categories', ''));
		?>

		<h2><?php echo JText::_('OSM_K2_CATEGORIES'); ?></h2>
		<p class="text-info"><?php echo JText::_('OSM_K2_CATEGORIES_EXPLAIN'); ?></p>
		<table class="admintable adminform" style="width: 100%;">
			<?php
			foreach ($categories as $category)
			{
				?>
				<tr>
					<td>
						<label class="checkbox">
							<input
								type="checkbox"<?php if (in_array($category->id, $selectedCategories)) echo ' checked="checked"'; ?>
								value="<?php echo $category->id ?>"
								name="k2_item_categories[]"/> <strong><?php echo $category->name; ?></strong>
						</label>
					</td>
				</tr>
				<?php
			}
			?>
		</table>

		<h2><?php echo JText::_('OSM_K2_ITEMS'); ?></h2>
		<p class="text-info"><?php echo JText::_('OSM_K2_ITEMS_EXPLAIN'); ?></p>
		<table class="admintable adminform" style="width: 100%;">
			<tr>
				<td>
					<div class="accordion" id="k2accordion2">
						<?php
						$count = 0;
						foreach ($categories as $category)
						{
							if (!isset($articles[$category->id]))
							{
								continue;
							}
							?>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#k2accordion2"
									   href="#k2collapse<?php echo $category->id; ?>" style="display: inline;">
										<?php echo $category->name; ?>
									</a>
									<label class="checkbox"> <input type="checkbox" value="<?php echo $category->id ?>"
									                                id="<?php echo $category->id ?>" class="k2checkAll"
									                                name=""> <strong>#</strong> </label>
								</div>
								<div id="k2collapse<?php echo $category->id; ?>"
								     class="accordion-body collapse <?php if ($count == 0) echo ' in'; ?>">
									<div class="accordion-inner">
										<?php
										$categoryArticles = $articles[$category->id];
										foreach ($categoryArticles as $article)
										{
											?>
											<label class="checkbox" style="display: block;">
												<input
													type="checkbox" <?php if (in_array($article->id, $planArticles)) echo ' checked="checked" '; ?>
													value="<?php echo $article->id; ?>"
													id="k2article_<?php echo $article->id; ?>" name="k2_article_id[]"
													class="k2checkall_<?php echo $category->id ?> k2_item_checkbox"/>
												<strong><?php echo $article->title; ?></strong>
											</label>
											<?php
										}
										?>
									</div>
								</div>
							</div>
							<?php
							$count++;
						}
						?>
					</div>
				</td>
			</tr>
			<?php
			if ($row->id)
			{
				?>
				<input type="hidden" value="<?php echo implode(',', $planArticles) ?>" name="k2_article_ids"
				       class="k2_article_ids"/>
				<?php
			}
			else
			{
				?>
				<input type="hidden" value="" name="k2_article_ids" class="k2_article_ids"/>
				<?php
			}
			?>
		</table>
		<script type="text/javascript">
			(function ($) {
				$(document).ready(function () {
					$(".k2checkAll").click(function () {
						var ID = $(this).attr("id");
						if ($(this).is(':checked')) {
							$('.k2checkall_' + ID).attr("checked", true);
						}
						else {
							$('.k2checkall_' + ID).attr("checked", false);
						}
						$('.k2_article_ids').val(getItemIds());
					});

					$(".k2_item_checkbox").click(function () {
						$('.k2_article_ids').val(getItemIds());
					});

					var k2itemIdArray = new Array;
					getItemIds = (function () {
						k2itemIdArray = [];
						$('.k2_item_checkbox:checked').each(function () {
							k2itemIdArray.push($(this).val());
						});
						console.log(k2itemIdArray);
						return k2itemIdArray;
					})

				})
			})(jQuery)
		</script>
		<?php
	}

	public function onAfterRoute()
	{
		$app = JFactory::getApplication();

		if ($app->isAdmin())
		{
			return true;
		}

		$user = JFactory::getUser();

		if ($user->authorise('core.admin'))
		{
			return true;
		}

		if ($this->params->get('protection_method', 0) == 1)
		{
			return true;
		}

		$option    = $app->input->getCmd('option');
		$view      = $app->input->getCmd('view');
		$task      = $app->input->getCmd('task');
		$articleId = $app->input->getInt('id', 0);

		if ($option != 'com_k2' || ($view != 'item' && $task != 'download') || !$articleId)
		{
			return true;
		}

		$planIds = $this->getRequiredPlanIds($articleId);

		if (count($planIds))
		{
			//Check to see the current user has an active subscription plans
			require_once JPATH_ROOT . '/components/com_osmembership/helper/helper.php';

			$activePlans = OSMembershipHelper::getActiveMembershipPlans();

			if (!count(array_intersect($planIds, $activePlans)))
			{
				OSMembershipHelper::loadLanguage();

				$msg = JText::_('OS_MEMBERSHIP_K2_ARTICLE_ACCESS_RESITRICTED');
				$msg = str_replace('[PLAN_TITLES]', $this->getPlanTitles($planIds), $msg);

				// Try to find the best redirect URL
				$redirectUrl = OSMembershipHelper::getRestrictionRedirectUrl($planIds);

				if (!$redirectUrl)
				{
					$redirectUrl = $this->params->get('redirect_url', OSMembershipHelper::getViewUrl(array('categories', 'plans', 'plan', 'register')));
				}

				if (!$redirectUrl)
				{
					$redirectUrl = JUri::root();
				}

				// Store URL of this page to redirect user back after user logged in if they have active subscription of this plan
				$session = JFactory::getSession();
				$session->set('osm_return_url', JUri::getInstance()->toString());
				$session->set('required_plan_ids', $planIds);

				JFactory::getApplication()->redirect($redirectUrl, $msg);
			}
		}
	}

	/**
	 * Hide fulltext of article to none-subscribers
	 *
	 * @param     $context
	 * @param     $row
	 * @param     $params
	 * @param int $page
	 *
	 * @return bool|void
	 */
	public function onContentPrepare($context, &$row, &$params, $page = 0)
	{
		if (is_object($row) && $this->params->get('protection_method', 0) == 1 && $context == 'com_k2.item')
		{
			$planIds = $this->getRequiredPlanIds($row->id);

			if (count($planIds))
			{
				//Check to see the current user has an active subscription plans
				require_once JPATH_ROOT . '/components/com_osmembership/helper/helper.php';

				$activePlans = OSMembershipHelper::getActiveMembershipPlans();

				if (!count(array_intersect($planIds, $activePlans)))
				{
					$message     = OSMembershipHelper::getMessages();
					$fieldSuffix = OSMembershipHelper::getFieldSuffix();

					if (strlen($message->{'content_restricted_message' . $fieldSuffix}))
					{
						$msg = $message->{'content_restricted_message' . $fieldSuffix};
					}
					else
					{
						$msg = $message->content_restricted_message;
					}

					$msg = str_replace('[PLAN_TITLES]', $this->getPlanTitles($planIds), $msg);

					// Try to find the best redirect URL
					$redirectUrl = OSMembershipHelper::getRestrictionRedirectUrl($planIds);

					if (!$redirectUrl)
					{
						$redirectUrl = $this->params->get('redirect_url', OSMembershipHelper::getViewUrl(array('categories', 'plans', 'plan', 'register')));
					}

					if (!$redirectUrl)
					{
						$redirectUrl = JUri::root();
					}

					// Add the required plans to redirect URL
					$redirectUri = JUri::getInstance($redirectUrl);
					$redirectUri->setVar('filter_plan_ids', implode(',', $planIds));

					// Store URL of this page to redirect user back after user logged in if they have active subscription of this plan
					$session = JFactory::getSession();
					$session->set('osm_return_url', JUri::getInstance()->toString());
					$session->set('required_plan_ids', $planIds);

					$msg = str_replace('[SUBSCRIPTION_URL]', $redirectUri->toString(), $msg);

					$t[]       = $row->introtext;
					$t[]       = '<div class="text-info">' . $msg . '</div>';
					$row->text = implode(' ', $t);
				}
			}
		}

		return true;
	}

	/**
	 * The the Ids of the plans which users can subscribe for to access to the given article
	 *
	 * @param int $articleId
	 *
	 * @return array
	 */
	private function getRequiredPlanIds($articleId)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('DISTINCT plan_id')
			->from('#__osmembership_k2items')
			->where('article_id = ' . $articleId)
			->where('plan_id IN (SELECT id FROM #__osmembership_plans WHERE published = 1)');
		$db->setQuery($query);

		try
		{
			$planIds = $db->loadColumn();
		}
		catch (Exception $e)
		{
			$planIds = array();
		}

		// Check categories
		$query->clear()
			->select('catid')
			->from('#__k2_items')
			->where('id = ' . (int) $articleId);
		$db->setQuery($query);
		$catId = $db->loadResult();

		$query->clear()
			->select('id, params')
			->from('#__osmembership_plans')
			->where('published = 1');
		$db->setQuery($query);
		$plans = $db->loadObjectList();

		foreach ($plans as $plan)
		{
			$params = new Registry($plan->params);

			if ($articleCategories = $params->get('k2_item_categories'))
			{
				$articleCategories = explode(',', $articleCategories);

				if (in_array($catId, $articleCategories))
				{
					$planIds[] = $plan->id;
				}
			}
		}

		return $planIds;
	}

	/**
	 * Get imploded titles of the given plans
	 *
	 * @param array $planIds
	 *
	 * @return string
	 */
	private function getPlanTitles($planIds)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('title')
			->from('#__osmembership_plans')
			->where('id IN (' . implode(',', $planIds) . ')')
			->where('published = 1')
			->order('ordering');
		$db->setQuery($query);

		return implode(' ' . JText::_('OSM_OR') . ' ', $db->loadColumn());
	}

	/**
	 * Display k2 items which subscriber can access to in his profile
	 *
	 * @param $row
	 *
	 * @return array|void
	 */
	public function onProfileDisplay($row)
	{

		if (!$this->params->get('display_k2_items_in_profile'))
		{
			return;
		}

		ob_start();
		$this->displayK2Items();
		$form = ob_get_clean();

		return array('title' => JText::_('OSM_MY_K2_ITMES'),
		             'form'  => $form,
		);
	}

	/**
	 * Display list of accessible k2 items
	 */
	private function displayK2Items()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$items         = array();
		$activePlanIds = OSMembershipHelper::getActiveMembershipPlans();

		// Get categories
		$query->select('id, params')
			->from('#__osmembership_plans')
			->where('id IN (' . implode(',', $activePlanIds) . ')');
		$db->setQuery($query);
		$plans  = $db->loadObjectList();
		$catIds = array();

		foreach ($plans as $plan)
		{
			$params = new Registry($plan->params);

			if ($articleCategories = $params->get('k2_item_categories'))
			{
				$catIds = array_merge($catIds, explode(',', $articleCategories));
			}
		}

		if (count($activePlanIds) > 1)
		{
			$query->clear()
				->select('a.id, a.catid, a.title, a.alias, a.hits, c.name AS category_name')
				->from('#__k2_items AS a')
				->innerJoin('#__k2_categories AS c ON a.catid = c.id')
				->innerJoin('#__osmembership_k2items AS b ON a.id = b.article_id')
				->where('b.plan_id IN (' . implode(',', $activePlanIds) . ')')
				->where('a.published = 1')
				->order('plan_id')
				->order('a.ordering');
			$db->setQuery($query);

			$items = array_merge($items, $db->loadObjectList());
		}

		if (count($catIds))
		{
			$query->clear()
				->select('a.id, a.catid, a.title, a.alias, a.hits, c.name AS category_name')
				->from('#__k2_items AS a')
				->innerJoin('#__k2_categories AS c ON a.catid = c.id')
				->where('a.catid IN (' . implode(',', $catIds) . ')')
				->where('a.published = 1')
				->order('a.ordering');
			$db->setQuery($query);

			$items = array_merge($items, $db->loadObjectList());
		}

		if (empty($items))
		{
			return;
		}
		?>
		<table class="adminlist table table-striped" id="adminForm">
			<thead>
			<tr>
				<th class="title"><?php echo JText::_('OSM_TITLE'); ?></th>
				<th class="title"><?php echo JText::_('OSM_CATEGORY'); ?></th>
				<th class="center"><?php echo JText::_('OSM_HITS'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			require_once JPATH_ROOT . '/components/com_k2/helpers/route.php';
			foreach ($items as $item)
			{
				$k2itemLink = JRoute::_(K2HelperRoute::getItemRoute($item->id, $item->catid));
				?>
				<tr>
					<td><a href="<?php echo $k2itemLink ?>"><?php echo $item->title; ?></a></td>
					<td><?php echo $item->category_name; ?></td>
					<td class="center">
						<?php echo $item->hits; ?>
					</td>
				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
		<?php
	}
}