<?php
/**
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2012 - 2017 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die;

class plgSystemMembershipPro extends JPlugin
{
	/**
	 * Flag to see whether the plan subscription status for this record has been processed or not
	 *
	 * @var bool
	 */
	private $subscriptionProcessed = false;

	/**
	 * This method is run after subscription record is successfully stored in database
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	public function onAfterStoreSubscription($row)
	{
		if (strpos($row->payment_method, 'os_offline') !== false)
		{
			$config = OSMembershipHelper::getConfig();

			if ($config->activate_invoice_feature)
			{
				static::generateInvoiceNumber($row);
			}

			if ($config->auto_generate_membership_id)
			{
				static::generateMembershipId($row);
			}

			$row->store();
		}
	}

	/**
	 * This method is run after subscription become active, ie after user complete payment or admin approve the subscription
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	public function onMembershipActive($row)
	{
		$config = OSMembershipHelper::getConfig();

		if (!$row->user_id && $row->username && $row->user_password)
		{
			static::createUserAccount($row);
		}

		if (!$config->send_activation_email)
		{
			static::activateUserAccount($row);
		}

		if ($config->activate_invoice_feature && !$row->group_admin_id && !$row->invoice_number)
		{
			static::generateInvoiceNumber($row);
		}

		if ($config->auto_generate_membership_id && !$row->membership_id)
		{
			static::generateMembershipId($row);
		}

		$row->store();

		$this->subscriptionProcessed = true;

		if ($row->group_admin_id == 0)
		{
			static::updateSubscriptionExpiredDate($row);

			static::updatePlanSubscriptionStatus($row);

			static::updateSendingReminderStatus($row);
		}
	}

	/**
	 * Block the user account when membership is expired
	 *
	 * @param $row
	 *
	 * @return bool
	 */
	public function onMembershipExpire($row)
	{
		if ($row->user_id && $this->params->get('block_account_when_expired', 0))
		{
			$user = JFactory::getUser($row->user_id);
			if (!$user->authorise('core.admin'))
			{
				$user->set('block', 1);
				$user->save(true);
			}
		}

		$this->subscriptionProcessed = true;

		if (!$row->group_admin_id)
		{
			static::updatePlanSubscriptionStatus($row);
		}

		return true;
	}

	/**
	 * Recalculate some important subscription information when a subscription record is being deleted
	 *
	 * @param string                      $context
	 * @param OSMembershipTableSubscriber $row
	 */
	public function onSubscriptionAfterDelete($context, $row)
	{
		if ($row->profile_id > 0 && $row->plan_id > 0)
		{
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->clear()
				->select('id, profile_id, plan_id, published, from_date, to_date')
				->from('#__osmembership_subscribers')
				->where('plan_id = ' . $row->plan_id)
				->where('profile_id = ' . $row->profile_id)
				->where('(published >= 1 OR payment_method LIKE "os_offline%")')
				->order('id');
			$db->setQuery($query);
			$subscriptions = $db->loadObjectList();

			if (!empty($subscriptions))
			{
				$isActive         = false;
				$isPending        = false;
				$isExpired        = false;
				$lastActiveDate   = null;
				$lastExpiredDate  = null;
				$planMainRecordId = 0;
				$planFromDate     = $subscriptions[0]->from_date;
				foreach ($subscriptions as $subscription)
				{
					if ($subscription->plan_main_record)
					{
						$planMainRecordId = $subscription->id;
					}

					if ($subscription->published == 1)
					{
						$isActive       = true;
						$lastActiveDate = $subscription->to_date;
					}
					elseif ($subscription->published == 0)
					{
						$isPending = true;
					}
					elseif ($subscription->published == 2)
					{
						$isExpired       = true;
						$lastExpiredDate = $subscription->to_date;
					}
				}

				if ($isActive)
				{
					$published  = 1;
					$planToDate = $lastActiveDate;
				}
				elseif ($isPending)
				{
					$published = 0;
				}
				elseif ($isExpired)
				{
					$published  = 2;
					$planToDate = $lastExpiredDate;
				}
				else
				{
					$published  = 3;
					$planToDate = $subscription->to_date;
				}

				$query->clear()
					->update('#__osmembership_subscribers')
					->set('plan_subscription_status = ' . (int) $published)
					->set('plan_subscription_from_date = ' . $db->quote($planFromDate))
					->set('plan_subscription_to_date = ' . $db->quote($planToDate))
					->where('plan_id = ' . $row->plan_id)
					->where('profile_id = ' . $row->profile_id);
				$db->setQuery($query);
				$db->execute();

				if (empty($planMainRecordId))
				{
					$planMainRecordId = $subscriptions[0]->id;
					$query->clear()
						->update('#__osmembership_subscribers')
						->set('plan_main_record = 1')
						->where('id = ' . $planMainRecordId);
					$db->setQuery($query);
					$db->execute();
				}
			}
		}

		if ($row->is_profile == 1 && $row->user_id > 0)
		{
			// We need to fix the profile record
			require_once JPATH_ROOT . '/components/com_osmembership/helper/subscription.php';

			OSMembershipHelperSubscription::fixProfileId($row->user_id);
		}
	}

	/**
	 * Update plan subscription status when subscription record updated
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	public function onMembershipUpdate($row)
	{
		if (!$this->subscriptionProcessed && JFactory::getApplication()->isAdmin())
		{
			static::updateSubscriptionExpiredDate($row);

			static::updatePlanSubscriptionStatus($row);
		}
	}

	/**
	 * Handle Login redirect
	 *
	 * @param $options
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function onUserAfterLogin($options)
	{
		if (!file_exists(JPATH_ROOT . '/components/com_osmembership/osmembership.php'))
		{
			return;
		}

		$app = JFactory::getApplication();

		if ($app->isAdmin())
		{
			return;
		}

		require_once JPATH_ROOT . '/components/com_osmembership/helper/helper.php';

		if ($loginRedirectUrl = OSMembershipHelper::getLoginRedirectUrl())
		{
			$app->setUserState('users.login.form.return', $loginRedirectUrl);
		}
	}

	/**
	 * Method to generate invoice number for the subscription record
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	private static function generateInvoiceNumber($row)
	{
		if (OSMembershipHelper::needToCreateInvoice($row))
		{
			$row->invoice_number = OSMembershipHelper::getInvoiceNumber($row);
		}
	}

	/**
	 * Create user account for subscriber after subscription being active
	 *
	 * @param OSMembershipTableSubscriber $row
	 *
	 * @throws Exception
	 */
	private static function createUserAccount($row)
	{
		$data['username']   = $row->username;
		$data['first_name'] = $row->first_name;
		$data['last_name']  = $row->last_name;
		$data['email']      = $row->email;

		//Password
		$privateKey        = md5(JFactory::getConfig()->get('secret'));
		$key               = new JCryptKey('simple', $privateKey, $privateKey);
		$crypt             = new JCrypt(new JCryptCipherSimple, $key);
		$data['password1'] = $crypt->decrypt($row->user_password);
		try
		{
			$row->user_id = (int) OSMembershipHelper::saveRegistration($data);
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage());
		}
	}

	/**
	 * Active user account automatically after subscription active
	 *
	 * @param $row
	 */
	private static function activateUserAccount($row)
	{
		if (JComponentHelper::getParams('com_users')->get('useractivation') != 2)
		{
			$user = JFactory::getUser($row->user_id);
			if ($user->get('block'))
			{
				$user->set('block', 0);
				$user->set('activation', '');
				$user->save(true);
			}
		}
	}

	/**
	 * Generate Membership ID for a subscription record
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	private static function generateMembershipId($row)
	{
		if ($row->user_id)
		{
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('MAX(membership_id)')
				->from('#__osmembership_subscribers')
				->where('user_id = ' . $row->user_id);
			$db->setQuery($query);
			$row->membership_id = (int) $db->loadResult();
		}

		if (!$row->membership_id)
		{
			$row->membership_id = OSMembershipHelper::getMembershipId();
		}
	}

	/**
	 * Calculate and store subscription expired date of the user for the plan he just processed subscription
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	private static function updateSubscriptionExpiredDate($row)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('MAX(to_date)')
			->from('#__osmembership_subscribers')
			->where('published = 1')
			->where('profile_id = ' . $row->profile_id)
			->where('plan_id = ' . $row->plan_id);
		$db->setQuery($query);
		$subscriptionExpiredDate = $db->loadResult();

		if ($subscriptionExpiredDate)
		{
			$query->clear()
				->update('#__osmembership_subscribers')
				->set('plan_subscription_to_date = ' . $db->quote($subscriptionExpiredDate))
				->where('profile_id = ' . $row->profile_id)
				->where('plan_id = ' . $row->plan_id);
			$db->setQuery($query);
			$db->execute();
		}
	}

	/**
	 * Update status of the plan for the user when subscription status change
	 *
	 * @param $row
	 */
	public static function updatePlanSubscriptionStatus($row)
	{
		require_once JPATH_ROOT . '/components/com_osmembership/helper/subscription.php';
		$subscriptionStatus = OSMembershipHelperSubscription::getPlanSubscriptionStatusForUser($row->profile_id, $row->plan_id);
		$db                 = JFactory::getDbo();
		$query              = $db->getQuery(true);
		$query->update('#__osmembership_subscribers')
			->set('plan_subscription_status = ' . $subscriptionStatus)
			->where('profile_id = ' . $row->profile_id)
			->where('plan_id = ' . $row->plan_id);
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * Clear subscription expired reminder status
	 *
	 * @param OSMembershipTableSubscriber $row
	 */
	private static function updateSendingReminderStatus($row)
	{
		if ($row->user_id > 0)
		{
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);
			$now   = $db->quote(JFactory::getDate()->toSql());
			$query->update('#__osmembership_subscribers')
				->set('first_reminder_sent = 1')
				->set('second_reminder_sent = 1')
				->set('third_reminder_sent = 1')
				->set('first_reminder_sent_at = ' . $now)
				->set('second_reminder_sent_at = ' . $now)
				->set('third_reminder_sent_at = ' . $now)
				->where('user_id = ' . $row->user_id)
				->where('plan_id = ' . $row->plan_id)
				->where('id != ' . $row->id);

			$db->setQuery($query);
			$db->execute();
		}
	}
}
