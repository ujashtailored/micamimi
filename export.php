<?php

 error_reporting( E_ALL );
    /**
     * @package    Joomla.Site
     *
     * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
     * @license    GNU General Public License version 2 or later; see LICENSE.txt
     */

    /**
     * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
     */
    define('JOOMLA_MINIMUM_PHP', '5.3.10');

    if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
    {
        die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
    }

    /**
     * Constant that is checked in included files to prevent direct access.
     * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
     */
    define('_JEXEC', 1);

    if (file_exists(__DIR__ . '/defines.php'))
    {
        include_once __DIR__ . '/defines.php';
    }

    if (!defined('_JDEFINES'))
    {
        define('JPATH_BASE', __DIR__);
        require_once JPATH_BASE . '/includes/defines.php';
    }

    require_once JPATH_BASE . '/includes/framework.php';

    $db = JFactory::getDBO();

    // Create a new query object.
    $query = $db->getQuery(true);


    $queryuser = "SELECT DISTINCT a.user_id,b.name,b.email FROM `pqwoe_saxum_iplogger` as a INNER JOIN `pqwoe_users` as b on a.user_id = b.id";
       
    $db->setQuery($queryuser);
    $users = $db->loadObjectList(); 

if($_REQUEST['submit']=='export')
{


    $users = $_REQUEST['users'];
    for($i=0;$i<count($users);$i++)
    {
        $inusers.= $users[$i].',';
    }
    $inusers = substr($inusers,0,'-1');

    $from =  $_REQUEST['from-date'];
    $to =  $_REQUEST['to-date'];

    $query = "SELECT u.name, u.email, p.user_id,p.ip, p.visitDate FROM `pqwoe_saxum_iplogger` as p , `pqwoe_users` as u WHERE p.`visitDate` >= '".$from."' and p.`visitDate` <= '".$to."' and `user_id` in($inusers) and u.id= p.user_id order by `visitDate` ASC";
    

    $db->setQuery($query);
    $rows = $db->loadObjectList();

    /* export to archive{date}.xls */
    $fileName = "archive" . date('Ymd') . ".xls";
    header("Content-Disposition: attachment; filename=\"$fileName\"");
    header("Content-Type: application/vnd.ms-excel");
    echo "Name"."\t"."Email"."\t"."User ID"."\t"."IP"."\t"."visitDate"."\n";
    foreach($rows as $row)  { echo $row->name."\t".$row->email."\t".$row->user_id."\t".$row->ip."\t".$row->visitDate."\n"; }
    exit;
}

?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="bootstrap/bootstrap-theme.min.css" />
    <script src="bootstrap/jquery.min.js"></script>
    <script src="bootstrap/bootstrap.min.js"></script>
</head>
<body>
    <div class="export-form">
        <div class="container">
            <form action="" method="post" style="margin-top: 20px;" >

                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="users">Select users:</label>
                        <select multiple class="form-control" id="users" name="users[]" style="height: 250px;" >
                            <?php
                                foreach($users as $user)
                                {
                                    echo '<option value="'.$user->user_id.'">'.$user->user_id.' ( '.$user->name.' - '.$user->email.' )</option>';
                                }
                            ?>
                        </select>
                        <span class="notice">To select multiple items, hold CTRL and click</span>
                    </div>
                </div>

                <div class="row-2">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="from-date">From date:</label>
                            <input type="date" id="from-date" name="from-date">
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="to-date">To date:</label>
                            <input type="date" id="to-date" name="to-date">
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <button type="submit" name="submit" value="export" class="btn btn-default" id="export" style="width: 200px;" >Export</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <?php
        //echo '<pre>'; print_r($_REQUEST); echo '</pre>';
    ?>
</body>

<script type="text/javascript">
    jQuery(document).ready(function(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
         if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }

        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("from-date").setAttribute("max", today);
        document.getElementById("to-date").setAttribute("max", today);
    });

    jQuery('#export').click(function(){
        var users = jQuery('#users').val();
        var frommeta = jQuery('#from-date').val()
        var tometa = jQuery('#to-date').val();

        if(users=='' || users==undefined){
            alert('Please select users');
            jQuery('#users').focus();
            return false;
        }
        else if(frommeta == '' || frommeta == undefined)
        {
            alert('Please select a from date');
            jQuery('#from-date').focus();
            return false;
        }
        else if(tometa=='' || tometa == undefined)
        {
            alert('Please select a to date');
            jQuery('#to-date').focus();
            return false;
        }
        else
        {
            jQuery('form').reset();
        }

    });
</script>

</html>