<?php
error_reporting( E_ALL ); 
/**
 * @package    Joomla.Site
 *
 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
 */
define('JOOMLA_MINIMUM_PHP', '5.3.10');

if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
{
    die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
}

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php'))
{
    include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', __DIR__);
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';
 
$db = JFactory::getDBO();

// Create a new query object.
$query = $db->getQuery(true);

$query = 'SELECT u.email, p.user_id,p.ip, p.visitDate FROM `pqwoe_saxum_iplogger` as p , `pqwoe_users` as u WHERE p.`visitDate` >= "2018-02-01" and p.`visitDate` <= "2018-04-30" and `user_id` in(347,348) and u.id= p.user_id order by `visitDate` ASC';

$db->setQuery($query);
$rows = $db->loadObjectList();
/*echo count($rows);
echo '<hr/>';
echo '<pre>'; print_r($rows); exit();*/


$from = "2018-02-01";
$to = "2018-04-30";

//from MONTH
$time  = strtotime($from);
$fromMonth = date('F Y',$time);

//to MONTH
$time  = strtotime($to);
$toMonth = date('F Y',$time);

//months between Start and End dates
$start    = new DateTime($from);
$start->modify('first day of this month');
$end      = new DateTime($to);
$end->modify('first day of next month');
$interval = DateInterval::createFromDateString('1 month');
$period   = new DatePeriod($start, $interval, $end);

$months = array();
foreach ($period as $dt) {
    array_push($months, $dt->format("Y-m"));
}
//echo '<pre>'; print_r($months); exit(); 


include 'phpspreadsheet/spreadsheet/vendor/autoload.php';
  
//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();


// Set document properties
$spreadsheet->getProperties()->setCreator('PhpOffice')
        ->setLastModifiedBy('PhpOffice')
        ->setTitle('Office 2007 XLSX Test Document')
        ->setSubject('Office 2007 XLSX Test Document')
        ->setDescription('PhpOffice')
        ->setKeywords('PhpOffice')
        ->setCategory('PhpOffice');

// Add some data
$spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Hello');
// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('URL Added');


$spreadsheet->createSheet();
// Add some data
$spreadsheet->setActiveSheetIndex(1)
        ->setCellValue('A1', 'world!');
// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('URL Removed');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Xlsx)

$writer = new Xlsx($spreadsheet);
$fxls ="archive-" . date('Ymd') . ".xls";
$writer->save($fxls);


header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($fxls));
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize('file.txt'));
readfile($fxls);
exit;
  
?>