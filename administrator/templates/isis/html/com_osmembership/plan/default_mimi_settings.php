<?php
/**
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2012 - 2017 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;
?>
<fieldset class="adminform"><legend class="adminform"><?php echo JText::_('MIMI SETTINGS'); ?></legend>
<div class="control-group">
			<div class="control-label">
				<?php echo JText::_('OSM_ALLOW_DISTRICTDATA');?>
			</div>
			<div class="controls">
				<?php echo $this->lists['allow_districtdata'];?>
			</div>
		</div>
	<?php /*START:Edited -hirin -1/10/2018 -view records limit per plan*/ ?>
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_RECORDS_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="records_limit" id="records_limit" size="10" maxlength="250" value="<?php echo $this->item->records_limit;?>" />
			<?php //echo JText::_('OSM_RECORDS_LIMIT_DESC'); ?>
		</div>
	</div>
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_DISTRICTDATA_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="district_limit" id="district_limit" size="10" maxlength="250" value="<?php echo $this->item->district_limit;?>" />
			<?php //echo JText::_('OSM_DISTRICTDATA_LIMIT_DESC'); ?>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_DOWNLOADPM_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="downloadpm_limit" id="downloadpm_limit" size="10" maxlength="250" value="<?php echo $this->item->downloadpm_limit;?>" />
			<?php //echo JText::_('OSM_DOWNLOADPM_LIMIT_DESC'); ?>
		</div>
	</div>
	<?php /*END:Edited -hirin -1/10/2018 -view records limit per plan*/ ?>
<legend class="adminform"><?php echo JText::_('MVMI SETTINGS'); ?></legend>
	

	
	<div class="control-group">
			<div class="control-label">
				<?php echo JText::_('OSM_ALLOW_VILLAGETDATA');?>
			</div>
			<div class="controls">
				<?php echo $this->lists['allow_villagedata'];?>
			</div>
		</div>
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_RECORDS_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="mvmi_records_limit" id="mvmi_records_limit" size="10" maxlength="250" value="<?php echo $this->item->mvmi_records_limit;?>" />
			<?php// echo JText::_('OSM_RECORDS_LIMIT_DESC'); ?>
		</div>
	</div>
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_DISTRICTDATA_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="mvmi_district_limit" id="mvmi_district_limit" size="10" maxlength="250" value="<?php echo $this->item->mvmi_district_limit;?>" />
			<?php //echo JText::_('OSM_DISTRICTDATA_LIMIT_DESC'); ?>
		</div>
	</div>
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_VILLAGEDATA_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="village_limit" id="village_limit" size="10" maxlength="250" value="<?php echo $this->item->village_limit;?>" />
			<?php //echo JText::_('OSM_VILLAGEDATA_LIMIT_DESC'); ?>
		</div>
	</div>
	<div class="control-group">
		<div class="control-label">
			<?php echo  JText::_('OSM_DOWNLOADPM_LIMIT'); ?>
		</div>
		<div class="controls">
			<input class="input-small" type="text" name="mvmi_downloadpm_limit" id="mvmi_downloadpm_limit" size="10" maxlength="250" value="<?php echo $this->item->mvmi_downloadpm_limit;?>" />
			<?php //echo JText::_('OSM_DOWNLOADPM_LIMIT_DESC'); ?>
		</div>
	</div>
	<?php /*END:Edited -hirin -1/10/2018 -view records limit per plan*/ ?>

