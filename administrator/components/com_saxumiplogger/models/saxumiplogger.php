<?php
/**
 * $Id: saxumiplogger.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );

class SaxumiploggerModelSaxumiplogger extends JModelLegacy
{    
	function SaxumiploggerModelSaxumiplogger()
	{
		parent::__construct();
	}

    function getPlugins()
    {
    	$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);

		$query->select('folder AS type, name AS name, enabled')
			->from('#__extensions')
			->where('type ='.$db->Quote('plugin'))
			->where('element like'.$db->Quote('saxumiplogger%'))
			->order('ordering');

		$plugins = $db->setQuery($query)
			->loadObjectList();

		if ($error = $db->getErrorMsg()) {
			JError::raiseWarning(500, $error);
			return false;
		}
    	
        return $plugins;
    }

}
