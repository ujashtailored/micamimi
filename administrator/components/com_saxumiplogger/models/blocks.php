<?php
/**
 * $Id: blocks.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.modellist' );

class SaxumiploggerModelBlocks extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'ip','a.ip'	,
				'description','a.description'
			);
		}

		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		// Create a new query object.		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
				)
			);
			
		$query->from('#__saxum_iplogger_block as a');
		
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'ip:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 3), true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'description:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 12), true) . '%');
				$query->where('(description LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
		}
		
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		
		return $query;
	}
	
	protected function populateState($ordering = null, $direction = null) 
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);	
		
        parent::populateState('a.id', 'asc');
	}

}
  	
