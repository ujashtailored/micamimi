<?php
/**
 * $Id: excp.php 129 2015-08-10 07:21:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

class SaxumiploggerTableExcp extends JTable
{
	/** @var int Primary key */
	var $id = 0;
	/** @var string */
	var $ip ='';
	var $description='';
	 
	function __construct(&$db)
 	{
    	parent::__construct( '#__saxum_iplogger_excp', 'id', $db );
    }

	public function check()
	{	
		require_once JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'controller.php';
		
		$err=0;
		if(preg_match("[-]", $this->ip)){
			$ips=explode("-",$this->ip);
			foreach ($ips as $ip){
				//first of all the format of the ip address is matched
				if(preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/",$ip) && $err==0){
					//now all the intger values are separated
    				$parts=explode(".",$ip);
					//now we need to check each part can range from 0-255
				    foreach($parts as $ip_parts)
				    {
				    	if(intval($ip_parts)>255 || intval($ip_parts)<0)
				    	$err=1; //if number is not within range of 0-255
				    }
				    $err=0;
				} else {
					$err=1;
				}
			}
		}
		else {
			if(preg_match("/^(((\d{1,3})[.]){3}(\d{1,3})$)/",$this->ip)||preg_match("/^((\d{1,3})\.){1,3}$/",$this->ip)){
				//now all the intger values are separated
   				$parts=explode(".",$this->ip);
				//now we need to check each part can range from 0-255
			    foreach($parts as $ip_parts)
			    {
			    	if(intval($ip_parts)>255 || intval($ip_parts)<0)
			    	$err=1; //if number is not within range of 0-255
			    }
			    $err=0;
			} else {
				$err=1;
			}
		}

		if($err==1)
		{
			$this->setError(JText::_('COM_SAXUMIPLOGGER_NO_VALID_IP'));
			return false;
		}
		
		$db =& JFactory::getDBO();
		$sql = $db->getQuery(true)
			->select('ip')
			->from('#__saxum_iplogger_excp')
			->where('id!='.$this->id);
		$db->setQuery($sql);
		$iptable = $db->loadColumn();
		$err=SaxumiploggerController::ipinlist($iptable,$this->ip);
			
		if($err==1)
		{
  			JError::raiseWarning( 500, JText::_( 'COM_SAXUMIPLOGGER_ALREADY_REGISTERED') );
			return false;
		}
		
		return true;
	}
}
