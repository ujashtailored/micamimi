<?php
/**
 * $Id: view.html.php 125 2015-08-01 12:36:59Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/


defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewBlock extends JViewLegacy
{

	public function display($tpl = null) 
	{
		// get the Data
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user = JFactory::getUser();
		$isNew = ($this->item->id == 0);
		$canDo = SaxumiploggerController::getActions();
		JHTML::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css' );
		JToolBarHelper::title($isNew ? JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_('COM_SAXUMIPLOGGER_BLOCK_NEW') : JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_('COM_SAXUMIPLOGGER_BLOCK_EDIT'), 'block.png');
		if ($isNew) 
		{
			JToolBarHelper::apply('block.apply');
			JToolBarHelper::save('block.save');
			JToolBarHelper::custom('block.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
		}
		else
		{
			JToolBarHelper::apply('block.apply');
			JToolBarHelper::save('block.save');
			JToolBarHelper::custom('block.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			JToolBarHelper::custom('block.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
		}
		JToolBarHelper::cancel('block.cancel', $isNew ? 'JTOOLBAR_CANCEL'
		                                                   : 'JTOOLBAR_CLOSE');
	}
	
}
?>

