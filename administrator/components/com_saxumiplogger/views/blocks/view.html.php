<?php
/**
 * $Id: view.html.php 206 2016-08-14 21:02:15Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewBlocks extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	
	function display($tpl = null) 
	{
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		SaxumiploggerController::addSubmenu('blocks');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		jimport('joomla.filesystem.file');
		$pEnabled = JPluginHelper::getPlugin('system','saxumiplogger_block');

		if(!$pEnabled) JError::raiseNotice(500, JText::_('COM_SAXUMIPLOGGER_BLOCK_PLUGIN_DISABLED'));
  		JError::raiseNotice( 500, JText::_( 'COM_SAXUMIPLOGGER_OWN_IP').': '.SaxumiploggerController::getip() );
		
		// Set the toolbar
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();
 
		// Display the template
		parent::display($tpl);
	}
	
	protected function addToolBar() 
	{
		$canDo = SaxumiploggerController::getActions();
		JHTML::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css' );
		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_('COM_SAXUMIPLOGGER_BLOCKED'),'block.png');
		if ($canDo->get('core.block.edit')) 
		{
			JToolBarHelper::addNew('block.add');
			JToolBarHelper::editList('block.edit');
			JToolBarHelper::deleteList('', 'blocks.delete');
			JToolBarHelper::divider();
		}
		JToolBarHelper::cancel( 'cancel', 'Close' );
	}
	
	protected function getSortFields()
	{
		return array(
			'a.id' => JText::_('JGRID_HEADING_ID'),
			'a.ip' => JText::_('COM_SAXUMIPLOGGER_IP'),
			'a.description' => JText::_('COM_SAXUMIPLOGGER_DESCR')
		);
	}
	
}
?>
