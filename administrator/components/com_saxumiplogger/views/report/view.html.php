<?php
/**
 * $Id: view.html.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewReport extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $params;
	
	function display($tpl = null)
	{	
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		$this->users		= $this->get('Users');
		
		$this->params = JComponentHelper::getParams( 'com_saxumiplogger' );
		$option = JRequest::getCmd('option');
		SaxumiploggerController::location_update( );

		SaxumiploggerController::addSubmenu('report');
		
        $this->sortDirection = $this->state->get('list.direction');
        $this->sortColumn = $this->state->get('list.ordering');
                
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
				
		// Set the toolbar
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();
		
		parent::display($tpl);
	}
	
	protected function addToolBar() 
	{
		$canDo = SaxumiploggerController::getActions();
		JHTML::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css' );
			
		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_( 'COM_SAXUMIPLOGGER_REPORT' ), 'report.png' );	
		if (JRequest::getCmd('mode')=='details') {
			JToolBarHelper::back();
		}
		if ($canDo->get('core.refresh')){
   			JToolBarHelper::custom('refresh','refresh','refresh',JText::_( 'COM_SAXUMIPLOGGER_REFRESH' ),true,false);
		}
		if ($canDo->get('core.export')){
   			JToolBarHelper::custom('export','archive','archive',JText::_( 'COM_SAXUMIPLOGGER_EXPORT' ),false,false);
		}
		if ($canDo->get('core.purge')){
   			JToolBarHelper::custom('purge','delete','delete',JText::_( 'COM_SAXUMIPLOGGER_PURGE' ),false,false);
		}
		JToolBarHelper::cancel( 'cancel', 'Close' );
		
		JHtmlSidebar::setAction('index.php?option=com_saxumiplogger&view=report');

		JHtmlSidebar::addFilter(
			JText::_('COM_SAXUMIPLOGGER_SELECT_USER'),
			'filter_userid',
			JHtml::_('select.options', $this->users, 'id', 'username', $this->state->get('filter.userid'), true)
		);
	}
	
	protected function getSortFields()
	{
		return array(
			'username' => JText::_('COM_SAXUMIPLOGGER_USER'),
			'name' => JText::_('COM_SAXUMIPLOGGER_NAME'),
			'email' => JText::_('COM_SAXUMIPLOGGER_EMAIL'),
			'a.ip' => JText::_('COM_SAXUMIPLOGGER_IP'),
			'a.country_code, a.city' => JText::_('COM_SAXUMIPLOGGER_COUNTRY'),
			'a.visitDate' => JText::_('COM_SAXUMIPLOGGER_DATE'),
			'a.client_id' => JText::_('COM_SAXUMIPLOGGER_CLIENT')
		);
	}
}
