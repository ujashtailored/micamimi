<?php
/**
 * $Id: default.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
?>

<form action="index.php?option=com_saxumiplogger&view=info" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>

		<h2><font color="black">Saxum IPLogger</font></h2>
		
		<p>This extension stores the IP-addresses of the site's registered users when they log in and makes statistics about their location. You should inform them about this in your privacy policy or terms of service.</p>
		<p>This product includes GeoLite data created by MaxMind, available from <a href="http://www.maxmind.com" target="_blank">www.maxmind.com</a>.</p>
		<p>Created by Saxum 2003 Bt. Please visit <a href="http://saxum2003.hu" target="_blank">www.saxum2003.hu</a> to find out more about us. </p>
		
		<h3>Version</h3>
		<p><?php echo $this->version;?></p>
		
		<h3>Copyright</h3>
		<p>© 2010 - <?php echo date("Y"); ?> Saxum 2003 Bt.</p>
		
		<h3>License</h3>
		<p><a href="http://www.gnu.org/licenses/gpl-3.0.html" target="_blank">GPLv3</a></p>
		
		<h3>Icons</h3>
		<p>Icons used in this component are designed by <a href="http://kyo-tux.deviantart.com" target="_blank">Kyo-Tux (Asher)</a></p>
		
		<input type="hidden" name="option" value="com_saxumiplogger" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="controller" value="" />
	</div>
</form>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>
