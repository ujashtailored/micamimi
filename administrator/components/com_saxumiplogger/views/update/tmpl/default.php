<?php
/**
 * $Id: default.php 18 2013-12-19 12:26:36Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access'); 
?>

<script type="text/javascript">
<!--
function processAction( action )
{
	var dv=document.getElementById("dvMain");
	dv.style.display = "none";
	dv=document.getElementById("dvUpload");
	dv.style.display = "none";
	dv=document.getElementById("dvSub");
	dv.style.display = "block";
	
	var form = document.adminForm;
	form.task.value=action;
	submitform();
}
function check_file(upload_max_filesize,post_max_size ){
	var c=document.getElementById("upload_file");
	var s=upload_max_filesize.slice(-1);
	var f=upload_max_filesize.substring(0,upload_max_filesize.length-1);
	if (s=='K') f=f*1024;
	if (s=='M') f=f*1024*1024;
	if (s=='G') f=f*1024*1024*1024;
	s=post_max_size.slice(-1);
	var p=post_max_size.substring(0,post_max_size.length-1);
	if (s=='K') p=p*1024;
	if (s=='M') p=p*1024*1024;
	if (s=='G') p=p*1024*1024*1024;
	if (f<c.files[0].size) alert(Joomla.JText._("COM_SAXUMIPLOGGER_PHP_MAX_FILESIZE")+upload_max_filesize);
	if (p<c.files[0].size) alert(Joomla.JText._("COM_SAXUMIPLOGGER_PHP_POST_SIZE")+post_max_size);
}
//-->
</script>

<form action="index.php?option=com_saxumiplogger&view=update" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>

		<div id="dvMain" name="dvMain" class="fltlft"
		<?php 
		if (!in_array('curl', get_loaded_extensions()) && ini_get('allow_url_fopen')!=1){
			echo 'style="display:none;"';
		}
		?>
		>
			<fieldset class="uploadform">
				<legend><?php echo JText::_('COM_SAXUMIPLOGGER_DATAFILE_STATUS'); ?></legend>
				<p>
				<?php 
				$date = JFactory::getDate();
				$d=$this->params->get('date_format');
				if (empty($d)){$d='Y-m-d H:i:s';} 
				if ($this->file['size']==0){
					echo JText::_( 'COM_SAXUMIPLOGGER_EMPTY_DATAFILE_TEXT' )."<br>";
				}
				elseif ($date->toUnix()-$this->file['moddate']>3456000) {
					echo JText::_( 'COM_SAXUMIPLOGGER_OLD_DATAFILE_TEXT' )."<br>";
					echo JText::_( 'COM_SAXUMIPLOGGER_MODIFICATION_DATE_TEXT' );
					echo JHTML::_('date',$this->file['moddate'], $d);
				}
				else {
					echo JText::_( 'COM_SAXUMIPLOGGER_DATAFILE_TEXT' )."<br>";
					echo JText::_( 'COM_SAXUMIPLOGGER_MODIFICATION_DATE_TEXT' )." ";
					echo JHTML::_('date',$this->file['moddate'], $d);
				}
				?></p>
				<p>
				<label for="update_file"><?php echo JText::_( 'COM_SAXUMIPLOGGER_REFRESH_TEXT' )?></label> 
				<input class="btn btn-primary" type="button" id="update_file" name="update_file" value="<?php echo JText::_( 'COM_SAXUMIPLOGGER_UPDATE' ); ?>" onclick="processAction('file_update');" /><br>
				</p>
				<p>
				<?php echo JText::_( 'COM_SAXUMIPLOGGER_PATIENCE_TEXT' );?>
				</p>
			</fieldset>
		</div>
		
		<div id="dvUpload" name="dvUpload" class="fltlft">
			<fieldset class="uploadform">
				<legend><?php echo JText::_('COM_SAXUMIPLOGGER_UPLOAD_FILE'); ?></legend>
				<label>
				<?php 
				if (!in_array('curl', get_loaded_extensions()) && ini_get('allow_url_fopen')!=1){
					echo JText::_( 'COM_SAXUMIPLOGGER_NOCURL_NOFOPEN' );
				} else {
					echo JText::_( 'COM_SAXUMIPLOGGER_MANUAL_UPDATE' );			
				}
				?></label>
				<label>
				<?php echo JText::sprintf( 'COM_SAXUMIPLOGGER_DOWNLOAD_URL','http://saxum2003.hu/downloads/category/2-iplogger.html?download=10%3Adatafile');?>
				</label>
				<label for="upload_file"><?php echo JText::_('COM_SAXUMIPLOGGER_DATA_FILE'); ?></label>
				<input class="input_box" id="upload_file" name="upload_file" type="file" size="57" onchange="check_file(<?php echo '\''.ini_get('upload_max_filesize').'\',\''.ini_get('post_max_size').'\'';?>);"/>
				<input class="btn btn-primary" type="button" value="<?php echo JText::_('COM_SAXUMIPLOGGER_UPLOAD'); ?>" onclick="processAction('file_upload');"  />
			</fieldset>
		</div>
		
		<div id="dvSub" name="dvSub" style="display:none; width:100%; height: 300px; float:left; ">
			<br/><br/><br/><br/><br/><br/><br/>
			<div style="vertical-align:middle; text-align:center;">
				<?php echo JHTML::_('image','administrator/components/com_saxumiplogger/assets/images/wait.gif','') ?>
				<br /><?php echo JText::_('COM_SAXUMIPLOGGER_PATIENCE_TEXT'); ?><br />
				<br /><br /><br />
			</div>
		</div>
		
		<input type="hidden" name="option" value="com_saxumiplogger" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="controller" value="" />
	</div>
</form>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>
