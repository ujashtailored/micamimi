<?php
/**
 * $Id: view.html.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access'); 

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewUpdate extends JViewLegacy
{
	function display($tpl = null)
	{	
		SaxumiploggerController::addSubmenu('update');
		
		$mainframe = JFactory::getApplication();
		$option = JRequest::getCmd('option');
		
		// Access check.
		if (!JFactory::getUser()->authorise('core.update', 'com_saxumiplogger')){
			return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		}
		
		$file_to= JPATH_COMPONENT.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat';
		
		if (file_exists($file_to)) {
			$file['size']	= filesize($file_to);
			$file['moddate'] = filemtime($file_to);
		}else
		{
			$file['size']	= 0;
		}
		
		$params = JComponentHelper::getParams( 'com_saxumiplogger' );
		
		$this->assignRef('params',		$params);
		$this->assignRef( 'file'	, $file );
		
		// Set the toolbar
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display( $tpl );
	}
	
	protected function addToolBar() 
	{
		JHTML::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css' );
			
		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_( 'COM_SAXUMIPLOGGER_REPORT' ), 'report.png' );
		JToolBarHelper::cancel( 'cancel', 'Close' );	
	}

}

