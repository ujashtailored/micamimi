<?php
/**
 * $Id: default_user.php 35 2014-02-19 21:03:56Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');

$version = new JVersion;
if ($version->getShortVersion()<'3.2.2'){
	$canDo = UsersHelper::getActions();
} else {
	$canDo = JHelperContent::getActions('com_users');
}
$loggeduser = JFactory::getUser();
$countries=$this->countries;
$cities=$this->cities;

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="index.php?option=com_saxumiplogger&view=statistics" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<div class="adminform">
		    <div class="sax-cpanel-left">
				<div id="filter-bar" class="btn-toolbar">
					<div class="filter-search btn-group pull-left">
						<label for="filter_search" class="element-invisible"><?php echo JText::_('COM_SAXUMIPLOGGER_FILTER_SEARCH_DESC'); ?></label>
						<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="<?php echo JHtml::tooltipText('COM_SAXUMIPLOGGER_FILTER_SEARCH_DESC'); ?>" />
					</div>
					<div class="btn-group pull-left hidden-phone">
						<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
						<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
					</div>
					<div class="btn-group pull-right hidden-phone">
						<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
						<?php echo $this->pagination->getLimitBox(); ?>
					</div>
					<div class="btn-group pull-right hidden-phone">
						<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
						<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
							<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
							<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
							<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');  ?></option>
						</select>
					</div>
					<div class="btn-group pull-right">
						<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
						<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
							<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
							<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
						</select>
					</div>
				</div>
				<div class="clearfix"> </div>
				<table class="table table-striped" id="statistics">
				<thead>
					<tr>
						<th>
							<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_USER'), 'username', $this->sortDirection, $this->sortColumn); ?>
						</th>
						<th>
							<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NAME'), 'name', $this->sortDirection, $this->sortColumn); ?>
						</th>
						<th width="100">
							<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NUM_OF_LOGINS'), 'total', $this->sortDirection, $this->sortColumn); ?>
						</th>			
						<th width="100">
							<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NUM_OF_IPS'), 'ip_count', $this->sortDirection, $this->sortColumn); ?>
						</th>			
						<th width="100">
							<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NUM_OF_COUNTRIES'), 'country_count', $this->sortDirection, $this->sortColumn); ?>
						</th>			
						<th width="100">
							<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NUM_OF_CITIES'), 'city_count', $this->sortDirection, $this->sortColumn); ?>
						</th>
					</tr>			
				</thead>
				<?php
				$k = 0;
				for ($i=0, $n=count( $this->items ); $i < $n; $i++)
				{
					$row = &$this->items[$i];
					$canEdit	= $canDo->get('core.edit');
					$canChange	= $loggeduser->authorise('core.edit.state',	'com_users');
					// If this group is super admin and this user is not super admin, $canEdit is false
					if ((!$loggeduser->authorise('core.admin')) && JAccess::check($row->id, 'core.admin')) {
						$canEdit	= false;
						$canChange	= false;
					}
					?>
					<tr class="<?php echo "row$k"; ?>">
						<td>
							<?php if ($canEdit && (int)$row->user_id!=0) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id='.(int) $row->user_id); ?>">
								<?php echo JText::_($row->username); ?></a>
							<?php else : ?>
								<?php echo  JText::_($row->username); ?>
							<?php endif; ?>
						</td>
						<td>
							<?php if ($canEdit && (int)$row->user_id!=0) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id='.(int) $row->user_id); ?>">
								<?php echo JText::_($row->name); ?></a>
							<?php else : ?>
								<?php echo  JText::_($row->name); ?>
							<?php endif; ?>
						</td>
						<td align=center>
							<a href="<?php echo 'index.php?option=com_saxumiplogger&amp;view=report&amp;filter_search=user:'.JText::_($row->username).'&amp;mode=details'; ?>">
							<?php echo $row->total; ?>
							</a>
						</td>
						<td align=center>
							<a href="<?php echo 'index.php?option=com_saxumiplogger&amp;task=details&amp;user_id='. $row->user_id.'&amp;mode=ip'; ?>">
							<?php echo $row->ip_count; ?>
							</a>
						</td>
						<td align=center>
							<a href="<?php echo 'index.php?option=com_saxumiplogger&amp;task=details&amp;user_id='. $row->user_id.'&amp;mode=country'; ?>">
							<?php echo $row->country_count; ?>
							</a>
						</td>
						<td align=center>
							<a href="<?php echo 'index.php?option=com_saxumiplogger&amp;task=details&amp;user_id='. $row->user_id.'&amp;mode=city'; ?>">
							<?php echo $row->city_count; ?>
							</a>
						</td>
					</tr>
					<?php
					$k = 1 - $k;
				}
				?>
				<tfoot>
						<tr>
							<td colspan="10">
								<?php echo $this->pagination->getListFooter(); 
								?>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="sax-cpanel-right"">
			    <fieldset class="adminform">
		        <legend><?php echo JText::_('COM_SAXUMIPLOGGER_TOP_10_COUNTRIES'); ?></legend> 
					<?php include_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'statistics'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'countries.php');?>
				</fieldset>
			    <fieldset class="adminform">
		        <legend><?php echo JText::_('COM_SAXUMIPLOGGER_TOP_10_CITIES'); ?></legend> 
					<?php include_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'statistics'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'cities.php');?>
				</fieldset>
			</div>
		</div>
		<input type="hidden" name="option" value="com_saxumiplogger" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="tpl" value="<?php echo $this->tpl; ?>" />
		<input type="hidden" name="controller" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $this->sortColumn; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->sortDirection; ?>" />
	</div>
</form>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>