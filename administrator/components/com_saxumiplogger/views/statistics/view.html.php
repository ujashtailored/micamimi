<?php
/**
 * $Id: view.html.php 18 2013-12-19 12:26:36Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewStatistics extends JViewLegacy
{
	protected $pagination;
	protected $state;
	/**
	 * Report view display method
	 * @return void
	 **/
	function display($tpl = null)
	{	
		$mainframe = JFactory::getApplication();
		$option = JRequest::getCmd('option');
		JHtml::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css'  );
		$canDo = SaxumiploggerController::getActions();
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');

		SaxumiploggerController::location_update( );
		SaxumiploggerController::addSubmenu('statistics');
		
        $this->sortDirection = $this->state->get('list.direction');
        $this->sortColumn = $this->state->get('list.ordering');

		$this->params = JComponentHelper::getParams( 'com_saxumiplogger' );

		if ($tpl == null) $tpl=JRequest::getCmd('tpl');
		if ($tpl == null) {
			if ($this->params->get('default_stat')==0) $tpl='user';
			if ($this->params->get('default_stat')==1) $tpl='ip';
		}

		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_( 'COM_SAXUMIPLOGGER_STATISTICS' ), 'statistics' );
		if ($tpl=='ip') {
   			JToolBarHelper::custom('change_group','copy','copy',JText::_( 'COM_SAXUMIPLOGGER_GROUP_BY_USER' ),false,false);
		} else {
			JToolBarHelper::custom('change_group','copy','copy',JText::_( 'COM_SAXUMIPLOGGER_GROUP_BY_IP' ),false,false);
		}	
		if ($canDo->get('core.purge')){
	   		JToolBarHelper::custom('purge','delete','delete',JText::_( 'COM_SAXUMIPLOGGER_PURGE' ),false,false);
		}	
		JToolBarHelper::cancel( 'cancel', 'Close' );

		$this->tpl=$tpl;
        $model = $this->getModel(); // Statistics

		// Get data from the model
		if ($tpl=='user'){
			$this->items = $this->get('Items');
			$countries  = $model->getCountries();
			$cities 	= $model->getCities();
		
			$this->assignRef('countries',	$countries);
			$this->assignRef('cities',		$cities);
		}
		if ($tpl=='ip'){
			$this->items = $model->getIpData();
			$ips 	= $model->getIps();
			$this->assignRef('ips',	$ips);
		}
        
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}
		
	function details($tpl=null)
	{
		$mainframe = JFactory::getApplication();
		$option = JRequest::getCmd('option');
		jimport('joomla.html.pagination');
		JHtml::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css'  );
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		
		$params = JComponentHelper::getParams( 'com_saxumiplogger' );
		SaxumiploggerController::addSubmenu('statistics');
		
        $this->sortDirection = $this->state->get('list.direction');
        $this->sortColumn = $this->state->get('list.ordering');

		$this->params = JComponentHelper::getParams( 'com_saxumiplogger' );

		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_( 'COM_SAXUMIPLOGGER_STATISTICS' ), 'statistics' );	
		JToolBarHelper::back();
		JToolBarHelper::cancel('cancel','Close');

        $model = $this->getModel(); // Statistics

		$this->items		= $model->getDetails();

		JHTML::_('behavior.tooltip');

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}
	
	protected function getSortFields()
	{
		return array(
			'username' => JText::_('COM_SAXUMIPLOGGER_USER'),
			'name' => JText::_('COM_SAXUMIPLOGGER_NAME'),
			'total' => JText::_('COM_SAXUMIPLOGGER_NUM_OF_LOGINS'),
			'ip_count' => JText::_('COM_SAXUMIPLOGGER_NUM_OF_IPS'),
			'country_count' => JText::_('COM_SAXUMIPLOGGER_NUM_OF_COUNTRIES'),
			'city_count' => JText::_('COM_SAXUMIPLOGGER_NUM_OF_CITIES')
		);
	}
	
	protected function getSortFields2()
	{
		return array(
			'ip' => JText::_('COM_SAXUMIPLOGGER_IP'),
			'country_code' => JText::_('COM_SAXUMIPLOGGER_COUNTRY'),
			'city' => JText::_('COM_SAXUMIPLOGGER_CITY'),
			'total' => JText::_('COM_SAXUMIPLOGGER_NUM_OF_LOGINS'),
			'user_count' => JText::_('COM_SAXUMIPLOGGER_NUM_OF_USERS')
		);
	}
}
