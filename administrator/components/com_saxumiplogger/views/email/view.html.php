<?php
/**
 * $Id: view.html.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/


defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewEmail extends JViewLegacy
{

	public function display($tpl = null) 
	{
		// get the Data
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
	}
	
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		JRequest::setVar('hidemainmenu', true);
		$user = JFactory::getUser();
		$isNew = ($this->item->id == 0);
		$canDo = SaxumiploggerController::getActions();
		JHTML::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css' );
		JToolBarHelper::title($isNew ? JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_('COM_SAXUMIPLOGGER_EMAIL_NEW') : JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_('COM_SAXUMIPLOGGER_EMAIL_EDIT'), 'email.png');
		if ($isNew) 
		{
			JToolBarHelper::apply('email.apply');
			JToolBarHelper::save('email.save');
			}
		else
		{
			JToolBarHelper::apply('email.apply');
			JToolBarHelper::save('email.save');
			}
		JToolBarHelper::cancel('email.cancel', $isNew ? 'JTOOLBAR_CANCEL'
		                                                   : 'JTOOLBAR_CLOSE');
	}
	
}
?>

