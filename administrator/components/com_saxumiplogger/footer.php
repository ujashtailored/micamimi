<?php
/**
 * $Id: footer.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/
defined('_JEXEC') or die('Restricted access');

echo '<div style="clear:both"><p align=center>Powered by <a href="http://saxum2003.hu"><b>Saxum</b></a></p></div>';
?>