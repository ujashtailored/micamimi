<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'user.cancel' || document.formvalidator.isValid(document.getElementById('user-form')))
		{
			Joomla.submitform(task, document.getElementById('user-form'));
		}
	};

	Joomla.twoFactorMethodChange = function(e)
	{
		var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

		jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
			if (el.id != selectedPane)
			{
				jQuery('#' + el.id).hide(0);
			}
			else
			{
				jQuery('#' + el.id).show(0);
			}
		});
	};
");

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>

<form action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="user-form" class="form-validate form-horizontal" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_USERS_USER_ACCOUNT_DETAILS')); ?>

				<?php //START::Edited -Mrunal -17/06/2017 -get other user fields ?>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_designation-lbl" for="jform_designation" class="hasPopover required"
							title="<?php echo JText::_( 'DESIGNATION' ); ?>" data-content="<?php echo JText::_('Select your designation.') ?>" >
							<?php echo JText::_( 'DESIGNATION' ); ?>
						</label>
					</div>
					<div class="controls">
						<select name="jform[designation]" id="jform_designation" value="" >
							<option value="mr" 		<?php if($this->item->designation == "mr" 	) { echo "selected"; } ?> >	Mr.		</option>
							<option value="miss" 	<?php if($this->item->designation == "miss" ) { echo "selected"; } ?> >	Miss.	</option>
							<option value="mrs" 	<?php if($this->item->designation == "mrs" 	) { echo "selected"; } ?> >	Mrs.	</option>
							<option value="dr" 		<?php if($this->item->designation == "dr" 	) { echo "selected"; } ?> >	Dr.		</option>
							<option value="prof" 	<?php if($this->item->designation == "prof" ) { echo "selected"; } ?> >	Prof.	</option>
						</select>
					</div>
				</div>

				<?php //Edited -Mrunal -17/06/2017 -breck foreach for main fields to get custom fields in beween default fields.
				$fields = $this->form->getFieldset('user_details');

				$field = $fields['jform_name']; ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<?php echo $field->input; ?>
					</div>
				</div>
				<?php $field = $fields['jform_username']; ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<?php echo $field->input; ?>
					</div>
				</div>

				<?php $field = $fields['jform_password']; ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<input type="password" style="display:none">
						<?php echo $field->input; ?>
					</div>
				</div>

				<?php $field = $fields['jform_password2']; ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<?php echo $field->input; ?>
					</div>
				</div>

				<?php $field = $fields['jform_email']; ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<?php echo $field->input; ?>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label id="jform_institute_name-lbl" for="jform_institute_name" class="hasPopover required"
							title="<?php echo JText::_( 'Company' ); ?>" data-content="<?php echo JText::_('Enter your company.') ?>" >
							<?php echo JText::_( 'Company' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<input type="text" name="jform[institute_name]" id="jform_institute_name" value="<?php echo $this->item->institute_name; ?>" class="required" size="40" required="required" >
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_address-lbl" for="jform_address" class="hasPopover"
							title="<?php echo JText::_( 'ADDRESS' ); ?>" data-content="<?php echo JText::_('Enter your address.') ?>" >
							<?php echo JText::_( 'ADDRESS' ); ?>
						</label>
					</div>
					<div class="controls">
						<textarea name="jform[address]" id="jform_address" ><?php echo $this->item->address; ?></textarea>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_mobile-lbl" for="jform_mobile" class="hasPopover required"
							title="<?php echo JText::_( 'MOBILE' ); ?>" data-content="<?php echo JText::_('Enter your mobile.') ?>" >
							<?php echo JText::_( 'MOBILE' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<input type="text" name="jform[mobile]" id="jform_mobile" value="<?php echo $this->item->mobile; ?>" class="required" size="40" required="required" />
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_contactnumber-lbl" for="jform_contactnumber" class="hasPopover required"
							title="<?php echo JText::_( 'CONTACT_NUMBER' ); ?>"	data-content="<?php echo JText::_('Enter your contact number.') ?>" >
							<?php echo JText::_( 'CONTACT_NUMBER' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<input type="text" name="jform[contactnumber]" id="jform_contactnumber" value="<?php echo $this->item->contactnumber; ?>" class="required" size="40" required="required" />
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label id="jform_paymentoption-lbl" for="jform_paymentoption" class="hasPopover required"
							title="<?php echo JText::_( 'PAYMENT_OPTION' ); ?>"	data-content="<?php echo JText::_('Enter your payment option.') ?>" >
							<?php echo JText::_( 'PAYMENT_OPTION' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<select name="jform[paymentoption]" id="jform_paymentoption" class="required">
							<option value="1" 		<?php if($this->item->paymentoption == "1" 	) { echo "selected"; } ?> >	Cheque </option>
							<option value="2" 		<?php if($this->item->paymentoption == "2" 	) { echo "selected"; } ?> >	DD	</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_chequeno-lbl" for="jform_chequeno" class="hasPopover required"
							title="<?php echo JText::_( 'CHEQUE_NUMBER' ); ?>"		data-content="<?php echo JText::_('Enter your cheque number.') ?>" >
							<?php echo JText::_( 'CHEQUE_NUMBER' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<input type="text" name="jform[chequeno]" id="jform_chequeno" value="<?php echo $this->item->chequeno; ?>" class="required" size="40" required="required" />
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_bankname-lbl" for="jform_bankname" class="hasPopover required"
							title="<?php echo JText::_( 'BANK_NAME' ); ?>" data-content="<?php echo JText::_('Enter your bank name.') ?>" >
							<?php echo JText::_( 'BANK_NAME' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<input type="text" name="jform[bankname]" id="jform_bankname" value="<?php echo $this->item->bankname; ?>" class="required" size="40" required="required" />
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_bankname-lbl" for="jform_bankname" class="hasPopover required"
							title="<?php echo JText::_( 'PAYMENT_DATE' ); ?>"	data-content="<?php echo JText::_('Enter your payment date.') ?>" >
							<?php echo JText::_( 'PAYMENT_DATE' ); ?>
							<span class="star">&nbsp;*</span>
						</label>
					</div>
					<div class="controls">
						<?php echo JHTML::_('calendar', $this->item->date ,'jform[date]','jform_date',$format = '%Y-%m-%d',array('class'=>'inputbox required','size'=>'35','maxlength'=>'20','style'=>'height:24px;')); ?>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label id="jform_whywnattobuy-lbl" for="jform_whywnattobuy" class="hasPopover"
							title="<?php echo JText::_( 'WHYWANTTOBUY' ); ?>" data-content="" >
							<?php echo JText::_( 'WHYWANTTOBUY' ); ?>
						</label>
					</div>
					<div class="controls">
						<textarea name="jform[whywnattobuy]" id="jform_whywnattobuy" ><?php echo $this->item->whywnattobuy; ?></textarea>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label id="jform_ipaddress-lbl" for="jform_ipaddress" class="hasPopover"
							title="<?php echo JText::_( 'Allowed IP' ); ?>" data-content="<?php echo JText::_('Multiple IP comma seperated.') ?>" >
							<?php echo JText::_( 'Allowed IP' ); ?>
						</label>
					</div>
					<div class="controls">
						<textarea name="jform[ipaddress]" id="jform_ipaddress" ><?php echo $this->item->ipaddress; ?></textarea>
					</div>
				</div>

				<?php $fields_rendered = array('jform_name','jform_username','jform_password','jform_password2','jform_email');
				foreach ($this->form->getFieldset('user_details') as $key => $field) : ?>
					<?php if(!in_array($key, $fields_rendered)){ ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php if ($field->fieldname == 'password') : ?>
								<?php // Disables autocomplete ?> <input type="password" style="display:none">
							<?php endif; ?>
							<?php echo $field->input; ?>
						</div>
					</div>
					<?php } ?>
				<?php endforeach; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

			<?php if ($this->grouplist) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS')); ?>
					<?php echo $this->loadTemplate('groups'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>

			<?php
			$this->ignore_fieldsets = array('user_details');
			echo JLayoutHelper::render('joomla.edit.params', $this);
			?>

		<?php if (!empty($this->tfaform) && $this->item->id) : ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'twofactorauth', JText::_('COM_USERS_USER_TWO_FACTOR_AUTH')); ?>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
						title="<?php echo '<strong>' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL') . '</strong><br />' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_DESC'); ?>">
					<?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL'); ?>
				</label>
			</div>
			<div class="controls">
				<?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false); ?>
			</div>
		</div>
		<div id="com_users_twofactor_forms_container">
			<?php foreach ($this->tfaform as $form) : ?>
			<?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
			<div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
				<?php echo $form['form'] ?>
			</div>
			<?php endforeach; ?>
		</div>

		<fieldset>
			<legend>
				<?php echo JText::_('COM_USERS_USER_OTEPS'); ?>
			</legend>
			<div class="alert alert-info">
				<?php echo JText::_('COM_USERS_USER_OTEPS_DESC'); ?>
			</div>
			<?php if (empty($this->otpConfig->otep)) : ?>
			<div class="alert alert-warning">
				<?php echo JText::_('COM_USERS_USER_OTEPS_WAIT_DESC'); ?>
			</div>
			<?php else : ?>
			<?php foreach ($this->otpConfig->otep as $otep) : ?>
			<span class="span3">
				<?php echo substr($otep, 0, 4); ?>-<?php echo substr($otep, 4, 4); ?>-<?php echo substr($otep, 8, 4); ?>-<?php echo substr($otep, 12, 4); ?>
			</span>
			<?php endforeach; ?>
			<div class="clearfix"></div>
			<?php endif; ?>
		</fieldset>

		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</fieldset>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
