<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<style type="text/css">
	#facebook{
		margin-top:30px;
		float:left;
	}
	.facebook_block{
		background-color:#9FC0FF;
		border:2px solid #3B5998;
		float:left;
		height:30px;
		margin-left:5px;
		width:8px;
	    opacity:0.1;
		-webkit-transform:scale(0.7);
		-webkit-animation-name: facebook;
	 	-webkit-animation-duration: 1s;
	 	-webkit-animation-iteration-count: infinite;
	 	-webkit-animation-direction: linear;
	}
	#block_1{
	 	-webkit-animation-delay: .1s;
	}
	#block_2{
	 	-webkit-animation-delay: .2s;
	}
	#block_3{
	 	-webkit-animation-delay: .3s;
	}
	#block_4{
	 	-webkit-animation-delay: .4s;
	}
	#block_5{
	 	-webkit-animation-delay: .5s;
	}
	#block_6{
	 	-webkit-animation-delay: .6s;
	}
	@-webkit-keyframes facebook{
		0%{-webkit-transform: scale(1.2);opacity:1;}
		100%{-webkit-transform: scale(0.7);opacity:0.1;}
	}
</style>

<?php $app = JFactory::getApplication();

if($app->input->get('ajaxprocess', 0, 'int') == 0 ) { ?>

	<form  name="adminForm" id="adminForm" enctype="multipart/form-data" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=import'); ?>">
		<?php if (!empty( $this->sidebar)) : ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif; ?>
				<table class="table table-striped" id="userList">
					<tr class="row0">
						<td>
							<input type="file" 	 name="toupload" 	value="" />
							<input type="hidden" name="option" 		value="com_mica" />
							<input type="hidden" name="task" 		value="import.upload" />
							<input type="submit" name="Submit" id="submit" />
							<?php echo JHtml::_('form.token'); ?>
						</td>
					</tr>
				</table>
			</div>
	</form>

<?php } else {?>

	<form name="adminForm" id="adminForm" method="post">
		<?php if (!empty( $this->sidebar)) : ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif; ?>
				<table class="table table-striped" id="userList">
					<tr class="row0">
						<td>
							<div id="preloader" style="display:none;">
								<div id='facebook' >
									<div id='block_1' class='facebook_block'></div>
									<div id='block_2' class='facebook_block'></div>
									<div id='block_3' class='facebook_block'></div>
									<div id='block_4' class='facebook_block'></div>
									<div id='block_5' class='facebook_block'></div>
									<div id='block_6' class='facebook_block'></div>
								</div>
							</div>
							<div id="op"></div>
							<div id="SheetData"></div>
						</td>
					</tr>
				</table>
			</div>
	</form>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#op").html(jQuery("#preloader").html());
			jQuery.ajax({
				url  : 'index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get("filename");?>&task=import.selectSheet&view=import',
				type : 'GET'
			}).done(function(dataResp) {
				
				jQuery("#op").html();
				jQuery("#op").html(dataResp);
			});
		});

		jQuery(document).on('click', '#step2', function(event) {
			event.preventDefault();

			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.insertToDatabasePreview&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val(),
				type : 'GET'
			}).done(function(dataResp) {
				jQuery("#SheetData").html();
				jQuery("#SheetData").html(dataResp);
			});
		});

		jQuery(document).on('click', '#step3', function(event) {
			event.preventDefault();

			jQuery("#preview").html(jQuery("#preloader").html());
			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.insertToDatabase&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val()+"&table="+jQuery("#adminForm input[name='table']:checked").val()+"",
				type : 'GET'
			}).done(function(dataResp) {
				jQuery("#SheetData").html();
				jQuery("#SheetData").html(dataResp);
			});
		});
	</script>

<?php
}
