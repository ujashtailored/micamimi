<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app    = JFactory::getApplication(); ?>

<script type='text/JavaScript'>
	jQuery(document).ready(function($) {
		jQuery(document).on('change', '.toggle_variables', function(event) {
			event.preventDefault();
			var name = jQuery(this).attr('id').split("_section")[0];
			if(this.checked) {
				jQuery(".attr_"+name).prop("checked", true);
			}else{
				jQuery(".attr_"+name).prop("checked", false);
			}
		});
		

		// $('#state').change(function () {
		// 	    $("#province").attr("disabled", $("#state").val() == "countab");
		// 		});
		// 			var $state = $('#state'), $province = $('#province'),$province = $('#province1'),$province = $('#province2');
		// 			$state.change(function () {
		// 			if ($state.val() == 'countab') {
		// 			$province.removeAttr('disabled');
		// 			} 
		// 			if ($state.val() == 'countabc') {
		// 			$province.removeAttr('disabled');
		// 			} 
		// 			}).trigger('change'); 
		});
function callme()
{

	return true;
}

</script>

<form  name="adminForm" id="adminForm" method="post" onsubmit="return callme();" action ="<?php echo JRoute::_('index.php?option=com_mica&view=variablegrouping'); ?>">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>

		

		<?php $i = 0;
		$avoid_variables  = array("OGR_FID", "india_state", "layer", "id","name", "StateName", "LocationID", "DistrictName", "india_information", "distshp", "distcensus", "first_name", "first_iso");
		$avoid_variables2 = array("state", "name_2", "censucode", "first_na_1", "UA_Name", "UA_Name", "india_city", "place_name", "country", "place", "TownName");
		$avoid_variables  = array_merge($avoid_variables, $avoid_variables2);

		foreach ($this->allfields as $field_section_title => $each_section_fields){


			?>
			<table class='table table-striped adminlist <?php echo strtolower($field_section_title)."_section"; ?>' >
				<thead>
					<th colspan="4" class="nowrap center">
						<?php echo $this->groupname." --- ". $field_section_title." --- "." Variables"; ?><BR>
						<span style="color:red;font-size:small">One group cannot have more than 330 variables</span>
						<input type="hidden" name="operator[<?php echo $field_section_title;?>][groupid]" value="<?php echo $this->groupdetails['id']; ?>">
					</th>
				</thead>
				<tbody><tr>
					<?php 
						$grpid =$this->groupdetails['id'];
						$i=0;
					foreach ($each_section_fields as $key => $each_field){
						$i++;
						if($i>332)
							break;
						if (!in_array($each_field, $avoid_variables)) {
						
							?>
							<td valign="top" class="nowrap left">
								<?php echo $each_field; ?>
							</td>
							<td>
							<?php
							

							$filteredItems = reset(array_filter($this->operatorslist, function($elem) use($each_field,$field_section_title,$grpid){
								return $elem->field === $each_field && $elem->table==$field_section_title  && $grpid == $elem->groupid;
								}));

							
							if(!empty($filteredItems))

							{ 	

									
									$currentid = $filteredItems->id;
									$sel       = $filteredItems->operator;
									$slabel    =explode(",",$filteredItems->field_sublabel);
									$label = $filteredItems->field_label;
									if ($label =="")
										$label =$each_field;

							}
							else
							{
								
								$currentid    = 0;
								$sel = "";
								$slabel =array();
								$label = $each_field;
							}

							?>
							<input type="hidden" value="<?php echo $currentid;?>" name="operator<?php echo '['.$field_section_title.'][field]['.$each_field.'][id]';?>">
							<select name="operator<?php echo '['.$field_section_title.'][field]['.$each_field.'][value]';?>" id="operator_<?php echo $i;?>" style="width: 150px">
								<option value="">--select--</option>
								<option value="RowElements" <?php  echo $sel=="RowElements"? "selected": ""?>>Row Elements</option>
								<option value="FixedvalueDistrictlevel" <?php  echo $sel=="FixedvalueDistrictlevel"? "selected": ""?>>Fixed value at District level</option>
								<option value="Total" <?php  echo $sel=="Total"? "selected": ""?>>Total</option>
								<option value="count" <?php  echo $sel=="count"? "selected": ""?>>Count</option>
								<option value="Countsofab" <?php  echo $sel=="Countsofab" ? "selected": ""?>>Count of a,b</option>
								<option value="Countsofabc" <?php  echo $sel=="Countsofabc"? "selected": ""?>>Count of a,b and c</option>
								<option value="Median/Average" <?php  echo $sel=="Median/Average"? "selected": ""?>>Median/ Averagec</option>
					    	</select>
					   	</td>
					    <td>
					    	<input type="text" name="operator<?php echo '['.$field_section_title.'][field]['.$each_field.'][title]';?>"  value="<?php echo $label;?>"  size="30" aria-invalid="false" placeholder="Title">
					    </td>
					    <td>
							<input type="text" name="operator<?php echo '['.$field_section_title.'][field]['.$each_field.'][slabel][]';?>" class="text" value="<?php echo isset($slabel[0])?$slabel[0]:"";?>"    placeholder="Sub-Label" style="width: 60px">
							<input type="text" name="operator<?php echo '['.$field_section_title.'][field]['.$each_field.'][slabel][]';?>" class="text" value="<?php echo isset($slabel[1])?$slabel[1]:"";?>"  placeholder="Sub-Label" style="width: 60px">
							<input type="text" name="operator<?php echo '['.$field_section_title.'][field]['.$each_field.'][slabel][]';?>" class="text" value="<?php echo isset($slabel[2])?$slabel[2]:"";?>"  placeholder="Sub-Label" style="width: 60px">
							

						</td>
								
						<?php 
					
							echo "</tr><tr>";
					}
					$i++;
					}
					?></tr>
				</tbody>
			</table>
		<?php } ?>

		<input type='hidden' name='task' value='' />
		<!-- <input type='hidden' name='id' value='<?php echo $this->groupdetails['id']; ?>' /> -->
 	
		<?php echo JHtml::_('form.token'); ?>
		</div>
</form>


