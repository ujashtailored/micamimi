<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<?php $app = JFactory::getApplication(); ?>

<form  name="adminForm" id="adminForm" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=variablegrouping'); ?>">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>
			<table class="table table-striped" id="userList">
				<thead>
					<tr>
						<th class="nowrap left">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th class="nowrap center"><?php echo JText::_('Group Name') ?></th>
						<th class="nowrap center"><?php echo JText::_('Published / Unpublished Group') ?></th>
						<th class="nowrap center"><?php echo JText::_('Actions') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($this->items as $i => $item){ ?>
						<tr class="row<?php echo $i % 2; ?>">
							<td class="left">
								<?php echo JHtml::_('grid.id', $i, $item->id); ?>
							</td>
							<td class="center">
								<a href="<?php echo JRoute::_('index.php?option=com_mica&view=variablegrouping&task=variablegrouping.add&id='.$item->id); ?>">
									<?php echo $item->group; ?>	
								</a>
							</td>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->publish, $i, 'variablegrouping.', 1, 'cb'); ?>
							</td>
							<td class="center">
								<a href="<?php echo JRoute::_('index.php?option=com_mica&task=variablegrouping.editsummayoperators&id='.$item->id); ?>">
									Edit Summary Operators
								</a>
							</td>

						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
			</table>
			<input type="hidden" name="task" value="saveoperators" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
