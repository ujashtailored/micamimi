<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<form name="adminForm" id="adminForm" method="POST" action ="<?php echo JRoute::_('index.php?option=com_mica&view=configuration');?>" >
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>

		<table class="table table-striped" id="userList">
			<tr class="row0">
				<td> Tomcat Url </td>
				<td>
					<input type="text" name="tomcaturl" value="<?php echo $this->configurationdata['tomcatpath'];?>" style="width:500px;"/>
				</td>
			</tr>
		</table>
		<input type="hidden" name="option" 	id="option"	value="com_mica" />
		<input type="hidden" name="view" 	id="view" 	value="configuration" />
		<input type="hidden" name="task" 	id="task"	value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
