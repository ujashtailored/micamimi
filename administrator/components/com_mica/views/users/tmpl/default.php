<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<?php $app = JFactory::getApplication(); ?>

<form  name="adminForm" id="adminForm" enctype="multipart/form-data" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=users'); ?>">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>
			<table class="table table-striped" id="userList">
				<thead>
					<tr>
						<th class="nowrap left"><?php echo JText::_('Plan') ?></th>
						<th class="nowrap center"><?php echo JText::_('Add/Edit Attribute') ?></th>
						<th class="nowrap center"><?php echo JText::_('Edit Type') ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($this->items as $i => $item){ ?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="left">
							<?php echo $item->title; ?>
						</td>
						<td class="center">
							<a href="<?php echo JRoute::_('index.php?option=com_mica&view=users&task=users.updateAttr&plan='.$item->title.'&planid='.$item->id); ?>">
								<?php echo JText::_('Add/Edit Attribute') ?>
							</a>
						</td>
						<td class="center">
							<a href="<?php echo JRoute::_('index.php?option=com_mica&view=users&task=users.updateType&plan='.$item->title.'&planid='.$item->id); ?>">
								<?php echo JText::_('Add/Edit Type') ?>
							</a>
						</td>
					</tr>
				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
			</table>
			<input type="hidden" name="task" value="" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
