<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for Mica Users.
 *
 * @since  1.6
 */
class MicaViewUsers extends JViewLegacy
{

	/**
	 * The item data.
	 *
	 * @var   object
	 * @since 1.6
	 */
	protected $items;

	/**
	 * The pagination object.
	 *
	 * @var   JPagination
	 * @since 1.6
	 */
	protected $pagination;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->items      = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		MicaHelper::addSubmenu('users');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
		echo JHTML::_('behavior.keepalive');
	}

	public function updateAttr($tpl= null)
	{
		$this->model = $this->getModel();

		$data = $this->get('UpdateAttrData');
		$this->userattribute = $data['userattribute'];
		$this->selecteddata  = $data['selecteddata'];

		$this->db_table      = array("State","District","Town","Urban");
		$this->allattributes = $this->model->getAllAttributes($this->db_table);

		MicaHelper::addSubmenu('users');
			//$this->addToolbar();
			$bar = JToolbar::getInstance('toolbar');
			JToolbarHelper::title(JText::_('Plan Manager: Add/Edit Attribute '), 'users user');
			JToolbarHelper::cancel('users.cancel_updateattr');
			JToolbarHelper::save('users.saveAttr');
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}


	public function updateType($tpl= null)
	{
		$this->model     = $this->getModel();
		$this->typesData = $this->get('typesData');

		MicaHelper::addSubmenu('users');
			//$this->addToolbar();
			$bar = JToolbar::getInstance('toolbar');
			JToolbarHelper::title(JText::_('Plan Manager: Add/Edit Type '), 'users user');
			JToolbarHelper::cancel('users.cancel_updatetype');
			JToolbarHelper::save('users.save');
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		// Get the toolbar object instance
		$bar = JToolbar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('Plan Manager'), 'users user');
	}

}
