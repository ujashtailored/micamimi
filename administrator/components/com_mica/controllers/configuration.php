<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Configuration controller class.
 *
 * @since  1.6
 */
class MicaControllerConfiguration extends JControllerAdmin
{

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Configuration', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	function save(){
		// Get the model.
		$model = $this->getModel();
		$msg   = $model->save();

		$this->setMessage($msg);
		$this->setRedirect('index.php?option=com_mica&view=configuration');
	}

	public function cancel(){
		$this->setRedirect("index.php?option=com_mica&view=configuration", JText::_('Operation Cancelled.'), 'warning');
		return;
	}

}
