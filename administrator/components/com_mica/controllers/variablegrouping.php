<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Variable Grouping controller class.
 *
 * @since  1.6
 */
class MicaControllerVariablegrouping extends JControllerLegacy
{

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Variablegrouping', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * A task to render a form to add new a group.
	 */
	public function add(){
		$view = $this->getView('Variablegrouping', 'html', 'MicaView');
		$view->setModel($this->getModel(), true);
		$view->setLayout('add');
		return $view->add();
	}



	/**
	 * Function to save posted data for group.
	 */
	public function save(){

		$groupname      = $this->input->post->get('groupname', '', 'raw');
		// $deleteimage = $this->input->post->get('deleteimage', 0, 'int');
		 $id          = $this->input->post->get('id');
		//$groupid        = $this->input->post->get('groupid', '', 'raw');
		$tabel          = $this->input->post->get('tabel', '', 'raw');
		$field          = $this->input->post->get('field', '', 'raw');
		$operator       = $this->input->post->get('operator', '', 'raw');

		$checkedattr = array("State" => array(), "District" => array(), "Urban" => array(), "Villages" => array() );
		foreach ($checkedattr as $field_type => $each_field) {
			$checkedattr[$field_type] = $this->input->post->get("attr_".$field_type, array(), 'array');
			//$each_field = $this->input->post->get("attr_".$field_type, array(), 'array');
		}

		$insertedicon = $this->uploadIcon();
		$db           = JFactory::getDBO();

		if ($id > 0 ) {

			if( $insertedicon != ""){
				$icon_update = ", ".$db->quoteName('icon')." = ".$db->quote($insertedicon);
			}

			if( $deleteimage == 1){
				$icon_update = ", ".$db->quoteName('icon')." = ".$db->quote('');
			}
				 $query = "UPDATE  ".$db->quoteName('#__mica_group')."
				SET ".$db->quoteName('group')." = ".$db->quote($groupname).$icon_update."
				WHERE ".$db->quoteName('id')." = ".$db->quote($id);

			$db->setQuery($query);
			$db->execute();
		}else{
			$query = "INSERT INTO  ".$db->quoteName('#__mica_group')."
				(".$db->quoteName('group').", ".$db->quoteName('icon').") VALUES (".$db->quote($groupname).", ".$db->quote($insertedicon).")";
			// $query = "INSERT INTO  ".$db->quoteName('#__mica_group_field_summary_map')."
			// 	(".$db->quoteName($groupname).", ".$db->quoteName($tabel).", ".$db->quoteName($field)." ".$db->quoteName($operator).") VALUES (".$db->quote($groupname).", ".$db->quote($tabel).", ".$db->quote($field).", ".$db->quote($operator).")";
			$db->setQuery($query);
			$db->execute();
			$id = $db->insertid();
		}

		if ($id > 0) {
			 $query = "DELETE FROM  ".$db->quoteName('#__mica_group_field')." WHERE  ".$db->quoteName('groupid')." = ".$db->quote($id);

			$db->setQuery($query);
			$db->execute();
		}

		$VALUES = array();
		$field_namess =array();
		if (count($checkedattr) > 0) {
			foreach($checkedattr as $field_type => $fields){
				if (count($fields) > 0) {
					foreach($fields as $key => $each_field){
						 //delete previous entry of the same field for selected table
						 $query = "DELETE FROM  ".$db->quoteName('#__mica_group_field')." WHERE  ".$db->quoteName('field')." = ".$db->quote($each_field)." and ".$db->quoteName('table')." = ".$db->quote($field_type)." ";
						$db->setQuery($query);
						$db->execute();
						$field_namess[]=$each_field;

						$VALUES[] = " (".$db->quote($id).", ".$db->quote($each_field).", ".$db->quote($field_type).") ";
					}
				}
			}
		}
		if (count($VALUES) > 0) {
			$query = "INSERT INTO  ".$db->quoteName('#__mica_group_field')."
				(".$db->quoteName('groupid').", ".$db->quoteName('field').", ".$db->quoteName('table').")
				VALUES ".implode(", ", $VALUES);
			$db->setQuery($query);
			$db->execute();
		}
		//update summary_map table to change the group id when it is already selected with another groupid
		if(!empty($field_namess))
		{

			  $query  = "UPDATE ".$db->quoteName('#__mica_group_field_summary_map')."
						SET ".$db->quoteName('groupid')."=".$db->quote($id)."
						WHERE `field` in ('".implode("','",$field_namess)."')";

			$db->setQuery($query);
			$db->execute();

			 $query  = "Delete from ".$db->quoteName('#__mica_group_field_summary_map')."
						where ".$db->quoteName('groupid')."=".$db->quote($id)."
						and `field` not in ('".implode("','",$field_namess)."')";
						echo $query;

			$db->setQuery($query);
			$db->execute();

		}



		$msg = JText::_( 'GROUP_UPDATED' );

		if ($id > 0) {
			if($insertedicon === 0 && $deleteimage != 1) {
				$msg = JText::_( 'GROUP_INSERTED_WITH_IMAGE_ERROR' );
			}
		}else{
			if($insertedicon === 0 ){
				$msg = JText::_( 'GROUP_INSERTED_WITH_IMAGE_ERROR' );
			}
		}

		if($deleteimage == 1){
			$oldimage = $this->input->post->get('oldimage', '', 'raw');
			unlink(JPATH_SITE."/components/com_mica/images/".$oldimage);
		}

		$this->setRedirect("index.php?option=com_mica&view=variablegrouping", $msg, 'success');
	}

	public function saveoperators()
	{
		$operator = $this->input->post->get('operator', '', 'raw');
		$db       = JFactory::getDBO();

		$group_id = $operator['Villages']['groupid'];

		if(!empty($operator))
		{
			foreach ($operator as $key => $value) {
				if($value!="")
				{
					$table   = $key;
					$groupid = $value['groupid'];
					foreach ($value['field'] as $fkey => $fvalue) {
						$field            = $fkey;
						$operator         = $fvalue['value'];
						$field_label      = $fvalue['title'];
						$fvalue['slabel'] = array_filter($fvalue['slabel'], function($value) { return $value !== ''; });
						$sublabel         = implode(",",$fvalue['slabel']);
						$id               = $fvalue['id'];

						if($id > 0)
					 	{
					 		$query  = "UPDATE ".$db->quoteName('#__mica_group_field_summary_map')."
					 	 		SET ".$db->quoteName('operator')."=".$db->quote($operator).",
						 	 ".$db->quoteName('field_label')."=".$db->quote($field_label).",
						 	 ".$db->quoteName('field_sublabel')."=".$db->quote($sublabel)."
						 	 WHERE ".$db->quoteName('id')."= ".$id;

						 	$db->setQuery($query);
							$db->execute();
						}
						else
						{
							echo $query  = "INSERT INTO  ".$db->quoteName('#__mica_group_field_summary_map')."
						 		(".$db->quoteName('groupid').", ".$db->quoteName('field').", ".$db->quoteName('table').", ".$db->quoteName('operator').",".$db->qn('field_label').",".$db->qn('field_sublabel').") VALUES (".$db->quote($groupid).", ".$db->quote($field).", ".$db->quote($table).", ".$db->quote($operator).", ".$db->quote($field_label).", ".$db->quote($sublabel).")";
						 	$db->setQuery($query);
							$db->execute();
						}
					}
				}
			}

		}

		$msg = JText::_( 'GROUP_UPDATED' );

		$this->setRedirect("index.php?option=com_mica&task=variablegrouping.editsummayoperators&id=".$group_id, $msg, 'success');
	}

	/**
	 * Uploads Icon for groups.
	 */
	private function uploadIcon(){
		$file = $this->input->files->get('iconimage', array(), 'array');
		if (count($file) > 0) {
			list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

			$validimage = array("image/jpeg","image/png","image/pjpeg","image/gif","image/jpg");
			if($width > 33 && $height >33 && !in_array($validimage, $file['type'])){
				return false;
			}

			if(move_uploaded_file($file['tmp_name'], JPATH_SITE."/components/com_mica/images/".$file['name'])){
				return $file['name'];
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * A redirection task called to calcel the group form.
	 */
	public function cancel_add(){
		$this->setRedirect("index.php?option=com_mica&view=variablegrouping", JText::_('Operation Cancelled'), 'warning');
	}

	/**
	 * Publish Groups.
	 */
	public function publish(){
		$cid = $this->input->post->get('cid', array(), 'array');
		if (count($cid) > 0) {
			$cid = implode(",", $cid);

			$db = JFactory::getDBO();
			$STATE_SQL = " UPDATE ".$db->quoteName('#__mica_group')."
				SET ".$db->quoteName('publish')." = ".$db->quote(1)."
				WHERE ".$db->quoteName('id')." IN ('".$cid."')" ;
			$db->setQuery($STATE_SQL);
			$db->execute();

			$this->setRedirect("index.php?option=com_mica&view=variablegrouping", JText::_('GROUP_PUBLISHED'), 'success');
		}else{
			$this->setRedirect("index.php?option=com_mica&view=variablegrouping", JText::_('Kindly select group first.'), 'error');
		}
	}

	/**
	 * Unpublish Groups.
	 */
	public function unpublish(){
		$cid = $this->input->post->get('cid', array(), 'array');
		if (count($cid) > 0) {
			$cid = implode(",", $cid);

			$db = JFactory::getDBO();
			$STATE_SQL = " UPDATE ".$db->quoteName('#__mica_group')."
				SET ".$db->quoteName('publish')." = ".$db->quote(0)."
				WHERE ".$db->quoteName('id')." IN ('".$cid."')" ;
			$db->setQuery($STATE_SQL);
			$db->execute();

			$this->setRedirect("index.php?option=com_mica&view=variablegrouping", JText::_('GROUP_UNPUBLISHED'), 'success');
		}else{
			$this->setRedirect("index.php?option=com_mica&view=variablegrouping", JText::_('Kindly select group first.'), 'error');
		}
	}

	/**
	*
	**/
	public function editsummayoperators()
	{

		$view = $this->getView('Variablegrouping', 'html', 'MicaView');
		$view->setModel($this->getModel(), true);
		$view->setLayout('summaryoperators');
		return $view->editsummayoperators();
	}

	/**
	 * A task to be called to delete groups.
	 */
	public function delete(){
		$cid = $this->input->post->get('cid', array(), 'array');
		if (count($cid) > 0) {
			$cid = implode(",", $cid);

			$db = JFactory::getDBO();
			$DELETE_SQL = " DELETE FROM ".$db->quoteName('#__mica_group')." WHERE ".$db->quoteName('id')." IN ('".$cid."')" ;
			$db->setQuery($DELETE_SQL);
			$db->execute();

			$DELETE_SQL = " DELETE FROM ".$db->quoteName('#__mica_group_field')." WHERE ".$db->quoteName('groupid')." IN ('".$cid."')" ;
			$db->setQuery($DELETE_SQL);
			$db->execute();

			$msg = (count($cid) > 1) ? JText::_('GROUPS_DELETED') : JText::_('GROUPS_DELETED');
			$this->setRedirect("index.php?option=com_mica&view=variablegrouping", $msg, 'success');
		}else{
			$this->setRedirect("index.php?option=com_mica&view=variablegrouping", JText::_('Kindly select group first.'), 'error');
		}
	}
}
