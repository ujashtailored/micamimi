<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components/com_mica/css/dashboard.css', 'text/css');
?>

<div class="dashboard">
	<div class="leftpanel">
		<div class="header-leftpanel"><?php echo JTEXT::_("MY_WORKSPACE");?></div>
		<div class="data-leftpanel">
			<ul>
				<li>
					<span class='workspacelink' style='width:70%'>
						<b>Name</b>
					</span>
					<span class='workspacedate' style='width:30%'>
						<b>Last Modify</b>
					</span>
				</li>
				<?php
				foreach ($this->Workspace as $eachworkspace){
					$modify_date = " ---";
					if($eachworkspace['modify_date']!="0000-00-00 00:00:00"){
						$modify_date = JHTML::_('date', $eachworkspace['modify_date'], 'd-M-y');
					}?>
					<li >
						<span class='workspacelink'>
							<a href='<?php echo JRoute::_('index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid='.$eachworkspace['id'] ) ?>' >
								<?php echo $eachworkspace['name']; ?>
							</a>
						</span>
						<span class='workspacedate' ><?php echo $modify_date; ?></span>
					</li>
				<?php } ?>
				<li class='exploredatalink' style="height:25px;">
					<a href="<?php echo JRoute::_( 'index.php?option=com_mica&view=micafront&Itemid=188' ); ?>">
						<?php echo JTEXT::_("Explore_Data"); ?>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="rightpanel">
		<div class="header-rightpanel">
			<?php echo JTEXT::_("MY_ACCOUNT");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<li>
					<span class="data-label"><?php echo JTEXT::_("USERNAME");?></span>
					<span class="data-text">
						<?php echo $this->userinfo->username; ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("EMAIL");?></span>
					<span class="data-text">
						<?php echo $this->userinfo->email; ?>
					</span>
				</li>
				<?php /*<li>
					<span class="data-label">< ?php echo JTEXT::_("CHANGE_PASSWORD");? ></span>
					<span class="data-text">
						< ?php echo JTEXT::_("CHANGE_PASSWORD");? >
					</span>
				</li> */ ?>
				<li>
					<span class="data-label"><?php echo JTEXT::_("DASHBOARD_COMPANY");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->institute_name; } else { echo $this->getParentinfo->institute_name;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("ADDRESS");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->address; } else { echo $this->getParentinfo->address;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("CONTEECT_NUMBER");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->contactnumber; } else { echo $this->getParentinfo->contactnumber;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("MOBILE");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->mobile; } else { echo $this->getParentinfo->mobile;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("PAYMENT_TYPE");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ){
							if($this->userinfo->paymentoption==1) { $ptype="Cheque ".$this->userinfo->bankname; } else { $ptype="DD ".$this->userinfo->bankname; } echo $ptype;
						}else{
							if($this->getParentinfo->paymentoption==1) { $ptype="Cheque ".$this->getParentinfo->bankname; } else { $ptype="DD ".$this->getParentinfo->bankname; } echo $ptype;
						} ?>
					</span>
				</li>
			</ul>
			<div class="header-rightpanel">
				<?php echo JTEXT::_("MY_SUBSCRIPTION"); ?>
			</div>
			<div class="data-rightpanel">
				<ul>
					<li>
						<span class="data-label"><?php echo JTEXT::_("PLAN");?></span>
						<?php if( $this->getParentinfo == null ){ ?>
							<span class="data-text"><?php echo $this->userinfo->plandetails->title; ?></span>
						<?php } else { ?>
							<span class="data-text"><?php print_r($this->getParentsubscriptioninfo->title); ?></span>
						<?php } ?>
					 </li>
					<li>
						<span class="data-label"><?php echo JTEXT::_("PURCHASE_DATE");?></span>
						<?php if( $this->getParentinfo == null ){ ?>
							<span class="data-text"><?php echo JHTML::_('date', $this->userinfo->plandetails->plan_subscription_from_date, 'd-M-y'); ?></span>
						<?php } else { ?>
							<span class="data-text"><?php echo JHTML::_('date', $this->getParentsubscriptioninfo->plan_subscription_from_date, 'd-M-y'); ?></span>
						<?php } ?>
						<span class="data-text"></span>
					</li>
					<li>
						<span class="data-label"><?php echo JTEXT::_("LAST_PAYMENT");?></span>
						<span class="data-text"><?php echo JHTML::_('date',$this->userinfo->date, 'd-M-y'); ?></span>
					</li>
					<li>
						<span class="data-label"><?php echo JTEXT::_("EXPIRY");?></span>
						<?php if( $this->getParentinfo == null ){ ?>
							<!-- <span class="data-text"><?php //echo JworkspacedateHTML::_('date',$this->userinfo->plandetails->plan_subscription_to_date, 'd-M-y'); ?></span> -->
							<span class="data-text"><?php echo JHTML::_('date',$this->userinfo->plandetails->plan_subscription_to_date, 'd-M-y'); ?></span>
						<?php } else { ?>
							<span class="data-text"><?php echo JHTML::_('date',$this->getParentsubscriptioninfo->plan_subscription_to_date, 'd-M-y'); ?></span>
						<?php } ?>
					</li>
					<?php //$params= unserialize(base64_decode($this->userinfo->plandetails->par));
					if( $this->getParentinfo == null ) {
						$totalAccount   = $this->getCreatedAccountFromAEC;
						$createdAccount = $this->getCreatedAccount;
						//echo $totalAccount."/".$createdAccount; ?>
						<li>
							<span class="data-label"><?php echo JTEXT::_("TOTAL_USERS_ADDED");?></span>
							<span class="data-text">
								<a href="index.php?option=com_adduserfrontend&view=childusers">
									<?php echo "(".$createdAccount."/".$totalAccount.")"; ?>
								</a>
								<?php //if($this->getCreatedAccount <= (($params['userlimit']-1))) {
								if($createdAccount < $totalAccount && empty($this->allowAdd)) {
									echo "&nbsp;<a class='createuserlink' href='index.php?option=com_adduserfrontend&view=adduserfrontend'>Create New User</a>";
								} ?>
							</span>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>
