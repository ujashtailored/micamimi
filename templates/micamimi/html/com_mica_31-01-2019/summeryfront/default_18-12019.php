<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  com_mica
	 *
	 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined('_JEXEC') or die;

	$db      = JFactory::getDbo();
	$session = JFactory::getSession();
	$doc     = JFactory::getDocument();
	$app     = JFactory::getApplication('site');


	$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery-ui.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');
		$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/slick/slick.css', 'text/css');

	$itemid        = 188;
	$type1         = "";
	$m_type_rating = $app->input->get("m_type_rating", '', 'raw');
?>
	<script src="<?php echo JURI::base()?>components/com_mica/js/Chart.bundle.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/utils.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>

	<!-- start bubble chart js-->
	<script src="<?php echo JURI::base()?>components/com_mica/js/d3.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/bubble_chart.js"></script>
	<!-- end bubble chart js-->

	<script type="text/javascript">
		var JQuery = jQuery.noConflict();
		var $      = jQuery.noConflict();
	</script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-migrate-1.2.1.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jspdf.min.js"></script>
	<script type="text/javascript">
		var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
		var districtalert     = '<?php echo JText::_("Please Select District first!!!")?>';
		var preselected       = '<?php echo $app->input->get("summarydistrict", "", "raw"); ?>';
		var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
		var preselectedm_type = '<?php echo $app->input->get("m_type", "", "raw"); ?>';
		var chart1;
		var keys              = [];
		var trashedLabels     = [];
		var trashedData       = [];
		var datasetValues     = [];
		var flag              = false;
	</script>
	<script src="templates/micamimi/js/jquery.scrollbar.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.scrollbar-inner').scrollbar();
		});
	</script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/summeryfield.js"></script>

	<style type="text/css">

	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	.btn-disabled,
	.btn-disabled[disabled] {
		cursor: default !important;
		pointer-events: none;
	}
	body {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
}
</style>

<div class="row-fluid top-row">
	<div class="col-md-4">
		<h1 class="title-explore"><?php echo JText::_('VILLAGE SUMMARY');?></h1>
	</div>
	<div class="col-md-8 text-right">
		<div class="sorter-tab">
			<ul>
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="graphdata" class="actionbtn btn-disabled" disabled="disabled" onclick="loadcharts();"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li>
			</ul>
		</div>
		<div class="worspacce-edit">
			My workspace <a class="workspacce-edit" href="javascript:void(0)" onclick="document.getElementById('workspacceedit').style.display='block';document.getElementById('fade').style.display='block'">
				<i class="fas fa-pencil-alt"></i>
			</a>
			<h2><div id="activeworkspacename">
				</div>
			</h2>
		</div>
	</div>
</div>
<div id="workspacceedit" style="display:none;" class="white_content2 large-popup">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('workspacceedit').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>
	<div class="poptitle">
		<?php echo JTEXT::_('MY_WORKSPACE');?>
	</div>

	<div class="scroll_div">
		<div class="col-md-8">
			<div class="blockcontent">
				<ul class="allworkspace">

					<?php
						$this->user = JFactory::getUser();
						$id = $this->user->id;

						if($id == 0){
							return -1;
						}

						$db   = JFactory::getDBO();
						$query = "SELECT name, id, is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($this->user->id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1) . "AND" . $db->quoteName('data') . " REGEXP '.*\"dataof\";s:[0-9]+:\"villageSummary\".*'";

								$db->setQuery($query);
								$result = $db->loadAssocList();
								$app               = JFactory::getApplication('site');
								$session           = JFactory::getSession();
								$str               = "";
								$activeworkspace   = $session->get('activeworkspace');
								$selected          = "";
								$activeprofilename = $eachresult['name'];
								$i                 = 1;

								foreach($result as $eachresult){
									if(($i%2)==0){
										$clear="clear";
									}else{
										$clear="";
									}

									if($activeworkspace == $eachresult['id']){

										$selected          = "selected";
										$edit              = "Edit/Delete";
										$activeprofilename = $eachresult['name'];

										echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('Active Workspace :<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
										$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
										echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
										$active_text = "<span style='font-weight:normal;'>(Active)<span>";
										$active_text = "";
									}
									else
									{
										$selected    = "";
										$edit        = "Select";
										$onclick     = "changeWorkspace(".$eachresult['id'].")";
										$active_text = "";
									}

									$str .= '
									<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
									<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
									<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'

							}
							echo $str;
							echo '<div id="lightn" class="white_contentn" style="display: none;">
							<div class="divclose">
							<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
							<img src="media/system/images/closebox.jpeg" alt="X" />
							</a>
							</div>
							<div align="left">
							<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
							<div class="frontbutton readon">
							<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
							</div>
							<div class="frontbutton">
							<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
							</div>
							</div>';

							?>
						</ul>
					</div>
				</div>

				<div class="col-md-4">
					<div class="contentblock endcontent">
						<div class="contenthead">New Workspace</div>
						<div class="blockcontent newworkspace">
							<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
							<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="explore-data">
			<form name="adminForm" id="micaform" action="" method="POST">
				<div class="row">
					<div class="col-md-4">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#state" aria-controls="state" role="tab" data-toggle="tab" id="state_tab">
									<em class="icon-state"></em>State
								</a>
							</li>
							<li role="presentation">
								<a href="#district" aria-controls="district" role="tab" data-toggle="tab" id="district_tab">
									<em class="icon-district"></em>District
								</a>
							</li>
							<li role="presentation">
				            	<a href="#sub_district" aria-controls="sub_district" role="tab" data-toggle="tab" id="sub_district_tab">
				            		<em class="icon-district"></em>Sub District
				            	</a>
				            </li>
							<li role="presentation">
								<a href="#variables" aria-controls="variables" role="tab" data-toggle="tab" id="variables_tab">
									<em class="icon-variables"></em>Variables
								</a>
							</li>
						</ul>

						<!-- START Tab panes -->
						<div class="tab-content leftcontainer" id="leftcontainer">
							<!-- START State -->
							<div role="tabpanel" class="tab-pane active" id="state">
								<div class="top-sorter-div">
									<div class="row">
										<div class="col-md-5 col-xs-5">
											<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
												<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
											</div>
											|
											<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
											<label for="state_allcheck"></label>
											|
											<a href="javascript:void(0);" onclick="sortListDir('id01')">
												<i class="fa fa-sort"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="scrollbar-inner">
									<ul class="list1 statelist" id="id01">
										<?php
										$states = explode(",", $app->input->get("summarystate", '', 'raw'));
										for($i = 0; $i < count($this->state_items); $i++){
											$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
											<li>
												<input type="checkbox" class="state_checkbox" name="summarystate[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="summarystate<?php echo $i;?>">
												<label for="summarystate<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
											</li>
										<?php  } ?>
									</ul>
								</div>
							</div>
							<!-- END State -->

							<!-- START District -->
							<div role="tabpanel" class="tab-pane" id="district">
								<div class="loader-div" id="loader" style="display: none">
								      <img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
								</div>
								<div id="districtspan">
									<div class="top-sorter-div">
										<div class="row">
											<div class="col-md-5 col-xs-5">
												<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
											</div>
											<div class="col-md-7 col-xs-7 text-right">
												<div class="searchbox">
													<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
												</div>
												|
												<input type="checkbox" name="district_checkall" class="allcheck" id="district_allcheck">
												<label for="district_allcheck"></label>
												|
												<a href="javascript:void(0);" onclick="district_sortListDir()">
													<i class="fa fa-sort"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="scrollbar-inner">
												<ul class="list1 districtlist" id="district_list" onchange="getSubDistrict(this.value)">
												</ul>
											</div>
										</div>
										<div class="col-md-2 padding-none">
											<div class="dist-icon-list">
												<div id="statecode" >
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END District -->

							<!-- START Sub District -->
				        	<div role="tabpanel" class="tab-pane" id="sub_district">
				        		<div id="subdistrictspan">
					        		<div class="top-sorter-div">
				                		<div class="row">
				                			<div class="col-md-5 col-xs-5">
				                				<h3><?php echo JText::_('SUB_DISCTRICT_LABEL'); ?></h3>
				                			</div>
				                			<div class="col-md-7 col-xs-7 text-right">
				                				<div class="searchbox">
				                					<input type="search" class="searchtop" id="sub_district_searchtop" placeholder="Search">
				                				</div>
				                				|
												<input type="checkbox" name="sub_district_checkall" class="allcheck" id="sub_district_allcheck">
												<label for="sub_district_allcheck"></label>
				                				|
				                				<a href="javascript:void(0);" onclick="sub_district_sortListDir()">
				                					<i class="fa fa-sort"></i>
				                				</a>
				                			</div>
				                		</div>
				                	</div>
				                	<div class="row">
			                			<div class="col-md-10">
			                				<div class="scrollbar-inner">
					                            <ul class="list1 subdistrictlist">
												</ul>
											</div>
			                			</div>
			                			<div class="col-md-2 padding-none">
			                				<div class="dist-icon-list">
			                					<div id="statecode">
			                					</div>
			                				</div>
			                			</div>
			                		</div>
				                </div>
				        	</div>
				        	<!-- END Sub District -->

							<!-- START Variable -->
							<div role="tabpanel" class="tab-pane" id="variables">
								<div id="variablespan">
									<div class="top-sorter-div">
										<div class="row">
											<div class="col-md-5 col-xs-5">
												<h3><?php echo JText::_('Select Variable'); ?></h3>
											</div>
											<div class="col-md-7 col-xs-7 text-right">
												<div class="searchbox">
													<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
												</div>
												|
												<input type="checkbox" name="variable_checkall" class="allcheck" id="variable_allcheck">
												<label for="variable_allcheck"></label>
												|
												<a href="#sort" onclick="variable_sortListDir()">
													<i class="fa fa-sort"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="scrollbar-inner">
												<div id="variables"></div>
												<div id="statetotal"></div>
											</div>
										</div>
										<div class="col-md-2">
											<div id="variableshortcode"></div>
										</div>
									</div>
								</div>
							</div>
							<!-- END Variable -->
						</div>
						<!-- END Tab panes -->
					</div>

					<div class="col-md-8">
						<div class="full-data-view">
							<div id="result_table" style="display:none">
								<div class="text contenttoggle1" id="textscroll">
									<div class="button-area text-right">
										<a href="#alldata" onclick="downloadAction();"><i class="fas fa-download"></i><?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?></a>
									</div>

									<div class="collpsable1">
									</div>

									<div class="activecontent right-scroll-div tableview">
										<div class="alldata contenttoggle2" id="jsscrollss alldata" style="width:940px;height:524px;max-height:524px;">
										</div>
										<?php echo "<br /><br />"; ?>
									</div>
								</div>
							</div>

							<!-- Graph START -->
							<div id="graph" style="display:none;">
								<div class="graphs contenttoggle1">
									<div class="row">
										<div class="col-md-4">
											<div class="chart_type">
												<label>Chart Type:</label>
												<select name="charttype" class="inputbox" id="chartype">
													<option value="bar" selected>Bar Chart</option>
													<option value="line">line Chart</option>
													<option value="radar">Radar Chart</option>
													<option value="bubble">Bubble Chart</option>
												</select>

												<label>Show Legend</label>
												<input type="checkbox" id="enablelegend" name="enablelegend"  value="1" />
											</div>
										</div>

										<div class="col-md-5">
											<div class="row">
												<div class="col-md-12">
													<?php
													$variableoptions="";
													$variableopopup = '<div id="light1007" class="white_content2 tab-pane" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1007\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select District</div>';
													$distopopup     = '<div id="light1008" class="white_content2 tab-pane" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1008\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select  Variable</div>';
													$distoptions    ='<div id="variabelist">
													</div>';
													?>
													<?php
													echo $distopopup.$distoptions."</div>";
													?>

													<a href="javascript:void(0);" id="graphdistrict" onClick="document.getElementById('light1008').style.display='block';document.getElementById('fade').style.display='block'"><b>Select  Variable</b></a>

													&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;

													<a href="javascript:void(0);" id="graphvariable" onClick="document.getElementById('light1007').style.display='block';document.getElementById('fade').style.display='block'"><b>Select  District </b></a>

													<?php
													echo $variableopopup.'<div id="cblist"></div></div>';
													?>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="button-area-2">
												<!-- <div id="showchart"><input type="button"  name="showchart"  class="button" value="Show Chart" /></div> -->
												<div id="exportchartbutton" class="exportbtn">
													<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
												</div>
											</div>
										</div>
									</div>
									 <canvas id="myChart" style="width:100%; height:520px; background-color:#FFFFFF;"></canvas>
									 <div id="venn" style="width:100%; height:520px; background-color:#FFFFFF;display:none;" ><svg></svg></div>
									 <div id="venn_label"></div>

									<!-- end of amcharts script -->
								</div>
							</div>
							<!-- graph end -->

							<!--Start Speed -->
							<div id="potentiometer" style="display:none;">
								<div class="speedometer contenttoggle1" >
									<div class="filter-section">
										<div style="display:none;" id="speedfiltershow">
											<div class="rightgrptext frontbutton" >
												<input style="margin-left:3px;" type="button" class="frontbutton" value="Edit Filter" name="editfilter"/>
											</div>
										</div>
										<div class="sfilter">
											<div class="top-filter-sec">
												<div class="fl_title">
													<b><?php echo JText::_('FILTER_BY');?></b>
												</div>
												<div class="fl_type radio-list">
													<ul>
														<li>
															<input id="filter1" type="radio" name="filter" value="0" class="filterspeed"/>
															<label for="filter1"><?php echo JText::_('FILTER_BY_LAYER');?></label>
														</li>
														<li>
															<input id="filter2" type="radio" name="filter" value="1" class="filterspeed"/>
															<label for="filter2"><?php echo JText::_('FILTER_BY_VARIABLE');?></label>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="sfilter">
											<div>
												<div class="lft speed" style="display:none;">
													<ul class="filters-list">
														<li>

															<select name="speed_variable" id="speed_variable" class="inputbox" multiple>

																<?php
																$attr = $this->themeticattribute;
																$eachattr = explode(",",$attributes);
																foreach($eachattr as $eachattrs){
																	echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
																} ?>
															</select>
														</li>
														<li>
															<select name="speed_region" id="speed_region" class="inputbox"  multiple >
																<?php
																foreach($this->customdata as $eachdata){
																	echo '<option value='.str_replace(" ","%20",$eachdata->name).'>'.$eachdata->name.'</option>';
																} ?>
															</select>
														</li>
														<li>
															<div class="frontbutton ">
																<input type="button" name="showspeed" id="showspeed" class="" value="Create">
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div id="spedometer_region"></div>
								</div>
							</div>
							<!--End Speed -->

							<div id="quartiles" class="matrix contenttoggle1" style="display:none;overflow:scroll;width:938px;height:400px;">
								<div id="fullscreentable"></div>
							</div>
							<!--START default3 -->
							<div id="default3" class="default3 contenttoggle1">
								<p>Step 1 : Select State</p>
								<p>Step 2 : Select District of selected State</p>
								<p>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</p>
								<p>Step 4 : Select the Variables you want to view. (Multiple selection allowed)</p>
								<p>Step 5 : Select MPI (Multiple selection allowed),</p>
								<p>Step 6 : Select sector wise composite score. (Multiple selection allowed)
								Note : Mark ‘All’ in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</p>
								<!-- <div align="left">
									 <a href="javascript:void(0);" id="submitta" style="display:none;" class="actionbtn btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a>
									</div> -->
								</div>
								<!--End default3 -->

								<!--start default -->
								<div id="default" class="white_content2 alert-variable">
									<div class="popup_alert">
										<div class="default contenttoggle1" >
											<div class="divclose">
												<a href="javascript:void(0);" onclick="document.getElementById('default').style.display='none';document.getElementById('fade').style.display='none';">
													<img src="media/system/images/closebox.jpeg" alt="X">
												</a>
											</div>
											<div class="box-white">
												<p>Please select variables from each of the following dimensions to view a report. You can select from left panel or by clicking the links above.
												</div>
												<ul class="list1">
													<li id="statetext"><i class="far fa-check-square"></i>state</li>
													<li id="districttext"><i class="far fa-check-square"></i>District</li>
													<li id="variabletext"><i class="far fa-check-square"></i>Variabel</li>
												</ul>


												<div align="right">
													<a href="javascript:void(0);" id="apply_chnages" class="actionbtn btn-disabled btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a>
												</div>
												<!-- <a href="/" id="submit" class="actionbtn" onclick="getDataNew();" disabled="disabled"><i class="fas fa-table"></i>Get Data</a> -->
											</div>
										</div>
									</div>
									<!--End default -->
									<!--start default1 -->
									<div id="default1" class="white_content2 alert-variable" style="display:none;">
										<div class="popup_alert">
											<div class="divclose">
												<a href="javascript:void(0);" onclick="document.getElementById('default1').style.display='none';">
													<img src="media/system/images/closebox.jpeg" alt="X">
												</a>
											</div>
											<div class="box-white">
												<p>Selections have been modified. Click on "Apply Changes" at any time to refresh the report with the changes made. Otherwise, click on "Cancel" to go back to previous selections.</p>
											</div>
											<div align="right">
												<a href="javascript:void(0);" id="applyChangesInitial" class="actionbtn btn-disabled btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a></li>
											</div>
										</div>
									</div>
									<!--End default1 -->
								</div>
							</div>
						</div>
					</div>
					<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
						<input type="hidden" name="refeterview" value="villagefront" />
						<input type="hidden" name="option" 		value="com_mica" />
						<input type="hidden" name="zoom" id="zoom" value="6" />
						<input type="hidden" name="view" 		value="summeryresults" />
						<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
						<input type="hidden" id="comparedata" 	value="0" />
					</div>
					<div id="light" class="white_content2 large-popup variable_add_popup" style="display: none;">
						<div class="divclose">
							<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';">
								<img src="media/system/images/closebox.jpeg" alt="X" />
							</a>
						</div>

						<script type="text/javascript">
							var lastchar = '';

							function checkfilledvalue(){

								var tmp1 = $('#new_custom_attribute_final_output').val();
								var tmp2 = $('#new_name').val();


								var flg  = 0;
								var len1 = parseInt(tmp1.length) - 1;
								var len2 = parseInt(tmp2.length) - 1;
							/*if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
								alert("ifin");
								alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
							}else if(lastchar=='opr' && flg == 0){
								alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
							}*/
							if(tmp1=='' || tmp2=='')
							{

							}
							else
							{
								if(!validateFormula_v2()){
									return false;
								}

								addCustomAttr($('#new_name').val(),encodeURIComponent($('#new_custom_attribute_final_output').val()));
							}
						}

						function addCustomAttr(attrname,attributevale){


							JQuery.ajax({
								url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
								method  : 'GET',
								success : function(data)
								{
										//getAttribute_v2(5);
										$("#light").hide();
										getDataNew();

										return;
									}

								});
						}



						function undofield(){

							document.getElementById('custom_attribute').innerHTML = '';


							lastchar = '';
						}
						function ClearFields()
						{
							$("#new_custom_attribute_final_output").val('')
						}



						JQuery(".full-data-view").on("#editvariable","click",function(){
							JQuery('#save').attr("id","updatecustom");
							JQuery('#updatecustom').attr("onclick","javascript:void(0)");
							is_updatecustom=1;
							JQuery('#updatecustom').attr("value","Update");
						});

						JQuery(".full-data-view").on(".customedit",'click',function(){
							if(is_updatecustom == 1){
								//alert(JQuery(this).find(":selected").text());
								JQuery('#new_name').val(JQuery(this).find(":selected").text());
								JQuery('#new_name').attr('disabled', true);
								oldattrval=JQuery(this).find(":selected").val();
								JQuery('#oldattrval').val(oldattrval);
								JQuery('textarea#custom_attribute').text("");
								lastchar = '';
								moverightarea();
							}
						});
					</script>
					<div class="variable_custom_data">
						<ul class="nav">
							<li class="active"><a data-toggle="tab" href="#home">Custom</a></li>
							<li><a data-toggle="tab" href="#menu1">Manage</a></li>
							<li><a data-toggle="tab" href="#menu2">Add New</a></li>
						</ul>

						<div class="tab-content">
							<div id="home" class="tab-pane fade in active">
								<div class="poptitle">
									Custom Variable
								</div>
								<div id="CustomVariable">
								</div>
							</div>
							<div id="menu1" class="tab-pane fade">
								<div id="customvariables">
									<div id="lightmanagevariable">

										<div class="poptitle">
											<?php echo JTEXT::_('MANAGE_CUSTOM');?>
										</div>

										<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
											<thead>
												<tr>
													<th align="center" width="10%">Use</th>
													<th align="center" width="45%">Variable</th>
													<th align="center" width="45%">Action</th>
												</tr>
											</thead>
											<?php
											$i             =0;

											$finalarray[1] =array_unique($finalarray[1]);
											foreach($finalarray[1] as $eacharray){
												if(in_array($eacharray,$finalarray[0])){
													$checked  = "checked";
													$selected = "Remove";
													$action   = "";
												}else{
													$checked  = "";
													$selected = "Select";
													$action   = "checked";
												}

												if(($i%2)==0){
													$clear = "clear";
												}else{
													$clear ="";
												}
												$namevalue = explode(":",$eacharray);


												echo '
												<tr>
												<td align="center" width="10%">
												<input type="checkbox" name="variable1"  class="dcv_checkbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
												<label></label>
												</td>
												<td align="center" class="customdelete" width="45%">'.$namevalue[0].'</td>
												<td align="center" width="45%">
												<a href="javascript:void(0);" style="float:left;padding-left:5px;">
												<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
												</a>
												<a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable	\').click();">
												<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
												</a>
												</td>
												</tr>';
												$i++;
											} ?>
										</table>

										<div class="readon frontbutton">
											<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
										</div>
									</div>

								</div>
							</div>
						</div>
					</form>
				</div>
			<div id="fade" class="black_overlay" style="display:none"></div>

				<script type="text/javascript">

				/*-------------export -----------*/
				/*document.getElementById('export').addEventListener("click", downloadPDF);
				function downloadPDF()
				{

					var canvas = document.querySelector('#myChart');
					var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
					var doc = new jsPDF('landscape');
					doc.setFontSize(20);
					doc.text(15, 15, "Cool Chart");
					doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
					doc.save('canvas.pdf');
				}*/
				/*-------------export end-----------*/




					$('#variablespan').on('change', '.variable_checkbox', function() {
						checkvalidation();
					});

					$('.leftcontainer').on('click', '.district_checkbox', function() {
						checkvalidation();
					});

					$('#subdistrictspan').on('change', '.sub_district_checkbox', function() {
					    checkvalidation();
					});

					$('.state_checkbox').change(function() {
						checkvalidation();
					});

					$('.allcheck').change(function() {
						checkvalidation();
					});

					$(".leftcontainer").on("click",".variable_checkbox",  function() {
						checkvalidation();
					});



					/*$(".explore-data").on('click','a#district_tab',function() {
						$(document).ready(function() {
							$('.slider').slick({
								dots: false,
								vertical: true,
								slidesToShow: 11,
								slidesToScroll: 1,
								verticalSwiping: true,
								arrrow: true,
							});
						});
					});*/






	</script>
