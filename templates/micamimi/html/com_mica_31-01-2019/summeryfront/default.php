<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  com_mica
	 *
	 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined('_JEXEC') or die;

	$db      = JFactory::getDbo();
	$session = JFactory::getSession();
	$doc     = JFactory::getDocument();
	$app     = JFactory::getApplication('site');


	$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery-ui.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');
		$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
	$doc->addStyleSheet(JURI::base().'components/com_mica/js/slick/slick.css', 'text/css');

	$itemid        = 188;
	$type1         = "";
	$m_type_rating = $app->input->get("m_type_rating", '', 'raw');
?>
	<script src="<?php echo JURI::base()?>components/com_mica/js/Chart.bundle.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/utils.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>

	<!-- start bubble chart js-->
	<script src="<?php echo JURI::base()?>components/com_mica/js/d3.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/bubble_chart.js"></script>
	<!-- end bubble chart js-->

	<script type="text/javascript">
		var JQuery = jQuery.noConflict();
		var $      = jQuery.noConflict();
	</script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-migrate-1.2.1.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/jspdf.min.js"></script>
	<script type="text/javascript">
		var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
		var districtalert     = '<?php echo JText::_("Please Select District first!!!")?>';
		var variabeltalert    = '<?php echo JText::_("Please Select variabel first!!!")?>';
		var metervariabel    = '<?php echo JText::_("Please Select variabel first!!!")?>';
		var meterdistrict    = '<?php echo JText::_("Please Select district first!!!")?>';
		var preselected       = '<?php echo $app->input->get("summarydistrict", "", "raw"); ?>';
		var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
		var preselectedm_type = '<?php echo $app->input->get("m_type", "", "raw"); ?>';
		var chart1;
		var keys              = [];
		var trashedLabels     = [];
		var trashedData       = [];
		var datasetValues     = [];
		var flag              = false;
	</script>
	<script src="templates/micamimi/js/jquery.scrollbar.js"></script>
	
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.scrollbar-inner').scrollbar();
		});
		 console.firebug=true;
	</script>
	<script src="<?php echo JURI::base()?>components/com_mica/maps/OpenLayers.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/summeryfield.js"></script>
	

	<style type="text/css">

	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	.btn-disabled,
	.btn-disabled[disabled] {
		cursor: default !important;
		pointer-events: none;
	}
	body {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
}
</style>

<div class="row-fluid top-row">
	<div class="col-md-4">
		<h1 class="title-explore"><?php echo JText::_('VILLAGE SUMMARY');?></h1>
	</div>
	<div class="col-md-8 text-right">
		<div class="sorter-tab">
			<ul>
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="graphdata" class="actionbtn btn-disabled" disabled="disabled" onclick="loadcharts();"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li>
			</ul>
		</div>
		<div class="worspacce-edit">
			My workspace <a class="workspacce-edit" href="javascript:void(0)" onclick="document.getElementById('workspacceedit').style.display='block';document.getElementById('fade').style.display='block'">
				<i class="fas fa-pencil-alt"></i>
			</a>
			<h2><div id="activeworkspacename">
				</div>
			</h2>
		</div>
	</div>
</div>
<div id="workspacceedit" style="display:none;" class="white_content2 large-popup">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('workspacceedit').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>
	<div class="poptitle">
		<?php echo JTEXT::_('MY_WORKSPACE');?>
	</div>

	<div class="scroll_div">
		<div class="col-md-8">
			<div class="blockcontent">
				<ul class="allworkspace">

					<?php
						$this->user = JFactory::getUser();
						$id = $this->user->id;

						if($id == 0){
							return -1;
						}

						$db   = JFactory::getDBO();
						$query = "SELECT name, id, is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($this->user->id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1) . "AND" . $db->quoteName('data') . " REGEXP '.*\"dataof\";s:[0-9]+:\"villageSummary\".*'";

								$db->setQuery($query);
								$result = $db->loadAssocList();
								$app               = JFactory::getApplication('site');
								$session           = JFactory::getSession();
								$str               = "";
								$activeworkspace   = $session->get('activeworkspace');
								$selected          = "";
								$activeprofilename = $eachresult['name'];
								$i                 = 1;

								foreach($result as $eachresult){
									if(($i%2)==0){
										$clear="clear";
									}else{
										$clear="";
									}

									if($activeworkspace == $eachresult['id']){

										$selected          = "selected";
										$edit              = "Edit/Delete";
										$activeprofilename = $eachresult['name'];

										echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('Active Workspace :<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
										$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
										echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
										$active_text = "<span style='font-weight:normal;'>(Active)<span>";
										$active_text = "";
									}
									else
									{
										$selected    = "";
										$edit        = "Select";
										$onclick     = "changeWorkspace(".$eachresult['id'].")";
										$active_text = "";
									}

									$str .= '
									<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
									<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
									<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'

							}
							echo $str;
							echo '<div id="lightn" class="white_contentn" style="display: none;">
							<div class="divclose">
							<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
							<img src="media/system/images/closebox.jpeg" alt="X" />
							</a>
							</div>
							<div align="left">
							<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
							<div class="frontbutton readon">
							<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
							</div>
							<div class="frontbutton">
							<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
							</div>
							</div>';

							?>
						</ul>
					</div>
				</div>

				<div class="col-md-4">
					<div class="contentblock endcontent">
						<div class="contenthead">New Workspace</div>
						<div class="blockcontent newworkspace">
							<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
							<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="explore-data">
			<form name="adminForm" id="micaform" action="" method="POST">
				<div class="row">
					<div class="col-md-4">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#state" aria-controls="state" role="tab" data-toggle="tab" id="state_tab">
									<em class="icon-state"></em>State
								</a>
							</li>
							<li role="presentation">
								<a href="#district" aria-controls="district" role="tab" data-toggle="tab" id="district_tab">
									<em class="icon-district"></em>District
								</a>
							</li>
							<li role="presentation">
				            	<a href="#sub_district" aria-controls="sub_district" role="tab" data-toggle="tab" id="sub_district_tab">
				            		<em class="icon-district"></em>Sub District
				            	</a>
				            </li>
							<li role="presentation">
								<a href="#variables" aria-controls="variables" role="tab" data-toggle="tab" id="variables_tab">
									<em class="icon-variables"></em>Variables
								</a>
							</li>
						</ul>

						<!-- START Tab panes -->
						<div class="tab-content leftcontainer" id="leftcontainer">
							<!-- START State -->
							<div role="tabpanel" class="tab-pane active" id="state">
								<div class="loader-div" id="loader" style="display: none">
									<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
								</div>
								<div class="top-sorter-div">
									<div class="row">
										<div class="col-md-5 col-xs-5">
											<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
												<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
											</div>
											|
											<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
											<label for="state_allcheck"></label>
											|
											<a href="javascript:void(0);" onclick="sortListDir('id01')">
												<i class="fa fa-sort"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="scrollbar-inner">
									<ul class="list1 statelist" id="id01">
										<?php
										$states = explode(",", $app->input->get("summarystate", '', 'raw'));
										for($i = 0; $i < count($this->state_items); $i++){
											$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
											<li>
												<input type="checkbox" class="state_checkbox" name="summarystate[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="summarystate<?php echo $i;?>">
												<label for="summarystate<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
											</li>
										<?php  } ?>
									</ul>
								</div>
							</div>
							<!-- END State -->

							<!-- START District -->
							<div role="tabpanel" class="tab-pane" id="district">
								<div class="loader-div" id="loader" style="display: none">
								      <img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
								</div>
								<div id="districtspan">
									<div class="top-sorter-div">
										<div class="row">
											<div class="col-md-5 col-xs-5">
												<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
											</div>
											<div class="col-md-7 col-xs-7 text-right">
												<div class="searchbox">
													<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
												</div>
												|
												<input type="checkbox" name="district_checkall" class="allcheck" id="district_allcheck">
												<label for="district_allcheck"></label>
												|
												<a href="javascript:void(0);" onclick="district_sortListDir()">
													<i class="fa fa-sort"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="scrollbar-inner">
												<ul class="list1 districtlist" id="district_list" onchange="getSubDistrict(this.value)">
												</ul>
											</div>
										</div>
										<div class="col-md-2 padding-none">
											<div class="dist-icon-list">
												<div id="statecode" >
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END District -->

							<!-- START Sub District -->
				        	<div role="tabpanel" class="tab-pane" id="sub_district">
					        		<div class="loader-div" id="loader" style="display: none">
									      <img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
									</div>
				        		<div id="subdistrictspan">
					        		<div class="top-sorter-div">
				                		<div class="row">
				                			<div class="col-md-5 col-xs-5">
				                				<h3><?php echo JText::_('SUB_DISCTRICT_LABEL'); ?></h3>
				                			</div>
				                			<div class="col-md-7 col-xs-7 text-right">
				                				<div class="searchbox">
				                					<input type="search" class="searchtop" id="subdistrict_searchtop" placeholder="Search">
				                				</div>
				                				|
												<input type="checkbox" name="subdistrict_checkall" class="allcheck" id="subdistrict_allcheck">
												<label for="subdistrict_allcheck"></label>
				                				|
				                				<a href="javascript:void(0);" onclick="sub_district_sortListDir()">
				                					<i class="fa fa-sort"></i>
				                				</a>
				                			</div>
				                		</div>
				                	</div>
				                	<div class="row">
			                			<div class="col-md-10">
			                				<div class="scrollbar-inner">
					                            <ul class="list1 subdistrictlist">
												</ul>
											</div>
			                			</div>
			                			<div class="col-md-2 padding-none">
			                				<div class="dist-icon-list">
			                					<div id="statecode">
			                					</div>
			                				</div>
			                			</div>
			                		</div>
				                </div>
				        	</div>
				        	<!-- END Sub District -->

							<!-- START Variable -->
							<div role="tabpanel" class="tab-pane" id="variables">
								<div class="loader-div" id="loader" style="display: none">
								      <img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
								</div>
								<div id="variablespan">
									<div class="top-sorter-div">
										<div class="row">
											<div class="col-md-5 col-xs-5">
												<h3><?php echo JText::_('Select Variable'); ?></h3>
											</div>
											<div class="col-md-7 col-xs-7 text-right">
												<div class="searchbox">
													<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
												</div>
												|
												<input type="checkbox" name="variable_checkall" class="allcheck" id="variable_allcheck">
												<label for="variable_allcheck"></label>
												|
												<a href="#sort" onclick="variable_sortListDir()">
													<i class="fa fa-sort"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="scrollbar-inner">
												<div id="variables"></div>
												<div id="statetotal"></div>
											</div>
										</div>
										<div class="col-md-2">
											<div id="variableshortcode"></div>
										</div>
									</div>
								</div>
							</div>
							<!-- END Variable -->
						</div>
						<!-- END Tab panes -->
					</div>

					<div class="col-md-8">
						<div class="full-data-view">
							<div id="result_table" style="display:none">
								<div class="text contenttoggle1" id="textscroll">
									<div class="button-area text-right">
										<a href="#alldata" onclick="downloadAction();"><i class="fas fa-download"></i><?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?></a>
									</div>

									<div class="collpsable1">
									</div>

									<div class="activecontent right-scroll-div tableview">
										<div class="alldata contenttoggle2" id="jsscrollss alldata">
										</div>
										<?php echo "<br /><br />"; ?>
									</div>
								</div>
							</div>

							<!-- Graph START -->
							<div id="graph" style="display:none;">
								<div class="graphs contenttoggle1">
									<div class="row">
										<div class="col-md-4">
											<div class="chart_type">
												<label>Chart Type:</label>
												<select name="charttype" class="inputbox" id="chartype">
													<option value="bar" selected>Bar Chart</option>
													<option value="line">line Chart</option>
													<option value="radar">Radar Chart</option>
													<option value="bubble">Bubble Chart</option>
												</select>

												<label>Show Legend</label>
												<input type="checkbox" id="enablelegend" name="enablelegend"  value="1" />
											</div>
										</div>

										<div class="col-md-5">
											<div class="row">
												<div class="col-md-12">
													<?php
													$variableoptions="";
													$variableopopup = '<div id="light1007" class="white_content2 tab-pane" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1007\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select District</div>';
													$distopopup     = '<div id="light1008" class="white_content2 tab-pane" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1008\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select  Variable</div>';
													$distoptions    ='<div id="variabelist">
													</div>';
													?>
													<?php
													echo $distopopup.$distoptions."</div>";
													?>

													<a href="javascript:void(0);" id="graphdistrict" onClick="document.getElementById('light1008').style.display='block';document.getElementById('fade').style.display='block'"><b>Select  Variable</b></a>

													&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;

													<a href="javascript:void(0);" id="graphvariable" onClick="document.getElementById('light1007').style.display='block';document.getElementById('fade').style.display='block'"><b>Select  District </b></a>

													&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;

													<a href="javascript:void(0);" id="secondLevelGraphvariable" onClick="document.getElementById('light1010').style.display='block';document.getElementById('fade').style.display='block'"><b>Select Variable for second level graph</b></a>

													<?php
														echo $variableopopup.'<div id="cblist" ></div>';
														echo $secondLevelVariableopopup.'<div id="secondLevelVariabelist" ></div></div>';
			                                        ?>
													<?php
													//echo $variableopopup.'<div id="cblist"></div></div>';
													?>
												</div>
											</div>
										</div>
										<div class="col-md-3">
										<div id="graphfunction" style="display:none;" class="white_content2">
											<div class="divclose">
												<a href="javascript:void(0);" onclick="document.getElementById('graphfunction').style.display='none';document.getElementById('fade').style.display='none';">
												<img src="media/system/images/closebox.jpeg" alt="X">
												</a>
												</div>
											</div>
											<div class="button-area-2">
											<div id="exportchartbutton" class="exportbtn">
												<input type="button" id="graphfunction" onClick="document.getElementById('graphfunction').style.display='block';document.getElementById('fade').style.display='block'"  class="button" value="Graph Data"></a>
											</div>
												<div id="exportchartbutton" class="exportbtn">
													<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
												</div>
											</div>
										</div>
									</div>
									 <canvas id="myChart" style="width:100%; height:520px; background-color:#FFFFFF;"></canvas>
									 <div id="venn" style="width:100%; height:520px; background-color:#FFFFFF;display:none;" ><svg></svg></div>
									 <div id="venn_label"></div>

									<!-- end of amcharts script -->
								</div>
							</div>
							<!-- graph end -->
							<!--Start GIS -->
							<div id="gis" style="display:none;">
								<!--Start Thematic -->
								<div id="light_thematic" style="display: none;" class="tq white_content">

									<div class="divclose">
										<a href="javascript:void(0);" onclick="document.getElementById('light_thematic').style.display='none';">
											<img src="media/system/images/closebox.jpeg" alt="X">
										</a>
									</div>

									<div class="" style="width:100%;float:left;">
										<div class="thematicarea clearfix">
											<div class="blockcontent">
												<div id='blockcontent_data'>
												<?php function getCustomAttrName($name,$activeworkspace) {
													$db    = JFactory::getDBO();
													$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
														WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
															AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
													$db->setQuery($query);
													$result = $db->loadAssoc();

													if(count($result) == 0){
														return array($name,$name);
													}else{
														return array($result[0],$result[1]);
													}
												}

												$activeworkspace = $this->activeworkspace;


												$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
													WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
													ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
												$db->setQuery($query);
												$result = $db->loadObjectList();

												$id    = array();
												$level = array();
												$i     = 1;
												$j     = 1;
												$grouping = array();
												foreach($result as $range){
													if($range->level == "0"){
														$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
													}else{
														$pin = str_replace("#","",$range->color);
														$str = " <div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
													}

													$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
													//$id[] = $range->custom_formula;
													//$grouping[$range->custom_formula] = $range->level;
													$level[$range->custom_formula][] = $range->level;
													$i++;
													$j++;
												}

												$i            = 0;
												$range        = array();
												$str          = array();
												$totalcount   = 0;
												$l            = 0;
												$tojavascript = array();
												$grpname = array();
												foreach($grouping as $key => $val){
													$grname         = getCustomAttrName($key, $activeworkspace);
													$grpname[]      = $grname;
													$str[]          = implode(" ",$val);
													$ranges[]       = count($val);
													$levelunique[]  = $level[$key][0];
													$tojavascript[] = $key;
													$l++;
												}
												$tojavascript = implode(",",$tojavascript);
												//$str="";
												for($i = 0; $i < count($grpname); $i++){
													echo "<div class='contentblock1 '>
														<div class='blockcontent'>
															<ul class='maingrp bullet-add clear' >
																<li>
																	<div class='themehead'>".$grpname[$i][0]."</div>
																	<div class='themeedit'>
																		<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
																			<img src='".JUri::base()."components/com_mica/images/edit.png' />
																		</a>
																		<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
																			&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
																		</a>
																	</div>
																	<ul class='range' >".$str[$i]."</ul>
																</li>
															</ul>
														</div>
													</div>";
												} ?>
												</div>
												<div class='contentblock3'>
													<script type='text/javascript'>
														var totalthemecount     = "<?php echo count($grpname); ?>";
														var usedlevel           = '<?php echo implode(",",$levelunique); ?>';
														var havingthematicquery = '<?php echo $tojavascript; ?>';
													</script>
													<div id="themeconent">
														<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>" method="get" >
															<table cellspacing="4" cellpadding="0" border="0" width="90%" style="float: left;width: 335px;">
																<tr>
																	<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL');?></h3></td>
																</tr>
																<tr>
																	<td>
																		<b><?php echo JText::_('ATTRIBUTE_LABEL');?></b>
																	</td>
																	<td>
																		<select name="thematic_attribute" id="thematic_attribute" class="inputbox" style="width: 200px;" onchange="getMinmaxVariable_v2(this.value);">
																			<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
																			<optgroup label="Default Variable">
																				<?php $attr = $this->themeticattribute;
																				$eachattr = explode(",",$attr);
																				foreach($eachattr as $eachattrs){
																					echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
																				} ?>
																			</optgroup>
																			<optgroup label="Custom Variable">
																				<?php $attr = $this->customattribute;
																				$eachattr = explode(",",$attr);
																				$eachattr = array_filter($eachattr);
																				foreach($eachattr as $eachattrs){
																					$eachattrs=explode(":",$eachattrs);
																					echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
																				} ?>
																			</optgroup>
																		</select>
																	</td>
																</tr>
																<tr id="minmaxdisplay">
																	<td></td>
																	<td class="minmaxdisplay"></td>
																</tr>
																<tr>
																	<td>
																		<b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b>
																	</td>
																	<td>
																		<input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;">
																	</td>
																</tr>
																<tr class="colorhide">
																	<td>
																		<b><?php echo JText::_('SELECT_COLOR_LABEL');?></b>
																	</td>
																<td>
																	<input class="simple_color" value="" style="width: 194px;"/>
																</td>
																</tr>
																<tr>
																	<td colspan="2">
																		<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval"></table>
																	</td>
																</tr>
																<input type="hidden" name="maxvalh" id="maxvalh" value="">
																<input type="hidden" name="minvalh" id="minvalh" value="">
																<input type="hidden" name="level" id="level" value="">
																<tr>
																	<td colspan="2" align="center">
																		<div class="readon frontbutton" style="float: left;">
																			<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" onclick="saveSLD()"/>
																		</div>
																		<div class="readon frontbutton" style="float: left;padding-left: 5px;">
																			<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup();"/>
																		<div>
																	</td>
																</tr>
															</table>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--End Thematic -->
								<div class="gis contenttoggle1" style="border:1px solid red">
									<div class="text contenttoggle1">
										<div class="button-area text-right">
											<a href="javascript:void(0);" onclick="downloadMapPdf();" id="options" class="frontbutton" style="text-decoration: none;">
											<input type="button" class="frontbutton" name="Export" value="Export">
											</a>
										</div>
										<div class="button-area text-right">
											<?php  $fromthematic = $session->get('fromthematic'); ?>
											<a href="javascript:void(0);" id="fullscreen" class="frontbutton" <?php if($fromthematic==1){ ?> fromthematic="1" <?php }else{?> fromthematic="0"  <?php }?> >Full Screen
													<!-- <input style="margin-right: 3px;" type="button" class="frontbutton" name="fullscreen" value="Full Screen"> -->
											</a>
										</div>
										<div class="button-area text-right">
											<a href="javascript:void(0);" id="thematic_btn" class="frontbutton" onclick="document.getElementById('light_thematic').style.display='block';">Thematic Query
												<!-- <input style="margin-right: 3px;" type="button" class="frontbutton" name="thematicButton" value="Thematic Query"> -->
											</a>
										</div>
									</div>
									<div class="map_controllers"></div>
									<div id="map" style=" height:400px;width: 100%;"></div>
								</div>
							</div>
					<!--Start GIS -->

							<!--Start Speed -->
							<div id="potentiometer" style="display:none;">
								<div class="speedometer contenttoggle1" >
									<div class="filter-section">
										<div style="display:none;" id="speedfiltershow">

										</div>
										<div class="sfilter">
											<div class="top-filter-sec">
												<div class="fl_title">
													<span class="filterbylabel"></span>
												</div>
												<div class="fl_type radio-list fl_right">
													<!-- start popup for potentiometer Filters-->
													<div id="variablefiltetr" style="display:none;" class="white_content2 filterpopup">
														<div class="divclose">
														<a href="javascript:void(0);" data-me="variablefiltetr" class="closeme1">
														<img src="media/system/images/closebox.jpeg" alt="X">
														</a>

														</div>
														<div class="filterbylabel"></div>
														<div class="sfilter">
															<div>
																<div class="lft speed" style="display:none;">
																<div class="row">
																	<div class="col-sm-6">
																		<div id="speed_variable"></div>
																	</div>
																	<div class="col-sm-6">
																		<div id="speed_region"></div>
																	</div>
																</div>
																<div class="frontbutton ">
																	<input type="button" name="showspeed" id="showspeed" class="" value="Create">
																</div>
																</div>
															</div>
														</div>
													</div>
													<!-- end popup for potentiometer Filters-->
													<ul>
														<li>
															<a href="javascript:void(0);" id="filter1" data-val="0"  class="filterspeed">Filter By  Layer</a>

														</li>
														<li>
															<a href="javascript:void(0);" id="filter2"  data-val="1"  class="filterspeed">Filter By  Variable</a>

														</li>
													</ul>


												</div>

											</div>
										</div>

									</div>
									<div id="spedometer_region" class="right-scroll-div spedometer_sec"></div>
								</div>
							</div>
							<!--End Speed -->

							<div id="quartiles" class="matrix contenttoggle1">
								<div id="fullscreentable"></div>
							</div>
							<!--START default3 -->
							<div id="default3" class="default3 contenttoggle1">
								<p>Step 1 : Select State</p>
								<p>Step 2 : Select District of selected State</p>
								<p>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</p>
								<p>Step 4 : Select the Variables you want to view. (Multiple selection allowed)</p>
								<p>Step 5 : Select MPI (Multiple selection allowed),</p>
								<p>Step 6 : Select sector wise composite score. (Multiple selection allowed)
								Note : Mark ‘All’ in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</p>
								<!-- <div align="left">
									 <a href="javascript:void(0);" id="submitta" style="display:none;" class="actionbtn btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a>
									</div> -->
								</div>
								<!--End default3 -->

								<!--start default -->
								<div id="default" class="white_content2 alert-variable">
									<div class="popup_alert">
										<div class="default contenttoggle1" >
											<div class="divclose">
												<a href="javascript:void(0);" onclick="document.getElementById('default').style.display='none';document.getElementById('fade').style.display='none';">
												<!-- 	<img src="media/system/images/closebox.jpeg" alt="X"> -->
												</a>
											</div>
											<div class="box-white">
												<p>Please select variables from each of the following dimensions to view a report. You can select from left panel or by clicking the links above.
												</div>
												<ul class="list1">
													<li id="statetext"><i class="far fa-check-square"></i>state</li>
													<li id="districttext"><i class="far fa-check-square"></i>District</li>
													<li id="variabletext"><i class="far fa-check-square"></i>Variabel</li>
												</ul>


												<div align="right">
													<a href="javascript:void(0);" id="apply_chnages" class="actionbtn btn-disabled btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a>
												</div>
												<!-- <a href="/" id="submit" class="actionbtn" onclick="getDataNew();" disabled="disabled"><i class="fas fa-table"></i>Get Data</a> -->
											</div>
										</div>
									</div>
									<!--End default -->
									<!--start default1 -->
									<div id="default1" class="white_content2 alert-variable" style="display:none;">
										<div class="popup_alert">
											<div class="divclose">
												<a href="javascript:void(0);" onclick="document.getElementById('default1').style.display='none';">
													<!-- <img src="media/system/images/closebox.jpeg" alt="X"> -->
												</a>
											</div>
											<div class="box-white">
												<p>Selections have been modified. Click on "Apply Changes" at any time to refresh the report with the changes made. Otherwise, click on "Cancel" to go back to previous selections.</p>
											</div>
											<div align="right">
												<a href="javascript:void(0);" id="applyChangesInitial" class="actionbtn btn-disabled btn-submit" disabled="disabled"><i class="fas fa-table"></i>Apply Changes</a></li>
											</div>
										</div>
									</div>
									<!--End default1 -->
								</div>
							</div>
						</div>
					</div>
					<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
						<input type="hidden" name="refeterview" value="villagefront" />
						<input type="hidden" name="option" 		value="com_mica" />
						<input type="hidden" name="zoom" id="zoom" value="6" />
						<input type="hidden" name="view" 		value="summeryresults" />
						<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
						<input type="hidden" id="comparedata" 	value="0" />
					</div>

			<div id="fade" class="black_overlay" style="display:none"></div>

				<script type="text/javascript">

				/*-------------export -----------*/
				document.getElementById('export').addEventListener("click", downloadPDF);
				function downloadPDF()
				{

					var canvas = document.querySelector('#myChart');
					var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
					var doc = new jsPDF('landscape');
					doc.setFontSize(20);
					doc.text(15, 15, "Cool Chart");
					doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
					doc.save('canvas.pdf');
				}
				/*-------------export end-----------*/

					$('#variablespan').on('change', '.variable_checkbox', function() {
						checkvalidation();
					});

					$('.leftcontainer').on('click', '.district_checkbox', function() {
						checkvalidation();
					});

					$('#subdistrictspan').on('change', '.subdistrict_checkbox', function() {
					    checkvalidation();
					});

					$('.state_checkbox').change(function() {
						checkvalidation();
					});

					$('.allcheck').change(function() {
						checkvalidation();
					});

					$(".leftcontainer").on("click",".variable_checkbox",  function() {
						checkvalidation();
					});
	</script>
	<script type="text/javascript">
	var siteurl = '<?php echo JURI::base();?>';
	var tomcaturl ="<?php echo $this->tomcaturl; ?>";

	$("#enablelegend").click(function() {
	    chart1.options.legend.display = true;
	    chart1.update();
	    if (document.getElementById('enablelegend').checked) {
	        chart1.options.legend.display = true;
	        chart1.update();

	    } else {
	        chart1.options.legend.display = false;
	        chart1.update();


	    }

	});
	var gis_state = '<?php echo str_replace(",","','",$session->get('state'));?>';
	var gis_gis_district = '';
	var districtsearchvariable = '';
	var gis_UnselectedDistrict = '';
	var javazoom  = 6;
	var gis_town ='';
	var townsearchvariable = '';
</script>
<script type="text/javascript" id="j1">
	var globalcorrdinates  = "";
	// var map             = new OpenLayers.Map("map");
	var oldzoom            = 0;
	var geo                = [];
	var dystate            = [];
	var geo                = [];
	//OpenLayers.ProxyHost = "http://192.168.5.159/cgi-bin/proxy.cgi?url=";
	// use a CQL parser for easy filter creation
	var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;
	OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
	var tomcaturl="<?php echo $this->tomcaturl; ?>";
	var mainzoom = "<?php echo $this->zoom; ?>";
	var tomcatpathlink = "<?php echo 'http://mica-mimi.in/'.TOMCAT_SLD_FOLDER.'/'; ?>";
	var userid = "<?php echo $this->userid; ?>";
	var activeworkspace = "<?php echo $this->activeworkspace; ?>";
	var sldlinkStateBoundaries = tomcatpathlink+userid+"/"+activeworkspace+"/rail_state.sld";
	var sldlinkDistricts = tomcatpathlink+userid+"/"+activeworkspace+"/india_information.sld";
	var sldlinkCities = tomcatpathlink+userid+"/"+activeworkspace+"/my_table.sld";
	var sldlinkUrban = tomcatpathlink+userid+"/"+activeworkspace+"/jos_mica_urban_agglomeration.sld";
	var mainGeometry = "<?php echo $this->geometry; ?>";
	var mainlonglat = "<?php echo $app->input->get('longlat', '', 'raw'); ?>";
	var mainState = "<?php echo $this->state; ?>";
	var mainDistrict = "<?php echo $this->district; ?>";
	var mainUnselectedDistrict = "<?php echo $this->UnselectedDistrict; ?>";
	var javazoom = mainzoom;
	var mainTown = "<?php echo $this->town; ?>";
	var mainUrban = "<?php echo $this->urban; ?>";

	function onPopupClose(evt) {
		// 'this' is the popup.
		JQuery("#featurePopup").remove();
	}

	function displaymenu(e){
		//$("#menupopup").remove();
		//var OuterDiv=$('<div  />');
		//OuterDiv.append($("#menu").html());
		//OuterDiv.append($("#menu").html());
		//console.log(e);
		//OuterDiv.css({"z-index":"100000","left":e.screenX,"top":e.screenY,"display":"block"});
		//console.log({e.layerX,e.layerY});

		// popup = new OpenLayers.Popup("menupopup",map.getLonLatFromPixel(new OpenLayers.Pixel({e.layerX,e.layerY})),new OpenLayers.Size(200,300),$("#menu").html(), false);
		//	map.popup = popup;
		//evt.feature = feature;
		//	map.addPopup(popup);
	}

	function onFeatureSelect(response)
	{


		response = response.trim("");

		//console.log("response=> "+response);

		var getsegment=response.split("OGR_FID:");
		console.log("getsegment=> "+getsegment);
		var id;

		if(typeof(getsegment[1])=="undefined")
		{
			return false;
		}
		else
		{
			id=getsegment[1].trim();
			//alert(getsegment[3]);
			if(typeof(getsegment[2])!="undefined" ){
			  if(getsegment[2]==362 || getsegment[2]==344 || getsegment[2]==205 || getsegment[2]==520 || getsegment[2]==210 || getsegment[2]==211 || getsegment[2]==206)
					id = getsegment[2].trim();
			   if(getsegment[3]==209)
							  id=getsegment[3].trim();
				if(getsegment[4]==209)
							  id=getsegment[4].trim();
				if(getsegment[3]==207)
							  id=getsegment[3].trim();
				if(getsegment[1]==205)
							  id=getsegment[1].trim();
				if(getsegment[1]==206 && getsegment[2]==207 && getsegment[3]==212)
							  id=getsegment[2].trim();
			}
		}

		id = parseInt(id);
		JQuery(".olPopup").css({"display":"none"});

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.popupAttributes&id="+id+"&zoom="+javazoom,
			method  : 'GET',
			success : function(data){
				popup = new OpenLayers.Popup.FramedCloud("featurePopup",
					map.getLonLatFromPixel(new OpenLayers.Pixel(coordinates.xy.x,coordinates.xy.y)),
					new OpenLayers.Size(300,150),
					data,null ,true, onPopupClose
				);
				map.popup = popup;
				//evt.feature = feature;
				map.addPopup(popup);
			}
		});
	}

	function onFeatureUnselect(evt) {
		feature = evt.feature;
		if (feature.popup) {
			popup.feature = null;
			map.removePopup(feature.popup);
			feature.popup.destroy();
			feature.popup = null;
		}
	}

	function stateCqlFilter()
	{
		if(mainState != "all")
		{
			statetojavascript = mainState.replace("," , "','");
			return "name in ('"+statetojavascript+"')";
		}
	}

	function districtCqlFilter()
	{

		if(mainDistrict != "all" && mainDistrict != "" )
		{
			statetojavascript = mainDistrict;
			//statetojavascript =statetojavascript.replace(",", "','");
			return "OGR_FID in ("+statetojavascript+")";
			//return "OGR_FID in ('345','83','347','623')";

			//return "OGR_FID in ('"+statetojavascript+"')";

			/*if(mainUnselectedDistrict.length > mainDistrict)
			{

				statetojavascript = mainDistrict;
				statetojavascript ="'"+ statetojavascript.replace(",", "','")+"'";
				return "OGR_FID in ('"+statetojavascript+"')";
			}
			else
			{
				alert('123');
				statetojavascript = mainUnselectedDistrict;
				statetojavascript = statetojavascript.replace(",","','");
				return "OGR_FID not in ('"+statetojavascript+"')";
			}*/
		}
		else if(mainDistrict == "all")
		{
			return  "state in ('"+statetojavascript+"')";
		}
	}

	function cityCqlFilter(){
		if(mainTown != "all" &&  mainTown != "")
		{
			statetojavascript = mainTown;
			//statetojavascript =statetojavascript.replace(",", "','");
			return "OGR_FID in ('"+statetojavascript+"')";
		}

		if(mainTown == "all")
		{
			return  "state in ('"+statetojavascript+"')";
		}
		<?php  /*if($this->town!="all" && $this->town!="" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			$statetojavascript = $this->town;
			$statetojavascript = str_replace(",","','",$statetojavascript);
			echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
		}

		if($this->town == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			echo " return  \"state in ('".$statetojavascript."')\";";
		}*/ ?>
	}

	function urbanCqlFilter()
	{
		if(mainUrban != "all" && mainUrban != "")
		{
			statetojavascript = mainUrban;
			//statetojavascript =statetojavascript.replace(",", "','");
			return "OGR_FID in ('"+statetojavascript+"')";
		}

		if(mainUrban == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			return  "state in ('"+statetojavascript+"')";
		}

		<?php  /*if($this->urban != "all" && $this->urban != ""){
			//$stateArray=explode(",",JRequest::GetVar('state'));
			$statetojavascript = $this->urban;
			$statetojavascript = str_replace(",","','",$statetojavascript);
			echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
		}

		if($this->urban == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
		}*/ ?>
	}

	function changeAttrOnZoom(javazoom,response){
		onFeatureSelect(response.responseText);
	}

	function reloadCustomAttr(){
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getCustomAttr",
			method  : 'GET',
			success : function(data){
				JQuery("#addcustomattribute").html(data);
			}
		});
	}

	JQuery(".delcustom").live("click",function(){
		var myid    = JQuery(this).attr('id');
		var segment = myid.split("_");
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteCustomAttr&id="+segment[2],
			method  : 'GET',
			success : function(data){
				JQuery("#"+myid).parent("tr").remove();
			}
		});
	});
</script>
