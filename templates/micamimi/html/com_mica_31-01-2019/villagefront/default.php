<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$app = JFactory::getApplication('site');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');
// For Vertical Slider
$doc->addStyleSheet(JURI::base().'components/com_mica/js/slick/slick.css', 'text/css');
$itemid        = 188;
$type1         = "";//$app->input->get('villagem_type', '', 'raw');
$m_type_rating = $app->input->get("village_m_type_rating", '', 'raw');
//$composite     = $app->input->get("village_composite", '', 'raw');
?>
	<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>
<!-- Start Chart JS -->
<script src="<?php echo JURI::base()?>components/com_mica/js/Chart.bundle.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/utils.js"></script>
<!-- End Chart JS -->
<!-- For Vertical Slider -->
<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>

<script type="text/javascript">

	var JQuery = jQuery.noConflict();
	var $      = jQuery.noConflict();
	jQuery(document).ready(function($) {
		jQuery('#micaform').submit(function(event) {
			var tempVar = JSON.stringify(jQuery('#villages_villages').val());
			jQuery('#tempvillagejson').val(tempVar);
			jQuery('#villages_villages').attr('disabled', 'disabled');
		});
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>

<script src="<?php echo JURI::base()?>components/com_mica/js/villagefield.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
	<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<style type="text/css">
canvas {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}

.btn-disabled,
	.btn-disabled[disabled] {
	cursor: default !important;
	pointer-events: none;
	}
</style>
<script type="text/javascript">
	var JQuery             = jQuery.noConflict();
	var $                  = jQuery.noConflict();
	var statealert         = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var districtalert      = '<?php echo JText::_("SELECT_DISTRICT_ALERT")?>';
	var industryLevelalert = '<?php echo JText::_("Please select IndustryLevel")?>';
	var variablealert      = '<?php echo JText::_("SELECT_VARIABLE_ALERT")?>';
	var metervariabel      = '<?php echo JText::_("Please select variabel")?>';
	var meterdistrict      = '<?php echo JText::_("Please select district")?>';
	var preselected        = '<?php echo $app->input->get("villagedistrict", "", "raw"); ?>';
	var preselecteddata    = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type  = '<?php echo $app->input->get("villagem_type", "", "raw"); ?>';
	var chart1;
	var keys              = [];
	var trashedLabels     = [];
	var trashedData       = [];
	var datasetValues     = [];
	var validation_flag = false;

	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	JQuery(function() {
		JQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		JQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});
	JQuery(document).ready(function(){ });

	function checkAll(classname){
		JQuery("."+classname).each(function (){
			JQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		JQuery("."+classname).each(function (){
			JQuery(this).attr("checked",false);
		});
	}
</script>

<script src="templates/micamimi/js/jquery.scrollbar.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();
    });
</script>


<div class="row-fluid top-row">
	<div class="col-md-2">
		<h1 class="title-explore"><?php echo JText::_('Village Data');?></h1>
	</div>
	<div class="col-md-10 text-right">
		<div class="sorter-tab">
			<ul>
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="loadchart" class="actionbtn btn-disabled" disabled="disabled" onclick="loadCharts();"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li>
			</ul>
		</div>

		<div class="worspacce-edit">
			My workspace <a class="workspacce-edit" href="javascript:void(0)" onclick="document.getElementById('workspacceedit').style.display='block';document.getElementById('fade').style.display='block'">
				<i class="fas fa-pencil-alt"></i>
			</a>
			<h2>
				<div id="activeworkspacename"></div>
			</h2>
		</div>
	</div>
</div>

<div id="workspacceedit" style="display:none;" class="white_content2 large-popup">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('workspacceedit').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>
	<div class="poptitle">
		<?php echo JTEXT::_('MY_WORKSPACE');?>
	</div>

	<div class="scroll_div">
		<div class="col-md-8">
			<div class="blockcontent">
				<ul class="allworkspace">
					<?php
						$this->user = JFactory::getUser();
						$id = $this->user->id;

						if($id == 0){
							return -1;
						}

						$db   = JFactory::getDBO();
						$query="SELECT name, id, is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1). "AND" . $db->quoteName('data') . " REGEXP '.*\"dataof\";s:[0-9]+:\"villages\".*'";
						$db->setQuery($query);
						$result = $db->loadAssocList();

						$app               = JFactory::getApplication('site');
						$session = JFactory::getSession();
						$str               = "";

						//$activeworkspace   = $app->input->get('activeworkspace', '', 'raw');
						$activeworkspace   = $session->get('activeworkspace');
						$selected          = "";
						$activeprofilename = $eachresult['name'];
						$i                 = 1;
						foreach($result as $eachresult){
							if(($i%2)==0){
								$clear="clear";
							}else{
								$clear="";
							}

							if($activeworkspace == $eachresult['id']){

								$selected          = "selected";
								$edit              = "Edit/Delete";
								$activeprofilename = $eachresult['name'];

								echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
								$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
								echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
								$active_text = "<span style='font-weight:normal;'>(Active)<span>";
								$active_text = "";

							}else{

								$selected    = "";
								$edit        = "Select";
								$onclick     = "changeWorkspace(".$eachresult['id'].")";
								$active_text = "";
							}

							$str .= '
								<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
										<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
										<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'
						}
						echo $str;
						echo '<div id="lightn" class="white_contentn" style="display: none;">
								<div class="divclose">
									<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
										<img src="media/system/images/closebox.jpeg" alt="X" />
									</a>
								</div>
								<div align="left">
									<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
									<div class="frontbutton readon">
										<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
									</div>
									<div class="frontbutton">
										<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
									</div>
								</div>';
					?>
				</ul>
			</div>
		</div>

		<div class="col-md-4">
			<div class="contentblock endcontent">
				<div class="contenthead">New Workspace</div>
				<div class="blockcontent newworkspace">
					<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
					<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="explore-data">
	<form name="adminForm" id="micaform" action="" method="POST">
		<div class="row">
			<div class="col-md-4">
				<ul class="nav nav-tabs" role="tablist">
		            <li role="presentation" class="active" id="state_tab">
		            	<a href="#state" aria-controls="state" role="tab" data-toggle="tab">
		            		<em class="icon-state"></em>State
		            	</a>
		            </li>

		            <li role="presentation">
		            	<a href="#district" aria-controls="district" role="tab" data-toggle="tab" id="district_tab">
		            		<em class="icon-district"></em>District
		            	</a>
		            </li>

		            <li role="presentation">
		            	<a href="#sub_district" aria-controls="sub_district" role="tab" data-toggle="tab" id="sub_district_tab">
		            		<em class="icon-district"></em>Sub District
		            	</a>
		            </li>

		            <li role="presentation">
		            	<a href="#village" aria-controls="village" role="tab" data-toggle="tab" id="village_tab">
		            		<em class="icon-type"></em>Village
		            	</a>
		            </li>

		            <li role="presentation">
		            	<a href="#variables" aria-controls="variables" role="tab" data-toggle="tab" id="variables_tab">
		            		<em class="icon-variables"></em>Variables
		            	</a>
		            </li>
		        </ul>


				<!-- START Tab panes -->
		        <div class="tab-content leftcontainer" id="leftcontainer">
		        	<!-- START State -->
		        	<div role="tabpanel" class="tab-pane active" id="state">
		        		<div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
						<div id="statespan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
		                				<label for="allcheck"></label>
		                				|
		                				<a href="javascript:void(0);" onclick="sortListDir('id01')">
												<i class="fa fa-sort"></i>
											</a>
		                			</div>
		                		</div>
		                	</div>
		                </div>
	                	<div class="scrollbar-inner">
                            <ul class="list1 statelist" id="id01">
                            	<?php
								$states = explode(",", $app->input->get("villagestate", '', 'raw'));

									for($i = 0; $i < count($this->state_items); $i++){
										$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
										<li>
											<input type="checkbox" class="state_checkbox" name="villagestate[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="check<?php echo $i;?>">
											<label for="check<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
										</li>
								<?php  } ?>
							</ul>
                    	</div>
		        	</div>
		        	<!-- END State -->

					<!-- START District -->
		        	<div role="tabpanel" class="tab-pane" id="district">
		        		<div id="districtspan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
		                				</div>
		                				|


		                				<a href="javascript:void(0);" onclick="district_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>
		                	<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
			                            <ul class="list1 districtlist" onchange="getSubDistrict(this.value)">
										</ul>
									</div>
	                			</div>
	                			<div class="col-md-2 padding-none">
	                				<div class="dist-icon-list">
	                					<div id="statecode">
	                					</div>
	                				</div>
	                			</div>
	                		</div>
		                </div>
		        	</div>
		        	<!-- END District -->

		        	<!-- START Sub District -->
		        	<div role="tabpanel" class="tab-pane" id="sub_district">
		        		<div id="subdistrictspan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('SUB_DISCTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="subdistrict_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<a href="javascript:void(0);" onclick="subdistrict_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>
		                	<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
			                            <ul class="list1 subdistrictlist" onchange="getdistrictattr(this.value)">
										</ul>
									</div>
	                			</div>
	                			<div class="col-md-2 padding-none">
	                				<div class="dist-icon-list">
	                					<div id="subdistrictcode">
	                					</div>
	                				</div>
	                			</div>
	                		</div>
		                </div>
		        	</div>
		        	<!-- END Sub District -->

					<!-- START Village -->
		        	<div role="tabpanel" class="tab-pane" id="village">
		        		<div id="villagespan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('VILLAGE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="village_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="village_allcheck" class="allcheck" id="village_allcheck">
		                				<label for="allcheck"></label>
		                				|
		                				<a href="javascript:void(0);" onclick="villages_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>
		                	<div class="scrollbar-inner">
		                		<ul class="list1 villagelist">
								</ul>
		                	</div>
		                </div>
		        	</div>
		        	<!-- END Village -->

					<!-- START Variable -->
		        	<div role="tabpanel" class="tab-pane" id="variables">
		        		<div id="variablespan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('Select Variable'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="variable_allcheck" class="allcheck" id="variable_allcheck">
		                				<label for="allcheck"></label>
		                				|
		                					<a href="#sort" onclick="variable_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>

	                	<div class="row">
							<div class="col-md-10">
								<div class="scrollbar-inner">
									<div id="statetotal"></div>
	        						<div id="variables"></div>
								</div>
							</div>
							<div class="col-md-2">
								<div id="variableshortcode"></div>
							</div>
						</div>
						 </div>
		        	</div>
		        	<!-- END Variable -->
		        </div>
		        <!-- END Tab panes -->
			</div>

			<div class="col-md-8">
				<div class="full-data-view">
					<div id="result_table" style="display:none">
						<div class="text contenttoggle1" id="textscroll">
							<div class="button-area text-right">
								<a href="#alldata" onclick="downloadAction();"><i class="fas fa-download"></i><?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?>
								</a>
							</div>

							<div class="collpsable1">
							</div>

							<div class="activecontent right-scroll-div tableview" >
								<div class="alldata contenttoggle2" id="jsscrollss alldata">
								</div>
								<?php echo "<br /><br />"; ?>
							</div>
						</div>
					</div>

					<!--Start GRAPH -->
					<div id="graph" style="display:none;">
						<div class="graphs contenttoggle1">
							<div class="row">
								<div class="col-md-4">
									<div class="chart_type">
										<label>Chart Type:</label>
										<select name="charttype" class="inputbox" id="chartype" onchange="loadCharts();">
											<option value="bar" selected>Bar Chart</option>
											<option value="line">line Chart</option>
											<option value="radar">Radar Chart</option>
										</select>

										<label>Show Legend</label>
										<input type="checkbox" id="enablelegend" name="enablelegend"  value="1" />
									</div>
								</div>
								<div class="col-md-3">
									<div id="graphfunction" class="white_content2 large-popup variable_add_popup" style="display: none;">
											<div class="divclose">
												<a id="closeextra" href="javascript:void(0);" onclick="document.getElementById('graphfunction').style.display='none';document.getElementById('fade').style.display='none';">
													<img src="media/system/images/closebox.jpeg" alt="X" />
												</a>
											</div>
											<div class="graphFilter">
												<ul class="nav">
													<li id="graphDistrict" class="active"><a data-toggle="tab" href="#graphDistrictTab" class="getmanagedata">District</a></li>
													<li id="graphVariables"><a data-toggle="tab" href="#graphVariablesTab" >Variables</a></li>
													
												</ul>
												<div class="tab-content">
												    <div id="graphDistrictTab" class="tab-pane fade in active">
												    	<div class="poptitle">
															District
														</div>
														<div id="variabelist"></div>
												    </div>

												    <div id="graphVariablesTab" class="tab-pane fade">
												    	<div class="poptitle">
															Variabels
														</div>
												     	<div id="cblist"></div>
												    </div>
												</div>
											</div>
										</div>
									<div class="button-area-2">
										<div id="exportchartbutton" class="exportbtn">
												<input type="button" id="graphfunction" onClick="document.getElementById('graphfunction').style.display='block';document.getElementById('fade').style.display='block'"  class="button" value="Graph Data">
											</div>
										<!-- <div id="showchart"><input type="button"  name="showchart"  class="button" value="Show Chart" /></div> -->
										<div id="exportchartbutton" class="exportbtn">
											<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
										</div>
									</div>
								</div>
							</div>
							<canvas id="myChart" style="width:100%; height:520px; background-color:#FFFFFF"></canvas>
							<!-- end of amcharts script -->
						</div>
					</div>
					<!-- END GRAPH -->

					<!--Start GIS -->
					<div id="gis" style="display:none;">
						<div class="gis contenttoggle1">
							<!-- <script src="components/com_mica/maps/OpenLayers.js"></script> -->
							<link rel="stylesheet" href="components/com_mica/maps/theme/default/style.css" type="text/css">
							<link rel="stylesheet" href="components/com_mica/maps/examples/style.css" type="text/css">
							<div id="map"  style="width: 100%; border: 1px solid #C5E1FC; height:535px; float:left; margin-bottom:25px">
								<table cellspacing="0" cellpadding="0" border="0" style="margin:0px 5px 5px 5px; width:100%;">
									<tbody>
										<tr>
											<td align="right">
												<?php  $fromthematic = $session->get('fromthematic'); ?>
												<a href="javascript:void(0);" id="fullscreen" class="frontbutton" <?php if($fromthematic==1){ ?> fromthematic="1" <?php }else{?> fromthematic="0"  <?php }?> >
													<input style="margin-right: 3px;" type="button" class="frontbutton" name="fullscreen" value="Full Screen">
												</a>

												<a href="javascript:void(0);" onclick="downloadMapPdf();" id="options" class="frontbutton" style="text-decoration: none;">
													<input style="margin-right: 3px;" type="button" class="frontbutton" name="Export" value="Export">
													<?php /*<img  title="Export Map" src="< ?php echo JURI::base();? >/components/com_mica/images/export.png" /> */ ?>
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- <script type="text/javascript" id="j1">
									var globalcorrdinates  = "";
									// var map             = new OpenLayers.Map("map");
									var oldzoom            = 0;
									var geo                = [];
									var dystate            = [];
									var geo                = [];
									//OpenLayers.ProxyHost = "http://192.168.5.159/cgi-bin/proxy.cgi?url=";
									// use a CQL parser for easy filter creation
									var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;
									OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
									var tomcaturl="<?php echo $this->tomcaturl; ?>";

									OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
										defaultHandlerOptions: {
											'single'         : true,
											'double'         : false,
											'pixelTolerance' : 0,
											'stopSingle'     : true,
											'stopDouble'     : false
										},
										initialize: function(options) {
											this.handlerOptions = OpenLayers.Util.extend(
												{}, this.defaultHandlerOptions
											);
											OpenLayers.Control.prototype.initialize.apply(
												this, arguments
											);
											this.handler = new OpenLayers.Handler.Click(
												this, {
													'click':this.getfeaturenfo,'dblclick': this.onDblclick
												}, this.handlerOptions
											);
										},
										onDblclick: function(e) {
											JQuery(".olPopup").css({"display":"none"});
											JQuery("#featurePopup_close").css({"display":"none"});
											//changeAttrOnZoom(map.zoom);
										},
										getfeaturenfo :function(e) {
											coordinates = e;
											var params = {
												REQUEST       : "GetFeatureInfo",
												projection    : "EPSG:4326",
												EXCEPTIONS    : "application/vnd.ogc.se_xml",
												BBOX          : map.getExtent().toBBOX(10),
												SERVICE       : "WMS",
												INFO_FORMAT   : 'text/html',
												QUERY_LAYERS  : selectlayer(map.zoom),
												FEATURE_COUNT : 6,
												Layers        : selectlayer(map.zoom),
												WIDTH         : map.size.w,
												HEIGHT        : map.size.h,
												X             : parseInt(e.xy.x),
												Y             : parseInt(e.xy.y),
												CQL_FILTER    : selectfilter(),
												srs           : map.layers[0].params.SRS
											};

											// handle the wms 1.3 vs wms 1.1 madness
											if(map.layers[0].params.VERSION == "1.3.0") {
												params.version = "1.3.0";
												params.i       = e.xy.x;
												params.j       = e.xy.y;
											} else {
												params.version = "1.1.1";
												params.y       = parseInt(e.xy.y);
												params.x       = parseInt(e.xy.x);
											}
											OpenLayers.loadURL("<?php echo $this->tomcaturl; ?>", params, this, setHTML, setHTML);
											var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
											//lonLat.transform(map.displayProjection,map.getProjectionObject());
											//map.setCenter(lonLat, map.zoom);
											map.panTo(lonLat);
											//map.addControl(ovControl);
											//OpenLayers.Event.stop(e);
										}
									});

									function setHTML(response){
										changeAttrOnZoom(map.zoom,response);
									};

									function selectfilter(){
										if(javazoom==5 || javazoom==6 ){
											return stateCqlFilter();
										}else if(javazoom==7){
											return districtCqlFilter();
										}else  if(javazoom==8){
											return urbanCqlFilter();
										}else{
											return cityCqlFilter();
										}
									}

									function selectlayer(cuurzoom){
										if(javazoom==5 || javazoom==6){
											return "rail_state";
										}else if(javazoom==7){
											return "india_information";
										}else  if(javazoom==8){
											return "jos_mica_urban_agglomeration";
										}else{
											return "my_table";
										}
									}

									var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
									var options = {
										controls      : [],
										maxExtent     : bounds,
										projection    : "EPSG:4326",
										maxResolution : 'auto',
										zoom          :<?php echo $this->zoom;?>,
										units         : 'degrees'
									};

									map = new OpenLayers.Map('map', options);
									var land = new OpenLayers.Layer.WMS("State Boundaries",
										"<?php echo $this->tomcaturl; ?>",
										{
											Layer       : 'india:rail_state',
											transparent : true,
											format      : 'image/png',
											CQL_FILTER  : stateCqlFilter(),
											SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/rail_state.sld'; ?>"
										},
										{
											isBaseLayer: false
										}
									);

									var opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
										"<?php echo $this->tomcaturl; ?>",
										{
											Layers           : 'india:rail_state',
											transparent      : false,
											format           : 'image/png',
											styles           : "dummy_state",
											transitionEffect : 'resize'
										},
										{
											isBaseLayer: true
										}
									);

									var districts = new OpenLayers.Layer.WMS("districts",
										"<?php echo $this->tomcaturl; ?>",
										{
											Layer       : 'india:india_information',
											transparent : true,
											format      : 'image/png',
											CQL_FILTER  : districtCqlFilter(),
											SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/india_information.sld'; ?>"
										},
										{
											isBaseLayer: false
										},
										{
											transitionEffect: 'resize'
										}
									);

									var cities = new OpenLayers.Layer.WMS("Cities",
										"<?php echo $this->tomcaturl; ?>",
										{
											Layer       : 'india:my_table',
											transparent : true,
											format      : 'image/png',
											CQL_FILTER  : cityCqlFilter(),
											SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/my_table.sld'; ?>"
										},
										{
											isBaseLayer : false
										},
										{
											transitionEffect : 'resize'
										}
									);

									var urban = new OpenLayers.Layer.WMS("Urban",
										"<?php echo $this->tomcaturl; ?>",
										{
											Layer       : 'india:jos_mica_urban_agglomeration',
											transparent : true,
											format      : 'image/png',
											CQL_FILTER  : urbanCqlFilter(),
											SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/jos_mica_urban_agglomeration.sld'; ?>"
										},
										{
											isBaseLayer: false
										},
										{
											transitionEffect: 'resize'
										}
									);

									if(map.zoom==5 || map.zoom==6 ){
										map.addLayers([land]);
									}else if(map.zoom==7){
										//map.removeLayer(land,districts);
										//map.addLayers([districts,land]);
										map.addLayers([districts]);
										//land.setVisibility(false);
									}else if(map.zoom==8){
										//map.removeLayer(land,cities);
										//map.addLayers([cities,land]);
										map.addLayers([urban]);
										//land.setVisibility(false);
										// districts.setVisibility(false);
									}else{
										map.addLayers([cities]);
										//land.setVisibility(false);
									}

									map.div.oncontextmenu = function noContextMenu(e) {
										if (OpenLayers.Event.isRightClick(e)){
											displaymenu(e);
										}
										// return false; //cancel the right click of brower
									};

									//map.addControl(exportMapControl);
										map.addControl(new OpenLayers.Control.PanZoomBar({
										position : new OpenLayers.Pixel(5, 15)
									}));
									map.addControl(new OpenLayers.Control.Navigation({
										dragPanOptions: {enableKinetic: true}
									}));

								   	//map.addControl(new OpenLayers.Control.Scale($('scale')));
									map.addControl(new OpenLayers.Control.Attribution());
									//map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
									//map.addControl(new OpenLayers.Control.LayerSwitcher());
									//alert(bounds);

									var click = new OpenLayers.Control.Click();
									map.addControl(click);
									click.activate();
									map.zoomTo(<?php echo $this->zoom;?>);

									<?php  if($this->geometry != "0" && $this->geometry != "") { ?>
										var format    = new OpenLayers.Format.WKT();
										var feature   = format.read('<?php echo $this->geometry;?>');
										var homepoint = feature.geometry.getCentroid();
									<?php } ?>

									map.addLayers([opverviewland]);
									//if (!map.getCenter(bounds))
										//map.zoomToMaxExtent();
									map.zoomToExtent(bounds);
									//map.zoomToMaxExtent();

									function onPopupClose(evt) {
										// 'this' is the popup.
										JQuery("#featurePopup").remove();
									}

									function displaymenu(e){
										//$("#menupopup").remove();
										//var OuterDiv=$('<div  />');
										//OuterDiv.append($("#menu").html());
										//OuterDiv.append($("#menu").html());
										//console.log(e);
										//OuterDiv.css({"z-index":"100000","left":e.screenX,"top":e.screenY,"display":"block"});
										//console.log({e.layerX,e.layerY});

										// popup = new OpenLayers.Popup("menupopup",map.getLonLatFromPixel(new OpenLayers.Pixel({e.layerX,e.layerY})),new OpenLayers.Size(200,300),$("#menu").html(), false);
										//	map.popup = popup;
										//evt.feature = feature;
										//	map.addPopup(popup);
									}

									function onFeatureSelect(response)
									{
										response = response.trim("");
										//console.log("response=> "+response);
										var getsegment=response.split("OGR_FID:");
										console.log("getsegment=> "+getsegment);
										var id;

										if(typeof(getsegment[1])=="undefined")
										{
											return false;
										}
										else
										{
											id=getsegment[1].trim();
											//alert(getsegment[3]);
											if(typeof(getsegment[2])!="undefined" ){
											  if(getsegment[2]==362 || getsegment[2]==344 || getsegment[2]==205 || getsegment[2]==520 || getsegment[2]==210 || getsegment[2]==211 || getsegment[2]==206)
													id = getsegment[2].trim();
											   if(getsegment[3]==209)
															  id=getsegment[3].trim();
												if(getsegment[4]==209)
															  id=getsegment[4].trim();
												if(getsegment[3]==207)
															  id=getsegment[3].trim();
												if(getsegment[1]==205)
															  id=getsegment[1].trim();
												if(getsegment[1]==206 && getsegment[2]==207 && getsegment[3]==212)
															  id=getsegment[2].trim();
											}
										}

										id = parseInt(id);
										JQuery(".olPopup").css({"display":"none"});

										JQuery.ajax({
											url     : "index.php?option=com_mica&task=showresults.popupAttributes&id="+id+"&zoom="+javazoom,
											method  : 'GET',
											success : function(data){
												popup = new OpenLayers.Popup.FramedCloud("featurePopup",
													map.getLonLatFromPixel(new OpenLayers.Pixel(coordinates.xy.x,coordinates.xy.y)),
													new OpenLayers.Size(300,150),
													data,null ,true, onPopupClose
												);
												map.popup = popup;
												//evt.feature = feature;
												map.addPopup(popup);
											}
										});
									}

									function onFeatureUnselect(evt) {
										feature = evt.feature;
										if (feature.popup) {
											popup.feature = null;
											map.removePopup(feature.popup);
											feature.popup.destroy();
											feature.popup = null;
										}
									}

									<?php  echo "var javazoom= ".$this->zoom.";";?>
									<?php $curlonglat = $app->input->get('longlat', '', 'raw');
									if($curlonglat =="") {
										echo "var curlonglat= '77.941406518221,21.676757633686';";
									}else{
										echo "var curlonglat= '".$app->input->get('longlat','77.941406518221,21.676757633686', 'raw')."';";
									} ?>

									//alert(curlonglat);
									var longlatsegment=curlonglat.split(",");

								 	<?php if($this->geometry!="0") {?>
									   	map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
										map.zoomTo(6);
									<?php } else {?>
									   	map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
										map.zoomTo(4);
									<?php } ?>
									//map.events.register("click", map, regionclick);

									function stateCqlFilter(){
										<?php if($this->state!="all") {
											//$stateArray=explode(",",JRequest::GetVar('state'));
											$statetojavascript=str_replace(",","','",$this->state);
											echo " return \"name in ('".$statetojavascript."')\";";
										} ?>
									}

									function districtCqlFilter() {
										<?php if($this->district != "all" && $this->district != "") {
											//$stateArray=explode(",",JRequest::GetVar('district'));
											if(strlen($this->UnselectedDistrict) > $this->district) {
												$statetojavascript = $this->district;
												//if(substr_count($statetojavascript,"362")){
													//$statetojavascript=$statetojavascript.",363";
													//echo "<pre>Inside";
											 	//}
											 	//echo $statetojavascript;exit;
												$statetojavascript = str_replace(",", "','", $statetojavascript);
												echo " return \"".$this->districtsearchvariable." in ('".$statetojavascript."') \";";

											}else{
												$statetojavascript=$this->UnselectedDistrict;
												$statetojavascript=str_replace(",","','",$statetojavascript);
												echo " return \"".$this->districtsearchvariable." not in ('".$statetojavascript."') \";";
											}
										}else if($this->district == "all"){
											echo " return  \"state in ('".$statetojavascript."')\";";
										} ?>
									}

									function cityCqlFilter(){
										<?php  if($this->town!="all" && $this->town!="" ){
											//$stateArray=explode(",",JRequest::GetVar('town'));
											$statetojavascript = $this->town;
											$statetojavascript = str_replace(",","','",$statetojavascript);
											echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
										}

										if($this->town == "all" ){
											//$stateArray=explode(",",JRequest::GetVar('town'));
											echo " return  \"state in ('".$statetojavascript."')\";";
										} ?>
									}

									function urbanCqlFilter(){
										<?php  if($this->urban != "all" && $this->urban != ""){
											//$stateArray=explode(",",JRequest::GetVar('state'));
											$statetojavascript = $this->urban;
											$statetojavascript = str_replace(",","','",$statetojavascript);
											echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
										}

										if($this->urban == "all" ){
											//$stateArray=explode(",",JRequest::GetVar('town'));
											echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
										} ?>
									}

									function changeAttrOnZoom(javazoom,response){
										onFeatureSelect(response.responseText);
									}

									function reloadCustomAttr(){
										JQuery.ajax({
											url     : "index.php?option=com_mica&task=showresults.getCustomAttr",
											method  : 'GET',
											success : function(data){
												JQuery("#addcustomattribute").html(data);
											}
										});
									}

									JQuery(".delcustom").live("click",function(){
										var myid    = JQuery(this).attr('id');
										var segment = myid.split("_");
										JQuery.ajax({
											url     : "index.php?option=com_mica&task=showresults.deleteCustomAttr&id="+segment[2],
											method  : 'GET',
											success : function(data){
												JQuery("#"+myid).parent("tr").remove();
											}
										});
									});
								</script> -->
							</div>
						</div>
					</div>
					<!--Start GIS -->

					<!-- Start Matrix -->
					<div id="quartiles" class="matrix contenttoggle1">
						<div id="fullscreentable"></div>
					</div>
					<!-- End Matrix -->

					<!--Start Speed -->
					<div id="potentiometer" style="display:none;">
						<div class="speedometer contenttoggle1" >
							<div class="filter-section">
										<div style="display:none;" id="speedfiltershow">
											
										</div>
										<div class="sfilter">
											<div class="top-filter-sec">
												<div class="fl_title">
													<span class="filterbylabel"></span>
												</div>
												<div class="fl_type radio-list fl_right">
													<!-- start popup for potentiometer Filters-->
													<div id="variablefiltetr" style="display:none;" class="white_content2 large-popup">
														<div class="divclose">
														<a href="javascript:void(0);" data-me="variablefiltetr" class="closeme1">
														<img src="media/system/images/closebox.jpeg" alt="X">
														</a>
														
														</div>
														<div class="filterbylabel"></div>
														<div class="sfilter">
															<div>
																<div class="lft speed" style="display:none;">
																<div class="row">
																	<div class="col-sm-6">
																		<div id="speed_variable"></div>
																	</div>
																	<div class="col-sm-6">
																		<div id="speed_region"></div>
																	</div>
																</div>
																<div class="frontbutton ">
																	<input type="button" name="showspeed" id="showspeed" class="" value="Create">
																</div>
																</div>
															</div>
														</div>
													</div>
													<!-- end popup for potentiometer Filters-->
													<ul>	
														<li>
															<a href="javascript:void(0);" id="filter1" data-val="0"  class="filterspeed">Filter By  Layer</a>
															
														</li>
														<li>
															<a href="javascript:void(0);" id="filter2"  data-val="1"  class="filterspeed">Filter By  Variable</a>
															
														</li>
													</ul>


												</div>

											</div>
										</div>
										
									</div>
							<div id="spedometer_region" class="right-scroll-div spedometer_sec" style="width: 915px; overflow: auto;"></div>
						</div>
					</div>
					<!--End Speed -->

					<!-- START Right End Side Steps -->
					<div id="default3" class="default3 contenttoggle1">
						<p>Step 1 : Select State</p>
						<p>Step 2 : Select District of selected State</p>
						<p>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</p>
						<p>Step 4 : Select the Variables you want to view. (Multiple selection allowed)</p>
						<p>Step 5 : Select MPI (Multiple selection allowed),</p>
						<p>Step 6 : Select sector wise composite score. (Multiple selection allowed)
						Note : Mark ‘All’ in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</p>
						<div align="left">
							 <a href="javascript:void(0);" id="submitta" style="display:none;" class="actionbtn btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a>
						</div>
					</div>
					<!-- START Right End Side Steps -->

					<!--START default -->
					<div id="default" class="white_content2 alert-variable" >
						<div class="popup_alert">
							<div class="default contenttoggle1" >
								<div class="divclose">
									<a href="javascript:void(0);" onclick="document.getElementById('default').style.display='none';document.getElementById('fade').style.display='none';">
										<!-- <img src="media/system/images/closebox.jpeg" alt="X"> -->
									</a>
								</div>
								<div class="box-white">
									<p>Please select variables from each of the following dimensions to view a report. You can select from left panel or by clicking the links above.</p>
								</div>
								<ul class="list1">
									<li id="statetext"><i class="far fa-check-square"></i>state</li>
									<li id="districttext"><i class="far fa-check-square"></i>District</li>
									<li id="villageText"><i class="far fa-check-square"></i>Village</li>
									<li id="variabletext"><i class="far fa-check-square"></i>Variabel</li>
								</ul>

							    <div align="right">
							    	<a href="javascript:void(0);" id="applyChangesInitial" class="actionbtn btn-disabled btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply Changes</a>
							    </div>
							</div>
						</div>
					</div>
					<!--END default -->

					<!--start default -->
					<div id="default1" class="white_content2 alert-variable" style="display:none;">
						<div class="popup_alert">
							<div class="divclose">
								<a href="javascript:void(0);" onclick="document.getElementById('default1').style.display='none';document.getElementById('fade').style.display='none';">
									<!-- <img src="media/system/images/closebox.jpeg" alt="X"> -->
								</a>
							</div>
							<div class="box-white">
								<p>Selections have been modified. Click on "Apply Changes" at any time to refresh the report with the changes made. Otherwise, click on "Cancel" to go back to previous selections.</p>
							</div>
							<div align="right">
							 	<a href="javascript:void(0);" id="applyChanges" class="actionbtn btn-disabled btn-submit" disabled="disabled"><i class="fas fa-table"></i>Apply Changes</a>
							</div>
						</div>
					</div>
					<!-- END Right End Side Steps -->
				</div>
			</div>

			<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
				<input type="hidden" name="refeterview" value="villagefront" />
				<input type="hidden" name="option" 		value="com_mica" />
				<input type="hidden" name="zoom" id="zoom" value="6" />
				<input type="hidden" name="view" 		value="villageshowresults" />
				<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
				<input type="hidden" id="comparedata" 	value="0" />
				<input type="hidden" name="tempvillagejson" id="tempvillagejson" value="ronak"/>
				<input type="hidden" id="viewRecordPlanLimit" value="0" />
				<input type="hidden" id="downloadAllowedPlanLimit" value="0" />
			</div>

			<!-- custom variable calcultor start-->
			<div id="light" class="white_content2 large-popup" style="display: none;">
				<div class="divclose">
					<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
						<img src="media/system/images/closebox.jpeg" alt="X" />
					</a>
				</div>

				<script type="text/javascript">
					var lastchar = '';
					function moverightarea(){
						var val      = document.getElementById('custom_attribute').innerHTML;

						var selected = 0;
						for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
							var avail_attribute = document.micaform1.avail_attribute;
							if(document.micaform1.avail_attribute[i].selected){
								selected = 1;
								if(lastchar=='attr'){
									alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
								}else{
									document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
									lastchar = 'attr';
								}
							}
						}
						if(selected==0){
							alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
						}
					}
					$("#copyBtn").click(function(){
					    var selected = $("#selectBox").val();
					    $("#output").append("\n * " + selected);
					});

					function addplussign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"+";
							lastchar = 'opr';
						}
					}

					function addminussign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"-";
							lastchar = 'opr';
						}
					}

					function addmultiplysign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"*";
							lastchar = 'opr';
						}
					}

					function adddivisionsign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"/";
							lastchar = 'opr';
						}
					}

					function addLeftBraket(){
						var val = document.getElementById('custom_attribute').innerHTML;
						document.getElementById('custom_attribute').innerHTML = val+"(";
						checkIncompleteFormula();
					}

					function addRightBraket(){
						var val = document.getElementById('custom_attribute').innerHTML;
						document.getElementById('custom_attribute').innerHTML = val+")";
						checkIncompleteFormula();
					}

					function addPersentage(){
						var val = document.getElementById('custom_attribute').innerHTML;
						//if(lastchar=='opr'){
							//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						//}else{
							document.getElementById('custom_attribute').innerHTML = val+"%";
							lastchar = 'opr';
						//}
					}

					function addNumeric(){
						var val       = document.getElementById('custom_attribute').value;
						var customval = document.getElementById('custom_numeric').value;
						document.getElementById('custom_attribute').innerHTML = val+customval;
						lastchar = '';
					}

					function checkIncompleteFormula(){
						var str   = document.getElementById('custom_attribute').innerHTML;
						var left  = str.split("(");
						var right = str.split(")");

						if(left.length==right.length){
							document.getElementById('custom_attribute').style.color="black";
						}else{
							document.getElementById('custom_attribute').style.color="red";
						}
					}

					function Click(val1){
						var val = document.getElementById('custom_attribute').innerHTML;
						document.getElementById('custom_attribute').innerHTML = val+val1;
						checkIncompleteFormula();
					}

					function checkfilledvalue(){

						var tmp1 = document.getElementById('custom_attribute').innerHTML;
						var tmp2 = document.getElementById('new_name').value;
						var flg  = 0;
						var len1 = parseInt(tmp1.length) - 1;
						var len2 = parseInt(tmp2.length) - 1;
						if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
							alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
						}else if(lastchar=='opr' && flg == 0){
							alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
						}

						if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){

						}else{
							if(!validateFormula_v2()){
								return false;
							}
							addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
						}
					}

					function addCustomAttr(attrname,attributevale){

								JQuery.ajax({
								url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
								method  : 'GET',
								success : function(data)
								{


									//getAttribute_v2(5);
									$("#light").hide();
									getDataNew();

									return;
								}

							});
						}



					function undofield(){

						document.getElementById('custom_attribute').innerHTML = '';
						lastchar = '';
					}
					function ClearFields()
					{

				     	document.getElementById("custom_attribute").value = "";
						document.getElementById("custom_numeric").value = "";
					}

					$(function(){
					    $('select').change(function(){
					        $that = $(this);
					        $('textarea').val(function(){
					            return $(this).prop('defaultValue') + ' '+$that.val();
					        });
					    });
					});

					JQuery("#editvariable").live("click",function(){
						JQuery('#save').attr("id","updatecustom");
						JQuery('#updatecustom').attr("onclick","javascript:void(0)");
						is_updatecustom=1;
						JQuery('#updatecustom').attr("value","Update");
					});

					JQuery(".customedit").live('click',function(){
						if(is_updatecustom == 1){
							//alert(JQuery(this).find(":selected").text());
							JQuery('#new_name').val(JQuery(this).find(":selected").text());
							JQuery('#new_name').attr('disabled', true);
							oldattrval=JQuery(this).find(":selected").val();
							JQuery('#oldattrval').val(oldattrval);
							JQuery('textarea#custom_attribute').text("");
							lastchar = '';
							moverightarea();
						}
					});
				</script>

				<div id="light">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#home">Custom</a></li>
					    <li><a data-toggle="tab" href="#menu1">Manage</a></li>
					    <li><a data-toggle="tab" href="#menu2">Add New</a></li>
					</ul>

				  	<div class="tab-content">
					    <div id="home" class="tab-pane fade in active">
					    	<div id="CustomVariable">
					      	</div>
					    </div>

					    <div id="menu1" class="tab-pane fade">
					     	<div id="customvariables">
								<div id="lightmanagevariable">
									<div class="poptitle">
										<?php echo JTEXT::_('MANAGE_CUSTOM');?>
									</div>

									<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
										<thead>
											<tr>
												<th align="center" width="10%">Use</th>
												<th align="center" width="45%">Variable</th>
												<th align="center" width="45%">Action</th>
											</tr>
										</thead>
										<?php
										$i=0;

										$finalarray[1] =array_unique($finalarray[1]);
										foreach($finalarray[1] as $eacharray){
											if(in_array($eacharray,$finalarray[0])){
												$checked  = "checked";
												$selected = "Remove";
												$action   = "";
											}else{
												$checked  = "";
												$selected = "Select";
												$action   = "checked";
											}

											if(($i%2)==0){
												$clear = "clear";
											}else{
												$clear ="";
											}
											$namevalue = explode(":",$eacharray);


											echo '
											<tr>
										    <td align="center" width="10%">
											 <input type="checkbox" name="variable1"  class="dcv_checkbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
											 <label></label>
												</td>
												<td align="center" class="customdelete" width="45%">'.$namevalue[0].'</td>
												<td align="center" width="45%">
													<a href="javascript:void(0);" style="float:left;padding-left:5px;">
														<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
													</a>
													<a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable	\').click();">
														<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
													</a>
												</td>
											</tr>';
											$i++;
										} ?>
									</table>

									<div class="readon frontbutton">
										<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
									</div>
								</div>
						  	</div>
					    </div>

						<div id="menu2" class="tab-pane fade">
							<div id="light" class="white_content2 large-popup" style="display: none;">
								<div class="divclose">
									<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
										<img src="media/system/images/closebox.jpeg" alt="X" />
									</a>
								</div>

								<script type="text/javascript">
									var lastchar = '';
									function moverightarea(){
										var val      = document.getElementById('custom_attribute').innerHTML;

										var selected = 0;
										for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
											var avail_attribute = document.micaform1.avail_attribute;
											if(document.micaform1.avail_attribute[i].selected){
												selected = 1;
												if(lastchar=='attr'){
													alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
												}else{
													document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
													lastchar = 'attr';
												}
											}
										}
										if(selected==0){
											alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
										}
									}
									$("#copyBtn").click(function(){
									    var selected = $("#selectBox").val();
									    $("#output").append("\n * " + selected);
									});

									function addplussign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"+";
											lastchar = 'opr';
										}
									}

									function addminussign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"-";
											lastchar = 'opr';
										}
									}

									function addmultiplysign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"*";
											lastchar = 'opr';
										}
									}

									function adddivisionsign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"/";
											lastchar = 'opr';
										}
									}

									function addLeftBraket(){
										var val = document.getElementById('custom_attribute').innerHTML;
										document.getElementById('custom_attribute').innerHTML = val+"(";
										checkIncompleteFormula();
									}

									function addRightBraket(){
										var val = document.getElementById('custom_attribute').innerHTML;
										document.getElementById('custom_attribute').innerHTML = val+")";
										checkIncompleteFormula();
									}

									function addPersentage(){
										var val = document.getElementById('custom_attribute').innerHTML;
										//if(lastchar=='opr'){
											//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										//}else{
											document.getElementById('custom_attribute').innerHTML = val+"%";
											lastchar = 'opr';
										//}
									}

									function addNumeric(){
										var val       = document.getElementById('custom_attribute').value;
										var customval = document.getElementById('custom_numeric').value;
										document.getElementById('custom_attribute').innerHTML = val+customval;
										lastchar = '';
									}

									function checkIncompleteFormula(){
										var str   = document.getElementById('custom_attribute').innerHTML;
										var left  = str.split("(");
										var right = str.split(")");

										if(left.length==right.length){
											document.getElementById('custom_attribute').style.color="black";
										}else{
											document.getElementById('custom_attribute').style.color="red";
										}
									}

									function Click(val1){
										var val = document.getElementById('custom_attribute').innerHTML;
										document.getElementById('custom_attribute').innerHTML = val+val1;
										checkIncompleteFormula();
									}

									function checkfilledvalue(){

										var tmp1 = document.getElementById('custom_attribute').innerHTML;
										var tmp2 = document.getElementById('new_name').value;

										var flg  = 0;
										var len1 = parseInt(tmp1.length) - 1;
										var len2 = parseInt(tmp2.length) - 1;
										if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
											alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
										}else if(lastchar=='opr' && flg == 0){
											alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
										}

										if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){

										}else{
											if(!validateFormula_v2()){
												return false;
											}
											addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
										}
									}

									function addCustomAttr(attrname,attributevale){

												JQuery.ajax({
												url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
												method  : 'GET',
												success : function(data)
												{


													//getAttribute_v2(5);
													$("#light").hide();
													getDataNew();

													return;
												}

											});
										}



									function undofield(){

										document.getElementById('custom_attribute').innerHTML = '';
										lastchar = '';
									}
									function ClearFields()
									{

								     	document.getElementById("custom_attribute").value = "";
										document.getElementById("custom_numeric").value = "";
									}

									$(function(){
									    $('select').change(function(){
									        $that = $(this);
									        $('textarea').val(function(){
									            return $(this).prop('defaultValue') + ' '+$that.val();
									        });
									    });
									});

									JQuery("#editvariable").live("click",function(){
										JQuery('#save').attr("id","updatecustom");
										JQuery('#updatecustom').attr("onclick","javascript:void(0)");
										is_updatecustom=1;
										JQuery('#updatecustom').attr("value","Update");
									});

									JQuery(".customedit").live('click',function(){
										if(is_updatecustom == 1){
											//alert(JQuery(this).find(":selected").text());
											JQuery('#new_name').val(JQuery(this).find(":selected").text());
											JQuery('#new_name').attr('disabled', true);
											oldattrval=JQuery(this).find(":selected").val();
											JQuery('#oldattrval').val(oldattrval);
											JQuery('textarea#custom_attribute').text("");
											lastchar = '';
											moverightarea();
										}
									});
								</script>

								<div id="light">
									<ul class="nav nav-tabs">
									    <li class="active"><a data-toggle="tab" href="#home">Custom</a></li>
									    <li><a data-toggle="tab" href="#menu1">Manage</a></li>
									    <li><a data-toggle="tab" href="#menu2">Add New</a></li>
									</ul>

								  	<div class="tab-content">
									    <div id="home" class="tab-pane fade in active">
									    	<div id="CustomVariable">
									      	</div>
									    </div>

									    <div id="menu1" class="tab-pane fade">
									     	<div id="customvariables">
												<div id="lightmanagevariable">
													<div class="poptitle">
														<?php echo JTEXT::_('MANAGE_CUSTOM');?>
													</div>

													<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
														<thead>
															<tr>
																<th align="center" width="10%">Use</th>
																<th align="center" width="45%">Variable</th>
																<th align="center" width="45%">Action</th>
															</tr>
														</thead>
														<?php
														$i = 0;

														$finalarray[1] =array_unique($finalarray[1]);
														foreach($finalarray[1] as $eacharray){
															if(in_array($eacharray,$finalarray[0])){
																$checked  = "checked";
																$selected = "Remove";
																$action   = "";
															}else{
																$checked  = "";
																$selected = "Select";
																$action   = "checked";
															}

															if(($i%2)==0){
																$clear = "clear";
															}else{
																$clear ="";
															}
															$namevalue = explode(":",$eacharray);

															echo '
															<tr>
														    <td align="center" width="10%">
															 <input type="checkbox" name="variable1"  class="dcv_checkbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
															 <label></label>
																</td>
																<td align="center" class="customdelete" width="45%">'.$namevalue[0].'</td>
																<td align="center" width="45%">
																	<a href="javascript:void(0);" style="float:left;padding-left:5px;">
																		<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
																	</a>
																	<a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable	\').click();">
																		<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
																	</a>
																</td>
															</tr>';
															$i++;
														} ?>
													</table>

													<div class="readon frontbutton">
														<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
													</div>
												</div>
										  	</div>
									    </div>

										<div id="menu2" class="tab-pane fade">
									       	<div class="row">
									       	 	<div class="col-md-12">
								      				<h5 class="text-danger"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></h5>
								      			</div>

									       	 	<div class="col-md-12"><b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox" style="width: 90%!important">
									       	 	</div>

								      			<div class="col-md-12">
													<label><b></b>Select Variable </b></label>
													<select name="avail_attribute" id="avail_attribute" class="avial_select" style="width:80%!important;">
														<optgroup label="Default Variable" class="defaultvariable">
															<?php
															$attr= $this->themeticattribute;//themeticattribute
															$eachattr=explode(",",$attr);
															foreach($eachattr as $eachattrs){
																echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
															} ?>
														</optgroup>
													</select>
								      			</div>

								      			<div class="col-md-12">
							      					<table  border="0" align="center" cellpadding="0" cellspacing="0" class="cal_t" style="background: #dbdbdd">
														<tr>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="btn btn-warning btn-sm btn-block"  value="+" onclick="addplussign();">
															</td>
														   	<td width="60">
														   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="btn btn-warning btn-sm btn-block"  value="-" onclick="addminussign();">
														   	</td>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="btn btn-warning btn-sm btn-block"  value="*" onclick="addmultiplysign();">
															</td>
														   	<td width="60">
														   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="btn btn-warning btn-sm btn-block"  value="/" onclick="adddivisionsign();">
														   	</td>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="btn btn-warning btn-sm btn-block"  value="%" onclick="addPersentage();">
															</td>

															<td width="60">
																<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="btn btn-warning btn-sm btn-block">
															</td>
														</tr>
														<tr>
															<td width="60">
																<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="btn btn-warning btn-sm btn-block">
															</td>
														   	<td width="60">
														   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="btn btn-warning btn-sm btn-block">
														   	</td>
															<td width="60">
																<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="btn btn-warning btn-sm btn-block">
															</td>
														 	<td width="60">
														 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="btn btn-warning btn-sm btn-block">
														 	</td>

															<td width="60">
																<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="btn btn-warning btn-sm btn-block">
															</td>
															<td width="60">
																<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="btn btn-warning btn-sm btn-block">
															</td>
														</tr>
														<tr>
															<td width="60">
																<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="btn btn-warning btn-sm btn-block">
															</td>
															<td width="60">
																<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="btn btn-warning btn-sm btn-block">
															</td>
															<td width="60">
																<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="btn btn-warning btn-sm btn-block">
															</td>

															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"  value="(" onclick="addLeftBraket();">
															</td>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"   value=")" onclick="addRightBraket();">
															</td>
															<td></td>
														</tr>
													</table>
											   	</div>

								      			<div class="col-md-12">
			      									<label><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></label>
													<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox" style="width:65%!important;">
													<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="btn btn-success" value="Add Value" onclick="addNumeric();">
						      					</div>

								      			<div class="col-md-12">
													<textarea class="form-control" name="custom_attribute" id="custom_attribute" rows="3"  readonly="readonly" style="width:100%!important;"></textarea>
								      			</div>

									      		<div class="col-md-12">
									      			<div class="col-md-6">
														<input type="button" name="save" id="save" class="btn btn-primary" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
													</div>
													<div class="col-md-6">
														<input type="button" name="clear" id="clear" class="btn btn-danger" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="ClearFields();">
													</div>
									      		</div>
									       	</div>
									    </div>
								  	</div>
								</div>

						       	<div class="row">
						       	 	<div class="col-md-12">
					      				<h5 class="text-danger"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></h5>
					      			</div>
						       	 	<div class="col-md-12"><b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox" style="width: 90%!important"></div>

					      			<div class="col-md-12">
										<label><b></b>Select Variable </b></label>
										<select name="avail_attribute" id="avail_attribute" class="avial_select" style="width:80%!important;">
											<optgroup label="Default Variable" class="defaultvariable">
												<?php
												$attr= $this->themeticattribute;//themeticattribute
												$eachattr=explode(",",$attr);
												foreach($eachattr as $eachattrs){
													echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
												} ?>
											</optgroup>
										</select>
					      			</div>

					      			<div class="col-md-12">
				      					<table  border="0" align="center" cellpadding="0" cellspacing="0" class="cal_t" style="background: #dbdbdd">
											<tr>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="btn btn-warning btn-sm btn-block"  value="+" onclick="addplussign();">
												</td>
											   	<td width="60">
											   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="btn btn-warning btn-sm btn-block"  value="-" onclick="addminussign();">
											   	</td>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="btn btn-warning btn-sm btn-block"  value="*" onclick="addmultiplysign();">
												</td>
											   	<td width="60">
											   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="btn btn-warning btn-sm btn-block"  value="/" onclick="adddivisionsign();">
											   	</td>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="btn btn-warning btn-sm btn-block"  value="%" onclick="addPersentage();">
												</td>

												<td width="60">
													<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="btn btn-warning btn-sm btn-block">
												</td>
											</tr>
											<tr>
												<td width="60">
													<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="btn btn-warning btn-sm btn-block">
												</td>
											   	<td width="60">
											   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="btn btn-warning btn-sm btn-block">
											   	</td>
												<td width="60">
													<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="btn btn-warning btn-sm btn-block">
												</td>
											 	<td width="60">
											 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="btn btn-warning btn-sm btn-block">
											 	</td>

												<td width="60">
													<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="btn btn-warning btn-sm btn-block">
												</td>
												<td width="60">
													<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="btn btn-warning btn-sm btn-block">
												</td>
											</tr>
											<tr>
												<td width="60">
													<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="btn btn-warning btn-sm btn-block">
												</td>
												<td width="60">
													<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="btn btn-warning btn-sm btn-block">
												</td>
												<td width="60">
													<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="btn btn-warning btn-sm btn-block">
												</td>

												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"  value="(" onclick="addLeftBraket();">
												</td>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"   value=")" onclick="addRightBraket();">
												</td>
												<td></td>
											</tr>
										</table>
									</div>

					      			<div class="col-md-12">
										<label><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></label>
										<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox" style="width:65%!important;">
										<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="btn btn-success" value="Add Value" onclick="addNumeric();">
			      					</div>

					      			<div class="col-md-12">
										<textarea class="form-control" name="custom_attribute" id="custom_attribute" rows="3"  readonly="readonly" style="width:100%!important;"></textarea>
					      			</div>

						      		<div class="col-md-12">
						      			<div class="col-md-6">
											<input type="button" name="save" id="save" class="btn btn-primary" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
										</div>
										<div class="col-md-6">
											<input type="button" name="clear" id="clear" class="btn btn-danger" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="ClearFields();">
										</div>
						      		</div>
						       	</div>
					   		</div>
				  		</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="fade" class="black_overlay" style="display:none"></div>
<script type="text/javascript">
	
	$(document).on('click', '#district_tab', function(){
		JQuery(document).ready(function() {
  	  		JQuery('.slider').slick({
		  		dots: false,
		    	vertical: true,
		    	slidesToShow: 11,
		    	slidesToScroll: 1,
		    	verticalSwiping: true,
		    	arrrow:true,
		  	});
		});
	});

	/*-------------export -----------*/
document.getElementById('export').addEventListener("click", downloadPDF);
function downloadPDF()
{

	var canvas = document.querySelector('#myChart');
	var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
	var doc = new jsPDF('landscape');
	doc.setFontSize(20);
	doc.text(15, 15, "Cool Chart");
	doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
	doc.save('canvas.pdf');
}
/*-------------export end-----------*/

	//$(document).ready(function() {

		/*$('#applyChanges').click(function() {
		    $("#tabledata").css("pointerEvents", "auto");
		    $("#tabledata").css("cursor", "pointer");
		    $("#loadchart").css("pointerEvents", "auto");
		    $("#loadchart").css("cursor", "pointer");
		    $("#matrix").css("pointerEvents", "auto");
		    $("#matrix").css("cursor", "pointer");
		    $("#pmdata").css("pointerEvents", "auto");
		    $("#pmdata").css("cursor", "pointer");
		    $("#gisdata").css("pointerEvents", "auto");
		    $("#gisdata").css("cursor", "pointer");
		});*/
   // });
/*	JQuery("#showspeed").live("click",function(){
		var speedvar    =new Array();
		var speedregion =new Array();
		if (typeof(JQuery("#speed_variable").attr("multiple")) == "string") {
			JQuery("#speed_variable").each(function(){
				speedvar.push(encodeURIComponent(JQuery(this).val()));
			});
			speedregion.push(encodeURIComponent(JQuery("#speed_region").val()));
		}
		else {
			speedvar.push(encodeURIComponent(JQuery("#speed_variable").val()));
			JQuery("#speed_region").each(function(){
				speedregion.push(encodeURIComponent(JQuery(this).val()));
			});
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=villageshowresults.getsmeter&region="+speedregion+"&speedvar="+speedvar+"&filterspeed="+filterspeed_radio,
			type    : 'GET',
			success : function(data){
				JQuery("#spedometer_region").html("");
				JQuery("#spedometer_region").html(data);
				initspeed();
				JQuery("#speedfiltershow").css({"display":"block"});
				JQuery(".sfilter").css({"display":"none"});
				JQuery("#spedometer_region").css({"width":"915px"});
				JQuery("#spedometer_region").css({"overflow":"auto"});
			}
		});
	});*/


					
</script>