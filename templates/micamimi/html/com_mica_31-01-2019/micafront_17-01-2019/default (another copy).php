<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$app = JFactory::getApplication('site');

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
// $doc->addStyleSheet(JURI::base().'components/com_mica/js/newjs/jquery-ui.multiselect.widget.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');



$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');



$itemid        = 188;
$type1         = $app->input->get('m_type', '', 'raw');
$m_type_rating = $app->input->get("m_type_rating", '', 'raw');
$composite     = $app->input->get("composite", '', 'raw');
/*echo "<pre />";print_r($composite);
echo "<pre />";print_r($this->composite_attr);exit;*/
?>



<? /*<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script>*/?>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
<? /*<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>
*/?>
<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.cookie.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/swfobject.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amcharts.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amfallback.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<!-- <script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.multiselect.widget.js"></script> -->
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/maps/OpenLayers.js"></script>

<script type="text/javascript">
	//var choosenjq = jQuery.noConflict();
	var JQuery = jQuery.noConflict();
	var $ = jQuery.noConflict();
	// choosenjq(document).ready(function(){
	// 	choosenjq	("select").multiselect({
	// 			showCheckbox: true,
	// 			hideSelect:true,
	// 			isMultiple:true,
	// 			isOpen: false,
	// 			filterLabelText:"Select State"
	// 	});
	// });
	/*function tabclickevent()
	{
		var elem = choosenjq('ul.nav.nav-tabs > li.active > a').attr('aria-controls');
		var newelemtrigger ='#tag'+elem;
		alert(newelemtrigger);
		//choosenjq(newelemtrigger+' button.ui-multiselect--display').trigger('click');
	}
	choosenjq(document).ready(function(){
		tabclickevent();
		choosenjq('ul.nav.nav-tabs > li > a').click(function(){
			tabclickevent();
		})
	});*/

</script>


<style type="text/css">
is-multiple {display:block}

</style>

<? /*<script src="<?php echo JURI::base()?>components/com_mica/js/field.js"></script> */?>
<script type="text/javascript">
	var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var preselected       = '<?php echo $app->input->get("district", "", "raw"); ?>';
	var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type = '<?php echo $app->input->get("m_type", "", "raw"); ?>';

	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	jQuery(function() {
		jQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		jQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});
	jQuery(document).ready(function(){ });

	function checkAll(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",false);
		});
	}
</script>

<script src="templates/micamimi/js/jquery.scrollbar.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();
    });
</script>

<div class="row-fluid top-row">
	<div class="col-md-2">
		<h1 class="title-explore"><?php echo JText::_('SELECT_CR_MSG');?></h1>
	</div>
	<div class="col-md-10 text-right">
		<div class="sorter-tab">
			<ul>
				<li>
					<a href="javascript:void(0);" id="submit" class="actionbtn" onclick="getData();"><i class="fas fa-table"></i><?php echo JText::_('SHOW_DATA'); ?></a></li>
				</li>
				<li>
					<a href="javascript:void(0);" id="comparedata" class="actionbtn"><i class="fas fa-table"></i><?php echo JText::_('TABEL'); ?></a></li>
				</li>
				<li>
					<a href="javascript:void(0);" id="summary">Summary</a>
				</li>
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="graphdata" class="actionbtn"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="qtdata" class="actionbtn"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li>
			</ul>
		</div>
		<div class="worspacce-edit">
			My workspace <a href="<?php echo $this->workspacelinkdiv; ?>"><i class="fas fa-pencil-alt"></i></a>
			<h2>DEFAULT</h2>
		</div>
	</div>
</div>

<div class="explore-data">
	<!-- <form name="adminForm" id="micaform" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="POST"> -->
	<form name="adminForm" id="micaform" action="" method="POST">
		<div class="row">
			<div class="col-md-4">
	            <ul class="nav nav-tabs" role="tablist">
	                <li role="presentation" class="active">
	                	<a href="#tabstate" aria-controls="state" role="tab" data-toggle="tab">
	                		<em class="icon-state"></em>State
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabdistrict" aria-controls="district" role="tab" data-toggle="tab">
	                		<em class="icon-district"></em>District
	                	</a>
	                </li>
	                <!-- <li role="presentation">
	                	<a href="#tabvillage" aria-controls="village" role="tab" data-toggle="tab">
	                		<em class="icon-district"></em>Village
	                	</a>
	                </li> -->
	                <li role="presentation">
	                	<a href="#tabtype" aria-controls="type" role="tab" data-toggle="tab">
	                		<em class="icon-type"></em>Type
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabvariables" aria-controls="variables" role="tab" data-toggle="tab">
	                		<em class="icon-variables"></em>Variables
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabmpi" aria-controls="mpi" role="tab" data-toggle="tab">
	                		<em class="icon-mpi"></em>MPI
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabswcs" aria-controls="swcs" role="tab" data-toggle="tab">
	                		<em class="icon-swcs"></em>SWCS
	                	</a>
	                </li>
	            </ul>

	            <!-- Tab panes -->

	            <div class="tab-content leftcontainer">


	            	<!-- state start -->
	                <div role="tabpanel" class="tab-pane active" id="tabstate">
	                <div class="loader-div" id="loader" style="display: none">
				     <img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
					</div>
	                	<div id="statespan">
							<?/* <select name="state[]" id="state" class="inputbox-mainscr" onchange="getfrontdistrict(this)" multiple="multiple">
								<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
								/*$states = explode(",", $app->input->get("state", '', 'raw'));
								for($i = 0; $i < count($this->state_items); $i++){
									$selected = (in_array($this->state_items[$i]->name, $states)) ? " selected " :  ""; ?>
									<option value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $selected;?>>
										<?php echo $this->state_items[$i]->name; ?>
									</option>
								<?php  } ?>
							</select>   */?>
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
		                				<label for="state_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
							<div class="scrollbar-inner">
	                            <ul class="list1 statelist">
	                            	<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
									$states = explode(",", $app->input->get("state", '', 'raw'));

										for($i = 0; $i < count($this->state_items); $i++){
											$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
											<li>
												<input type="checkbox" class="state_checkbox" name="state[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="check<?php echo $i;?>">
												<label for="check<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
											</li>
									<?php  } ?>

								</ul>
                        	</div>
                        </div>
	                </div>
					<!-- district start -->
	                <div role="tabpanel" class="tab-pane" id="tabdistrict">
	                 <div class="loader-div" id="loaderdistrict" style="display: none">
				     <img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
					</div>

	                	<div id="districtspan">
							<?php /* <select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
								<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
							</select> */?>

							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('DISTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="district_checkall" class="allcheck" id="district_allcheck">
		                				<label for="district_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="scrollbar-inner">
	                            <ul class="list1 districtlist">

								</ul>
							</div>

						</div>


	                </div>

	                  <!-- village start -->
	                  <?php /* <div role="tabpanel" class="tab-pane" id="tabvillage">
						<div id="villagespan">*/?>
							<?php /* <select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
								<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
							</select> */?>

							<?php /*<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('VILLAGE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="village_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="village_checkall" class="allcheck" id="village_allcheck">
		                				<label for="district_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="scrollbar-inner">
	                            <ul class="list1 villagelist">

								</ul>
							</div>

						</div>


	                </div>*/?>

                       <!-- type start -->
                       <div role="tabpanel" class="tab-pane" id="tabtype">
                       		<div id="typespan">
		                       <div class="top-sorter-div">
				                		<div class="row">
				                			<div class="col-md-5 col-xs-5">
				                				<h3><?php echo JText::_('TYPE_LABEL'); ?></h3>
				                			</div>
				                			<div class="col-md-7 col-xs-7 text-right">
				                				<div class="searchbox">
				                					<input type="search" class="searchtop" id="type_searchtop" placeholder="Search">
				                				</div>
				                				|
				                				<input type="checkbox" name="type_checkall" class="allcheck" id="type_allcheck">
				                				<label for="type_allcheck"></label>
				                				|
				                				<a href="#sort">
				                					<i class="fa fa-sort"></i>
				                				</a>
				                			</div>
				                		</div>
			                		</div>


		                    <?php $type = explode(',', $this->typesArray); ?>
	                          <ul class="list1 typelist">
                             	<li>

		                           <?php if(in_array("Rural", $type)) { ?>
									<input type="checkbox"	 id="m_type_rural" class="inputbox type_checkbox" name="m_type[]" value="Rural" <?php echo (in_array("Rural", $type1)) ? "checked" : "";?> id="m_type_rural">

				                      <label for="m_type_rural"><span><?php echo JText::_('RURAL'); ?></span></label>
				                      <?php } ?>
									</li>
									<li>
									<?php
					               if(in_array("Urban", $type)) { ?>
										<input type="checkbox" class="inputbox type_checkbox" name="m_type[]" value="Urban" <?php echo (in_array("Urban", $type1)) ? "checked" : "";?> id="m_type_urban">
										<label for="m_type_urban"><span><?php echo JText::_('URBAN'); ?></span></label>
										<?php } ?>
									</li>
									<li>
									<?php if(count($type) == 2) { ?>
										<input type="checkbox"	 id="m_type_all" class="inputbox type_checkbox" name="m_type[]" value="Total" <?php echo (in_array("Total", $type1)) ? "checked" : "";?> id="m_type_variabel">

				                     <label for="m_type_all"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
				                     <?php } ?>
								</li>
							</ul>


	                  	</div>
	                  </div>


					  <!-- <div class="top-sorter-div">

                     <!-- variables start-->
	          		<div role="tabpanel" class="tab-pane" id="tabvariables">
	                	<div id="variablespan">
		                	<div class="top-sorter-div">
			                		<div class="row">
			                			<div class="col-md-5 col-xs-5">
			                				<h3><?php echo JText::_('VARIABLE_LABEL'); ?></h3>
			                			</div>
			                			<div class="col-md-7 col-xs-7 text-right">
			                				<div class="searchbox">
			                					<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="villages_checkall" class="allcheck" id="variable_allcheck">
			                				<label for="viarable_allcheck"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
			                			</div>
			                		</div>
		                		</div>
                            <div class="scrollbar-inner">
		                		<div id="statetotal"></div>
		                		<div id="variables"></div>
		                	</div>
		                </div>
		            </div>
	              <!-- variables END-->

	                <!-- mpi start -->
	                <div role="tabpanel" class="tab-pane" id="tabmpi">
						<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('MPI_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="mpi_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="mpi_checkall" class="allcheck" id="mpi_allcheck">
		                				<label for="mpi_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>

	                   <ul class="list1 mpilist">
	                    	<li>
	                    		<?php if(in_array("Rural", $type)) { ?>
								<input type="checkbox" id="m_type_rural1" class="inputbox mpi_checkbox" name="m_type_rating[]" value="Rural" <?php echo (in_array("Rural",$m_type_rating)) ? "checked" : "";?> >

			                      <label for="m_type_rural1"><span><?php echo JText::_('RURAL'); ?></span></label>
			                      <?php } ?>
	                    	</li>
	                    	<li>

	                    		<?php
				               if(in_array("Urban", $type)) { ?>
									<input type="checkbox" id="m_type_urban1" class="inputbox mpi_checkbox" name="m_type_rating[]" value="Urban" <?php echo (in_array("Urban",$m_type_rating)) ? "checked" : "";?> >
									<label for="m_type_urban1"><span><?php echo JText::_('URBAN'); ?></span></label>
									<?php } ?>
	                    	</li>
	                    	<li>
	                    		<?php if(count($type) == 2) { ?>
									<input type="checkbox"	 id="m_type_all1" class="inputbox mpi_checkbox" name="m_type_rating[]" value="Total" <?php echo (in_array("Total",$m_type_rating)) ? "checked": (empty($m_type_rating)) ? "checked" : "";?> >

			                     <label for="m_type_all1"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
			                     <?php } ?>
	                    	</li>
	                    </ul>
	                </div>
                   <!-- mpi END -->
	                <!-- swcs start -->
	                <div role="tabpanel" class="tab-pane" id="tabswcs">
						<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('SWCS_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="swcs_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="swcs_checkall" class="allcheck" id="swcs_allcheck">
		                				<label for="state_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
                     <div class="scrollbar-inner">

							<?php // edited heena 24/06/13 put a condition
							if(in_array("Rural", $type)) {
								$prefix = array("Rural");
							}
							if(in_array("Urban", $type)) {
								$prefix = array("Urban");
							}
							if(count($type) == 2){
								$prefix = array("Rural","Urban","Total");
							} // edited end
							$x=0;
                             echo '<ul class="list1 swcslist">';
							foreach($this->composite_attr as $eachattr){

								echo '<li class="variable_group"><label>'.ucwords(strtolower(JTEXT::_($eachattr->field))).'</label></li>';

                                   foreach($prefix as $eachprefix){
									$checked = (in_array($eachprefix."_".$eachattr->field, $composite)) ? " checked " : "";
									echo '<li><input type="checkbox" name="composite[]" value="'.$eachprefix."_".$eachattr->field.'" '.$checked.' id="variable_check_'.$x.'" class="swcs_checkbox">';

									echo '<label for="variable_check_'.$x.'"">'.ucwords(strtolower(JTEXT::_($eachprefix))).'</label></li>';
									 $x++;
								}



							}
							 echo '</ul>';
							 ?>

                       </div>
	                </div>
	                <!-- swcs END -->
	            </div>
			</div>
			<div class="col-md-8">
				<div class="full-data-view">

				<div id="result_table" class="scrollbar-inner">
				</div>
				<div id="graph">
				</div>
				<div id="gis">
				</div>
				<div id="quartiles">
				</div>
				<div id="potentiometer">
				</div>




			</div>
		</div>
		<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
			<!-- <div class="readon frontbutton">
				<input type="button" name="reset" onClick="window.location='index.php?option=com_mica&view=micafront&reset=1';"
					class="readon button" value="<?php echo JText::_('Reset'); ?>" />
			</div> -->
			<!-- <div class="readon frontbutton">
				<input type="submit" name="submit" id="submit" class="readon button" value="<?php //echo JText::_('SHOW_DATA'); ?>" />
			</div> -->

			<input type="hidden" name="refeterview" value="micafront" />
			<input type="hidden" name="option" 		value="com_mica" />
			<input type="hidden" name="zoom" id="zoom" value="6" />
			<input type="hidden" name="view" 		value="showresults" />
			<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
			<input type="hidden" id="comparedata" 	value="0" />
		</div>
	</form>
</div>




<script type="text/javascript">

	$(document).ready(function() {



		//var choosenjq = jQuery.noConflict();
		 var graphdist = JQuery("#district").val();

		// JQuery.ajax({
		// 	url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
		// 	type    : 'POST',
		// 	data    : "dist="+graphdist,
		// 	success : function(data){
		// 		JQuery("#selectdistrictgraph").html(data);
		// 	}
		// });

		 getAttribute_v2(5,"district");


		 if(typeof(preselected) != "undefined"){
		//JQuery('#dataof').change();
		displaycombo_v2();
		getVariableList_v2();


	}
	var minval        = "";
	var maxval        = "";
	var colorselected = "";
	var edittheme     = 0;

	JQuery(".hideothers").css({"display":"none"});
	//JQuery('#attr').click();
	if(typeof(havingthematicquery)!="undefined")
	{
		var singlequery = havingthematicquery.split(",");
		JQuery("#thematic_attribute option").each(function(){
			for(var i=0;i<=singlequery.length;i++){
				if(singlequery[i]==JQuery(this).val()){
					JQuery(this).attr("disabled",true);
				}
			}
		});
	}

		// $('#state_allcheck').click(function(event) {
		// 	/* Act on the event */
		// 	$(".state_checkbox").prop("checked",$(this).prop("checked"));
		// });

		//for select all to work for all the tabs

		$('.allcheck').click(function(event) {
			/* Act on the event */
			var id      = $(this).attr('id');
			var input   = id.split('_');
			$("." +input[0]+"_checkbox").prop("checked",$(this).prop("checked"));

			if(input[0]=="state")
			{
				var allVals = [];
				var slvals = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getfrontdistrict_v2(selected);

			}
           else if(input[0]=="variable")
           {

             	var allVals = [];
				var slvals = [];
				$('.variable_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getVariableList_v2(selected);
           }
           else
           {


           }
        });

    //For check all function to work well with all other checkboxes:- start

	$('.leftcontainer').on("change",'.list1 input[type=checkbox]', function(){
		var classname =$(this).attr('class');
		//classname = classname.split(" ");
		if(classname.indexOf("_checkbox")!=-1)
		{
			end   = classname.indexOf("_checkbox");
			start = classname.indexOf(" ")!=-1?classname.indexOf(" "):0 ;
			result = classname.substring(start, end).trim();

		}
        //console.log('#'+result+'_allcheck');
		if($(this).prop('checked')==false)
		{
			//console.log('#'+result+'_allcheck');
			$('#'+result+'_allcheck').prop('checked', false); //works with mpi_allcheck, swcs_checkbox, swcs_allcheck
		}
		else
		{
			//console.log("222");
			if($('.'+result+'list input[type=checkbox]:checked').length == $('.'+result+'list input[type=checkbox]').length)
			{
				$('#'+result+'_allcheck').prop('checked', true);
			}
			else
				$('#'+result+'_allcheck').prop('checked', false);
		}

	});

    //For check all function to work well with all other checkboxes:- END

    	//for search START

           $('.searchtop').keyup(function(event) {
		    var input, filter, a, id;
		    id      = $(this).attr('id');
		    input   = id.split('_');
		    filter  = this.value.toUpperCase();

		    $("."+ input[0] +"list li").each(function(){
		    	a = $('label',this).text();
				if (a.toUpperCase().indexOf(filter) > -1)
		            $(this).show();
		        else
		            $(this).hide();
		   });
		});
		//for search END


		$('.state_checkbox').click(function(e){
			if($(this).prop('checked')==true)
			{
                var allVals = [];
				var slvals = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getfrontdistrict_v2(selected);
				// alert('Selected Checkbox values are: ' + slvals)
			}

		})

// 	JQuery(".variablegrp").live("click",function(){
// 	var myid = JQuery(this).attr("id");
// 	var dis  = JQuery('.'+myid).css("display");

// 	JQuery(".hideothers").css({"display":"none"});
// 	JQuery('.variablegrp').addClass("deactive");
// 	JQuery(this).addClass("active");
// 	JQuery(this).removeClass("deactive");
// 	if(dis=="block"){
// 		JQuery('.'+myid).css("display","none");
// 		JQuery(this).addClass("deactive");
// 		JQuery(this).removeClass("active");
// 	}else{
// 		JQuery('.'+myid).css("display","block");
// 		JQuery(this).addClass("active");
// 		JQuery(this).removeClass("deactive");
// 	}
// });
	});


//REVERSE AJAX IN STATE
	  function getfrontdistrict_v2(val){


          	//alert(val);loader

        // $( "#loader").show();
	 	 //console.log('shown')
		   if(val == "" ){
		   	//$('.state_checkbox').prop('checked', true);
			JQuery('.level2').css({'display':'none'});
			getAttribute_v2(5,"state");
			//document.getElementById('m_type').selectedIndex=0;
			JQuery(".distattrtypeselection").css({"display":"none"});
			return false;
		}else if(val=="all"){

			document.getElementById('district').selectedIndex=0;
			document.getElementById('urban').selectedIndex=0;
			document.getElementById('town').selectedIndex=0;
			//document.getElementById('m_type').selectedIndex=0;
			JQuery('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({'display':'none'});

			getAttribute_v2(5,"state");
			return false;
		}else{
			getAttribute_v2(5,"state");
			//document.getElementById('m_type').selectedIndex=0;

			JQuery(".distattrtypeselection").css({"display":"none"});
		}
		
		  $( "#loader").show();
	 	// console.log('shown')


		JQuery('.distattrtypeselection').css({'display':'none'});



		JQuery.ajax({
		    url     : "index.php?option=com_mica&task=micafront.getsecondlevel&stat="+val+"&preselected="+preselecteddata,
			method  : 'post',
			success : function(combos){
				//$( "#loader").show();
	 	        // console.log('shown')

				var segment = combos.split("split");
				JQuery(".districtlist").html(segment[0]);

				if(preselecteddata != ""){
					getAttribute_v2(5,"district");
				}
                var dataof       = JQuery("#dataof").val();

				if(typeof(preselecteddataof)!="undefined")
				{
					var selectedtype = preselecteddataof.toLowerCase();
					if(selectedtype == "ua" || selectedtype == "UA"){
						selectedtype="urban";
					}


				}
				if(dataof == "ua" || dataof == "UA"){
						dataof="urban";
					}
					$( "#loader").hide();
					//console.log('hidden')

				/*if(typeof(preselecteddata)!="undefined" && dataof.toLowerCase()==selectedtype){
					JQuery('#'+selectedtype).val(preselecteddata).attr("select","selected");
					JQuery('#'+selectedtype).change();
					alert("in1");
					eval("get"+selectedtype(preselecteddata));
				}*/
			}
		});
	}
	function getdistrict_v2(val){
	if(val == "" ){
		JQuery('.level2').css({'display':'none'});
		getAttribute_v2(5,"state");
		return false;
	}else if(val == "all"){
		JQuery('.level2').css({'display':'none'});
		getAttribute_v2(5,"state");
		return false;
	}else{
		getAttribute_v2(5,"state");
	}
	JQuery('.distattrtypeselection').css({'display':'none'});

	JQuery.ajax({
		url     : "index.php?option=com_mica&&task=micafront.getsecondlevel&stat="+val,
		method  : 'post',
		success : function(combos){
			JQuery('.level2').css({'display':'block'});
			var segment = combos.split("split");
			JQuery("#districtspan").html(segment[0]);
			JQuery("#villagespan").html(segment[0]);
			//choosenjq("select").chosen();
		}
	});
}

function getdistrictattr_v2(val){
	var val=JQuery('.district_checkbox').val();
	   $("#loaderdistrict").show();
	 	 //console.log('shown')

	//alert(val);
	if(val==""){
		JQuery('.townspan').css({'display':'block'});
		JQuery('.urbanspan').css({'display':'block'});
		JQuery('.distattrtypeselection').css({'display':'none'});
		resetCombo('district');
		getAttribute_v2(5,"state");
	}else{
		getAttribute_v2(5,"district");


		JQuery.ajax({
			url     : "index.php?option=com_mica&task=micafront.getsecondlevelvillage&villagedistrict="+val,
			method  : 'post',
			success : function(combos){
				var segment = combos.split("split");
				JQuery('.villagelist').html(segment[0]);
				//choosenjq("select").multiselect({selectedList: 1});
				var dataof = JQuery("#dataof").val();

				if(dataof == "ua" || dataof == "UA"){
					dataof="urban";
				}
				 $("#loaderdistrict").hide();
	 	         //console.log('hidden')
			}
		});
	}
	//JQuery('.htitle').css({'width':'150px'});
}
//REVERSE AJAX IN STATE
	function getVariableList_v2(){
	if(typeof(JQuery("#variablegrp"))!="undefined")
	{
		var grplist = JQuery("#variablegrp").val();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getAttribute&zoom=5&type=district&grplist="+grplist,
			method  : 'GET',
			async   : true,
			success : function(data){
				JQuery("#grplist").html(data);
				//choosenjq("#attributes").multiselect({selectedList: 1,noneSelectedText: 'Select Variable '});
			}
		});
	}
}

	// REVERSE AJAX IN STATE
	function getAttribute_v2(javazoom,type){
		JQuery.ajax({
			url     :"index.php?option=com_mica&task=showresults.getAttribute&zoom="+parseInt(javazoom)+"&type="+type,
			method  : 'GET',
			async   : true,
			success : function(data){

				JQuery("#variables").html(data);
				JQuery(".hideothers").css({"display":"none"});

					JQuery(".variablegrp").addClass("deactive");

					//choosenjq("#variablegrp").multiselect({selectedList: 1,noneSelectedText: 'Select Variable Group'});

				getVariableList_v2();
			}
		});
	}


function getData(){

		//var grplist = JQuery("#variablegrp").val();
		var data = $('#micaform').serialize();
        JQuery.ajax({
			url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
			//url     : "index.php?option=com_mica&task=micafront.getDataajax",
			method  : 'POST',
			async   : true,
			data    : data,
			success : function(data){


				JQuery("#result_table").html(data);
				//console.log(JQuery("#result_table"));
				//choosenjq("#attributes").multiselect({selectedList: 1,noneSelectedText: 'Select Variable '});
			}
		});

}
function gettown_v2(val){
	//JQuery('.htitle').css({'width':'150px'});
	JQuery('.distattrtypeselection').css({'display':'none'});
	JQuery('.districtspan').css({'display':'none'});

	JQuery('.urbanspan').css({'display':'none'});
	if(val == ""){
		JQuery('.districtspan').css({'display':'block'});

		JQuery('.urbanspan').css({'display':'block'});
	}
	getAttribute_v2(8,"town");
}

function geturban_v2(val){
	JQuery('.htitle').css({'width':'150px'});
	JQuery('.distattrtypeselection').css({'display':'none'});
	if(val == ""){
		JQuery('.districtspan').css({'display':'block'});

		JQuery('.townspan').css({'display':'block'});
	}
	getAttribute_v2(8,"urban");
}

function getvalidation_v2(){
	var myurl = "";
	var zoom  = 5;
	if(document.getElementById('state').value==''){
		//alert(statealert);
		return false;
	}else{
		myurl = "state=";
		JQuery('#state :selected').each(function(i, selected) {
			myurl +=JQuery(selected).val()+",";
			zoom=5;
		});

		myurl +="&district=";
		JQuery('#district :selected').each(function(i, selected) {
			myurl += JQuery(selected).val()+",";
			zoom  = 6;
		});
		myurl +="&attributes=";

		JQuery('input.statetotal_attributes[type=checkbox]').each(function () {
			if(this.checked){
				myurl +=JQuery(this).val()+",";
			}
		});
		JQuery("#zoom").val(zoom);
		//window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=showresults&Itemid=108";
		return false;
	}
}
function checkRadio_v2(frmName,rbGroupName){
	var radios = document[frmName].elements[rbGroupName];
	for(var i=0;i<radios.length;i++){
		if(radios[i].checked){
	   		return true;
	  	}
	}
	return false;
}
function changeWorkspace_v2(value){
	if(value == 0){
		JQuery('.createneworkspace').css({'display':'block'});
		JQuery('.createnewworkspace').css({'display':''});
		JQuery('.thematiceditoption').css('display','block');
	}else{
		window.location = "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid="+value;
	}
}
JQuery("#createworkspace").live("click",function(){
	var workspacename = JQuery("#new_w_txt").val();
	if(workspacename == ""){
		alert("Please Enter workspace name");
		return false;
	}
	window.location = "index.php?option=com_mica&task=showresults.saveWorkspace&name="+workspacename;
});
JQuery("#updateworkspace").live("click",function(){
	var workspacename = JQuery("#new_w_txt").val();
	var workspaceid   = JQuery("#profile").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid,
		method  : 'GET',
		success : function(data){
			window.location = 'index.php?option=com_mica&view=showresults&Itemid=108&msg=1';
		}
	});
});
JQuery("#deleteworkspace").live("click",function(){
	if(!confirm("Are you Sure to Delete Workspace?")){
		return false;
	}

	var workspaceid = JQuery("#profile").val();
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.deleteWorkspace&workspaceid="+workspaceid,
		method  : 'GET',
		success : function(data){
			window.location = 'index.php?option=com_mica&view=micafront&Itemid=100&msg=-1';
		}
	});
});
JQuery("#submit").live("click",function(){
	if(JQuery("#comparedata").val() == 1){
		var statecompare    = JQuery("#statecompare").val();
		var districtcompare = JQuery("#districtcompare").val();
		var towncompare     = JQuery("#towncompare").val();
		var urbancompare    = JQuery("#urbancompare").val();

		if(urbancompare != ""){
			if(urbancompare == "all"){
				alert("All UA already Selected");
				return false;
			}
			var urban=JQuery("#urban").val();
			if(urban == ""){
				alert("Please Select UA to compare");
				return false;
			}else if(urban == "all"){
				alert("You can not select All UA to compare");
				return false;
			}
		}else if(towncompare != ""){
			if(towncompare == "all"){
				alert("All Towns already Selected");
				return false;
			}
			var town=JQuery("#town").val();
			if(town == ""){
				alert("Please Select Town to compare");
				return false;
			}else if(town == "all"){
				alert("You can not select All Town to compare");
				return false;
			}
		}
		else if(districtcompare!="")
		{
			if(districtcompare=="all")
			{
				alert("All Districts already Selected");
				return false;
			}
			var dist = JQuery("#district").val();
			if(dist == ""){
				alert("Please Select District to compare");
				return false;
			}
			else if(dist=="all")
			{
				alert("You can not select All District to compare");
				return false;
			}
		}
		else if(statecompare!="")
		{
			if(statecompare=="all")
			{
				alert("All States already Selected");
				return false;
			}
			var stateval=JQuery("#state").val();
			if(stateval=="")
			{
				alert("Please Select State to compare");
				return false;
			}
			else if(stateval=="all")
			{
				alert("You can not select All State to compare");
				return false;
			}
		}
		return true;

	}else{
		var resetmtype      = 0;
		var checked         = 0;
		var dataof          = JQuery("#dataof").val();
		var districtcompare = JQuery("#district").val();
		if(dataof=="District"){
			if(JQuery("#district").val()==""){
				alert("Please Select District First");
				return false;
			}
		}
		else if(dataof=="UA")
		{
			if(JQuery("#urban").val()=="")
			{
				alert("Please Select UA First");
				return false;
			}
		}
		else if(dataof=="Town")
		{
			if(JQuery("#Town").val()=="")
			{
				alert("Please Select Town First");
				return false;
			}

		}

		JQuery("input:checkbox:checked").each(function(){
			var chk = JQuery(this).attr("checked");
			if(chk=="checked"){
				checked=1;
			}
		});

		if(checked==0){
			alert("Please select variable first");
			return false;
		}else{
			if(typeof(districtcompare)=="undefined" || districtcompare==""){
				//JQuery("#m_type").remove();
				//alert(JQuery("#m_type").val());
				return true;
			}
		}
	}

});
function getMinmaxVariable_v2(value){
	JQuery(".minmaxdisplay").html("Please Wait...");
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.getMinMax&value="+encodeURIComponent(value),
		method  : 'GET',
		success : function(data){
			var segment = data.split(",");
			minval = segment[0];
			maxval = segment[1];
			JQuery(".minmaxdisplay").html("<b>MIN :</b>"+segment[0]+"<b><br/>MAX :</b>"+segment[1]+"");
			JQuery("#maxvalh").val(segment[1]);
			JQuery("#minvalh").val(segment[0]);
		}
	});
}
JQuery("#no_of_interval").live("keyup",function(e){
	var str="";
	if(e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105){
		if(typeof(edittheme)=="undefined"){
			edittheme=0;
		}
		createTable(edittheme);
	}
});
function createTable_v2(edittheme){
	var maxval      = JQuery("#maxvalh").val();
	var minval      = JQuery("#minvalh").val();
	var level       = JQuery("#level").val();
	var str         = "";
	var diff        = maxval-minval;
	var max         = minval;
	var disp        = 0;
	var setinterval = diff/(JQuery("#no_of_interval").val());
		setinterval = setinterval.toFixed(2);
	start = minval;
	//alert(edittheme);
	if(JQuery("#no_of_interval").val()>5){
		alert("Interval must be less then 5");
		return false;
	}
	var colorselected="";
	if(edittheme!=1 && usedlevel.indexOf("0")==-1){
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
		JQuery(".colorhide").css({"display":""});
		colorselected=1;
	}else if(edittheme==1 && level=="0"){
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
		JQuery(".colorhide").css({"display":""});colorselected=1;
	}else{
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Pin</th></tr></thead>';
		JQuery(".colorhide").css({"display":"none"});colorselected=0;
	}

	for(var i=1;i<=JQuery("#no_of_interval").val();i++){
		if((edittheme!=1 && usedlevel.indexOf("0")==-1) ){

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
			start = end;

		}else if(edittheme!=1 && usedlevel.indexOf("0")!=-1 && level!="0"){

			if(usedlevel.indexOf("1")==-1){
				level=1;JQuery("#level").val(1);
			}else{
				level=2; JQuery("#level").val(2);
			}

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
			start = end;

		}else if(edittheme==1 && level!="0"){

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
			start = end;

		}else if(edittheme==1 && usedlevel.indexOf("0")!=-1 && level=="0"){

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
			start = end;

		}else{

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ></tr>';
			start = end;

		}
	}

	JQuery("#displayinterval").html(str);
	if(colorselected == 1){
		JQuery('.simpleColorChooser').click();
	}
	//return str;
}

JQuery('.simpleColorChooser').live("click",function(){
	var value = JQuery('.simple_color').val();
		value = value.replace("#","");
	var steps = JQuery("#no_of_interval").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.getColorGradiant&value="+encodeURIComponent(value)+"&steps="+parseInt(steps++),
		method  : 'GET',
		success : function(data){
			colorselected = data;
			var segment = data.split(",");
				segment = segment.reverse();
			for(var i=1;i<=segment.length;i++){
				JQuery("#color"+(i)).fadeIn(1000,JQuery("#color"+(i)).css({"background-color":"#"+segment[i]}));
			}
		}
	});
});
function getColorPallet_v2(){
	JQuery('.simple_color').simpleColor({
		cellWidth   : 9,
		cellHeight  : 9,
		border      : '1px solid #333333',
		buttonClass : 'colorpickerbutton'
	});
}

JQuery("#savesld").live("click",function(){
	var formula     = JQuery("#thematic_attribute").val();
	//var condition = JQuery("#condition").val();
	var limit       = JQuery("#no_of_interval").val();
	var level       = JQuery("#level").val();
	if(formula=="")
	{
		alert("Please Select Variable");
		return false;
	}
	else if(limit=="")
	{
		alert("Please Select Interval");
		return false;
	}

	var from  ="";
	var to    ="";
	var color ="";
	for(var i=1;i<=limit;i++)
	{
		from +=JQuery("#from"+i).val()+",";
		to   +=JQuery("#to"+i).val()+",";

		if(level=="0" || level=="")
		{
			color +=rgb2hex(JQuery("#color"+i).css("background-color"))+",";
		}
		else
		{
			color +=i+",";
		}
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
		method  : 'GET',
		success : function(data){
			window.location.href  =window.location;
		}
	});
});

JQuery(".deletegrp").live("click",function(){
	var level    = JQuery(this).attr("class");
	var getlevel = level.split(" ");
	var getdelid = JQuery(this).attr("id");
	var segment  = getdelid.split("del_");

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.deletethematicQueryToSession&formula="+encodeURIComponent(segment[1])+"&level="+getlevel[1],
		method  : 'GET',
		success : function(data){
			window.location = '';
		}
	});
});

function rgb2hex(rgb) {
	if(typeof(rgb)=="undefined"){
		return "ffffff";
	}

    if (  rgb.search("rgb") == -1 ) {
        return rgb;
    } else {
		rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
		return  hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
}

	JQuery(".edittheme").live("click",function(){
	edittheme=1;
	var cnt = JQuery("#themeconent").html();

	JQuery("#themeconent").html("");
	JQuery(".themeconent").html(cnt);
	JQuery('.simpleColorContainer').remove();

	getColorPallet_v2();

	var classname = JQuery(this).attr("class");
	var myid      = this.id;
	var getcount  = classname.split(" ");
	var seg       = myid.split("__");
	//JQuery("#thematicquerypopup").click();
	JQuery("#thematic_attribute").val(seg[0]);
	JQuery("#level").val(seg[1]);

	JQuery("#thematic_attribute").change();
	JQuery("#no_of_interval").val(getcount[1]);
	createTable(edittheme);
	JQuery("#no_of_interval").val(getcount[1]);
	JQuery(".range_"+seg[1]).each(function(i){
		i++;
		rangeid     = (JQuery(this).attr("id"));
		var mylimit = rangeid.split("-");
		JQuery("#from"+i).val(mylimit[0]);
		JQuery("#to"+i).val(mylimit[1]);
	});

	var endcolor = "";
	if(seg[1] == "0"){
		for(var i=1;i<=getcount[1];i++){
			JQuery("#color"+(i)).css({"background-color":""+JQuery(".col_"+i).css("background-color")});
			if(getcount[1] == i){
				JQuery(".simpleColorDisplay").css({"background-color":JQuery(".col_"+i).css("background-color")});
			}
			//rangeid=JQuery(".range"+i).attr("id");
		}
	}
	//	JQuery("#thematicquerypopup").click();
	thematicquerypopup_v2();
});

function thematicquerypopup_v2(){
	//alert(totalthemecount);

	if(totalthemecount==3 && (typeof(edittheme)=="undefined" || (edittheme)==0)){
		alert("You Can Select maximum 3 Thematic Query for Single Workspace");
		return false;
	}else{
		//document.getElementById('light2').style.display='block';
		//document.getElementById('fade').style.display='block';
		JQuery("#light2").fadeIn();
		JQuery("#fade").fadeIn();
	}
}

JQuery("#updatecustom").live("click",function(){
	var new_name   = JQuery('#new_name').val();
	var oldattrval = JQuery('#oldattrval').val();
	//alert(new_name);
	//alert(oldattrval);
	if(JQuery("#new_name").val()==""){
		alert("Please Select Custom Variable First!!");
		return false;
	}

	if(!validateFormula_v2()){
		return false;
	}

	var attributevale=JQuery('textarea#custom_attribute').text();
	//alert(attributevale);
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.updateCustomAttr&attrname="+new_name+"&attributevale="+encodeURIComponent(attributevale)+"&oldattrval="+encodeURIComponent(oldattrval),
		method  : 'GET',
		success : function(data){
			//JQuery("#closeextra").click();
			//reloadCustomAttr();
			window.location='index.php?option=com_mica&view=showresults&Itemid=108';
		}
	});
});

JQuery("#deletevariable").live("click",function(){
	var new_name      = JQuery('.customedit').find(":selected").text();
	var attributevale = JQuery('textarea#custom_attribute').text();
	if(new_name=="" && attributevale==""){
		alert("Please Select Custom Variable");
		return false;
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.deleteCustomAttr&attrname="+new_name+"&attributevale="+attributevale,
		method  : 'GET',
		success : function(data){
			//alert(data);
			window.location='index.php?option=com_mica&view=showresults&Itemid=108';
			//window.location='index.php?option=com_mica&view=showresults&Itemid=108';
		}
	});
});

function closePopup_v2(){
	edittheme = 0;
	document.getElementById('light2').style.display = 'none';
	document.getElementById('fade').style.display   = 'none';
	JQuery("#displayinterval").html("");

	//JQuery("#thematic_attribute").val(0).attr("selected");
	document.getElementById("thematic_attribute").selectedIndex = 0;
	JQuery("#no_of_interval").val("");
	JQuery(".minmaxdisplay").html("");
	if(JQuery(".themeconent").html().length>10){
		JQuery("#themeconent").html(JQuery(".themeconent").html());
		JQuery('.simpleColorContainer').remove();
		getColorPallet();
		JQuery(".simpleColorDisplay").css({"background-color":"#FFFFCC"});
	}
}

function validateFormula_v2(){
	var assignformula = "";
	var formula       = JQuery('textarea#custom_attribute').text();

	JQuery("#avail_attribute option").each(function(){
		//alert(JQuery(this).val());
		//alert(formula);
		var myclass=JQuery(this).parent().attr("class");
		if(JQuery(this).val()==formula.trim() && myclass!="defaultvariablelib"){
			assignformula ="You have already assign '"+formula+"' value to '"+JQuery(this).val()+"' Variable";
		}
	});

	if(assignformula!=""){
		alert(assignformula);
		return false;
	}
	return true;
}

function downloadMapPdf_v2(){
	//alert(tomcaturl);
	var start         = map.layers;
	var selectedlayer = "";

	if(javazoom==5 || javazoom==6)
	{
		selectedlayer ="india:rail_state";
	}
	else if(javazoom==7)
	{
		selectedlayer = "india:india_information";
	}
	else if(javazoom==8)
	{
		selectedlayer = "india:jos_mica_urban_agglomeration";
	}
	else
	{
		selectedlayer = "india:my_table";
	}

	var baselayer     = "india:rail_state";
	var buildrequest  = "";
	var buildrequest1 = "";
	for (x in start){
		for (y in start[x].params){
			if(start[x].params.LAYER==selectedlayer){
				if(y=="FORMAT"){
					buildrequest1 += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
				}else{
					buildrequest += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
				}
			}
		}
	}

	buildrequest=buildrequest+"FORMATEQTimage/png";
	buildrequest1=buildrequest1+"FORMATEQTimage/png";
	//alert(tomcaturl+"?"+buildrequest+"&BBOX="+map.getExtent().toBBOX()+"&WIDTH=800&HEIGHT=600");
	var finalurl = buildrequest+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
	var finalurl1 = buildrequest1+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
	window.open("index.php?option=com_mica&task=showresults.exportMap&mapparameter="+finalurl+"&baselayer="+finalurl1);
}

function exportImage_v2(name) {
    // exporting
	var store       = '0'; // '1' to store the image on the server, '0' to output on browser
	//var name      = '001'; // name of the image
	var imgtype     = 'jpeg'; // choose among 'png', 'jpeg', 'jpg', 'gif'
	var opt         = 'index.php?option=com_mica&task=showresults.amExport&store='+store+'&name='+name+'&imgtype='+imgtype;
	var flashMovie  = document.getElementById('chartdiv');
	// previoushtml =JQuery("#exportchartbutton").html();//"Please Wait..."

	JQuery("#exportchartbutton").html("Please Wait...");//"Please Wait..."
	data=  flashMovie.exportImage_v2(opt);
	flashMovie.amReturnImageData_v2("chartdiv",data);
}
function amReturnImageData_v2(chartidm,data){
	var onclick      = "exportImage(JQuery('#chartype option:selected').text())";
	var button       = '<input type="button" name="downloadchart" onclick="'+onclick+'" class="button" value="Export">';
	var previoushtml = JQuery("#exportchartbutton").html(button);
}

JQuery(".variablegrp").live("click",function(){
	var myid = JQuery(this).attr("id");
	var dis  = JQuery('.'+myid).css("display");

	JQuery(".hideothers").css({"display":"none"});
	JQuery('.variablegrp').addClass("deactive");
	JQuery(this).addClass("active");
	JQuery(this).removeClass("deactive");
	if(dis=="block"){
		JQuery('.'+myid).css("display","none");
		JQuery(this).addClass("deactive");
		JQuery(this).removeClass("active");
	}else{
		JQuery('.'+myid).css("display","block");
		JQuery(this).addClass("active");
		JQuery(this).removeClass("deactive");
	}
});
function preloader_v2(){
	var preloaderstr="<div id='facebook' ><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><div id='block_4' class='facebook_block'></div><div id='block_5' class='facebook_block'></div><div id='block_6' class='facebook_block'></div></div>";
}
JQuery(".hovertext").live("mouseover",function(e){
	var allclass  =JQuery(this).attr("class");
	var segment   =allclass.replace("hovertext ","");
	var x         = e.pageX - this.offsetLeft;
	var y         = e.pageY - this.offsetTop;
	var popuphtml ="<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>"+segment+"</div>";
	JQuery(this).append(popuphtml);
});
JQuery(".hovertext").live("click",function(e){
	JQuery(this).parent().prev().find("input").prop("checked",true);//,true);
});

JQuery(".hovertext").live("mouseout",function(e){
	JQuery("#popupattr").remove();
});

JQuery(".customvariablecheckbox").live("click",function(){
	var check = (JQuery(this).attr("checked"));

	if(check == "checked"){
		JQuery("#new_name").val(JQuery(this).attr("id"));
		JQuery("#custom_attribute").text(JQuery(this).val());
		JQuery("#save").click();
		//addCustomVariable(JQuery(this).val(),JQuery(this).attr("id"));
	}else{
		window.location = "index.php?option=com_mica&task=showresults.deleteattribute&attr="+JQuery(this).attr("id");
	}
});
JQuery(document).ready(function(){
	JQuery("li #menu100").parent().attr("class","active");
});

function toggleCustomAction_v2(ele, action){
	if(action != ""){
		//JQuery(ele).prev().attr("checked",action);
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("checked",action);
	}else{
		//JQuery(ele).prev().removeAttr("checked");
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').removeAttr("checked");
	}
	//JQuery(ele).prev().click();
	JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').click();
}
JQuery(".customedit").live("click", function(){
	JQuery('#save').attr("id","updatecustom");
	JQuery('#updatecustom').attr("onclick","javascript:void(0)");
	JQuery('#updatecustom').attr("value","Update");

	JQuery('#new_name').val(JQuery(this).attr("id"));
	JQuery('#new_name').attr('disabled', true);
	JQuery('.aa').html("Edit");

	oldattrval=JQuery(this).attr("value");
	JQuery('#oldattrval').val(oldattrval);
	JQuery('textarea#custom_attribute').text(JQuery(this).attr("value"));
	lastchar = '';
	//moverightarea();
	//JQuery('#new_name').val(JQuery(this).prev().attr("id"));
	//JQuery('#custom_attribute').val(JQuery(this).prev().val());
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block';
});

JQuery("#closeextra").live("click",function(){
	JQuery('#updatecustom').attr("id","save");
	JQuery('#save').attr("onclick","checkfilledvalue();");
	JQuery('#save').attr("value","Save");
	JQuery('#new_name').val("");
	JQuery('#new_name').attr('disabled', false);
	JQuery('.aa').html("Add New");
});
JQuery(".deletecustomvariable").live("click",function(){
	var names = new Array();
	var i     = 0;
	JQuery(".dcv:checked").each(function(i){
		names.push(JQuery(this).attr("id"));
	});
	window.location="index.php?option=com_mica&task=showresults.deleteCustomVariableFromLib&attrname="+names;
});

JQuery("#fullscreen, #fullscreen1").live("click",function(){
	var fromthematic  =	JQuery("#fullscreen").attr("fromthematic");
	/*JQuery("#map").addClass("fullscreen");
	JQuery(this).css({"display":"none"});
	JQuery("#fullscreenoff").fadeIn();
	JQuery("#map").css({"width":JQuery(window).width()-20,"height":JQuery(window).height()-20});
	map.updateSize();*/

	if(fromthematic==1){
		popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
	}else{
		popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component', 'MapWin');
	}
	popped.document.body.innerHTML = "<div id='map' style='height:"+JQuery(window).height()+"px;width:"+JQuery(window).width()+"px;'></div>";
});

JQuery(".fullscreeniconoff").live("click",function(){
	JQuery("#fullscreentable").removeClass("fullscreentable");
	JQuery("#matrixclose").remove();
	JQuery(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");

	JQuery(this).removeClass("fullscreeniconoff");
	JQuery(this).addClass("fullscreenicon");
	JQuery("#fade").css({"display":"none"});
	JQuery("#tablescroll").removeAttr("width");
	JQuery("#tablescroll").removeAttr("overflow");
	JQuery("#tablescroll").removeAttr("height");
});
JQuery(".filterspeed").live("click",function(){
	var selection     =JQuery(this).val();
	filterspeed_radio = selection;
	if(selection == "0"){
		$("#speed_variable").attr("multiple","true");
		$("#speed_variable").removeClass("inputbox");

		$("#speed_variable").removeClass("chzn-done");
		$("#speed_region").removeClass("chzn-done");

		$("#speed_region").removeAttr("multiple");
		$("#speed_variable_chzn").remove();
		$("#speed_region_chzn").remove();
		//choosenjq("#speed_region").chosen({max_selected_options: 1});
		//choosenjq("#speed_variable").chosen({max_selected_options: 5});
		JQuery("#speed_region").addClass("inputbox");
	}else{
		JQuery("#speed_variable").removeAttr("multiple");
		JQuery("#speed_variable").addClass("inputbox");
		JQuery("#speed_region").attr("multiple","true");

		JQuery("#speed_variable").removeClass("chzn-done");
		JQuery("#speed_region").removeClass("chzn-done");

		JQuery("#speed_variable_chzn").remove();
		JQuery("#speed_region_chzn").remove();

		JQuery("#speed_region").removeClass("inputbox");
		choosenjq("#speed_region").chosen({max_selected_options: 5});
		choosenjq("#speed_variable").chosen({max_selected_options: 1});
	}

	JQuery(".speed").css({"display":"block"});
	//choosenjq("#speed_variable").val('').trigger("liszt:updated");
	//choosenjq("#speed_region").val('').trigger("liszt:updated");
});

JQuery("#showspeed").live("click",function(){
	var speedvar    =new Array();
	var speedregion =new Array();
	if(typeof(JQuery("#speed_variable").attr("multiple")) == "string"){
		JQuery("#speed_variable").each(function(){
			speedvar.push(encodeURIComponent(JQuery(this).val()));
		});
		speedregion.push(encodeURIComponent(JQuery("#speed_region").val()));
	}else{
		speedvar.push(encodeURIComponent(JQuery("#speed_variable").val()));
		JQuery("#speed_region").each(function(){
			speedregion.push(encodeURIComponent(JQuery(this).val()));
		});
	}
	//var filterspeed = JQuery('input:radio[name=filter]:checked]').val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.getsmeter&region="+speedregion+"&speedvar="+speedvar+"&filterspeed="+filterspeed_radio,
		type    : 'GET',
		success : function(data){
			JQuery("#spedometer_region").html("");
			JQuery("#spedometer_region").html(data);
			initspeed();
			JQuery("#speedfiltershow").css({"display":"block"});
			JQuery(".sfilter").css({"display":"none"});
			JQuery("#spedometer_region").css({"width":"915px"});
			JQuery("#spedometer_region").css({"overflow":"auto"});
		}
	});
});

JQuery("#speedfiltershow").live("click",function(){
	JQuery("#speedfiltershow").css({"display":"none"});
	JQuery(".sfilter").css({"display":"block"});
	JQuery("#spedometer_region").css({"width":"681px"});
	JQuery("#spedometer_region").css({"overflow":"auto"});
});

JQuery("#downmatrix").live("click",function(){
	window.location.href="index.php?option=com_mica&task=showresults.downloadMatrix&rand="+Math.floor(Math.random()*100000);
});

JQuery("#matrix").live("click",function(){
	if(JQuery("#fullscreentable").html().length < 100){
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getMaxForMatrix",
			method  : 'POST',
			success : function(data){
				JQuery("#fullscreentable").html(data);
			}
		});
	}
});

JQuery("#speedometer").live("click",function(){
	JQuery.ajax({
		url    :"index.php?option=com_mica&task=showresults.speedometer",
		method : 'POST',
		success:function(data){
			JQuery("#speed_region").html(data);
		}
	});
});

// JQuery(document).ready(function(){
// 	var graphdist = JQuery("#district").val();

// 	$.ajax({
// 		url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
// 		type    : 'POST',
// 		data    : "dist="+graphdist,
// 		success : function(data){
// 			JQuery("#selectdistrictgraph").html(data);
// 		}
// 	});
// });

JQuery("#showchart").live("click",function(){
	var checkedelements = new Array();
	var checkeddist     = new Array();
	var checkevar       = new Array();
	JQuery("#selectdistrictgraph").find(".districtchecked").each(function(){
		if(JQuery(this).attr("checked")){
			checkedelements.push(this.val);
			checkeddist.push(JQuery(this).val());
		}
	});

	JQuery("#light1007").find(".variablechecked").each(function(){
		if(JQuery(this).attr("checked")){
			checkedelements.push(JQuery(this).val());
			checkevar.push(JQuery(this).val());
		}
	});

	if(checkedelements.length >15){
		alert("Variable + District Total Should be less then 15");
		return false;
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=showresults.getGraph",
		type    : 'POST',
		data    : "dist="+checkeddist+"&attr="+checkevar,
		success : function(data){
			var segments = data.split("<->");
			datastr    = segments[0];
			grpsetting = segments[1];
			JQuery("#chartype").change();
		}
	});
});

JQuery(document).ready(function(){
    JQuery("#summary").click(function(){
        JQuery("#summary_data").toggle();
        JQuery("#summary").toggleClass('active');
    });
});

$('.actionbtn').click(function(event) {
	/* Act on the event */


	switch ($(this).attr('id')) {
		case "tabledata":
			$('#result_table').find('.contenttoggle1').hide();
			$('#result_table').find('.contenttoggle1.text').show();
			break;
		case "graphdata":
			$('#result_table').find('.contenttoggle1').hide();
			$('#result_table').find('.contenttoggle1.graphs').show();
			break;
		case "gisdata":
			$('#result_table').find('.contenttoggle1').hide();
			$('#result_table').find('.contenttoggle1.gis').show();
			break;
		case "qtdata":
			$('#result_table').find('.contenttoggle1').hide();
			$('#result_table').find('.contenttoggle1.matrix').show();
			break;
		case "pmdata":
			$('#result_table').find('.contenttoggle1').hide();
			$('#result_table').find('.contenttoggle1.speedometer').show();
			break;
		case "comparedata":
			$('#result_table').find('.contenttoggle2').hide();
			$('#result_table').find('.contenttoggle2.compare').show();
			break;
		case "summary":
			$('#result_table').find('.contenttoggle2').hide();
			$('#result_table').find('.contenttoggle2.compare').show();
		break;

		default:
			// statements_def
			break;
	};

});


function displaycombo_v2(value){
	if(JQuery("#state").val()=="all"){
		alert("Please Select State First");
		document.getElementById('dataof').selectedIndex=0;
		document.getElementById("district").selectedIndex=0;
		return false;
	}else{
		var allVals = [];
				var slvals = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
		getfrontdistrict_v2(selected);
	}

	if(value=="District"){
		JQuery(".districtspan").css({"display":"block"});
		JQuery(".townspan").css({"display":"none"});
		JQuery(".urbanspan").css({"display":"none"});
	}/*else if(value=="Town"){
		JQuery(".districtspan").css({"display":"none"});
		JQuery(".townspan").css({"display":"block"});
		JQuery(".urbanspan").css({"display":"none"});
		JQuery(".distattrtypeselection").css({"display":"none"});
		document.getElementById("district").selectedIndex=0;
		document.getElementById('m_type').selectedIndex=0;
		JQuery(".distattrtypeselection").css({"display":"none"});
	}else if(value=="UA"){
		JQuery(".districtspan").css({"display":"none"});
		JQuery(".townspan").css({"display":"none"});
		document.getElementById('m_type').selectedIndex=0;
		JQuery(".distattrtypeselection").css({"display":"none"});
		JQuery(".urbanspan").css({"display":"block"});
		document.getElementById("district").selectedIndex=0;
	}else{
		JQuery(".districtspan").css({"display":"none"});
		JQuery(".townspan").css({"display":"none"});
		JQuery(".urbanspan").css({"display":"none"});
		JQuery(".distattrtypeselection").css({"display":"none"});
		document.getElementById('m_type').selectedIndex=0;
	}*/
}

</script>