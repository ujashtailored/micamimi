<?php
/**
 * edited  by salim
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$doc     = JFactory::getDocument();
$app     = JFactory::getApplication('site');
$db      = JFactory::getDbo();
$session = JFactory::getSession();

// $doc = JFactory::getDocument();
// $app = JFactory::getApplication('site');

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
// $doc->addStyleSheet(JURI::base().'components/com_mica/js/newjs/jquery-ui.multiselect.widget.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');

// For Vertical Slider
$doc->addStyleSheet(JURI::base().'components/com_mica/js/slick/slick.css', 'text/css');

$itemid        = 188;
$type1         = $app->input->get('m_type', '', 'raw');
$m_type_rating = $app->input->get("m_type_rating", '', 'raw');
$composite     = $app->input->get("composite", '', 'raw');
/*echo "<pre />";print_r($composite);
echo "<pre />";print_r($this->composite_attr);exit;*/
header('Access-Control-Allow-Origin: *');
?>

<script src="components/com_mica/maps/OpenLayers.js"></script>
<link rel="stylesheet" href="components/com_mica/maps/theme/default/style.css" type="text/css">
<link rel="stylesheet" href="components/com_mica/maps/examples/style.css" type="text/css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script src="http://www.chartjs.org/samples/latest/utils.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script> -->

<!-- <script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script> -->
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.mousewheel.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>

<?php /*<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>*/ ?>

<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.cookie.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<!-- <script src="<?php echo JURI::base()?>components/com_mica/js/swfobject.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amcharts.js"></script> -->
<!-- <script src="<?php echo JURI::base()?>components/com_mica/js/amfallback.js"></script> -->
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<!-- <script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.multiselect.widget.js"></script> -->
<!-- <script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>-->
<!-- <script src="<?php //echo JURI::base()?>components/com_mica/maps/OpenLayers.js"></script> -->
<!--bubblechart -->
<!-- dowlode pdf -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
<!--bubblechart end -->
<!-- For Vertical Slider -->
<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>


<script type="text/javascript">
	//var choosenjq = jQuery.noConflict();
	var JQuery = jQuery.noConflict();
	var $ = jQuery.noConflict();

	// choosenjq(document).ready(function(){
	// 	choosenjq	("select").multiselect({
	// 			showCheckbox: true,
	// 			hideSelect:true,
	// 			isMultiple:true,
	// 			isOpen: false,
	// 			filterLabelText:"Select State"
	// 	});
	// });
	/*function tabclickevent()
	{
		var elem = choosenjq('ul.nav.nav-tabs > li.active > a').attr('aria-controls');
		var newelemtrigger ='#tag'+elem;
		alert(newelemtrigger);
		//choosenjq(newelemtrigger+' button.ui-multiselect--display').trigger('click');
	}
	choosenjq(document).ready(function(){
		tabclickevent();
		choosenjq('ul.nav.nav-tabs > li > a').click(function(){
			tabclickevent();
		})
	});*/

</script>


<style type="text/css">
is-multiple {display:block}
canvas {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}
</style>

<?php /*<script src="<?php echo JURI::base()?>components/com_mica/js/field.js"></script> */?>
<script type="text/javascript">
	var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var districtalert     = '<?php echo JText::_("SELECT_DISTRICT_ALERT")?>';
	var typealert         = '<?php echo JText::_("SELECT_TYPE_ALERT")?>';
	var variablealert     = '<?php echo JText::_("SELECT_VARIABLE_ALERT")?>';
	var mpiealert         = '<?php echo JText::_("SELECT_MPI_ALERT")?>';
	var swcsalert         = '<?php echo JText::_("SELECT_SWCS_ALERT")?>';
	var preselected       = '<?php echo $app->input->get("district", "", "raw"); ?>';
	var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type = '<?php echo $app->input->get("m_type", "", "raw"); ?>';
	var chart1;
	var keys = [];
	var trashedLabels = [];
	var trashedData = [];
	var datasetValues = [];


	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	jQuery(function() {
		jQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		jQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});


	function checkAll(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",false);
		});
	}
</script>

<script src="templates/micamimi/js/jquery.scrollbar.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();
    });
</script>

<div class="row-fluid top-row">
	<div class="col-md-2">
		<h1 class="title-explore"><?php echo JText::_('SELECT_CR_MSG');?></h1>
	</div>
	<div class="col-md-10 text-right">
		<div class="sorter-tab">
			<ul>
				<li>
					<a href="javascript:void(0);" id="submit" class="actionbtn" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('SHOW_DATA'); ?></a></li>
				</li>
				<!-- <li>
					<a href="javascript:void(0);" id="summary" class="actionbtn">Summary</a>
				</li> -->
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="graphdata" class="actionbtn" onclick="loadCharts();"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="matrix" class="actionbtn"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li>
			</ul>
		</div>
		<div class="worspacce-edit">
			My workspace <a class="workspacce-edit" href="javascript:void(0)" onclick="document.getElementById('workspacceedit').style.display='block';document.getElementById('fade').style.display='block'">
				<i class="fas fa-pencil-alt"></i>
			</a>
			<!-- My workspace <a class="workspacce-edit" href="<?php echo $this->workspacelinkdiv; ?>"><i class="fas fa-pencil-alt"></i></a> -->
			<h2>
			<div id="activeworkspacename">

			</div>
			</h2>
		</div>
	</div>
</div>

<div id="workspacceedit" style="display:none;" class="white_content2 large-popup">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('workspacceedit').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>
	<div class="poptitle">
		<?php echo JTEXT::_('MY_WORKSPACE');?>
	</div>

	<div class="scroll_div">
		<div class="col-md-8">
			<div class="blockcontent">
				<ul class="allworkspace">
					<?php
					$this->user = JFactory::getUser();
					$id = $this->user->id;

					if($id == 0){
						return -1;
					}

					$db   = JFactory::getDBO();
					$query="SELECT name, id, is_default
						FROM ".$db->quoteName('#__mica_user_workspace')."
						WHERE ".$db->quoteName('userid')." = ".$db->quote($id)."
							AND ".$db->quoteName('is_default')." <> ".$db->quote(1);
					$db->setQuery($query);
					$result = $db->loadAssocList();

					$app               = JFactory::getApplication('site');
					$session = JFactory::getSession();
					$str               = "";

					//$activeworkspace   = $app->input->get('activeworkspace', '', 'raw');
					$activeworkspace   = $session->get('activeworkspace');
					$selected          = "";
					$activeprofilename = $eachresult['name'];
					$i                 = 1;
					foreach($result as $eachresult){
						if(($i%2)==0){
							$clear="clear";
						}else{
							$clear="";
						}

						if($activeworkspace == $eachresult['id']){

							$selected          = "selected";
							$edit              = "Edit/Delete";
							$activeprofilename = $eachresult['name'];

							echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('Active Workspace :<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
							$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
							echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
							$active_text = "<span style='font-weight:normal;'>(Active)<span>";
							$active_text = "";

						}else{

							$selected    = "";
							$edit        = "Select";
							$onclick     = "changeWorkspace(".$eachresult['id'].")";
							$active_text = "";
						}

						$str .= '
							<li class="'.$clear." ".$selected.'">
								<div style="width:70%;float:left;">
									<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
								</div>
								<div style="width:30%;float:right;">
									<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
								</div>
							</li>';// '.$selected.'
					}
					echo $str;
					echo '<div id="lightn" class="white_contentn" style="display: none;">
				<div class="divclose">
					<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
						<img src="media/system/images/closebox.jpeg" alt="X" />
					</a>
				</div>
				<div align="left">
					<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
					<div class="frontbutton readon">
						<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
					</div>
					<div class="frontbutton">
						<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
					</div>
				</div>';

					 ?>
				</ul>
			</div>
		</div>

		<div class="col-md-4">
			<div class="contentblock endcontent">
				<div class="contenthead">New Workspace</div>
				<div class="blockcontent newworkspace">
					<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
					<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="explore-data">
	<!-- <form name="adminForm" id="micaform" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="POST"> -->
	<form name="adminForm" id="micaform" action="" method="POST">
		<div class="row">
			<div class="col-md-4">
	            <ul class="nav nav-tabs" role="tablist">
	                <li role="presentation" class="active" id="state_tab">
	                	<a href="#tabstate" aria-controls="state" role="tab" data-toggle="tab">
	                		<em class="icon-state"></em>State
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabdistrict" aria-controls="district" role="tab" data-toggle="tab" id="district_tab">
	                		<em class="icon-district"></em>District
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabIndustryLevel" aria-controls="IndustryLevel" role="tab" data-toggle="tab" id="industry_level_tab">
	                		<em class="icon-district"></em>Industry Level Group
	                	</a>
	                </li>
	                <!-- <li role="presentation">
	                	<a href="#tabvillage" aria-controls="village" role="tab" data-toggle="tab">
	                		<em class="icon-district"></em>Village
	                	</a>
	                </li> -->
	                <li role="presentation">
	                	<a href="#tabtype" aria-controls="type" role="tab" data-toggle="tab" id="type_tab">
	                		<em class="icon-type"></em>Type
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabvariables" aria-controls="variables" role="tab" data-toggle="tab" id="variable_tab">
	                		<em class="icon-variables"></em>Variables
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabmpi" aria-controls="mpi" role="tab" data-toggle="tab" id="mpi_tab">
	                		<em class="icon-mpi"></em>MPI
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#tabswcs" aria-controls="swcs" role="tab" data-toggle="tab" id="swcs_tab">
	                		<em class="icon-swcs"></em>SWCS
	                	</a>
	                </li>
	            </ul>

	            <!-- Tab panes -->

	            <div class="tab-content leftcontainer" id="leftcontainer">
	            	<!-- State start -->
	                <div role="tabpanel" class="tab-pane active" id="tabstate">
		                <div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
	                	<div id="statespan">
							<?/* <select name="state[]" id="state" class="inputbox-mainscr" onchange="getfrontdistrict(this)" multiple="multiple">
								<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
								/*$states = explode(",", $app->input->get("state", '', 'raw'));
								for($i = 0; $i < count($this->state_items); $i++){
									$selected = (in_array($this->state_items[$i]->name, $states)) ? " selected " :  ""; ?>
									<option value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $selected;?>>
										<?php echo $this->state_items[$i]->name; ?>
									</option>
								<?php  } ?>
							</select>   */?>
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
		                				<label for="state_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
							<div class="scrollbar-inner">
	                            <ul class="list1 statelist">
	                            	<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
									$states = explode(",", $app->input->get("state", '', 'raw'));

										for($i = 0; $i < count($this->state_items); $i++){
											$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
											<li>
												<input type="checkbox" class="state_checkbox" name="state[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="check<?php echo $i;?>">
												<label for="check<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
											</li>
									<?php  } ?>

								</ul>
	                    	</div>
	                    </div>
	                </div>
					<!-- State End -->

					<!-- district start -->
	                <div role="tabpanel" class="tab-pane" id="tabdistrict">
	                	<div class="loader-div" id="loaderdistrict" style="display: none">
				     		<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>

	                	<div id="districtspan">
							<?php /* <select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
								<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
							</select> */?>

							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('DISTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="district_checkall" class="allcheck" id="district_allcheck">
		                				<label for="district_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
			                            <ul class="list1 districtlist" onchange="getdistrictattr_v2(this.value)">
										</ul>
									</div>
	                			</div>
	                			<div class="col-md-2 padding-none">
	                				<div class="dist-icon-list">
	                					<div id= "statecode" >
	                					</div>
	                				</div>
	                			</div>
	                		</div>
						</div>
	                </div>

	                <!-- Industry Level Group Start-->
	                <div role="tabpanel" class="tab-pane" id="tabIndustryLevel">
		                <div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
	                	<div id="IndustryLevelSpan">
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('INDUSTRY_LEVEL_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="industryLevel_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
							<div class="scrollbar-inner">
								<ul class="list1 industryLevelList">
	                            	<?php echo $this->industrylevelgroups;?>
	                            </ul>
	                    	</div>
	                    </div>
	                </div>
					<!-- Industry Level Group End-->

	                  <!-- village start -->
	                  <?php /* <div role="tabpanel" class="tab-pane" id="tabvillage">
						<div id="villagespan">*/?>
							<?php /* <select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
								<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
							</select> */?>

							<?php /*<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('VILLAGE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="village_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="village_checkall" class="allcheck" id="village_allcheck">
		                				<label for="district_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="scrollbar-inner">
	                            <ul class="list1 villagelist">

								</ul>
							</div>

						</div>


					</div>*/?>

	               	<!-- type start -->
	            	<div role="tabpanel" class="tab-pane" id="tabtype">
	               		<div id="typespan">
	                    	<div class="top-sorter-div">
	                			<div class="row">
	                				<div class="col-md-5 col-xs-5">
	                					<h3><?php echo JText::_('TYPE_LABEL'); ?></h3>
	                				</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="type_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="type_checkall" class="allcheck" id="type_allcheck">
		                				<label for="type_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
	                			</div>
	            			</div>
							<?php $type = explode(',', $this->typesArray); ?>
	                    	<ul class="list1 typelist">
	                    		<li>
		                           <?php if(in_array("Rural", $type)) { ?>
									<input type="checkbox"	 id="m_type_rural" class="inputbox type_checkbox" name="m_type[]" value="Rural" <?php echo (in_array("Rural", $type1)) ? "checked" : "";?> id="m_type_rural">

				                      <label for="m_type_rural"><span><?php echo JText::_('RURAL'); ?></span></label>
				                      <?php } ?>
								</li>
								<li>
									<?php
				               		if(in_array("Urban", $type)) { ?>
										<input type="checkbox" class="inputbox type_checkbox" name="m_type[]" value="Urban" <?php echo (in_array("Urban", $type1)) ? "checked" : "";?> id="m_type_urban">
										<label for="m_type_urban"><span><?php echo JText::_('URBAN'); ?></span></label>
									<?php } ?>
								</li>
								<li>
									<?php if(count($type) == 2) { ?>
										<input type="checkbox"	 id="m_type_all" class="inputbox type_checkbox" name="m_type[]" value="Total" <?php echo (in_array("Total", $type1)) ? "checked" : "";?> id="m_type_variabel">
										<label for="m_type_all"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
			                     	<?php } ?>
								</li>
							</ul>
	              		</div>
					</div>
					<!-- type END-->

	                <!-- variables start-->
	          		<div role="tabpanel" class="tab-pane" id="tabvariables">
	                	<div id="variablespan">
		                	<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('VARIABLE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="villages_checkall" class="allcheck" id="variable_allcheck">
		                				<label for="viarable_allcheck"></label>
		                				|
		                				<a href="#sort">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
	                					<div id="customvariables">
	                						<?php include  JPATH_ROOT.'/templates/micamimi/html/com_mica/micafront/custom_variable.php';?>
	                					</div>
				                		<div id="statetotal"></div>
				                		<div id="variables"></div>
				                	</div>
	                			</div>
	                			<div class="col-md-2">
	                				<div id="variableshortcode"></div>
	                			</div>
	                		</div>
		                </div>
		            </div>
	              	<!-- variables END-->

	                <!-- mpi start -->
	                <div role="tabpanel" class="tab-pane" id="tabmpi">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('MPI_LABEL'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" class="searchtop" id="mpi_searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="mpi_checkall" class="allcheck" id="mpi_allcheck">
	                				<label for="mpi_allcheck"></label>
	                				|
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	            		</div>

	                   	<ul class="list1 mpilist">
	                    	<li>
	                    		<?php if(in_array("Rural", $type)) { ?>
								<input type="checkbox" id="m_type_rural1" class="inputbox mpi_checkbox" name="m_type_rating[]" value="Rural" <?php echo (in_array("Rural",$m_type_rating)) ? "checked" : "";?> >

			                    <label for="m_type_rural1"><span><?php echo JText::_('RURAL'); ?></span></label>
			                    <?php } ?>
	                    	</li>
	                    	<li>

	                    		<?php
				               if(in_array("Urban", $type)) { ?>
									<input type="checkbox" id="m_type_urban1" class="inputbox mpi_checkbox" name="m_type_rating[]" value="Urban" <?php echo (in_array("Urban",$m_type_rating)) ? "checked" : "";?> >
									<label for="m_type_urban1"><span><?php echo JText::_('URBAN'); ?></span></label>
									<?php } ?>
	                    	</li>
	                    	<li>
	                    		<?php if(count($type) == 2) { ?>
									<input type="checkbox"	 id="m_type_all1" class="inputbox mpi_checkbox" name="m_type_rating[]" value="Total" <?php echo (in_array("Total",$m_type_rating)) ? "checked": (empty($m_type_rating)) ? "checked" : "";?> >

			                     <label for="m_type_all1"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
			                     <?php } ?>
	                    	</li>
	                    </ul>
	                </div>
	               <!-- mpi END -->
	                <!-- swcs start -->
	                <div role="tabpanel" class="tab-pane" id="tabswcs">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('SWCS_LABEL'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" class="searchtop" id="swcs_searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="swcs_checkall" class="allcheck" id="swcs_allcheck">
	                				<label for="state_allcheck"></label>
	                				|
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	            		</div>
	                    <div class="scrollbar-inner">

								<?php // edited heena 24/06/13 put a condition
								if(in_array("Rural", $type)) {
									$prefix = array("Rural");
								}
								if(in_array("Urban", $type)) {
									$prefix = array("Urban");
								}
								if(count($type) == 2){
									$prefix = array("Rural","Urban","Total");
								} // edited end
								$x=0;
	                             echo '<ul class="list1 swcslist">';
								foreach($this->composite_attr as $eachattr){

									echo '<li class="variable_group"><label>'.ucwords(strtolower(JTEXT::_($eachattr->field))).'</label></li>';

	                                   foreach($prefix as $eachprefix){
										$checked = (in_array($eachprefix."_".$eachattr->field, $composite)) ? " checked " : "";
										echo '<li><input type="checkbox" name="composite[]" value="'.$eachprefix."_".$eachattr->field.'" '.$checked.' id="variable_check_'.$x.'" class="swcs_checkbox">';

										echo '<label for="variable_check_'.$x.'">'.ucwords(strtolower(JTEXT::_($eachprefix))).'</label></li>';
										 $x++;
									}
								}
								 echo '</ul>';
								 ?>
						</div>
	                </div>
	                <!-- swcs END -->
	            </div>
			</div>


		<div class="col-md-8">
			<div class="full-data-view">

				<!--Start DataTable -->
				<div id="result_table" class="scrollbar-inner" style="display:none">
					<div class="text contenttoggle1" id="textscroll">
						<div class="button-area text-right">
							<!-- <a href="javascript:void(0);" class="toggle2 saperator" id="alldata"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a>
							<a href="javascript:void(0);" class="toggle2 saperator" id="compare"><i class="fa fa-check" aria-hidden="true"></i><?php echo JText::_('COMPARE_TAB_LABEL'); ?></a> -->
							<!--  <span class="toggle2 saperator" id="alldata" ><?php echo JText::_('TEXT_TAB_LABEL'); ?></span><span class="toggle2 saperator" id="compare"><?php echo JText::_('COMPARE_TAB_LABEL'); ?></span> -->
							<a href="#alldata" onclick="downloadAction();"><i class="fas fa-download"></i><?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?></a>
						</div>

						<div class="collpsable1">
							<!-- <span class="toggle2 saperator" id="alldata" ><?php echo JText::_('TEXT_TAB_LABEL'); ?></span><span class="toggle2 saperator" id="compare"><?php echo JText::_('COMPARE_TAB_LABEL'); ?></span> -->
						</div>

						<div class="activecontent" >
							<div class="alldata contenttoggle2" id="jsscrollss alldata" style="width:940px;height:524px;max-height:524px;"></div>
							<?php echo "<br /><br />"; ?>
						</div>
					</div>
				</div>
				<!--End DataTable -->

				<!--Start Graph -->
				<div id="graph" style="display:none;">
					<div class="graphs contenttoggle1">
						<div class="row">
							<div class="col-md-4">
								<div class="chart_type">
									<label>Chart Type:</label>
									<select name="charttype" class="inputbox" id="chartype" onchange="loadCharts();">
										<option value="bar" selected>Bar Chart</option>
										<option value="line">line Chart</option>
										<option value="radar">Radar Chart</option>
										<option value="bubble">Bubble Chart</option>
									</select>

									<label>Show Legend</label>
									<input type="checkbox" id="enablelegend" name="enablelegend"  value="1" />
								</div>
							</div>
							<div class="col-md-5">
								<div class="row">
									<div class="col-md-12">
										<?php
											$variableoptions="";
											$variableopopup = '<div id="light1007" class="white_content2 tab-pane" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1007\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select Variable</div>';
											$distopopup     = '<div id="light1008" class="white_content2 tab-pane" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1008\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select District</div>';
											$distoptions    ='<div id="cblist">
			                                               </div>';
										?>
										<?php
											echo $distopopup.$distoptions."</div>";
										?>

										<a href="javascript:void(0);" id="graphdistrict" onClick="document.getElementById('light1008').style.display='block';document.getElementById('fade').style.display='block'"><b>Select District</b></a>

										&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;

										<a href="javascript:void(0);" id="graphvariable" onClick="document.getElementById('light1007').style.display='block';document.getElementById('fade').style.display='block'"><b>Select Variable</b></a>

										<?php
											echo $variableopopup.'<div id="variabelist"></div></div>';
		                                ?>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="button-area-2">
									<!-- <div id="showchart"><input type="button"  name="showchart"  class="button" value="Show Chart" /></div> -->
									<div id="exportchartbutton" class="exportbtn">
										<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
									</div>
								</div>
							</div>
						</div>
						<canvas id="myChart" style="width:100%; height:520px; background-color:#FFFFFF"></canvas>
						<!-- end of amcharts script -->
					</div>
				</div>
				<!--End Graph -->

				<!--Start GIS -->
				<div id="gis" style="display:none;">
					<!--Start Thematic -->
						<div id="light_thematic" style="display: none;" class="tq white_content">

							<div class="divclose">
								<a href="javascript:void(0);" onclick="document.getElementById('light_thematic').style.display='none';">
									<img src="media/system/images/closebox.jpeg" alt="X">
								</a>
							</div>

							<div class="" style="width:100%;float:left;">
								<div class="thematicarea clearfix">
									<div class="blockcontent">
										<?php function getCustomAttrName($name,$activeworkspace) {
											$db    = JFactory::getDBO();
											$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
												WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
													AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
											$db->setQuery($query);
											$result = $db->loadAssoc();

											if(count($result) == 0){
												return array($name,$name);
											}else{
												return array($result[0],$result[1]);
											}
										}

										$activeworkspace = $this->activeworkspace;

										$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
											WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
											ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
										$db->setQuery($query);
										$result = $db->loadObjectList();

										$id    = array();
										$level = array();
										$i     = 1;
										$j     = 1;
										$grouping = array();
										foreach($result as $range){
											if($range->level == "0"){
												$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
											}else{
												$pin = str_replace("#","",$range->color);
												$str = " <div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
											}

											$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
											//$id[] = $range->custom_formula;
											//$grouping[$range->custom_formula] = $range->level;
											$level[$range->custom_formula][] = $range->level;
											$i++;
											$j++;
										}

										$i            = 0;
										$range        = array();
										$str          = array();
										$totalcount   = 0;
										$l            = 0;
										$tojavascript = array();
										$grpname = array();
										foreach($grouping as $key => $val){
											$grname         = getCustomAttrName($key, $activeworkspace);
											$grpname[]      = $grname;
											$str[]          = implode(" ",$val);
											$ranges[]       = count($val);
											$levelunique[]  = $level[$key][0];
											$tojavascript[] = $key;
											$l++;
										}
										$tojavascript = implode(",",$tojavascript);
										//$str="";
										for($i = 0; $i < count($grpname); $i++){
											echo "<div class='contentblock1 '>
												<div class='blockcontent'>
													<ul class='maingrp bullet-add clear' >
														<li>
															<div class='themehead'>".$grpname[$i][0]."</div>
															<div class='themeedit'>
																<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
																	<img src='".JUri::base()."components/com_mica/images/edit.png' />
																</a>
																<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
																	&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
																</a>
															</div>
															<ul class='range' >".$str[$i]."</ul>
														</li>
													</ul>
												</div>
											</div>";
										} ?>
										<div class='contentblock3'>
											<script type='text/javascript'>
												var totalthemecount     = "<?php echo count($grpname); ?>";
												var usedlevel           = '<?php echo implode(",",$levelunique); ?>';
												var havingthematicquery = '<?php echo $tojavascript; ?>';
											</script>
											<div id="themeconent">
												<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>" method="get" >
													<table cellspacing="4" cellpadding="0" border="0" width="90%" style="float: left;width: 335px;">
														<tr>
															<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL');?></h3></td>
														</tr>
														<tr>
															<td>
																<b><?php echo JText::_('ATTRIBUTE_LABEL');?></b>
															</td>
															<td>
																<select name="thematic_attribute" id="thematic_attribute" class="inputbox" style="width: 200px;" onchange="getMinmaxVariable_v2(this.value);">
																	<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
																	<optgroup label="Default Variable">
																		<?php $attr = $this->themeticattribute;
																		$eachattr = explode(",",$attr);
																		foreach($eachattr as $eachattrs){
																			echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
																		} ?>
																	</optgroup>
																	<optgroup label="Custom Variable">
																		<?php $attr = $this->customattribute;
																		$eachattr = explode(",",$attr);
																		$eachattr = array_filter($eachattr);
																		foreach($eachattr as $eachattrs){
																			$eachattrs=explode(":",$eachattrs);
																			echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
																		} ?>
																	</optgroup>
																</select>
															</td>
														</tr>
														<tr id="minmaxdisplay">
															<td></td>
															<td class="minmaxdisplay"></td>
														</tr>
														<tr>
															<td>
																<b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b>
															</td>
															<td>
																<input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;">
															</td>
														</tr>
														<tr class="colorhide">
															<td>
																<b><?php echo JText::_('SELECT_COLOR_LABEL');?></b>
															</td>
														<td>
															<input class="simple_color" value="" style="width: 194px;"/>
														</td>
														</tr>
														<tr>
															<td colspan="2">
																<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval"></table>
															</td>
														</tr>
														<input type="hidden" name="maxvalh" id="maxvalh" value="">
														<input type="hidden" name="minvalh" id="minvalh" value="">
														<input type="hidden" name="level" id="level" value="">
														<tr>
															<td colspan="2" align="center">
																<div class="readon frontbutton" style="float: left;">
																	<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" />
																</div>
																<div class="readon frontbutton" style="float: left;padding-left: 5px;">
																	<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup();"/>
																<div>
															</td>
														</tr>
													</table>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<!--End Thematic -->
					<div class="gis" style="width: 100%; border: 1px solid #C5E1FC; height:560px; float:left; margin-bottom:25px">
						<div class="text contenttoggle1">
							<div class="button-area text-right">
								<a href="javascript:void(0);" onclick="downloadMapPdf();" id="options" class="frontbutton" style="text-decoration: none;">
								<input type="button" class="frontbutton" name="Export" value="Export">
								</a></div>
										<div class="button-area text-right">
											<?php  $fromthematic = $session->get('fromthematic'); ?>
										<a href="javascript:void(0);" id="fullscreen" class="frontbutton" <?php if($fromthematic==1){ ?> fromthematic="1" <?php }else{?> fromthematic="0"  <?php }?> >Full Screen
											<!-- <input style="margin-right: 3px;" type="button" class="frontbutton" name="fullscreen" value="Full Screen"> -->
										</a>
									</div><div class="button-area text-right">
										<a href="javascript:void(0);" id="thematic_btn" class="frontbutton" onclick="document.getElementById('light_thematic').style.display='block';">Thematic Query
											<!-- <input style="margin-right: 3px;" type="button" class="frontbutton" name="thematicButton" value="Thematic Query"> -->
										</a></div>


						</div>


						<div id="map"  style="width: 100%; border: 1px solid #C5E1FC; height:580px; float:left; margin-bottom:25px"></div>
					</div>
				</div>
				<!--End GIS -->

				<!--Start Matrix -->
				<div id="quartiles" class="matrix contenttoggle1" style="display:none;overflow:scroll;width:938px;height:277px;">
					<div id="fullscreentable"></div>
				</div>
				<!--End Matrix -->

				<!--Start Speed -->
				<div id="potentiometer" style="display:none;">
					<div class="speedometer contenttoggle1" >
						<div class="filter-section">
							<div style="display:none;" id="speedfiltershow">
								<div class="rightgrptext frontbutton" >
									<input style="margin-left:3px;"type="button" class="frontbutton" value="Edit Filter" name="editfilter"/>
								</div>
							</div>
							<div class="sfilter">
								<div class="top-filter-sec">
									<div class="fl_title">
										<b><?php echo JText::_('FILTER_BY');?></b>
									</div>
									<div class="fl_type">
										<ul>
											<li>
												<input id="filter1" type="radio" name="filter" value="0" class="filterspeed"/>
												<label for="filter1"><?php echo JText::_('FILTER_BY_LAYER');?></label>
											</li>
											<li>
												<input id="filter2" type="radio" name="filter" value="1" class="filterspeed"/>
												<label for="filter2"><?php echo JText::_('FILTER_BY_VARIABLE');?></label>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="sfilter">
								<div>
									<div class="lft speed" style="display:none;" >
										<ul class="filters-list">
											<li>
												<select name="speed_variable" id="speed_variable" class="inputbox" multiple>
													<?php /*<option value="0">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/
													$attr     = $this->themeticattribute;
													$eachattr = explode(",",$attr);
													foreach($eachattr as $eachattrs){
														echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
													} ?>
												</select>
											</li>
											<li>
												<select name="speed_region" id="speed_region" class="inputbox"  multiple >
													<?php /*<option value="0">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/
													foreach($this->customdata as $eachdata){
														echo '<option value='.str_replace(" ","%20",$eachdata->name).'>'.$eachdata->name.'</option>';
													} ?>
												</select>
											</li>
											<li>
												<div class="frontbutton ">
													<input type="button" name="showspeed" id="showspeed" class="" value="Create">
												</div>
											</li>
										</ul>

									</div>
								</div>
							</div>
						</div>
						<div id="spedometer_region"></div>
					</div>
				</div>
				<!--End Speed -->
			</div>
		</div>

		<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
			<!-- <div class="readon frontbutton">
				<input type="button" name="reset" onClick="window.location='index.php?option=com_mica&view=micafront&reset=1';"
					class="readon button" value="<?php echo JText::_('Reset'); ?>" />
			</div> -->
			<!-- <div class="readon frontbutton">
				<input type="submit" name="submit" id="submit" class="readon button" value="<?php //echo JText::_('SHOW_DATA'); ?>" />
			</div> -->
			<input type="hidden" name="refeterview" value="micafront" />
			<input type="hidden" name="option" 		value="com_mica" />
			<input type="hidden" name="zoom" id="zoom" value="6" />
			<input type="hidden" name="view" 		value="showresults" />
			<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
			<input type="hidden" id="comparedata" 	value="0" />
		</div>
	</form>


</div>



<!-- calcultor -->
<div id="light" class="white_content" style="display: none;">
	<!--Loading Please wait....
	<a href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a>-->
	<div class="divclose">
		<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X" />
		</a>
	</div>

	<script type="text/javascript">
		var lastchar = '';
		function moverightarea(){
			var val      = document.getElementById('custom_attribute').innerHTML;
			var selected = 0;
			for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
				var avail_attribute = document.micaform1.avail_attribute;
				if(document.micaform1.avail_attribute[i].selected){
					selected = 1;
					if(lastchar=='attr'){
						alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
					}else{
						document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
						lastchar = 'attr';
					}
				}
			}
			if(selected==0){
				alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
			}
		}

		function addplussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"+";
				lastchar = 'opr';
			}
		}

		function addminussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"-";
				lastchar = 'opr';
			}
		}

		function addmultiplysign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"*";
				lastchar = 'opr';
			}
		}

		function adddivisionsign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"/";
				lastchar = 'opr';
			}
		}

		function addLeftBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+"(";
			checkIncompleteFormula();
		}

		function addRightBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+")";
			checkIncompleteFormula();
		}

		function addPersentage(){
			var val = document.getElementById('custom_attribute').innerHTML;
			//if(lastchar=='opr'){
				//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			//}else{
				document.getElementById('custom_attribute').innerHTML = val+"%";
				lastchar = 'opr';
			//}
		}

		function addNumeric(){
			var val       = document.getElementById('custom_attribute').value;
			var customval = document.getElementById('custom_numeric').value;
			document.getElementById('custom_attribute').innerHTML = val+customval;
			lastchar = '';
		}

		function checkIncompleteFormula(){
			var str   = document.getElementById('custom_attribute').innerHTML;
			var left  = str.split("(");
			var right = str.split(")");

			if(left.length==right.length){
				document.getElementById('custom_attribute').style.color="black";
			}else{
				document.getElementById('custom_attribute').style.color="red";
			}
		}

		function Click(val1){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+val1;
			checkIncompleteFormula();
		}

		function checkfilledvalue(){
			var tmp1 = document.getElementById('custom_attribute').innerHTML;
			var tmp2 = document.getElementById('new_name').value;
			var flg  = 0;
			var len1 = parseInt(tmp1.length) - 1;
			var len2 = parseInt(tmp2.length) - 1;
			if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
				flg = 1;
			}

			if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){
				alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
			}else if(lastchar=='opr' && flg == 0){
				alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
			}else{
				if(!validateFormula()){
					return false;
				}
				addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
			}
		}

		function undofield(){
			document.getElementById('custom_attribute').innerHTML = '';
			lastchar = '';
		}

		JQuery("#editvariable").live("click",function(){
			JQuery('#save').attr("id","updatecustom");
			JQuery('#updatecustom').attr("onclick","javascript:void(0)");
			is_updatecustom=1;
			JQuery('#updatecustom').attr("value","Update");
		});

		JQuery(".customedit").live('click',function(){
			if(is_updatecustom == 1){
				//alert(JQuery(this).find(":selected").text());
				JQuery('#new_name').val(JQuery(this).find(":selected").text());
				JQuery('#new_name').attr('disabled', true);
				oldattrval=JQuery(this).find(":selected").val();
				JQuery('#oldattrval').val(oldattrval);
				JQuery('textarea#custom_attribute').text("");
				lastchar = '';
				moverightarea();
			}
		});
	</script>

	<form name="micaform1" id="micaform1" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="get">
		<table cellspacing="5" cellpadding="0" border="0" width="100%">
			<tr>
				<td colspan="2" align="center">
					<div class="poptitle aa"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" >
					<?php /*<a href="javascript:void(0);" id="editvariable"><img src="< ?php echo JUri::base();? >components/com_mica/images/edit.png" alt="Edit Custom Variable" title="Edit Custom Variable"/>< ?php //echo JText::_('EDIT_CUSTOM_ATTRIBUTE_LABEL');? ></a>*/ ?>
				</td>
				<?php /*<td colspan="2" align="center" ><a href="javascript:void(0);" id="deletevariable">< ?php echo JText::_('DELETE_CUSTOM_ATTRIBUTE_LABEL');? ></a></td>*/ ?>
			</tr>
			<tr>
				<td colspan="2">
					<b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox">
				</td>
			</tr>
			<tr>
				<td width="68%">
					<br><b><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></b>
				</td>
				<td align="right" width="32%">
					<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox">
					<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="button" style="width: 77px;" value="Add Value" onclick="addNumeric();">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2">
					<table cellspacing="0" cellpadding="0" border="1" width="100%">
						<tr valign="top">
							<td width="25%">
								<select name="avail_attribute" id="avail_attribute" size="10" class="avial_select">
									<optgroup label="Default Variable" class="defaultvariable">
										<?php
										$attr= $this->themeticattribute;//themeticattribute
										$eachattr=explode(",",$attr);
										foreach($eachattr as $eachattrs){
											echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
										} ?>
									</optgroup>
									<?php /*
									<optgroup label="Custom Variable" class="customedit">
										<?php $attr= $this->customattributelib;
										$eachattr=explode(",",$attr);
										$eachattr=array_filter($eachattr);
										foreach($eachattr as $eachattrs){
											$eachattrs=explode(":",$eachattrs);
											echo "<option value='".$eachattrs[1]."'>".JTEXT::_($eachattrs[0])."</option>";
										} ?>
									</optgroup> */ ?>
									<?php /*
									<optgroup label="Custom Library Variable" class="defaultvariablelib">
										<?php $attr= $this->customattributelib;
										$eachattr=explode(",",$attr);
										$eachattr=array_filter($eachattr);
										foreach($eachattr as $eachattrs){
											$eachattrs=explode(":",$eachattrs);
											echo "<option value='".$eachattrs[1]."'>".JTEXT::_($eachattrs[0])."</option>";
										} ?>
									</optgroup>*/ ?>
								</select>
							</td>
							<td width="2%"></td>
							<td width="37%">
								<table  border="1" align="center" cellpadding="0" cellspacing="0" class="cal_t">
									<tr>
										<td colspan="5">
											<input type="button" title="<?php echo JText::_('MOVE_RIGHT_TIP');?>" name="moveright" id="moveright" class="button" style="width: 98%" value=">>" onclick="moverightarea();">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="button"  value="+" onclick="addplussign();">
										</td>
									   	<td width="60">
									   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="button"  value="-" onclick="addminussign();">
									   	</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="button"  value="*" onclick="addmultiplysign();">
										</td>
									   	<td width="60">
									   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="button"  value="/" onclick="adddivisionsign();">
									   	</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="button"  value="%" onclick="addPersentage();">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="button">
										</td>
										<td width="60">
											<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="button">
										</td>
									   	<td width="60">
									   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="button">
									   	</td>
										<td width="60">
											<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="button">
										</td>
									 	<td width="60">
									 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="button">
									 	</td>
									</tr>
									<tr>
										<td width="60">
											<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="button">
										</td>
										<td width="60">
											<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="button">
										</td>
										<td width="60">
											<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="button">
										</td>
										<td width="60">
											<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="button">
										</td>
										<td width="60">
											<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="button">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="button"  value="(" onclick="addLeftBraket();">
										</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="button"  value=")" onclick="addRightBraket();">
										</td>
									</tr>
								</table>
							</td>
						 	<td width="2%"></td>
							<td width="32%">
								<div class="t_area_t">
									<textarea name="custom_attribute" id="custom_attribute" rows="10" cols="30" readonly="readonly" class="inputbox"></textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="5" >
								<div  class="frontbutton" >
									<input type="button" name="save" id="save" class="button" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
								</div>
								<div class="frontbutton" >
									<input type="button" name="clear" id="clear" class="button" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="undofield();">
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>
<!-- calculatror end -->


<script type="text/javascript">

	var siteurl = '<?php echo JURI::base();?>';
	$(document).ready(function()
	{
		if(typeof(usedlevel) != "undefined"){
			var ulevel=usedlevel.split(",");
			if(ulevel.length == 3){
				JQuery("#themeconent").css({"display":"none"});
			}
			getColorPallet_v2();
		}




		$('.allcheck').click(function(event) {
			/* Act on the event */
			var id      = $(this).attr('id');
			var input   = id.split('_');
			$("." +input[0]+"_checkbox").prop("checked",$(this).prop("checked"));

			if(input[0]=="state")
			{
				var allVals = [];
				var slvals = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getfrontdistrict_v2(selected);

			}
			else if(input[0]=="district")
           {

             	var allVals = [];
				var slvals = [];
				$('.district_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getdistrictattr_v2(selected);
           }
           else if(input[0]=="variable")
           {

             	var allVals = [];
				var slvals = [];
				$('.variable_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getVariableList_v2(selected);
           }
           else
           {


           }
        });

		//var choosenjq = jQuery.noConflict();
		var graphdist = JQuery("#district").val();

		// JQuery.ajax({
		// 	url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
		// 	type    : 'POST',
		// 	data    : "dist="+graphdist,
		// 	success : function(data){
		// 		JQuery("#selectdistrictgraph").html(data);
		// 	}
		// });

		 getAttribute_v2(5,"district");


		 if(typeof(preselected) != "undefined"){
			//JQuery('#dataof').change();
			displaycombo_v2();
			getVariableList_v2();
		}

		var minval        = "";
		var maxval        = "";
		var colorselected = "";
		var edittheme     = 0;

		JQuery(".hideothers").css({"display":"none"});
		//JQuery('#attr').click();
		if(typeof(havingthematicquery)!="undefined")
		{
			var singlequery = havingthematicquery.split(",");
			JQuery("#thematic_attribute option").each(function(){
				for(var i=0;i<=singlequery.length;i++){
					if(singlequery[i]==JQuery(this).val()){
						JQuery(this).attr("disabled",true);
					}
				}
			});
		}

			// $('#state_allcheck').click(function(event) {
			// 	/* Act on the event */
			// 	$(".state_checkbox").prop("checked",$(this).prop("checked"));
			// });

			//for select all to work for all the tabs

	    //For check all function to work well with all other checkboxes:- start

		$('.leftcontainer').on("change",'.list1 input[type=checkbox]', function(){
			var classname =$(this).attr('class');
			//classname = classname.split(" ");
			if(classname.indexOf("_checkbox")!=-1)
			{
				end   = classname.indexOf("_checkbox");
				start = classname.indexOf(" ")!=-1?classname.indexOf(" "):0 ;
				result = classname.substring(start, end).trim();

			}
	        //console.log('#'+result+'_allcheck');
			if($(this).prop('checked')==false)
			{
				//console.log('#'+result+'_allcheck');
				$('#'+result+'_allcheck').prop('checked', false); //works with mpi_allcheck, swcs_checkbox, swcs_allcheck
			}
			else
			{
				//console.log("222");
				if($('.'+result+'list input[type=checkbox]:checked').length == $('.'+result+'list input[type=checkbox]').length)
				{
					$('#'+result+'_allcheck').prop('checked', true);
				}
				else
					$('#'+result+'_allcheck').prop('checked', false);
			}

		});

    	//For check all function to work well with all other checkboxes:- END

    	//for search START

        $('.searchtop').keyup(function(event) {
		    var input, filter, a, id;
		    id      = $(this).attr('id');
		    input   = id.split('_');
		    filter  = this.value.toUpperCase();

		    $("."+ input[0] +"list li").each(function(){
		    	a = $('label',this).text();
				if (a.toUpperCase().indexOf(filter) > -1)
		            $(this).show();
		        else
		            $(this).hide();
		   });
		});
		//for search END

		$('#district_tab').click(function(e){
			if($('.state_checkbox:checked').length == 0){
				alert(statealert);
				return false;
			}

			JQuery('#state_tab :selected').each(function(i, selected) {
				myurl +=JQuery(selected).val()+",";
				zoom=5;
			});
		});

		$('#type_tab').click(function(e){
			if($('.state_checkbox:checked').length == 0){
				alert(statealert);
				return false;
			}

			if($('.district_checkbox:checked').length == 0){
				alert(districtalert);
				return false;
			}
		});

		$('#variable_tab').click(function(e){
			if($('.state_checkbox:checked').length == 0){
				alert(statealert);
				return false;
			}

			if($('.district_checkbox:checked').length == 0){
				alert(districtalert);
				return false;
			}

			if($('.type_checkbox:checked').length == 0){
				alert(typealert);
				return false;
			}
		});

		$('#mpi_tab').click(function(e){
			if($('.state_checkbox:checked').length == 0){
				alert(statealert);
				return false;
			}

			if($('.district_checkbox:checked').length == 0){
				alert(districtalert);
				return false;
			}

			if($('.type_checkbox:checked').length == 0){
				alert(typealert);
				return false;
			}

		});

		$('#swcs_tab').click(function(e){
			if($('.state_checkbox:checked').length == 0){
				alert('statealert');
				return false;
			}

			if($('.district_checkbox:checked').length == 0){
				alert('districtalert');
				return false;
			}

			if($('.type_checkbox:checked').length == 0){
				alert('typealert');
				return false;
			}
		});


		$('.state_checkbox').click(function(e){
				var slvals = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getfrontdistrict_v2(selected);
		})

		$('.district_checkbox').click(function(e){
				var slvals = [];
				$('.district_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',') ;
				getAttribute_v2(selected);
		})

		// 	JQuery(".variablegrp").live("click",function(){
		// 	var myid = JQuery(this).attr("id");
		// 	var dis  = JQuery('.'+myid).css("display");

		// 	JQuery(".hideothers").css({"display":"none"});
		// 	JQuery('.variablegrp').addClass("deactive");
		// 	JQuery(this).addClass("active");
		// 	JQuery(this).removeClass("deactive");
		// 	if(dis=="block"){
		// 		JQuery('.'+myid).css("display","none");
		// 		JQuery(this).addClass("deactive");
		// 		JQuery(this).removeClass("active");
		// 	}else{
		// 		JQuery('.'+myid).css("display","block");
		// 		JQuery(this).addClass("active");
		// 		JQuery(this).removeClass("deactive");
		// 	}
		// });
	});


	//REVERSE AJAX IN STATE
	function getfrontdistrict_v2(val){
      	//alert(val);loader

    	// $( "#loader").show();
 	 	//console.log('shown')
	   if(val == "" ){
		   	//$('.state_checkbox').prop('checked', true);
			JQuery('.level2').css({'display':'none'});
			getAttribute_v2(5,"state");
			//document.getElementById('m_type').selectedIndex=0;
			JQuery(".distattrtypeselection").css({"display":"none"});

			return false;
		}else if(val=="all"){

			document.getElementById('district').selectedIndex=0;
			document.getElementById('urban').selectedIndex=0;
			document.getElementById('town').selectedIndex=0;
			//document.getElementById('m_type').selectedIndex=0;
			JQuery('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({'display':'none'});
			getAttribute_v2(5,"state");

			return false;
		}else{
			getAttribute_v2(5,"state");
			//document.getElementById('m_type').selectedIndex=0;
			JQuery(".distattrtypeselection").css({"display":"none"});
		}
	 	$( "#loader").show();
 		// console.log('shown')

		JQuery('.distattrtypeselection').css({'display':'none'});



		JQuery.ajax({
		    url     : "index.php?option=com_mica&task=micafront.getsecondlevel&stat="+val+"&preselected="+preselecteddata,
			method  : 'post',
			success : function(data){
				//$( "#loader").show();
	 	        // console.log('shown')

				var segment = data.split("split");
				//var segment = data.split("split");

				JQuery(".districtlist").html(segment[0]);
				JQuery("#statecode").html(segment[1]);
				console.log(JQuery("#statecode").html(segment[1]));

				if(preselecteddata != ""){
					getAttribute_v2(5,"district");
				}
	            var dataof       = JQuery("#dataof").val();

				if(typeof(preselecteddataof)!="undefined")
				{
					var selectedtype = preselecteddataof.toLowerCase();
					if(selectedtype == "ua" || selectedtype == "UA"){
						selectedtype="urban";
					}


				}
				if(dataof == "ua" || dataof == "UA"){
						dataof="urban";
					}

					$( "#loader").hide();
					//console.log('hidden')

				/*if(typeof(preselecteddata)!="undefined" && dataof.toLowerCase()==selectedtype){
					JQuery('#'+selectedtype).val(preselecteddata).attr("select","selected");
					JQuery('#'+selectedtype).change();
					alert("in1");
					eval("get"+selectedtype(preselecteddata));
				}*/
			}
		});
	}

	function getdistrict_v2(val){
		if(val == "" ){
			JQuery('.level2').css({'display':'none'});
			getAttribute_v2(5,"state");
			return false;
		}else if(val == "all"){
			JQuery('.level2').css({'display':'none'});
			getAttribute_v2(5,"state");
			return false;
		}else{
			getAttribute_v2(5,"state");
		}

		JQuery('.distattrtypeselection').css({'display':'none'});

		JQuery.ajax({
			url     : "index.php?option=com_mica&&task=micafront.getsecondlevel&stat="+val,
			method  : 'post',
			success : function(combos){
				JQuery('.level2').css({'display':'block'});
				var segment = combos.split("split");
				JQuery("#districtspan").html(segment[0]);
				JQuery("#villagespan").html(segment[0]);
				//choosenjq("select").chosen();
			}
		});
	}

	function getdistrictattr_v2(val){

		var val=JQuery('.district_checkbox').val();
		   $("#loaderdistrict").show();
		 	 //console.log('shown')

		//alert(val);
		if(val==""){
			JQuery('.townspan').css({'display':'block'});
			JQuery('.urbanspan').css({'display':'block'});
			JQuery('.distattrtypeselection').css({'display':'none'});
			resetCombo('district');

			getAttribute_v2(5,"state");
		}else{
			getAttribute_v2(5,"district");
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=micafront.getsecondlevelvillage&villagedistrict="+val,
				method  : 'post',
				success : function(combos){
					var segment = combos.split("split");
					JQuery('.villagelist').html(segment[0]);
					//choosenjq("select").multiselect({selectedList: 1});
					var dataof = JQuery("#dataof").val();

					if(dataof == "ua" || dataof == "UA"){
						dataof="urban";
					}
					 $("#loaderdistrict").hide();
		 	         //console.log('hidden')
				}
			});
		}
		//JQuery('.htitle').css({'width':'150px'});
	}

	//REVERSE AJAX IN STATE
	function getVariableList_v2(){
		if(typeof(JQuery("#variablegrp"))!="undefined")
		{
			var grplist = JQuery("#variablegrp").val();
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.getAttribute&zoom=5&type=district&grplist="+grplist,
				method  : 'GET',
				async   : true,
				success : function(data){
					JQuery("#grplist").html(data);
					//choosenjq("#attributes").multiselect({selectedList: 1,noneSelectedText: 'Select Variable '});
				}
			});
		}
	}


	// AJAX IN STATE

	function getAttribute_v2(javazoom,type){

		//var district=JQuery('.district_checkbox').val();

        var allVals = [];
		var slvals = [];
		$('.district_checkbox:checked').each(function() {
			slvals.push($(this).val())
		})
		selected = slvals.join(',') ;

		JQuery.ajax({
			url     :"index.php?option=com_mica&task=showresults.getAttribute&zoom="+parseInt(javazoom)+"&type="+type+"&district="+selected,
			method  : 'GET',
			async   : true,
			success : function(data){
				var segment = data.split("split");

				JQuery("#variables").html(segment[0]);

				JQuery("#variableshortcode").html(segment[1]);
				JQuery(".hideothers").css({"display":"none"});

					JQuery(".variablegrp").addClass("deactive");

					//choosenjq("#variablegrp").multiselect({selectedList: 1,noneSelectedText: 'Select Variable Group'});

				//getVariableList_v2();
			}
		});
	}

	function getData()
	{

		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		if($('.district_checkbox:checked').length == 0){
			alert(districtalert);
			return false;
		}

		if($('.type_checkbox:checked').length == 0){
			alert(typealert);
			return false;
		}

		if($('.variable_checkbox:checked').length == 0){
			alert(variablealert);
			return false;
		}

		/*if($('.mpi_checkbox:checked').length == 0){
			alert(mpialert);
			return false;
		}

		if($('.swcs_checkbox:checked').length == 0){
			alert(swcsalert);
			return false;
		}*/


			//var grplist = JQuery("#variablegrp").val();
			var data = $('#micaform').serialize();
	        JQuery.ajax({
				url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
				//url     : "index.php?option=com_mica&task=micafront.getDataajax",
				method  : 'POST',
				async   : true,
				data    : data,
				success : function(data){
					JQuery("#result_table").html(data);
                    loadCharts();

					// JQuery("#result_table").find("#textscroll").jScrollPane({verticalArrowPositions: 'os',horizontalArrowPositions: 'os',autoReinitialise: true,autoReinitialiseDelay:1000});

					var graphdist="";
					JQuery('.district_checkbox:checked').each(function(i, selected) {

							graphdist +=JQuery(this).val()+",";
					});
					graphdist= graphdist.slice(0, -1);
					JQuery.ajax({
						url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
						type    : 'POST',
						data    : "dist="+graphdist,
						success : function(data){
							JQuery("#result_table").find("#selectdistrictgraph").html(data);
							//set custom_variables = custom Attributes under All Variables List
							//JQuery('.variablelist').append(JQuery("#result_table").find(".custom_ver").html());



						}
					});

					//console.log(JQuery("#result_table"));
					//choosenjq("#attributes").multiselect({selectedList: 1,noneSelectedText: 'Select Variable '});
				}
			});

	}

	/*
	Load Charts
	*/
	function loadCharts()
	{
		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		if($('.district_checkbox:checked').length == 0){
			alert(districtalert);
			return false;
		}

		if($('.type_checkbox:checked').length == 0){
			alert(typealert);
			return false;
		}

		if($('.variable_checkbox:checked').length == 0){
			alert(variablealert);
			return false;
		}
		var data = $('#micaform').serialize();
		JQuery.ajax({
		    url     : "index.php?option=com_mica&task=showresults.getGraphajax",
			method  : 'post',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
	        },
			success : function(data){
				jQuery('#main_loader').hide();
				JQuery("#graph").show();
				JQuery("#result_table").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();

				/*------------------bakground coclor------------*/
 				var backgroundColor = 'white';
				Chart.plugins.register({
				    beforeDraw: function(c) {
				        var ctx = c.chart.ctx;
				        ctx.fillStyle = backgroundColor;
				        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
				    }
				});

                /*--------------bakground coclor end------------*/






				var x = document.getElementById("chartype").value;
				var ctx = document.getElementById('myChart').getContext('2d');
				var datasets = [];
				var i =0;
				var result = JQuery.parseJSON(data);
				var color = Chart.helpers.color;
				var colorcode= ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
				var labels =[];
				var joinedClassesmain=[];
				var i =0;
				var keys= JQuery.map(result, function(element,index) {return index});

				var joinedClasses = new Array();
				JQuery.each(result,function(key,value){
					JQuery.each(value,function(key1,value1){
						if(i==0)
						{
							labels.push(key1);
						}
							if (joinedClasses[key1] === undefined)
							{
								joinedClasses[key1]= new Array();
							}
							joinedClasses[key1][key]=value1;// vaule define
							});
					    i++;
					});
				i=0;
				JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
					var res = value.split("~~");
					var cityname=res[0];
                    var citycode=res[1];


                   var container = $('#cblist');


				   var statecity = $('<input />', { type: 'checkbox',  class: 'chartstates_checkbox', checked: 'checked',id: 'chartstates_'+citycode, value: citycode }).appendTo(container);
  					$('<label />', { 'for': 'check', text: cityname }).appendTo(container);


					var newres = Object.keys(joinedClasses[value]).map(function(k) {
				  		return [k, joinedClasses[value][k]];
						});

					var joinedClassesmain= [];
					var radius= [];
					$.each(newres, function (key1,value1) {


				        joinedClassesmain.push(value1[1]);
				        radius.push(value1[1]);

				    });
				    var container1 = $('#variabelist');
				    var variable1= [];

				    if(i==0)
				    {
						$.each(newres, function (key1,value1) {
					    	variable1=value1[0];
					    	var variabel=$('<input />', { type: 'checkbox',  class: 'chartvariables_checkbox', checked: 'checked',  id: 'chartvariables_'+variable1, value: variable1 }).appendTo(container1);
					    $('<label />', { 'for': 'chart_variables_checkbox', text: variable1 }).appendTo(container1);
					    });
					}
					datasetValues[citycode] = joinedClassesmain;
					datasets.push({
                            'label':cityname, //city name should come here
                            'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
				 			'borderColor':colorcode[i],
				 		 	'borderWidth':1,
				 		 	'data':joinedClassesmain,

				 		 	'id':citycode,
				 		 	'id1':variable1


				 		});
					i++;
				});

                    chart1 = new Chart(ctx, {
				    type: x,

				    // The data for our dataset
				    data: {
				         labels: keys,
				         datasets: datasets

				          },
				    options: {
				    	 legend: {
				      display: false,
				    },


				    }
					});
            	}
			});
	}

	JQuery('#graph').on('click','.chartvariables_checkbox',function(){
		//console.log(chart1);
		var variabelname = jQuery(this).val();

		var indexoflabels = $.inArray( variabelname, chart1.data.labels );
		//console.log(trashedData);

		if(indexoflabels != -1)
		{

			 if(document.getElementById('chartvariables_'+variabelname).checked)
			 {

			 }
			 else
			 {

				 var templabel = chart1.data.labels.splice(indexoflabels, 1);
				 //console.log(templabel);
				 trashedLabels.push(templabel[0]);
				//console.log(trashedLabels);

				 chart1.data.datasets.forEach(function(val,index)
				 {


				   	var citycode =val.id;
				  	var tempdata = val.data.splice(indexoflabels, 1);
				  	console.log(tempdata);
				  	if(trashedData[citycode]==undefined)
				  		trashedData[citycode]=[];
				  	trashedData[citycode][templabel[0]]=tempdata[0];



				 });


			 }

		}
		else
		{
			var indexoflabels_t = $.inArray( variabelname, trashedLabels );
			console.log("erer", indexoflabels);
			if(indexoflabels_t != -1)
			{
				chart1.data.labels.push(variabelname);
				chart1.data.datasets.forEach(function(val,index)
	             {
	             	tempdata = trashedData[val.id][variabelname];

	     				val.data.push(tempdata);
	             });

			}


		}

		chart1.update();
		//alert(variabelname);
	});

	JQuery('#graph').on('click','.chartstates_checkbox',function(){

		var state_id = jQuery(this).val();

		chart1.data.datasets.forEach(function(val,index){

			if(val.id==state_id)
			{
	           if(document.getElementById('chartstates_'+state_id).checked) {
			       val._meta[0].hidden=false;
				     return;

				} else {
				     val._meta[0].hidden=true;
				     return;
				}


			}
			chart1.update();

		})

	});

	$("#enablelegend").click(function() {
	   chart1.options.legend.display=true;
	  chart1.update();
	  if(document.getElementById('enablelegend').checked)
			 {
			 	chart1.options.legend.display=true;
	  			chart1.update();

			 }
			 else
			 {
			 	chart1.options.legend.display=false;
	  			chart1.update();


			 }

	});

/*-------------export -----------*/
document.getElementById('export').addEventListener("click", downloadPDF);
function downloadPDF() {

  var canvas = document.querySelector('#myChart');
    //creates image
    var canvasImg = canvas.toDataURL("image/jpeg", 1.0);

    //creates PDF from img
    var doc = new jsPDF('landscape');
    doc.setFontSize(20);
    doc.text(15, 15, "Cool Chart");
    doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
    doc.save('canvas.pdf');
}



/*-------------export end-----------*/








	function gettown_v2(val){
		//JQuery('.htitle').css({'width':'150px'});
		JQuery('.distattrtypeselection').css({'display':'none'});
		JQuery('.districtspan').css({'display':'none'});

		JQuery('.urbanspan').css({'display':'none'});
		if(val == ""){
			JQuery('.districtspan').css({'display':'block'});

			JQuery('.urbanspan').css({'display':'block'});
		}
		getAttribute_v2(8,"town");
	}

	function geturban_v2(val){
		JQuery('.htitle').css({'width':'150px'});
		JQuery('.distattrtypeselection').css({'display':'none'});
		if(val == ""){
			JQuery('.districtspan').css({'display':'block'});

			JQuery('.townspan').css({'display':'block'});
		}
		getAttribute_v2(8,"urban");
	}

	function getvalidation_v2(){
		var myurl = "";
		var zoom  = 5;
		if(document.getElementById('state').value==''){
			//alert(statealert);
			return false;
		}else{
			myurl = "state=";
			JQuery('#state :selected').each(function(i, selected) {
				myurl +=JQuery(selected).val()+",";
				zoom=5;
			});

			myurl +="&district=";
			JQuery('#district :selected').each(function(i, selected) {
				myurl += JQuery(selected).val()+",";
				zoom  = 6;
			});
			myurl +="&attributes=";

			JQuery('input.statetotal_attributes[type=checkbox]').each(function () {
				if(this.checked){
					myurl +=JQuery(this).val()+",";
				}
			});
			JQuery("#zoom").val(zoom);
			//window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=showresults&Itemid=108";
			return false;
		}
	}

	function checkRadio_v2(frmName,rbGroupName){
		var radios = document[frmName].elements[rbGroupName];
		for(var i=0;i<radios.length;i++){
			if(radios[i].checked){
		   		return true;
		  	}
		}
		return false;
	}

	function changeWorkspace(value){

		if(value == 0){
			JQuery('.createneworkspace').css({'display':'block'});
			JQuery('.createnewworkspace').css({'display':''});
			JQuery('.thematiceditoption').css('display','block');
		}else{
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid="+value+"&tmpl=component",
				method  : 'GET',
				success : function(data){
					result = JQuery.parseJSON(data);

					$("#workspacceedit").load(location.href+" #workspacceedit>*","");
					$('#leftcontainer').load(location.href+" #leftcontainer>*","", function() {
				  		getAttribute_v2(5,"district");

						if(typeof(preselected) != "undefined"){
							displaycombo_v2();
							getVariableList_v2();
						}
					});
					//$("#leftcontainer").load(location.href+" #leftcontainer>*","");

					$("#activeworkspacename").html(result[0].name);
					$("#workspacceedit").hide();

					//update form fields  as per selected workspace id
					setTimeout(function() {
						getDataNew();
					}, 1000);

					//event.preventDefault();
					//JQuery("#result_table").html(data);
					//JQuery("#workspacceedit").html(data);
					//console.log(data);
				}
			});
			//window.location = "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid="+value;
		}
	}

	JQuery("#createworkspace").live("click",function(){
		var workspacename = JQuery("#new_w_txt").val();
		if(workspacename == ""){
			alert("Please Enter workspace name");
			return false;
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.saveWorkspace&name="+workspacename,
			method  : 'GET',
			success : function(data){
				$("#workspacceedit").load(location.href+" #workspacceedit>*","");
				$("#activeworkspacename").text(workspacename);
			}
		});
		//window.location = "index.php?option=com_mica&task=showresults.saveWorkspace&name="+workspacename;
	});

	JQuery("#updateworkspace").live("click",function(){
		var workspacename       = JQuery("#new_w_txt").val();
		var workspaceid         = JQuery("#profile").val();
		var updateworkspacetext = $("#updateworkspacetext").val();

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid,
			method  : 'GET',
			success : function(data){
				    $("#workspacceedit").load(location.href+" #workspacceedit>*","");
					$("#activeworkspacename").text(updateworkspacetext);
					//window.location = 'index.php?option=com_mica&view=showresults&Itemid=108&msg=1';
			}
		});
	});

	JQuery("#deleteworkspace").live("click",function(){
		if(!confirm("Are you Sure to Delete Workspace?")){
			return false;
		}

		var workspaceid = JQuery("#profile").val();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteWorkspace&workspaceid="+workspaceid,
			method  : 'GET',
			success : function(data){
				//window.location = 'index.php?option=com_mica&view=micafront&Itemid=100&msg=-1';
				$("#workspacceedit").load(location.href+" #workspacceedit>*","");
				$('#activeworkspacename').text("");
			}
		});
	});

	JQuery("#submit").live("click",function(){
		if(JQuery("#comparedata").val() == 1){
			var statecompare    = JQuery("#statecompare").val();
			var districtcompare = JQuery("#districtcompare").val();
			var towncompare     = JQuery("#towncompare").val();
			var urbancompare    = JQuery("#urbancompare").val();

			if(urbancompare != ""){
				if(urbancompare == "all"){
					alert("All UA already Selected");
					return false;
				}
				var urban=JQuery("#urban").val();
				if(urban == ""){
					alert("Please Select UA to compare");
					return false;
				}else if(urban == "all"){
					alert("You can not select All UA to compare");
					return false;
				}
			}else if(towncompare != ""){
				if(towncompare == "all"){
					alert("All Towns already Selected");
					return false;
				}
				var town=JQuery("#town").val();
				if(town == ""){
					alert("Please Select Town to compare");
					return false;
				}else if(town == "all"){
					alert("You can not select All Town to compare");
					return false;
				}
			}
			else if(districtcompare!="")
			{
				if(districtcompare=="all")
				{
					alert("All Districts already Selected");
					return false;
				}
				var dist = JQuery("#district").val();
				if(dist == ""){
					alert("Please Select District to compare");
					return false;
				}
				else if(dist=="all")
				{
					alert("You can not select All District to compare");
					return false;
				}
			}
			else if(statecompare!="")
			{
				if(statecompare=="all")
				{
					alert("All States already Selected");
					return false;
				}
				var stateval=JQuery("#state").val();
				if(stateval=="")
				{
					alert("Please Select State to compare");
					return false;
				}
				else if(stateval=="all")
				{
					alert("You can not select All State to compare");
					return false;
				}
			}
			return true;

		}else{
			var resetmtype      = 0;
			var checked         = 0;
			var dataof          = JQuery("#dataof").val();
			var districtcompare = JQuery("#district").val();
			if(dataof=="District"){
				if(JQuery("#district").val()==""){
					alert("Please Select District First");
					return false;
				}
			}
			else if(dataof=="UA")
			{
				if(JQuery("#urban").val()=="")
				{
					alert("Please Select UA First");
					return false;
				}
			}
			else if(dataof=="Town")
			{
				if(JQuery("#Town").val()=="")
				{
					alert("Please Select Town First");
					return false;
				}

			}

			JQuery("input:checkbox:checked").each(function(){
				var chk = JQuery(this).attr("checked");
				if(chk=="checked"){
					checked=1;
				}
			});

			if(checked==0){
				alert("Please select variable first");
				return false;
			}else{
				if(typeof(districtcompare)=="undefined" || districtcompare==""){
					//JQuery("#m_type").remove();
					//alert(JQuery("#m_type").val());
					return true;
				}
			}
		}
	});

	function getMinmaxVariable_v2(value){
		JQuery(".minmaxdisplay").html("Please Wait...");
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getMinMax&value="+encodeURIComponent(value),
			method  : 'GET',
			success : function(data){
				var segment = data.split(",");
				minval = segment[0];
				maxval = segment[1];
				JQuery(".minmaxdisplay").html("<b>MIN :</b>"+segment[0]+"<b><br/>MAX :</b>"+segment[1]+"");
				JQuery("#maxvalh").val(maxval);
				JQuery("#minvalh").val(minval);
			}
		});
	}

	JQuery("#no_of_interval").live("keyup",function(e){
		var str="";
		if(e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105){
			if(typeof(edittheme)=="undefined"){
				edittheme=0;
			}
			createTable_v2(edittheme);
		}
	});


	JQuery("#no_of_interval").live("keyup",function(e){
		var str="";
		if(e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105){
			if(typeof(edittheme)=="undefined"){
				edittheme=0;
			}
			createTable_v2(edittheme);
		}
	});

	function createTable_v2(edittheme){

		var maxval      = JQuery("#maxvalh").val();
		var minval      = JQuery("#minvalh").val();
		var level       = JQuery("#level").val();
		var str         = "";
		var diff        = maxval-minval;
		var max         = minval;
		var disp        = 0;
		var setinterval = diff/(JQuery("#no_of_interval").val());
			setinterval = setinterval.toFixed(2);
		start = minval;
		//alert(edittheme);
		if(JQuery("#no_of_interval").val()>5){
			alert("Interval must be less then 5");
			return false;
		}
		var colorselected="";
		if(edittheme!=1 && usedlevel.indexOf("0")==-1){
			str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
			JQuery(".colorhide").css({"display":""});
			colorselected=1;
		}else if(edittheme==1 && level=="0"){
			str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
			JQuery(".colorhide").css({"display":""});colorselected=1;
		}else{
			str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Pin</th></tr></thead>';
			JQuery(".colorhide").css({"display":"none"});colorselected=0;
		}

		for(var i=1;i<=JQuery("#no_of_interval").val();i++){
			if((edittheme!=1 && usedlevel.indexOf("0")==-1) ){

				end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
				str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
				start = end;

			}else if(edittheme!=1 && usedlevel.indexOf("0")!=-1 && level!="0"){

				if(usedlevel.indexOf("1")==-1){
					level=1;JQuery("#level").val(1);
				}else{
					level=2; JQuery("#level").val(2);
				}

				end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
				str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
				start = end;

			}else if(edittheme==1 && level!="0"){

				end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
				str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
				start = end;

			}else if(edittheme==1 && usedlevel.indexOf("0")!=-1 && level=="0"){

				end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
				str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
				start = end;

			}else{

				end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
				str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ></tr>';
				start = end;

			}
		}

		JQuery("#displayinterval").html(str);
		if(colorselected == 1)
		{
			JQuery('.simpleColorChooser').click();
		}
		//return str;
	}

	JQuery('.simpleColorChooser').live("click",function()
	{
		var value = JQuery('.simple_color').val();
			value = value.replace("#","");
		var steps = JQuery("#no_of_interval").val();

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getColorGradiant&value="+encodeURIComponent(value)+"&steps="+parseInt(steps++),
			method  : 'GET',
			success : function(data){
				colorselected = data;
				var segment = data.split(",");
					segment = segment.reverse();

				for(var i=1;i<=segment.length;i++)
				{

					var ncol = "#"+JQuery.trim(segment[i]);
					JQuery("#color"+i).fadeIn(1000,JQuery("#color"+i).css({"background-color":ncol}));
				}
			}
		});
	});

	function getColorPallet_v2(){

		JQuery('.simple_color').simpleColor({
			cellWidth   : 9,
			cellHeight  : 9,
			border      : '1px solid #333333',
			buttonClass : 'colorpickerbutton'
		});
	}

	JQuery("#savesld").live("click",function(){
		var formula     = JQuery("#thematic_attribute").val();
		//var condition = JQuery("#condition").val();
		var limit       = JQuery("#no_of_interval").val();
		var level       = JQuery("#level").val();
		if(formula=="")
		{
			alert("Please Select Variable");
			return false;
		}
		else if(limit=="")
		{
			alert("Please Select Interval");
			return false;
		}

		var from  ="";
		var to    ="";
		var color ="";
		for(var i=1;i<=limit;i++)
		{
			from +=JQuery("#from"+i).val()+",";
			to   +=JQuery("#to"+i).val()+",";

			if(level=="0" || level=="")
			{
				color +=rgb2hex(JQuery("#color"+i).css("background-color"))+",";
			}
			else
			{
				color +=i+",";
			}
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
			method  : 'GET',
			success : function(data){
				//window.location.href  =window.location;
				JQuery("#gis").load(" #gis > *");
				JQuery("#gisdata").click();
				JQuery("#blockcontent").load(" #blockcontent > *");
				document.getElementById('light_thematic').style.display = 'none';

				//JQuery(".tq").hide();

				//map.addLayers([districts]);
			}
		});
	});

	JQuery(".deletegrp").live("click",function(){
		var level    = JQuery(this).attr("class");
		var getlevel = level.split(" ");
		var getdelid = JQuery(this).attr("id");
		var segment  = getdelid.split("del_");

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deletethematicQueryToSession&formula="+encodeURIComponent(segment[1])+"&level="+getlevel[1],
			method  : 'GET',
			success : function(data){
				window.location = '';
			}
		});
	});

	function rgb2hex(rgb) {
		if(typeof(rgb)=="undefined"){
			return "ffffff";
		}

	    if (  rgb.search("rgb") == -1 ) {
	        return rgb;
	    } else {
			rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
			function hex(x) {
				return ("0" + parseInt(x).toString(16)).slice(-2);
			}
			return  hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	    }
	}

	JQuery(".edittheme").live("click",function()
	{
		edittheme=1;
		var cnt = JQuery("#themeconent").html();

		JQuery("#themeconent").html("");
		JQuery(".themeconent").html(cnt);
		JQuery('.simpleColorContainer').remove();

		getColorPallet_v2();

		var classname = JQuery(this).attr("class");
		var myid      = this.id;
		var getcount  = classname.split(" ");
		var seg       = myid.split("__");
		//JQuery("#thematicquerypopup").click();
		JQuery("#thematic_attribute").val(seg[0]);
		JQuery("#level").val(seg[1]);

		JQuery("#thematic_attribute").change();
		JQuery("#no_of_interval").val(getcount[1]);
		createTable_v2(edittheme);
		JQuery("#no_of_interval").val(getcount[1]);
		JQuery(".range_"+seg[1]).each(function(i){
			i++;
			rangeid     = (JQuery(this).attr("id"));
			var mylimit = rangeid.split("-");
			JQuery("#from"+i).val(mylimit[0]);
			JQuery("#to"+i).val(mylimit[1]);
		});

		var endcolor = "";
		if(seg[1] == "0"){
			for(var i=1;i<=getcount[1];i++){
				JQuery("#color"+(i)).css({"background-color":""+JQuery(".col_"+i).css("background-color")});
				if(getcount[1] == i){
					JQuery(".simpleColorDisplay").css({"background-color":JQuery(".col_"+i).css("background-color")});
				}
				//rangeid=JQuery(".range"+i).attr("id");
			}
		}
		//	JQuery("#thematicquerypopup").click();
		thematicquerypopup_v2();
	});

	function thematicquerypopup_v2(){
		//alert(totalthemecount);

		if(totalthemecount==3 && (typeof(edittheme)=="undefined" || (edittheme)==0)){
			alert("You Can Select maximum 3 Thematic Query for Single Workspace");
			return false;
		}else{
			//document.getElementById('light2').style.display='block';
			//document.getElementById('fade').style.display='block';
			JQuery("#light2").fadeIn();
			JQuery("#fade").fadeIn();
		}
	}

	JQuery("#updatecustom").live("click",function(){
		var new_name   = JQuery('#new_name').val();
		var oldattrval = JQuery('#oldattrval').val();
		//alert(new_name);
		//alert(oldattrval);
		if(JQuery("#new_name").val()==""){
			alert("Please Select Custom Variable First!!");
			return false;
		}

		if(!validateFormula_v2()){
			return false;
		}

		var attributevale=JQuery('textarea#custom_attribute').text();
		//alert(attributevale);
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.updateCustomAttr&attrname="+new_name+"&attributevale="+encodeURIComponent(attributevale)+"&oldattrval="+encodeURIComponent(oldattrval),
			method  : 'GET',
			success : function(data){
				//JQuery("#closeextra").click();
				//reloadCustomAttr();
				window.location='index.php?option=com_mica&view=showresults&Itemid=108';
			}
		});
	});

	JQuery("#deletevariable").live("click",function(){
		var new_name      = JQuery('.customedit').find(":selected").text();
		var attributevale = JQuery('textarea#custom_attribute').text();
		if(new_name=="" && attributevale==""){
			alert("Please Select Custom Variable");
			return false;
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteCustomAttr&attrname="+new_name+"&attributevale="+attributevale,
			method  : 'GET',
			success : function(data){
				//alert(data);
				window.location='index.php?option=com_mica&view=showresults&Itemid=108';
				//window.location='index.php?option=com_mica&view=showresults&Itemid=108';
			}
		});
	});

	function closePopup_v2(){
		edittheme = 0;
		document.getElementById('light2').style.display = 'none';
		document.getElementById('fade').style.display   = 'none';
		JQuery("#displayinterval").html("");

		//JQuery("#thematic_attribute").val(0).attr("selected");
		document.getElementById("thematic_attribute").selectedIndex = 0;
		JQuery("#no_of_interval").val("");
		JQuery(".minmaxdisplay").html("");
		if(JQuery(".themeconent").html().length>10){
			JQuery("#themeconent").html(JQuery(".themeconent").html());
			JQuery('.simpleColorContainer').remove();
			getColorPallet_v2();
			JQuery(".simpleColorDisplay").css({"background-color":"#FFFFCC"});
		}
	}

	function validateFormula_v2(){
		var assignformula = "";
		var formula       = JQuery('textarea#custom_attribute').text();

		JQuery("#avail_attribute option").each(function(){
			//alert(JQuery(this).val());
			//alert(formula);
			var myclass=JQuery(this).parent().attr("class");
			if(JQuery(this).val()==formula.trim() && myclass!="defaultvariablelib"){
				assignformula ="You have already assign '"+formula+"' value to '"+JQuery(this).val()+"' Variable";
			}
		});

		if(assignformula!=""){
			alert(assignformula);
			return false;
		}
		return true;
	}

	function downloadMapPdf_v2(){
		//alert(tomcaturl);
		var start         = map.layers;
		var selectedlayer = "";

		if(javazoom==5 || javazoom==6)
		{
			selectedlayer ="india:rail_state";
		}
		else if(javazoom==7)
		{
			selectedlayer = "india:india_information";
		}
		else if(javazoom==8)
		{
			selectedlayer = "india:jos_mica_urban_agglomeration";
		}
		else
		{
			selectedlayer = "india:my_table";
		}

		var baselayer     = "india:rail_state";
		var buildrequest  = "";
		var buildrequest1 = "";
		for (x in start){
			for (y in start[x].params){
				if(start[x].params.LAYER==selectedlayer){
					if(y=="FORMAT"){
						buildrequest1 += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
					}else{
						buildrequest += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
					}
				}
			}
		}

		buildrequest=buildrequest+"FORMATEQTimage/png";
		buildrequest1=buildrequest1+"FORMATEQTimage/png";
		//alert(tomcaturl+"?"+buildrequest+"&BBOX="+map.getExtent().toBBOX()+"&WIDTH=800&HEIGHT=600");
		var finalurl = buildrequest+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
		var finalurl1 = buildrequest1+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
		window.open("index.php?option=com_mica&task=showresults.exportMap&mapparameter="+finalurl+"&baselayer="+finalurl1);
	}

	function exportImage_v2(name) {
	    // exporting
		var store       = '0'; // '1' to store the image on the server, '0' to output on browser
		//var name      = '001'; // name of the image
		var imgtype     = 'jpeg'; // choose among 'png', 'jpeg', 'jpg', 'gif'
		var opt         = 'index.php?option=com_mica&task=showresults.amExport&store='+store+'&name='+name+'&imgtype='+imgtype;
		var flashMovie  = document.getElementById('chartdiv');
		// previoushtml =JQuery("#exportchartbutton").html();//"Please Wait..."

		JQuery("#exportchartbutton").html("Please Wait...");//"Please Wait..."
		data=  flashMovie.exportImage_v2(opt);
		flashMovie.amReturnImageData_v2("chartdiv",data);
	}

	function amReturnImageData_v2(chartidm,data){
		var onclick      = "exportImage(JQuery('#chartype option:selected').text())";
		var button       = '<input type="button" name="downloadchart" onclick="'+onclick+'" class="button" value="Export">';
		var previoushtml = JQuery("#exportchartbutton").html(button);
	}

	JQuery(".variablegrp").live("click",function(){
		var myid = JQuery(this).attr("id");
		var dis  = JQuery('.'+myid).css("display");

		JQuery(".hideothers").css({"display":"none"});
		JQuery('.variablegrp').addClass("deactive");
		JQuery(this).addClass("active");
		JQuery(this).removeClass("deactive");
		if(dis=="block"){
			JQuery('.'+myid).css("display","none");
			JQuery(this).addClass("deactive");
			JQuery(this).removeClass("active");
		}else{
			JQuery('.'+myid).css("display","block");
			JQuery(this).addClass("active");
			JQuery(this).removeClass("deactive");
		}
	});

	function preloader_v2(){
		var preloaderstr="<div id='facebook' ><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><div id='block_4' class='facebook_block'></div><div id='block_5' class='facebook_block'></div><div id='block_6' class='facebook_block'></div></div>";
	}

	JQuery(".hovertext").live("mouseover",function(e){
		var allclass  =JQuery(this).attr("class");
		var segment   =allclass.replace("hovertext ","");
		var x         = e.pageX - this.offsetLeft;
		var y         = e.pageY - this.offsetTop;
		var popuphtml ="<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>"+segment+"</div>";
		JQuery(this).append(popuphtml);
	});

	JQuery(".hovertext").live("click",function(e){
		JQuery(this).parent().prev().find("input").prop("checked",true);//,true);
	});

	JQuery(".hovertext").live("mouseout",function(e){
		JQuery("#popupattr").remove();
	});

	JQuery(".customvariablecheckbox").live("click",function(){
		var check = (JQuery(this).attr("checked"));

		// if(check == "checked"){
		// var check = (JQuery(this).attr('checked','checked'));

		if(check){
			//alert("fg");
			JQuery("#new_name").val(JQuery(this).attr("id"));
			JQuery("#custom_attribute").text(JQuery(this).val());
			JQuery("#save").click();
			//addCustomVariable(JQuery(this).val(),JQuery(this).attr("id"));
		}else{
			JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteattribute&attr="+JQuery(this).attr("id"),
			method  : 'GET',
			success : function(data){
				//alert(data);
				//window.location='index.php?option=com_mica&view=showresults&Itemid=108';
				//window.location='index.php?option=com_mica&view=showresults&Itemid=108';
				getData();
			}
		});
			//window.location = "index.php?option=com_mica&task=showresults.deleteattribute&attr="+JQuery(this).attr("id");
		}
	});

	JQuery(document).ready(function(){
		JQuery("li #menu100").parent().attr("class","active");
	});

	function toggleCustomAction_v2(ele, action){

		if(action != ""){
			//JQuery(ele).prev().attr("checked",action);
			JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("checked",action);
		}else{
			//JQuery(ele).prev().removeAttr("checked");
			JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').removeAttr("checked");
		}
		//JQuery(ele).prev().click();
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').click();
	}

	JQuery(".customedit").live("click", function(){
		JQuery('#save').attr("id","updatecustom");
		JQuery('#updatecustom').attr("onclick","javascript:void(0)");
		JQuery('#updatecustom').attr("value","Update");

		JQuery('#new_name').val(JQuery(this).attr("id"));
		JQuery('#new_name').attr('disabled', true);
		JQuery('.aa').html("Edit");

		oldattrval=JQuery(this).attr("value");
		JQuery('#oldattrval').val(oldattrval);
		JQuery('textarea#custom_attribute').text(JQuery(this).attr("value"));
		lastchar = '';
		//moverightarea();
		//JQuery('#new_name').val(JQuery(this).prev().attr("id"));
		//JQuery('#custom_attribute').val(JQuery(this).prev().val());
		document.getElementById('light').style.display='block';
		document.getElementById('fade').style.display='block';
	});

	JQuery("#closeextra").live("click",function(){
		JQuery('#updatecustom').attr("id","save");
		JQuery('#save').attr("onclick","checkfilledvalue();");
		JQuery('#save').attr("value","Save");
		JQuery('#new_name').val("");
		JQuery('#new_name').attr('disabled', false);
		JQuery('.aa').html("Add New");
	});

	JQuery(".deletecustomvariable").live("click",function(){
		var names = new Array();
		var i     = 0;
		JQuery(".dcv:checked").each(function(i){
			names.push(JQuery(this).attr("id"));
		});
			JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteCustomVariableFromLib&attrname="+names,
			type    : 'GET',
			success : function(data){

				JQuery(".row summarydata").load();


			}
		});

		//window.location="index.php?option=com_mica&task=showresults.deleteCustomVariableFromLib&attrname="+names
	});

	JQuery("#fullscreen, #fullscreen1").live("click",function(){
		var fromthematic  =	JQuery("#fullscreen").attr("fromthematic");
		/*JQuery("#map").addClass("fullscreen");
		JQuery(this).css({"display":"none"});
		JQuery("#fullscreenoff").fadeIn();
		JQuery("#map").css({"width":JQuery(window).width()-20,"height":JQuery(window).height()-20});
		map.updateSize();*/

		if(fromthematic==1){
			popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
		}else{
			popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component', 'MapWin');
		}
		popped.document.body.innerHTML = "<div id='map' style='height:"+JQuery(window).height()+"px;width:"+JQuery(window).width()+"px;'></div>";
	});

	JQuery(".fullscreeniconoff").live("click",function(){
		JQuery("#fullscreentable").removeClass("fullscreentable");
		JQuery("#matrixclose").remove();
		JQuery(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");

		JQuery(this).removeClass("fullscreeniconoff");
		JQuery(this).addClass("fullscreenicon");
		JQuery("#fade").css({"display":"none"});
		JQuery("#tablescroll").removeAttr("width");
		JQuery("#tablescroll").removeAttr("overflow");
		JQuery("#tablescroll").removeAttr("height");
	});

	JQuery(".fullscreenicon").live("click",function(){

		JQuery("#fullscreentable").addClass("fullscreentable");
		JQuery(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'></td></tr></table>");
		JQuery(this).addClass("fullscreeniconoff");
		JQuery(this).removeClass("fullscreenicon");
		var toappend='<div class="divclose" id="matrixclose" style="text-align:right;"><a href="javascript:void(0);" onclick="JQuery(\'.fullscreeniconoff\').click();"><img src="'+siteurl+'/media/system/images/closebox.jpeg" alt="X"></a></div>';
		var html=JQuery("#fullscreentable").html();
		JQuery("#fullscreentable").html(toappend+"<div id='tablescroll'>"+html+"</div>");
		JQuery("#fadefade").css({"display":"block"});
		JQuery("#tablescroll").css({"width":"100%"});
		JQuery("#tablescroll").css({"height":"100%"});
		JQuery("#tablescroll").css({"overflow":"auto"});
	});

	JQuery(".filterspeed").live("click",function()
	{
		var selection     =JQuery(this).val();
		filterspeed_radio = selection;
		if(selection == "0"){


			$("#speed_variable").attr("multiple","true");
			$("#speed_variable").removeClass("inputbox");

			$("#speed_variable").removeClass("chzn-done");
			$("#speed_region").removeClass("chzn-done");

			$("#speed_region").removeAttr("multiple");

			$("#speed_variable_chzn").remove();
			$("#speed_region_chzn").remove();
			//choosenjq("#speed_region").chosen({max_selected_options: 1});
			//choosenjq("#speed_variable").chosen({max_selected_options: 5});
			JQuery("#speed_region").addClass("inputbox");
		}else{
			JQuery("#speed_variable").removeAttr("multiple");
			JQuery("#speed_variable").addClass("inputbox");
			JQuery("#speed_region").attr("multiple","true");

			JQuery("#speed_variable").removeClass("chzn-done");
			JQuery("#speed_region").removeClass("chzn-done");

			JQuery("#speed_variable_chzn").remove();
			JQuery("#speed_region_chzn").remove();

			JQuery("#speed_region").removeClass("inputbox");
			//choosenjq("#speed_region").chosen({max_selected_options: 5});
			//choosenjq("#speed_variable").chosen({max_selected_options: 1});
		}

		JQuery(".speed").css({"display":"block"});
		//choosenjq("#speed_variable").val('').trigger("liszt:updated");
		//choosenjq("#speed_region").val('').trigger("liszt:updated");
	});

	JQuery("#showspeed").live("click",function(){
		var speedvar    =new Array();
		var speedregion =new Array();
		if(typeof(JQuery("#speed_variable").attr("multiple")) == "string"){
			JQuery("#speed_variable").each(function(){
				speedvar.push(encodeURIComponent(JQuery(this).val()));
			});
			speedregion.push(encodeURIComponent(JQuery("#speed_region").val()));
		}else{
			speedvar.push(encodeURIComponent(JQuery("#speed_variable").val()));
			JQuery("#speed_region").each(function(){
				speedregion.push(encodeURIComponent(JQuery(this).val()));
			});
		}
		//var filterspeed = JQuery('input:radio[name=filter]:checked]').val();

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getsmeter&region="+speedregion+"&speedvar="+speedvar+"&filterspeed="+filterspeed_radio,
			type    : 'GET',
			success : function(data){
				JQuery("#spedometer_region").html("");
				JQuery("#spedometer_region").html(data);
				initspeed();
				JQuery("#speedfiltershow").css({"display":"block"});
				JQuery(".sfilter").css({"display":"none"});
				JQuery("#spedometer_region").css({"width":"915px"});
				JQuery("#spedometer_region").css({"overflow":"auto"});

				/*JQuery("#quartiles").hide();
				JQuery("#result_table").hide();
				JQuery("#graph").hide();
				JQuery("#potentiometer").hide();*/

			}
		});
	});

	JQuery("#speedfiltershow").live("click",function(){
		JQuery("#speedfiltershow").css({"display":"none"});
		JQuery(".sfilter").css({"display":"block"});
		JQuery("#spedometer_region").css({"width":"681px"});
		JQuery("#spedometer_region").css({"overflow":"auto"});
	});

	JQuery("#downmatrix").live("click",function(){
		window.location.href="index.php?option=com_mica&task=showresults.downloadMatrix&rand="+Math.floor(Math.random()*100000);
	});

	JQuery("#matrix").live("click",function(){

		if($('.state_checkbox:checked').length == 0){
				alert(statealert);
				return false;
			}

			if($('.district_checkbox:checked').length == 0){
				alert(districtalert);
				return false;
			}

			if($('.type_checkbox:checked').length == 0){
				alert(typealert);
				return false;
			}

			if($('.variable_checkbox:checked').length == 0){
				alert(variablealert);
				return false;
			}
		//if(JQuery("#fullscreentable").html().length < 100){
			var data = $('#micaform').serialize();
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.getMaxForMatrix",
				method  : 'POST',
				async   : true,
				data    : data,
				beforeSend: function() {
					jQuery('#main_loader').show();
		        },
				success : function(data){
					jQuery('#main_loader').hide();
					JQuery("#quartiles").show();
					JQuery("#result_table").hide();
					JQuery("#graph").hide();
					JQuery("#potentiometer").hide();
					JQuery("#gis").hide();
					JQuery("#fullscreentable").html(data);
				}
			});
		//}
	});

	JQuery("#speedometer").live("click",function(){
		JQuery.ajax({
			url    :"index.php?option=com_mica&task=showresults.speedometer",
			method : 'POST',
			success:function(data){
				JQuery("#speed_region").html(data);
			}
		});
	});

// JQuery(document).ready(function(){
// 	var graphdist = JQuery("#district").val();

// 	$.ajax({
// 		url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
// 		type    : 'POST',
// 		data    : "dist="+graphdist,
// 		success : function(data){
// 			JQuery("#selectdistrictgraph").html(data);
// 		}
// 	});
// });

	JQuery("#showchart").live("click",function(){
		var checkedelements = new Array();
		var checkeddist     = new Array();
		var checkevar       = new Array();
		JQuery("#selectdistrictgraph").find(".districtchecked").each(function(){
			if(JQuery(this).attr("checked")){
				checkedelements.push(this.val);
				checkeddist.push(JQuery(this).val());
			}
		});

		JQuery("#light1007").find(".variablechecked").each(function(){
			if(JQuery(this).attr("checked")){
				checkedelements.push(JQuery(this).val());
				checkevar.push(JQuery(this).val());
			}
		});

		if(checkedelements.length >15){
			alert("Variable + District Total Should be less then 15");
			return false;
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getGraph",
			type    : 'POST',
			data    : "dist="+checkeddist+"&attr="+checkevar,
			success : function(data){
				var segments = data.split("<->");
				datastr    = segments[0];
				grpsetting = segments[1];
				JQuery("#chartype").change();
			}
		});
	});

// JQuery(document).ready(function(){
//     JQuery("#summary").click(function(){
//         JQuery("#summary_data").toggle();
//         JQuery("#summary").toggleClass('active');
//     });
// });

	/*$('.actionbtn').click(function(event) {
		/* Act on the event */


		/*switch ($(this).attr('id')) {
			case "tabledata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.text').show();
				break;
			case "graphdata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.graphs').show();
				break;
			case "gisdata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.gis').show();
				break;
			case "matrix":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.matrix').show();
				break;
			case "pmdata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.speedometer').show();
				break;
			case "comparedata":
				$('#result_table').find('.contenttoggle2').hide();
				$('#result_table').find('.contenttoggle2.compare').show();
				break;
			case "summary":

				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.summary').show();
			break;

			default:
				// statements_def
				break;
		};

	});*/



	function getRemove_v2(cls,a)
	{
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteattribute&attr="+a +"&chkcls="+ cls,
			method  : 'GET',
			async   : true,
			success : function(data)
			{
				if(a=="MPI")
				{
					JQuery(".mpi_checkbox").prop("checked",false);
				}
				else
				{
					value = a.replace(/ /g,"_");
					JQuery(".variable_checkbox[value='"+a+"']").prop("checked",false);
					JQuery(".swcs_checkbox[value=Rural_Score_"+value+"]").prop("checked",false);
					JQuery(".swcs_checkbox[value=Urban_Score_"+value+"]").prop("checked",false);
					JQuery(".swcs_checkbox[value=Total_Score_"+value+"]").prop("checked",false);
				}
				//getData();
				getDataNew();
			}
		});
	}

	function displaycombo_v2(value){
		if(JQuery("#state").val()=="all"){
			alert("Please Select State First");
			document.getElementById('dataof').selectedIndex=0;
			document.getElementById("district").selectedIndex=0;
			return false;
		}else{
					var allVals = [];
					var slvals = [];
					$('.state_checkbox:checked').each(function() {
						slvals.push($(this).val())
					})
					selected = slvals.join(',') ;
			getfrontdistrict_v2(selected);
		}

		if(value=="District"){
			JQuery(".districtspan").css({"display":"block"});
			JQuery(".townspan").css({"display":"none"});
			JQuery(".urbanspan").css({"display":"none"});
		}/*else if(value=="Town"){
			JQuery(".districtspan").css({"display":"none"});
			JQuery(".townspan").css({"display":"block"});
			JQuery(".urbanspan").css({"display":"none"});
			JQuery(".distattrtypeselection").css({"display":"none"});
			document.getElementById("district").selectedIndex=0;
			document.getElementById('m_type').selectedIndex=0;
			JQuery(".distattrtypeselection").css({"display":"none"});
		}else if(value=="UA"){
			JQuery(".districtspan").css({"display":"none"});
			JQuery(".townspan").css({"display":"none"});
			document.getElementById('m_type').selectedIndex=0;
			JQuery(".distattrtypeselection").css({"display":"none"});
			JQuery(".urbanspan").css({"display":"block"});
			document.getElementById("district").selectedIndex=0;
		}else{
			JQuery(".districtspan").css({"display":"none"});
			JQuery(".townspan").css({"display":"none"});
			JQuery(".urbanspan").css({"display":"none"});
			JQuery(".distattrtypeselection").css({"display":"none"});
			document.getElementById('m_type').selectedIndex=0;
		}*/
	}

	function getvalidation(num){
		//alert('hdfdfd');

		var myurl = "";
			myurl = "state=";
			myurl += JQuery("#state").val();
			myurl += "&district=";
			myurl += JQuery("#district").val();
			myurl += "&attributes=";
		var attr = "";
		JQuery('#result_table').find('.statetotal_attributes'+num).each(function (key, value) {
			JQuery('.variable_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked"));
		});
		getData();
		return true;
	}

/*		var lastchar = '';
		function moverightarea(){
			var val      = document.getElementById('custom_attribute').innerHTML;
			var selected = 0;
			console.log(JQuery("#avail_attribute option").length);

			//for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
			for(i = JQuery("#avail_attribute option").length-1; i >= 0; i--){
				//var avail_attribute = document.micaform1.avail_attribute;
				var avail_attribute = JQuery("#avail_attribute option:selected").val();
				console.log();
				if(document.micaform1.avail_attribute[i].selected){
					selected = 1;
					if(lastchar=='attr'){
						alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
					}else{
						document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
						lastchar = 'attr';
					}
				}
			}
			if(selected==0){
				alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
			}
		}

		function addplussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"+";
				lastchar = 'opr';
			}
		}

		function addminussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"-";
				lastchar = 'opr';
			}
		}

		function addmultiplysign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"*";
				lastchar = 'opr';
			}
		}

		function adddivisionsign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"/";
				lastchar = 'opr';
			}
		}

		function addLeftBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+"(";
			checkIncompleteFormula();
		}

		function addRightBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+")";
			checkIncompleteFormula();
		}

		function addPersentage(){
			var val = document.getElementById('custom_attribute').innerHTML;
			//if(lastchar=='opr'){
				//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			//}else{
				document.getElementById('custom_attribute').innerHTML = val+"%";
				lastchar = 'opr';
			//}
		}

		function addNumeric(){
			var val       = document.getElementById('custom_attribute').value;
			//alert(val);
			var customval = document.getElementById('custom_numeric').value;
			document.getElementById('custom_attribute').innerHTML = val+customval;
			lastchar = '';
		}

		function checkIncompleteFormula(){
			var str   = document.getElementById('custom_attribute').innerHTML;
			var left  = str.split("(");
			var right = str.split(")");

			if(left.length==right.length){
				document.getElementById('custom_attribute').style.color="black";
			}else{
				document.getElementById('custom_attribute').style.color="red";
			}
		}

		function Click(val1){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+val1;
			checkIncompleteFormula();
		}

		function checkfilledvalue(){
			var tmp1 = document.getElementById('custom_attribute').innerHTML;
			var tmp2 = document.getElementById('new_name').value;
			var flg  = 0;
			var len1 = parseInt(tmp1.length) - 1;
			var len2 = parseInt(tmp2.length) - 1;
			if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
				flg = 1;
			}

			if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){
				alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
			}else if(lastchar=='opr' && flg == 0){
				alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
			}else{
				if(!validateFormula_v2()){
					return false;
				}
				addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
			}
		}

		function addCustomAttr(attrname,attributevale){

					JQuery.ajax({
						url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
						method  : 'GET',
						success : function(data){
							//window.location='index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
							//$(".full-data-view").load(location.href+" .full-data-view>*","");
							$("#light").hide();
							$(".black_overlay").css({"display":"none"});
							getData();
						}
					});
				}

		function undofield(){
			document.getElementById('custom_attribute').innerHTML = '';
			lastchar = '';
		}


	// function alldata() {
	//     var x = document.getElementById("alldata");
	//     if (x.style.display === "none") {
	//         x.style.display = "block";
	//     } else {
	//         x.style.display = "none";
	//     }
	// }

	// function compareData() {
	//     var x = document.getElementById("compare");
	//     if (x.style.display === "none") {
	//         x.style.display = "block";
	//     } else {
	//         x.style.display = "none";
	//     }
	// }
// jQuery(document).ready(function($) {
// 	//alert('compare');
// 	$('.contenttoggle2') .hide()
// 	$('a[href^="#"]').on('click', function(event) {
// 		//alert('compare');
// 	$('.contenttoggle2') .hide()
// 	    var target = $(this).attr('href');

// 	    $('.contenttoggle2'+target).toggle();

// 	});
// });


// $(document).ready(function () {
//     $("#alldata").click(function () {
//         $("#alldata").toggle();
//     });
//      $("#compare").click(function () {
//         $("#compare").toggle();
//     });
// });


// jQuery(document).ready(function($) {
// $('a[href^="#"]').on('click', function(event) {

//     var target = $( $(this).attr('href') );
//     target.fadeToggle(100);
//     if( target.length ) {
//         event.preventDefault();
//         $('html, body').animate({
//             scrollTop: target.offset().top
//         }, 2000);
//     }

// });
// });



		JQuery("#editvariable").live("click",function(){
			JQuery('#save').attr("id","updatecustom");
			JQuery('#updatecustom').attr("onclick","javascript:void(0)");
			is_updatecustom=1;
			JQuery('#updatecustom').attr("value","Update");
		});

		JQuery(".customedit").live('click',function(){
			if(is_updatecustom == 1){
				//alert(JQuery(this).find(":selected").text());
				JQuery('#new_name').val(JQuery(this).find(":selected").text());
				JQuery('#new_name').attr('disabled', true);
				oldattrval=JQuery(this).find(":selected").val();
				JQuery('#oldattrval').val(oldattrval);
				JQuery('textarea#custom_attribute').text("");
				lastchar = '';
				moverightarea();
			}
		});
*/



	JQuery(document).keyup(function(e){$
		if(e.keyCode == "27"){
			document.getElementById('light').style.display = 'none';
			document.getElementById('light_thematic').style.display = 'none';
			document.getElementById('fade').style.display  = 'none';
		}
	});

	var is_updatecustom=0;


	JQuery(".deleterow").live("click",function(){

		var value = JQuery(this).val();
		var ans   = confirm("Are you Sure to delete Variable "+value);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.deleteattribute&attr="+value,
				type    : 'GET',
				success : function(data){
					JQuery("#"+value).fadeOut();
					window.location='index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});

	JQuery(".deletecustom").live("click",function(){
		var value = JQuery(this).val();
		var ans   = confirm("Are you Sure to delete Custom Variable "+value);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.deleteCustomAttribute&attr="+value,
				type    : 'GET',
				success : function(data){
					JQuery("#"+value).fadeOut();
					window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});

	JQuery(".removecolumn").live("click",function(){
		var value       = JQuery(this).val();
		//var classname = JQuery(this).parent().attr('class');
		var segment     = value.split("`");
		var ans         = confirm("Are you Sure to delete  "+segment[2]);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.deleteVariable&table="+segment[0]+"&value="+segment[1],
				type    : 'GET',
				success : function(data){
					//JQuery(this).fadeOut();
					window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});
	var compare =1;

	//JQuery(document).ready(function()

	JQuery("#district_tab").live("click",function(){
		JQuery(document).ready(function() {
  	  		JQuery('.slider').slick({
		  		dots: false,
		    	vertical: true,
		    	slidesToShow: 11,
		    	slidesToScroll: 1,
		    	verticalSwiping: true,
		    	arrrow:true,
		  	});
		});
	});

	JQuery(document).ready(function(){
		JQuery(".contenttoggle").css({"display":"none"});
		//var init=$(".active").attr("id");
		//$("."+init).css({"display":"block"});

		JQuery(".toggle").live("click",function(){
			alert("toggle");
			var myclass=JQuery(this).attr("class");
			JQuery(".toggle").css({"border-right":"1px solid #0154A1"});
			JQuery(this).prev("span").css({"border-right":"1px solid white"});
			//JQuery(this).next("span").css({"border-right":"1px solid white"});
			var myclasssplit=myclass.split(" ");
			if(typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active"){
				JQuery(".toggle").removeClass("active");
				JQuery(".contenttoggle").slideUp();
				return false;
			}
			//JQuery(".toggle").css({"border-right":"1px solid white"});
			var myid=JQuery(this).attr("id");
			JQuery(".toggle").removeClass("active");
			JQuery(this).addClass("active");
			JQuery(".contenttoggle").slideUp();
			JQuery("."+myid).slideDown();
		});

		JQuery(".toggle1").live("click",function(){
			JQuery(".toggle1").css({"border-right":"1px solid #0154A1"});
			JQuery(this).prev("span").css({"border-right":"none"});
			//JQuery(this).next("span").css({"border-right":"none"});
			JQuery(".toggle1").last("span").css({"border-right":"white"})
			var myclass      = JQuery(this).attr("class");
			var myclasssplit = myclass.split(" ");

			if(typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active"){
				JQuery(".toggle1").removeClass("active");
				JQuery(".contenttoggle1").slideUp();
				JQuery(".toggle1").css({"border-right":"1px solid #0154A1"});
				JQuery(".toggle1").last("span").css({"border-right":"white"})
				return false;
			}
			var myid=JQuery(this).attr("id");
			JQuery(".toggle1").removeClass("active");
			JQuery(this).addClass("active");
			JQuery(this).css({"border-right":"1px solid #0154A1"});
			JQuery(".contenttoggle1").slideUp();
			JQuery("."+myid).slideDown();
			JQuery.cookie("tab", myid);
			if(myid == "text"){
				JQuery('#jsscrollss').not(".firsttableheader,.secondtableheader,.thirdtableheader").jScrollPane({verticalArrowPositions: 'os',horizontalArrowPositions: 'os'});
			}
		});

		JQuery(".toggle2").live("click",function(){
			var myclass=JQuery(this).attr("class");
			var myclasssplit = myclass.split(" ");
			if(typeof(myclasssplit[1])!="undefined" && myclasssplit[1]=="active")
			{
				JQuery(".toggle2").removeClass("active");
				JQuery(".contenttoggle2").css({"display":"none"});
				return false;
			}
			var myid=JQuery(this).attr("id");

			JQuery(".toggle2").removeClass("active");
			JQuery(this).addClass("active");
			JQuery(".contenttoggle2").css({"display":"none"});
			JQuery("."+myid).css({"display":"block"});
		});
	});

	function getindlvlgrpvar(){
		//var groupid = jQuery('select#indlvlgrps').val();

		var groupid = JQuery('input[name=indlvlgrps]:checked').val();

		jQuery.ajax({
			url: "index.php?option=com_mica&task=micafront.getIndLvlGrp",
			type: 'POST',
			data: "groupid="+groupid,
			success: function(data, textStatus, xhr) {
				var data = jQuery.parseJSON(data);
				jQuery('.variable_checkbox').each(function(index, el) {
					if (JQuery('.variable_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked"))){
						/*jQuery('#ui-multiselect-attributes-option-'+index).attr('selected', false);
						jQuery('#ui-multiselect-attributes-option-'+index).click();*/

						JQuery('.variable_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked", false));
					}
				});

				jQuery('.type_checkbox').each(function(index, el) {
					if (JQuery('.type_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked"))){
						/*jQuery('#ui-multiselect-m_type-option-'+index).attr('selected', false);
						jQuery('#ui-multiselect-m_type-option-'+index).click();*/

						JQuery('.type_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked", false));
					}
				});
				if (data['variables'] != 'false') {
					jQuery('.variable_checkbox').each(function(index, el) {

						if(jQuery.inArray(jQuery(this).val(), data['variables']) !== -1){
			                /*jQuery(this).attr('selected');
			                jQuery('#variable_check_'+index).attr('aria-selected', true);
			                jQuery('#variable_check_'+index).attr('selected', true);
							jQuery('#variable_check_'+index).click();*/

							jQuery('.variable_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked"));
						}
					});
					jQuery('.variable_checkbox').val(data['variables']);
				}
				if (data['type'] != 'false') {
					jQuery('.type_checkbox').each(function(index, el) {
						if(jQuery.inArray(jQuery(this).val(), data['type']) !== -1){
			                /*jQuery(this).attr('selected');
			                jQuery('#ui-multiselect-m_type-option-'+index).attr('aria-selected', true);
			                jQuery('#ui-multiselect-m_type-option-'+index).attr('selected', true);
							jQuery('#ui-multiselect-m_type-option-'+index).click();*/

							jQuery('.type_checkbox[value='+JQuery(this).val()+']').prop("checked",JQuery(this).prop("checked"));
						}
					});
					jQuery('.type_checkbox').val(data['type']);
				}
			}
		});
	}

	// edited by salim STARTED 25-10-2018
	function getDataNew()
	{
		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		if($('.district_checkbox:checked').length == 0){
			alert(districtalert);
			return false;
		}

		if($('.type_checkbox:checked').length == 0){
			alert(typealert);
			return false;
		}

		if($('.variable_checkbox:checked').length == 0){
			alert(variablealert);
			return false;
		}

		var data = $('#micaform').serialize();
        JQuery.ajax({
			//url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
			url     : "index.php?option=com_mica&task=showresults.getDataajax",
			method  : 'POST',
			async   : true,
			data    : data,
			beforeSend: function() {
              jQuery('#main_loader').show();
           	},
			success : function(data){
				JQuery("#result_table").show();
				JQuery("#graph").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();
				JQuery("#gis").hide();

				JQuery(".alldata").html(data);

                //loadCharts();
				var graphdist="";
				JQuery('.district_checkbox:checked').each(function(i, selected) {

						graphdist +=JQuery(this).val()+",";
				});
				graphdist= graphdist.slice(0, -1);
				JQuery.ajax({
					url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
					type    : 'POST',
					data    : "dist="+graphdist,
					success : function(data){
						jQuery('#main_loader').hide();
						JQuery("#result_table").find("#selectdistrictgraph").html(data);
						//set custom_variables = custom Attributes under All Variables List
						//JQuery('.variablelist').append(JQuery("#result_table").find(".custom_ver").html());



					}
				});

				//console.log(JQuery("#result_table"));
				//choosenjq("#attributes").multiselect({selectedList: 1,noneSelectedText: 'Select Variable '});
			}
		});
	}

	function downloadAction(){
		window.location='index.php?option=com_mica&task=showresults.exportexcel';
	}

	// For Potentio Meter ... edited by salim STARTED 26-10-2018
	JQuery("#pmdata").live("click",function()
	{

		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		if($('.district_checkbox:checked').length == 0){
			alert(districtalert);
			return false;
		}

		if($('.type_checkbox:checked').length == 0){
			alert(typealert);
			return false;
		}

		if($('.variable_checkbox:checked').length == 0){
			alert(variablealert);
			return false;
		}

		JQuery("#potentiometer").show();
		JQuery("#potentiometer").load(" #potentiometer > *");

		JQuery("#filter1").attr('checked', 'checked');
		JQuery("#filterspeed").trigger("click");

		//JQuery("#speed_variable").val(JQuery("#speed_variable option:first").val());
		//JQuery("#showspeed").attr('checked', 'checked').trigger("click");



		JQuery("#result_table").hide();
		JQuery("#graph").hide();
		JQuery("#quartiles").hide();
		JQuery("#gis").hide();
	});

	// For Gis Map ... edited by salim STARTED 26-10-2018
	JQuery("#gisdata").live("click",function(){

		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		if($('.district_checkbox:checked').length == 0){
			alert(districtalert);
			return false;
		}

		if($('.type_checkbox:checked').length == 0){
			alert(typealert);
			return false;
		}

		if($('.variable_checkbox:checked').length == 0){
			alert(variablealert);
			return false;
		}


		//JQuery("#map").load(" #map > *");
		jQuery('#map').html("");
		var data = $('#micaform').serialize();
        JQuery.ajax({
			//url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
			url     : "index.php?option=com_mica&task=showresults.getGISDataajax",
			method  : 'POST',
			async   : true,
			data    : data,
			beforeSend: function() {
              jQuery('#main_loader').show();
              //JQuery("#gis").load(" #gis > *");
              //jQuery('#gis').html("");
           	},
			success : function(data){
				//JQuery("#gis").load(" #gis > *");

				jQuery('#main_loader').hide();
				JQuery("#gis").show();
				JQuery("#result_table").hide();
				JQuery("#graph").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();
				//JQuery(".tq").show();
				JQuery("#thematic_attribute").load(" #thematic_attribute > *");



				//JQuery("#gis").append( data );
				latestData = JQuery.parseJSON(data);
				//latestData.gis_tomcaturl;

				tomcaturl              = latestData.gis_tomcaturl;
				mainzoom               = latestData.gis_zoom;
				javazoom               = latestData.gis_zoom;
				userid                 = latestData.gis_userid;
				activeworkspace        = latestData.gis_activeworkspace;
				mainGeometry           = latestData.gis_geometry;
				mainlonglat            = latestData.gis_longlat;
				mainState              = latestData.gis_state;
				mainDistrict           = latestData.gis_district;
				mainUnselectedDistrict = latestData.gis_UnselectedDistrict;
				mainTown               = latestData.gis_town;
				mainUrban              = latestData.gis_urban;

				OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
					defaultHandlerOptions: {
						'single'         : true,
						'double'         : false,
						'pixelTolerance' : 0,
						'stopSingle'     : true,
						'stopDouble'     : false
					},
					initialize: function(options) {
						this.handlerOptions = OpenLayers.Util.extend(
							{}, this.defaultHandlerOptions
						);
						OpenLayers.Control.prototype.initialize.apply(
							this, arguments
						);
						this.handler = new OpenLayers.Handler.Click(
							this, {
								'click':this.getfeaturenfo,'dblclick': this.onDblclick
							}, this.handlerOptions
						);
					},
					onDblclick: function(e) {
						JQuery(".olPopup").css({"display":"none"});
						JQuery("#featurePopup_close").css({"display":"none"});
						//changeAttrOnZoom(map.zoom);
					},
					getfeaturenfo :function(e) {
						coordinates = e;
						var params = {
							REQUEST       : "GetFeatureInfo",
							projection    : "EPSG:4326",
							EXCEPTIONS    : "application/vnd.ogc.se_xml",
							BBOX          : map.getExtent().toBBOX(10),
							SERVICE       : "WMS",
							INFO_FORMAT   : 'text/html',
							QUERY_LAYERS  : selectlayer(map.zoom),
							FEATURE_COUNT : 6,
							Layers        : selectlayer(map.zoom),
							WIDTH         : map.size.w,
							HEIGHT        : map.size.h,
							X             : parseInt(e.xy.x),
							Y             : parseInt(e.xy.y),
							CQL_FILTER    : selectfilter(),
							srs           : map.layers[0].params.SRS
						};

						// handle the wms 1.3 vs wms 1.1 madness
						if(map.layers[0].params.VERSION == "1.3.0") {
							params.version = "1.3.0";
							params.i       = e.xy.x;
							params.j       = e.xy.y;
						} else {
							params.version = "1.1.1";
							params.y       = parseInt(e.xy.y);
							params.x       = parseInt(e.xy.x);
						}
						OpenLayers.loadURL(tomcaturl, params, this, setHTML, setHTML);
						var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
						//lonLat.transform(map.displayProjection,map.getProjectionObject());
						//map.setCenter(lonLat, map.zoom);
						map.panTo(lonLat);
						//map.addControl(ovControl);
						//OpenLayers.Event.stop(e);
					}
				});

				var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
				var options = {
					controls      : [],
					maxExtent     : bounds,
					projection    : "EPSG:4326",
					maxResolution : 'auto',
					zoom          : mainzoom,
					units         : 'degrees'
				};

				map = new OpenLayers.Map('map', options);

				land = new OpenLayers.Layer.WMS("State Boundaries",
					tomcaturl,
					{
						Layer       : 'india:rail_state',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : stateCqlFilter(),
						SLD         : sldlinkStateBoundaries
					},
					{
						isBaseLayer: false
					}
				);

				opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
					tomcaturl,
					{
						Layers           : 'india:rail_state',
						transparent      : false,
						format           : 'image/png',
						styles           : "dummy_state",
						transitionEffect : 'resize'
					},
					{
						isBaseLayer: true
					}
				);

				districts = new OpenLayers.Layer.WMS("districts",
					tomcaturl,
					{
						Layer       : 'india:india_information',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : districtCqlFilter(),
						SLD         : sldlinkDistricts
					},
					{
						isBaseLayer: false
					},
					{
						transitionEffect: 'resize'
					}
				);

				cities = new OpenLayers.Layer.WMS("Cities",
					tomcaturl,
					{
						Layer       : 'india:my_table',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : cityCqlFilter(),
						SLD         : sldlinkCities
					},
					{
						isBaseLayer : false
					},
					{
						transitionEffect : 'resize'
					}
				);

				urban = new OpenLayers.Layer.WMS("Urban",
					tomcaturl,
					{
						Layer       : 'india:jos_mica_urban_agglomeration',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : urbanCqlFilter(),
						SLD         : sldlinkUrban
					},
					{
						isBaseLayer: false
					},
					{
						transitionEffect: 'resize'
					}
				);

				if(mainzoom==5 || mainzoom==6 ){
					map.addLayers([land]);
				}else if(mainzoom==7){
					map.addLayers([districts]);
				}else if(mainzoom==8){
					map.addLayers([urban]);
				}else{
					map.addLayers([cities]);
				}


				map.div.oncontextmenu = function noContextMenu(e) {
					if (OpenLayers.Event.isRightClick(e)){
						displaymenu(e);
					}
					// return false; //cancel the right click of brower
				};

				//map.addControl(exportMapControl);
					map.addControl(new OpenLayers.Control.PanZoomBar({
					position : new OpenLayers.Pixel(5, 15)
				}));
				map.addControl(new OpenLayers.Control.Navigation({
					dragPanOptions: {enableKinetic: true}
				}));

			   	//map.addControl(new OpenLayers.Control.Scale($('scale')));
				map.addControl(new OpenLayers.Control.Attribution());
				//map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
				//map.addControl(new OpenLayers.Control.LayerSwitcher());
				//alert(bounds);

				var click = new OpenLayers.Control.Click();
				map.addControl(click);
				click.activate();
				map.zoomTo(mainzoom);

				if(mainGeometry != 0 && mainGeometry != "")
				{
					var format    = new OpenLayers.Format.WKT();
					var feature   = format.read(mainGeometry);
					var homepoint = feature.geometry.getCentroid();
				}

				map.addLayers([opverviewland]);
				//if (!map.getCenter(bounds))
					//map.zoomToMaxExtent();
				map.zoomToExtent(bounds);
				//map.zoomToMaxExtent();

				//map.zoomTo(mainzoom);

				if(mainlonglat == "")
				{
					var curlonglat= '77.941406518221,21.676757633686';
				}
				else
				{
					var curlonglat = mainlonglat;
				}

				//alert(curlonglat);
				var longlatsegment=curlonglat.split(",");


		 		if(mainGeometry !="0")
		 		{
		 			map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
					map.zoomTo(6);
		 		}
		 		else
		 		{
		 			map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
					map.zoomTo(4);
		 		}
			}
		});
	});
</script>
<script type="text/javascript" id="j1">
	var globalcorrdinates  = "";
	// var map             = new OpenLayers.Map("map");
	var oldzoom            = 0;
	var geo                = [];
	var dystate            = [];
	var geo                = [];
	//OpenLayers.ProxyHost = "http://192.168.5.159/cgi-bin/proxy.cgi?url=";
	// use a CQL parser for easy filter creation
	var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;
	OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
	var tomcaturl="<?php echo $this->tomcaturl; ?>";
	var mainzoom = "<?php echo $this->zoom; ?>";
	var tomcatpathlink = "<?php echo 'http://mica-mimi.in/'.TOMCAT_SLD_FOLDER.'/'; ?>";
	var userid = "<?php echo $this->userid; ?>";
	var activeworkspace = "<?php echo $this->activeworkspace; ?>";
	var sldlinkStateBoundaries = tomcatpathlink+userid+"/"+activeworkspace+"/rail_state.sld";
	var sldlinkDistricts = tomcatpathlink+userid+"/"+activeworkspace+"/india_information.sld";
	var sldlinkCities = tomcatpathlink+userid+"/"+activeworkspace+"/my_table.sld";
	var sldlinkUrban = tomcatpathlink+userid+"/"+activeworkspace+"/jos_mica_urban_agglomeration.sld";
	var mainGeometry = "<?php echo $this->geometry; ?>";
	var mainlonglat = "<?php echo $app->input->get('longlat', '', 'raw'); ?>";
	var mainState = "<?php echo $this->state; ?>";
	var mainDistrict = "<?php echo $this->district; ?>";
	var mainUnselectedDistrict = "<?php echo $this->UnselectedDistrict; ?>";
	var javazoom = mainzoom;
	var mainTown = "<?php echo $this->town; ?>";
	var mainUrban = "<?php echo $this->urban; ?>";

	function onPopupClose(evt) {
		// 'this' is the popup.
		JQuery("#featurePopup").remove();
	}

	function displaymenu(e){
		//$("#menupopup").remove();
		//var OuterDiv=$('<div  />');
		//OuterDiv.append($("#menu").html());
		//OuterDiv.append($("#menu").html());
		//console.log(e);
		//OuterDiv.css({"z-index":"100000","left":e.screenX,"top":e.screenY,"display":"block"});
		//console.log({e.layerX,e.layerY});

		// popup = new OpenLayers.Popup("menupopup",map.getLonLatFromPixel(new OpenLayers.Pixel({e.layerX,e.layerY})),new OpenLayers.Size(200,300),$("#menu").html(), false);
		//	map.popup = popup;
		//evt.feature = feature;
		//	map.addPopup(popup);
	}

	function onFeatureSelect(response)
	{


		response = response.trim("");

		//console.log("response=> "+response);

		var getsegment=response.split("OGR_FID:");
		console.log("getsegment=> "+getsegment);
		var id;

		if(typeof(getsegment[1])=="undefined")
		{
			return false;
		}
		else
		{
			id=getsegment[1].trim();
			//alert(getsegment[3]);
			if(typeof(getsegment[2])!="undefined" ){
			  if(getsegment[2]==362 || getsegment[2]==344 || getsegment[2]==205 || getsegment[2]==520 || getsegment[2]==210 || getsegment[2]==211 || getsegment[2]==206)
					id = getsegment[2].trim();
			   if(getsegment[3]==209)
							  id=getsegment[3].trim();
				if(getsegment[4]==209)
							  id=getsegment[4].trim();
				if(getsegment[3]==207)
							  id=getsegment[3].trim();
				if(getsegment[1]==205)
							  id=getsegment[1].trim();
				if(getsegment[1]==206 && getsegment[2]==207 && getsegment[3]==212)
							  id=getsegment[2].trim();
			}
		}

		id = parseInt(id);
		JQuery(".olPopup").css({"display":"none"});

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.popupAttributes&id="+id+"&zoom="+javazoom,
			method  : 'GET',
			success : function(data){
				popup = new OpenLayers.Popup.FramedCloud("featurePopup",
					map.getLonLatFromPixel(new OpenLayers.Pixel(coordinates.xy.x,coordinates.xy.y)),
					new OpenLayers.Size(300,150),
					data,null ,true, onPopupClose
				);
				map.popup = popup;
				//evt.feature = feature;
				map.addPopup(popup);
			}
		});
	}

	function onFeatureUnselect(evt) {
		feature = evt.feature;
		if (feature.popup) {
			popup.feature = null;
			map.removePopup(feature.popup);
			feature.popup.destroy();
			feature.popup = null;
		}
	}

	function stateCqlFilter()
	{
		if(mainState != "all")
		{
			statetojavascript = mainState.replace("," , "','");
			return "name in ('"+statetojavascript+"')";
		}
	}

	function districtCqlFilter()
	{

		if(mainDistrict != "all" && mainDistrict != "" )
		{
			statetojavascript = mainDistrict;
			//statetojavascript =statetojavascript.replace(",", "','");
			return "OGR_FID in ("+statetojavascript+")";
			//return "OGR_FID in ('345','83','347','623')";

			//return "OGR_FID in ('"+statetojavascript+"')";

			/*if(mainUnselectedDistrict.length > mainDistrict)
			{

				statetojavascript = mainDistrict;
				statetojavascript ="'"+ statetojavascript.replace(",", "','")+"'";
				return "OGR_FID in ('"+statetojavascript+"')";
			}
			else
			{
				alert('123');
				statetojavascript = mainUnselectedDistrict;
				statetojavascript = statetojavascript.replace(",","','");
				return "OGR_FID not in ('"+statetojavascript+"')";
			}*/
		}
		else if(mainDistrict == "all")
		{
			return  "state in ('"+statetojavascript+"')";
		}
	}

	function cityCqlFilter(){
		if(mainTown != "all" &&  mainTown != "")
		{
			statetojavascript = mainTown;
			statetojavascript =statetojavascript.replace(",", "','");
			return "OGR_FID in ('"+statetojavascript+"')";
		}

		if(mainTown == "all")
		{
			return  "state in ('"+statetojavascript+"')";
		}
		<?php  /*if($this->town!="all" && $this->town!="" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			$statetojavascript = $this->town;
			$statetojavascript = str_replace(",","','",$statetojavascript);
			echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
		}

		if($this->town == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			echo " return  \"state in ('".$statetojavascript."')\";";
		}*/ ?>
	}

	function urbanCqlFilter()
	{
		if(mainUrban != "all" && mainUrban != "")
		{
			statetojavascript = mainUrban;
			statetojavascript =statetojavascript.replace(",", "','");
			return "OGR_FID in ('"+statetojavascript+"')";
		}

		if(mainUrban == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			return  "state in ('"+statetojavascript+"')";
		}

		<?php  /*if($this->urban != "all" && $this->urban != ""){
			//$stateArray=explode(",",JRequest::GetVar('state'));
			$statetojavascript = $this->urban;
			$statetojavascript = str_replace(",","','",$statetojavascript);
			echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
		}

		if($this->urban == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('town'));
			echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
		}*/ ?>
	}

	function changeAttrOnZoom(javazoom,response){
		onFeatureSelect(response.responseText);
	}

	function reloadCustomAttr(){
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.getCustomAttr",
			method  : 'GET',
			success : function(data){
				JQuery("#addcustomattribute").html(data);
			}
		});
	}

	JQuery(".delcustom").live("click",function(){
		var myid    = JQuery(this).attr('id');
		var segment = myid.split("_");
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=showresults.deleteCustomAttr&id="+segment[2],
			method  : 'GET',
			success : function(data){
				JQuery("#"+myid).parent("tr").remove();
			}
		});
	});
</script>