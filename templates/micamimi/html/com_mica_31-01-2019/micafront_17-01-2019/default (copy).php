<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$app = JFactory::getApplication('site');

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');

$itemid        = 188;
$type1         = $app->input->get('m_type', '', 'raw');
$m_type_rating = $app->input->get("m_type_rating", '', 'raw');
$composite     = $app->input->get("composite", '', 'raw');
/*echo "<pre />";print_r($composite);
echo "<pre />";print_r($this->composite_attr);exit;*/
?>

<script type="text/javascript">
if(!window.jQuery)
{
	var script = document.createElement('script');
   script.type = "<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js";
   script.src = "path/to/jQuery";
   document.getElementsByTagName('head')[0].appendChild(script);
 }

</script>
<? /*<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script> */?>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>
<script type="text/javascript">
	var choosenjq = jQuery.noConflict();
	var JQuery = jQuery.noConflict();
	choosenjq(document).ready(function(){
		choosenjq("select").multiselect({selectedList: 1});
	});
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/field.js"></script>
<script type="text/javascript">
	var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var preselected       = '<?php echo $app->input->get("district", "", "raw"); ?>';
	var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type = '<?php echo $app->input->get("m_type", "", "raw"); ?>';

	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	jQuery(function() {
		jQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		jQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});
	jQuery(document).ready(function(){ });

	function checkAll(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",false);
		});
	}
</script>

<script src="templates/micamimi/js/jquery.scrollbar.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();
    });
</script>

<div class="row-fluid top-row">
	<div class="col-md-4">
		<h1 class="title-explore"><?php echo JText::_('SELECT_CR_MSG');?></h1>
	</div>
	<div class="col-md-8 text-right">
		<div class="sorter-tab">
			<ul>
				<li class="active"><a href="#"><i class="fas fa-table"></i>Table</a></li>
				<li><a href="#"><i class="far fa-chart-bar"></i>Graphs</a></li>
				<li><a href="#"><i class="fas fa-map-marker-alt"></i>GIS</a></li>
				<li><a href="#"><i class="fas fa-list-ul"></i>Quartiles</a></li>
				<li><a href="#"><i class="fas fa-tachometer-alt"></i>Potentio Meter</a></li>
			</ul>
		</div>
		<div class="worspacce-edit">
			My workspace <a href="#"><i class="fas fa-pencil-alt"></i></a>
			<h2>DEFAULT</h2>
		</div>
	</div>
</div>

<div class="explore-data">
	<form name="adminForm" id="micaform" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="POST">
		<div class="row">
			<div class="col-md-4">
	            <ul class="nav nav-tabs" role="tablist">
	                <li role="presentation" class="active">
	                	<a href="#state" aria-controls="state" role="tab" data-toggle="tab">
	                		<em class="icon-state"></em>State
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#district" aria-controls="district" role="tab" data-toggle="tab">
	                		<em class="icon-district"></em>District
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#type" aria-controls="type" role="tab" data-toggle="tab">
	                		<em class="icon-type"></em>Type
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#variables" aria-controls="variables" role="tab" data-toggle="tab">
	                		<em class="icon-variables"></em>Variables
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#mpi" aria-controls="mpi" role="tab" data-toggle="tab">
	                		<em class="icon-mpi"></em>MPI
	                	</a>
	                </li>
	                <li role="presentation">
	                	<a href="#swcs" aria-controls="swcs" role="tab" data-toggle="tab">
	                		<em class="icon-swcs"></em>SWCS
	                	</a>
	                </li>
	            </ul>

	            <!-- Tab panes -->
	            <div class="tab-content">
	            	<!-- state start -->
	                <div role="tabpanel" class="tab-pane active" id="state">
	                	<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" name="" id="searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="state_checkall" id="state_allcheck">
	                				<label for="allcheck"></label>
	                				|
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	                	</div>
	                	<div class="scrollbar-inner">
	                	<!-- <!-- <select name="state[]" id="state" class="inputbox-mainscr" onchange="getfrontdistrict(this)" multiple="multiple">
							<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
							$states = explode(",", $app->input->get("state", '', 'raw'));
							for($i = 0; $i < count($this->state_items); $i++){
								$selected = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
								<option value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $selected;?>>
									<?php echo $this->state_items[$i]->name; ?>
								</option>
							<?php  } ?>
						</select> --> 
						</div>

						<div class="scrollbar-inner">
                            <ul class="list1 statelist">
                            	<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
								$states = explode(",", $app->input->get("state", '', 'raw'));

									for($i = 0; $i < count($this->state_items); $i++){
										$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
										<li>
											<input type="checkbox" class="state_checkbox" name="state[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="check<?php echo $i;?>">
											<label for="check<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
										</li>
								<?php  } ?>
								
							</ul>
                        </div>
	                </div>

	                <!-- district start -->
	                <div role="tabpanel" class="tab-pane" id="district">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" name="" id="searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="" id="allcheck">
	                				<label for="allcheck"></label>
	                				|
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	                	</div>

	                	<span id="districtspan">
							<select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
								<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
							</select>
						</span>

						<div class="scrollbar-inner">
                            <ul class="list1 statelist">
								<?
								for($i = 0; $i < count($this->state_items); $i++){
									$selected = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
									<li>
									<input type="checkbox" class="state_checkbox" name="state[]"  value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="district_check<?php echo $i;?>">
									<label for="district_check<?php echo $i;?>">	<?php echo $this->state_items[$i]->name; ?></label></li>							
								<?php  } ?>
							</ul>
						</div>
	                </div>

					<!-- type start -->
	                <div role="tabpanel" class="tab-pane" id="type">
	                	<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('Select Type'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	                	</div>

	                	<?php $type = explode(',', $this->typesArray); ?>
						<select	name="m_type[]" id="m_type" class="inputbox" multiple="multiple" >
							<?php if(in_array("Rural", $type)) { ?>
								<option value="Rural" <?php echo (in_array("Rural", $type1)) ? "selected" : "";?> >	<?php echo JText::_('RURAL'); ?>	</option>
							<?php }
							if(in_array("Urban", $type)) { ?>
								<option value="Urban" <?php echo (in_array("Urban", $type1)) ? "selected" : "";?> >	<?php echo JText::_('URBAN'); ?>	</option>
							<?php }
							if(count($type) == 2) { ?>
								<option value="Total" <?php echo (in_array("Total", $type1)) ? "selected" : "";?> >	<?php echo JText::_('ALL_VARIABLE'); ?>	</option>
							<?php } ?>
						</select>
						<div class="box2">							
                            <ul class="type-list">								
								<li>
									<input type="checkbox" name="state[]"  value="" <?php echo $checked;?> id="check_rural" class="rural-check">
									<label for="check_rural"><span><?php echo JText::_('RURAL'); ?></span></label>
								</li>
								<li>
									<input type="checkbox" name="state[]"  value="" <?php echo $checked;?> id="check_urban" class="urban-check">
									<label for="check_urban"><span><?php echo JText::_('URBAN'); ?></span></label>
								</li>
								<li>
									<input type="checkbox" name="state[]"  value="" <?php echo $checked;?> id="check_all_variable" class="all-check">
									<label for="check_all_variable"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
								</li>
							</ul>							
						</div>
	                </div>

					<!-- variables start -->
	                <div role="tabpanel" class="tab-pane" id="variables">
	                	<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('Select Variables'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" name="" id="searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="" id="allcheck">
	                				<label for="allcheck"></label>
	                				|
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	                	</div>

	                	<div id="statetotal"></div>

						<div class="col-md-11">
		                	<div class="scrollbar-inner">
								<div class="list2" id="demographics">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>Demographics</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="child_population">
		                					<label for="child_population">Child Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="female_child_population">
		                					<label for="female_child_population">Female Child Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="female_non_worker">
		                					<label for="female_non_worker">Female Non Worker Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Female_Population">
		                					<label for="Female_Population">Female Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Female_Worker_Population">
		                					<label for="Female_Worker_Population">Female Worker Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Literate_Female">
		                					<label for="Literate_Female">Literate Female</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Literate_Male">
		                					<label for="Literate_Male">Literate Male</label>
										</li>
										<li>
											<input type="checkbox" name="" id="child_population">
		                					<label for="child_population">Child Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="female_child_population">
		                					<label for="female_child_population">Female Child Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="female_non_worker">
		                					<label for="female_non_worker">Female Non Worker Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Female_Population">
		                					<label for="Female_Population">Female Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Female_Worker_Population">
		                					<label for="Female_Worker_Population">Female Worker Population</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Literate_Female">
		                					<label for="Literate_Female">Literate Female</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Literate_Male">
		                					<label for="Literate_Male">Literate Male</label>
										</li>
									</ul>
								</div>
								<div class="list2" id="hh_size_and_usage">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>HH Size and Usage</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="Female_Headed_HH">
		                					<label for="Female_Headed_HH">Female Headed HH</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH">
		                					<label for="HH">HH</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_with_three_rooms">
		                					<label for="HH_with_three_rooms">HH with more than three rooms</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_with_one_room">
		                					<label for="HH_with_one_room">HH with one room</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_with_two_rooms">
		                					<label for="HH_with_two_rooms">HH with two rooms</label>
										</li>
										<li>
											<input type="checkbox" name="" id="Residential_HH">
		                					<label for="Residential_HH">Residential HH</label>
										</li>
									</ul>
								</div>

								<div class="list2" id="hh-basic">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>HH Basic Amenities</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="HH_having_Bathroom_Enclosure_with_Roof">
		                					<label for="HH_having_Bathroom_Enclosure_with_Roof">HH having Bathroom Enclosure with Roof</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Drinking_Water_Facility">
		                					<label for="HH_having_Drinking_Water_Facility">HH having Drinking Water Facility</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Latrine_Facility_within_Premises">
		                					<label for="HH_having_Latrine_Facility_within_Premises">HH having Latrine Facility within Premises</label>
										</li>
									</ul>
								</div>

								<div class="list2" id="hh-light-and-fuel">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>HH Light and Fuel</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="HH_having_Bathroom_Enclosure_with_Roof">
		                					<label for="HH_having_Bathroom_Enclosure_with_Roof">HH having Bathroom Enclosure with Roof</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Drinking_Water_Facility">
		                					<label for="HH_having_Drinking_Water_Facility">HH having Drinking Water Facility</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Latrine_Facility_within_Premises">
		                					<label for="HH_having_Latrine_Facility_within_Premises">HH having Latrine Facility within Premises</label>
										</li>
									</ul>
								</div>

								<div class="list2" id="agriculture">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>Agriculture</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="HH_having_Bathroom_Enclosure_with_Roof">
		                					<label for="HH_having_Bathroom_Enclosure_with_Roof">HH having Bathroom Enclosure with Roof</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Drinking_Water_Facility">
		                					<label for="HH_having_Drinking_Water_Facility">HH having Drinking Water Facility</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Latrine_Facility_within_Premises">
		                					<label for="HH_having_Latrine_Facility_within_Premises">HH having Latrine Facility within Premises</label>
										</li>
									</ul>
								</div>

								<div class="list2" id="financial-services">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>Financial Services</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="HH_having_Bathroom_Enclosure_with_Roof">
		                					<label for="HH_having_Bathroom_Enclosure_with_Roof">HH having Bathroom Enclosure with Roof</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Drinking_Water_Facility">
		                					<label for="HH_having_Drinking_Water_Facility">HH having Drinking Water Facility</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Latrine_Facility_within_Premises">
		                					<label for="HH_having_Latrine_Facility_within_Premises">HH having Latrine Facility within Premises</label>
										</li>
									</ul>
								</div>

								<div class="list2" id="commercial-use-of-premises">
									<div class="title-sector">
										<div class="col-md-5 col-xs-5">
											<h3>Commercial Use of Premises</h3>
										</div>
										<div class="col-md-7 col-xs-7 text-right">
											<div class="searchbox">
			                					<input type="search" name="" id="searchtop" placeholder="Search">
			                				</div>
			                				|
			                				<input type="checkbox" name="" id="demographics_all">
			                				<label for="demographics_all"></label>
			                				|
			                				<a href="#sort">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                				</div>
									</div>
									<ul>
										<li>
											<input type="checkbox" name="" id="HH_having_Bathroom_Enclosure_with_Roof">
		                					<label for="HH_having_Bathroom_Enclosure_with_Roof">HH having Bathroom Enclosure with Roof</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Drinking_Water_Facility">
		                					<label for="HH_having_Drinking_Water_Facility">HH having Drinking Water Facility</label>
										</li>
										<li>
											<input type="checkbox" name="" id="HH_having_Latrine_Facility_within_Premises">
		                					<label for="HH_having_Latrine_Facility_within_Premises">HH having Latrine Facility within Premises</label>
										</li>
									</ul>
								</div>

	                        </div>
                        </div>
                        <div class="col-md-1">
                        	<div class="custom-button text-center">
                        		<a href="javascript:void(0)">
                        			<i class="fas fa-plus-circle"></i><br>
                        			Custom
                        		</a>
                        	</div>
                        	<ul class="icons-list">
                        		<li>
                        			<a href="#demographics"><span class="demographics-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#hh_size_and_usage"><span class="hh-size-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#hh-basic"><span class="hh-basic-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#hh-light-and-fuel"><span class="hh-light-and-fuel-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#agriculture"><span class="agriculture-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#financial-services"><span class="financial-services-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#commercial-use-of-premises"><span class="commercial-use-of-premises-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#registered-active-companies"><span class="registered-active-companies-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#social-use-of-premises"><span class="social-use-of-premises-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#vehicle-ownership"><span class="vehicle-ownership-icon">&nbsp;</span></a>
                        		</li>
                        		<li>
                        			<a href="#miscellaneous"><span class="miscellaneous-icon">&nbsp;</span></a>
                        		</li>
                        		
                        	</ul>
                        </div>
	                </div>

	                <!-- mpi start -->
	                <div role="tabpanel" class="tab-pane" id="mpi">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('Mkt Potential Index'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<a href="#sort">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	                	</div>

						<select	name="m_type_rating[]"  class="inputbox" multiple="multiple" >
							<?php // edited heena 24/06/13 put a condition
							if(in_array("Rural", $type)) { ?>
								<option value="Rural" <?php echo (in_array("Rural",$m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('RURAL'); ?>	</option>
							<?php }
							if(in_array("Urban", $type)) { ?>
								<option value="Urban" <?php echo (in_array("Urban",$m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('URBAN'); ?>	</option>
							<?php }
							if(count($type) == 2){?>
								<option value="Total" <?php echo (in_array("Total",$m_type_rating)) ? "selected": (empty($m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('ALL_VARIABLE'); ?>	</option>
							<?php } // edited end?>
						</select>

	                	<div class="box2">
							
							<ul class="type-list">								
								<li>
									<input type="checkbox" name="state[]"  value="" <?php echo $checked;?> id="check_rural" class="rural-check">
									<label for="check_rural"><span><?php echo JText::_('RURAL'); ?></span></label>
								</li>
								<li>
									<input type="checkbox" name="state[]"  value="" <?php echo $checked;?> id="check_urban" class="urban-check">
									<label for="check_urban"><span><?php echo JText::_('URBAN'); ?></span></label>
								</li>
								<li>
									<input type="checkbox" name="state[]"  value="" <?php echo $checked;?> id="check_all_variable" class="all-check">
									<label for="check_all_variable"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
								</li>
							</ul>
							
						</div>
	                </div>

	                <!-- swcs start -->
	                <div role="tabpanel" class="tab-pane" id="swcs">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('sector wise composite score'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" name="" id="searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="" id="allcheck">
	                				<label for="allcheck"></label>
	                			</div>
	                		</div>
	                	</div>

	                	<select name="composite[]" id="composite" class="inputbox-mainscr" multiple="multiple">
							<?php // edited heena 24/06/13 put a condition
							if(in_array("Rural", $type)) {
								$prefix = array("Rural");
							}
							if(in_array("Urban", $type)) {
								$prefix = array("Urban");
							}
							if(count($type) == 2){
								$prefix = array("Rural","Urban","Total");
							} // edited end

							foreach($this->composite_attr as $eachattr){
								echo '<optgroup  label="'.ucwords(strtolower(JTEXT::_($eachattr->field))).'">';
								foreach($prefix as $eachprefix){
									$checked = (in_array($eachprefix."_".$eachattr->field, $composite)) ? " selected " : "";
									echo '<option value="'.$eachprefix."_".$eachattr->field.'" '.$checked.'>'.ucwords(strtolower(JTEXT::_($eachprefix))).'</option>';
								}
								echo '</optgroup>';
							} ?>
						</select>

						<div class="scrollbar-inner">
							<div class="list3">
								<div class="title-sector">
									<div class="col-md-10 col-xs-10">
										<h3>Agricultural</h3>
									</div>
									<div class="col-md-2 col-xs-2 text-right">
										<input type="checkbox" name="" id="agricultural">
		                				<label for="agricultural"></label>
	                				</div>
								</div>
								<ul>
									<li>
										<input type="checkbox" name="" id="agricultural1">
	                					<label for="agricultural1">Rural</label>
									</li>
									<li>
										<input type="checkbox" name="" id="agricultural2">
	                					<label for="agricultural2">Urban</label>
									</li>
									<li>
										<input type="checkbox" name="" id="agricultural3">
	                					<label for="agricultural3">Total</label>
									</li>
								</ul>
							</div>
							<div class="list3">
								<div class="title-sector">
									<div class="col-md-10 col-xs-10">
										<h3>Basic Amenities</h3>
									</div>
									<div class="col-md-2 col-xs-2 text-right">
										<input type="checkbox" name="" id="amenities">
		                				<label for="amenities"></label>
	                				</div>
								</div>
								<ul>
									<li>
										<input type="checkbox" name="" id="amenities1">
	                					<label for="amenities1">Rural</label>
									</li>
									<li>
										<input type="checkbox" name="" id="amenities2">
	                					<label for="amenities2">Urban</label>
									</li>
									<li>
										<input type="checkbox" name="" id="amenities3">
	                					<label for="amenities3">Total</label>
									</li>
								</ul>
							</div>
							<div class="list3">
								<div class="title-sector">
									<div class="col-md-10 col-xs-10">
										<h3>Commercial Use of Premises</h3>
									</div>
									<div class="col-md-2 col-xs-2 text-right">
										<input type="checkbox" name="" id="premises">
		                				<label for="premises"></label>
	                				</div>
								</div>
								<ul>
									<li>
										<input type="checkbox" name="" id="premises1">
	                					<label for="premises1">Rural</label>
									</li>
									<li>
										<input type="checkbox" name="" id="premises2">
	                					<label for="premises2">Urban</label>
									</li>
									<li>
										<input type="checkbox" name="" id="premises3">
	                					<label for="premises3">Total</label>
									</li>
								</ul>
							</div>
							<div class="list3">
								<div class="title-sector">
									<div class="col-md-10 col-xs-10">
										<h3>Financial Services</h3>
									</div>
									<div class="col-md-2 col-xs-2 text-right">
										<input type="checkbox" name="" id="financial">
		                				<label for="financial"></label>
	                				</div>
								</div>
								<ul>
									<li>
										<input type="checkbox" name="" id="financial1">
	                					<label for="financial1">Rural</label>
									</li>
									<li>
										<input type="checkbox" name="" id="financial2">
	                					<label for="financial2">Urban</label>
									</li>
									<li>
										<input type="checkbox" name="" id="financial3">
	                					<label for="financial3">Total</label>
									</li>
								</ul>
							</div>

							<div class="list3">
								<div class="title-sector">
									<div class="col-md-10 col-xs-10">
										<h3>Light and Fuel</h3>
									</div>
									<div class="col-md-2 col-xs-2 text-right">
										<input type="checkbox" name="" id="light">
		                				<label for="light"></label>
	                				</div>
								</div>
								<ul>
									<li>
										<input type="checkbox" name="" id="light1">
	                					<label for="light1">Rural</label>
									</li>
									<li>
										<input type="checkbox" name="" id="light2">
	                					<label for="light2">Urban</label>
									</li>
									<li>
										<input type="checkbox" name="" id="light3">
	                					<label for="light3">Total</label>
									</li>
								</ul>
							</div>

							<div class="list3">
								<div class="title-sector">
									<div class="col-md-10 col-xs-10">
										<h3>Media Ownership</h3>
									</div>
									<div class="col-md-2 col-xs-2 text-right">
										<input type="checkbox" name="" id="media">
		                				<label for="media"></label>
	                				</div>
								</div>
								<ul>
									<li>
										<input type="checkbox" name="" id="media1">
	                					<label for="media1">Rural</label>
									</li>
									<li>
										<input type="checkbox" name="" id="media2">
	                					<label for="media2">Urban</label>
									</li>
									<li>
										<input type="checkbox" name="" id="media3">
	                					<label for="media3">Total</label>
									</li>
								</ul>
							</div>

                        </div>
	                </div>
	            </div>
			</div>
			<div class="col-md-8">
				<div class="full-data-view">

				<div id="table">
				</div>
				<div id="graph">
				</div>
				<div id="gis">
				</div>
				<div id="quartiles">
				</div>
				<div id="potentiometer">
				</div>



					<!-- <div class="popup_div">
						<div class="popup_inner">
							<p>Please select variables from left panel or by clicking the side icons to view report</p>
							<div class="button-style text-right">
								<a href="javascript:void(0)">APPLY CHANGES</a>
							</div>
						</div>
					</div>
 -->

					<!-- <div class="halfright" >
						<div class="righttop">
							<div class="maintable" style="line-height: 36px;">
								<div class="right" >
									<h3>Step 1 :  Select State</h3>
								</div>
							</div>
							<div class="maintable" style="line-height: 36px;">
								<div class="right">
									<h3>Step 2 :  Select District of selected State</h3>
								</div>
							</div>
							<div class="maintable" style="line-height: 36px;">
								<div class="right">
									<h3>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</h3>
								</div>
							</div>
							<div class="maintable" style="line-height: 36px;">
								<div class="right">
									<h3>Step 4 : Select the Variables you want to view. (Multiple selection allowed) </h3>
								</div>
							</div>
							<div class="maintable" style="line-height: 36px;">
								<div class="right">
									<h3>Step 5 : Select MPI (Multiple selection allowed)</h3>
								</div>
							</div>
							<div class="maintable" style="line-height: 36px;">
								<div class="right">
									<h3>Step 6 : Select sector wise composite score. (Multiple selection allowed)</h3>
								</div>
							</div>
							<div class="maintable" style="line-height: 36px;">
								<div class="right">
									<h3><span style="color:black;">Note :</span> Mark  <span style="color:black;"> ‘All’ </span> in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</h3>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
		<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
			<div class="readon frontbutton">
				<input type="button" name="reset" onClick="window.location='index.php?option=com_mica&view=micafront&reset=1';"
					class="readon button" value="<?php echo JText::_('Reset'); ?>" />
			</div>
			<div class="readon frontbutton">
				<input type="submit" name="submit" id="submit" class="readon button" value="<?php echo JText::_('SHOW_DATA'); ?>" />
			</div>
			<input type="hidden" name="refeterview" value="micafront" />
			<input type="hidden" name="option" 		value="com_mica" />
			<input type="hidden" name="zoom" id="zoom" value="6" />
			<input type="hidden" name="view" 		value="showresults" />
			<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
			<input type="hidden" id="comparedata" 	value="0" />
		</div>
	</form>
</div>


<!-- <div style="height:400px;">
	<form name="adminForm" id="micaform" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="POST">
		<div class="maincontainer">
			<div class="frontitle">
				<span><h1><?php echo JText::_('SELECT_CR_MSG');?></h1></span>
			</div>
			<div class="halfleft" >
				<div class="leftbottop">
					<div class="maintable">
						<div class="left">
							<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
						</div>
						<div class="right">
							<h3>
								<select name="state[]" id="state" class="inputbox-mainscr" onchange="getfrontdistrict(this)" multiple="multiple">
									<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
									$states = explode(",", $app->input->get("state", '', 'raw'));
									for($i = 0; $i < count($this->state_items); $i++){
										$selected = (in_array($this->state_items[$i]->name, $states)) ? " selected " :  ""; ?>
										<option value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $selected;?>>
											<?php echo $this->state_items[$i]->name; ?>
										</option>
									<?php  } ?>
								</select>
							</h3>
						</div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
						</div>
						<div class="right">
							<h3>
								<span id="districtspan">
									<select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
										<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
									</select>
								</span>
							</h3>
						</div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3>Type</h3>
						</div>
						<div class="right">
							<?php $type = explode(',', $this->typesArray); ?>
							<h3>
								<select	name="m_type[]" id="m_type" class="inputbox" multiple="multiple" >
									<?php if(in_array("Rural", $type)) { ?>
										<option value="Rural" <?php echo (in_array("Rural", $type1)) ? "selected" : "";?> >	<?php echo JText::_('RURAL'); ?>	</option>
									<?php }
									if(in_array("Urban", $type)) { ?>
										<option value="Urban" <?php echo (in_array("Urban", $type1)) ? "selected" : "";?> >	<?php echo JText::_('URBAN'); ?>	</option>
									<?php }
									if(count($type) == 2) { ?>
										<option value="Total" <?php echo (in_array("Total", $type1)) ? "selected" : "";?> >	<?php echo JText::_('ALL_VARIABLE'); ?>	</option>
									<?php } ?>
								</select>
							</h3>
						</div>
					</div>
				</div>
				<div class="leftbottom">
					<div class="maintable" >
						<div class="left">
							<h3>Variable</h3>
						</div>
						<div class="right">
							<h3><div id="statetotal"></div></h3>
						</div>
					</div>
					<div class="maintable">
						<div class="maintable togglingattribute" ></div>
					</div>

					<div class="maintable" >
						<div class="left">
							<h3>Mkt Potential Index</h3>
						</div>
						<div class="right">
							<h3>
								<select	name="m_type_rating[]"  class="inputbox" multiple="multiple" >
									<?php // edited heena 24/06/13 put a condition
									if(in_array("Rural", $type)) { ?>
										<option value="Rural" <?php echo (in_array("Rural",$m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('RURAL'); ?>	</option>
									<?php }
									if(in_array("Urban", $type)) { ?>
										<option value="Urban" <?php echo (in_array("Urban",$m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('URBAN'); ?>	</option>
									<?php }
									if(count($type) == 2){?>
										<option value="Total" <?php echo (in_array("Total",$m_type_rating)) ? "selected": (empty($m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('ALL_VARIABLE'); ?>	</option>
									<?php } // edited end?>
								</select>
							</h3>
						</div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3>Composite Score</h3>
						</div>
						<div class="right">
							<h3>
								<select name="composite[]" id="composite" class="inputbox-mainscr" multiple="multiple">
									<?php // edited heena 24/06/13 put a condition
									if(in_array("Rural", $type)) {
										$prefix = array("Rural");
									}
									if(in_array("Urban", $type)) {
										$prefix = array("Urban");
									}
									if(count($type) == 2){
										$prefix = array("Rural","Urban","Total");
									} // edited end

									foreach($this->composite_attr as $eachattr){
										echo '<optgroup  label="'.ucwords(strtolower(JTEXT::_($eachattr->field))).'">';
										foreach($prefix as $eachprefix){
											$checked = (in_array($eachprefix."_".$eachattr->field, $composite)) ? " selected " : "";
											echo '<option value="'.$eachprefix."_".$eachattr->field.'" '.$checked.'>'.ucwords(strtolower(JTEXT::_($eachprefix))).'</option>';
										}
										echo '</optgroup>';
									} ?>
								</select>
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="halfright" >
				<div class="righttop">
					<div class="maintable" style="line-height: 36px;">
						<div class="right" >
							<h3>Step 1 :  Select State</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 2 :  Select District of selected State</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 4 : Select the Variables you want to view. (Multiple selection allowed) </h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 5 : Select MPI (Multiple selection allowed)</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 6 : Select sector wise composite score. (Multiple selection allowed)</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3><span style="color:black;">Note :</span> Mark  <span style="color:black;"> ‘All’ </span> in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
			<div class="readon frontbutton">
				<input type="button" name="reset" onClick="window.location='index.php?option=com_mica&view=micafront&reset=1';"
					class="readon button" value="<?php echo JText::_('Reset'); ?>" />
			</div>
			<div class="readon frontbutton">
				<input type="submit" name="submit" id="submit" class="readon button" value="<?php echo JText::_('SHOW_DATA'); ?>" />
			</div>
			<input type="hidden" name="refeterview" value="micafront" />
			<input type="hidden" name="option" 		value="com_mica" />
			<input type="hidden" name="zoom" id="zoom" value="6" />
			<input type="hidden" name="view" 		value="showresults" />
			<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
			<input type="hidden" id="comparedata" 	value="0" />
		</div>

	</form>
</div> -->

<script type="text/javascript">
	
	jQuery('#state_allcheck').click(function(event) {
		/* Act on the event */
		jQuery('.state_checkbox').prop('checked',jQuery(this).prop('checked'));
	});
	jQuery(document).ready(function($) {
		
	
		jQuery('#submit').click(function(event) {

			var data = $('form').serialize();
			
		
		/* Act on the event */
		
			$.ajax({
				url: 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>',
				type: 'POST',
				data: data,
			})
			.done(function() {
				console.log("success");
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
			event.stopPropagation();
			return false;
		});


	//get all district if any state checkbox gets selected
	$('.state_checkbox').click(function(event) {
		/* Act on the event */
		getfrontdistrict(this);
	});
	
	});



</script>