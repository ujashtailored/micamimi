<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.micamimi
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

ini_set('pcre.backtrack_limit',100000000);
ini_set('pcre.recursion_limit',100000000);

$itemid = JRequest::getVar('Itemid');
$menu = &JSite::getMenu();
$active = $menu->getItem($itemid);

$params = $menu->getParams( $active->id );
$pageclass = $params->get( 'pageclass_sfx' );

/** @var JDocumentHtml $this */
$document = &JFactory::getDocument();
$app      = JFactory::getApplication();

$user     = JFactory::getUser();
$this->setHtml5(true);// Output as HTML5
$params   = $app->getTemplate(true)->params;// Getting params from template

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view   = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task   = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$id     = $app->input->getCmd('id', '');
$sitename = $app->get('sitename');



//JHtml::_('bootstrap.framework');// Add JavaScript Frameworks
JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));// Add template js
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));// Add html5 shiv
JHtml::_('script', 'bootstrap.min.js', array('version' => 'auto', 'relative' => true));// Added Bootstrap js

JHtml::_('stylesheet', 'user.css', array('version' => 'auto', 'relative' => true));// Check for a custom CSS file
JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));// Check for a custom js file
JHtml::_('bootstrap.loadCss', false, $this->direction);// Load optional RTL Bootstrap CSS
JHtml::_('stylesheet', 'template_protostar.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'template.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets
JHtml::_('stylesheet', 'bootstrap.min.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets
JHtml::_('stylesheet', 'custom.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets


$body_class = '';
if(	$option == 'com_content' 	&& 	$view == 'article'  && $id == 100 )   {
 	$body_class = " special_closebutton ";
 	JHTML::_('behavior.modal');
}

$content = "bodyonly";
if($this->countModules('left-menu')){
	if($option == 'com_rsform' && $view = 'rsform'){
		$content = "bodyleftonly_rsform";
	}else{
		$content = "bodyleftonly";
	}
}

if($option == 'com_user' && $view == 'register'){
	$content = "bodyleftregister";
}
$document->setGenerator('');

?>

<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<!--[if IE 7]>
		<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!--[if IE 8]>
		<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/ie8only.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<?php 
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		if ($menu->getActive() == $menu->getDefault()) {
			echo '<link rel="stylesheet" type="text/css" href="templates/micamimi/css/jquery.scrollbar.css">';
			echo '<script type="text/javascript" src="templates/micamimi/js/jquery.scrollbar.js"></script>';
			echo '<script>jQuery(document).ready(function(){ jQuery(".scrollbar-inner").scrollbar(); });</script>';
		}
	?>
<style type="text/css">
	/* The Modal (background) */
.modal2 {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index:1025; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
.modal2 .close {
	display:  none;
}

/* Modal Content */
.modal2-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close2 {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close2:hover,
.close2:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
	<style type="text/css">
			.main_news_loader {
				display: none;
				position: fixed;
				left: 0;
				top: 0;
				width: 100%;
				z-index: 1024000;
				background: rgba(255,255,255,0.7);
				height: 100%;
				text-align: center;
			}
			.main_news_loader .loader-img1 {
				position: absolute;
				top: 50%;
				left: 50%;
				margin-left: -32px;
				margin-top: -32px;
			}
		</style>
</head>
<body class="site <?php echo $body_class; ?> <?php echo $pageclass ? htmlspecialchars($pageclass) : 'default';  ?> <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. ($this->direction === 'rtl' ? ' rtl' : '');
?>">

<?php 
			if ($menu->getActive() == $menu->getDefault()) {
				?>
				<div id="homepagePopup" class="modal2">
				  <!-- Modal content -->
				  <div class="modal2-content">
				    <span class="close2">&times;</span>
				    <div class="modal2-text"><jdoc:include type="message" /></div>
				  </div>

				</div>

			<?php
			}
			?>

	<div id="bd">
		<div id="main_loader" class="main_news_loader" style="display: none;">
			<div class="loader-img1" id="newsletter_loader">
	  			<img src="/images/loaderimg.gif" alt="">
	  		</div>
		</div>
		
		<div>
			<div id="header" class="clearfix">
				<div class="container">
					<div class="col-md-5">
						<!-- <jdoc:include type="modules" name="left_logo" style="none"/> -->
						<div class="micalogo">
							<p style="margin: 10px 0px 0px 0px;">
								<a href="<?php echo $this->baseurl; ?>/" >
									<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo_new.png" border="0" alt="mica" />
								</a>
							</p>
						</div>
						<h1 class="logo_img" title="mica">
							<a href="<?php echo $this->baseurl; ?>/" >
								<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo_home.png" border="0" alt="mica" />
							</a>
							<a href="<?php echo $this->baseurl; ?>/" >
								<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/mvmi-logo.png" border="0" alt="mica" />
							</a>
						</h1>
						<jdoc:include type="modules" name="logo-content" style="none"/>
					</div>
					<div class="top-right col-md-7">
						<jdoc:include type="modules" name="user9" style="none"/>
					</div>
				</div>
			</div>
			<div class="menuwrapper">
				<div class="container">
					<div class="row">
						<div class="col-md-10">
							<div id="menus_area">
								<div id="sfmenu">
									<?php if ($this->countModules('smenu')) : ?>
										<nav class="navigation" role="navigation">
											<div class="navbar pull-left">
												<a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
													<span class="element-invisible"><?php echo JTEXT::_('TPL_PROTOSTAR_TOGGLE_MENU'); ?></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</a>
											</div>
											<div class="nav-collapse">
												<!-- <jdoc:include type="modules" name="position-1" style="none" /> -->
												<!-- <jdoc:include type="modules" name="mica-menu" style="xhtml" /> -->
												<jdoc:include type="modules" name="smenu" style="xhtml" />
											</div>
										</nav>
									<?php endif; ?>
								</div>
								<?php if ($this->countModules('mmenu')) : ?>
									<div class="mobile_menu">
										<jdoc:include type="modules" name="mmenu" style="xhtml" />
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="clearfix">
								<div class="acc_link text-right">
									<?php if($user->get('id') > 0){ ?>
										<?php /*<jdoc:include type="modules" name="account_link" /> */ ?>
										<div class="custom">
											<p>
												<a href="<?php echo JRoute::_('index.php?option=com_mica&view=dashboard'); ?>">My Account</a>
												|
												<a href="javascript: void(0)" onclick="document.micalogoutform.submit();">Logout</a>
												<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="micalogoutform" class="form-vertical" name="micalogoutform" >
													<input type="hidden" name="option" value="com_users" />
													<input type="hidden" name="task" value="user.logout" />
													<?php echo JHtml::_('form.token'); ?>
												</form>
											</p>
										</div>
									<?php }else{ ?>
										<jdoc:include type="modules" name="user4" style="rounded"/>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div id="bredcum">
					<div class="container">
						<jdoc:include type="modules" name="breadcrumb" />
					</div>
				</div> -->
			</div>
			<?php if ($this->countModules('banner-content')) : ?>
				<div class="bannerarea">
					<div class="container">
						<jdoc:include type="modules" name="banner-content" style="xhtml" />
					</div>
				</div>
			<?php endif; ?>

			<?php if ($this->countModules('home-banner')) : ?>
				<div class="bannersection">
					<jdoc:include type="modules" name="home-banner" style="xhtml" />
				</div>
			<?php endif; ?>			

			<?php if ($this->countModules('user_menu')) : ?>
				<div class="userwrapper">
					<div class="container">
						<jdoc:include type="modules" name="user_menu" style="xhtml" />
					</div>
				</div>
			<?php endif; ?>
			
			<div class="main-container-box">
							<div class="container">
								<div id="mainbody" class="clearfix">
									<div id="body_content1" class="clearfix">
										<div id="<?php echo $content; ?>">
											<div id="inner_mainbody" class="clearfix">
												<!-- <?php if($this->countModules('left-menu')){ ?>
													<div id="leftcol">
														<jdoc:include type="modules" name="left-menu" style="rounded" />
													</div>
												<?php } ?> -->
												<div id="<?php echo $content; ?>">
													<div class="bodyarea_main">
														<jdoc:include type="message" />
														<!-- msg -->
														<jdoc:include type="component" />
													</div>

													<div class="bodynew">
														<jdoc:include type="modules" name="rsmonial" style="rounded" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
			<?php if ($this->countModules('mpi-section')) : ?>
				<div class="mpi-wrapper">
					<div class="container">
						<jdoc:include type="modules" name="mpi-section" style="xhtml" />
					</div>
				</div>
			<?php endif; ?>
		
			<?php if ($this->countModules('industry-section')) : ?>
				<div class="industry-wrapper">
					<div class="container">
						<jdoc:include type="modules" name="industry-section" style="xhtml" />
					</div>
				</div>
			<?php endif; ?>

			

			
			<?php if ($this->countModules('bottom-section1') or $this->countModules('bottom-section2'))  : ?>
				<div class="bottom-section">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<jdoc:include type="modules" name="bottom-section1" style="xhtml" />
							</div>
							<div class="col-md-6 right-section">
								<jdoc:include type="modules" name="bottom-section2" style="xhtml" />
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			

			<div id="footer" class="clearfix" >
				<div class="container">
					<!-- <div id="footer_left">
						<jdoc:include type="modules" name="copyright-mica" />
					</div>
					<div id="footer_right">
						<jdoc:include type="modules" name="footer" />
					</div> -->
					<div class="copyright-area text-center">
						<jdoc:include type="modules" name="copyright-mica" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
<?php 
	if ($menu->getActive() == $menu->getDefault()) {
	?>
	<script type="text/javascript">
	// Get the modal
	jQuery(document).ready(function($) {
		console.log("erer");
		var modal = $("#homepagePopup");
		var span = $(".close2")[0];
		if($(modal).find('.modal2-text').html()!='')
		{
			if($('.modal2-text').find('#system-message-container').html().trim()!='')
		  		$(modal).show();
		  

		}
		
		$('body').on('click','.close2',function(event) {
			  	//if (event.target == modal) {
			  	$(modal).hide();
			  	$('#system-message .close').click();
		});

		

	
	});
		
</script>
<?php  } ?>