<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */
$document = &JFactory::getDocument();
$app      = JFactory::getApplication();
$user     = JFactory::getUser();
$this->setHtml5(true);// Output as HTML5
$params   = $app->getTemplate(true)->params;// Getting params from template

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view   = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task   = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$id     = $app->input->getCmd('id', '');
$sitename = $app->get('sitename');

//JHtml::_('bootstrap.framework');// Add JavaScript Frameworks
JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));// Add template js
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));// Add html5 shiv
JHtml::_('stylesheet', 'user.css', array('version' => 'auto', 'relative' => true));// Check for a custom CSS file
JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));// Check for a custom js file
JHtml::_('bootstrap.loadCss', false, $this->direction);// Load optional RTL Bootstrap CSS
JHtml::_('stylesheet', 'template_protostar.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets
JHtml::_('stylesheet', 'template.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets

$body_class = '';
if(	$option == 'com_content' 	&& 	$view == 'article'  && $id == 100 )   {
 	$body_class = " special_closebutton ";
 	JHTML::_('behavior.modal');
}

$content = "bodyonly";
if($this->countModules('left-menu')){
	if($option == 'com_rsform' && $view = 'rsform'){
		$content = "bodyleftonly_rsform";
	}else{
		$content = "bodyleftonly";
	}
}

if($option == 'com_user' && $view == 'register'){
	$content = "bodyleftregister";
}
$document->setGenerator('');
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<!--[if IE 7]>
		<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!--[if IE 8]>
		<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/ie8only.css" rel="stylesheet" type="text/css" />
	<![endif]-->
</head>
<body class="site <?php echo $body_class; ?>">
	<div id="bd">
		<div id="main-wrapper">
			<?php if($option == 'com_content' 	&& 	$view == 'featured'){ ?>
				<div id="left_logo">
					<jdoc:include type="modules" name="left_logo" />
				</div>
				<div id="header" class="clearfix">
					<div class="logo_content">
						<jdoc:include type="modules" name="user9" />
					</div>
					<div id="logo">
						<h1 class="logo_img" title="mica">
							<a href="<?php echo $this->baseurl; ?>/" >
								<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo_home.png" border="0" alt="mica" />
							</a>
						</h1>
					</div>
				</div>
				<jdoc:include type="message" />
				<div id="mainbody" class="clearfix">
					<div id="rightcol_login" class="clearfix">
						<jdoc:include type="modules" name="user4" style="rounded"/>
					</div>
					<div id="body_content" class="clearfix">
						<jdoc:include type="modules" name="user13" style="rounded"/>
					</div>
					<div id="mainmenu" class="clearfix">
						<jdoc:include type="modules" name="mica-menu" style="xhtml" />
					</div>
				</div>

			<?php }else{ ?>

				<div id="inner_header" class="clearfix">
					<div id="inner_logo">
						<h1 class="logo_img" title="mica">
							<a href="<?php echo $this->baseurl; ?>/" >
								<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo.png" border="0" alt="mica" />
							</a>
						</h1>
					</div>
					<div class="acc_link">
						<?php if($user->get('id') > 0){ ?>
							<?php /*<jdoc:include type="modules" name="account_link" /> */ ?>
							<div class="custom">
								<p>
									<a href="<?php echo JRoute::_('index.php?option=com_mica&view=dashboard'); ?>">My Account</a>
									|
									<a href="javascript: void(0)" onclick="document.micalogoutform.submit();">Logout</a>
									<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="micalogoutform" class="form-vertical" name="micalogoutform" >
										<input type="hidden" name="option" value="com_users" />
										<input type="hidden" name="task" value="user.logout" />
										<?php echo JHtml::_('form.token'); ?>
									</form>
								</p>
							</div>
						<?php } ?>
					</div>
					<div id="menus_area">
						<div id="sfmenu">
							<jdoc:include type="modules" name="smenu" style="xhtml" />
						</div>
						<div id="bredcum">
							<jdoc:include type="modules" name="breadcrumb" />
						</div>
					</div>
				</div>
				<div id="inner_mainbody" class="clearfix">
					<?php if($this->countModules('left-menu')){ ?>
						<div id="leftcol">
							<jdoc:include type="modules" name="left-menu" style="rounded" />
						</div>
					<?php } ?>
					<div id="<?php echo $content; ?>">
						<div class="bodyarea_main">
							<jdoc:include type="message" />
							<jdoc:include type="component" />
						</div>

						<div class="bodynew">
							<jdoc:include type="modules" name="rsmonial" style="rounded" />
						</div>
					</div>
				</div>

			<?php } ?>

			<div id="footer" class="clearfix" >
				<div id="footer_left">
					<jdoc:include type="modules" name="copyright-mica" />
				</div>
				<div id="footer_right">
					<jdoc:include type="modules" name="footer" />
				</div>
			</div>
		</div>
	</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
