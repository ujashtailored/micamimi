<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0"
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
 xmlns="http://www.opengis.net/sld"
 xmlns:ogc="http://www.opengis.net/ogc"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA[india_information]]></Name>
			<UserStyle>
			<Title>s0</Title>
			<FeatureTypeStyle>
			
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[The Dangs]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ddccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Dohad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ddccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Porbandar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Narmada]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Tapi]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
			</FeatureTypeStyle>
			</UserStyle>
			</NamedLayer></StyledLayerDescriptor>