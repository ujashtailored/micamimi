<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0"
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
 xmlns="http://www.opengis.net/sld"
 xmlns:ogc="http://www.opengis.net/ogc"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA[india_information]]></Name>
			<UserStyle>
			<Title>s0</Title>
			<FeatureTypeStyle>
			
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sheopur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Morena]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Datia]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Panna]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhopal]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Harda]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Narsimhapur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Dindori]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Guna]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Ashoknagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Anuppur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Alirajpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Umaria]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Burhanpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e5d8ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhind]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Gwalior]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Tikamgarh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Neemuch]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Shajapur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Rajgarh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Vidisha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sehore]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Raisen]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Hoshangabad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Katni]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jabalpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jhabua]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Barwani]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Indore]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Shahdol]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sidhi]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Singrauli]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccb2ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Shivpuri]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Chhatarpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Damoh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Rewa]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Ratlam]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Ujjain]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Betul]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Mandla]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Balaghat]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Seoni]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Dewas]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[East Nimar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#b28cff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Satna]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Mandsaur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Chhindwara]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[West Nimar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Dhar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#9966ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
			</FeatureTypeStyle>
			</UserStyle>
			</NamedLayer></StyledLayerDescriptor>