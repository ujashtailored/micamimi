<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0"
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
 xmlns="http://www.opengis.net/sld"
 xmlns:ogc="http://www.opengis.net/ogc"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA[india_information]]></Name>
			<UserStyle>
			<Title>s0</Title>
			<FeatureTypeStyle>
			
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Gaya]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6e0d6</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Munger]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6e0d6</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Begusarai]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6e0d6</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Gopalganj]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6e0d6</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sitamarhi]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6e0d6</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jamui]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Rohtas]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Buxar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Patna]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Banka]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhagalpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Samastipur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Saran]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Saharsa]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#adc1ad</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Arwal]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Nawada]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhojpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sheikhpura]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Vaishali]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Muzaffarpur]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Katihar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Purnia]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Araria]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Supaul]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sheohar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#84a384</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jehanabad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Nalanda]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Khagaria]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Darbhanga]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Madhepura]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kishanganj]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Madhubani]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#5b845b</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kaimur-bhabua]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#336633</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Lakhisarai]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#336633</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Siwan]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#336633</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Purba Champaran]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#336633</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Pashchim Champaran]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#336633</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
			</FeatureTypeStyle>
			</UserStyle>
			</NamedLayer></StyledLayerDescriptor>