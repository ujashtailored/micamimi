<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="registration-complete<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<h1>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
	<?php endif; ?>

	<div>
		<div><?php echo JText::_('Account Pending') ?></div>
		<div>
			<p>Dear Subscriber,</p>
			<p>Thanks a lot for your association with MICA Indian Marketing Intelligence (MIMI). Please note that your payment and account authorization is under process. We will contact you soon, once your payment is realized and your account is activated.</p>
			<p>For any information or query, please contact :- +91 02717 308314 / +91-9586859700</p>
		</div>
	</div>
</div>
