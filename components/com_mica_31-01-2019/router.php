<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Routing class from com_mica
 *
 * @since  3.2
 */
class MicaRouter extends JComponentRouterView
{
	/**
	 * Mica Component router constructor
	 *
	 * @param   JApplicationCms  $app   The application object
	 * @param   JMenu            $menu  The menu object to work with
	 */
	public function __construct($app = null, $menu = null)
	{
		$this->registerView(new JComponentRouterViewconfiguration('dashboard'));
		$this->registerView(new JComponentRouterViewconfiguration('fullmap'));
		$this->registerView(new JComponentRouterViewconfiguration('micafront'));
		$this->registerView(new JComponentRouterViewconfiguration('showresults'));
		$this->registerView(new JComponentRouterViewconfiguration('villagefront'));
		$this->registerView(new JComponentRouterViewconfiguration('villageshowresults'));

		parent::__construct($app, $menu);

		$this->attachRule(new JComponentRouterRulesMenu($this));

		$params = JComponentHelper::getParams('com_mica');

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new JComponentRouterRulesStandard($this));
			$this->attachRule(new JComponentRouterRulesNomenu($this));
		}
		else
		{
			JLoader::register('MicaRouterRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			$this->attachRule(new MicaRouterRulesLegacy($this));
		}
	}
}

/**
 * Mica router functions
 *
 * These functions are proxys for the new router interface
 * for old SEF extensions.
 *
 * @param   array  &$query  REQUEST query
 *
 * @return  array  Segments of the SEF url
 *
 * @deprecated  4.0  Use Class based routers instead
 */
function micaBuildRoute(&$query)
{
	$app = JFactory::getApplication();
	$router = new UsersRouter($app, $app->getMenu());

	return $router->build($query);
}

/**
 * Convert SEF URL segments into query variables
 *
 * @param   array  $segments  Segments in the current URL
 *
 * @return  array  Query variables
 *
 * @deprecated  4.0  Use Class based routers instead
 */
function micaParseRoute($segments)
{
	$app = JFactory::getApplication();
	$router = new UsersRouter($app, $app->getMenu());

	return $router->parse($segments);
}
