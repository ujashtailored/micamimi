<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$app = JFactory::getApplication('site');

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jqueryselect/chosen.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/maps/theme/default/style.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/maps/examples/style.css', 'text/css');

?>

<script type="text/javascript">
	var JQuery = jQuery.noConflict();
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.cookie.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jqueryselect/chosen.jquery.js"></script>
<script type="text/javascript">
	var choosenjq = oldJQuery.noConflict();
	choosenjq(document).ready(function(){
		choosenjq("select").chosen();
		choosenjq("#avail_attribute").chosen({
			allow_single_deselect : true
		});
	});
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.mousewheel.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/field.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/swfobject.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amcharts.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amfallback.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/maps/OpenLayers.js"></script>

<div id="map"  style="width: 938px; height: 650px;">
	<?php /*
		<table cellspacing="0" cellpadding="0" border="0" style="margin:5px 5px 5px 5px;">
			<tbody>
				<tr>
					<td>
						<a href="javascript:void(0);" onclick="downloadMapPdf();" id="options" >
							<img  title="Export Map" src="< ?php echo JURI::base();? >/components/com_mica/images/export.png" />
						</a>
					</td>
					<td>
						<a href="javascript:void(0);" onclick="downloadMapPdf();" id="options" >&nbsp;Export Map</a>
					</td>
					<td>
						<a href="javascript:void(0);" id="fullscreen" >&nbsp;Full Screen</a>
					</td>
					<td>
						<a href="javascript:void(0);" id="fullscreenoff" style="display:none;">&nbsp;Full Screen off</a>
					</td>
				</tr>
			</tbody>
		</table>
	*/ ?>

	<script type="text/javascript">
		JQuery("#map").css("height",JQuery(window).height());
		JQuery("#map").css("width",JQuery(window).width());
		var globalcorrdinates = "";
		// var map            = new OpenLayers.Map("map");
		var oldzoom = 0;
		var geo     = [];
		var dystate = [];
		var geo     = [];
		//OpenLayers.ProxyHost = "http://192.168.5.159/cgi-bin/proxy.cgi?url=";
		// use a CQL parser for easy filter creation
		var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;
		OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
		var tomcaturl = "<?php echo $this->tomcaturl; ?>";

		OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
			defaultHandlerOptions: {
				'single'         : true,
				'double'         : false,
				'pixelTolerance' : 0,
				'stopSingle'     : true,
				'stopDouble'     : false
			},
			initialize: function(options) {
				this.handlerOptions = OpenLayers.Util.extend(
					{}, this.defaultHandlerOptions
				);
				OpenLayers.Control.prototype.initialize.apply(
					this, arguments
				);
				this.handler = new OpenLayers.Handler.Click(
					this, {
						'click' : this.getfeaturenfo,'dblclick': this.onDblclick
					}, this.handlerOptions
				);
			},
			onDblclick: function(e) {
				JQuery(".olPopup").css({"display":"none"});
				JQuery("#featurePopup_close").css({"display":"none"});
				//changeAttrOnZoom(map.zoom);
			},
			getfeaturenfo :function(e) {
				coordinates = e;
				var params = {
					REQUEST       : "GetFeatureInfo",
					projection    : "EPSG:4326",
					EXCEPTIONS    : "application/vnd.ogc.se_xml",
					BBOX          : map.getExtent().toBBOX(),
					SERVICE       : "WMS",
					INFO_FORMAT   : 'text/html',
					QUERY_LAYERS  : selectlayer(map.zoom),
					FEATURE_COUNT : 50,
					Layers        : selectlayer(map.zoom),
					WIDTH         : map.size.w,
					HEIGHT        : map.size.h,
					X             : parseInt(e.xy.x),
					Y             : parseInt(e.xy.y),
					CQL_FILTER    : selectfilter(),
					srs           : map.layers[0].params.SRS
				};


				// handle the wms 1.3 vs wms 1.1 madness
				if(map.layers[0].params.VERSION == "1.3.0") {
					params.version = "1.3.0";
					params.i = e.xy.x;
					params.j = e.xy.y;
				} else {
					params.version = "1.1.1";
					params.y = parseInt(e.xy.y);
					params.x = parseInt(e.xy.x);
				}
				OpenLayers.loadURL("<?php echo $this->tomcaturl; ?>", params, this, setHTML, setHTML);
				//console.log(e);
				var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
				//lonLat.transform(map.displayProjection,map.getProjectionObject());
				//map.setCenter(lonLat, map.zoom);
				map.panTo(lonLat);
				//map.addControl(ovControl);
				//OpenLayers.Event.stop(e);
			}
		});

		function setHTML(response){
			changeAttrOnZoom(map.zoom,response);
		};

		function selectfilter(){
			if(javazoom == 5 || javazoom == 6 ){
				return stateCqlFilter();
			}else if(javazoom == 7){
				return districtCqlFilter();
			}else  if(javazoom == 8){
				return urbanCqlFilter();
			}else{
				return cityCqlFilter();
			}
		}

		function selectlayer(cuurzoom){
			if(javazoom == 5 || javazoom == 6){
				return "rail_state";
			}else if(javazoom == 7){
				return "india_information";
			}else  if(javazoom == 8){
				return "jos_mica_urban_agglomeration";
			}else{
				return "my_table";
			}
		}

		var bounds  = new OpenLayers.Bounds(-180.0, -85.0511, 180.0, 85.0511);
		var options = {
			controls      : [],
			maxExtent     : bounds,
			projection    : "EPSG:4326",
			maxResolution : 'auto',
			zoom          :<?php echo $this->zoom;?>,
			units         : 'degrees'
		};
	   	map = new OpenLayers.Map('map', options);

		var land = new OpenLayers.Layer.WMS("State Boundaries",
			"<?php echo $this->tomcaturl; ?>",
			{
				Layer       : 'india:rail_state',
				transparent : true,
				format      : 'image/png',
				CQL_FILTER  : stateCqlFilter(),
				SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/rail_state.sld'; ?>"
			},
			{
				isBaseLayer: false
			}
		);
		var opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
				"<?php echo $this->tomcaturl; ?>",
				{
					Layers           : 'india:rail_state',
					transparent      : false,
					format           : 'image/png',
					styles           : "dummy_state",
					transitionEffect : 'resize'
				},
				{
					isBaseLayer: true
				}
			);
		var districts = new OpenLayers.Layer.WMS("districts",
			"<?php echo $this->tomcaturl; ?>",
			{
				Layer       : 'india:india_information',
				transparent : true,
				format      : 'image/png',
				CQL_FILTER  : districtCqlFilter(),
				SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/india_information.sld'; ?>"
			},
			{
				isBaseLayer: false
			},
			{
				transitionEffect: 'resize'
			}
		);

		var cities = new OpenLayers.Layer.WMS("Cities",
			"<?php echo $this->tomcaturl; ?>",
			{
				Layer       : 'india:my_table',
				transparent : true,
				format      : 'image/png',
				CQL_FILTER  : cityCqlFilter(),
				SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/my_table.sld'; ?>"
			},
			{
				isBaseLayer: false
			},
			{
				transitionEffect: 'resize'
			}
		);
		var urban = new OpenLayers.Layer.WMS("Urban",
			"<?php echo $this->tomcaturl; ?>",
			{
				Layer       : 'india:jos_mica_urban_agglomeration',
				transparent : true,
				format      : 'image/png',
				CQL_FILTER  : urbanCqlFilter(),
				SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/jos_mica_urban_agglomeration.sld'; ?>"
			},
			{
				isBaseLayer: false
			},
			{
				transitionEffect: 'resize'
			}
		);


		if(map.zoom == 5 || map.zoom == 6 ){
			map.addLayers([land]);
		}else if(map.zoom == 7){
			//map.removeLayer(land,districts);
			// map.addLayers([districts,land]);
		 	map.addLayers([districts]);
			//land.setVisibility(false);
		}else if(map.zoom == 8){
			//map.removeLayer(land,cities);
			// map.addLayers([cities,land]);
			map.addLayers([urban]);
			//land.setVisibility(false);
			// districts.setVisibility(false);
		}else{
			map.addLayers([cities]);
			//land.setVisibility(false);
		}

		map.div.oncontextmenu = function noContextMenu(e) {
			if (OpenLayers.Event.isRightClick(e)){
			   	displaymenu(e);
			}
	  		// return false; //cancel the right click of brower
	  	};

		//map.addControl(exportMapControl);
		map.addControl(new OpenLayers.Control.PanZoomBar({
			position: new OpenLayers.Pixel(5, 15)
		}));
		map.addControl(new OpenLayers.Control.Navigation({dragPanOptions: {enableKinetic: true}}));
		map.addControl(new OpenLayers.Control.Scale($('scale')));
		map.addControl(new OpenLayers.Control.Attribution());
		map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
		//map.addControl(new OpenLayers.Control.LayerSwitcher());
		//alert(bounds);

		var click = new OpenLayers.Control.Click();
		map.addControl(click);
		click.activate();

		map.zoomTo(<?php echo $this->zoom;?>);

		<?php  if($this->geometry != "0" && $this->geometry != "") { ?>
			var format    = new OpenLayers.Format.WKT();
			var feature   = format.read('<?php echo $this->geometry;?>');
			var homepoint = feature.geometry.getCentroid();
		<?php } ?>

		map.addLayers([opverviewland]);
		//	if (!map.getCenter(bounds))
		//    map.zoomToMaxExtent();
		map.zoomToExtent(bounds);
		//map.zoomToMaxExtent();

		function onPopupClose(evt) {
			// 'this' is the popup.
			JQuery("#featurePopup").remove();
		}

		function displaymenu(e){
			//$("#menupopup").remove();
			//var OuterDiv=$('<div  />');
			//OuterDiv.append($("#menu").html());
			//OuterDiv.append($("#menu").html());
			//console.log(e);
			//OuterDiv.css({"z-index":"100000","left":e.screenX,"top":e.screenY,"display":"block"});
			//console.log({e.layerX,e.layerY});

			// popup = new OpenLayers.Popup("menupopup",map.getLonLatFromPixel(new OpenLayers.Pixel({e.layerX,e.layerY})),new OpenLayers.Size(200,300),$("#menu").html(), false);
			//	map.popup = popup;
			//evt.feature = feature;
			//	map.addPopup(popup);
		}

		function onFeatureSelect(response) {
			response       = response.trim("");
			var getsegment = response.split(":");
			var id         = "";

			if(typeof(getsegment[1]) == "undefined"){
				//console.log(response);
				return false;
			}else{
				id = getsegment[getsegment.length-1];
			}

			JQuery(".olPopup").css({"display":"none"});
			console.log(getsegment);

			JQuery.ajax({
				url  : "index.php?option=com_mica&task=showresults.popupAttributes&id="+id+"&zoom="+javazoom,
				type : 'GET'
			})
			.done(function(data) {
				popup = new OpenLayers.Popup.FramedCloud("featurePopup",
					map.getLonLatFromPixel(new OpenLayers.Pixel(coordinates.xy.x,coordinates.xy.y)),
					new OpenLayers.Size(300,150),
					data,null ,true, onPopupClose
				);
				map.popup = popup;
				//evt.feature = feature;
				map.addPopup(popup);
			});
		}

		function onFeatureUnselect(evt) {
			feature = evt.feature;
			if (feature.popup) {
				popup.feature = null;
				map.removePopup(feature.popup);
				feature.popup.destroy();
				feature.popup = null;
			}
		}

		var javazoom = <?php echo $this->zoom; ?>

		<?php $curlonglat = $app->input->get('longlat', '', 'raw');

		if(($curlonglat =="")){
			echo "var curlonglat= '77.941406518221,21.676757633686';";
		}else{
			echo "var curlonglat= '".$app->input->get('longlat','77.941406518221,21.676757633686', 'raw')."';";
		} ?>

		//alert(curlonglat);
		var longlatsegment=curlonglat.split(",");

		<?php if($this->geometry!="0") {?>
   			map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
			map.zoomTo(6);
		<?php } else {?>
   			map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
			map.zoomTo(4);
		<?php } ?>

		//map.events.register("click", map, regionclick);

		function stateCqlFilter(){
			<?php if($this->state!="all") {
				//$stateArray=explode(",",$app->input->get('state'));
				$statetojavascript=str_replace(",","','",$this->state);
				echo " return \"name in ('".$statetojavascript."')\";";
			} ?>
		}

		function districtCqlFilter(){
			<?php if($this->district!="all" && $this->district!="") {
				//$stateArray=explode(",",$app->input->get('district'));
				$statetojavascript = $this->district;
				$statetojavascript = str_replace(",","','",$statetojavascript);
				echo " return \"".$this->districtsearchvariable." in ('".$statetojavascript."') \";";
			}else if($this->district=="all"){
				echo " return  \"state in ('".$statetojavascript."')\";";
			} ?>
		}

		function cityCqlFilter(){
			<?php if($this->town != "all" && $this->town != "" ){
				//$stateArray=explode(",",$app->input->get('town'));
				$statetojavascript=$this->town;
				$statetojavascript=str_replace(",","','",$statetojavascript);
				echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
			}

			if($this->town=="all" ){
				//$stateArray=explode(",",$app->input->get('town'));
				echo " return  \"state in ('".$statetojavascript."')\";";
			} ?>
		}

		function urbanCqlFilter(){
			<?php if($this->urban!="all" && $this->urban!="") {
				//$stateArray=explode(",",$app->input->get('state'));
				$statetojavascript=$this->urban;
				$statetojavascript=str_replace(",","','",$statetojavascript);
				echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
			}

			if($this->urban=="all" ){
				//	$stateArray=explode(",",$app->input->get('town'));
				echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
			} ?>
		}

		function changeAttrOnZoom(javazoom,response){
			/*
				JQuery.ajax({
				method:"GET",
				url:"index.php?option=com_mica&view=showresults&task=getAttribute&zoom="+parseInt(javazoom),
				success:function(data)
				{  	if(data==-1)
					{
						window.location ='index.php?option=com_user&view=login';
					}
					else if(data==-2)
					{
						var confirmaction=confirm("You haven't select Attributes or your membership has been expired n.Do you want to select/upgrade plan for process further?");
						if(confirmaction==1)
						{
							window.location ='index.php?option=com_acctexp&task=showSubscriptionPlans';
						}
						else
						{

						}
					}
					else
					{	//alert(data);
						if(oldzoom !=javazoom)
						{
						//$("#ajaxattribute").html(data);
						oldzoom=javazoom;
						}
						if(response)

						JQuery("#avail_attribute").html("");
						JQuery("#thematic_attribute").html("");
							var avail_attribute="";
							JQuery('input.statetotal_attributes1[type=checkbox]').each(function () {
							avail_attribute +="<option value='"+JQuery(this).val()+"'>"+JQuery(this).val()+"</option>";
							});
							JQuery("#avail_attribute").html(avail_attribute);
							JQuery("#thematic_attribute").html(avail_attribute);



					}
				}
				});
			*/
			onFeatureSelect(response.responseText);
		}

		function addCustomAttr(attrname,attributevale){
			JQuery.ajax({
				url  : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
				type : 'GET'
			})
			.done(function(data) {
				//JQuery("#closeextra").click();
				//	reloadCustomAttr();
				window.location='index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
			});
		}

		function reloadCustomAttr(){
			JQuery.ajax({
				url  : "index.php?option=com_mica&task=showresults.getCustomAttr",
				type : 'GET'
			})
			.done(function(data) {
				//alert(data);
				JQuery("#addcustomattribute").html(data);
				//reloadCustomAttr();
			});
		}

		JQuery(document).on('click', '.delcustom', function(event) {
			var myid    = JQuery(this).attr('id');
			var segment = myid.split("_");

			JQuery.ajax({
				url  : "index.php?option=com_mica&task=showresults.deleteCustomAttr&id="+segment[2],
				type : 'GET'
			})
			.done(function(data) {
				 JQuery("#"+myid).parent("tr").remove();
			});
		});
	</script>

</div>
