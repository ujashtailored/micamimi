<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Summeryfront view class for Mica.
 *
 * @since  1.6
 */
class MicaViewSummeryfront extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$app     = JFactory::getApplication();
		$session = JFactory::getSession();
		$reset   = $app->input->get('reset', 0, 'int');

		if($reset == 0){
			$session->set('oldattributes', $session->get('attributes'));
		}else{
			$session->set('oldattributes', "");
			$session->set('attributes', null);
			$session->set('state', null);
			$session->set('district', null);
			$session->set('urban', null);
			// $session->set('villages', null);
			$session->set('activeworkspace', null);
			$session->set('attrname', null);
			$session->set('customattribute', null);
			$session->set('m_type', null);
			$session->set('activetable', null);
			$session->set('activedata' ,null);
			$session->set('composite', null);
			$session->set('m_type_rating', null);
			$session->set('customformula', null);
			$session->set('Groupwisedata', null);
			$session->set('indlvlgrps', null);
			$session->set('summarystate', null);
			$session->set('summarydistrict', null);
			$session->set('summarySubDistrict', null);
			$session->set('summeryattributes', null);
		}

		// Need to clear session varialbles of village view in district view
		/*$session->set('summeryattributes', null);
		
		$session->set('summarydistrict', null);*/
		$session->set('villages_villages', null);
		$session->set('village_m_type_rating', null);
		$session->set('villagem_type', null);
		$session->set('village_composite', null);
		// End

		$app->input->set('composite', $session->get('composite'));
		$app->input->set('m_type_rating', $session->get('m_type_rating'));
		$app->input->set('attributes', $session->get('attributes'));
		$app->input->set('state', $session->get('state'));
		$app->input->set('m_type', $session->get('m_type'));
		$app->input->set('preselected', $session->get('district'));
		$app->input->set('indlvlgrps', $session->get('indlvlgrps'));

		$msg = $app->input->get('msg', '', 'raw');
		if($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}else if($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items         = $this->get('StateItems');
		$this->composite_attr      = $this->get('compositeAttr');
		$this->typesArray          = $this->get('AttributeType');
		$this->industrylevelgroups = $this->get('IndustryLevelGroups');


		////summary state
		$state = $app->input->get('summarystate', '', 'raw');
	
		$stats = array();
		foreach($state as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('summarystate',$state);
		}
		//echo "<pre/>";print_r($session->get('state'));exit;

		$sessionstate = $session->get('summarystate');

		
		$app->input->set('summarystate', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('summarystate', $sessionstate);

	////summary state
			////summary district
		$attributes = $app->input->get('summarydistrict', '', 'raw');
			if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('summarydistrict');
		if($sessionattr != ""){
			if($app->input->get('summarydistrict', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('summarydistrict',$addtosession );
		$app->input->set('summarydistrict',$addtosession);

		// summary start sub district

		$district = $app->input->get('summarySubDistrict', '', 'raw');
		
		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('summarySubDistrict', $district);
		}

		$sessiondistrict = $session->get('summarySubDistrict');
			// /print_r($sessiondistrict);
		if($sessiondistrict != ""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);	
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}
	
		$session->set('summarySubDistrict',$sessiondistrict);
		$app->input->set('summarySubDistrict',$sessiondistrict);
		
		////summary end sub district
		////summary attribute
		$attributes = $app->input->get('summeryattributes', '', 'raw');
			if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('summeryattributes');
		if($sessionattr != ""){
			if($app->input->get('summeryattributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('summeryattributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);
		//print_r($addtosession);

			////summary attribute

		return parent::display($tpl);
	}
}
