<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Summeryfront view class for Mica.
 *
 * @since  1.6
 */
class MicaViewSummeryfront extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$app     = JFactory::getApplication();
		$session = JFactory::getSession();
		$reset   = $app->input->get('reset', 0, 'int');

		if($reset == 0){
			$session->set('oldattributes', $session->get('attributes'));
		}else{
			$session->set('oldattributes', "");
			$session->set('attributes', null);
			$session->set('state', null);
			$session->set('district', null);
			$session->set('urban', null);
			// $session->set('villages', null);
			$session->set('activeworkspace', null);
			$session->set('attrname', null);
			$session->set('customattribute', null);
			$session->set('m_type', null);
			$session->set('activetable', null);
			$session->set('activedata' ,null);
			$session->set('composite', null);
			$session->set('m_type_rating', null);
			$session->set('customformula', null);
			$session->set('Groupwisedata', null);
			$session->set('indlvlgrps', null);
		}

		// Need to clear session varialbles of village view in district view
		$session->set('summeryattributes', null);
		$session->set('summarystate', null);
		$session->set('summarydistrict', null);
		$session->set('villages_villages', null);
		$session->set('village_m_type_rating', null);
		$session->set('villagem_type', null);
		$session->set('village_composite', null);
		// End

		$app->input->set('composite', $session->get('composite'));
		$app->input->set('m_type_rating', $session->get('m_type_rating'));
		$app->input->set('attributes', $session->get('attributes'));
		$app->input->set('state', $session->get('state'));
		$app->input->set('m_type', $session->get('m_type'));
		$app->input->set('preselected', $session->get('district'));
		$app->input->set('indlvlgrps', $session->get('indlvlgrps'));

		$msg = $app->input->get('msg', '', 'raw');
		if($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}else if($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items         = $this->get('StateItems');
		$this->composite_attr      = $this->get('compositeAttr');
		$this->typesArray          = $this->get('AttributeType');
		$this->industrylevelgroups = $this->get('IndustryLevelGroups');

		return parent::display($tpl);
	}
}
