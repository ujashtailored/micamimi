<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

/**
 * Villages Front controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerVillagefront extends MicaController
{
	// Custom Constructor
	var $tomcat_url             = "";

	var $townsearchvariable     = "OGR_FID";

	var $urbansearchvariable    = "OGR_FID";

	var $districtsearchvariable = "OGR_FID";
	var $subdistrictsearchvariable ="Sub_District_Code";

	/**
	 * [__construct description]
	 *
	 * @param   array  $default  [description]
	 */
	function __construct($default = array())
	{
		parent::__construct($default);

		$this->_table_prefix = '#__mica_';
		$this->tomcat_url    = "http://http://162.144.250.77:8080/geoserver/india/wms";
		DEFINE('TOMCAT_URL', $this->tomcat_url);
	}

	/**
	 * A task to return data for AJAX.
	 *
	 * @return  void
	 */
	public function getsecondlevel()
	{
		$db   = JFactory::getDBO();
		$districtsearchvariable = $this->districtsearchvariable;
		$stat                   = $this->input->get('stat', '', 'raw');

		// $preselected          = $this->input->get('preselected','');
		$stat        = explode(",", $stat);
		$session     = JFactory::getSession();
		$preselected = $session->get("villagedistrict");
		$preselected = explode(",", $preselected);
		foreach ($stat as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}

		$query = "SELECT state_code,LOWER(name) as name
				FROM rail_state";
		$db->setQuery($query);

		try {
			$state_codes = $db->loadAssocList();


		} catch (Exception $e) {

		}

		$stat = implode(",", $stats);
		$stat = str_replace(",", "','", $stat);
		$stat = "'" . $stat . "'";

		if ($stat)
		{
			// $query = "SELECT " . $districtsearchvariable . ", distshp, state
			// 	FROM " . $db->quoteName('india_information') . "
			// 	WHERE " . $db->quoteName('state') . " IN (" . $stat . ")
			// 		AND " . $db->quoteName('OGR_FID') . " != " . $db->quote(490) . "
			// 	GROUP BY " . $db->quoteName('distshp') . ", " . $db->quoteName('state') . "
			// 	ORDER BY " . $db->quoteName('state') . ", " . $db->quoteName('distshp') . " ASC ";
			 $query = "SELECT district as OGR_FID, district_name as distshp, state_name as state
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('state_name') . " IN (" . $stat . ")
				GROUP BY " . $db->quoteName('district_name') . ", " . $db->quoteName('state_name') . "
				ORDER BY " . $db->quoteName('state_name') . ", " . $db->quoteName('district_name') . " ASC ";



			$db->setQuery($query);

			try
			{
				$district_data = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$district_data = array();
			}
		}
		else
		{
			$district_data = array();
		}

		$state    = array();
		$gpbydist = array();

		foreach ($district_data as $eachdist)
		{
			$gpbydist[$eachdist->state][] = $eachdist;
		}

		$district_data = $gpbydist;
		$returndata    = '';
		$x             = 0;
		$groupstate    = '';

		if (count($district_data))
		{
			//$returndata .= '<select name="villagedistrict" id="villagedistrict" class="inputbox" onchange="getdistrictattr(this.value)"  >';
			//$returndata.='<ul class="list1 districtlist" onchange="getdistrictattr(this.value)">';

			$groupstate.='<div class="slider">';

			foreach ($district_data as $key => $val)
			{

				//$returndata .= '<optgroup label="' . $key . '">';
				
				 $state_id = array_search(strtolower($key), array_column($state_codes, 'name'));
				 
				$returndata.='<li class="district_group" id='.$state_codes[$state_id]['state_code'].'><label class="district_label">'.$key.'</label></li>';

				$groupstate.="<div><a href=#".$state_codes[$state_id]['state_code'].">".strtoupper($state_codes[$state_id]['state_code'])."</a></div>";

				$class=strtolower(str_replace(" ", "_", $key));
				$class=strtolower(str_replace("&", "_", $class));



				for ($i = 0; $i < count($val); $i++)
				{
					if (in_array($val[$i]->$districtsearchvariable, $preselected))
					{
						//$selected = "selected";
						$selected =  'checked="checked"';
					}
					else
					{
						$selected = "";
					}

					//$returndata .= '<option value="' . $val[$i]->$districtsearchvariable . '" ' . $selected . '>' . $val[$i]->distshp . '</option>';

					$returndata.='<li class="'.$class.'"><input type="radio" class="district_checkbox" name="villagedistrict" value="'.$val[$i]->$districtsearchvariable.'" '.$selected.' id="district_check_'.$x.'" ><label for="district_check_'.$x.'">'.$val[$i]->distshp.'</label></li>';
							$x++;
				}

				//$returndata .= '</optgroup>';
			}
			$groupstate.= '</div>';
			//$returndata .= '</select>';
		}
		else
		{
			//$returndata .= '<select name="villagedistrict" id="villagedistrict" class="inputbox" onchange="getdistrict(this.value)"></select>';
			$returndata.='<ul class="list1 districtlist"></ul>';
		}
		$returndata .='</ul>';

		/*$getUrbanAgglormationNames=$this->getUrbanAgglormationNamesByStateNames($stat);
		$getUATownNamesByStateNames=$this->getUATownNamesByStateNames($stat);*/
		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $returndata . "split" .$groupstate."split" . $getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
		exit;
	}

	/**
	 * A task to return Sub-district data for AJAX.
	 *
	 * @return  void
	 */
	public function getSubDistrict()
	{
		$subdistrictsearchvariable ="Sub_District_Code";
		$districtIds            = $this->input->getString('villagedistrict');
		$session                = JFactory::getSession();
		$preselectedVillages    = $session->get('villageSubDistrict');
	
		if (!is_array($preselectedVillages))
		{
			$preselectedVillages = explode(',', $preselectedVillages);
		}

		if ($districtIds == 'null')
		{
			$districtIds = $session->get('villagedistrict');
		}

		$db = JFactory::getDBO();

		if (!empty($districtIds))
		{
			$districtIds = explode(",", $districtIds);
			$districtIds = implode(",", $districtIds);
			$districtIds = str_replace(",", "','", $districtIds);
			$inCoumnName = "'" . $districtIds . "'";

			/*$query = "SELECT " . $subdistrictsearchvariable . ", Sub_District_Code, Sub_District_Name, district, district_name
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
				GROUP BY " . $db->quoteName('Sub_District_Code') . "
				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('Sub_District_Name') . " ASC ";*/
				$query = "SELECT " . $subdistrictsearchvariable . ", Sub_District_Code, Sub_District_Name, district, district_name
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
				GROUP BY " . $db->quoteName('Sub_District_Code') . "
				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('Sub_District_Name') . " ASC ";

				$db->setQuery($query);

			try
			{
				$villageData = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$villageData = array();
			}
		}
		else
		{
			$villageData = array();
		}

		$gpbydist = array();

		foreach ($villageData as $eachdist)
		{
			$gpbydist[$eachdist->district_name][] = $eachdist;
		}

		$villageData = $gpbydist;
		$returndata  = '';
		$x           = 0;

		if (count($villageData))
		{
			$groupdistrict.='<div class="slider">';
			foreach ($villageData as $key => $val)
			{
				
				$result = mb_substr(strtoupper($key), 0, 2);
				$returndata .= '<li class="district_group" id='.$result.'><label class="subdistrict_label">'.$key.'</label></li>';
				$groupdistrict.="<div><a href=#".$result.">".$result."</a></div>";
				$class=strtolower(str_replace(" ", "_", $key));
				$class=strtolower(str_replace("&", "_", $class));

				for ($i = 0; $i < count($val); $i++)
				{
					//echo "<pre>";print_r($val);exit;
					if (in_array($val[$i]->$subdistrictsearchvariable, $preselectedVillages))
					{
							$selected =  'checked="checked"';
					}
					else
					{
						$selected = "";
					}

					$returndata .= '<li class="'.$class.'">
							<input type="radio" class="subdistrict_checkbox" name="villageSubDistrict" value="'.$val[$i]->Sub_District_Code.'" '.$selected.' id="subdistrict_check_'.$x.'" >
							<label for="subdistrict_check_'.$x.'">'.$val[$i]->Sub_District_Name.'</label></li>';
							$x++;
				}
			}
			$groupdistrict.= '</div>';
		}
		else
		{
			$returndata .= '<ul class="list1 villagelist"></ul>';
		}
		$returndata .= '</ul>';

		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $returndata . "split". $groupdistrict."split".$getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
		exit;
	}

	/**
	 * A task to return village data for AJAX.
	 *
	 * @return  void
	 */
	public function getsecondlevelvillage()
	{
		$districtsearchvariable = $this->districtsearchvariable;
		//$subdistrictsearchvariable ="Village_Code";

		$subdistrictIds         = $this->input->getString('villageSubDistrict');
		$session                = JFactory::getSession();
		$preselectedVillages    = $session->get('villages_villages');


		if (!is_array($preselectedVillages))
		{
			$preselectedVillages = explode(',', $preselectedVillages);
		}

		if ($subdistrictIds == 'null')
		{
			$subdistrictIds = $session->get('villageSubDistrict');
		}

		$db = JFactory::getDBO();

		if (!empty($subdistrictIds))
		{
			$subdistrictIds = explode(",", $subdistrictIds);
			$subdistrictIds = implode(",", $subdistrictIds);
			$subdistrictIds = str_replace(",", "','", $subdistrictIds);
			$inCoumnName = "'" . $subdistrictIds . "'";

			$query = "SELECT " . $districtsearchvariable . ", place_name, Sub_District_Code, Sub_District_Name,district_name
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('Sub_District_Code') . " IN (" . $inCoumnName . ")
				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";

			$db->setQuery($query);

			try
			{
				$villageData = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$villageData = array();
			}
		}
		else
		{
			$villageData = array();
		}

		$gpbydist = array();

		foreach ($villageData as $eachdist)
		{
			$gpbydist[$eachdist->Sub_District_Name." - ".$eachdist->district_name][] = $eachdist;
		}

		$villageData = $gpbydist;
		$returndata  = '';
		$x           = 0;

		if (count($villageData))
		{
			foreach ($villageData as $key => $val)
			{
				$returndata .= '<li class="district_group"><label class="villages_label">'.$key.'</label></li>';
				$class=strtolower(str_replace(" ", "_", $key));
				$class=strtolower(str_replace("&", "_", $class));

				for ($i = 0; $i < count($val); $i++)
				{
					if (in_array($val[$i]->$districtsearchvariable, $preselectedVillages))
					{
						$selected = "checked";
					}
					else
					{
						$selected = "";
					}

					$returndata .= '<li class="'.$class.'">
							<input type="checkbox" class="village_checkbox" name="villages_villages[]" value="'.$val[$i]->$districtsearchvariable.'" '.$selected.' id="village_check_'.$x.'" >
							<label for="village_check_'.$x.'">'.$val[$i]->place_name.'</label></li>';
							$x++;
				}
			}
		}
		else
		{
			$returndata .= '<ul class="list1 villagelist"></ul>';
		}
		$returndata .= '</ul>';

		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $returndata . "split" . $getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
		exit;
	}

	/**
	 * [getUrbanAgglormationNamesByStateNames description]
	 *
	 * @param   [type]  $state  [description]
	 *
	 * @return  [type]          [description]
	 */
	function getUrbanAgglormationNamesByStateNames($state)
	{
		$urbansearchvariable = $this->urbansearchvariable;
		$db                  = JFactory::getDBO();

		if ($state)
		{
			$query = "SELECT " . $urbansearchvariable . ",place_name
				FROM " . $db->quoteName('jos_mica_urban_agglomeration') . "
				WHERE " . $db->quoteName('place_name') . " LIKE '%" . $state . "%'";
			$db->setQuery($query);
			$urban_data = $db->loadObjectList();
		}
		else
		{
			$urban_data = array();
		}

		$returndata = '';

		if (count($urban_data))
		{
			$returndata .= '<select name="urban" id="urban" class="inputbox" onchange="geturban(this.value)" >';
			$returndata .= '<option value="">' . JText::_('PLEASE_SELECT') . '</option><option value="all">' . JText::_('ALL') . '</option>';

			for ($i = 0; $i < count($urban_data); $i++)
			{
				$returndata .= '<option value="' . $urban_data[$i]->$urbansearchvariable . '">' . $urban_data[$i]->place_name . '</option>';
			}

			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="urban" id="urban" class="inputbox" ></select>';
		}

		return $returndata;
	}


	/**
	 * [getUATownNamesByStateNames description]
	 *
	 * @param   [type]  $state  [description]
	 *
	 * @return  [type]          [description]
	 */
	function getUATownNamesByStateNames($state)
	{
		$townsearchvariable = $this->townsearchvariable;
		$db = JFactory::getDBO();

		if ($state)
		{
			$query = "SELECT " . $this->townsearchvariable . ",place_name
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('state') . " LIKE '%" . $state . "%'";
				$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}
		else
		{
			$town_data = array();
		}

		$returndata = '';

		if (count($town_data))
		{
			$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="gettown(this.value)" >';
			$returndata .= '<option value="">' . JText::_('PLEASE_SELECT') . '</option><option value="all">' . JText::_('ALL') . '</option>';

			for ($i = 0; $i < count($town_data); $i++)
			{
				$returndata .= '<option value="' . $town_data[$i]->$townsearchvariable . '">' . $town_data[$i]->place_name . '</option>';
			}

			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="gettown(this.value)"></select>';
		}

		return $returndata;
	}

	/**
	 * [gettown description]
	 *
	 * @return  void
	 */
	function gettown()
	{
		$db       = JFactory::getDBO();
		$stat     = $this->input->get('stat', '', 'raw');
		$district = $this->input->get('villagedistrict', '', 'raw');

		if ($stat && $district)
		{
			$query = "SELECT id,TownName
				FROM " . $db->quoteName('#__mica_towns') . "
				WHERE " . $db->quoteName('StateName') . " LIKE '%" . $stat . "%'
					AND " . $db->quoteName('DistrictName') . " LIKE '%" . $district . "%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}
		else
		{
			$town_data = array();
		}

		$returndata = '';

		if (count($town_data))
		{
			$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="showhidetype(this.value)">';
			$returndata .= '<option value="all">' . JText::_('ALL') . '</option>';

			for ($i = 0; $i < count($town_data); $i++)
			{
				$returndata .= '<option value="' . $town_data[$i]->TownName . '">' . $town_data[$i]->TownName . '</option>';
			}

			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="showhidetype(this.value)">
							<option value="">' . JText::_('PLEASE_SELECT') . '</option>
							</select>';
		}

		echo $returndata;
		exit;
	}

	/**
	 * [getStateField description]
	 *
	 * @return  void
	 */
	public function getStateField()
	{
		$state = $this->input->get('villagestate', '', 'raw');
		$db = JFactory::getDBO();
		$query = "SELECT * FROM " . $db->quoteName('#__mica_state') . " WHERE " . $db->quoteName('StateName') . " LIKE '%" . $state . "%' ";
		$db->setQuery($query);
		$state_data = $db->loadObjectList();
		$returnable = array();

		foreach ($state_data as $key => $val)
		{
			foreach ($val as $key1 => $val1)
			{
				$returnable[$key1] = $val1;
			}
		}

		echo json_encode($state_data);
		exit;
	}

	/**
	 * [getDistrictField description]
	 *
	 * @return  void
	 */
	public function getDistrictField()
	{
		$stat = $this->input->get('villagestate', '', 'raw');
		$dist = $this->input->get('dist', '', 'raw');

		$db    = JFactory::getDBO();
		$query = "SELECT * FROM  " . $db->quoteName('#__mica_district_r_u') . "
			WHERE " . $db->quoteName('StateName') . " LIKE '%" . $stat . "%'
				AND " . $db->quoteName('DistrictName') . " LIKE '%" . $dist . "%' ";
		$db->setQuery($query);
		$district_data = $db->loadObjectList();
		$returnable    = array();

		foreach ($district_data as $key => $val)
		{
			foreach ($val as $key1 => $val1)
			{
				$returnable[$key1] = $val1;
			}
		}

		echo json_encode($district_data);
		exit;
	}
}
