<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );

/**
 * MICA Fullmap model.
 *
 * @since  1.6
 */
class MicaModelFullmap extends JModelLegacy
{

	var $_data                  = null;
	var $_total                 = null;
	var $_pagination            = null;
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $ruralprefix            = "Rural_";
	var $urnbanprefix           = "Urban_";
	var $totalprefix            = "Total_";

	function __construct(){
		parent::__construct();

		$app  = JFactory::getApplication();
		$user = JFactory::getUser();
		if($user->id == 0){
			$app->redirect("index.php");exit;
		}

		$this->_table_prefix = '#__mica_';

		/*$limit      = $mainframe->getUserStateFromRequest($context.'limit','limit',25,0);
		$limitstart = $mainframe->getUserStateFromRequest($context.'limitstart','limitstart',0);*/
		$limit      = $app->input->get('limit', 25, 0);
		$limitstart = $app->input->get('limitstart', 0);

		//$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$jlimitstart = $app->input->get('limitstart');
		if(!$jlimitstart){
			$limitstart = 0;
		}

		$this->setState('limit',$limit);
		$this->setState('limitstart',$limitstart);
	}

	/**
	 * A function called from view to get states.
	 */
	public function getStateItems(){
		$state_data = null;
		$db    = $this->getDBO();
		$query = "SELECT id, name FROM ".$db->quoteName('rail_state')." GROUP BY ".$db->quoteName('name');
		$db->setQuery($query);
		try {
			$state_data = $db->loadObjectList();
		} catch (Exception $e) {

		}
		return $state_data;
	}

	/**
	 * A function called from view to get states.
	 */
	public function getAttributeCheck($db_table, $attributes){
		$session =JFactory::getSession();
		$db      = $this->getDBO();

		$query="DESCRIBE ".$db_table;
		$db->setQuery($query);
		$fields = $db->loadObjectList();

		$m_type=$session->get('m_type');
		foreach($fields as $eachfield){
			$tablefield[] = $eachfield->Field;
		}

		//$attributes=explode(",",$attributes);
		$attrstr = array();
		foreach($attributes as $eachattr){
			$status = 0;
			if($m_type == "Total"){
				if(in_array($this->ruralprefix.$eachattr,$tablefield))
				{
					$attrstr[] = $this->ruralprefix.$eachattr;
					$status    = 1;
				}
				if(in_array($this->urnbanprefix.$eachattr,$tablefield))
				{
					$attrstr[] = $this->urnbanprefix.$eachattr;
					$status    = 1;
				}
				if(in_array($this->totalprefix.$eachattr,$tablefield))
				{
					$attrstr[] = $this->totalprefix.$eachattr;
					$status    = 1;
				}
			}else if($m_type == "Urban"){
				if(in_array($this->urnbanprefix.$eachattr,$tablefield)){
					$attrstr[] = $this->urnbanprefix.$eachattr;
					$status    = 1;
				}
			}else if($m_type == "Rural"){
				if(in_array($this->ruralprefix.$eachattr,$tablefield)){
					$attrstr[] = $this->ruralprefix.$eachattr;
					$status    = 1;
				}
			}

			if($status == 0){
				if(in_array($eachattr,$tablefield)){
					$attrstr[] = $eachattr;
				}
			}
		}

		//echo implode(",",$attrstr);exit;
		return implode(",",$attrstr);
	}

	/**
	 * A function called from view.
	 */
	public function getCustomData($infotype = null){
		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$db      = $this->getDBO();

		$states       = $session->get('state');
		$mystates     = explode(",",$states);
		$mystates     = array_filter($mystates);
		$statesinsert = implode("','",$mystates);

		$district     = $session->get('district');
		$town         = $session->get('town');
		$urban        = $session->get('urban');
		$attributes   = $session->get('attributes');
		$m_type       = $session->get('m_type');

		$attributes   = explode(",",$attributes);
		$mytmp        = array_filter($attributes);

		$db_table     = "";
		if($town == "" && $urban == ""){
			if($district!=""){
				$db_table = "india_information";
			}else{
				$db_table = "rail_state";
			}

			if($m_type == "Total" ){
				$attributes = $this->getAttributeCheck($db_table,$mytmp);
			}else if($m_type == "Rural"){
				$db_table   = "india_information";
				$attributes = $this->getAttributeCheck($db_table,$attributes);
			}else if($m_type == "Urban"){
				$db_table   = "india_information";
				$attributes = $this->getAttributeCheck($db_table,$attributes);
			}else{
				$attributes = implode(",",$mytmp);
			}
		}
		$session->set('themeticattribute',$attributes);

		if($urban != ""){
			$attributes = implode(",",$attributes);

			if($urban!="all,"  && $urban !="all"){

				$mystates = explode(",",$urban);
				$mystates = array_filter($mystates);
				$mystates = implode("','",$mystates);
				$where    = " WHERE ".$db->quoteName($this->urbansearchvariable)." IN ('".$mystates."')"."
					AND ".$db->quoteName('state')." IN ('".$statesinsert."')
					AND ".$db->quoteName('place_name')." <> 'null' ";

				if($session->get('urban_orgfid')!=""){
					$state_orgfid = $session->get('urban_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " AND ".$db->quoteName($this->urbansearchvariable)." IN ('".$mystates1."') ";
				}

			}else{

				$where = "WHERE 1=1 AND ".$db->quoteName('state')." IN ('".$statesinsert."')
					AND ".$db->quoteName('place_name')." <> 'null' ";

				if($session->get('urban_orgfid') != ""){
					$state_orgfid = $session->get('urban_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " AND ".$db->quoteName($this->urbansearchvariable)." IN ('".$mystates1."') ";
				}
			}

			$query = "SELECT ".$attributes." , place_name as name ,".$this->urbansearchvariable."
				FROM ".$db->quoteName('jos_mica_urban_agglomeration')."
				".$where;
			$db->setQuery($query);

			$session->set('activetable',"jos_mica_urban_agglomeration");
			$session->set('activenamevariable',"place_name");
			$session->set('activesearchvariable',$this->urbansearchvariable);
			$session->set('themeticattribute',$attributes);

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}

			if($infotype != ""){
				$session->set('popupactivedata', $this->_data);
			}else{
				$session->set('activedata', $this->_data);
			}
			return $this->_data;
		}

		if($town != ""){

			$attributes=implode(",",$attributes);

			if($town != "all,"  && $town != "all"){
				$mystates = explode(",",$town);
				$mystates = array_filter($mystates);
				$mystates = implode("','",$mystates);
				$where    = "WHERE ".$db->quoteName($this->townsearchvariable)." IN ('".$mystates."')
					AND ".$db->quoteName('state')." IN ('".$statesinsert."')
					AND ".$db->quoteName('place_name')." <> 'null' ";

				if($session->get('town_orgfid') != ""){
					$state_orgfid = $session->get('town_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " AND ".$db->quoteName($this->townsearchvariable)." IN ('".$mystates1."') ";
				}

			}else{

				$where = " WHERE 1=1  AND ".$db->quoteName('state')." IN ('".$statesinsert."')
					AND ".$db->quoteName('place_name')." <> 'null' ";

				if($session->get('town_orgfid') != ""){
					$state_orgfid = $session->get('town_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " AND ".$db->quoteName($this->townsearchvariable)." IN ('".$mystates1."') ";
				}
			}

			$query = "SELECT ".$attributes.",place_name as name ,".$this->townsearchvariable."
				FROM ".$db->quoteName('my_table')."
				".$where."
				AND ".$db->quoteName('state')." IN ('".$statesinsert."') ";
			$db->setQuery($query);

			$session->set('activetable',"my_table");
			$session->set('activenamevariable',"place_name");
			$session->set('activesearchvariable',$this->townsearchvariable);
			$session->set('themeticattribute',$attributes);

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}

			if($infotype != ""){
				$session->set('popupactivedata', $this->_data);
			}else{
				$session->set('activedata', $this->_data);
			}
			return $this->_data;
		}

		if(trim($district) == null || trim($district) == ","){

			if($states != "all," && $states != ""  && $states != "all"){

				$mystates = explode(",",$states);
				$mystates = array_filter($mystates);
				$mystates = implode("','",$mystates);
				$where    = "WHERE ".$db->quoteName('name')." IN ('".$mystates."')";

				if($session->get('state_orgfid') != ""){
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where     .= " AND ".$db->quoteName($this->statesearchvariable)." IN ('".$mystates1."') ";
				}

			}else{
				$mystates = "1=1";
				$where    = "";

				if($session->get('state_orgfid') != ""){
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " WHERE ".$db->quoteName($this->statesearchvariable)." IN ('".$mystates1."') ";
				}
			}

			$query = "SELECT ".$attributes." , ".$this->statesearchvariable." ,name
				FROM ".$db->quoteName('rail_state')."
				".$where."
				GROUP BY ".$db->quoteName('name')."
				ORDER BY ".$db->quoteName('name');
			$db->setQuery($query);

			$session->set('activetable', "rail_state");
			$session->set('activenamevariable', "name");
			$session->set('activesearchvariable', $this->statesearchvariable);

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}

			if($infotype != ""){
				$session->set('popupactivedata', $this->_data);
			}else{
				$session->set('activedata', $this->_data);
			}
			return $this->_data;

		}else{

			if($district!="all," && $district !="all"){
				$states   = explode(",",$states);
				$states   = array_filter($states);
				$states   = implode("','",$states);
				$district = explode(",",$district);
				$district = array_filter($district);
				$district = implode("','",$district);
				$where    = "WHERE ".$db->quoteName($this->districtsearchvariable)." IN ('".$district."')
					AND ".$db->quoteName('state')." IN ('".$states."') ";

				if($session->get('district_orgfid') != ""){
					$state_orgfid = $session->get('district_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " AND ".$db->quoteName($this->districtsearchvariable)." IN ('".$mystates1."') ";
				}

			}else{

				$states = explode(",",$states);
				$states = array_filter($states);
				$states = implode("','",$states);
				$where  = "WHERE 1=1 AND ".$db->quoteName('state')." IN ( '".$states."') ";

				if($session->get('district_orgfid') != ""){
					$state_orgfid = $session->get('district_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1 = explode(",",$state_orgfid);
					$mystates1 = array_filter($mystates1);
					$mystates1 = implode("','",$mystates1);
					$where    .= " AND ".$db->quoteName($this->districtsearchvariable)." IN ('".$mystates1."') ";
				}

			}

			$query = "SELECT ".$attributes." ,".$this->districtsearchvariable.", distshp as name
				FROM ".$db->quoteName('india_information')."
				".$where."
				AND ".$db->quoteName('distshp')." <> ''
				GROUP BY ".$db->quoteName('distshp')."
				ORDER BY ".$db->quoteName('distshp');
			$db->setQuery($query);

			$session->set('activetable', "india_information");
			$session->set('activenamevariable', "distshp");
			$session->set('activesearchvariable', $this->districtsearchvariable);

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}

			if($infotype != ""){
				$session->set('popupactivedata', $this->_data);
			}else{
				$session->set('activedata', $this->_data);
			}
			return $this->_data;
		}
	}


	/**
	 * function called from controller.
	 */
	public function getMaxForMatrix(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$table    = $session->get('activetable');
		$variable = $session->get('themeticattribute');
		$variable = explode(",",$variable);

		$maxValsSelect = "";
		foreach($variable as $eachvariable){
			$maxValsSelect[] = " MAX(" . $eachvariable . ") AS '" . $eachvariable . "'";
		}
		$maxValsSelect = implode(" , ", $maxValsSelect);

		//Complete the query
		$maxValsQuery = "SELECT $maxValsSelect FROM $table";
		$db->setQuery($maxValsQuery);

		try {
			$res = $db->loadAssocList();
		} catch (Exception $e) {
			$res = NULL;
		}
		return $res;
	}

	/**
	 *
	 */
	public function getGraph(){
		$session = JFactory::getSession();

		$states     = $session->get('state');
		$district   = $session->get('district');
		$town       = $session->get('town');
		$urban      = $session->get('urban');

		$attributes = $session->get('attributes');
		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);

		$attr = array();
		$i    = 0;
		foreach($this->_data as $key=>$val){
			foreach($val as $ek=>$ev){
				if($ev==""){
					$ev="0";
				}
				foreach($attributes as $eachattr){
					if($ek == $eachattr){
						$attr[$ek][]=$ev;
					}

					if($ek == $this->urnbanprefix.$eachattr){
						$attr[$this->urnbanprefix.$eachattr][] = $ev;
					}

					if($ek == $this->totalprefix.$eachattr){
						$attr[$this->totalprefix.$eachattr][] = $ev;
					}

					if($ek == $this->ruralprefix.$eachattr){
						$attr[$this->ruralprefix.$eachattr][] = $ev;
					}

					if($ek == "name"){
						$attrs['name'][$i] = $ev;
					}
				}
			}
			$i++;
		}

		$customattribute = $session->get('customattribute');
		$eachcustomattr  = array();
		$customattribute = explode(",",$customattribute);
		$customattribute = array_filter($customattribute);
		$z = 0;

		foreach($attrs['name'] as $eachname){
			foreach($customattribute as $eachcustom){
				$eachcustom = explode(":",$eachcustom);
				$eachcustomattr[$eachcustom[0]][$z] = $this->getCustomAttributeValue($eachcustom[1],$this->_data,$eachname);
			}
			$z++;
		}

		$attr          = array_merge($attr,$eachcustomattr);
		$arraytoreturn = "";
		$lastkey       = count(($attr));
		$z             = 1;
		foreach($attr as $key => $val){
			if($z !== $lastkey){
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val)."sln";
			}else{
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val);
			}
			$z++;
		}
		return $arraytoreturn;
	}

	/**
	 *
	 */
	public function getGraphSettings(){
		$session = JFactory::getSession();

		$states        = $session->get('state');
		$district      = $session->get('district');
		$town          = $session->get('town');
		$urban         = $session->get('urban');
		//$attributes  = $session->get('attributes');
		$data_settings = "";

		if($town != ""){

			$i=0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
				$data_settings .= "</graph>";
				$i++;
			}

		}else if($urban != ""){
			$i=0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
				$data_settings .= "</graph>";
				$i++;
			}

		}else if(trim($district)==","  || strlen($district)==0){
			$i=0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
				$data_settings .= "</graph>";
				$i++;
			}
		}else{
			$i = 0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
				$data_settings .= "</graph>";
				$i++;
			}
		}
		/*
			$session = JFactory::getSession();
			$attrnamesession=$session->get('customattribute');
			if(strlen($attrnamesession)>0)
			{
				$attrnamesession=explode(",",$attrnamesession);
				$attrnamesession=array_filter($attrnamesession);
				foreach($attrnamesession as $eachattr)
				{
					$name=explode(":",$eachattr);
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title><![CDATA[".$name[0]."]]></title>";
					$data_settings .= "</graph>";
					$i++;

				}

			}
		*/
		return $data_settings;
	}

	public function getDisplayGroup(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$urban           = $session->get('urban');
		$town            = $session->get('town');
		$district        = $session->get('district');
		$state           = $session->get('state');
		$attributes      = $session->get('attributes');
		$attributes      = explode(",",$attributes);
		$customattribute = $session->get('customattribute');

		$customattribute = explode(",",$customattribute);
		foreach($customattribute as $eachcustom){
			$eachcustom = explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];
		}
		$eachcustomattr = array_filter($eachcustomattr);

		if($district != ""){
			$db_table = "district";
		}else if($town != ""){
			$db_table = "town";
		}else if($urban!=""){
			$db_table = "urban";
		}else{
			$db_table = "state";
		}

		$query = "SELECT  * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
			INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
			WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
				AND ".$db->quoteName('grp.publish')." = ".$db->quote(1);
		$db->setQuery($query);
		$list = $db->loadAssocList();

		$change_group_name = 1;
		/*if(!empty($eachcustomattr[$mykeys[$i]]) && isset($eachcustomattr[$mykeys[$i]])){
			$ev=$this->getCustomAttributeValue($eachkey->$mykeys[$i],$this->_data);
			$data .="<td>".JTEXT::_($ev)."</td>";
		}*/

		foreach($list as $each){
			foreach($this->_data as $togrp){
				foreach($togrp as $key=>$val){
					$statename = $togrp->name;
					//echo var_dump(stristr($each['field'],$key,1));

					if($each['field']==$key || $this->urnbanprefix.$each['field']==$key || $this->totalprefix.$each['field']==$key || $this->ruralprefix.$each['field']==$key){
						{
							$i=0;
							foreach($attributes as $eachsessionattr){
								if(stristr($each['field'],$eachsessionattr)){
									$segment = explode($eachsessionattr,$key);
									if($segment[1] == ""){
										$todisplay[$statename][$each['group']][JTEXT::_($eachsessionattr)][]=array(str_replace("_","",$segment[0])=>$val);
										$icon[$each['group']]=$each['icon'];
									}
								}

								if(count($eachcustomattr) != 0){
									foreach($eachcustomattr as $keys => $vals){
										//if(stristr($vals,$eachsessionattr))
										{
											$ev = $this->getCustomAttributeValue($vals,$this->_data,$statename);
											$todisplay[$statename]['Custom Variable'][$keys][$vals] = array($keys => $ev);
										}
									}
								}
								//ksort($todisplay[$statename][$each['group']][$eachsessionattr],SORT_STRING);
							}
						}
					}
				}
				//break;
			}
		}
		//echo "<pre>";print_r($todisplay);exit;
		//echo "<pre>";print_r($todisplay);exit;
		$session->set('Groupwisedata',$todisplay);
		return array($todisplay,$icon);
	}

	function countkeys($data){
		$i=0;
		foreach($data as $eachdata){
			$i=$i+count($eachdata);
		}
		return $i;
	}

	/**
	 * A function called from view.
	 */
	public function getNewAttributeTable(){
		$session       = JFactory::getSession();

		$gpdata        = $this->getDisplayGroup();
		$groupwisedata = $gpdata[0];
		$icon          = $gpdata[1];

		$activetable = $session->get("activetable");
		if($activetable == "india_information"){
			$datatablename = "District Name";
		}

		if($activetable == "rail_state"){
			$datatablename = "State Name";
		}

		if($activetable =="jos_mica_urban_agglomeration"){
			$datatablename = "UA Name";
		}

		if($activetable == "my_table"){
			$datatablename = "City Name";
		}

		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		foreach($customattribute as $eachcustom){
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];

		}

		$i = 1;
		$j = 0;
		$k = 0;
		$l = 0;
		$headerspan = 0;
		//$groupwisedata=array_reverse($groupwisedata);
		foreach($groupwisedata as $mainname=>$data){
			$header[$i]     = "<td >".$mainname."</td>";
			$grpspan        = 0;
			$rearrangeddata = array();
			$tmp            = "";

			foreach($data as $grpname => $grpdata){
				if($grpname=="Mkt Potential Index"){
					$tmp = $grpdata;
				}else{
					$rearrangeddata[$grpname]=$grpdata;
				}
				$rea++;
			}

			$Market = array("Mkt Potential Index"=>$tmp);
			$data   = array_merge($Market,$rearrangeddata);
			foreach($data as $grpname => $grpdata){
				//$headerspan +=$this->getColspan($grpdata,2);
				foreach($grpdata as $variablegrp => $variabledata){
					$grpspan            = 0;
					$vargrpname[$i][$k] = "<td colspan='".count($variabledata)."'><span class=''>".$variablegrp."<a href='index.php?option=com_mica&task=showresults.deleteattribute&attr=".$variablegrp."' class='right_arrow'>X</a></span></td>";
					foreach($variabledata as $eachvariabledata){
						foreach($eachvariabledata as $key => $value){
							$mygrpvariablename[$i][$k][$l] = "<td>".$key."</td>";
							$mygrpdata[$i][$k][$l]         = "<td>".$value."</td>";
							$grpspan++;
							$headerspan++;
						}

						$img="";
						if($icon[$grpname]!=""){
							$img="<img src='".JUri::base()."components/com_mica/images/".$icon[$grpname]."'/>";
						}

						$grpheader[$i][$j] = "<td colspan='".$this->countkeys($grpdata)."'><div class='leftgrpimg'>".$img."</div><div class='rightgrptext'>".$grpname."</div></td>";
						$l++;
					}
					$k++;
				}
				$j++;
			}
			$i++;
		}
		//$mygrpvariablename=array_map('array_filter', $mygrpvariablename);

		$str = "<tr class='firsttableheader'><td width='200'>".$datatablename."</td>";//<tr><td colspan='".(($headerspan)+1)."'>Data</td></tr>
		foreach($grpheader as $inserheader){
			foreach($inserheader as $inserheaderval){
				$str .=$inserheaderval;
			}
			break;
		}
		$str .="</tr>";
		$str .="<tr class='secondtableheader'><td></td>";
		foreach($vargrpname as $eachgrpname){
			foreach($eachgrpname as $name){
				$str .=$name;
			}
			break;
		}
		$str .="</tr>";

		$a = array_pop(array_unique(array_pop(array_values(array_unique($mygrpvariablename)))));
		$a = array_filter(array_values($a));

		if(strlen(str_replace("<td></td>","",$a[0]))==0){
			$skip = 1;
		}else{
			$skip = 0;
		}

		if($skip == 0){
			$str .="<tr class='thirdtableheader'><td ></td>";
			foreach($mygrpvariablename as $variablenamegrp){
				foreach($variablenamegrp as $varname){
					foreach($varname as $each){
						$str .=$each;
					}
				}
				break;
			}
			$str .="</tr>";
		}

		foreach($mygrpdata as $key=>$gdata){
			$str .="<tr>".$header[$key];
			foreach($gdata as $data){
				foreach($data as $d){
					$str .=$d;
				}
			}
			$str .="</tr>";
		}
		return $str;
	}

	/**
	 *
	 */
	public function hasValues($input, $deepCheck = true) {
		foreach($input as $value) {
			if(is_array($value) && $deepCheck) {
				if($this->hasValues($value, $deepCheck))
					return true;
			}
			elseif(!empty($value) && !is_array($value))
				return true;
		}
		return false;
	}

	/**
	 *
	 */
	public function getColspan($groupwisedata,$onlevel){
		$keys = array_keys($groupwisedata);
		$vals = array_values($groupwisedata);

		if($onlevel==3){
			return count($keys);
		}else if($onlevel==2){
			return count($keys)*count($vals[0]);
		}else if($onlevel==1){
			return  $this->getColspan($groupwisedata,2)+$this->getColspan($groupwisedata,3);
		}
	}

	function getAttributeTable(){
		$app     = JFactory::getApplication();
		$session = JFactory::getSession();

		$states   = $app->input->get('state','', 'raw');
		$district = $app->input->get('district','', 'raw');
		$m_type   = $app->input->get('m_type','', 'raw');

		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		foreach($customattribute as $eachcustom){
			$eachcustom = explode(":",$eachcustom);
		}

		$compareflag = 0;
		if(count($this->_data) ==0){
			return "No Data Available";
		}else if(count($this->_data) >1){
			$compareflag = 1;
		}

		$heads=array();
		if(1 != 1){
			if(trim($district)==""){
				$i=0;
				foreach($this->_data as $key=>$val){
					foreach($val as $ek=>$ev){
						if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable){
							$heads[$ek][]=$ev;
						}else{
							$deleteindex[]=$ev;
						}
					}
				}
			}else{
				$i=0;
				foreach($this->_data as $key=>$val){
					foreach($val as $ek=>$ev){
						if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable){
							$heads[$ek][]=$ev;
						}else{
							$deleteindex[]=$ev;
						}
					}
				}
			}

			$heads          = array_reverse($heads);
			$heads          = array_filter($heads);
			$heads          = array_merge($heads,$eachcustomattr);
			$deleteindex    = array_reverse($deleteindex);
			$mykeys         = array_keys($heads);
			$eachcustomattr = array_filter($eachcustomattr);


			$head = "";
			$i    = 0;
			$data = "";
			foreach($mykeys as $eachkey){
				if($eachkey=="name"){
					$head .="<th>State Name</th>";
				}else if($eachkey=="distshp"){
					$head .="<th>District Name</th>";
				}else if($eachkey=="place_name"){
					$head .="<th>Town Name</th>";
				}else if($eachkey=="UA_name"){
					$head .="<th>Urban Name</th>";
				}else{
					$head .="<th>".$eachkey."</th>";
				}
			}

			$head ="<tr>".$head."</tr>";

			foreach($this->_data as $eachkey){
				$data .="<tr>";
				for($i=0;$i<count($mykeys);$i++){
					if(!empty($eachcustomattr[$mykeys[$i]]) && isset($eachcustomattr[$mykeys[$i]])){
						$ev   =$this->getCustomAttributeValue($eachkey->$mykeys[$i],$this->_data);
						$data .="<td>".JTEXT::_($ev)."</td>";
					}else if(is_object($eachkey)){
						$data .="<td>".JTEXT::_($mykeys[$i])."</td>";
					}
				}
				$data .="</tr>";
			}
		}else{
			foreach($this->_data as $key=>$val){
				foreach($val as $ek=>$ev){
					if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable){
						$heads[$ek][]=$ev;
					}else{
						$deleteindex[]=$ev;
					}
				}
			}

			$heads         = array_reverse($heads);
			//$deleteindex = array_reverse($deleteindex);
			$mykeys        = array_keys($heads);
			$data          = "";
			$j             = 0;
			$heads         = array_merge($heads,$eachcustomattr);
			$heads         = array_filter($heads);

			foreach($heads as $ek=>$ev){
				$originalname[] =$ek;
				if($ek=="name"){
					$ek="State Name";
				}else if($ek=="distshp"){
					$ek="District Name";
				}else if($ek=="place_name"){
					$ek="Town Name";
				}else if($ek=="UA_Name"){
					$ek="Urban Name";
				}else{
					$ek =$ek;
				}

				if(!empty($eachcustomattr[$ek])){
					$ev       = $this->getCustomAttributeValue($eachcustomattr[$ek],$this->_data);
					$rowclass = "deletecustom";
				}else{
					$rowclass = "deleterow";
				}

				if($j!=0){
					$todisplay = "<td ><input type='checkbox' class='".$rowclass."' value='".$originalname[$j]."' /></td>";
				}else{
					$todisplay = "<td >Delete</td>";
				}
				$data .="<tr id='".$originalname[$j]."'>".$todisplay."<td>".$ek."</td>";

				$i=0;

				foreach($ev as $k=>$v){
					if($i==0){
						if($j == 0){
							$myid          ="id = '".$v."'";
							$myclass       ="class='".$ek." '";
							$deletedcolumn ="<input type='checkbox' id='removecolumn' class ='removecolumn' value='".$originalname[$j]."`".$deleteindex[$k]."`".$v."' />";
						}else{
							$myid          ="id = ''";
							$myclass       ="";
							$deletedcolumn ="";
						}
					}else{
						if($j==0){
							$myid = "id = '".$v."'";
						}else{
							$myid = "id = ''";
						}
						$myclass="class = ''";
					}
					$data .="<td ".$myclass." ".$id.">".$deletedcolumn.JTEXT::_($v)."</td>";
				}
				$data .="</tr>";
				$j++;
			}
		}
		return $head.$data;
	}

	/**
	 *
	 */
	function getCustomAttributeValue($name,$data, $identifier = null){
		$session = JFactory::getSession();
		$db      = $this->getDbo();

		$returnable = array();
		$i          = 0;

		$name1  = str_replace("+","",$name);
		$name1  = str_replace("*","",$name1);
		$name1  = str_replace("-","",$name1);
		$name1  = str_replace("/","",$name1);
		$name1  = str_replace("SIN(","",$name1);
		$name1  = str_replace("COS(","",$name1);
		$name1  = str_replace("TAN(","",$name1);
		$name1  = str_replace("LOG(","",$name1);
		$name1  = str_replace("LOG10(","",$name1);
		$name1  = str_replace("SQRT(","",$name1);
		$name1  = str_replace("EXP(","",$name1);
		$name1  = str_replace("^","",$name1);
		$name1  = str_replace("EXP(","",$name1);
		$name1  = str_replace("ABS(","",$name1);
		$name1  = str_replace("PI(","",$name1);
		$name1  = str_replace(")","",$name1);
		$chkval = explode(" ",$name1);
		$chkval = array_filter($chkval);
		$field  = implode(",",$chkval);

		$table          = $session->get("activetable");
		$searchvariable = $session->get("activenamevariable");

		//foreach($data as $ed)
		{
			$dupname = $name;

			$query = "SELECT ".$field." FROM ".$table." WHERE ".$searchvariable." LIKE '".$identifier."'";
			$db->setQuery($query);
			try {
				$list  = $db->loadObjectList();
			} catch (Exception $e) {
				$list  = NULL;
			}
			$list = array_pop($list);

			//if($ed->name==$identifier)
			{
				foreach($list as $k=>$v){
					$dupname        =str_replace($k,$v,$dupname);
					$returnable[$i] =$dupname;
				}
			}
			$i++;
		}
		$i=0;

		foreach($returnable as $reeval){
			$result="";
			@eval('$result = '.$reeval.';');
			$returnables[$i]=  $result;
			$i++;
		}
		//echo $returnables[0];
		return $returnables[0];
	}

	function getWorkspace(){
		return workspaceHelper::loadWorkspace();
	}

	function updateWorkspace(){
		return workspaceHelper::updateWorkspace();
	}

	function deleteWorkspace(){
		return workspaceHelper::deleteWorkspace();
	}

	function saveWorkspace(){
		return workspaceHelper::saveWorkspace();
	}

	function unsetDefault(){
		return workspaceHelper::unsetDefault();
	}

	function getSldToDataBase(){
		$session         = JFactory::getSession();
		$activeworkspace = $session->get('activeworkspace');
		$sld             = cssHelper::getLayerProperty($activeworkspace,1);
		echo $sld;exit;
	}

	function getthematicQueryFromDb(){
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();

		$activeworkspace=$session->get('activeworkspace');
		$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')." WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace);
		$db->setQuery($query);
		$result = $db->loadObjectList();

		/*$customformulafromsession=$session->get('customformula',$attributes);
		$customformula=$formula.":".$from."->".$to."->".$color."|";
		$customformulafromsession = $customformulafromsession.$customformula;
		$session->set('customformula',$customformulafromsession);*/
	}

	function getSldLevel(){
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();

		$activeworkspace = $session->get('activeworkspace');
		$query="SELECT level FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
			GROUP BY ".$db->quoteName('level');
		$db->setQuery($query);
		$result=$db->loadObjectList();

		$str   = "";
		$level = "0,1,2";
		foreach($result as $eachlevel){
			$level =str_replace($eachlevel->level.",","",$level);
		}
		return $level;
	}

	/**
	 * A function called from view.
	 */
	public function getBreadCrumb(){
		$session = JFactory::getSession();

		$urban      = $session->get('urban');
		$town       = $session->get('town');
		$m_type     = $session->get('m_type');
		$district   = $session->get('district');
		$state      = $session->get('state');
		$dataof     = $session->get('dataof');

		$statecount = explode(",",$state);
		$separator  = "<span class='ja-pathway-text'>&nbsp; &gt; &nbsp;</span>";//JHTML::_('image.site', 'arrow.png');
		$breadcrumb ="<div class='breadcrumb ' id='ja-pathway'><span class='ja-pathway-text'>Your Query :</span>";

		if(count($statecount)==1){
			$breadcrumb .= "<span class='ja-pathway-text'>State(".$state.")</span>".$separator;

			if($dataof == "District"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID==$district){
						$selected = $eachdata->OGR_FID."&m_type=".$m_type;
						$name     = $eachdata->name;

						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}
						break;

					}else{

						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}

						if($district == "all"){
							$name     = "All";
							$selected = "all&m_type=".$m_type;
						}else{
							$name     = "Compare";
							$selected = "all&m_type=".$m_type;

						}
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>District(".$name.")</span>".$separator;
				$breadcrumb .="<span class='ja-pathway-text'>Type(".ucfirst($m_typedisp).")</span>";
			}

			if($dataof == "Town"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == $town){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($town=="all"){
							$name="All";
						}else{
							$name="Compare";
						}
						$selected="all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Town(".$name.")</span>";
			}

			if($dataof=="UA" || $dataof=="Urban"){
				$dataof="UA";
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == trim($urban)){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($urban=="all"){
							$name="All";
						}else{
							$name="Compare";
						}
						$selected="all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Urban(".$name.")</span>";
			}

		}else{

			$breadcrumb .="<span class='ja-pathway-text'>State</span>".$separator;
			if($dataof == "District"){
				$breadcrumb .="<span class='ja-pathway-text'>District</span>".$separator;
				$breadcrumb .="<span class='ja-pathway-text'>".ucfirst($m_type)."</span>";
			}

			if($dataof == "Town"){
				$breadcrumb .="<span class='ja-pathway-text'>Town</span>";
			}

			if($dataof == "UA"){
				$breadcrumb .="<span class='ja-pathway-text'>Urban</span>";
			}
			$selected = "";
		}

		$breadcrumb .= "<a class='ja-pathway-text' href='index.php?option=com_mica&view=micafront&state=".urlencode($state)."&dataof=".$dataof."&selected=".$selected."'>Modify</a>";
		$breadcrumb .= "</div>";
		return $breadcrumb;
	}

	/**
	 * A function called from view.
	 */
	public function getcustomattrlib(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$activetable = $session->get("activetable");

		$query = "SELECT name, attribute
			FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get("activeworkspace"))."
				AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
		$db->setQuery($query);
		$allcostomattr = $db->loadAssocList();

		$str = "";
		foreach($allcostomattr as $eachcustomattr){
			$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
			$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
		}
		$session->set('customattributelib',$str);
	}


	/**
	 * A function called from view.
	 */
	public function getInitialGeometry(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$state = $session->get('state');
		$state = explode(",",$state);
		$state = array_filter($state);
		$state = implode("','",$state);

		if($state!="all" && substr_count($state,",")==0){
			/*$where = "1=1";
			$query   = "select AsText(GROUP_CONCAT(india_state)) as poly from rail_state where ".$where;*/
			$query   = "SELECT AsText((india_state)) as poly
				FROM ".$db->quoteName('rail_state')."
				WHERE ".$db->quoteName('name')." in('".$state."') ";
		}else{
			return 0;
		}

		$db->setQuery($query);
		$allvertex=$db->loadAssocList();

		$str       = "";
		$allvertex = array_reverse($allvertex);
		foreach($allvertex as $eachvertex){
			//$str[] =$eachvertex['AsText(india_state)'];
			//echo $eachvertex['poly']; exit;
			return $eachvertex['poly'];
		}
		return implode(",",$str);
	}

}
