<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Villagefront model.
 *
 * @since  1.6
 */
class MicaModelVillagefront extends JModelLegacy
{
	var $_data       = null;

	var $_total      = null;

	var $_pagination = null;

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * [getStateItems description]
	 *
	 * @return  [type]  [description]
	 */
	public function getStateItems()
	{
		$db    = $this->getDBO();
		$query = "SELECT state as id,state as name
			FROM " . $db->quoteName('india_information') . "
			WHERE " . $db->quoteName('state') . " IS NOT NULL
				AND " . $db->quoteName('state') . " != " . $db->quote("Null") . "
			GROUP BY " . $db->quoteName('state');
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	/**
	 * [getcompositeAttr description]
	 *
	 * @return  [type]  [description]
	 */
	public function getcompositeAttr()
	{
		$db    = $this->getDBO();
		$query = "SELECT mgf.field, mgf.id FROM " . $db->quoteName('#__mica_group_field') . " AS mgf
				INNER JOIN " . $db->quoteName('#__mica_group') . " AS gp
					ON " . $db->quoteName('mgf.groupid') . " = " . $db->quoteName('gp.id') . "
						AND " . $db->quoteName('gp.group') . " LIKE " . $db->quote('Score') . "
			ORDER BY " . $db->quoteName('field') . " ASC ";

		/* select * from #__mica_group gp,#__mica_group_field mgf  where gp.`group` like 'Composite Score' and gp.id=mgf.groupid*/
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	/**
	 * [getAttributeType description]
	 *
	 * @return  [type]  [description]
	 */
	public function getAttributeType()
	{
		$db     = $this->getDBO();
		$userid = JFactory::getUser()->id;

		// Edited heena 27/06/13 if child user then rural-urban comes from parent plan
		$sel = "SELECT login_userid FROM " . $db->quoteName('#__mica_adduser') . " WHERE " . $db->quoteName('created_userid') . " = " . $db->quote($userid);
		$db->setQuery($sel);
		$login_userid       = (int) $db->loadResult();
		$subscriber_user_id = ($login_userid > 0) ? $login_userid : $userid;
		$query = "SELECT plan_id FROM " . $db->quoteName('#__osmembership_subscribers') . " WHERE " . $db->quoteName('user_id') . " = " . $db->quote($subscriber_user_id);
		$db->setQuery($query);
		$planid = $db->loadResult();

		// Edited end

		$typeQry = "SELECT " . $db->quoteName('type') . "
			FROM " . $db->quoteName('#__mica_user_attribute_type') . "
			WHERE " . $db->quoteName('aec_plan_id') . " = " . $db->quote($planid);
		$db->setQuery($typeQry);

		return $db->loadResult();
	}

	public function planAuthentication()
	{
		$userid = JFactory::getUser()->id;

		$db    = $this->getDBO();

		$query = "SELECT os.allow_villagedata FROM " . $db->quoteName('#__osmembership_plans') . " AS op
				INNER JOIN " . $db->quoteName('#__osmembership_subscribers') . " AS os
					ON " . $db->quoteName('op.id') . " = " . $db->quoteName('os.plan_id') . "
						AND " . $db->quoteName('os.user_id') . " LIKE " . $db->quote($userid) . "
			ORDER BY " . $db->quoteName('os.id') . " DESC ";

		$db->setQuery($query);

		$allow_villagedata = $db->loadResult();

		return $allow_villagedata;
	}
}
