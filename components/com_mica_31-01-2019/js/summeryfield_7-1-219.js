var filterspeed_radio;
JQuery(document).ready(function(){

$('.chartvariables_checkbox').click()
$('.chartvariables_checkbox:eq( 1 )').click()
$('.chartvariables_checkbox:eq( 2 )').click()
$('.chartvariables_checkbox:eq( 3 )').click()
$('.chartvariables_checkbox:eq( 4 )').click()
$('.chartvariables_checkbox:eq( 5 )').click()

	$('#state_tab,#district_tab,#variables_tab').click(function(e) {
		checkvalidation();
	});



	$('.allcheck').click(function(event) {
			/* Act osn the event */
			var id      = $(this).attr('id');
			var input   = id.split('_');
			$("." +input[0]+"_checkbox").prop("checked",$(this).prop("checked"));

			if(input[0]=="state")
			{
				checkvalidation();
				var allVals = [];
				var slvals = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');
				if (selected) {
					$("#statetext").attr('style','color:"#ba171b"');
				} else {
					$("#statetext").attr('style','color: "black"');
				}
				getfrontdistrict(selected);

			}
			else if(input[0]=="district")
			{
				checkvalidation();

				var allVals = [];
				var slvals = [];
				$('.district_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');

				if(selected) {
					$("#districttext").attr('style','color:"#ba171b"');
				} else {
					$("#districttext").attr('style','color: "black"');
				}
				getdistrictattr(selected);

			}


			else
			{


			}
		});


	$('#district_tab').click(function(e) {


		$('#state_tab :selected').each(function(i, selected) {
			myurl += $(selected).val() + ",";
			zoom = 9;
		});
	});

	/// check validation
	$('#state_tab,#district_tab,#variables_tab').click(function(e) {
		checkvalidation();
	});

		//For check all function to work well with all other checkboxes:- start
		$('.leftcontainer').on("change",'.list1 input[type=checkbox]', function(){
			var classname =$(this).attr('class');
			//classname = classname.split(" ");
			if(classname.indexOf("_checkbox")!=-1)
			{
				end   = classname.indexOf("_checkbox");
				start = classname.indexOf(" ")!=-1?classname.indexOf(" "):0 ;
				result = classname.substring(start, end).trim();

			}
	        //console.log('#'+result+'_allcheck');
	        if($(this).prop('checked')==false)
	        {
				//console.log('#'+result+'_allcheck');
				$('#'+result+'_allcheck').prop('checked', false); //works with mpi_allcheck, swcs_checkbox, swcs_allcheck
			}
			else
			{
				//console.log("222");
				if($('.'+result+'list input[type=checkbox]:checked').length == $('.'+result+'list input[type=checkbox]').length)
				{
					$('#'+result+'_allcheck').prop('checked', true);
				}
				else
					$('#'+result+'_allcheck').prop('checked', false);
			}

		});
    	//For check all function to work well with all other checkboxes:- END\


    	var graphdist = $("#district").val();
	//	getAttribute(9, "district");


    	//for search START
    	$('.searchtop').keyup(function(event) {
    		var input, filter, a, id;
    		id     = $(this).attr('id');
    		input  = id.split('_');
    		filter = this.value.toUpperCase();

    		$("."+ input[0] +"list li").each(function(){
    			a = $('label',this).text();
    			if (a.toUpperCase().indexOf(filter) > -1)
    				$(this).show();
    			else
    				$(this).hide();
    		});
    	});
		//for search END


		$('.state_checkbox').click(function(e) {
		checkvalidation();
		var slvals = [];
		$('.state_checkbox:checked').each(function() {
		slvals.push($(this).val())
		})
		selected = slvals.join(',');
		getfrontdistrict(selected);
		})


		$('.chartvariables_checkbox').click()
		$('.chartvariables_checkbox:eq( 1 )').click()
		$('.chartvariables_checkbox:eq( 2 )').click()
		$('.chartvariables_checkbox:eq( 3 )').click()
		$('.chartvariables_checkbox:eq( 4 )').click()
		$('.chartvariables_checkbox:eq( 5 )').click()


		$('.chartstates_checkbox').click()
		$('.chartstates_checkbox:eq( 1 )').click()
		$('.chartstates_checkbox:eq( 2 )').click()
		$('.chartstates_checkbox:eq( 3 )').click()

		JQuery('.distattrtypeselection').css({'display':'none'});
		JQuery('.districtspan,.urbanspan,.townspan').css({'display':'none'});
		JQuery('.group').css({'float':'left'});
		JQuery('.createneworkspace').css({'display':'none'});
		JQuery('.createnewworkspace').css({'display':'none'});

	//getAttribute(9);

	if(typeof(preselected) != "undefined"){
		displaycombo();
	}
});

function checkvalidation()
{
	var statecheck = false, districtcheck = false, variablecheck = false;
	//$("#default").show();
	//check for state selected or not
		if ($('.state_checkbox:checked').length == 0) {
			$("#statetext").attr("style", "color:black");
			statecheck = false;
		}
		else
		{

	   		$("#statetext").attr("style", "color:#ba171b");//red
	   		$("#applyChangesInitial").attr("style", "display:block");
	   		$("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
	   		statecheck = true;
	   	}
	    // check for district selected or not
	    if ($('.district_checkbox:checked').length == 0) {
	    	$("#districttext").attr("style", "color:black");
	    	districtcheck = false;
	    }
	    else
	    {

	    	$("#districttext").attr("style", "color:#ba171b");
	    	$("#applyChangesInitial").attr("style", "display:block");
	    	$("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
	    	districtcheck = true;
	    }


	    if ($('.variable_checkbox:checked').length == 0) {
	    	$("#variabletext").attr("style", "color:black");
	    	variablecheck =  false;
	    }
	    else
	    {
	    	$("#variabletext").attr("style", "color:#ba171b");
	    	$("#applyChangesInitial").attr("style", "display:block");
	    	$("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
	    	variablecheck = true;
	    }
	    if(variablecheck == districtcheck == statecheck == true)
	    {
	    	$("#apply_chnages").removeAttr('disabled').removeClass('btn-disabled');

	    }
	    else
	    {
	    	$("#apply_chnages").attr('disabled','disabled').addClass('btn-disabled');


	    }

	    if(flag == true)
	    {
	    	$("#default").hide();
	    	$("#tabledata").attr('disabled','disabled').addClass('btn-disabled');
	    	$("#graphdata").attr('disabled','disabled').addClass('btn-disabled');
	    	$("#gisdata").attr('disabled','disabled').addClass('btn-disabled');
	    	$("#matrix").attr('disabled','disabled').addClass('btn-disabled');
	    	$("#pmdata").attr('disabled','disabled').addClass('btn-disabled');
	    	$("#default1").show();
	    }
	    else
	    {
	    	$("#default").show();
	    }

}


$(document).on('click', '#apply_chnages', function(){
	$("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
	$("#graphdata").removeAttr('disabled').removeClass('btn-disabled');
	$("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
	$("#matrix").removeAttr('disabled').removeClass('btn-disabled');
	$("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
});

$(document).on('click', '#applyChangesInitial', function(){
	$("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
	$("#graphdata").removeAttr('disabled').removeClass('btn-disabled');
	$("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
	$("#matrix").removeAttr('disabled').removeClass('btn-disabled');
	$("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
});



$(document).on('click', '#enablelegend', function(){
	chart1.options.legend.display = true;
	chart1.update();
	if (document.getElementById('enablelegend').checked) {
		chart1.options.legend.display = true;
		chart1.update();

	} else {
		chart1.options.legend.display = false;
		chart1.update();


	}

});

function getRemove_v2(cls, a) {
	$.ajax({
		url: "index.php?option=com_mica&task=summeryresults.deleteattribute&attr=" + a + "&chkcls=" + cls,
		method: 'GET',
		async: true,
		success: function(data) {
			value = a.replace(/ /g, "_");
			$(".variable_checkbox[value='" + a + "']").prop("checked", false);

			getDataNew();
		}
	});
}
function getdistrict(val){
	
    if(val == "" ){
		JQuery('.level2').css({'display':'none'});
		getAttribute(9);
		return false;
	}else if(val == "all"){
		JQuery('.level2').css({'display':'none'});
		getAttribute(9);
		return false;
	}else{
		getAttribute(9);
	}
	JQuery('.distattrtypeselection').css({'display':'none'});

	JQuery.ajax({
		url     : "index.php?option=com_mica&&task=summeryfront.getsecondlevel&stat="+val,
		method  : 'post',
		success : function(combos){
			JQuery('.level2').css({'display':'block'});
			var segment = combos.split("split");
			JQuery(".districtlist").html(segment[0]);
			//choosenjq("select").chosen();
		}
	});
}

function displaycombo(value) {
	if ($("#state").val() == "all") {
		alert("Please Select State First");
		document.getElementById('dataof').selectedIndex = 0;
		document.getElementById("district").selectedIndex = 0;
		return false;
	} else {

		var allVals = [];
		var slvals = [];
		$('.state_checkbox:checked').each(function() {
			slvals.push($(this).val())
		})
		selected = slvals.join(',');
		getfrontdistrict(selected);
	}

	if (value == "District") {
		$(".districtspan").css({
			"display": "block"
		});
		$(".townspan").css({
			"display": "none"
		});
		$(".urbanspan").css({
			"display": "none"
		});
	}

}

function getvalidation(){
	var myurl = "";
	var zoom  = 5;
	if(document.getElementById('summarystate').value==''){
		//alert(statealert);
		return false;
	}else{
		myurl = "summarystate=";
		JQuery('#summarystate :selected').each(function(i, selected) {
			myurl +=JQuery(selected).val()+",";
			zoom=5;
		});

		myurl +="&summarydistrict=";
		JQuery('#summarydistrict :selected').each(function(i, selected) {
			myurl += JQuery(selected).val()+",";
			zoom  = 6;
		});
		myurl +="&summarydistrict=";

		JQuery('input.statetotal_attributes[type=checkbox]').each(function () {
			if(this.checked){
				myurl +=JQuery(this).val()+",";
			}
		});
		JQuery("#zoom").val(zoom);

		return false;
	}
}

function checkRadio(frmName,rbGroupName){
	var radios = document[frmName].elements[rbGroupName];
	for(var i=0;i<radios.length;i++){
		if(radios[i].checked){
			return true;
		}
	}
	return false;
}

function getAttribute(javazoom,type){
	JQuery.ajax({
		url     :"index.php?option=com_mica&task=summeryresults.getAttribute&zoom="+parseInt(javazoom)+"&type="+type+"&view=summaryshowresults",
		method  : 'GET',
		async   : true,
		success : function(data){
			var segment = data.split("split");
			//JQuery("#statetotal").html(data);
			$("#statetotal").html(segment[0]);
			$("#variableshortcode").html(segment[1]);
			$("#CustomVariable").html(segment[2]);


		}
	});
}

function filterAttributes(value){
	var i = 0;

	JQuery(".statetotalunselectall").click();
	if(value == "Urban"){
		JQuery(".hideurban").css({"display":"block"});
		JQuery(".hiderural").css({"display":"none"});
		JQuery(".hidetotal").css({"display":"none"});
	}

	if(value == "Rural"){
		JQuery(".hideurban").css({"display":"none"});
		JQuery(".hiderural").css({"display":"block"});
		JQuery(".hidetotal").css({"display":"none"});
	}

	if(value == "Total"){
		JQuery(".hideurban").css({"display":"block"});
		JQuery(".hiderural").css({"display":"block"});
		JQuery(".hidetotal").css({"display":"block"});
	}
}

JQuery("#attr").live("click",function(){
	JQuery("#statetotal").toggle();
});

function changeWorkspace(value) {
	if (value == 0) {
		$('.createneworkspace').css({
				'display': 'block'
		});
		$('.createnewworkspace').css({
			'display': ''
		});
		$('.thematiceditoption').css('display', 'block');
	} else {
		$.ajax({
			url: "index.php?option=com_mica&task=summeryresults.loadWorkspace&workspaceid="+value + "&view=summaryshowresults&tmpl=component",
			method: 'GET',
			success: function(data) {

				result = $.parseJSON(data);

				$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
				$('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {

				  getAttribute(9,"district");
					if (typeof(preselected) != "undefined") {
						displaycombo();
                   }
               });
                $("#activeworkspacename").html(result[0].name);
                $("#workspacceedit").hide();
                $("#fade").hide();
                $("#default").show();
                checkvalidation();
            }
        });
	}
}


$(document).on('click', '#createworkspace', function(){

	var workspacename = JQuery("#new_w_txt").val();
	if(workspacename == ""){
		alert("Please Enter workspace name");
		return false;
	}

	$.ajax({
		url: "index.php?option=com_mica&task=summeryresults.saveWorkspace&name=" + workspacename+"&view=summaryshowresults",
		method: 'GET',
		success: function(data) {
			$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
			$("#activeworkspacename").text(workspacename);
			$("#workspacceedit").hide();
			$("#fade").hide();
		}
	});
});

JQuery("#updateworkspace").live("click",function(){
	var workspacename = JQuery("#new_w_txt").val();
	var workspaceid   = JQuery("#profile").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid+"&view=summaryshowresults",
		method  : 'GET',
		success : function(data){
			$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
			$("#activeworkspacename").text(workspacename);
			$("#workspacceedit").hide();
			$("#fade").hide();

		}
	});
});

JQuery("#deleteworkspace").live("click",function(){
	if(!confirm("Are you Sure to Delete Workspace?")){
		return false;
	}

	var workspaceid = JQuery("#profile").val();
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.deleteWorkspace&view=summaryshowresults&workspaceid="+workspaceid,
		method  : 'GET',
		success : function(data){
			$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
			$("#activeworkspacename").text(workspacename);
			$("#workspacceedit").hide();
			$("#fade").hide();

		}
	});
});

JQuery("#submit").live("click",function(){
	if(JQuery("#comparedata").val() == 1){
		var statecompare    = JQuery("#statecompare").val();
		var districtcompare = JQuery("#districtcompare").val();
		var towncompare     = JQuery("#towncompare").val();
		var urbancompare    = JQuery("#urbancompare").val();

		if(urbancompare != ""){
			if(urbancompare == "all"){
				alert("All UA already Selected");
				return false;
			}
			var urban=JQuery("#urban").val();
			if(urban == ""){
				alert("Please Select UA to compare");
				return false;
			}else if(urban == "all"){
				alert("You can not select All UA to compare");
				return false;
			}
		}else if(towncompare != ""){
			if(towncompare == "all"){
				alert("All Towns already Selected");
				return false;
			}
			var villages=JQuery("#villages").val();
			if(villages == ""){
				alert("Please Select Villages to compare");
				return false;
			}else if(villages == "all"){
				alert("You can not select All Villages to compare");
				return false;
			}
		}
		else if(districtcompare!="")
		{
			if(districtcompare=="all")
			{
				alert("All Districts already Selected");
				return false;
			}
			var dist = JQuery("#summarydistrict").val();
			if(dist == ""){
				alert("Please Select District to compare");
				return false;
			}
			else if(dist=="all")
			{
				alert("You can not select All District to compare");
				return false;
			}
		}
		else if(statecompare!="")
		{
			if(statecompare=="all")
			{
				alert("All States already Selected");
				return false;
			}
			var stateval=JQuery("#summarystate").val();
			if(stateval=="")
			{
				alert("Please Select State to compare");
				return false;
			}
			else if(stateval=="all")
			{
				alert("You can not select All State to compare");
				return false;
			}
		}
		return true;

	}else{
		var resetmtype      = 0;
		var checked         = 0;
		var dataof          = JQuery("#dataof").val();
		var districtcompare = JQuery("#summarydistrict").val();
		if(dataof=="District"){
			if(JQuery("#summarydistrict").val()==""){
				alert("Please Select District First");
				return false;
			}
		}
		else if(dataof=="UA")
		{
			if(JQuery("#urban").val()=="")
			{
				alert("Please Select UA First");
				return false;
			}
		}
		else if(dataof=="Villages")
		{
			if(JQuery("#Villages").val()=="")
			{
				alert("Please Select Villages First");
				return false;
			}

		}

		JQuery("input:checkbox:checked").each(function(){
			var chk = JQuery(this).attr("checked");
			if(chk=="checked"){
				checked=1;
			}
		});

		if(checked==0){
			alert("Please select variable first");
			return false;
		}else{
			if(typeof(districtcompare)=="undefined" || districtcompare==""){
				//JQuery("#m_type").remove();
				//alert(JQuery("#m_type").val());
				return true;
			}
		}
	}

});

function checkAllgrp(grpid){
	JQuery('.statetotal_attributes'+grpid).each(function(index){
		JQuery(this).attr('checked','checked');
	});
}

function uncheckAllgrp(grpid){
	JQuery('.statetotal_attributes'+grpid).each(function(index){
		JQuery(this).removeAttr('checked','');
	});
}

function getMinmaxVariable(value){
	JQuery(".minmaxdisplay").html("Please Wait...");
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.getMinMax&value="+encodeURIComponent(value),
		method  : 'GET',
		success : function(data){
			var segment = data.split(",");
			minval = segment[0];
			maxval = segment[1];
			JQuery(".minmaxdisplay").html("<b>MIN :</b>"+segment[0]+"<b><br/>MAX :</b>"+segment[1]+"");
			JQuery("#maxvalh").val(segment[1]);
			JQuery("#minvalh").val(segment[0]);
		}
	});
}

JQuery("#no_of_interval").live("keyup",function(e){
	var str="";
	if(e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105){
		if(typeof(edittheme)=="undefined"){
			edittheme=0;
		}
		createTable(edittheme);
	}
});

function createTable(edittheme){
	var maxval      = JQuery("#maxvalh").val();
	var minval      = JQuery("#minvalh").val();
	var level       = JQuery("#level").val();
	var str         = "";
	var diff        = maxval-minval;
	var max         = minval;
	var disp        = 0;
	var setinterval = diff/(JQuery("#no_of_interval").val());
	setinterval = setinterval.toFixed(2);
	start = minval;
	//alert(edittheme);
	if(JQuery("#no_of_interval").val()>5){
		alert("Interval must be less then 5");
		return false;
	}
	var colorselected="";
	if(edittheme!=1 && usedlevel.indexOf("0")==-1){
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
		JQuery(".colorhide").css({"display":""});
		colorselected=1;
	}else if(edittheme==1 && level=="0"){
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
		JQuery(".colorhide").css({"display":""});colorselected=1;
	}else{
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Pin</th></tr></thead>';
		JQuery(".colorhide").css({"display":"none"});colorselected=0;
	}

	for(var i=1;i<=JQuery("#no_of_interval").val();i++){
		if((edittheme!=1 && usedlevel.indexOf("0")==-1) ){

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
			start = end;

		}else if(edittheme!=1 && usedlevel.indexOf("0")!=-1 && level!="0"){

			if(usedlevel.indexOf("1")==-1){
				level=1;JQuery("#level").val(1);
			}else{
				level=2; JQuery("#level").val(2);
			}

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
			start = end;

		}else if(edittheme==1 && level!="0"){

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
			start = end;

		}else if(edittheme==1 && usedlevel.indexOf("0")!=-1 && level=="0"){

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
			start = end;

		}else{

			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ></tr>';
			start = end;

		}
	}

	JQuery("#displayinterval").html(str);
	if(colorselected == 1){
		JQuery('.simpleColorChooser').click();
	}
	//return str;
}

JQuery('.simpleColorChooser').live("click",function(){
	var value = JQuery('.simple_color').val();
	value = value.replace("#","");
	var steps = JQuery("#no_of_interval").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.getColorGradiant&value="+encodeURIComponent(value)+"&steps="+parseInt(steps++),
		method  : 'GET',
		success : function(data){
			colorselected = data;
			var segment = data.split(",");
			segment = segment.reverse();
			for(var i=1;i<=segment.length;i++){
				JQuery("#color"+(i)).fadeIn(1000,JQuery("#color"+(i)).css({"background-color":"#"+segment[i]}));
			}
		}
	});
});

function getColorPallet(){
	JQuery('.simple_color').simpleColor({
		cellWidth   : 9,
		cellHeight  : 9,
		border      : '1px solid #333333',
		buttonClass : 'colorpickerbutton'
	});
}

JQuery("#savesld").live("click",function(){
	var formula     = JQuery("#thematic_attribute").val();
	//var condition = JQuery("#condition").val();
	var limit       = JQuery("#no_of_interval").val();
	var level       = JQuery("#level").val();
	if(formula=="")
	{
		alert("Please Select Variable");
		return false;
	}
	else if(limit=="")
	{
		alert("Please Select Interval");
		return false;
	}

	var from  ="";
	var to    ="";
	var color ="";
	for(var i=1;i<=limit;i++)
	{
		from +=JQuery("#from"+i).val()+",";
		to   +=JQuery("#to"+i).val()+",";

		if(level=="0" || level=="")
		{
			color +=rgb2hex(JQuery("#color"+i).css("background-color"))+",";
		}
		else
		{
			color +=i+",";
		}
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
		method  : 'GET',
		success : function(data){
			window.location.href  = window.location;
		}
	});
});

JQuery(".deletegrp").live("click",function(){
	var level    = JQuery(this).attr("class");
	var getlevel = level.split(" ");
	var getdelid = JQuery(this).attr("id");
	var segment  = getdelid.split("del_");

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.deletethematicQueryToSession&formula="+encodeURIComponent(segment[1])+"&level="+getlevel[1],
		method  : 'GET',
		success : function(data){
			window.location = '';
		}
	});
});

function rgb2hex(rgb) {
	if(typeof(rgb)=="undefined"){
		return "ffffff";
	}

	if (  rgb.search("rgb") == -1 ) {
		return rgb;
	} else {
		rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
		return  hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}
}

JQuery(".edittheme").live("click",function(){
	edittheme=1;
	var cnt = JQuery("#themeconent").html();

	JQuery("#themeconent").html("");
	JQuery(".themeconent").html(cnt);
	JQuery('.simpleColorContainer').remove();

	getColorPallet();

	var classname = JQuery(this).attr("class");
	var myid      = this.id;
	var getcount  = classname.split(" ");
	var seg       = myid.split("__");
	//JQuery("#thematicquerypopup").click();
	JQuery("#thematic_attribute").val(seg[0]);
	JQuery("#level").val(seg[1]);

	JQuery("#thematic_attribute").change();
	JQuery("#no_of_interval").val(getcount[1]);
	createTable(edittheme);
	JQuery("#no_of_interval").val(getcount[1]);
	JQuery(".range_"+seg[1]).each(function(i){
		i++;
		rangeid     = (JQuery(this).attr("id"));
		var mylimit = rangeid.split("-");
		JQuery("#from"+i).val(mylimit[0]);
		JQuery("#to"+i).val(mylimit[1]);
	});

	var endcolor = "";
	if(seg[1] == "0"){
		for(var i=1;i<=getcount[1];i++){
			JQuery("#color"+(i)).css({"background-color":""+JQuery(".col_"+i).css("background-color")});
			if(getcount[1] == i){
				JQuery(".simpleColorDisplay").css({"background-color":JQuery(".col_"+i).css("background-color")});
			}
			//rangeid=JQuery(".range"+i).attr("id");
		}
	}
	//	JQuery("#thematicquerypopup").click();
	thematicquerypopup();
});

function thematicquerypopup(){
	if(totalthemecount==3 && (typeof(edittheme)=="undefined" || (edittheme)==0)){
		alert("You Can Select maximum 3 Thematic Query for Single Workspace");
		return false;
	}else{
		//document.getElementById('light2').style.display='block';
		//document.getElementById('fade').style.display='block';
		JQuery("#light2").fadeIn();
		JQuery("#fade").fadeIn();
	}
}

JQuery("#updatecustom").live("click",function(){
	var new_name   = JQuery('#new_name').val();
	var oldattrval = JQuery('#oldattrval').val();
		if(JQuery("#new_name").val()==""){
		alert("Please Select Custom Variable First!!");
		return false;
	}

	if(!validateFormula()){
		return false;
	}

	var attributevale=JQuery('textarea#custom_attribute').text();
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.updateCustomAttr&attrname="+new_name+"&attributevale="+encodeURIComponent(attributevale)+"&oldattrval="+encodeURIComponent(oldattrval),
		method  : 'GET',
		success : function(data){
			//JQuery("#closeextra").click();
			//reloadCustomAttr();
			window.location='index.php?option=com_mica&view=summeryresults&Itemid=108';
		}
	});
});

JQuery("#deletevariable").live("click",function(){
	var new_name      = JQuery('.customedit').find(":selected").text();
	var attributevale = JQuery('textarea#custom_attribute').text();
	if(new_name=="" && attributevale==""){
		alert("Please Select Custom Variable");
		return false;
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.deleteCustomAttr&attrname="+new_name+"&attributevale="+attributevale,
		method  : 'GET',
		success : function(data){
			//alert(data);
			window.location='index.php?option=com_mica&view=summeryresults&Itemid=108';
			//window.location='index.php?option=com_mica&view=summeryresults&Itemid=108';
		}
	});
});


function district_sortListDir() {

	var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
	list = $('.variablelist');
	switching = true;
	dir2 = "desc";

	header =$('.district_label');
	for(z=0; z<(header.length); z++)
	{
		var inner_class= $(header[z]).text();
		inner_class = inner_class.split(' ').join('_');
		inner_class = inner_class.split('&').join('_');
		inner_class = inner_class.toLowerCase();
		switching = true;
		dir2 = dir2;

		while (switching) {

			switching = false;

			a = $('.'+inner_class);


			for (i = 0; i < (a.length - 1); i++) {
				shouldSwitch = false;

				if (dir2 == "asc") {
					if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

						shouldSwitch = true;
						break;
					}
				} else if (dir2 == "desc") {

					if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

						shouldSwitch = true;
						break;
					}
				}
			}
			if (shouldSwitch) {

				var other = $(a[i]);
				$(a[i+1]).after(other.clone());
				other.after($(a[i+1])).remove();
				switching = true;

				switchcount++;
			} else {

				if (switchcount == 0 && dir2 == "desc") {
					dir2 = "asc";
					switching = true;
				}
			}
		}
	}
}

function sortListDir(id) {

	var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
	list = document.getElementById(id);
	switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";
    //Make a loop that will continue until no switching has been done:
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        b = list.getElementsByTagName("LI");
        text =list.getElementsByTagName("label");

        //Loop through all list-items:
        for (i = 0; i < (text.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*check if the next item should switch place with the current item,
            based on the sorting direction (asc or desc):*/

            if (dir == "asc") {
            	if (text[i].innerHTML.toLowerCase() > text[i + 1].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically lower than current item,
                    mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
            	if (text[i].innerHTML.toLowerCase() < text[i + 1].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically higher than current item,
                    mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            b[i].parentNode.insertBefore(b[i + 1], b[i]);
            switching = true;
            //Each time a switch is done, increase switchcount by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
            	dir = "desc";
            	switching = true;
            }
        }
    }
}



function closePopup(){
	edittheme = 0;
	document.getElementById('light2').style.display = 'none';
	document.getElementById('fade').style.display   = 'none';
	JQuery("#displayinterval").html("");

	//JQuery("#thematic_attribute").val(0).attr("selected");
	document.getElementById("thematic_attribute").selectedIndex = 0;
	JQuery("#no_of_interval").val("");
	JQuery(".minmaxdisplay").html("");
	if(JQuery(".themeconent").html().length>10){
		JQuery("#themeconent").html(JQuery(".themeconent").html());
		JQuery('.simpleColorContainer').remove();
		getColorPallet();
		JQuery(".simpleColorDisplay").css({"background-color":"#FFFFCC"});
	}
}

function validateFormula(){
	var assignformula = "";
	var formula       = JQuery('textarea#custom_attribute').text();

	JQuery("#avail_attribute option").each(function(){
		//alert(JQuery(this).val());
		//alert(formula);
		var myclass=JQuery(this).parent().attr("class");
		if(JQuery(this).val()==formula.trim() && myclass!="defaultvariablelib"){
			assignformula ="You have already assign '"+formula+"' value to '"+JQuery(this).val()+"' Variable";
		}
	});

	if(assignformula!=""){
		return false;
	}
	return true;
}

function downloadMapPdf(){
	//alert(tomcaturl);
	var start         = map.layers;
	var selectedlayer = "";

	if(javazoom==5 || javazoom==6)
	{
		selectedlayer ="india:rail_state";
	}
	else if(javazoom==7)
	{
		selectedlayer = "india:india_information";
	}
	else if(javazoom==8)
	{
		selectedlayer = "india:jos_mica_urban_agglomeration";
	}
	else
	{
		selectedlayer = "india:my_table";
	}

	var baselayer     = "india:rail_state";
	var buildrequest  = "";
	var buildrequest1 = "";
	for (x in start){
		for (y in start[x].params){
			if(start[x].params.LAYER==selectedlayer){
				if(y=="FORMAT"){
					buildrequest1 += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
				}else{
					buildrequest += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
				}
			}
		}
	}

	buildrequest=buildrequest+"FORMATEQTimage/png";
	buildrequest1=buildrequest1+"FORMATEQTimage/png";
	//alert(tomcaturl+"?"+buildrequest+"&BBOX="+map.getExtent().toBBOX()+"&WIDTH=800&HEIGHT=600");
	var finalurl = buildrequest+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
	var finalurl1 = buildrequest1+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
	window.open("index.php?option=com_mica&task=summeryresults.exportMap&mapparameter="+finalurl+"&baselayer="+finalurl1);
}

function exportMap() {
	// set download url (toDataURL() requires the use of a proxy)
    // OpenLayers.Util.getElement("downloadLink").href = map.toDataURL();
}

  

//var previoushtml="";
function exportImage(name) {
    // exporting
	var store       = '0'; // '1' to store the image on the server, '0' to output on browser
	//var name      = '001'; // name of the image
	var imgtype     = 'jpeg'; // choose among 'png', 'jpeg', 'jpg', 'gif'
	var opt         = 'index.php?option=com_mica&task=summeryresults.amExport&store='+store+'&name='+name+'&imgtype='+imgtype;
	var flashMovie  = document.getElementById('chartdiv');
	// previoushtml =JQuery("#exportchartbutton").html();//"Please Wait..."

	JQuery("#exportchartbutton").html("Please Wait...");//"Please Wait..."
	data=  flashMovie.exportImage(opt);
	flashMovie.amReturnImageData("chartdiv",data);
}

function amReturnImageData(chartidm,data){
	var onclick      = "exportImage(JQuery('#chartype option:selected').text())";
	var button       = '<input type="button" name="down`loadchart`" onclick="'+onclick+'" class="button" value="Export">';
	var previoushtml = JQuery("#exportchartbutton").html(button);
}

//function downloadAction(){
	//window.location='index.php?option=com_mica&task=summeryresults.exportexcel';
		/*var table=JQuery("#alldata").html();

		JQuery.ajax({
			type:"POST",
			data : "table="+encodeURIComponent(table),
			url:"index.php?option=com_mica&view=summeryresults&task=exportexcel",
			success:function(data)
			{
			window.open(data);

			}
		});*/
//}
function downloadAction() {
	window.location = 'index.php?option=com_mica&task=summeryresults.exportexcel';
}


/*function downloadAction(){
	var table=JQuery("#alldata").html();

	JQuery.ajax({
			type:'POST',
			data : "table="+encodeURIComponent(table),
			url:"index.php?option=com_mica&view=summeryresults&task=exportexcel",
			success : function(data){
			window.open(data);
		}
	});
}*/

/*function downloadAction(){

	var table=JQuery("#alldata").html();

	JQuery.ajax({
		data : "table="+encodeURIComponent(table),
		url:"index.php?option=com_mica&view=summeryresults&task=exportexcel",
		method  : 'POST',
		success : function(data){
			window.open(data);
		}
	});
}*/



JQuery(".variablegrp").live("click",function(){
	var myid = JQuery(this).attr("id");
	var dis  = JQuery('.'+myid).css("display");

	JQuery(".hideothers").css({"display":"none"});
	JQuery('.variablegrp').addClass("deactive");
	JQuery(this).addClass("active");
	JQuery(this).removeClass("deactive");
	if(dis=="block"){
		JQuery('.'+myid).css("display","none");
		JQuery(this).addClass("deactive");
		JQuery(this).removeClass("active");
	}else{
		JQuery('.'+myid).css("display","block");
		JQuery(this).addClass("active");
		JQuery(this).removeClass("deactive");
	}
});

function preloader(){
	var preloaderstr="<div id='facebook' ><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><div id='block_4' class='facebook_block'></div><div id='block_5' class='facebook_block'></div><div id='block_6' class='facebook_block'></div></div>";
}

JQuery(".hovertext").live("mouseover",function(e){
	var allclass  =JQuery(this).attr("class");
	var segment   =allclass.replace("hovertext ","");
	var x         = e.pageX - this.offsetLeft;
	var y         = e.pageY - this.offsetTop;
	var popuphtml ="<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>"+segment+"</div>";
	JQuery(this).append(popuphtml);
});

JQuery(".hovertext").live("click",function(e){
	JQuery(this).parent().prev().find("input").prop("checked",true);//,true);
});

JQuery(".hovertext").live("mouseout",function(e){
	JQuery("#popupattr").remove();
});

JQuery(".customvariablecheckbox").live("click",function(){
	var check = (JQuery(this).attr("checked"));

	if(check == "checked"){
		JQuery("#new_name").val(JQuery(this).attr("id"));
		JQuery("#custom_attribute").text(JQuery(this).val());
		JQuery("#save").click();
		//addCustomVariable(JQuery(this).val(),JQuery(this).attr("id"));
	}else{
		window.location = "index.php?option=com_mica&task=summeryresults.deleteattribute&attr="+JQuery(this).attr("id");
	}
});

/*function fixheader()
{
	var tableOffset = JQuery("#datatable").offset().top;
	var $header = JQuery(".firsttableheader,.secondtableheader,.thirdtableheader").clone();
	var $fixedHeader = JQuery("#header-fixed").append($header);

	JQuery("#datatable").scroll(function() {
	    var offset = $(this).scrollTop();

	    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
	        $fixedHeader.show();
	    }
	    else if (offset < tableOffset) {
	        $fixedHeader.hide();
	    }
	});​
}*/

JQuery(document).ready(function(){
	JQuery("li #menu100").parent().attr("class","active");
});

function toggleCustomAction(ele, action){
	if(action != ""){
		//JQuery(ele).prev().attr("checked",action);
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("checked",action);
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').click();
	}else{
		//JQuery(ele).prev().removeAttr("checked");
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').removeAttr("checked");
		var names = new Array();
		names.push(JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("id"));
		window.location="index.php?option=com_mica&task=summeryresults.deleteCustomVariableFromLib&attrname="+names;
	}
	//JQuery(ele).prev().click();
}

JQuery(".customedit").live("click", function(){
	JQuery('#save').attr("id","updatecustom");
	JQuery('#updatecustom').attr("onclick","javascript:void(0)");
	JQuery('#updatecustom').attr("value","Update");

	JQuery('#new_name').val(JQuery(this).attr("id"));
	JQuery('#new_name').attr('disabled', true);
	JQuery('.aa').html("Edit");

	oldattrval=JQuery(this).attr("value");
	JQuery('#oldattrval').val(oldattrval);
	JQuery('textarea#custom_attribute').text(JQuery(this).attr("value"));
	lastchar = '';
	//moverightarea();
	//JQuery('#new_name').val(JQuery(this).prev().attr("id"));
	//JQuery('#custom_attribute').val(JQuery(this).prev().val());
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block';
});
$(document).on('click', '#closeextra', function(){

	JQuery('#updatecustom').attr("id","save");
	JQuery('#save').attr("onclick","checkfilledvalue();");
	JQuery('#save').attr("value","Save");
	JQuery('#new_name').val("");
	JQuery('#new_name').attr('disabled', false);
	JQuery('.aa').html("Add New");
});

JQuery(".deletecustomvariable").live("click",function(){
	var names = new Array();
	var i     = 0;
	JQuery(".dcv:checked").each(function(i){
		names.push(JQuery(this).attr("id"));
	});
	window.location="index.php?option=com_mica&task=summeryresults.deleteCustomVariableFromLib&attrname="+names;
});

JQuery("#fullscreen, #fullscreen1").live("click",function(){
	var fromthematic  =	JQuery("#fullscreen").attr("fromthematic");
	/*JQuery("#map").addClass("fullscreen");
	JQuery(this).css({"display":"none"});
	JQuery("#fullscreenoff").fadeIn();
	JQuery("#map").css({"width":JQuery(window).width()-20,"height":JQuery(window).height()-20});
	map.updateSize();*/

	if(fromthematic==1){
		popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
	}else{
		popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component', 'MapWin');
	}
	popped.document.body.innerHTML = "<div id='map' style='height:"+JQuery(window).height()+"px;width:"+JQuery(window).width()+"px;'></div>";
});

$(".full-data-view").on(".fullscreeniconoff", "click", function() {
	$("#fullscreentable").removeClass("fullscreentable");
	$("#matrixclose").remove();
	$(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");

	$(this).removeClass("fullscreeniconoff");
	$(this).addClass("fullscreenicon");
	$("#fade").css({
		"display": "none"
	});
	$("#tablescroll").removeAttr("width");
	$("#tablescroll").removeAttr("overflow");
	$("#tablescroll").removeAttr("height");
});
$(document).on('click', '.filterspeed', function(){

	var selection     =JQuery(this).val();
	filterspeed_radio = selection;
	if(selection == "0"){
		JQuery("#speed_variable").attr("multiple","true");
		JQuery("#speed_variable").removeClass("inputbox");

		JQuery("#speed_variable").removeClass("chzn-done");
		JQuery("#speed_region").removeClass("chzn-done");

		JQuery("#speed_region").removeAttr("multiple");
		JQuery("#speed_variable_chzn").remove();
		JQuery("#speed_region_chzn").remove();

		JQuery("#speed_region").addClass("inputbox");
	}else{
		JQuery("#speed_variable").removeAttr("multiple");
		JQuery("#speed_variable").addClass("inputbox");
		JQuery("#speed_region").attr("multiple","true");

		JQuery("#speed_variable").removeClass("chzn-done");
		JQuery("#speed_region").removeClass("chzn-done");

		JQuery("#speed_variable_chzn").remove();
		JQuery("#speed_region_chzn").remove();

		JQuery("#speed_region").removeClass("inputbox");

	}

	JQuery(".speed").css({"display":"block"});

});


$(document).on('click', '#showspeed', function(){

	var speedvar    =new Array();

	var speedregion =new Array();


	if(typeof($("#speed_variable").attr("multiple")) == "string"){
		$("#speed_variable").each(function(){
			speedvar.push(encodeURIComponent($(this).val()));

		});
		speedregion.push(encodeURIComponent($("#speed_region").val()));
	}else{
		speedvar.push(encodeURIComponent($("#speed_variable").val()));
		$("#speed_region").each(function(){
			speedregion.push(encodeURIComponent($(this).val()));
		});
	}


	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.getsmeter&region="+speedregion+"&speedvar="+speedvar+"&filterspeed="+filterspeed_radio,
		type    : 'GET',
		success: function(data) {

			$("#spedometer_region").html("");
			$("#spedometer_region").html(data);

			initspeed();

			$("#speedfiltershow").css({
				"display": "block"
			});
			$(".sfilter").css({
				"display": "none"
			});
			$("#spedometer_region").css({
				"width": "915px"
			});
			$("#spedometer_region").css({
				"overflow": "auto"
			});



		}
	});
});
$(document).on('click', '#speedfiltershow', function(){


	JQuery("#speedfiltershow").css({"display":"none"});
	JQuery(".sfilter").css({"display":"block"});
	JQuery("#spedometer_region").css({"width":"681px"});
	JQuery("#spedometer_region").css({"overflow":"auto"});
});


$(document).on('click', '#matrix', function(){


	if(JQuery("#fullscreentable").html().length < 100){
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getMaxForMatrix",
			method  : 'POST',
			success : function(data){
				JQuery("#fullscreentable").html(data);
			}
		});
	}
});

$(document).on('click', '#pmdata', function(){


	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.getMeterajax",
		method  : 'POST',
		success : function(data){

			var result = JQuery.parseJSON(data);
			var keys= JQuery.map(result, function(element,index) {return index});
			var variabeloutput = [];
			var cityoutput=[];
			var i=0;
				JQuery.each(result,function(key,value){ //key:- variabel name
					var variabel=key;
					variabeloutput.push('<option value="'+ variabel +'">'+ variabel +'</option>');
					JQuery.each(value,function(key1,value1){  // key1:- city name
                    var res = key1.split("~~");

                    	if(i==0)
						{
                    	cityoutput.push('<option value="'+ res[0] +'">'+ res[0] +'</option>');
                    	}
                    });
                });

				$('#speed_variable').html(variabeloutput.join(''));
				$('#speed_region').html(cityoutput.join(''));

			}
		});

});


$(document).on('click', '#downmatrix', function(){

	JQuery.ajax({
		url    :"index.php?option=com_mica&task=summeryresults.downloadMatrix&rand=" + Math.floor(Math.random() * 100000),
		method : 'POST',
		success:function(data){
			//JQuery("#speed_region").html(data);
		}
	});
});

// $(document).on('click', '#downmatrix', function(){

//     window.location.href = "index.php?option=com_mica&task=showresults.downloadMatrix&rand=" + Math.floor(Math.random() * 100000);
// });

/*JQuery(document).ready(function(){
	var graphdist = JQuery("#summarydistrict").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.getDistrictlist",
		type    : 'POST',
		data    : "dist="+graphdist,
		success : function(data){
			JQuery("#selectdistrictgraph").html(data);
		}
	});
});*/

$(document).on('click', '#showchart', function(){
	var checkedelements = new Array();
	var checkeddist     = new Array();
	var checkevar       = new Array();
	JQuery("#selectdistrictgraph").find(".districtchecked").each(function(){
		if(JQuery(this).attr("checked")){
			checkedelements.push(this.val);
			checkeddist.push(JQuery(this).val());
		}
	});

	JQuery("#light1007").find(".variablechecked").each(function(){
		if(JQuery(this).attr("checked")){
			checkedelements.push(JQuery(this).val());
			checkevar.push(JQuery(this).val());
		}
	});

	if(checkedelements.length >15){
		alert("Variable + District Total Should be less then 15");
		return false;
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.getGraph",
		type    : 'POST',
		data    : "dist="+checkeddist+"&attr="+checkevar,
		success : function(data){
			var segments = data.split("<->");
			datastr    = segments[0];
			grpsetting = segments[1];
			JQuery("#chartype").change();
		}
	});
});

function getDataNew()
{
	var data = $('#micaform').serialize();
	JQuery.ajax({
			//url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
			url     : "index.php?option=com_mica&task=summeryresults.getDataajax",
			method  : 'POST',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				flag = true;
				JQuery("#result_table").show();
				JQuery("#graph").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();
				JQuery("#gis").hide();

				JQuery(".alldata").html(data);

                //loadCharts();
                var graphdist = "";
                $('.district_checkbox:checked').each(function(i, selected) {

                	graphdist += $(this).val() + ",";
                });
                graphdist = graphdist.slice(0, -1);
                $.ajax({
                	url: "index.php?option=com_mica&task=summeryresults.getDistrictlist",
                	type: 'POST',
                	data: "dist=" + graphdist,
                	success: function(data) {
                		$('#main_loader').hide();
                		$("#result_table").find("#selectdistrictgraph").html(data);
                		$("#speedfiltershow").find("#speed_variable").html(data);
                	}
                });

                $("#default").hide();
                $("#default3").hide();
                $("#default1").hide();
                $("#light").hide();
                $("#applyChangesInitial").prop('btn-disabled', false);
                $("#applyChangesInitial").removeAttr('disabled');


            }
        });
}
	// For Gis Map ... edited by salim STARTED 26-10-2018
	$(document).on('click', '#gisdata', function(){

		$("#gisdata").show();
		$("#potentiometer").hide();
		$("#result_table").hide();
		$("#graph").hide();
		$("#quartiles").hide();
		alert("working.......");
	});

	//JQuery("#matrix").live("click",function(){
		$(document).on('click', '#matrix', function(){

		var data = $('#micaform').serialize();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getMaxForMatrix",
			method  : 'POST',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				$('#main_loader').hide();
				$("#quartiles").show();
				$("#result_table").hide();
				$("#graph").hide();
				$("#potentiometer").hide();
				$("#gis").hide();
				$("#fullscreentable").html(data);
			}
		});
	//}
});

	JQuery(".fullscreenicon").live("click",function(){

		JQuery("#fullscreentable").addClass("fullscreentable");
		JQuery(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'></td></tr></table>");
		JQuery(this).addClass("fullscreeniconoff");
		JQuery(this).removeClass("fullscreenicon");
		var toappend='<div class="divclose" id="matrixclose" style="text-align:right;"><a href="javascript:void(0);" onclick="JQuery(\'.fullscreeniconoff\').click();"><img src="'+siteurl+'/media/system/images/closebox.jpeg" alt="X"></a></div>';
		var html=JQuery("#fullscreentable").html();
		JQuery("#fullscreentable").html(toappend+"<div id='tablescroll'>"+html+"</div>");
		JQuery("#fadefade").css({"display":"block"});
		JQuery("#tablescroll").css({"width":"100%"});
		JQuery("#tablescroll").css({"height":"100%"});
		JQuery("#tablescroll").css({"overflow":"auto"});
	});


	$(document).keyup(function(e) {

		if (e.keyCode == "27") 
		{
       	document.getElementById('workspacceedit').style.display = 'none';
        document.getElementById('light1008').style.display = 'none';
        document.getElementById('light1007').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        }
   })

	function displaycombo(value) {

		if ($("#state").val() == "all") {
		alert("Please Select State First");
			document.getElementById('dataof').selectedIndex = 0;
			document.getElementById("summarydistrict").selectedIndex = 0;
			return false;
		} else {

			var allVals = [];
			var slvals = [];
			$('.state_checkbox:checked').each(function() {
				slvals.push($(this).val())
			})
			selected = slvals.join(',');
			 getfrontdistrict(selected);
		}

		if (value == "District") {
			$(".districtspan").css({
				"display": "block"
			});
			$(".townspan").css({
				"display": "none"
			});
			$(".urbanspan").css({
				"display": "none"
			});
		}

	}
	function variable_sortListDir() {

		var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
		list = $('.variablelist');
		switching = true;
		dir = "desc";

		header =$('.variable_label');
		for(z=0; z<(header.length); z++)
		{
			var inner_class= $(header[z]).text();
			inner_class = inner_class.split(' ').join('_');
			inner_class = inner_class.toLowerCase();
			switching = true;
			dir = dir;

			while (switching) {

				switching = false;

				a = $('.'+inner_class);


				for (i = 0; i < (a.length - 1); i++) {
					shouldSwitch = false;

					if (dir == "asc") {
						if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

							shouldSwitch = true;
							break;
						}
					} else if (dir == "desc") {

						if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

							shouldSwitch = true;
							break;
						}
					}
				}
				if (shouldSwitch) {

					var other = $(a[i]);
					$(a[i+1]).after(other.clone());
					other.after($(a[i+1])).remove();
					switching = true;

					switchcount++;
				} else {

					if (switchcount == 0 && dir == "desc") {
						dir = "asc";
						switching = true;
					}
				}
			}
		}
	}
		function loadCharts()
	{
		var data = $('#micaform').serialize();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getGraphajax",
			method  : 'post',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				jQuery('#main_loader').hide();
				JQuery("#graph").show();
				JQuery("#result_table").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();

				var backgroundColor = 'white';
				Chart.plugins.register({
					beforeDraw: function(c) {
						var ctx = c.chart.ctx;
						ctx.fillStyle = backgroundColor;
						ctx.fillRect(0, 0, c.chart.width, c.chart.height);
					}
				});

				var x = document.getElementById("chartype").value;
				var ctx = document.getElementById('myChart').getContext('2d');
				var datasets = [];
				var i =0;
				var result = JQuery.parseJSON(data);
				var color = Chart.helpers.color;
				var colorcode= ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
				var labels =[];
				var joinedClassesmain=[];
				var i =0;
				var keys= JQuery.map(result, function(element,index) {return index});
				var joinedClasses = new Array();
				JQuery.each(result,function(key,value){
					JQuery.each(value,function(key1,value1){
						if(i==0)
						{
							labels.push(key1);
						}
						if (joinedClasses[key1] === undefined)
						{
							joinedClasses[key1]= new Array();
						}
                            joinedClasses[key1][key]=value1;// vaule define
                        });
					i++;
				});
				i=0;
				var statecity = '';
				statecity = '<ul id ="statesForGraph" class="list1">';
                JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
                	var res = value.split("~~");
                	var cityname=res[0];
                	var citycode=res[1];


                	var container = $('#cblist');
                	statecity += "<li>"+
                	'<input type = "checkbox" class="chartstates_checkbox" '+
                	' id="chartstates_' + citycode +'" value="'+ citycode+ '" checked/>'+
                	'<label for="check">'+ cityname+ "</label>"+
                	"</li>";


                	var newres = Object.keys(joinedClasses[value]).map(function(k) {
                		return [k, joinedClasses[value][k]];
                	});

                	var joinedClassesmain= [];
                	var radius= [];
                	$.each(newres, function (key1,value1) {


                		joinedClassesmain.push(value1[1]);
                		radius.push(value1[1]);

                	});
                   var variable1= [];
                   var newhtml ='';
                   newhtml = '<ul id ="variablesForGraph" class="list1">';

                   $.each(newres, function (key1,value1) {

                   	variable1=value1[0];



                   	newhtml += "<li>"+
                   	'<input type = "checkbox" class="chartvariables_checkbox" '+
                   	' id="chartvariables_' + variable1 +'" value="'+ variable1+ '" checked/>'+
                   	'<label for="chartvariables_' + variable1 + '">'+ variable1+ "</label>"+
                   	"</li>";

                   });
                   newhtml +='</ul>';
                   $('#variabelist').html(newhtml);




                   datasetValues[citycode] = joinedClassesmain;

                   datasets.push({
                            'label':cityname, //city name should come here
                            'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
                            'borderColor':colorcode[i],
                            'borderWidth':1,
                            'data':joinedClassesmain,
							'id':citycode,
                            'id1':variable1
						});
                   i++;
               });
                statecity +='</ul>';

                $('#cblist').html(statecity);
                statecity='';

                chart1 = new Chart(ctx, {
                	type: x,

                    // The data for our dataset
                    data: {
                    	labels: keys,
                    	datasets: datasets

                    },
                    options: {
                    	legend: {
                    		display: false,
                    	},

                    	scales: {
                    		xAxes: [{
                    			ticks: {
                    				maxTicksLimit: 8,
                    				fontSize:5,
                    				fontFamily:'Helvetica',
                    				fontColor:'red',
                    				position:'top',
                    				fontStyle:'bold',


                    			}
                    		}]
                    	},

                    }
                });
                $('.chartvariables_checkbox').click();
                $('.chartvariables_checkbox:eq( 0 )').click();
                $('.chartvariables_checkbox:eq( 1 )').click();
                $('.chartvariables_checkbox:eq( 2 )').click();
                $('.chartvariables_checkbox:eq( 3 )').click();
                $('.chartvariables_checkbox:eq( 4 )').click();

            }
        });
}
/*	function loadCharts()
	{
		var data = $('#micaform').serialize();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getGraphajax",
			method  : 'post',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				jQuery('#main_loader').hide();
				JQuery("#graph").show();
				JQuery("#result_table").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();


				var backgroundColor = 'white';
				Chart.plugins.register({
					beforeDraw: function(c) {
						var ctx = c.chart.ctx;
						ctx.fillStyle = backgroundColor;
						ctx.fillRect(0, 0, c.chart.width, c.chart.height);
					}
				});


				var color = Chart.helpers.color;
				var result = JQuery.parseJSON(data);
				var i =0;
				var labels =[];
				var colorcode= ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
				var options = {
				type: 'bubble',
				data: {
					datasets: [
						{
						label: 'John',
						data: [
						{
						x: 3,
						y: 7,
						r: 10
						}
					],
					backgroundColor:color(colorcode[i]).alpha(0.5).rgbString(),
					hoverBackgroundColor: colorcode[i]
					}
					]
				}
				}

				var ctx = document.getElementById('myChart').getContext('2d');
				new Chart(ctx, options);



            }


        });
	}*/
	/*function loadCharts()
	{

		var data = $('#micaform').serialize();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getGraphajax",
			method  : 'post',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				jQuery('#main_loader').hide();
				JQuery("#graph").show();
				JQuery("#result_table").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();


				var backgroundColor = 'white';
				Chart.plugins.register({
					beforeDraw: function(c) {
						var ctx = c.chart.ctx;
						ctx.fillStyle = backgroundColor;
						ctx.fillRect(0, 0, c.chart.width, c.chart.height);
					}
				});

				var x = document.getElementById("chartype").value;
				var datasets = [];
				var i =0;
				var result = JQuery.parseJSON(data);
				var color = Chart.helpers.color;
				var colorcode= ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
				var labels =[];
				var joinedClassesmain=[];
				var i =0;
				var keys= JQuery.map(result, function(element,index) {return index});
				var joinedClasses = new Array();
				JQuery.each(result,function(key,value){
					JQuery.each(value,function(key1,value1){
						if(i==0)
						{
							labels.push(key1);
						}
						if (joinedClasses[key1] === undefined)
						{
							joinedClasses[key1]= new Array();
						}
                            joinedClasses[key1][key]=value1;// vaule define
                            console.log(joinedClasses[value]);
					});
					i++;
				});*/

				

				  
					/*var newres = Object.keys(joinedClasses[value]).map(function(k) {
                		return [k, joinedClasses[value][k]];
                	});
					*/
					/*var total     = 0;
                	var radius    = 0;
                	var xyr_data  = [];
                	$.each(newres, function (key1,value1) {
                		total+=parseFloat(this) || 0;
                		if (key1 == 0) {
                			radius = value1[0]/total;   
							joinedClassesmain.push(radius);
							xyr_data['x'] = value1;
	                		xyr_data['y'] = value1;
	                		xyr_data['r'] = radius;
                		}
                	});*/


			
               
				/*var finalres = Object.keys(joinedClasses[value]).map(function(k) {
                		return [k, joinedClasses[value][k]];
                	});*/

				

				/*var joinedClassesmain= [];
                	$.each(finalres, function (key1,value1) {
                		joinedClassesdata.push(value1[1]);
                	});

                	var total     = 0;
                	var radius    = 0;
                	var xyr_data  = [];	
                	$.each(joinedClassesdata, function (key1,value1) {
                		total+=parseFloat(this) || 0;
                		if (key1 == 0) {
                			radius = value1[0]/total;   
							joinedClassesmain.push(radius);
							xyr_data['x'] = value1;
	                		xyr_data['y'] = value1;
	                		xyr_data['r'] = radius;
                		}
                	});*/


				/*i=0;

				var statecity = '';
				statecity = '<ul id ="statesForGraph" class="list1">';
                JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
                	var res = value.split("~~");
                	var cityname=res[0];
                	var citycode=res[1];


                	var container = $('#cblist');
                	statecity += "<li>"+
                	'<input type = "checkbox" class="chartstates_checkbox" '+
                	' id="chartstates_' + citycode +'" value="'+ citycode+ '" checked/>'+
                	'<label for="check">'+ cityname+ "</label>"+
                	"</li>";
                  var newres = Object.keys(joinedClasses[value]).map(function(k) {
                		return [k, joinedClasses[value][k]];
                	});

                	var joinedClassesmain= [];
                	$.each(newres, function (key1,value1) {
                		joinedClassesmain.push(value1[1]);
                	});


                	
                	var variable1 = [];
                    var newhtml   = '';

                	newhtml = '<ul id ="variablesForGraph" class="list1">';
                   $.each(newres, function (key1,value1) {
                   	variable1 = value1[0];
					newhtml += "<li>"+
                   	'<input type = "checkbox" class="chartvariables_checkbox" '+
                   	' id="chartvariables_' + variable1 +'" value="'+ variable1+ '" checked/>'+
                   	'<label for="chartvariables_' + variable1 + '">'+ variable1+ "</label>"+
                   	"</li>";

                   });

                   newhtml +='</ul>';
                   $('#variabelist').html(newhtml);
				   
				   datasetValues[citycode] = joinedClassesmain;
				  i++;
				  
               });


                statecity +='</ul>';

                $('#cblist').html(statecity);
                statecity='';
                var xyr_data =0;
           	    var options = {
				type: 'bubble',
				data: { 
					datasets: [
						{
						label:  keys,
						data : 	xyr_data,
					
					backgroundColor:color(colorcode[i]).alpha(0.5).rgbString(),
					hoverBackgroundColor: colorcode[i]
					}
					]
				}
				
			}
			
				var ctx = document.getElementById('myChart').getContext('2d');
				new Chart(ctx, options);

			 }
        });
	}*/



function getfrontdistrict(val){

	if(val == "" ){
		$('.level2').css({'display':'none'});
		 getAttribute(9, "state");

		$(".distattrtypeselection").css({"display":"none"});
		return false;
	}else if(val=="all"){
		document.getElementById('summarydistrict').selectedIndex=0;
		document.getElementById('urban').selectedIndex=0;
		document.getElementById('villages').selectedIndex=0;

		$('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({'display':'none'});
		  getAttribute(9, "state");
		return false;
	}else{
		 getAttribute(9, "state");

		$(".distattrtypeselection").css({"display":"none"});
	}
 	$("#loader").show();
	$('.distattrtypeselection').css({'display':'none'});

	$.ajax({
		url     : "index.php?option=com_mica&task=summeryfront.getsecondlevel&stat="+val+"&preselected="+preselecteddata,
		method  : 'post',
		success : function(combos){
			var segment = combos.split("split");
			JQuery(".districtlist").html(segment[0]);
			JQuery("#statecode").html(segment[1]);

			if(preselecteddata != ""){
				getAttribute(9, "district");
			}
			var dataof       = JQuery("#dataof").val();
			if(dataof == "ua" || dataof == "UA"){
				dataof="urban";
			}
			$( "#loader").hide();
			checkvalidation();
		}
	});
}


$(document).on('click', '#pmdata', function(){
	$("#potentiometer").show();
	$("#result_table").hide();
	$("#graph").hide();
	$("#quartiles").hide();
	$("#gis").hide();
});



