var edittheme = 0;
$(document).ready(function() {
    $('#district_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        JQuery('#state_tab :selected').each(function(i, selected) {
            myurl +=JQuery(selected).val()+",";
            zoom=5;
        });
    });

         $('#industry_level_tab').click(function(e){
            if($('.state_checkbox:checked').length == 0){
                alert(statealert);
                return false;
            }

            if($('.district_checkbox:checked').length == 0){
                alert(districtalert);
                return false;
            }
        });


        $('#type_tab').click(function(e){
            if($('.state_checkbox:checked').length == 0){
                alert(statealert);
                return false;
            }

            if($('.district_checkbox:checked').length == 0){
                alert(districtalert);
                return false;
            }
            
        });

        $('#variable_tab').click(function(e){
            if($('.state_checkbox:checked').length == 0){
                alert(statealert);
                return false;
            }

            if($('.district_checkbox:checked').length == 0){
                alert(districtalert);
                return false;
            }

            if($('.type_checkbox:checked').length == 0){
                alert(typealert);
                return false;
            }
        });

        $('#mpi_tab').click(function(e){
            if($('.state_checkbox:checked').length == 0){
                alert(statealert);
                return false;
            }

            if($('.district_checkbox:checked').length == 0){
                alert(districtalert);
                return false;
            }

            if($('.type_checkbox:checked').length == 0){
                alert(typealert);
                return false;
            }

        });

        $('#swcs_tab').click(function(e){
            if($('.state_checkbox:checked').length == 0){
                alert(statealert);
                return false;
            }

            if($('.district_checkbox:checked').length == 0){
                alert(districtalert);
                return false;
            }

            if($('.type_checkbox:checked').length == 0){
                alert(typealert);
                return false;
            }
        });
    
    if(typeof(usedlevel) != "undefined"){
        var ulevel=usedlevel.split(",");
        if(ulevel.length == 3){
        	JQuery("#themeconent").css({"display":"none"});
        }
        getColorPallet_v2();
    }

    var graphdist = $("#district").val();

    if (typeof(preselected) != "undefined") {
        displaycombo_v2();

    }
    var minval = "";
    var maxval = "";
    var colorselected = "";

    $(".hideothers").css({
        "display": "none"
    });

    if (typeof(havingthematicquery) != "undefined") {
        var singlequery = havingthematicquery.split(",");
        $("#thematic_attribute option").each(function() {
            for (var i = 0; i <= singlequery.length; i++) {
                if (singlequery[i] == $(this).val()) {
                    $(this).attr("disabled", true);
                }
            }
        });
    }


    //Check all For District,State and Variables
    $('.allcheck').click(function(event) {
        checkvalidation();

        /* Act on the event */
        var id = $(this).attr('id');
        var input = id.split('_');
        $("." + input[0] + "_checkbox").prop("checked", $(this).prop("checked"));

        if (input[0] == "state") { ///For State
            var allVals = [];
            var slvals = [];
            $('.state_checkbox:checked').each(function() {
                slvals.push($(this).val())
            })
            selected = slvals.join(',');
            if (selected) {
                $("#statetext").attr('style','color:"#ba171b"');
            } else {
                $("#statetext").attr('style','color: "black"');
            }
            getfrontdistrict_v2(selected);


        } else if (input[0] == "district") { //For District

            var allVals = [];
            var slvals = [];
            $('.district_checkbox:checked').each(function() {
                slvals.push($(this).val())
            });

            selected = slvals.join(',');
             if (selected) {
                $("#districttext").attr('style','color:"#ba171b"');
            } else {
                $("#districttext").attr('style','color: "black"');
            }

            getdistrictattr_v2(selected);
        } else if (input[0] == "variable") { // For Variables

            var allVals = [];
            var slvals = [];
            $('.variable_checkbox:checked').each(function() {
                slvals.push($(this).val())
            });
            selected = slvals.join(',');
           // getVariableList_v2(selected);
        }
    });
    $("#matrix").click(function() {
       if (checkvalidation() == false)
            return false;

        //if($("#fullscreentable").html().length < 100){
        var data = $('#micaform').serialize();
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.getMaxForMatrix",
            method: 'POST',
            async: true,
            data: data,
            beforeSend: function() {
                $('#main_loader').show();
            },
            success: function(data) {
                $('#main_loader').hide();
                $("#quartiles").show();
                $("#result_table").hide();
                $("#graph").hide();
                $("#potentiometer").hide();
                $("#gis").hide();
                $("#fullscreentable").html(data);
            }
        });
        //}
    });

    $("#pmdata").click(function() {
        $("#potentiometer").show();
        $("#result_table").hide();
        $("#graph").hide();
        $("#quartiles").hide();
        $("#gis").hide();
    });

    $(document).on("click", "#createworkspace" , function() {

        var workspacename = $("#new_w_txt").val();
        if (workspacename == "") {
            alert("Please Enter workspace name");
            return false;
        }

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.saveWorkspace&name=" + workspacename,
            method: 'GET',
            success: function(data) {
                $("#workspacceedit").load(location.href + " #workspacceedit>*", "");
                $("#activeworkspacename").text(workspacename);
                  $("#workspacceedit").hide();
                  $("#fade").hide();
            }
        });

    });

    $(document).on("click", "#deleteworkspace" , function() {

        if (!confirm("Are you Sure to Delete Workspace?")) {
            return false;
        }

        var workspaceid = $("#profile").val();
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteWorkspace&workspaceid=" + workspaceid,
            method: 'GET',
            success: function(data) {
               $("#workspacceedit").load(location.href + " #workspacceedit>*", "");
                $("#activeworkspacename").text(workspacename);
                  $("#workspacceedit").hide();
                  $("#fade").hide();
            }
        });

    });

    $(document).on("click","#updateworkspace", function(){

        var workspacename = $("#new_w_txt").val();
        var workspaceid   = $("#profile").val();

        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid,
            method  : 'GET',
            success : function(data){
                 $("#workspacceedit").load(location.href + " #workspacceedit>*", "");
                $("#activeworkspacename").text(workspacename);
                  $("#workspacceedit").hide();
                   $("#fade").hide();

            }
        });
    });

    $(document).on("click", ".filterspeed" , function() {

        var selection = $(this).val();

        filterspeed_radio = selection;
        if (selection == "0") {
            $("#speed_variable").attr("multiple", "true");
            $("#speed_variable").removeClass("inputbox");

            $("#speed_variable").removeClass("chzn-done");
            $("#speed_region").removeClass("chzn-done");

            $("#speed_region").removeAttr("multiple");
            $("#speed_variable_chzn").remove();
            $("#speed_region_chzn").remove();
            //choosenjq("#speed_region").chosen({max_selected_options: 1});
            //choosenjq("#speed_variable").chosen({max_selected_options: 5});
            $("#speed_region").addClass("inputbox");
        } else {
            $("#speed_variable").removeAttr("multiple");
            $("#speed_variable").addClass("inputbox");
            $("#speed_region").attr("multiple", "true");

            $("#speed_variable").removeClass("chzn-done");
            $("#speed_region").removeClass("chzn-done");

            $("#speed_variable_chzn").remove();
            $("#speed_region_chzn").remove();

            $("#speed_region").removeClass("inputbox");
            //choosenjq("#speed_region").chosen({max_selected_options: 5});
            //choosenjq("#speed_variable").chosen({max_selected_options: 1});
        }

        $(".speed").css({
            "display": "block"
        });
        //choosenjq("#speed_variable").val('').trigger("liszt:updated");
        //choosenjq("#speed_region").val('').trigger("liszt:updated");
    });



    $(document).on( "click", "#showspeed", function() {

        var speedvar = new Array();
        var speedregion = new Array();
        if (typeof($("#speed_variable").attr("multiple")) == "string") {
            $("#speed_variable").each(function() {
                speedvar.push(encodeURIComponent($(this).val()));
            });
            speedregion.push(encodeURIComponent($("#speed_region").val()));
        } else {
            speedvar.push(encodeURIComponent($("#speed_variable").val()));
            $("#speed_region").each(function() {
                speedregion.push(encodeURIComponent($(this).val()));
            });
        }
        //var filterspeed = $('input:radio[name=filter]:checked]').val();

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.getsmeter&region=" + speedregion + "&speedvar=" + speedvar + "&filterspeed=" + filterspeed_radio,
            type: 'GET',
            success: function(data) {
                $("#spedometer_region").html("");
                $("#spedometer_region").html(data);
                initspeed();
                $("#speedfiltershow").css({
                    "display": "block"
                });
                $(".sfilter").css({
                    "display": "none"
                });
                $("#spedometer_region").css({
                    "width": "915px"
                });
                $("#spedometer_region").css({
                    "overflow": "auto"
                });



            }
        });
    });

    $(document).on("click", "#speedfiltershow" , function() {
        $("#speedfiltershow").css({
            "display": "none"
        });
        $(".sfilter").css({
            "display": "block"
        });
        $("#spedometer_region").css({
            "width": "681px"
        });
        $("#spedometer_region").css({
            "overflow": "auto"
        });
    });

    $('.leftcontainer').on("change", '.list1 input[type=checkbox]', function() {
        var classname = $(this).attr('class');
        //classname = classname.split(" ");
        if (classname.indexOf("_checkbox") != -1) {

            end = classname.indexOf("_checkbox");
            start = classname.indexOf(" ") != -1 ? classname.indexOf(" ") : 0;
            result = classname.substring(start, end).trim();

        }
        //console.log('#'+result+'_allcheck');
        if ($(this).prop('checked') == false) {
            //console.log('#'+result+'_allcheck');

            $('#' + result + '_allcheck').prop('checked', false); //works with mpi_allcheck, swcs_checkbox, swcs_allcheck

        } else {
            //console.log("222");
            if ($('.' + result + 'list input[type=checkbox]:checked').length == $('.' + result + 'list input[type=checkbox]').length) {

                $('#' + result + '_allcheck').prop('checked', true);

            } else

                $('#' + result + '_allcheck').prop('checked', false);

        }

    });

    //For check all function to work well with all other checkboxes:- END

    //for search START

    $('.searchtop').keyup(function(event) {
        var input, filter, a, id;
        id = $(this).attr('id');
        input = id.split('_');
        filter = this.value.toUpperCase();

        $("." + input[0] + "list li").each(function() {
            a = $('label', this).text();
            if (a.toUpperCase().indexOf(filter) > -1)
                $(this).show();
            else
                $(this).hide();
        });
    });

    //checkbox checked apply chnges button hide show

    $('#district_tab').click(function(e) {
        $('#state_tab :selected').each(function(i, selected) {
            myurl += $(selected).val() + ",";
            zoom = 5;
        });
    });

	/// check validation
   
    $('.state_checkbox').click(function(e) {
    	checkvalidation();
        var slvals = [];
        $('.state_checkbox:checked').each(function() {
            slvals.push($(this).val())
        })
        selected = slvals.join(',');
        getfrontdistrict_v2(selected);
    })

    $('#leftcontainer').on("click",'.district_checkbox',function(e) {
    	checkvalidation();

        var slvals = [];
        $('.district_checkbox:checked').each(function() {
            slvals.push($(this).val())
        })
        selected = slvals.join(',');


        getAttribute_v2(selected);
    })

    //Chart functionalities

    $('#graph').on('click', '.chartvariables_checkbox', function() {
        //console.log(chart1);
        var variabelname = $(this).val();
        var indexoflabels = $.inArray(variabelname, chart1.data.labels);
        //console.log(trashedData);
        if (indexoflabels != -1) {
            if (document.getElementById('chartvariables_' + variabelname).checked) {
            } else {
                var templabel = chart1.data.labels.splice(indexoflabels, 1);
                //console.log(templabel);
                trashedLabels.push(templabel[0]);
                //console.log(trashedLabels);
                
                chart1.data.datasets.forEach(function(val, index) {
                    var citycode = val.id;
                    var tempdata = val.data.splice(indexoflabels, 1);

                    if (trashedData[citycode] == undefined)
                            trashedData[citycode] = [];
                        trashedData[citycode][templabel[0]] = tempdata[0];
                });
            }
        } else {
            var indexoflabels_t = $.inArray(variabelname, trashedLabels);

            if (indexoflabels_t != -1) {
                chart1.data.labels.push(variabelname);
                chart1.data.datasets.forEach(function(val, index) {
                    tempdata = trashedData[val.id][variabelname];

                    val.data.push(tempdata);
                });

            }
        }
        chart1.update();
    });

    $('#graph').on('click', '.chartstates_checkbox', function() {
        var state_id = $(this).val();
        chart1.data.datasets.forEach(function(val, index) {
            if (val.id == state_id) {
                if (document.getElementById('chartstates_' + state_id).checked) {
                    val._meta[0].hidden = false;
                    return;

                } else {
                    val._meta[0].hidden = true;
                    return;
                }
            }
            chart1.update();
        })
    });

    /*$(".full-data-view").on("#no_of_interval", "keyup", function(e) {
        alert('call123');
        var str = "";
        if (e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105) {
            if (typeof(edittheme) == "undefined") {
                edittheme = 0;
            }
            createTable_v2(edittheme);
        }
    });*/

    $(document).on('keyup','#no_of_interval',function(e){
        
        /*if($('.themeconent')[0]) { 
            alert('if'+edittheme);
        }else{
            alert('else'+edittheme);
        }*/
        var str = "";
        if (e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105) {
            if (typeof(edittheme) == "undefined") {
                edittheme = 0;
            }
            createTable_v2(edittheme);
        }
    });
});



if ($('.customvariablecheckbox').prop('checked') == true) {
    $("#applyChangesInitial").prop('btn-disabled', false);
    $("#applyChangesInitial").removeAttr('disabled');

}



function getfrontdistrict_v2(val) {

    // $( "#loader").show();
    //console.log('shown')
    if (val == "") {
        //$('.state_checkbox').prop('checked', true);
        $('.level2').css({
            'display': 'none'
        });
        getAttribute_v2(5, "state");
        //document.getElementById('m_type').selectedIndex=0;
        $(".distattrtypeselection").css({
            "display": "none"
        });

        return false;
    } else if (val == "all") {

        document.getElementById('district').selectedIndex = 0;
        document.getElementById('urban').selectedIndex = 0;
        document.getElementById('town').selectedIndex = 0;
        //document.getElementById('m_type').selectedIndex=0;
        $('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({
            'display': 'none'
        });
        getAttribute_v2(5, "state");

        return false;
    } else {
        getAttribute_v2(5, "state");
        //document.getElementById('m_type').selectedIndex=0;
        $(".distattrtypeselection").css({
            "display": "none"
        });
    }
    $("#loader").show();
    // console.log('shown')

    $('.distattrtypeselection').css({
        'display': 'none'
    });

    $.ajax({
        url: "index.php?option=com_mica&task=micafront.getsecondlevel&stat=" + val + "&preselected=" + preselecteddata,
        method: 'post',
        success: function(data) {
            //$( "#loader").show();
            // console.log('shown')

            var segment = data.split("split");
            //var segment = data.split("split");

            $(".districtlist").html(segment[0]);
            $("#statecode").html(segment[1]);


            if (preselecteddata != "") {
                getAttribute_v2(5, "district");
            }
            var dataof = $("#dataof").val();

            if (typeof(preselecteddataof) != "undefined") {
                var selectedtype = preselecteddataof.toLowerCase();
                if (selectedtype == "ua" || selectedtype == "UA") {
                    selectedtype = "urban";
                }
            }
            if (dataof == "ua" || dataof == "UA") {
                dataof = "urban";
            }
            checkvalidation();
            $("#loader").hide();
        }
    });
}

function getdistrict_v2(val) {
    if (val == "") {
        $('.level2').css({
            'display': 'none'
        });
        getAttribute_v2(5, "state");
        return false;
    } else if (val == "all") {
        $('.level2').css({
            'display': 'none'
        });
        getAttribute_v2(5, "state");
        return false;
    } else {
        getAttribute_v2(5, "state");
    }

    $('.distattrtypeselection').css({
        'display': 'none'
    });

    $.ajax({
        url: "index.php?option=com_mica&&task=micafront.getsecondlevel&stat=" + val,
        method: 'post',
        success: function(combos) {
            $('.level2').css({
                'display': 'block'
            });
            var segment = combos.split("split");
            $("#districtspan").html(segment[0]);
            $("#villagespan").html(segment[0]);

        }
    });
}

function getdistrictattr_v2(val) {

    var val = $('.district_checkbox').val();
    $("#loaderdistrict").show();
    //console.log('shown')

    //alert(val);
    if (val == "") {
        $('.townspan').css({
            'display': 'block'
        });
        $('.urbanspan').css({
            'display': 'block'
        });
        $('.distattrtypeselection').css({
            'display': 'none'
        });
        resetCombo('district');

        getAttribute_v2(5, "state");
    } else {
        getAttribute_v2(5, "district");
        $.ajax({
            url: "index.php?option=com_mica&task=micafront.getsecondlevelvillage&villagedistrict=" + val,
            method: 'post',
            success: function(combos) {
                var segment = combos.split("split");
                $('.villagelist').html(segment[0]);
                //choosenjq("select").multiselect({selectedList: 1});
                var dataof = $("#dataof").val();

                if (dataof == "ua" || dataof == "UA") {
                    dataof = "urban";
                }
                $("#loaderdistrict").hide();
                //console.log('hidden')
            }
        });
    }

}


// AJAX IN STATE

function getAttribute_v2(javazoom, type)
{
    var allVals = [];
    var slvals = [];
    $('.district_checkbox:checked').each(function() {
        slvals.push($(this).val())
    })

    selected = slvals.join(',');

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getAttribute&zoom=" + parseInt(javazoom) + "&type=" + type + "&district=" + selected,
        method: 'GET',
        async: true,
        success: function(data) {
            var segment = data.split("split");

            $("#variables").html(segment[0]);

            $("#variableshortcode").html(segment[1]);
            $("#CustomVariable").html(segment[2]);
            $(".hideothers").css({
                "display": "none"
            });
        }
    });
}

/*
Load Charts
 */
function loadCharts()
{

    var data = $('#micaform').serialize();
    JQuery.ajax({
        url     : "index.php?option=com_mica&task=showresults.getGraphajax",
        method  : 'post',
        async   : true,
        data    : data,
        beforeSend: function() {
            jQuery('#main_loader').show();
        },
        success : function(data){
            jQuery('#main_loader').hide();
            JQuery("#graph").show();
            JQuery("#result_table").hide();
            JQuery("#quartiles").hide();
            JQuery("#potentiometer").hide();

            /*------------------bakground coclor------------*/
            var backgroundColor = 'white';
            Chart.plugins.register({
                beforeDraw: function(c) {
                    var ctx = c.chart.ctx;
                    ctx.fillStyle = backgroundColor;
                    ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                }
            });

            /*--------------bakground coclor end------------*/

            var x = document.getElementById("chartype").value;
            var ctx = document.getElementById('myChart').getContext('2d');
            var datasets = [];
            var i =0;
            var result = JQuery.parseJSON(data);
            var color = Chart.helpers.color;
            var colorcode= ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
            var labels =[];
            var joinedClassesmain=[];
            var i =0;
            var keys= JQuery.map(result, function(element,index) {return index});

            var joinedClasses = new Array();
            JQuery.each(result,function(key,value){
                JQuery.each(value,function(key1,value1){
                    if(i==0)
                    {
                        labels.push(key1);
                    }
                        if (joinedClasses[key1] === undefined)
                        {
                            joinedClasses[key1]= new Array();
                        }
                        joinedClasses[key1][key]=value1;// vaule define
                        });
                    i++;
                });
            i=0;
            JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
                var res = value.split("~~");
                var cityname=res[0];
                var citycode=res[1];

                 var container = $('#cblist');
                 var statecity = $('<input />', { type: 'checkbox',  class: 'chartstates_checkbox', checked: 'checked',id: 'chartstates_'+citycode, value: citycode }).appendTo(container);
                $('<label />', { 'for': 'check', text: cityname }).appendTo(container);

                /* var statecity = '';
                statecity = '<ul id ="variablesForGraph" class="list1">';
                statecity += "<li>"+
                    '<input type = "checkbox" class="chartstates_checkbox" '+
                                ' id="chartstates_' + citycode +'" value="'+ citycode+ '" checked/>'+
                    '<label for="check">'+ cityname+ "</label>"+
                     "</li>";
                statecity +='</ul>';

                $('#cblist').html(statecity);*/

                    var newres = Object.keys(joinedClasses[value]).map(function(k) {
                    return [k, joinedClasses[value][k]];
                    });

                var joinedClassesmain= [];
                var radius= [];
                $.each(newres, function (key1,value1) {


                    joinedClassesmain.push(value1[1]);
                    radius.push(value1[1]);

                });
               // var container1 = $('#variabelist');
                var variable1= [];
                var newhtml ='';
                newhtml = '<ul id ="variablesForGraph" class="list1">';

                $.each(newres, function (key1,value1) {
                        variable1=value1[0];

                         newhtml += "<li>"+
                    '<input type = "checkbox" class="chartvariables_checkbox" '+
                                ' id="chartvariables_' + variable1 +'" value="'+ variable1+ '" checked/>'+
                    '<label for="chartvariables_' + variable1 + '">'+ variable1+ "</label>"+
                     "</li>";


                    });


                newhtml +='</ul>';
                $('#variabelist').html(newhtml);

                datasetValues[citycode] = joinedClassesmain;
                datasets.push({
                        'label':cityname, //city name should come here
                        'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
                        'borderColor':colorcode[i],
                        'borderWidth':1,
                        'data':joinedClassesmain,

                        'id':citycode,
                        'id1':variable1


                    });
                i++;
            });

                chart1 = new Chart(ctx, {
                type: x,

                // The data for our dataset
                data: {
                     labels: keys,
                     datasets: datasets

                      },
                options: {
                     legend: {
                  display: false,
                },

                  scales: {
                  xAxes: [{
                    ticks: {
                      maxTicksLimit: 8,
                      fontSize:10
                    }
                  }]
                }
                }
                });

                 $('.chartvariables_checkbox').click();
                $('.chartvariables_checkbox:eq( 0 )').click();
                $('.chartvariables_checkbox:eq( 1 )').click();
                $('.chartvariables_checkbox:eq( 2 )').click();
                $('.chartvariables_checkbox:eq( 3 )').click();
                $('.chartvariables_checkbox:eq( 4 )').click();
            }
        });
}


function gettown_v2(val) {
    //$('.htitle').css({'width':'150px'});
    $('.distattrtypeselection').css({
        'display': 'none'
    });
    $('.districtspan').css({
        'display': 'none'
    });

    $('.urbanspan').css({
        'display': 'none'
    });
    if (val == "") {
        $('.districtspan').css({
            'display': 'block'
        });

        $('.urbanspan').css({
            'display': 'block'
        });
    }
    getAttribute_v2(8, "town");
}

function geturban_v2(val) {
    $('.htitle').css({
        'width': '150px'
    });
    $('.distattrtypeselection').css({
        'display': 'none'
    });
    if (val == "") {
        $('.districtspan').css({
            'display': 'block'
        });

        $('.townspan').css({
            'display': 'block'
        });
    }
    getAttribute_v2(8, "urban");
}

function getvalidation_v2() {
    var myurl = "";
    var zoom = 5;
    if (document.getElementById('state').value == '') {
        //alert(statealert);
        return false;
    } else {
        myurl = "state=";
        $('#state :selected').each(function(i, selected) {
            myurl += $(selected).val() + ",";
            zoom = 5;
        });

        myurl += "&district=";
        $('#district :selected').each(function(i, selected) {
            myurl += $(selected).val() + ",";
            zoom = 6;
        });
        myurl += "&attributes=";

        $('input.statetotal_attributes[type=checkbox]').each(function() {
            if (this.checked) {
                myurl += $(this).val() + ",";
            }
        });
        $("#zoom").val(zoom);
        //window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=showresults&Itemid=108";
        return false;
    }
}

function checkRadio_v2(frmName, rbGroupName) {
    var radios = document[frmName].elements[rbGroupName];
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            return true;
        }
    }
    return false;
}

function changeWorkspace(value) {
    //checkvalidation();

    if (value == 0) {
        $('.createneworkspace').css({
            'display': 'block'
        });
        $('.createnewworkspace').css({
            'display': ''
        });
        $('.thematiceditoption').css('display', 'block');
    } else {
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid=" + value + "&tmpl=component",
            method: 'GET',
            success: function(data) {
                result = $.parseJSON(data);

                $("#workspacceedit").load(location.href + " #workspacceedit>*", "");

                $('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
                    getAttribute_v2(5, "district");

                    if (typeof(preselected) != "undefined") {
                        displaycombo_v2();
                       // getVariableList_v2();
                    }
                });
                //$("#leftcontainer").load(location.href+" #leftcontainer>*","");
                $("#activeworkspacename").html(result[0].name);
                $("#workspacceedit").hide();
                $("#fade").hide();
                $("#default").show();
                //update form fields  as per selected workspace id
                setTimeout(function() {
                    //getDataNew();
                }, 1000);

                //event.preventDefault();
                //$("#result_table").html(data);
                //$("#workspacceedit").html(data);
                //console.log(data);
            }
        });
    }
}




$(".full-data-view").on("#submit", "click", function() {
    if ($("#comparedata").val() == 1) {
        var statecompare = $("#statecompare").val();
        var districtcompare = $("#districtcompare").val();
        var towncompare = $("#towncompare").val();
        var urbancompare = $("#urbancompare").val();

        if (urbancompare != "") {
            if (urbancompare == "all") {
                alert("All UA already Selected");
                return false;
            }
            var urban = $("#urban").val();
            if (urban == "") {
                alert("Please Select UA to compare");
                return false;
            } else if (urban == "all") {
                alert("You can not select All UA to compare");
                return false;
            }
        } else if (towncompare != "") {
            if (towncompare == "all") {
                alert("All Towns already Selected");
                return false;
            }
            var town = $("#town").val();
            if (town == "") {
                alert("Please Select Town to compare");
                return false;
            } else if (town == "all") {
                alert("You can not select All Town to compare");
                return false;
            }
        } else if (districtcompare != "") {
            if (districtcompare == "all") {
                alert("All Districts already Selected");
                return false;
            }
            var dist = $("#district").val();
            if (dist == "") {
                alert("Please Select District to compare");
                return false;
            } else if (dist == "all") {
                alert("You can not select All District to compare");
                return false;
            }
        } else if (statecompare != "") {
            if (statecompare == "all") {
                alert("All States already Selected");
                return false;
            }
            var stateval = $("#state").val();
            if (stateval == "") {
                alert("Please Select State to compare");
                return false;
            } else if (stateval == "all") {
                alert("You can not select All State to compare");
                return false;
            }
        }
        return true;

    } else {
        var resetmtype = 0;
        var checked = 0;
        var dataof = $("#dataof").val();
        var districtcompare = $("#district").val();
        if (dataof == "District") {
            if ($("#district").val() == "") {
                alert("Please Select District First");
                return false;
            }
        } else if (dataof == "UA") {
            if ($("#urban").val() == "") {
                alert("Please Select UA First");
                return false;
            }
        } else if (dataof == "Town") {
            if ($("#Town").val() == "") {
                alert("Please Select Town First");
                return false;
            }

        }

        $("input:checkbox:checked").each(function() {
            var chk = $(this).attr("checked");
            if (chk == "checked") {
                checked = 1;
            }
        });

        if (checked == 0) {
            alert("Please select variable first");
            return false;
        } else {
            if (typeof(districtcompare) == "undefined" || districtcompare == "") {
                //$("#m_type").remove();
                //alert($("#m_type").val());
                return true;
            }
        }
    }
});

function getMinmaxVariable_v2(value) {
    $(".minmaxdisplay").html("Please Wait...");
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getMinMax&value=" + encodeURIComponent(value),
        method: 'GET',
        success: function(data) {
            var segment = data.split(",");
            minval = segment[0];
            maxval = segment[1];
            $(".minmaxdisplay").html("<b>MIN :</b>" + segment[0] + "<b><br/>MAX :</b>" + segment[1] + "");
            $("#maxvalh").val(segment[1]);
            $("#minvalh").val(segment[0]);
        }
    });
}

function createTable_v2(edittheme) {
    var maxval = $("#maxvalh").val();
    var minval = $("#minvalh").val();
    var level = $("#level").val();
    var str = "";
    var diff = maxval - minval;
    var max = minval;
    var disp = 0;
    var setinterval = diff / ($("#no_of_interval").val());
    setinterval = setinterval.toFixed(2);
    start = minval;
    //alert(edittheme);
    if ($("#no_of_interval").val() > 5) {
        alert("Interval must be less then 5");
        return false;
    }
    var colorselected = "";
    if (edittheme != 1 && usedlevel.indexOf("0") == -1) {
        str += '<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
        $(".colorhide").css({
            "display": ""
        });
        colorselected = 1;
    } else if (edittheme == 1 && level == "0") {
        str += '<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
        $(".colorhide").css({
            "display": ""
        });
        colorselected = 1;
    } else {
        str += '<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Pin</th></tr></thead>';
        $(".colorhide").css({
            "display": "none"
        });
        colorselected = 0;
    }

    for (var i = 1; i <= $("#no_of_interval").val(); i++) {
        if ((edittheme != 1 && usedlevel.indexOf("0") == -1)) {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"></td>	</tr>';
            start = end;

        } else if (edittheme != 1 && usedlevel.indexOf("0") != -1 && level != "0") {

            if (usedlevel.indexOf("1") == -1) {

                level = 1;
                $("#level").val(1);
            } else {
                level = 2;
                $("#level").val(2);
            }

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"><img src="' + siteurl + '/components/com_mica/maps/img/layer' + level + '/pin' + i + '.png" /></td>	</tr>';
            start = end;

        } else if (edittheme == 1 && level != "0") {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"><img src="' + siteurl + '/components/com_mica/maps/img/layer' + level + '/pin' + i + '.png" /></td>	</tr>';
            start = end;

        } else if (edittheme == 1 && usedlevel.indexOf("0") != -1 && level == "0") {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"></td>	</tr>';
            start = end;

        } else {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ></tr>';
            start = end;

        }
    }

    $("#displayinterval").html(str);
   
    if (colorselected == 1) {
        $('.simpleColorChooser').click();
        //colorChooser();
    }
    //return str;
}

$('body').on('click', '.simpleColorChooser', function() {    
    var value = $('.simple_color').val();
    value = value.replace("#", "");
    var steps = $("#no_of_interval").val();

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getColorGradiant&value=" + encodeURIComponent(value) + "&steps=" + parseInt(steps++),
        method: 'GET',
        success: function(data) {
            colorselected = data;
            var segment = data.split(",");
            segment = segment.reverse();
            for (var i = 1; i <= segment.length; i++) {
                    var ncol = "#"+JQuery.trim(segment[i]);
                    JQuery("#color"+i).fadeIn(1000,JQuery("#color"+i).css({"background-color":ncol}));
                    /*$("#color" + (i)).fadeIn(1000, $("#color" + (i)).css({
                        "background-color": "#" + segment[i]
                    }));*/
            }
        }
    });
});


function getColorPallet_v2() {
	
    $('.simple_color').simpleColor({
        cellWidth: 9,
        cellHeight: 9,
        border: '1px solid #333333',
        buttonClass: 'colorpickerbutton'
    });
}

/*$(document).on("#savesld", "click", function() {
    var formula = $("#thematic_attribute").val();
    //var condition = $("#condition").val();
    var limit = $("#no_of_interval").val();
    var level = $("#level").val();
    if (formula == "") {
        alert("Please Select Variable");
        return false;
    } else if (limit == "") {
        alert("Please Select Interval");
        return false;
    }

    var from = "";
    var to = "";
    var color = "";
    for (var i = 1; i <= limit; i++) {
        from += $("#from" + i).val() + ",";
        to += $("#to" + i).val() + ",";

        if (level == "0" || level == "") {
            color += rgb2hex($("#color" + i).css("background-color")) + ",";
        } else {
            color += i + ",";
        }
    }

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.addthematicQueryToSession&formula=" + encodeURIComponent(formula) + "&from=" + from + "&to=" + to + "&color=" + color + "&level=" + level,
        method: 'GET',
        success: function(data) {
            window.location.href = window.location;
        }
    });
});*/

function saveSLD(){
    
    alert('call123');

    var formula     = $("#thematic_attribute").val();
    //var condition = JQuery("#condition").val();
    var limit       = $("#no_of_interval").val();
    var level       = $("#level").val();
    if(formula=="")
    {
        alert("Please Select Variable");
        return false;
    }
    else if(limit=="")
    {
        alert("Please Select Interval");
        return false;
    }

    var from  ="";
    var to    ="";
    var color ="";
    for(var i=1;i<=limit;i++)
    {
        from +=$("#from"+i).val()+",";
        to   +=$("#to"+i).val()+",";

        if(level=="0" || level=="")
        {
            color +=rgb2hex($("#color"+i).css("background-color"))+",";
        }
        else
        {
            color +=i+",";
        }
    }

    $.ajax({
        url     : "index.php?option=com_mica&task=showresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
        method  : 'GET',
        success : function(data){
            //window.location.href  =window.location;

            JQuery("#gis_script").load(" #gis_script > *");
            loadGis('thematic');

            /*$.ajax({
		        url     : "index.php?option=com_mica&task=showresults.setThematicQueryData",
		        method  : 'GET',
		        success : function(data){
		        	$("#blockcontent").load(" #blockcontent > *");
		        	$(".thematicData").html(data);
		        	$("#blockcontent").load(" #blockcontent > *");
		        	JQuery("#thematic_attribute").load(" #thematic_attribute > *");
		        }
		    });*/
            document.getElementById('light_thematic').style.display = 'none';
            document.getElementById('light2').style.display = 'none';
            document.getElementById('fade').style.display   = 'none';

		}
    });
}

$('body').on('click', 'a.deletegrp', function() {
    var level = $(this).attr("class");
    var getlevel = level.split(" ");
    var getdelid = $(this).attr("id");
    var segment = getdelid.split("del_");

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.deletethematicQueryToSession&formula=" + encodeURIComponent(segment[1]) + "&level=" + getlevel[1],
        method: 'GET',
        success: function(data) {
            //window.location = '';
            loadGis('thematic');
            document.getElementById('light_thematic').style.display = 'none';
        }
    });
});

function rgb2hex(rgb) {
    if (typeof(rgb) == "undefined") {
        return "ffffff";
    }

    if (rgb.search("rgb") == -1) {
        return rgb;
    } else {
        rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);

        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
}

$('body').on('click', 'a.edittheme', function() {
    document.getElementById('light_thematic').style.display = 'none';
    edittheme = 1;
    var cnt = $("#themeconent").html();

    $("#themeconent").html("");
    $(".themeconent").html(cnt);
    $('.simpleColorContainer').remove();

    getColorPallet_v2();

    var classname = $(this).attr("class");
    var myid = this.id;
    var getcount = classname.split(" ");
    var seg = myid.split("__");
    //$("#thematicquerypopup").click();
    $("#thematic_attribute").val(seg[0]);
    $("#level").val(seg[1]);

    $("#thematic_attribute").change();
    $("#no_of_interval").val(getcount[1]);
    createTable_v2(edittheme);
    $("#no_of_interval").val(getcount[1]);
    $(".range_" + seg[1]).each(function(i) {
        i++;
        rangeid = ($(this).attr("id"));
        var mylimit = rangeid.split("-");
        $("#from" + i).val(mylimit[0]);
        $("#to" + i).val(mylimit[1]);
    });

    var endcolor = "";
    if (seg[1] == "0") {
        for (var i = 1; i <= getcount[1]; i++) {
            $("#color" + (i)).css({
                "background-color": "" + $(".col_" + i).css("background-color")
            });
            if (getcount[1] == i) {
                $(".simpleColorDisplay").css({
                    "background-color": $(".col_" + i).css("background-color")
                });
            }
            //rangeid=$(".range"+i).attr("id");
        }
        //rangeid=$(".range"+i).attr("id");
    }
    //  $("#thematicquerypopup").click();
    thematicquerypopup_v2(edittheme);
});

/*$(document).on(".edittheme", "click", function() {
    alert('call');
    edittheme = 1;
    var cnt = $("#themeconent").html();

    $("#themeconent").html("");
    $(".themeconent").html(cnt);
    $('.simpleColorContainer').remove();

    getColorPallet_v2();

    var classname = $(this).attr("class");
    var myid = this.id;
    var getcount = classname.split(" ");
    var seg = myid.split("__");
    //$("#thematicquerypopup").click();
    $("#thematic_attribute").val(seg[0]);
    $("#level").val(seg[1]);

    $("#thematic_attribute").change();
    $("#no_of_interval").val(getcount[1]);
    createTable(edittheme);
    $("#no_of_interval").val(getcount[1]);
    $(".range_" + seg[1]).each(function(i) {
        i++;
        rangeid = ($(this).attr("id"));
        var mylimit = rangeid.split("-");
        $("#from" + i).val(mylimit[0]);
        $("#to" + i).val(mylimit[1]);
    });

    var endcolor = "";
    if (seg[1] == "0") {
        for (var i = 1; i <= getcount[1]; i++) {
            $("#color" + (i)).css({
                "background-color": "" + $(".col_" + i).css("background-color")
            });
            if (getcount[1] == i) {
                $(".simpleColorDisplay").css({
                    "background-color": $(".col_" + i).css("background-color")
                });
            }
            //rangeid=$(".range"+i).attr("id");
        }
        //rangeid=$(".range"+i).attr("id");
    }
    //	$("#thematicquerypopup").click();
    thematicquerypopup_v2();
});*/



function thematicquerypopup_v2(edittheme) {
    
   alert(edittheme);
   if (totalthemecount == 3 && (typeof(edittheme) == "undefined" || (edittheme) == 0)) {
        alert("You Can Select maximum 3 Thematic Query for Single Workspace");
        return false;
    } else {
        //document.getElementById('light2').style.display='block';
        //document.getElementById('fade').style.display='block';
        $("#light2").fadeIn();
        $("#fade").fadeIn();
    }
}

 function getindlvlgrpvar()
    {
        var groupid = $('input[name=indlvlgrps]:checked').val();

        $.ajax({
            url: "index.php?option=com_mica&task=micafront.getIndLvlGrp",
            type: 'POST',
            data: "groupid=" + groupid,
            success: function(data, textStatus, xhr) {
                var data = $.parseJSON(data);
                $('.variable_checkbox').each(function(index, el) {
                    if ($('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"))) {
                        $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked", false));
                    }
                });

                $('.type_checkbox').each(function(index, el) {
                    if ($('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"))) {
                        $('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked", false));
                    }
                });

                if (data['variables'] != 'false') {
                    $('.variable_checkbox').each(function(index, el)
                    {
                        if ($.inArray($(this).val(), data['variables']) !== -1) {
                            $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
                        }
                    });
                    $('.variable_checkbox').val(data['variables']);
                }

                if (data['type'] != 'false')
                {
                    $('.type_checkbox').each(function(index, el) {
                        if ($.inArray($(this).val(), data['type']) !== -1)
                        {
                            $('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
                        }
                    });
                    $('.type_checkbox').val(data['type']);
                }
                checkvalidation();
            }
        });
    }

$(document).on("click", "#updatecustom", function() {
    var new_name = $('#new_name').val();
    var oldattrval = $('#oldattrval').val();
    //alert(new_name);
    //alert(oldattrval);
    if ($("#new_name").val() == "") {
        alert("Please Select Custom Variable First!!");
        return false;
    }

    if (!validateFormula_v2()) {
        return false;
    }

    var attributevale = $('textarea#custom_attribute').text();
    //alert(attributevale);
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.updateCustomAttr&attrname=" + new_name + "&attributevale=" + encodeURIComponent(attributevale) + "&oldattrval=" + encodeURIComponent(oldattrval),
        method: 'GET',
        success: function(data) {

            getAttribute_v2();
            $("#light").hide();

            getDataNew();

        }
    });
});

$(".full-data-view").on("#deletevariable", "click", function() {
    var new_name = $('.customedit').find(":selected").text();
    var attributevale = $('textarea#custom_attribute').text();
    if (new_name == "" && attributevale == "") {
        alert("Please Select Custom Variable");
        return false;
    }

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.deleteCustomAttr&attrname=" + new_name + "&attributevale=" + attributevale,
        method: 'GET',
        success: function(data) {
            //alert(data);
            window.location = 'index.php?option=com_mica&view=showresults&Itemid=108';
            //window.location='index.php?option=com_mica&view=showresults&Itemid=108';
        }
    });
});

function closePopup_v2() {
    edittheme = 0;
    document.getElementById('light2').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    $("#displayinterval").html("");

    //$("#thematic_attribute").val(0).attr("selected");
    document.getElementById("thematic_attribute").selectedIndex = 0;
    $("#no_of_interval").val("");
    $(".minmaxdisplay").html("");
    if ($(".themeconent").html().length > 10) {
        $("#themeconent").html($(".themeconent").html());
        $('.simpleColorContainer').remove();
        getColorPallet_v2();
        $(".simpleColorDisplay").css({
            "background-color": "#FFFFCC"
        });
    }
}

function validateFormula_v2() {
    var assignformula = "";
    var formula = $('textarea#custom_attribute').text();

    $("#avail_attribute option").each(function() {
        //alert($(this).val());
        //alert(formula);
        var myclass = $(this).parent().attr("class");
        if ($(this).val() == formula.trim() && myclass != "defaultvariablelib") {
            assignformula = "You have already assign '" + formula + "' value to '" + $(this).val() + "' Variable";
        }
    });

    if (assignformula != "") {
        alert(assignformula);
        return false;
    }
    return true;
}

function downloadMapPdf_v2() {
    //alert(tomcaturl);
    var start = map.layers;
    var selectedlayer = "";

    if (javazoom == 5 || javazoom == 6) {
        selectedlayer = "india:rail_state";
    } else if (javazoom == 7) {
        selectedlayer = "india:india_information";
    } else if (javazoom == 8) {
        selectedlayer = "india:jos_mica_urban_agglomeration";
    } else {
        selectedlayer = "india:my_table";
    }

    var baselayer = "india:rail_state";
    var buildrequest = "";
    var buildrequest1 = "";
    for (x in start) {
        for (y in start[x].params) {
            if (start[x].params.LAYER == selectedlayer) {
                if (y == "FORMAT") {
                    buildrequest1 += (y + "EQT" + encodeURIComponent(start[x].params[y])) + "AND";
                } else {
                    buildrequest += (y + "EQT" + encodeURIComponent(start[x].params[y])) + "AND";
                }
            }
        }
    }

    buildrequest = buildrequest + "FORMATEQTimage/png";
    buildrequest1 = buildrequest1 + "FORMATEQTimage/png";
    //alert(tomcaturl+"?"+buildrequest+"&BBOX="+map.getExtent().toBBOX()+"&WIDTH=800&HEIGHT=600");
    var finalurl = buildrequest + encodeURIComponent("ANDBBOXEQT" + map.getExtent().toBBOX() + "ANDWIDTHEQT925ANDHEIGHTEQT650");
    var finalurl1 = buildrequest1 + encodeURIComponent("ANDBBOXEQT" + map.getExtent().toBBOX() + "ANDWIDTHEQT925ANDHEIGHTEQT650");
    window.open("index.php?option=com_mica&task=showresults.exportMap&mapparameter=" + finalurl + "&baselayer=" + finalurl1);
}

function exportImage_v2(name) {
    //alert(name);
    // exporting
    var store = '0'; // '1' to store the image on the server, '0' to output on browser
    //var name      = '001'; // name of the image
    var imgtype = 'jpeg'; // choose among 'png', 'jpeg', 'jpg', 'gif'
    var opt = 'index.php?option=com_mica&task=showresults.amExport&store=' + store + '&name=' + name + '&imgtype=' + imgtype;
    var flashMovie = document.getElementById('chartdiv');
    // previoushtml =$("#exportchartbutton").html();//"Please Wait..."

    $("#exportchartbutton").html("Please Wait..."); //"Please Wait..."

    data = flashMovie.exportImage_v2(opt);
    flashMovie.amReturnImageData_v2("chartdiv", data);
}

function amReturnImageData_v2(chartidm, data) {
    var onclick = "exportImage($('#chartype option:selected').text())";
    var button = '<input type="button" name="downloadchart" onclick="' + onclick + '" class="button" value="Export">';
    var previoushtml = $("#exportchartbutton").html(button);
}

$(".full-data-view").on(".variablegrp", "click", function() {
    var myid = $(this).attr("id");
    var dis = $('.' + myid).css("display");

    $(".hideothers").css({
        "display": "none"
    });
    $('.variablegrp').addClass("deactive");
    $(this).addClass("active");
    $(this).removeClass("deactive");
    if (dis == "block") {
        $('.' + myid).css("display", "none");
        $(this).addClass("deactive");
        $(this).removeClass("active");
    } else {
        $('.' + myid).css("display", "block");
        $(this).addClass("active");
        $(this).removeClass("deactive");
    }
});

function preloader_v2() {
    var preloaderstr = "<div id='facebook' ><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><div id='block_4' class='facebook_block'></div><div id='block_5' class='facebook_block'></div><div id='block_6' class='facebook_block'></div></div>";
}
$(".full-data-view").on(".hovertext", "mouseover", function(e) {
    var allclass = $(this).attr("class");
    var segment = allclass.replace("hovertext ", "");
    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;
    var popuphtml = "<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>" + segment + "</div>";
    $(this).append(popuphtml);
});
$(".full-data-view").on(".hovertext", "click", function(e) {
    $(this).parent().prev().find("input").prop("checked", true); //,true);
});

$(".full-data-view").on(".hovertext", "mouseout", function(e) {
    $("#popupattr").remove();
});

$("#tabvariables").on(".customvariablecheckbox", "click", function() {

    var check = ($(this).attr("checked"));

    if (check) {
        $("#default1").show();
        document.getElementById("applyChangesInitial").style.pointerEvents = "auto";
        document.getElementById("applyChangesInitial").style.cursor = "pointer";
        $("#light").hide();


    } else {
        /*alert("not clicked");
		return;*/
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteattribute&attr=" + $(this).attr("id"),
            method: 'GET',
            success: function(data) {

                getAttribute_v2(5);
                $("#default1").show();
                $("#light").hide();


            }
        });

        //window.location = "index.php?option=com_mica&task=showresults.deleteattribute&attr="+$(this).attr("id");
    }
});
$(document).ready(function() {
    $("li #menu100").parent().attr("class", "active");
});

function toggleCustomAction_v2(ele, action) {

    if (action != "") {
        //$(ele).prev().attr("checked",action);
        $(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("checked", action);
    } else {
        //$(ele).prev().removeAttr("checked");
        $(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').removeAttr("checked");
    }
    //$(ele).prev().click();
    $(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').click();
}

$(document).on("click", ".customedit", function() {

    $('#save').attr("id", "updatecustom");
    $('#updatecustom').attr("onclick", "javascript:void(0)");
    $('#updatecustom').attr("value", "Update");

    $('#new_name').val($(this).attr("id"));
    $('#new_name').attr('disabled', true);
    $('.aa').html("Edit");

    oldattrval = $(this).attr("value");
    $('#oldattrval').val(oldattrval);
    $('textarea#custom_attribute').text($(this).attr("value"));
    lastchar = '';
    //moverightarea();
    //$('#new_name').val($(this).prev().attr("id"));
    //$('#custom_attribute').val($(this).prev().val());

    $('#menu1').removeClass('in active');
    $('#menu2').addClass('in active');
    $('#custom_manage').removeClass('active');
    $('#custom_edit').addClass('active');
    $('#custom_edit a').text('Edit');
    $('#menu2 .poptitle').text('Edit');
    $('#light').css('display','block');
    $('#custom_manage a').removeAttr("href");
    $('#customlist a').removeAttr("href");
    $('.custom_edit_button').removeClass('col-md-6').addClass('col-md-4');
    r= $('<input type="button" name="cancel" id="cancel" class="btn btn-danger custom_cancel_btn" value="Cancel" >');
        $(".custom_cancel").append(r);

});

$(document).on("click", ".custom_cancel_btn" , function(){
    $('#custom_manage a').attr("href", "#menu1");
    $('#customlist a').attr("href", "#home");
    $('#menu2').removeClass('in active');
    $('#menu1').addClass('in active');
    $('#custom_edit').removeClass('active');
    $('#custom_manage').addClass('active');
    $('#custom_edit a').text('Add New');
    $('#menu2 .poptitle').text('Add New');
    $('#light').css('display','block');
    $('.custom_edit_button').removeClass('col-md-4').addClass('col-md-6');
    $('#cancel').remove();

});

$(".full-data-view").on("#closeextra", "click", function() {
    $('#updatecustom').attr("id", "save");
    $('#save').attr("onclick", "checkfilledvalue();");
    $('#save').attr("value", "Save");
    $('#new_name').val("");
    $('#new_name').attr('disabled', false);
    $('.aa').html("Add New");
});

$(document).on("click", ".deletecustomvariable", function() {

    var names = new Array();
    var i = 0;
    $(".dcv_checkbox:checked").each(function(i) {
        names.push($(this).attr("id"));
    });
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.deleteCustomVariableFromLib&attrname=" + names,
        type: 'GET',
        success: function(data) {
            getAttribute_v2(5);
            $("#light").hide();
            getDataNew();
        }
    });

    //window.location="index.php?option=com_mica&task=showresults.deleteCustomVariableFromLib&attrname="+names
});

$(".full-data-view").on("#fullscreen #fullscreen1", "click", function() {
    var fromthematic = $("#fullscreen").attr("fromthematic");
    /*$("#map").addClass("fullscreen");
	$(this).css({"display":"none"});
	$("#fullscreenoff").fadeIn();
	$("#map").css({"width":$(window).width()-20,"height":$(window).height()-20});
	map.updateSize();*/

    if (fromthematic == 1) {
        popped = open(siteurl + 'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
    } else {
        popped = open(siteurl + 'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component', 'MapWin');
    }
    popped.document.body.innerHTML = "<div id='map' style='height:" + $(window).height() + "px;width:" + $(window).width() + "px;'></div>";
});

$(".full-data-view").on(".fullscreeniconoff", "click", function() {
    $("#fullscreentable").removeClass("fullscreentable");
    $("#matrixclose").remove();
    $(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");

    $(this).removeClass("fullscreeniconoff");
    $(this).addClass("fullscreenicon");
    $("#fade").css({
        "display": "none"
    });
    $("#tablescroll").removeAttr("width");
    $("#tablescroll").removeAttr("overflow");
    $("#tablescroll").removeAttr("height");
});

$(".full-data-view").on(".fullscreenicon", "click", function() {

    $("#fullscreentable").addClass("fullscreentable");
    $(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'></td></tr></table>");
    $(this).addClass("fullscreeniconoff");
    $(this).removeClass("fullscreenicon");
    var toappend = '<div class="divclose" id="matrixclose" style="text-align:right;"><a href="javascript:void(0);" onclick="$(\'.fullscreeniconoff\').click();"><img src="' + siteurl + '/media/system/images/closebox.jpeg" alt="X"></a></div>';
    var html = $("#fullscreentable").html();
    $("#fullscreentable").html(toappend + "<div id='tablescroll'>" + html + "</div>");
    $("#fadefade").css({
        "display": "block"
    });
    $("#tablescroll").css({
        "width": "100%"
    });
    $("#tablescroll").css({
        "height": "100%"
    });
    $("#tablescroll").css({
        "overflow": "auto"
    });
});

//add by ujash
function pmdatalist() {

    var selection = $(".filterspeed").val();

    filterspeed_radio = selection;
    if (selection == "0") {
        $("#speed_variable").attr("multiple", "true");
        $("#speed_variable").removeClass("inputbox");

        $("#speed_variable").removeClass("chzn-done");
        $("#speed_region").removeClass("chzn-done");

        $("#speed_region").removeAttr("multiple");
        $("#speed_variable_chzn").remove();
        $("#speed_region_chzn").remove();
        //choosenjq("#speed_region").chosen({max_selected_options: 1});
        //choosenjq("#speed_variable").chosen({max_selected_options: 5});
        $("#speed_region").addClass("inputbox");
    } else {
        $("#speed_variable").removeAttr("multiple");
        $("#speed_variable").addClass("inputbox");
        $("#speed_region").attr("multiple", "true");

        $("#speed_variable").removeClass("chzn-done");
        $("#speed_region").removeClass("chzn-done");

        $("#speed_variable_chzn").remove();
        $("#speed_region_chzn").remove();

        $("#speed_region").removeClass("inputbox");
        //choosenjq("#speed_region").chosen({max_selected_options: 5});
        //choosenjq("#speed_variable").chosen({max_selected_options: 1});
    }

    $(".speed").css({
        "display": "block"
    });
}

/*$(".full-data-view").on(".filterspeed", "click", function() {

    var selection = $(this).val();

    filterspeed_radio = selection;
    if (selection == "0") {
        $("#speed_variable").attr("multiple", "true");
        $("#speed_variable").removeClass("inputbox");

        $("#speed_variable").removeClass("chzn-done");
        $("#speed_region").removeClass("chzn-done");

        $("#speed_region").removeAttr("multiple");
        $("#speed_variable_chzn").remove();
        $("#speed_region_chzn").remove();
        //choosenjq("#speed_region").chosen({max_selected_options: 1});
        //choosenjq("#speed_variable").chosen({max_selected_options: 5});
        $("#speed_region").addClass("inputbox");
    } else {
        $("#speed_variable").removeAttr("multiple");
        $("#speed_variable").addClass("inputbox");
        $("#speed_region").attr("multiple", "true");

        $("#speed_variable").removeClass("chzn-done");
        $("#speed_region").removeClass("chzn-done");

        $("#speed_variable_chzn").remove();
        $("#speed_region_chzn").remove();

        $("#speed_region").removeClass("inputbox");
        //choosenjq("#speed_region").chosen({max_selected_options: 5});
        //choosenjq("#speed_variable").chosen({max_selected_options: 1});
    }

    $(".speed").css({
        "display": "block"
    });
    //choosenjq("#speed_variable").val('').trigger("liszt:updated");
    //choosenjq("#speed_region").val('').trigger("liszt:updated");
});*/

/*function test() {

    var speedvar = new Array();
    var speedregion = new Array();
    if (typeof($("#speed_variable").attr("multiple")) == "string") {
        $("#speed_variable").each(function() {
            speedvar.push(encodeURIComponent($(this).val()));
        });
        speedregion.push(encodeURIComponent($("#speed_region").val()));
    } else {
        speedvar.push(encodeURIComponent($("#speed_variable").val()));
        $("#speed_region").each(function() {
            speedregion.push(encodeURIComponent($(this).val()));
        });
    }
    //var filterspeed = $('input:radio[name=filter]:checked]').val();

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getsmeter&region=" + speedregion + "&speedvar=" + speedvar + "&filterspeed=" + filterspeed_radio,
        type: 'GET',
        success: function(data) {
            $("#spedometer_region").html("");
            $("#spedometer_region").html(data);
            initspeed();
            $("#speedfiltershow").css({
                "display": "block"
            });
            $(".sfilter").css({
                "display": "none"
            });
            $("#spedometer_region").css({
                "width": "915px"
            });
            $("#spedometer_region").css({
                "overflow": "auto"
            });

            }
    });
}*/

$(".full-data-view").on("#showspeed", "click", function() {
    var speedvar = new Array();
    var speedregion = new Array();
    if (typeof($("#speed_variable").attr("multiple")) == "string") {
        $("#speed_variable").each(function() {
            speedvar.push(encodeURIComponent($(this).val()));
        });
        speedregion.push(encodeURIComponent($("#speed_region").val()));
    } else {
        speedvar.push(encodeURIComponent($("#speed_variable").val()));
        $("#speed_region").each(function() {
            speedregion.push(encodeURIComponent($(this).val()));
        });
    }
    //var filterspeed = $('input:radio[name=filter]:checked]').val();

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getsmeter&region=" + speedregion + "&speedvar=" + speedvar + "&filterspeed=" + filterspeed_radio,
        type: 'GET',
        success: function(data) {
            $("#spedometer_region").html("");
            $("#spedometer_region").html(data);
            initspeed();
            $("#speedfiltershow").css({
                "display": "block"
            });
            $(".sfilter").css({
                "display": "none"
            });
            $("#spedometer_region").css({
                "width": "915px"
            });
            $("#spedometer_region").css({
                "overflow": "auto"
            });



        }
    });
});



$(".full-data-view").on("#downmatrix", "click", function() {
    window.location.href = "index.php?option=com_mica&task=showresults.downloadMatrix&rand=" + Math.floor(Math.random() * 100000);
});



$(".full-data-view").on("#speedometer", "click", function() {
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.speedometer",
        method: 'POST',
        success: function(data) {
            $("#speed_region").html(data);
        }
    });
});

// $(document).ready(function(){
// 	var graphdist = $("#district").val();

// 	$.ajax({
// 		url     : "index.php?option=com_mica&task=showresults.getDistrictlist",
// 		type    : 'POST',
// 		data    : "dist="+graphdist,
// 		success : function(data){
// 			$("#selectdistrictgraph").html(data);
// 		}
// 	});
// });

$(".full-data-view").on("#showchart", "click", function() {
    var checkedelements = new Array();
    var checkeddist = new Array();
    var checkevar = new Array();
    $("#selectdistrictgraph").find(".districtchecked").each(function() {
        if ($(this).attr("checked")) {
            checkedelements.push(this.val);
            checkeddist.push($(this).val());
        }
    });

    $("#light1007").find(".variablechecked").each(function() {
        if ($(this).attr("checked")) {
            checkedelements.push($(this).val());
            checkevar.push($(this).val());
        }
    });

    if (checkedelements.length > 15) {
        alert("Variable + District Total Should be less then 15");
        return false;
    }

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getGraph",
        type: 'POST',
        data: "dist=" + checkeddist + "&attr=" + checkevar,
        success: function(data) {
            var segments = data.split("<->");
            datastr = segments[0];
            grpsetting = segments[1];
            $("#chartype").change();
        }
    });
});

// $(document).ready(function(){
//     $("#summary").click(function(){
//         $("#summary_data").toggle();
//         $("#summary").toggleClass('active');
//     });
// });

/*$('.actionbtn').click(function(event) {
		/* Act on the event */


/*switch ($(this).attr('id')) {
			case "tabledata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.text').show();
				break;
			case "graphdata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.graphs').show();
				break;
			case "gisdata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.gis').show();
				break;
			case "matrix":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.matrix').show();
				break;
			case "pmdata":
				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.speedometer').show();
				break;
			case "comparedata":
				$('#result_table').find('.contenttoggle2').hide();
				$('#result_table').find('.contenttoggle2.compare').show();
				break;
			case "summary":

				$('#result_table').find('.contenttoggle1').hide();
				$('#result_table').find('.contenttoggle1.summary').show();
			break;

			default:
				// statements_def
				break;
		};

	});*/



function getRemove_v2(cls, a) {
    alert(cls);
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.deleteattribute&attr=" + a + "&chkcls=" + cls,
        method: 'GET',
        async: true,
        success: function(data) {
            if (a == "MPI") {
                $(".mpi_checkbox").prop("checked", false);
            } else {
                value = a.replace(/ /g, "_");
                $(".variable_checkbox[value='" + a + "']").prop("checked", false);
                $(".swcs_checkbox[value=Rural_Score_" + value + "]").prop("checked", false);
                $(".swcs_checkbox[value=Urban_Score_" + value + "]").prop("checked", false);
                $(".swcs_checkbox[value=Total_Score_" + value + "]").prop("checked", false);
            }
            //getData();
            getDataNew();
        }
    });
}

function displaycombo_v2(value) {

    if ($("#state").val() == "all") {
        alert("Please Select State First");
        document.getElementById('dataof').selectedIndex = 0;
        document.getElementById("district").selectedIndex = 0;
        return false;
    } else {
        
        var allVals = [];
        var slvals = [];
        $('.state_checkbox:checked').each(function() {
            slvals.push($(this).val())
        })
        selected = slvals.join(',');
        getfrontdistrict_v2(selected);
    }

    if (value == "District") {
        $(".districtspan").css({
            "display": "block"
        });
        $(".townspan").css({
            "display": "none"
        });
        $(".urbanspan").css({
            "display": "none"
        });
    }
    /*else if(value=="Town"){
    			$(".districtspan").css({"display":"none"});
    			$(".townspan").css({"display":"block"});
    			$(".urbanspan").css({"display":"none"});
    			$(".distattrtypeselection").css({"display":"none"});
    			document.getElementById("district").selectedIndex=0;
    			document.getElementById('m_type').selectedIndex=0;
    			$(".distattrtypeselection").css({"display":"none"});
    		}else if(value=="UA"){
    			$(".districtspan").css({"display":"none"});
    			$(".townspan").css({"display":"none"});
    			document.getElementById('m_type').selectedIndex=0;
    			$(".distattrtypeselection").css({"display":"none"});
    			$(".urbanspan").css({"display":"block"});
    			document.getElementById("district").selectedIndex=0;
    		}else{
    			$(".districtspan").css({"display":"none"});
    			$(".townspan").css({"display":"none"});
    			$(".urbanspan").css({"display":"none"});
    			$(".distattrtypeselection").css({"display":"none"});
    			document.getElementById('m_type').selectedIndex=0;
    		}*/
}


function sortListDir(id) {

    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list = document.getElementById(id);
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";
    //Make a loop that will continue until no switching has been done:
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        b = list.getElementsByTagName("LI");
        text =list.getElementsByTagName("label");

        //Loop through all list-items:
        for (i = 0; i < (text.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*check if the next item should switch place with the current item,
				    based on the sorting direction (asc or desc):*/

            if (dir == "asc") {
                if (text[i].innerHTML.toLowerCase() > text[i + 1].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically lower than current item,
				          mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (text[i].innerHTML.toLowerCase() < text[i + 1].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically higher than current item,
				          mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
				      and mark that a switch has been done:*/
            b[i].parentNode.insertBefore(b[i + 1], b[i]);
            switching = true;
            //Each time a switch is done, increase switchcount by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
				      set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

function variable_sortListDir() {

    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list = $('.variablelist');
    switching = true;
    dir = "desc";

        header =$('.variable_label');
        for(z=0; z<(header.length); z++)
           {
               var inner_class= $(header[z]).text();
                inner_class = inner_class.split(' ').join('_');
                inner_class = inner_class.toLowerCase();
                switching = true;
                dir = dir;

    while (switching) {

        switching = false;

        a = $('.'+inner_class);


        for (i = 0; i < (a.length - 2); i++) {
            shouldSwitch = false;

            if (dir == "asc") {
                if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {

                if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {

                    var other = $(a[i]);
                    $(a[i+1]).after(other.clone());
                    other.after($(a[i+1])).remove();
                    switching = true;

            switchcount++;
        } else {

            if (switchcount == 0 && dir == "desc") {
                dir = "asc";
                switching = true;
            }
        }
    }
    }
}

function swsc_sortListDir() {

    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list = $('.variablelist');
    switching = true;
    dir1 = "asc";

        header =$('.swsc_label');
        for(z=0; z<(header.length); z++)
           {
               var inner_class= $(header[z]).text();
                inner_class = inner_class.split(' ').join('__');
                inner_class = inner_class.toLowerCase();
                switching = true;
                dir1 = dir1;

    while (switching) {

        switching = false;

        a = $('.'+inner_class);


        for (i = 0; i < (a.length - 1); i++) {
            shouldSwitch = false;

            if (dir1 == "asc") {
                if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

                    shouldSwitch = true;
                    break;
                }
            } else if (dir1 == "desc") {

                if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {

                    var other = $(a[i]);
                    $(a[i+1]).after(other.clone());
                    other.after($(a[i+1])).remove();
                    switching = true;

            switchcount++;
        } else {

            if (switchcount == 0 && dir1 == "asc") {
                dir1 = "desc";
                switching = true;
            }
        }
    }
    }
}

function district_sortListDir() {

    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list = $('.variablelist');
    switching = true;
    dir2 = "desc";

        header =$('.district_label');
        for(z=0; z<(header.length); z++)
           {
               var inner_class= $(header[z]).text();
                inner_class = inner_class.split(' ').join('_');
                inner_class = inner_class.split('&').join('_');
                inner_class = inner_class.toLowerCase();
                switching = true;
                dir2 = dir2;

    while (switching) {

        switching = false;

        a = $('.'+inner_class);


        for (i = 0; i < (a.length - 1); i++) {
            shouldSwitch = false;

            if (dir2 == "asc") {
                if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

                    shouldSwitch = true;
                    break;
                }
            } else if (dir2 == "desc") {

                if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {

                    var other = $(a[i]);
                    $(a[i+1]).after(other.clone());
                    other.after($(a[i+1])).remove();
                    switching = true;

            switchcount++;
        } else {

            if (switchcount == 0 && dir2 == "desc") {
                dir2 = "asc";
                switching = true;
            }
        }
    }
    }
}

function getvalidation(num) {
    //alert('hdfdfd');

    var myurl = "";
    myurl = "state=";
    myurl += $("#state").val();
    myurl += "&district=";
    myurl += $("#district").val();
    myurl += "&attributes=";
    var attr = "";
    $('#result_table').find('.statetotal_attributes' + num).each(function(key, value) {
        $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
    });
    getData();
    return true;
}




$(document).keyup(function(e) {
    $
    if (e.keyCode == "27") {
        document.getElementById('light').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
    }
});

var is_updatecustom = 0;


$(".full-data-view").on(".deleterow", "click", function() {

    var value = $(this).val();
    var ans = confirm("Are you Sure to delete Variable " + value);
    if (ans == 1) {
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteattribute&attr=" + value,
            type: 'GET',
            success: function(data) {
                $("#" + value).fadeOut();
                window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
            }
        });
    }
});

$(".full-data-view").on(".deletecustom", "click", function() {
    var value = $(this).val();
    var ans = confirm("Are you Sure to delete Custom Variable " + value);
    if (ans == 1) {
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteCustomAttribute&attr=" + value,
            type: 'GET',
            success: function(data) {
                $("#" + value).fadeOut();
                window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
            }
        });
    }
});

$(".full-data-view").on(".removecolumn", "click", function() {
    var value = $(this).val();
    //var classname = $(this).parent().attr('class');
    var segment = value.split("`");
    var ans = confirm("Are you Sure to delete  " + segment[2]);
    if (ans == 1) {
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteVariable&table=" + segment[0] + "&value=" + segment[1],
            type: 'GET',
            success: function(data) {
                //$(this).fadeOut();
                window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
            }
        });
    }
});
var compare = 1;

//$(document).ready(function()

$(".full-data-view").on("#district_tab", "click", function() {
    $(document).ready(function() {
        $('.slider').slick({
            dots: false,
            vertical: true,
            slidesToShow: 11,
            slidesToScroll: 1,
            verticalSwiping: true,
            arrrow: true,
        });
    });
});



$(document).ready(function() {
    $(".contenttoggle").css({
        "display": "none"
    });
    //var init=$(".active").attr("id");
    //$("."+init).css({"display":"block"});

    $("#result_table").on(".toggle", "click", function() {

        var myclass = $(this).attr("class");
        $(".toggle").css({
            "border-right": "1px solid #0154A1"
        });
        $(this).prev("span").css({
            "border-right": "1px solid white"
        });
        //$(this).next("span").css({"border-right":"1px solid white"});
        var myclasssplit = myclass.split(" ");
        if (typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active") {
            $(".toggle").removeClass("active");
            $(".contenttoggle").slideUp();
            return false;
        }
        //$(".toggle").css({"border-right":"1px solid white"});
        var myid = $(this).attr("id");
        $(".toggle").removeClass("active");
        $(this).addClass("active");
        $(".contenttoggle").slideUp();
        $("." + myid).slideDown();
    });

    $("#result_table").on(".toggle1", "click", function() {
        $(".toggle1").css({
            "border-right": "1px solid #0154A1"
        });
        $(this).prev("span").css({
            "border-right": "none"
        });
        //$(this).next("span").css({"border-right":"none"});
        $(".toggle1").last("span").css({
            "border-right": "white"
        })
        var myclass = $(this).attr("class");
        var myclasssplit = myclass.split(" ");

        if (typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active") {
            $(".toggle1").removeClass("active");
            $(".contenttoggle1").slideUp();
            $(".toggle1").css({
                "border-right": "1px solid #0154A1"
            });
            $(".toggle1").last("span").css({
                "border-right": "white"
            })
            return false;
        }
        var myid = $(this).attr("id");
        $(".toggle1").removeClass("active");
        $(this).addClass("active");
        $(this).css({
            "border-right": "1px solid #0154A1"
        });
        $(".contenttoggle1").slideUp();
        $("." + myid).slideDown();
        $.cookie("tab", myid);
        if (myid == "text") {
            $('#jsscrollss').not(".firsttableheader,.secondtableheader,.thirdtableheader").jScrollPane({
                verticalArrowPositions: 'os',
                horizontalArrowPositions: 'os'
            });
        }
    });

    $("#result_table").on("click",".toggle2",  function() {
        var myclass = $(this).attr("class");
        var myclasssplit = myclass.split(" ");
        if (typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active") {
            $(".toggle2").removeClass("active");
            $(".contenttoggle2").css({
                "display": "none"
            });
            return false;
        }
        var myid = $(this).attr("id");

        $(".toggle2").removeClass("active");
        $(this).addClass("active");
        $(".contenttoggle2").css({
            "display": "none"
        });
        $("." + myid).css({
            "display": "block"
        });
    });
});

function getindlvlgrpvar() {
    //var groupid = $('select#indlvlgrps').val();

    var groupid = $('input[name=indlvlgrps]:checked').val();

    $.ajax({
        url: "index.php?option=com_mica&task=micafront.getIndLvlGrp",
        type: 'POST',
        data: "groupid=" + groupid,
        success: function(data, textStatus, xhr) {
            var data = $.parseJSON(data);
            $('.variable_checkbox').each(function(index, el) {
                if ($('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"))) {
                    /*$('#ui-multiselect-attributes-option-'+index).attr('selected', false);
						$('#ui-multiselect-attributes-option-'+index).click();*/

                    $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked", false));
                }
            });

            $('.type_checkbox').each(function(index, el) {
                if ($('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"))) {
                    /*$('#ui-multiselect-m_type-option-'+index).attr('selected', false);
						$('#ui-multiselect-m_type-option-'+index).click();*/

                    $('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked", false));
                }
            });
            if (data['variables'] != 'false') {
                $('.variable_checkbox').each(function(index, el) {

                    if ($.inArray($(this).val(), data['variables']) !== -1) {
                        /*$(this).attr('selected');
			                $('#variable_check_'+index).attr('aria-selected', true);
			                $('#variable_check_'+index).attr('selected', true);
							$('#variable_check_'+index).click();*/

                        $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
                    }
                });
                $('.variable_checkbox').val(data['variables']);
            }
            if (data['type'] != 'false') {
                $('.type_checkbox').each(function(index, el) {
                    if ($.inArray($(this).val(), data['type']) !== -1) {
                        /*$(this).attr('selected');
			                $('#ui-multiselect-m_type-option-'+index).attr('aria-selected', true);
			                $('#ui-multiselect-m_type-option-'+index).attr('selected', true);
							$('#ui-multiselect-m_type-option-'+index).click();*/

                        $('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
                    }
                });
                $('.type_checkbox').val(data['type']);
            }
        }
    });
}

// edited by salim STARTED 25-10-2018
function getDataNew()
{
    var data = $('#micaform').serialize();
    $.ajax({
            //url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
            url: "index.php?option=com_mica&task=showresults.getDataajax",
            method: 'POST',
            async: false,
            data: data,
            beforeSend: function() {
                $('#main_loader').show();
            },
            success: function(data) {

                flag = true;
                $("#result_table").show();
                $("#graph").hide();
                $("#quartiles").hide();
                $("#potentiometer").hide();
                $("#gis").hide();
                $("#default3").hide();
                $(".alldata").html(data);

                //loadCharts();
                var graphdist = "";
                $('.district_checkbox:checked').each(function(i, selected) {

                    graphdist += $(this).val() + ",";
                });
                graphdist = graphdist.slice(0, -1);
                $.ajax({
                    url: "index.php?option=com_mica&task=showresults.getDistrictlist",
                    type: 'POST',
                    data: "dist=" + graphdist,
                    success: function(data) {
                        $('#main_loader').hide();
                        $("#result_table").find("#selectdistrictgraph").html(data);
                    }
                });

                $("#default").hide();
                $("#default1").hide();
                $("#light").hide();
                $("#applyChangesInitial").prop('btn-disabled', false);
                $("#apply_chnages").removeAttr('btn-disabled');
            }
        });
}

function downloadAction() {
    window.location = 'index.php?option=com_mica&task=showresults.exportexcel';
}




// For Potentio Meter ... edited by salim STARTED 26-10-2018


function pmdata() {
    $("#potentiometer").show();
    $("#result_table").hide();
    $("#graph").hide();
    $("#quartiles").hide();
    $("#gis").hide();
}

// For Gis Map ... edited by salim STARTED 26-10-2018
$("#gisdata").click(function() {
    $("#gis").show();
    $("#potentiometer").hide();
    $("#result_table").hide();
    $("#graph").hide();
    $("#quartiles").hide();
});







$(document).on('click', '#apply_chnages', function(){
    $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
    $("#graphdata").removeAttr('disabled').removeClass('btn-disabled');
    $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
    $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
    $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
});

$(document).on('click', '#applyChangesInitial', function(){
    $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
    $("#graphdata").removeAttr('disabled').removeClass('btn-disabled');
    $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
    $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
    $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
});

/*$(document).on('click', '#pmdata', function(){


        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.getGraphajax",
            method  : 'POST',
            success : function(data){

                var result = JQuery.parseJSON(data);
                var keys= JQuery.map(result, function(element,index) {return index});
                var variabeloutput = [];
                var cityoutput=[];
                JQuery.each(result,function(key,value){ //key:- variabel name
                    var variabel=key;
                    variabeloutput.push('<option value="'+ variabel +'">'+ variabel +'</option>');

                    JQuery.each(value,function(key1,value1){  // key1:- city name
                            var res = key1.split("~~");
                            cityoutput.push('<option value="'+ res[0] +'">'+ res[0] +'</option>');
                     });
                });
                $('#speed_variable').html(variabeloutput.join(''));
                $('#speed_region').html(cityoutput.join(''));
            }
        });

});*/




 
/// Get GIS gis map data

function loadGis_old()
{
    $("#gis").show();
	$("#potentiometer").hide();
    $("#result_table").hide();
    $("#graph").hide();
    $("#quartiles").hide();
    var data = $('#micaform').serialize();

    $.ajax({
        //url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
        url: "index.php?option=com_mica&task=showresults.getGISDataajax",
        method: 'POST',
        async: false,
        data: data,
        beforeSend: function() {
            $('#main_loader').show();
        },
        success: function(data) {
            var globalcorrdinates  = "";
            var oldzoom            = 0;
            var geo                = [];
            var dystate            = [];
            var geo                = [];
            var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;

            OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
            var tomcaturl = data.gis_tomcaturl;
            javazoom =  zoom = mainzoom = data.gis_zoom;
            gis_state = data.gis_state;
            gis_district = data.gis_district;
            gis_UnselectedDistrict = data.gis_UnselectedDistrict;
            districtsearchvariable = data.gis_districtsearchvariable;
            gis_town = data.gis_town;
            townsearchvariable = data.gis_townsearchvariable
            OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
                    defaultHandlerOptions: {
                        'single'         : true,
                        'double'         : false,
                        'pixelTolerance' : 0,
                        'stopSingle'     : true,
                        'stopDouble'     : false
                    },
                    initialize: function(options) {
                        this.handlerOptions = OpenLayers.Util.extend(
                            {}, this.defaultHandlerOptions
                        );
                        OpenLayers.Control.prototype.initialize.apply(
                            this, arguments
                        );
                        this.handler = new OpenLayers.Handler.Click(
                            this, {
                                'click':this.getfeaturenfo,'dblclick': this.onDblclick
                            }, this.handlerOptions
                        );
                    },
                    onDblclick: function(e) {
                        JQuery(".olPopup").css({"display":"none"});
                        JQuery("#featurePopup_close").css({"display":"none"});
                        //changeAttrOnZoom(map.zoom);
                    },
                    getfeaturenfo :function(e) {
                        coordinates = e;
                        var params = {
                            REQUEST       : "GetFeatureInfo",
                            projection    : "EPSG:4326",
                            EXCEPTIONS    : "application/vnd.ogc.se_xml",
                            BBOX          : map.getExtent().toBBOX(10),
                            SERVICE       : "WMS",
                            INFO_FORMAT   : 'text/html',
                            QUERY_LAYERS  : selectlayer(map.zoom),
                            FEATURE_COUNT : 6,
                            Layers        : selectlayer(map.zoom),
                            WIDTH         : map.size.w,
                            HEIGHT        : map.size.h,
                            X             : parseInt(e.xy.x),
                            Y             : parseInt(e.xy.y),
                            CQL_FILTER    : selectfilter(),
                            srs           : map.layers[0].params.SRS
                        };

                        // handle the wms 1.3 vs wms 1.1 madness
                        if(map.layers[0].params.VERSION == "1.3.0") {
                            params.version = "1.3.0";
                            params.i       = e.xy.x;
                            params.j       = e.xy.y;
                        } else {
                            params.version = "1.1.1";
                            params.y       = parseInt(e.xy.y);
                            params.x       = parseInt(e.xy.x);
                        }
                        OpenLayers.loadURL(tomcaturl, params, this, setHTML, setHTML);
                        var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
                        //lonLat.transform(map.displayProjection,map.getProjectionObject());
                        //map.setCenter(lonLat, map.zoom);
                        map.panTo(lonLat);
                        //map.addControl(ovControl);
                        //OpenLayers.Event.stop(e);
                    }
                });
            var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
            var options = {
                controls      : [],
                maxExtent     : bounds,
                projection    : "EPSG:4326",
                maxResolution : 'auto',
                zoom          :mainzoom,
                units         : 'degrees'
            };
            map = new OpenLayers.Map('map', options);
            var land = new OpenLayers.Layer.WMS("State Boundaries",
                tomcaturl,{
                    Layer       : 'india:rail_state',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : stateCqlFilter(data.gis_district),
                    SLD         : data.gis_sldrail_state
                },{isBaseLayer: false });
            var opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
                    tomcaturl,{
                        Layers           : 'india:rail_state',
                        transparent      : false,
                        format           : 'image/png',
                        styles           : "dummy_state",
                        transitionEffect : 'resize'
                    },{isBaseLayer: true});
            var districts = new OpenLayers.Layer.WMS("districts",
                     tomcaturl,{
                        Layer       : 'india:india_information',
                        transparent : true,
                        format      : 'image/png',
                        CQL_FILTER  : districtCqlFilter(),
                        SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/india_information.sld'; ?>"
                    },{isBaseLayer: false},{transitionEffect: 'resize'});

                var cities = new OpenLayers.Layer.WMS("Cities",
                     tomcaturl,{
                        Layer       : 'india:my_table',
                        transparent : true,
                        format      : 'image/png',
                        CQL_FILTER  : cityCqlFilter(),
                        SLD         : data.gis_sldmy_table
                    },
                    {isBaseLayer : false},
                    {transitionEffect : 'resize'});

                var urban = new OpenLayers.Layer.WMS("Urban",
                     tomcaturl,{
                        Layer       : 'india:jos_mica_urban_agglomeration',
                        transparent : true,
                        format      : 'image/png',
                        CQL_FILTER  : urbanCqlFilter(),
                        SLD         : data.gis_sldurban
                    },{isBaseLayer: false},{transitionEffect: 'resize'});
                if(map.zoom==5 || map.zoom==6 ){
                    map.addLayers([land]);
                }else if(map.zoom==7){
                   map.addLayers([districts]);
                }else if(map.zoom==8){
                    map.addLayers([urban]);
                }else{
                    map.addLayers([cities]);
                }



        }
    });
}

function loadGis(type)
{
    jQuery('#map').html("");
    //JQuery("#map").load(" #map > *");
    var data = $('#micaform').serialize();
    JQuery.ajax({
        //url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
        url     : "index.php?option=com_mica&task=showresults.getGISDataajax&type="+type,
        method  : 'POST',
        async   : true,
        data    : data,
        beforeSend: function() {
          jQuery('#main_loader').show();
        },
        success : function(data){
            //JQuery("#gis").load(" #gis > *");
            jQuery('#main_loader').hide();
            JQuery("#gis").show();
            JQuery("#result_table").hide();
            JQuery("#graph").hide();
            JQuery("#quartiles").hide();
            JQuery("#potentiometer").hide();

            JQuery("#thematic_attribute").load(" #thematic_attribute > *");    

            //JQuery(".tq").show();
            



            //JQuery("#gis").append( data );
            latestData = JQuery.parseJSON(data);

            //latestData.gis_tomcaturl;

            tomcaturl              = latestData.gis_tomcaturl;
            mainzoom               = latestData.gis_zoom;
            javazoom               = latestData.gis_zoom;
            userid                 = latestData.gis_userid;
            activeworkspace        = latestData.gis_activeworkspace;
            mainGeometry           = latestData.gis_geometry;
            mainlonglat            = latestData.gis_longlat;
            mainState              = latestData.gis_state;
            mainDistrict           = latestData.gis_district;
            mainUnselectedDistrict = latestData.gis_UnselectedDistrict;
            mainTown               = latestData.gis_town;
            mainUrban              = latestData.gis_urban;
            sldlinkDistricts   = latestData.gis_slddistrict;
            
            //$(".thematicData").html(latestData.gis_thematicdata);
            

            OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
                defaultHandlerOptions: {
                    'single'         : true,
                    'double'         : false,
                    'pixelTolerance' : 0,
                    'stopSingle'     : true,
                    'stopDouble'     : false
                },
                initialize: function(options) {
                    this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );
                    OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    );
                    this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click':this.getfeaturenfo,'dblclick': this.onDblclick
                        }, this.handlerOptions
                    );
                },
                onDblclick: function(e) {
                    JQuery(".olPopup").css({"display":"none"});
                    JQuery("#featurePopup_close").css({"display":"none"});
                    //changeAttrOnZoom(map.zoom);
                },
                getfeaturenfo :function(e) {
                    coordinates = e;
                    var params = {
                        REQUEST       : "GetFeatureInfo",
                        projection    : "EPSG:4326",
                        EXCEPTIONS    : "application/vnd.ogc.se_xml",
                        BBOX          : map.getExtent().toBBOX(10),
                        SERVICE       : "WMS",
                        INFO_FORMAT   : 'text/html',
                        QUERY_LAYERS  : selectlayer(map.zoom),
                        FEATURE_COUNT : 6,
                        Layers        : selectlayer(map.zoom),
                        WIDTH         : map.size.w,
                        HEIGHT        : map.size.h,
                        X             : parseInt(e.xy.x),
                        Y             : parseInt(e.xy.y),
                        CQL_FILTER    : selectfilter(),
                        srs           : map.layers[0].params.SRS
                    };

                    // handle the wms 1.3 vs wms 1.1 madness
                    if(map.layers[0].params.VERSION == "1.3.0") {
                        params.version = "1.3.0";
                        params.i       = e.xy.x;
                        params.j       = e.xy.y;
                    } else {
                        params.version = "1.1.1";
                        params.y       = parseInt(e.xy.y);
                        params.x       = parseInt(e.xy.x);
                    }
                    OpenLayers.loadURL(tomcaturl, params, this, setHTML, setHTML);
                    var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
                    //lonLat.transform(map.displayProjection,map.getProjectionObject());
                    //map.setCenter(lonLat, map.zoom);
                    map.panTo(lonLat);
                    //map.addControl(ovControl);
                    //OpenLayers.Event.stop(e);
                }
            });

            var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
            var options = {
                controls      : [],
                maxExtent     : bounds,
                projection    : "EPSG:4326",
                maxResolution : 'auto',
                zoom          : mainzoom,
                units         : 'degrees'
            };

            map = new OpenLayers.Map('map', options);


            /*land = new OpenLayers.Layer.WMS("State Boundaries",
                tomcaturl,
                {
                    Layer       : 'india:rail_state',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : stateCqlFilter(),
                    SLD         : sldlinkStateBoundaries
                },
                {
                    isBaseLayer: false
                }
            );*/

            opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
                tomcaturl,
                {
                    Layers           : 'india:rail_state',
                    transparent      : false,
                    format           : 'image/png',
                    styles           : "dummy_state",
                    transitionEffect : 'resize'
                },
                {
                    isBaseLayer: true
                }
            );

            districts = new OpenLayers.Layer.WMS("districts",
                tomcaturl,
                {
                    Layer       : 'india:india_information',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : districtCqlFilter(),
                    SLD         : sldlinkDistricts
                },
                {
                    isBaseLayer: false
                },
                {
                    transitionEffect: 'resize'
                }
            );

            /*cities = new OpenLayers.Layer.WMS("Cities",
                tomcaturl,
                {
                    Layer       : 'india:my_table',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : cityCqlFilter(),
                    SLD         : sldlinkCities
                },
                {
                    isBaseLayer : false
                },
                {
                    transitionEffect : 'resize'
                }
            );*/

            /*urban = new OpenLayers.Layer.WMS("Urban",
                tomcaturl,
                {
                    Layer       : 'india:jos_mica_urban_agglomeration',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : urbanCqlFilter(),
                    SLD         : sldlinkUrban
                },
                {
                    isBaseLayer: false
                },
                {
                    transitionEffect: 'resize'
                }
            );*/

            

            if(mainzoom==5 || mainzoom==6 ){

                //map.addLayers([districts]);
                map.addLayers([land]);
            }else if(mainzoom==7){
            	if(type == 'thematic') {
            		map.addLayers([districts]);
            		districts.redraw(true);
            	} else {
            		map.addLayers([districts]);	
            	}
            	
            }else if(mainzoom==8){
                //map.addLayers([districts]);
                map.addLayers([urban]);
            }else{
                map.addLayers([cities]);
            }


            map.div.oncontextmenu = function noContextMenu(e) {
                if (OpenLayers.Event.isRightClick(e)){
                    displaymenu(e);
                }
                // return false; //cancel the right click of brower
            };

            //map.addControl(exportMapControl);
                map.addControl(new OpenLayers.Control.PanZoomBar({
                position : new OpenLayers.Pixel(5, 15)
            }));
            map.addControl(new OpenLayers.Control.Navigation({
                dragPanOptions: {enableKinetic: true}
            }));

            //map.addControl(new OpenLayers.Control.Scale($('scale')));
            map.addControl(new OpenLayers.Control.Attribution());
            //map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
            //map.addControl(new OpenLayers.Control.LayerSwitcher());
            //alert(bounds);

            var click = new OpenLayers.Control.Click();
            map.addControl(click);
            click.activate();
            map.zoomTo(mainzoom);

            if(mainGeometry != 0 && mainGeometry != "")
            {
                var format    = new OpenLayers.Format.WKT();
                var feature   = format.read(mainGeometry);
                var homepoint = feature.geometry.getCentroid();
            }

            map.addLayers([opverviewland]);
            //if (!map.getCenter(bounds))
                //map.zoomToMaxExtent();
            map.zoomToExtent(bounds);
            //map.zoomToMaxExtent();

            //map.zoomTo(mainzoom);

            if(mainlonglat == "")
            {
                var curlonglat= '77.941406518221,21.676757633686';
            }
            else
            {
                var curlonglat = mainlonglat;
            }

            //alert(curlonglat);
            var longlatsegment=curlonglat.split(",");


            if(mainGeometry !="0")
            {
                map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
                map.zoomTo(6);
            }
            else
            {
                map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
                map.zoomTo(4);
            }

            if(type == "thematic") {
                totalthemecount     = latestData.gis_totalthemecount;
                usedlevel           = latestData.gis_usedlevel;
                havingthematicquery = latestData.gis_havingthematicquery;

                JQuery("#light_thematic").load(" #light_thematic > *",function(){
                    if(typeof(usedlevel) != "undefined"){
                        if (usedlevel != null) {
                            var ulevel=usedlevel.split(",");
                            if(ulevel.length == 3){
                                JQuery("#themeconent").css({"display":"none"});
                            }
                        }
                        getColorPallet_v2();
                    }
                });

                
                //getColorPallet_v2();
                
            }
            
        }
    });
}



/*function stateCqlFilter()
{
    
    if(gis_state!="all")
    {    //$stateArray=explode(",",JRequest::GetVar('state'));
        
         return "name in ('" + gis_state + "')";
    }
}
function districtCqlFilter() 
{
    if(gis_district!="all" && gis_district!="")
    {
        if(gis_UnselectedDistrict.length > gis_district.length)
        {
            statetojavascript = gis_district;
            return districtsearchvariable + " in ('"+statetojavascript+"') ";
        }
        else
        {
            statetojavascript = gis_UnselectedDistrict;
            statetojavascript = statetojavascript.replace(/,/g,"','");
            return districtsearchvariable + " in ('" + statetojavascript + "') ";
        }
    }
    else if(gis_district =="all")
    {
         return "state in ('" + statetojavascript + "')";
    }
}


function setHTML(response){
                    changeAttrOnZoom(map.zoom,response);
                };

function selectfilter(){
    if(javazoom==5 || javazoom==6 ){
        return stateCqlFilter();
    }else if(javazoom==7){
        return districtCqlFilter();
    }else  if(javazoom==8){
        return urbanCqlFilter();
    }else{
        return cityCqlFilter();
    }
}

function selectlayer(cuurzoom){
    if(javazoom==5 || javazoom==6){
        return "rail_state";
    }else if(javazoom==7){
        return "india_information";
    }else  if(javazoom==8){
        return "jos_mica_urban_agglomeration";
    }else{
        return "my_table";
    }
}*/
var lastchar = '';


         function operatorcall(op)
            {

                var Singlelineop = ['+','-','*','/','%'];
                var Trigonometric = ['SIN(','COS('];
                var data = $('#new_custom_attribute_final_output').val();

                if(op=='number' || op=='variables' || op=='pi')
                {
                        if(op=='number')//for number
                        {
                            var input =$('#custom_numeric').val();

                        }
                        else // for variable
                            var input = $('#avail_attribute').val();

                        if($('#new_custom_attribute_final_output').val()=="") //if empty
                        {
                            $('#new_custom_attribute_final_output').val($('#new_custom_attribute_final_output').val()+input) ;
                            return false
                        }
                        else
                        {
                            //for non empty result box
                            if (Singlelineop.indexOf(data.slice(-1))!=-1) //
                            {
                                $('#new_custom_attribute_final_output').val($('#new_custom_attribute_final_output').val()+input) ;
                                return false;

                            }
                            else
                            {
                                alert("invalid");
                                return false;
                            }
                            if (Trigonometric.indexOf(data.slice(-1))!=-1) // if last charactor is not an operator
                            {
                                $('#new_custom_attribute_final_output').val($('#new_custom_attribute_final_output').val()+input) ;
                                return false;
                            }
                            else
                            {
                                alert("invalid");
                                return false;
                            }
                    }
                }
                else
                {
                    //for operator
                    if($('#new_custom_attribute_final_output').val()=="") //if empty
                        {
                            alert('Either Select Varialbe or Add Value first');
                            return;
                        }
                        else // if not empty
                        {
                            if (Singlelineop.indexOf(data.slice(-1))==-1 )
                            {
                                if(op.length==1)
                                {
                                    $('#new_custom_attribute_final_output').val($('#new_custom_attribute_final_output').val() +op);
                                }
                                else if(op.length>=3)
                                {

                                    $('#new_custom_attribute_final_output').val(op +$('#new_custom_attribute_final_output').val() +')');
                                }
                            }
                            else
                            {
                                alert("invalid");

                            }

                        }
                        return;
                }

            }

function checkvalidation()
    {
    	var statecheck = false, districtcheck = false, typecheck = false, variablecheck = false;
    	//check for state selected or not
    	if ($('.state_checkbox:checked').length == 0) {
            $("#statetext").attr("style", "color:black");
            statecheck = false;
        }
        else
        {
       		$("#statetext").attr("style", "color:#ba171b");//red
            $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
            statecheck = true;
        }

        // check for district selected or not
        if ($('.district_checkbox:checked').length == 0) {
            $("#districttext").attr("style", "color:black");
            districtcheck = false;
        }
        else
        {
        	$("#districttext").attr("style", "color:#ba171b");
            $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
            districtcheck = true;
        }

        // check for Industry Level selected or not
        if ($('.industryLevel_checkbox:checked').length > 0) {
            $("#typetext").attr("style", "color:#ba171b");
            $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
            typecheck = true;

            $("#variabletext").attr("style", "color:#ba171b");
            $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
            variablecheck = true;
        }

        // check for type selected or not
        if ($('.type_checkbox:checked').length == 0) {
            $("#typetext").attr("style", "color:black");
            $("#default").show();
            typecheck = false;
        }
        else
        {
            $("#typetext").attr("style", "color:#ba171b");
            $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
            typecheck = true;
        }

        if ($('.variable_checkbox:checked').length == 0) {
            $("#variabletext").attr("style", "color:black");
            variablecheck =  false;
        }
        else
        {
        	$("#variabletext").attr("style", "color:#ba171b");
            $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
            variablecheck = true;
        }

        if(variablecheck == typecheck == districtcheck == statecheck == true)
        {
        	$("#apply_chnages").removeAttr('disabled').removeClass('btn-disabled');
        }
        else
        {
        	$("#apply_chnages").attr('disabled','disabled').addClass('btn-disabled');
        }

        if(flag == true)
        {
            $("#default").hide();
            $("#tabledata").attr('disabled','disabled').addClass('btn-disabled');
            $("#loadchart").attr('disabled','disabled').addClass('btn-disabled');
            $("#gisdata").attr('disabled','disabled').addClass('btn-disabled');
            $("#matrix").attr('disabled','disabled').addClass('btn-disabled');
            $("#pmdata").attr('disabled','disabled').addClass('btn-disabled');
            $("#default1").show();
        }
        else
        {
            $("#default").show();
        }
    }

    $(document).on('click', '#apply_chnages', function(){
        $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
        $("#loadchart").removeAttr('disabled').removeClass('btn-disabled');
        $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
        $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
        $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
    });

    $(document).on('click', '#applyChangesInitial', function(){
        $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
        $("#loadchart").removeAttr('disabled').removeClass('btn-disabled');
        $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
        $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
        $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
    });

$(".type_checkbox").change(function(){
checkvalidation();
});
$(".variable_checkbox").change(function(){
checkvalidation();
});



