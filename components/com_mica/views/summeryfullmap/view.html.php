<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );

/**
 * Fullmap view class for Mica.
 *
 * @since  1.6
 */
class MicaViewSummeryfullmap extends JViewLegacy
{

	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $tomcaturl = "";

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null){

		$app     = JFactory::getApplication();
		$session = JFactory::getSession();
		$user    = JFactory::getUser();
		$model   = JModelLegacy::getInstance('SummeryResults', 'MicaModel', array('ignore_request' => true));

		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}

		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles/micamimi18");
		}


		$msg = $app->input->get('msg');
		if($msg=="-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg=="0"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_CREATED') );
		}else if($msg=="1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_UPDATED') );
		}

		$m_type = $app->input->get('m_type', '', 'raw');

		if($session->get('activeworkspace') == ""){
			$result = workspaceHelper::loadWorkspace(1);

	    	if(!empty($result)){

	    		$session->set('activeworkspace', $result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile', $result[0]['id']);
				$session->set('is_default', "1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));

	    	}else{

				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default', "1");
				$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188");
	    	}

		}else if($session->get('is_default') == 1){

	    	workspaceHelper::updateWorkspace($innercall);

	    }else{

    	    $app->input->set('activeworkspace', $session->get('activeworkspace'));

	    }

		////summary state
		$state = $app->input->get('summarystate', '', 'raw');

		$stats = array();
		foreach($state as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('summarystate',$state);
		}
		$sessionstate = $session->get('summarystate');

		$app->input->set('summarystate', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('summarystate', $sessionstate);
		////summary state


		////summary district
		$summaryDistrict = $app->input->get('summarydistrict', '', 'raw');
			if(is_array($summaryDistrict)){
			$summaryDistrict = array_filter($summaryDistrict);
			$summaryDistrict = implode(",",$summaryDistrict);
		}
		$sessionSummaryDistrict = $session->get('summarydistrict');
		if($sessionSummaryDistrict != ""){
			if($app->input->get('summarydistrict', '', 'raw') != ""){
				$sessionDistrict = ($summaryDistrict);
			}else{
				$sessionDistrict = ($sessionSummaryDistrict);
			}
			$tmp          = explode(",",$sessionDistrict);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionDistrict = implode(",",$mytmp);
		}else{
			$sessionDistrict = $summaryDistrict;
		}
		$session->set('summarydistrict',$sessionDistrict);
		$app->input->set('summarydistrict',$sessionDistrict);

		// summary start sub district
		$district = $app->input->get('summarySubDistrict', '', 'raw');

		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('summarySubDistrict', $district);
		}

		$sessionSubDistrict = $session->get('summarySubDistrict');
			// /print_r($sessionSubDistrict);
		if($sessionSubDistrict != ""){
			$addtosession1   = ($sessionSubDistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessionSubDistrict = implode(",",$mytmp);
		}else{
			$sessionSubDistrict = $district;
		}

		$session->set('summarySubDistrict',$sessionSubDistrict);
		$app->input->set('summarySubDistrict',$sessionSubDistrict);

		////summary end sub district



		////summary attribute
		$attributes = $app->input->get('summeryattributes', '', 'raw');
			if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('summeryattributes');
		if($sessionattr != ""){
			if($app->input->get('summeryattributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('summeryattributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);
		////summary attribute

		if($sessiontown != ""){
			$app->input->set('db_table','Town');
			$zoom = 9;//JRequest::setVar('db_table','Town');
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$zoom = 8;//JRequest::setVar('db_table','Urban');
		}else if($sessionDistrict != ""){
			$app->input->set('db_table','District');
			$zoom = 7;//JRequest::setVar('db_table','District');
		}else{
			$app->input->set('db_table','State');
			$zoom = 6;//JRequest::setVar('db_table','State');
		}


		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}

		$state_items = $model->getStateItems();

		$this->state_items            = $state_items;
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;

		$this->Itemid = $app->input->get('Itemid', 188);

		$customattribute  = $session->get('customattribute');
		$new_name         = $app->input->get('new_name', array(), 'array');
		$custom_attribute = $app->input->get('custom_attribute', array(), 'array');

		$MaxForMatrix     = $this->get('MaxForMatrix');

		$this->MaxForMatrix = $MaxForMatrix;
		$this->customdata   = $customdata;
		$this->state        = $sessionstate;
		$this->district     = $sessionDistrict;
		$this->town         = $sessiontown;
		$this->urban        = $sessionurban;

		$AttributeTable = $this->get('NewAttributeTable');//$this->get('AttributeTable');
		$GraphSettings  = $this->get('GraphSettings');
		$graph          = $this->get('Graph');
		$GraphSettings  = $this->get('GraphSettings');

		$activesearchvariable = $session->get('activesearchvariable');
		$this->zoom           = $zoom;
		/*$tomcaturl = TOMCAT_URL;
		$this->tomcaturl = $tomcaturl;*/

		//$BreadCrumbcontent = $this->get('BreadCrumb');
		$BreadCrumbcontent = $model->getBreadCrumb();
		list($BreadCrumb, $workspacelink) = explode("~~~", $BreadCrumbcontent);
		$this->workspacelinkdiv   = $workspacelink;
		$this->BreadCrumb         = $BreadCrumb;

		$geometry                 = $model->getInitialGeometry();
		$this->geometry           = $geometry;
		$UnselectedDistrict       = $model->getUnselectedDistrict();
		$this->UnselectedDistrict = $UnselectedDistrict;

		$this->customdata = $model->getCustomData();

		$this->user            = $user;
		$this->userid          = $user->id;
		$this->activeworkspace = $session->get('activeworkspace');
		$this->is_default      = $session->get('is_default');
		$this->customattribute = $customattribute;

		//$this->get('customattrlib');
		$model->getcustomattrlib();
		$this->customattributelib = $session->get('customattributelib');
		$this->AttributeTable     = $AttributeTable;
		$this->GraphSettings      = $GraphSettings;
		$this->graph              = $graph;

		$this->m_type = $m_type;
		$app->input->set('m_type',$m_type);

		$this->attributes        = $addtosession;
		$this->new_name          = $new_name;
		$this->custom_attribute  = $custom_attribute;
		$this->pagination        = $pagination;
		$this->themeticattribute = $session->get('villageSummeryThemeticattribute');

		//cssHelper::saveSldToDatabase(1, 0);
		cssHelper::saveSldToDatabaseVillageSummry(1, 1);

		return parent::display($tpl);
	}

	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcaturl = $db->loadResult();
	}

}
