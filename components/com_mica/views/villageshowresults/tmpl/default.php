<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc     = JFactory::getDocument();
$app     = JFactory::getApplication('site');
$db      = JFactory::getDbo();
$session = JFactory::getSession();

$doc->addStyleSheet(JURI::base() . 'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base() . 'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base() . 'components/com_mica/css/jquery.jscrollpane.css', 'text/css');
$doc->addStyleSheet(JURI::base() . 'components/com_mica/js/jqueryselect/chosen.css', 'text/css');
$doc->addStyleSheet(JURI::base() . 'components/com_mica/maps/examples/style.css', 'text/css');
$doc->addStyleSheet(JURI::base() . 'components/com_mica/maps/theme/default/style.css', 'text/css');
?>

<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.cookie.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jqueryselect/chosen.jquery.js"></script>
<script type="text/javascript">
	var choosenjq = JQuery.noConflict();
	choosenjq(document).ready(function(){
		choosenjq("select").chosen();
		choosenjq("#avail_attribute").chosen({
			allow_single_deselect : true
		});
	});
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.mousewheel.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/villagefield.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/swfobject.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amcharts.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amfallback.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<script type="text/javascript">
	var siteurl = '<?php echo JURI::base();?>';
	JQuery(document).keyup(function(e){$
		if(e.keyCode == "27"){
			document.getElementById('light').style.display = 'none';
			document.getElementById('fade').style.display  = 'none';
		}
	});

	var is_updatecustom=0;
	JQuery(document).ready(function() {

	});

	JQuery(".deleterow").live("click",function(){
		var value = JQuery(this).val();
		var ans   = confirm("Are you Sure to delete Variable "+value);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.deleteattribute&attr="+value,
				type    : 'GET',
				success : function(data){
					JQuery("#"+value).fadeOut();
					window.location='index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});

	JQuery(".deletecustom").live("click",function(){
		var value = JQuery(this).val();
		var ans   = confirm("Are you Sure to delete Custom Variable "+value);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.deleteCustomAttribute&attr="+value,
				type    : 'GET',
				success : function(data){
					JQuery("#"+value).fadeOut();
					window.location = 'index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});

	JQuery(".removecolumn").live("click",function(){
		var value       = JQuery(this).val();
		//var classname = JQuery(this).parent().attr('class');
		var segment     = value.split("`");
		var ans         = confirm("Are you Sure to delete  "+segment[2]);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=villageshowresults.deleteVariable&table="+segment[0]+"&value="+segment[1],
				type    : 'GET',
				success : function(data){
					//JQuery(this).fadeOut();
					window.location = 'index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});
	var compare =1;
</script>

<div class="showresultbody">
	<div class="mimihead">
		<div class="headtopleft">
			<img src="<?php echo JURI::base();?>components/com_mica/images/top_left.png" width="6" height="23" border="0"/>
		</div>
		<div class="headtopright">
			<img src="<?php echo JURI::base();?>components/com_mica/images/top_right.png" width="6" height="23" border="0"/>
		</div>
	</div>

	<div class="breadcrumb clear">
		<div class="breadcrumb_left">
			<img src="<?php echo JURI::base();?>components/com_mica/images/breadcrumb_left.png" height="24"/>
		</div>

		<?php echo $this->BreadCrumb;?>

		<div class="breadcrumb_right">
			<img src="<?php echo JURI::base();?>components/com_mica/images/breadcrumb_right.png" height="24" />
		</div>

		<?php /*jimport('joomla.application.module.helper');
			// this is where you want to load your module position
			$modules = JModuleHelper::getModules("mica-attr");
			foreach($modules as $module){
				echo JModuleHelper::renderModule($module);
			}*/
			/*$module_attribs = array(
				'workspacelinkdiv'   => $this->workspacelinkdiv,
				'customattribute'    => $this->customattribute,
				'customattributelib' => $this->customattributelib
			);
			$module = JModuleHelper::getModule('mod_mica', $module_attribs);
			echo JModuleHelper::renderModule($module);*/

			include JPATH_COMPONENT.'/views/villageshowresults/tmpl/mod_mica.php';
		?>

	</div>
</div>

<!--<div class="bigsapretor"></div>	-->
</div>
<div class="micacomponentblock">
	<div class="mod_mica" >
		<style type="text/css">
			#tq.thematiceq{display:none; position:absolute; z-index:9999; background:#9D9F9E ; margin:30px 0px 0px -115px; padding:0px 30px !important;}
		</style>
		<div class="collpsable">
			<span class="toggle1 saperator" id="text"><?php echo JText::_('TEXT_TAB_LABEL'); ?></span>
			<span id="graphs" class="toggle1 endsap" ><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></span>
			<span id="matrix" class="toggle1 saperator"  ><?php echo JText::_('Matrix'); ?>  </span>
			<span id="tq" class="toggle1 saperator thematiceq"><?php echo JText::_('ThematicQuery'); ?></span>
			<span class="toggle1 saperator" id="speedometer" style="border-right:none;"> <?php echo JText::_('Speedometer'); ?> </span>
			<span  class="modify " ><?php echo $this->workspacelinkdiv; ?></span>
		</div>

		<div class="text contenttoggle1" style="display:none;">
			<div class="collpsable1">
				<?php /* <span class="toggle2 saperator" id="alldata">< ?php echo JText::_('TEXT_TAB_LABEL'); ? ></span><span class="toggle2 saperator" id="compare">< ?php echo JText::_('COMPARE_TAB_LABEL'); ? ></span>*/ ?>
				<span style="float: right;"  >
					<table cellspacing="0" cellpadding="0" border="0" style="margin:5px 5px 0px 0px;">
						<tr>
							<td >
								<a href="#alldata" onclick="downloadAction();">
									<img src="<?php echo JURI::base();?>components/com_mica/images/export_excel.png" title="Export to Excel"  />
								</a>
							</td>
							<td>
								<a href="#alldata" onclick="downloadAction();">&nbsp;<?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?></a>
							</td>
						</tr>
					</table>
				</span>
			</div>
			<div class="activecontent" >
				<div class="compare contenttoggle2" style="display:none;">
					<div style="width:625px;float:left;">
						<form name="micaform" id="micaform" action="index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $this->Itemid; ?>" method="POST">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="padding-left: 20px;">
								<tr>
									<td align="left"><b><?php echo JText::_('SELECT_CPR_MSG');?></b></td>
								</tr>
								<tr>
									<td align="left">
										<div class="maintable">
											<div class="left"><b><?php echo JText::_('STATE_LABEL'); ?></b>:&nbsp;</div>
											<div class="right">
												<select name="villagestate" id="villagestate" class="inputbox-mainscr" onchange="getdistrict(this.value)">
													<option value="all"><?php echo JText::_('ALL');?></option>
													<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/ ?>
													<?php for($i = 0; $i < count($this->state_items); $i++){ ?>
														<option value="<?php echo $this->state_items[$i]->name; ?>">
															<?php echo $this->state_items[$i]->name; ?>
														</option>
													<?php  } ?>
												</select>
											</div>
										</div>
									</td>
								</tr>
								<tr >
									<td align="left">

										<?php if($this->district != "") {?>
											<div class="districtspan maintable level2" >
												<div class="left"><b><?php echo JText::_('DISCTRICT_LABEL'); ?></b> :&nbsp;</div>
												<div class="right">
													<span id="districtspan">
														<select name="villagedistrict" id="villagedistrict" class="inputbox">
															<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
														</select>
													</span>
												</div>
											</div>
										<?php } else if ($this->urban != "") { ?>
											<div class="urbanspan maintable level2">
												<div class="left"><b><?php echo JText::_('URBAN_LABEL'); ?></b>:&nbsp;</div>
												<div class="right">
													<span id="urbanspan">
														<select name="urban" id="urban" class="inputbox">
															<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
														</select>
													</span>
												</div>
											</div>
										<?php } else if($this->villages != "") {?>
											<div class="townspan maintable level2">
												<div class="left"><b><?php echo JText::_('TOWN_LABEL'); ?></b>:&nbsp;</div>
												<div class="right">
													<span id="townspan">
														<select name="villages_villages" id="villages_villages" class="inputbox">
															<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
														</select>
													</span>
												</div>
											</div>
										<?php }?>
									</td>
								</tr>
								<tr>
									<td align="left"><br>
										<div class="right frontbutton">
											<input type="submit" name="submit" id="submit" class="" value="<?php echo JText::_('COMPARE_DATA'); ?>" style="width:100px;margin-left: 7px;">
										</div>
										<input type="hidden" name="option" value="com_mica" />
										<input type="hidden" name="zoom" id="zoom"value="" />
										<input type="hidden" name="view" value="villageshowresults" />
										<input type="hidden" name="Itemid" value="<?php echo $this->Itemid;?>" />
										<input type="hidden" name="depricatedm_type" value="<?php echo $this->m_type;?>" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php if ($app->input->get('villagestate') != "all" && $app->input->get('villagedistrict') != "all" && $app->input->get('villages_villages') != "all"
						&& $app->input->get('urban') != "all" && count($this->customdata) != 1 ) { ?>
					<div style="width:285px;float:left;">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<th><b>Remove <?php echo $this->dataof;?></b></th>
							</tr>
							<?php foreach($this->customdata as $eachdata){
								$activesearchvariable=$this->activesearchvariable;?>
								<tr>
									<td>
										<a href="index.php?option=com_mica&Itemid=188&task=villageshowresults.setFieldVal&id=<?php echo $eachdata->$activesearchvariable;?>">
											<?php echo $eachdata->name; ?>
										</a>
									</td>
								</tr>
							<?php }?>
						</table>
					</div>
					<?php }?>
				</div>

				<div class="alldata contenttoggle2" id="jsscrollss" style="width:940px;height:524px;max-height:524px;">
					<table cellspacing="0" cellpadding="0" border="0" width="100%"  class="draggable" id="datatable">
						<?php echo $this->AttributeTable;?>
						<table>
							<tr>
								<td>
									<?php echo $this->pagination->getPagesLinks(); ?>
								</td>
							</tr>
						</table>

						<?php /*$dist=explode(",",$this->district);
							$pages=round(count($dist)/50);
							if(round($pages) !=0){ ?>
								<tr>
									<td>< ?php $page= JRequest::getVar('page'); echo ($page!="" && $page!=0)? '<a href ="index.php?option=com_mica&view=villageshowresults&Itemid=188&page='.($page-1).'">Previous</a>' :""; ? ></td>
									<td>< ?php $page= JRequest::getVar('page'); echo ($page <=$pages && round($pages) !=0)? '<a href ="index.php?option=com_mica&view=villageshowresults&Itemid=188&page='.($page+1).'">Next</a>' :""; ? ></td>
								</tr>
							<?php } */
						?>
					</table>
					<?php echo "<br /><br />"; ?>
				</div>
			</div>
		</div>

		<div class="graphs contenttoggle1" style="display:none;">

			<table cellspacing="7" cellpadding="0" style="float:left;">
				<tr>
					<td>Chart Type :</td>
					<td>
						<select name="charttype" class="inputbox" id="chartype" onchange="changecharttype(this.value,JQuery('#enablelegend:checked').val());" style="width:121px !important;">
							<option value="amcolumn" selected>Bar Chart</option>
							<option value="amline">line Chart</option>
							<option value=amradar>Radar Chart</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Show Legend</td>
					<td>
						<input type="checkbox" id="enablelegend" name="enablelegend" onclick="changecharttype(JQuery('#chartype').val(),this.checked);" value="1" />
					</td>
				</tr>
			</table>

			<table style="float:left;" cellspacing="0" cellpadding="0">
				<tr>
					<td rowspan="2" style="padding: 10px;">
						<?php $variableoptions="";
						//foreach($this->customdata[0] as $key => $val){
						foreach($this->popVariables as $key => $val){
							if($key!="name" && $key!="state" && $key!="distshp" && $key!="OGR_FID"){
								$variableoptions.="<li style='width='100px;float:left;'><input type='checkbox' class='variablechecked' value='".$key."' />".JTEXT::_($key)."</li>";
							}
						}
						$variableopopup = '<div id="light1007" class="white_content2" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1007\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select Variable</div>';
						$distopopup = '<div id="light1008" class="white_content2" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1008\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select Village</div>';
						$distoptions ='<ul id="selectdistrictgraph" style="list-style:none;"></ul>'; ?>

						<?php echo $distopopup.$distoptions."</div>";?>
						<a href="javascript:void();" id="graphdistrict" onClick="document.getElementById('light1008').style.display='block';document.getElementById('fade').style.display='block'">Select Village</a>
					</td>
					<td rowspan="2" style="padding-right: 10px;">
						<a href="javascript:void();" id="graphvariable" onClick="document.getElementById('light1007').style.display='block';document.getElementById('fade').style.display='block'">Select Variable</a>
						<?php echo $variableopopup."<ul style='list-style:none;'>".$variableoptions."</ul></div>";?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><div id="showchart" style="padding-top: 0;" class="right"><input type="button"  name="showchart"  class="button"value="Show Chart" /> </div></td>
				</tr>
			</table>

			<table style="float:right;" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div id="exportchartbutton" style="padding-top: 0;" class="right">
							<input type="button"  name="downloadchart" onclick="exportImage(JQuery('#chartype option:selected').text());" class="button"value="Export" />
						</div>
					</td>
					<td></td>
				</tr>
			</table>

			<script type="text/javascript">
				var swfurl         = "";
				var amFallbacktype = "";
				var datastr        = "<?php echo $this->graph;?>";
				var grpsetting     = "<?php echo $this->GraphSettings;?>";

				changecharttype("amcolumn",0);

				function changecharttype(value,legend){
					if(legend=="undefined"){
						legend = 0;
					}
					swfurl ="<?php echo JURI::Base();?>components/com_mica/flash/"+value+".swf";
					amFallbacktype=value;
					JQuery("#chartdiv").html("");
					loadChart(swfurl,amFallbacktype,legend);
				}

				function loadChart(swfurl,amFallbacktype,legend){
					var params   = { bgcolor:"#FFFFFF", wmode: "opaque" };
					var s        = datastr.split('sln');
					var new_data = '';

					for(var i=0;i<s.length;i++){
						new_data += s[i];
						if(i-1 != s.length){
							new_data += '\n';
						}
					}

					//console.log(new_data);
					var flashVars = {
						path           : "components/com_mica/flash/",
						chart_data     : new_data,
						chart_settings : "<settings><angle>0</angle><depth>0</depth><redraw>true</redraw><data_type>csv</data_type><thousands_separator>,</thousands_separator><plot_area><margins><left>50</left><right>50</right><top>50</top><bottom>75</bottom></margins></plot_area><grid><category><dashed>1</dashed><dash_length>4</dash_length></category><value><dashed>1</dashed><dash_length>4</dash_length></value></grid><axes><category><width>.5</width><color>E7E7E7</color></category><value><width>.5</width><color>E7E7E7</color></value></axes><values><value><min>0</min></value></values><legend><enabled>"+legend+"</enabled></legend><angle>90</angle><depth>0</depth><column><hover_color>#EED600</hover_color><type>clustered</type><width>85</width><spacing>10</spacing><balloon_text>{title} : {value}</balloon_text><grow_time>3</grow_time><sequenced_grow>.5</sequenced_grow><data_labels_position>inside</data_labels_position></column><graphs>"+grpsetting+"</graphs><labels><label lid='0'><text><![CDATA[]]></text><y>18</y><text_color>000000</text_color><text_size>13</text_size><align>center</align></label></labels></settings>"
					};

					swfobject.embedSWF(swfurl, "chartdiv", "924", "600", "8.0.0", "<?php echo JURI::Base();?>components/com_mica/flash/expressInstall.swf", flashVars, params);

					// change 8 to 80 to test javascript version
					/*if(swfobject.hasFlashPlayerVersion("8")){
						swfobject.embedSWF(swfurl, "chartdiv", "924", "600", "8.0.0", "<?php echo JURI::Base();?>components/com_mica/flash/expressInstall.swf", flashVars, params);
						//var flashMovie = document.getElementById('chartdiv');
						//flashMovie.exportImage();
					}else{
						var amFallback = new AmCharts.AmFallback();
						// amFallback.settingsFile = flashVars.settings_file;  		// doesn't support multiple settings files or additional_chart_settins as flash does
						// amFallback.dataFile = flashVars.data_file;
						amFallback.chartSettings = flashVars.chart_settings;
						amFallback.pathToImages = "components/com_mica/images/amcharts/";
						//amFallback.settingsFile = flashVars.settings_file;
						amFallback.chartData = flashVars.chart_data;
						//amFallback.dataFile = flashVars.data_file;
						amFallback.type = amFallbacktype;
						amFallback.write("chartdiv");
					}*/
				}
			</script>
			<style></style>
			<div id="chartdiv" style="width:100%; height:400px; background-color:#FFFFFF"></div>
			<!-- end of amcharts script -->
		</div>

		<div class="tq contenttoggle1" >
			<div class="" style="width:100%;float:left;">
				<div class="thematicarea clearfix">
					<div class="blockcontent">
						<?php function getCustomAttrName($name,$activeworkspace) {
							$db    = JFactory::getDBO();
							$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
								WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
									AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
							$db->setQuery($query);
							$result = $db->loadAssoc();

							if(count($result) == 0){
								return array($name,$name);
							}else{
								return array($result[0],$result[1]);
							}
						}

						$activeworkspace = $this->activeworkspace;

						$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
							WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
							ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
						$db->setQuery($query);
						$result = $db->loadObjectList();

						$id    = array();
						$level = array();
						$i     = 1;
						$j     = 1;
						$grouping = array();
						foreach($result as $range){
							if($range->level == "0"){
								$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
							}else{
								$pin = str_replace("#","",$range->color);
								$str = " <div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
							}

							$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
							//$id[] = $range->custom_formula;
							//$grouping[$range->custom_formula] = $range->level;
							$level[$range->custom_formula][] = $range->level;
							$i++;
							$j++;
						}

						$i            = 0;
						$range        = array();
						$str          = array();
						$totalcount   = 0;
						$l            = 0;
						$tojavascript = array();
						$grpname = array();
						foreach($grouping as $key => $val){
							$grname         = getCustomAttrName($key, $activeworkspace);
							$grpname[]      = $grname;
							$str[]          = implode(" ",$val);
							$ranges[]       = count($val);
							$levelunique[]  = $level[$key][0];
							$tojavascript[] = $key;
							$l++;
						}
						$tojavascript = implode(",",$tojavascript);
						//$str="";
						for($i = 0; $i < count($grpname); $i++){
							echo "<div class='contentblock1 '>
								<div class='blockcontent'>
									<ul class='maingrp bullet-add clear' >
										<li>
											<div class='themehead'>".$grpname[$i][0]."</div>
											<div class='themeedit'>
												<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
													<img src='".JUri::base()."components/com_mica/images/edit.png' />
												</a>
												<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
													&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
												</a>
											</div>
											<ul class='range' >".$str[$i]."</ul>
										</li>
									</ul>
								</div>
							</div>";
						} ?>
						<div class='contentblock3'>
							<script type='text/javascript'>
								var totalthemecount     = "<?php echo count($grpname); ?>";
								var usedlevel           = '<?php echo implode(",",$levelunique); ?>';
								var havingthematicquery = '<?php echo $tojavascript; ?>';
							</script>
							<div id="themeconent">
								<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $this->Itemid; ?>" method="get" >
									<table cellspacing="4" cellpadding="0" border="0" width="90%" style="float: left;width: 335px;">
										<tr>
											<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL');?></h3></td>
										</tr>
										<tr>
											<td>
												<b><?php echo JText::_('ATTRIBUTE_LABEL');?></b>
											</td>
											<td>
												<select name="thematic_attribute" id="thematic_attribute" class="inputbox" style="width: 200px;" onchange="getMinmaxVariable(this.value);">
													<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
													<optgroup label="Default Variable">
														<?php $attr = $this->themeticattribute;
														$eachattr = explode(",",$attr);
														foreach($eachattr as $eachattrs){
															echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
														} ?>
													</optgroup>
													<optgroup label="Custom Variable">
														<?php $attr = $this->customattribute;
														$eachattr = explode(",",$attr);
														$eachattr = array_filter($eachattr);
														foreach($eachattr as $eachattrs){
															$eachattrs=explode(":",$eachattrs);
															echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
														} ?>
													</optgroup>
												</select>
											</td>
										</tr>
										<tr id="minmaxdisplay">
											<td></td>
											<td class="minmaxdisplay"></td>
										</tr>
										<tr>
											<td>
												<b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b>
											</td>
											<td>
												<input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;">
											</td>
										</tr>
										<tr class="colorhide">
											<td>
												<b><?php echo JText::_('SELECT_COLOR_LABEL');?></b>
											</td>
										<td>
											<input class="simple_color" value="" style="width: 194px;"/>
										</td>
										</tr>
										<tr>
											<td colspan="2">
												<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval"></table>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<div class="readon frontbutton" style="float: left;">
													<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" />
												</div>
												<div class="readon frontbutton" style="float: left;padding-left: 5px;">
													<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup();"/>
												<div>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Thematic -->

		<!--Start Speed -->
		<div class="speedometer contenttoggle1" style="display:none;">
			<table cellspacing="4" style="float:left;">
				<thead style="display:none;" id="speedfiltershow">
					<th class="rightgrptext frontbutton" >
						<input style="margin-left:3px;" type="button" class="frontbutton" value="Edit Filter" name="editfilter"/>
					</th>
				</thead>
				<tr class="sfilter">
					<td colspan="2">
						<table>
							<tr>
								<td>
									<b><?php echo JText::_('FILTER_BY');?></b>&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td>
									<input type="radio" name="filter" value="0" class="filterspeed"/></td><td><?php echo JText::_('FILTER_BY_LAYER');?>
								</td>
								<td>
									<input type="radio" name="filter" value="1" class="filterspeed"/></td><td> <?php echo JText::_('FILTER_BY_VARIABLE');?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="sfilter">
					<td colspan="2">
						<div class="lft speed" style="width:210px;display:none;" >
							<table cellspacing="4">
								<tr>
									<td>
										<select name="speed_variable" id="speed_variable" class="inputbox" style="width: 200px;" multiple>
											<?php /*<option value="0">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/
											$attr     = $this->themeticattribute;
											$eachattr = explode(",",$attr);
											foreach($eachattr as $eachattrs){
												echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
											} ?>
										</select>
									</td>
									</tr>
								<tr>
									<td>
										<select name="speed_region" id="speed_region" class="inputbox" style="width: 200px;" multiple >
											<?php /*<option value="0">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/
											foreach($this->customdata as $eachdata){
												echo '<option value='.str_replace(" ","%20",$eachdata->name).'>'.$eachdata->name.'</option>';
											} ?>
										</select>
									</td>
								</tr>
								<tr>
									<td>
										<div class="left frontbutton ">
											<input type="button" name="showspeed" id="showspeed" class="" value="Create">
										</div>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<div id="spedometer_region" style="float:left;width:560px;"></div>
		</div>
	</div>
	<!--End Speed -->

	<!--Start Matrix -->
	<div class="matrix contenttoggle1" style="display:none;overflow:scroll;width:938px;height:277px;">
		<div id="fullscreentable"></div>
	</div>
	<!--End Matrix -->
</div>
<div id="light" class="white_content" style="display: none;">
	<!--Loading Please wait....
	<a href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a>-->
	<div class="divclose">
		<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X" />
		</a>
	</div>

	<script type="text/javascript">
		var lastchar = '';
		function moverightarea(){
			var val      = document.getElementById('custom_attribute').innerHTML;
			var selected = 0;
			for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
				var avail_attribute = document.micaform1.avail_attribute;
				if(document.micaform1.avail_attribute[i].selected){
					selected = 1;
					if(lastchar=='attr'){
						alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
					}else{
						document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
						lastchar = 'attr';
					}
				}
			}
			if(selected==0){
				alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
			}
		}

		function addplussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"+";
				lastchar = 'opr';
			}
		}

		function addminussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"-";
				lastchar = 'opr';
			}
		}

		function addmultiplysign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"*";
				lastchar = 'opr';
			}
		}

		function adddivisionsign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"/";
				lastchar = 'opr';
			}
		}

		function addLeftBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+"(";
			checkIncompleteFormula();
		}

		function addRightBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+")";
			checkIncompleteFormula();
		}

		function addPersentage(){
			var val = document.getElementById('custom_attribute').innerHTML;
			//if(lastchar=='opr'){
				//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			//}else{
				document.getElementById('custom_attribute').innerHTML = val+"%";
				lastchar = 'opr';
			//}
		}

		function addNumeric(){
			var val       = document.getElementById('custom_attribute').value;
			var customval = document.getElementById('custom_numeric').value;
			document.getElementById('custom_attribute').innerHTML = val+customval;
			lastchar = '';
		}

		function checkIncompleteFormula(){
			var str   = document.getElementById('custom_attribute').innerHTML;
			var left  = str.split("(");
			var right = str.split(")");

			if(left.length==right.length){
				document.getElementById('custom_attribute').style.color="black";
			}else{
				document.getElementById('custom_attribute').style.color="red";
			}
		}

		function Click(val1){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+val1;
			checkIncompleteFormula();
		}

		function checkfilledvalue(){
			var tmp1 = document.getElementById('custom_attribute').innerHTML;
			var tmp2 = document.getElementById('new_name').value;
			var flg  = 0;
			var len1 = parseInt(tmp1.length) - 1;
			var len2 = parseInt(tmp2.length) - 1;
			if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
				flg = 1;
			}

			if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){
				alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
			}else if(lastchar=='opr' && flg == 0){
				alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
			}else{
				if(!validateFormula()){
					return false;
				}
				addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
			}
		}

		function undofield(){
			document.getElementById('custom_attribute').innerHTML = '';
			lastchar = '';
		}

		JQuery("#editvariable").live("click",function(){
			JQuery('#save').attr("id","updatecustom");
			JQuery('#updatecustom').attr("onclick","javascript:void(0)");
			is_updatecustom=1;
			JQuery('#updatecustom').attr("value","Update");
		});

		JQuery(".customedit").live('click',function(){
			if(is_updatecustom == 1){
				//alert(JQuery(this).find(":selected").text());
				JQuery('#new_name').val(JQuery(this).find(":selected").text());
				JQuery('#new_name').attr('disabled', true);
				oldattrval=JQuery(this).find(":selected").val();
				JQuery('#oldattrval').val(oldattrval);
				JQuery('textarea#custom_attribute').text("");
				lastchar = '';
				moverightarea();
			}
		});
	</script>

	<form name="micaform1" id="micaform1" action="index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $itemid; ?>" method="get">
		<table cellspacing="5" cellpadding="0" border="0" width="100%">
			<tr>
				<td colspan="2" align="center">
					<div class="poptitle aa"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" >
					<?php /*<a href="javascript:void(0);" id="editvariable"><img src="< ?php echo JUri::base();? >components/com_mica/images/edit.png" alt="Edit Custom Variable" title="Edit Custom Variable"/>< ?php //echo JText::_('EDIT_CUSTOM_ATTRIBUTE_LABEL');? ></a>*/ ?>
				</td>
				<?php /*<td colspan="2" align="center" ><a href="javascript:void(0);" id="deletevariable">< ?php echo JText::_('DELETE_CUSTOM_ATTRIBUTE_LABEL');? ></a></td>*/ ?>
			</tr>
			<tr>
				<td colspan="2">
					<b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox">
				</td>
			</tr>
			<tr>
				<td width="68%">
					<br><b><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></b>
				</td>
				<td align="right" width="32%">
					<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox">
					<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="button" style="width: 77px;" value="Add Value" onclick="addNumeric();">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2">
					<table cellspacing="0" cellpadding="0" border="1" width="100%">
						<tr valign="top">
							<td width="25%">
								<select name="avail_attribute" id="avail_attribute" size="10" class="avial_select">
									<optgroup label="Default Variable" class="defaultvariable">
										<?php $attr= $this->themeticattribute;//themeticattribute
										$eachattr=explode(",",$attr);
										foreach($eachattr as $eachattrs){
											echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
										} ?>
									</optgroup>
									<?php /*
									<optgroup label="Custom Variable" class="customedit">
										<?php $attr= $this->customattributelib;
										$eachattr=explode(",",$attr);
										$eachattr=array_filter($eachattr);
										foreach($eachattr as $eachattrs){
											$eachattrs=explode(":",$eachattrs);
											echo "<option value='".$eachattrs[1]."'>".JTEXT::_($eachattrs[0])."</option>";
										} ?>
									</optgroup> */ ?>
									<?php /*
									<optgroup label="Custom Library Variable" class="defaultvariablelib">
										<?php $attr= $this->customattributelib;
										$eachattr=explode(",",$attr);
										$eachattr=array_filter($eachattr);
										foreach($eachattr as $eachattrs){
											$eachattrs=explode(":",$eachattrs);
											echo "<option value='".$eachattrs[1]."'>".JTEXT::_($eachattrs[0])."</option>";
										} ?>
									</optgroup>*/ ?>
								</select>
							</td>
							<td width="2%"></td>
							<td width="37%">
								<table  border="1" align="center" cellpadding="0" cellspacing="0" class="cal_t">
									<tr>
										<td colspan="5">
											<input type="button" title="<?php echo JText::_('MOVE_RIGHT_TIP');?>" name="moveright" id="moveright" class="button" style="width: 98%" value=">>" onclick="moverightarea();">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="button"  value="+" onclick="addplussign();">
										</td>
									   	<td width="60">
									   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="button"  value="-" onclick="addminussign();">
									   	</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="button"  value="*" onclick="addmultiplysign();">
										</td>
									   	<td width="60">
									   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="button"  value="/" onclick="adddivisionsign();">
									   	</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="button"  value="%" onclick="addPersentage();">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="button">
										</td>
										<td width="60">
											<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="button">
										</td>
									   	<td width="60">
									   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="button">
									   	</td>
										<td width="60">
											<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="button">
										</td>
									 	<td width="60">
									 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="button">
									 	</td>
									</tr>
									<tr>
										<td width="60">
											<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="button">
										</td>
										<td width="60">
											<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="button">
										</td>
										<td width="60">
											<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="button">
										</td>
										<td width="60">
											<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="button">
										</td>
										<td width="60">
											<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="button">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="button"  value="(" onclick="addLeftBraket();">
										</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="button"  value=")" onclick="addRightBraket();">
										</td>
									</tr>
								</table>
							</td>
						 	<td width="2%"></td>
							<td width="32%">
								<div class="t_area_t">
									<textarea name="custom_attribute" id="custom_attribute" rows="10" cols="30" readonly="readonly" class="inputbox"></textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="5" >
								<div  class="frontbutton" >
									<input type="button" name="save" id="save" class="button" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
								</div>
								<div class="frontbutton" >
									<input type="button" name="clear" id="clear" class="button" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="undofield();">
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>

<div id="light2" class="white_content2" style="display: none;">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="closePopup();"><img src="media/system/images/closebox.jpeg" alt="X" /></a>
	</div>
	<div class="themeconent"></div>
</div>

<div style="display: none" id="menu">
	<ol style="width:100px;border:1px solid black;">
		<li><a id="compare" href="javascript:void();">Compare</a></li>
	</ol>
</div>

<div id="fade" class="black_overlay"></div>

<input type="hidden" id="statecompare" 		value="<?php echo $app->input->get('villagestate', '', 'raw');?>" />
<input type="hidden" id="districtcompare" 	value="<?php echo $app->input->get('villagedistrict', '', 'raw');?>" />
<input type="hidden" id="towncompare" 		value="<?php echo $app->input->get('villages_villages', '', 'raw');?>" />
<input type="hidden" id="urbancompare" 		value="<?php echo $app->input->get('urban', '', 'raw');?>" />
<input type="hidden" id="comparedata" 		value="1" />
<input type="hidden" id="oldattrval" 		value="" / >

<script  type="text/javascript">
	JQuery(document).ready(function(){
		var myid = JQuery.cookie("tab");

		//Edited Rajesh 28/01/2013
		jQuery('#tq').mouseover(function(){jQuery('#tq').show();});
		jQuery('#tq').click(function(){jQuery('#tq').show();});
		jQuery('#tq').mouseout(function() {jQuery('#tq').css('display','none');});
		//Edited ends

		if(typeof(myid) == "object"){
			JQuery(".toggle1#text").click();
			JQuery("#alldata").click();
		}else if(typeof(myid)=="text"){
			JQuery(".toggle1#text").click();
			JQuery("#alldata").click();
		}else{
			JQuery(".toggle1#"+myid).click();
		}

		JQuery("#vwtoggle").click(function(){
			if(JQuery(this).html() == "+"){
				JQuery(this).html("-");
			}else{
				JQuery(this).html("+");
			}
			JQuery("#vw").click();
		});

		<?php if( $_SERVER['HTTP_REFERER'] != JUri::base().substr($_SERVER['REQUEST_URI'],1) && $_SERVER['HTTP_REFERER'] != "") { ?>
			//JQuery(".toggle#vw").click();
		<?php } else{ ?>
			//JQuery(".toggle#matrix").click();
		<?php } ?>

		JQuery('#jsscrollss1').jScrollPane({verticalArrowPositions: 'os',horizontalArrowPositions: 'os',autoReinitialise: true,autoReinitialiseDelay:1000});
	});

	function onPopupClose(evt) {
		// 'this' is the popup.
		JQuery("#featurePopup").remove();
	}

	function stateCqlFilter(){
		<?php if($this->state!="all") {
			//$stateArray=explode(",",JRequest::GetVar('state'));
			$statetojavascript=str_replace(",","','",$this->state);
			echo " return \"name in ('".$statetojavascript."')\";";
		} ?>
	}

	function districtCqlFilter() {
		<?php if($this->district != "all" && $this->district != "") {
			//$stateArray=explode(",",JRequest::GetVar('district'));
			if(strlen($this->UnselectedDistrict) > $this->district) {
				$statetojavascript = $this->district;
				//if(substr_count($statetojavascript,"362")){
					//$statetojavascript=$statetojavascript.",363";
					//echo "<pre>Inside";
			 	//}
			 	//echo $statetojavascript;exit;
				$statetojavascript = str_replace(",", "','", $statetojavascript);
				echo " return \"".$this->districtsearchvariable." in ('".$statetojavascript."') \";";

			}else{
				$statetojavascript=$this->UnselectedDistrict;
				$statetojavascript=str_replace(",","','",$statetojavascript);
				echo " return \"".$this->districtsearchvariable." not in ('".$statetojavascript."') \";";
			}
		}else if($this->district == "all"){
			echo " return  \"state in ('".$statetojavascript."')\";";
		} ?>
	}

	function cityCqlFilter(){
		<?php  if($this->villages!="all" && $this->villages!="" ){
			//$stateArray=explode(",",JRequest::GetVar('villages'));
			$statetojavascript = $this->villages;
			$statetojavascript = str_replace(",","','",$statetojavascript);
			echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
		}

		if($this->villages == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('villages'));
			echo " return  \"state in ('".$statetojavascript."')\";";
		} ?>
	}

	function urbanCqlFilter(){
		<?php  if($this->urban != "all" && $this->urban != ""){
			//$stateArray=explode(",",JRequest::GetVar('state'));
			$statetojavascript = $this->urban;
			$statetojavascript = str_replace(",","','",$statetojavascript);
			echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
		}

		if($this->urban == "all" ){
			//$stateArray=explode(",",JRequest::GetVar('villages'));
			echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
		} ?>
	}

	function addCustomAttr(attrname,attributevale){
		JQuery.ajax({
			url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
			method  : 'GET',
			success : function(data){
				window.location='index.php?option=com_mica&view=villageshowresults&Itemid=<?php echo $this->Itemid; ?>';
			}
		});
	}

	function reloadCustomAttr(){
		JQuery.ajax({
			url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.getCustomAttr",
			method  : 'GET',
			success : function(data){
				JQuery("#addcustomattribute").html(data);
			}
		});
	}

	JQuery(".delcustom").live("click",function(){
		var myid    = JQuery(this).attr('id');
		var segment = myid.split("_");
		JQuery.ajax({
			url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.deleteCustomAttr&id="+segment[2],
			method  : 'GET',
			success : function(data){
				JQuery("#"+myid).parent("tr").remove();
			}
		});
	});
</script>
</div>
