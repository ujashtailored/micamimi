<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc     = JFactory::getDocument();
$app     = JFactory::getApplication('site');
$db      = JFactory::getDbo();
$session = JFactory::getSession();

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jqueryselect/chosen.css', 'text/css');
?>

<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.cookie.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jqueryselect/chosen.jquery.js"></script>
<script type="text/javascript">
	var choosenjq = JQuery.noConflict();
	choosenjq(document).ready(function(){
		choosenjq("select").chosen();
		choosenjq("#avail_attribute").chosen({
			allow_single_deselect : true
		});
	});
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.mousewheel.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<?/*<script src="<?php echo JURI::base()?>components/com_mica/js/field.js"></script>*/?>
<script src="<?php echo JURI::base()?>components/com_mica/js/swfobject.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amcharts.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/amfallback.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<script type="text/javascript">
	var siteurl = '<?php echo JURI::base();?>';
	JQuery(document).ready(function(){
		<?php if($this->geometry != "0") { ?>

		<?php } else {?>
			map.zoomTo(4);
		<?php } ?>
	});

	JQuery(document).keyup(function(e){$
		if(e.keyCode == "27"){
			document.getElementById('light').style.display = 'none';
			document.getElementById('fade').style.display  = 'none';
		}
	});

	var is_updatecustom=0;
	JQuery(document).ready(function() {

	});

	JQuery(".deleterow").live("click",function(){

		var value = JQuery(this).val();
		var ans   = confirm("Are you Sure to delete Variable "+value);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.deleteattribute&attr="+value,
				type    : 'GET',
				success : function(data){
					JQuery("#"+value).fadeOut();
					window.location='index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});

	JQuery(".deletecustom").live("click",function(){
		var value = JQuery(this).val();
		var ans   = confirm("Are you Sure to delete Custom Variable "+value);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.deleteCustomAttribute&attr="+value,
				type    : 'GET',
				success : function(data){
					JQuery("#"+value).fadeOut();
					window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});

	JQuery(".removecolumn").live("click",function(){
		var value       = JQuery(this).val();
		//var classname = JQuery(this).parent().attr('class');
		var segment     = value.split("`");
		var ans         = confirm("Are you Sure to delete  "+segment[2]);
		if(ans == 1){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=showresults.deleteVariable&table="+segment[0]+"&value="+segment[1],
				type    : 'GET',
				success : function(data){
					//JQuery(this).fadeOut();
					window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
				}
			});
		}
	});
	var compare =1;
</script>


<div class="micacomponentblock">
	<div class="mod_mica" >
		<style type="text/css">
			#tq.thematiceq{display:none; position:absolute; z-index:9999; background:#9D9F9E ; margin:30px 0px 0px -115px; padding:0px 30px !important;}
		</style>
		<div class="collpsable">
			<span class="toggle1 saperator" id="text"><?php echo JText::_('TEXT_TAB_LABEL'); ?></span>
			<span id="graphs" class="toggle1 endsap" ><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></span>
			<span id="gis" class="toggle1 saperator"><?php echo JText::_('GIS_TAB_LABEL'); ?></span>
			<span id="matrix" class="toggle1 saperator"  ><?php echo JText::_('Matrix'); ?>  </span>
			<span id="tq" class="toggle1 saperator thematiceq"><?php echo JText::_('ThematicQuery'); ?></span>
			<span class="toggle1 saperator" id="speedometer" style="border-right:none;"> <?php echo JText::_('Speedometer'); ?> </span>
			<span  class="modify " ><?php echo $this->workspacelinkdiv; ?></span>
		</div>

		<div class="text contenttoggle1" style="display:none;">
			<div class="collpsable1">
				<?php /* <span class="toggle2 saperator" id="alldata">< ?php echo JText::_('TEXT_TAB_LABEL'); ? ></span><span class="toggle2 saperator" id="compare">< ?php echo JText::_('COMPARE_TAB_LABEL'); ? ></span>*/ ?>
				<span style="float: right;"  >
					<table cellspacing="0" cellpadding="0" border="0" style="margin:5px 5px 0px 0px;">
						<tr>
							<td >
								<a href="#alldata" onclick="downloadAction();">
									<img src="<?php echo JURI::base();?>components/com_mica/images/export_excel.png" title="Export to Excel"  />
								</a>
							</td>
							<td>
								<a href="#alldata" onclick="downloadAction();">&nbsp;<?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?></a>
							</td>
						</tr>
					</table>
				</span>
			</div>
			<div class="activecontent" >
				<div class="compare contenttoggle2" style="display:block;">
					<div style="width:625px;float:left;">
						<form name="micaform" id="micaform" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>" method="POST">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="padding-left: 20px;">
								<tr>
									<td align="left"><b><?php echo JText::_('SELECT_CPR_MSG');?></b></td>
								</tr>
								<tr>
									<td align="left">
										<div class="maintable">
											<div class="left"><b><?php echo JText::_('STATE_LABEL'); ?></b>:&nbsp;</div>
											<div class="right">
												<select name="state" id="state" class="inputbox-mainscr" onchange="getdistrict(this.value)">
													<option value="all"><?php echo JText::_('ALL');?></option>
													<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/ ?>
													<?php for($i = 0; $i < count($this->state_items); $i++){ ?>
														<option value="<?php echo $this->state_items[$i]->name; ?>">
															<?php echo $this->state_items[$i]->name; ?>
														</option>
													<?php  } ?>
												</select>
											</div>
										</div>
									</td>
								</tr>
								<tr >
									<td align="left">

										<?php if($this->district != "") {?>
											<div class="districtspan maintable level2" >
												<div class="left"><b><?php echo JText::_('DISCTRICT_LABEL'); ?></b> :&nbsp;</div>
												<div class="right">
													<span id="districtspan">
														<select name="district" id="district" class="inputbox">
															<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
														</select>
													</span>
												</div>
											</div>
										<?php } else if ($this->urban != "") { ?>
											<div class="urbanspan maintable level2">
												<div class="left"><b><?php echo JText::_('URBAN_LABEL'); ?></b>:&nbsp;</div>
												<div class="right">
													<span id="urbanspan">
														<select name="urban" id="urban" class="inputbox">
															<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
														</select>
													</span>
												</div>
											</div>
										<?php } else if($this->town != "") {?>
											<div class="townspan maintable level2">
												<div class="left"><b><?php echo JText::_('TOWN_LABEL'); ?></b>:&nbsp;</div>
												<div class="right">
													<span id="townspan">
														<select name="town" id="town" class="inputbox">
															<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
														</select>
													</span>
												</div>
											</div>
										<?php }?>
									</td>
								</tr>
								<tr>
									<td align="left"><br>
										<div class="right frontbutton">
											<input type="submit" name="submit" id="submit" class="" value="<?php echo JText::_('COMPARE_DATA'); ?>" style="width:100px;margin-left: 7px;">
										</div>
										<input type="hidden" name="option" value="com_mica" />
										<input type="hidden" name="zoom" id="zoom"value="" />
										<input type="hidden" name="view" value="showresults" />
										<input type="hidden" name="Itemid" value="<?php echo $this->Itemid;?>" />
										<input type="hidden" name="depricatedm_type" value="<?php echo $this->m_type;?>" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php if ($app->input->get('state') != "all" && $app->input->get('district') != "all" && $app->input->get('town') != "all"
						&& $app->input->get('urban') != "all" && count($this->customdata) != 1 ) { ?>
					<div style="width:285px;float:left;">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<th><b>Remove <?php echo $this->dataof;?></b></th>
							</tr>
							<?php foreach($this->customdata as $eachdata){
								$activesearchvariable=$this->activesearchvariable;?>
								<tr>
									<td>
										<a href="index.php?option=com_mica&Itemid=188&task=showresults.setFieldVal&id=<?php echo $eachdata->$activesearchvariable;?>">
											<?php echo $eachdata->name; ?>
										</a>
									</td>
								</tr>
							<?php }?>
						</table>
					</div>
					<?php }?>
				</div>

				<div class="alldata contenttoggle2" id="jsscrollss" style="width:940px;height:524px;max-height:524px;">
					<table cellspacing="0" cellpadding="0" border="0" width="100%"  class="draggable" id="datatable">
						<?php echo $this->AttributeTable;?>
						<table>
							<tr>
								<td>
									<?php echo $this->pagination->getPagesLinks(); ?>
								</td>
							</tr>
						</table>

						<?php /*$dist=explode(",",$this->district);
							$pages=round(count($dist)/50);
							if(round($pages) !=0){ ?>
								<tr>
									<td>< ?php $page= JRequest::getVar('page'); echo ($page!="" && $page!=0)? '<a href ="index.php?option=com_mica&view=showresults&Itemid=188&page='.($page-1).'">Previous</a>' :""; ? ></td>
									<td>< ?php $page= JRequest::getVar('page'); echo ($page <=$pages && round($pages) !=0)? '<a href ="index.php?option=com_mica&view=showresults&Itemid=188&page='.($page+1).'">Next</a>' :""; ? ></td>
								</tr>
							<?php } */
						?>
					</table>
					<?php echo "<br /><br />"; ?>
				</div>
			</div>
		</div>

		<div class="graphs contenttoggle1" style="display:none;">

			<table cellspacing="7" cellpadding="0" style="float:left;">
				<tr>
					<td>Chart Type :</td>
					<td>
						<select name="charttype" class="inputbox" id="chartype" onchange="changecharttype(this.value,JQuery('#enablelegend:checked').val());" style="width:121px !important;">
							<option value="amcolumn" selected>Bar Chart</option>
							<option value="amline">line Chart</option>
							<option value=amradar>Radar Chart</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Show Legend</td>
					<td>
						<input type="checkbox" id="enablelegend" name="enablelegend" onclick="changecharttype(JQuery('#chartype').val(),this.checked);" value="1" />
					</td>
				</tr>
			</table>

			<table style="float:left;" cellspacing="0" cellpadding="0">
				<tr>
					<td rowspan="2" style="padding: 10px;">
						<?php $variableoptions="";
						foreach($this->customdata[0] as $key => $val){
							if($key!="name" && $key!="state" && $key!="distshp" && $key!="OGR_FID"){
								$variableoptions.="<li style='width='100px;float:left;'><input type='checkbox' class='variablechecked' value='".$key."' />".JTEXT::_($key)."</li>";
							}
						}
						$variableopopup = '<div id="light1007" class="white_content2" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1007\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select Variable</div>';
						$distopopup     = '<div id="light1008" class="white_content2" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light1008\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">Select District</div>';
						$distoptions    ='<ul id="selectdistrictgraph" style="list-style:none;"></ul>'; ?>

						<?php echo $distopopup.$distoptions."</div>";?>
						<a href="javascript:void();" id="graphdistrict" onClick="document.getElementById('light1008').style.display='block';document.getElementById('fade').style.display='block'">Select District</a>
					</td>
					<td rowspan="2" style="padding-right: 10px;">
						<a href="javascript:void();" id="graphvariable" onClick="document.getElementById('light1007').style.display='block';document.getElementById('fade').style.display='block'">Select Variable</a>
						<?php echo $variableopopup."<ul style='list-style:none;'>".$variableoptions."</ul></div>";?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><div id="showchart" style="padding-top: 0;" class="right"><input type="button"  name="showchart"  class="button"value="Show Chart" /> </div></td>
				</tr>
			</table>

			<table style="float:right;" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div id="exportchartbutton" style="padding-top: 0;" class="right">
							<input type="button"  name="downloadchart" onclick="exportImage(JQuery('#chartype option:selected').text());" class="button"value="Export" />
						</div>
					</td>
					<td></td>
				</tr>
			</table>

			<script type="text/javascript">
				var swfurl         = "";
				var amFallbacktype = "";
				var datastr        = "<?php echo $this->graph;?>";
				var grpsetting     = "<?php echo $this->GraphSettings;?>";

				changecharttype("amcolumn",0);

				function changecharttype(value,legend){
					if(legend=="undefined"){
						legend = 0;
					}
					swfurl ="<?php echo JURI::Base();?>components/com_mica/flash/"+value+".swf";
					amFallbacktype=value;
					JQuery("#chartdiv").html("");
					loadChart(swfurl,amFallbacktype,legend);
				}

				function loadChart(swfurl,amFallbacktype,legend){
					var params   = { bgcolor:"#FFFFFF", wmode: "opaque" };
					var s        = datastr.split('sln');
					var new_data = '';

					for(var i=0;i<s.length;i++){
						new_data += s[i];
						if(i-1 != s.length){
							new_data += '\n';
						}
					}

					//console.log(new_data);
					var flashVars = {
						path           : "components/com_mica/flash/",
						chart_data     : new_data,
						chart_settings : "<settings><angle>0</angle><depth>0</depth><redraw>true</redraw><data_type>csv</data_type><thousands_separator>,</thousands_separator><plot_area><margins><left>50</left><right>50</right><top>50</top><bottom>75</bottom></margins></plot_area><grid><category><dashed>1</dashed><dash_length>4</dash_length></category><value><dashed>1</dashed><dash_length>4</dash_length></value></grid><axes><category><width>.5</width><color>E7E7E7</color></category><value><width>.5</width><color>E7E7E7</color></value></axes><values><value><min>0</min></value></values><legend><enabled>"+legend+"</enabled></legend><angle>90</angle><depth>0</depth><column><hover_color>#EED600</hover_color><type>clustered</type><width>85</width><spacing>10</spacing><balloon_text>{title} : {value}</balloon_text><grow_time>3</grow_time><sequenced_grow>.5</sequenced_grow><data_labels_position>inside</data_labels_position></column><graphs>"+grpsetting+"</graphs><labels><label lid='0'><text><![CDATA[]]></text><y>18</y><text_color>000000</text_color><text_size>13</text_size><align>center</align></label></labels></settings>"
					};

					swfobject.embedSWF(swfurl, "chartdiv", "924", "600", "8.0.0", "<?php echo JURI::Base();?>components/com_mica/flash/expressInstall.swf", flashVars, params);

					// change 8 to 80 to test javascript version
					/*if(swfobject.hasFlashPlayerVersion("8")){
						swfobject.embedSWF(swfurl, "chartdiv", "924", "600", "8.0.0", "<?php echo JURI::Base();?>components/com_mica/flash/expressInstall.swf", flashVars, params);
						//var flashMovie = document.getElementById('chartdiv');
						//flashMovie.exportImage();
					}else{
						var amFallback = new AmCharts.AmFallback();
						// amFallback.settingsFile = flashVars.settings_file;  		// doesn't support multiple settings files or additional_chart_settins as flash does
						// amFallback.dataFile = flashVars.data_file;
						amFallback.chartSettings = flashVars.chart_settings;
						amFallback.pathToImages = "components/com_mica/images/amcharts/";
						//amFallback.settingsFile = flashVars.settings_file;
						amFallback.chartData = flashVars.chart_data;
						//amFallback.dataFile = flashVars.data_file;
						amFallback.type = amFallbacktype;
						amFallback.write("chartdiv");
					}*/
				}
			</script>
			<style></style>
			<div id="chartdiv" style="width:100%; height:400px; background-color:#FFFFFF"></div>
			<!-- end of amcharts script -->
		</div>

		<div class="tq contenttoggle1" >
			<div class="" style="width:100%;float:left;">
				<div class="thematicarea clearfix">
					<div class="blockcontent">
						<?php function getCustomAttrName($name,$activeworkspace) {
							$db    = JFactory::getDBO();
							$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
								WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
									AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
							$db->setQuery($query);
							$result = $db->loadAssoc();

							if(count($result) == 0){
								return array($name,$name);
							}else{
								return array($result[0],$result[1]);
							}
						}

						$activeworkspace = $this->activeworkspace;

						$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
							WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
							ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
						$db->setQuery($query);
						$result = $db->loadObjectList();

						$id    = array();
						$level = array();
						$i     = 1;
						$j     = 1;
						$grouping = array();
						foreach($result as $range){
							if($range->level == "0"){
								$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
							}else{
								$pin = str_replace("#","",$range->color);
								$str = " <div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
							}

							$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
							//$id[] = $range->custom_formula;
							//$grouping[$range->custom_formula] = $range->level;
							$level[$range->custom_formula][] = $range->level;
							$i++;
							$j++;
						}

						$i            = 0;
						$range        = array();
						$str          = array();
						$totalcount   = 0;
						$l            = 0;
						$tojavascript = array();
						$grpname = array();
						foreach($grouping as $key => $val){
							$grname         = getCustomAttrName($key, $activeworkspace);
							$grpname[]      = $grname;
							$str[]          = implode(" ",$val);
							$ranges[]       = count($val);
							$levelunique[]  = $level[$key][0];
							$tojavascript[] = $key;
							$l++;
						}
						$tojavascript = implode(",",$tojavascript);
						//$str="";
						for($i = 0; $i < count($grpname); $i++){
							echo "<div class='contentblock1 '>
								<div class='blockcontent'>
									<ul class='maingrp bullet-add clear' >
										<li>
											<div class='themehead'>".$grpname[$i][0]."</div>
											<div class='themeedit'>
												<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
													<img src='".JUri::base()."components/com_mica/images/edit.png' />
												</a>
												<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
													&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
												</a>
											</div>
											<ul class='range' >".$str[$i]."</ul>
										</li>
									</ul>
								</div>
							</div>";
						} ?>
						<div class='contentblock3'>
							<script type='text/javascript'>
								var totalthemecount     = "<?php echo count($grpname); ?>";
								var usedlevel           = '<?php echo implode(",",$levelunique); ?>';
								var havingthematicquery = '<?php echo $tojavascript; ?>';
							</script>
							<div id="themeconent">
								<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>" method="get" >
									<table cellspacing="4" cellpadding="0" border="0" width="90%" style="float: left;width: 335px;">
										<tr>
											<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL');?></h3></td>
										</tr>
										<tr>
											<td>
												<b><?php echo JText::_('ATTRIBUTE_LABEL');?></b>
											</td>
											<td>
												<select name="thematic_attribute" id="thematic_attribute" class="inputbox" style="width: 200px;" onchange="getMinmaxVariable(this.value);">
													<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
													<optgroup label="Default Variable">
														<?php $attr = $this->themeticattribute;
														$eachattr = explode(",",$attr);
														foreach($eachattr as $eachattrs){
															echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
														} ?>
													</optgroup>
													<optgroup label="Custom Variable">
														<?php $attr = $this->customattribute;
														$eachattr = explode(",",$attr);
														$eachattr = array_filter($eachattr);
														foreach($eachattr as $eachattrs){
															$eachattrs=explode(":",$eachattrs);
															echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
														} ?>
													</optgroup>
												</select>
											</td>
										</tr>
										<tr id="minmaxdisplay">
											<td></td>
											<td class="minmaxdisplay"></td>
										</tr>
										<tr>
											<td>
												<b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b>
											</td>
											<td>
												<input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;">
											</td>
										</tr>
										<tr class="colorhide">
											<td>
												<b><?php echo JText::_('SELECT_COLOR_LABEL');?></b>
											</td>
										<td>
											<input class="simple_color" value="" style="width: 194px;"/>
										</td>
										</tr>
										<tr>
											<td colspan="2">
												<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval"></table>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<div class="readon frontbutton" style="float: left;">
													<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" />
												</div>
												<div class="readon frontbutton" style="float: left;padding-left: 5px;">
													<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup();"/>
												<div>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Thematic -->

		<!--Start Speed -->
		<div class="speedometer contenttoggle1" style="display:none;">
			<table cellspacing="4" style="float:left;">
				<thead style="display:none;" id="speedfiltershow">
					<th class="rightgrptext frontbutton" >
						<input style="margin-left:3px;"type="button" class="frontbutton" value="Edit Filter" name="editfilter"/>
					</th>
				</thead>
				<tr class="sfilter">
					<td colspan="2">
						<table>
							<tr>
								<td>
									<b><?php echo JText::_('FILTER_BY');?></b>&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td>
									<input type="radio" name="filter" value="0" class="filterspeed"/></td><td><?php echo JText::_('FILTER_BY_LAYER');?>
								</td>
								<td>
									<input type="radio" name="filter" value="1" class="filterspeed"/></td><td> <?php echo JText::_('FILTER_BY_VARIABLE');?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="sfilter">
					<td colspan="2">
						<div class="lft speed" style="width:210px;display:none;" >
							<table cellspacing="4">
								<tr>
									<td>
										<select name="speed_variable" id="speed_variable" class="inputbox" style="width: 200px;" multiple>
											<?php /*<option value="0">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/
											$attr     = $this->themeticattribute;
											$eachattr = explode(",",$attr);
											foreach($eachattr as $eachattrs){
												echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
											} ?>
										</select>
									</td>
									</tr>
								<tr>
									<td>
										<select name="speed_region" id="speed_region" class="inputbox" style="width: 200px;" multiple >
											<?php /*<option value="0">< ?php echo JText::_('PLEASE_SELECT');? ></option>*/
											foreach($this->customdata as $eachdata){
												echo '<option value='.str_replace(" ","%20",$eachdata->name).'>'.$eachdata->name.'</option>';
											} ?>
										</select>
									</td>
								</tr>
								<tr>
									<td>
										<div class="left frontbutton ">
											<input type="button" name="showspeed" id="showspeed" class="" value="Create">
										</div>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<div id="spedometer_region" style="float:left;width:560px;"></div>
		</div>
	</div>
	<!--End Speed -->

	<!--Start Matrix -->
	<div class="matrix contenttoggle1" style="display:none;overflow:scroll;width:938px;height:277px;">
		<div id="fullscreentable"></div>
	</div>
	<!--End Matrix -->

	<div class="gis contenttoggle1" style="display:none;height:707px !important;">
		<?php /*<img alt="India Map" src="components/com_mica/images/ind-map-3.gif">
		<script src="http://openlayers.org/api/OpenLayers.js"></script>*/ ?>
		<script src="components/com_mica/maps/OpenLayers.js"></script>
		<link rel="stylesheet" href="components/com_mica/maps/theme/default/style.css" type="text/css">
		<link rel="stylesheet" href="components/com_mica/maps/examples/style.css" type="text/css">
		<div id="map"  style="width: 924px; border: 1px solid #C5E1FC;height: 652px;float:left;">
			<table cellspacing="0" cellpadding="0" border="0" style="margin:0px 5px 5px 5px;width:917px;">
				<tbody>
					<tr>
						<td>
							<a href="javascript:void(0);" onclick="downloadMapPdf();" id="options" class="frontbutton" style="text-decoration: none;">
								<input style="margin-right: 3px;" type="button" class="frontbutton" name="Export" value="Export">
								<?php /*<img  title="Export Map" src="< ?php echo JURI::base();? >/components/com_mica/images/export.png" /> */ ?>
							</a>
						</td>
						<td>
							<?php  $fromthematic = $session->get('fromthematic'); ?>
							<a href="javascript:void(0);" id="fullscreen" class="frontbutton" <?php if($fromthematic==1){ ?> fromthematic="1" <?php }else{?> fromthematic="0"  <?php }?> >
								<input style="margin-right: 3px;" type="button" class="frontbutton" name="fullscreen" value="Full Screen">
							</a>
						</td>
					</tr>
				</tbody>
			</table>

			<?php //echo $this->tomcaturl; exit;?>

			<script type="text/javascript" id="j1">
				var globalcorrdinates  = "";
				// var map             = new OpenLayers.Map("map");
				var oldzoom            = 0;
				var geo                = [];
				var dystate            = [];
				var geo                = [];
				//OpenLayers.ProxyHost = "http://192.168.5.159/cgi-bin/proxy.cgi?url=";
				// use a CQL parser for easy filter creation
				var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;
				OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
				var tomcaturl="<?php echo $this->tomcaturl; ?>";

				OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
					defaultHandlerOptions: {
						'single'         : true,
						'double'         : false,
						'pixelTolerance' : 0,
						'stopSingle'     : true,
						'stopDouble'     : false
					},
					initialize: function(options) {
						this.handlerOptions = OpenLayers.Util.extend(
							{}, this.defaultHandlerOptions
						);
						OpenLayers.Control.prototype.initialize.apply(
							this, arguments
						);
						this.handler = new OpenLayers.Handler.Click(
							this, {
								'click':this.getfeaturenfo,'dblclick': this.onDblclick
							}, this.handlerOptions
						);
					},
					onDblclick: function(e) {
						JQuery(".olPopup").css({"display":"none"});
						JQuery("#featurePopup_close").css({"display":"none"});
						//changeAttrOnZoom(map.zoom);
					},
					getfeaturenfo :function(e) {
						coordinates = e;
						var params = {
							REQUEST       : "GetFeatureInfo",
							projection    : "EPSG:4326",
							EXCEPTIONS    : "application/vnd.ogc.se_xml",
							BBOX          : map.getExtent().toBBOX(10),
							SERVICE       : "WMS",
							INFO_FORMAT   : 'text/html',
							QUERY_LAYERS  : selectlayer(map.zoom),
							FEATURE_COUNT : 6,
							Layers        : selectlayer(map.zoom),
							WIDTH         : map.size.w,
							HEIGHT        : map.size.h,
							X             : parseInt(e.xy.x),
							Y             : parseInt(e.xy.y),
							CQL_FILTER    : selectfilter(),
							srs           : map.layers[0].params.SRS
						};

						// handle the wms 1.3 vs wms 1.1 madness
						if(map.layers[0].params.VERSION == "1.3.0") {
							params.version = "1.3.0";
							params.i       = e.xy.x;
							params.j       = e.xy.y;
						} else {
							params.version = "1.1.1";
							params.y       = parseInt(e.xy.y);
							params.x       = parseInt(e.xy.x);
						}
						OpenLayers.loadURL("<?php echo $this->tomcaturl; ?>", params, this, setHTML, setHTML);
						var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
						//lonLat.transform(map.displayProjection,map.getProjectionObject());
						//map.setCenter(lonLat, map.zoom);
						map.panTo(lonLat);
						//map.addControl(ovControl);
						//OpenLayers.Event.stop(e);
					}
				});

				function setHTML(response){
					changeAttrOnZoom(map.zoom,response);
				};

				function selectfilter(){
					if(javazoom==5 || javazoom==6 ){
						return stateCqlFilter();
					}else if(javazoom==7){
						return districtCqlFilter();
					}else  if(javazoom==8){
						return urbanCqlFilter();
					}else{
						return cityCqlFilter();
					}
				}

				function selectlayer(cuurzoom){
					if(javazoom==5 || javazoom==6){
						return "rail_state";
					}else if(javazoom==7){
						return "india_information";
					}else  if(javazoom==8){
						return "jos_mica_urban_agglomeration";
					}else{
						return "my_table";
					}
				}

  				var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
				var options = {
					controls      : [],
					maxExtent     : bounds,
					projection    : "EPSG:4326",
					maxResolution : 'auto',
					zoom          :<?php echo $this->zoom;?>,
					units         : 'degrees'
				};
	   			map = new OpenLayers.Map('map', options);
				var land = new OpenLayers.Layer.WMS("State Boundaries",
					"<?php echo $this->tomcaturl; ?>",
					{
						Layer       : 'india:rail_state',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : stateCqlFilter(),
						SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/rail_state.sld'; ?>"
					},
					{
						isBaseLayer: false
					}
				);

				var opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
					"<?php echo $this->tomcaturl; ?>",
					{
						Layers           : 'india:rail_state',
						transparent      : false,
						format           : 'image/png',
						styles           : "dummy_state",
						transitionEffect : 'resize'
					},
					{
						isBaseLayer: true
					}
				);

				var districts = new OpenLayers.Layer.WMS("districts",
					"<?php echo $this->tomcaturl; ?>",
					{
						Layer       : 'india:india_information',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : districtCqlFilter(),
						SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/india_information.sld'; ?>"
					},
					{
						isBaseLayer: false
					},
					{
						transitionEffect: 'resize'
					}
				);

				var cities = new OpenLayers.Layer.WMS("Cities",
					"<?php echo $this->tomcaturl; ?>",
					{
						Layer       : 'india:my_table',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : cityCqlFilter(),
						SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/my_table.sld'; ?>"
					},
					{
						isBaseLayer : false
					},
					{
						transitionEffect : 'resize'
					}
				);

				var urban = new OpenLayers.Layer.WMS("Urban",
					"<?php echo $this->tomcaturl; ?>",
					{
						Layer       : 'india:jos_mica_urban_agglomeration',
						transparent : true,
						format      : 'image/png',
						CQL_FILTER  : urbanCqlFilter(),
						SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/jos_mica_urban_agglomeration.sld'; ?>"
					},
					{
						isBaseLayer: false
					},
					{
						transitionEffect: 'resize'
					}
				);

				if(map.zoom==5 || map.zoom==6 ){
					map.addLayers([land]);
				}else if(map.zoom==7){
					//map.removeLayer(land,districts);
					//map.addLayers([districts,land]);
					map.addLayers([districts]);
					//land.setVisibility(false);
				}else if(map.zoom==8){
					//map.removeLayer(land,cities);
					//map.addLayers([cities,land]);
					map.addLayers([urban]);
					//land.setVisibility(false);
					// districts.setVisibility(false);
				}else{
					map.addLayers([cities]);
					//land.setVisibility(false);
				}

				map.div.oncontextmenu = function noContextMenu(e) {
					if (OpenLayers.Event.isRightClick(e)){
						displaymenu(e);
					}
					// return false; //cancel the right click of brower
				};

				//map.addControl(exportMapControl);
	 			map.addControl(new OpenLayers.Control.PanZoomBar({
					position : new OpenLayers.Pixel(5, 15)
				}));
				map.addControl(new OpenLayers.Control.Navigation({
					dragPanOptions: {enableKinetic: true}
				}));

			   	//map.addControl(new OpenLayers.Control.Scale($('scale')));
				map.addControl(new OpenLayers.Control.Attribution());
				//map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
				//map.addControl(new OpenLayers.Control.LayerSwitcher());
				//alert(bounds);

				var click = new OpenLayers.Control.Click();
				map.addControl(click);
				click.activate();
				map.zoomTo(<?php echo $this->zoom;?>);

				<?php  if($this->geometry != "0" && $this->geometry != "") { ?>
					var format    = new OpenLayers.Format.WKT();
					var feature   = format.read('<?php echo $this->geometry;?>');
					console.log('<?php echo $this->geometry;?>');
					var homepoint = feature.geometry.getCentroid();
				<?php } ?>

				map.addLayers([opverviewland]);
				//if (!map.getCenter(bounds))
					//map.zoomToMaxExtent();
				map.zoomToExtent(bounds);
				//map.zoomToMaxExtent();

				function onPopupClose(evt) {
					// 'this' is the popup.
					JQuery("#featurePopup").remove();
				}

				function displaymenu(e){
					//$("#menupopup").remove();
					//var OuterDiv=$('<div  />');
					//OuterDiv.append($("#menu").html());
					//OuterDiv.append($("#menu").html());
					//console.log(e);
					//OuterDiv.css({"z-index":"100000","left":e.screenX,"top":e.screenY,"display":"block"});
					//console.log({e.layerX,e.layerY});

					// popup = new OpenLayers.Popup("menupopup",map.getLonLatFromPixel(new OpenLayers.Pixel({e.layerX,e.layerY})),new OpenLayers.Size(200,300),$("#menu").html(), false);
					//	map.popup = popup;
					//evt.feature = feature;
					//	map.addPopup(popup);
				}

				function onFeatureSelect(response) {
					response = response.trim("");
					//console.log("response=> "+response);
					var getsegment=response.split("OGR_FID:");
					console.log("getsegment=> "+getsegment);
					var id;

					if(typeof(getsegment[1])=="undefined"){
						return false;
					}else{
						id=getsegment[1].trim();
						//alert(getsegment[3]);
						if(typeof(getsegment[2])!="undefined" ){
						  if(getsegment[2]==362 || getsegment[2]==344 || getsegment[2]==205 || getsegment[2]==520 || getsegment[2]==210 || getsegment[2]==211 || getsegment[2]==206)
								id = getsegment[2].trim();
						   if(getsegment[3]==209)
										  id=getsegment[3].trim();
							if(getsegment[4]==209)
										  id=getsegment[4].trim();
							if(getsegment[3]==207)
										  id=getsegment[3].trim();
							if(getsegment[1]==205)
										  id=getsegment[1].trim();
							if(getsegment[1]==206 && getsegment[2]==207 && getsegment[3]==212)
										  id=getsegment[2].trim();

						}
					}

					id = parseInt(id);
					JQuery(".olPopup").css({"display":"none"});

					JQuery.ajax({
						url     : "index.php?option=com_mica&task=showresults.popupAttributes&id="+id+"&zoom="+javazoom,
						method  : 'GET',
						success : function(data){
							popup = new OpenLayers.Popup.FramedCloud("featurePopup",
								map.getLonLatFromPixel(new OpenLayers.Pixel(coordinates.xy.x,coordinates.xy.y)),
								new OpenLayers.Size(300,150),
								data,null ,true, onPopupClose
							);
							map.popup = popup;
							//evt.feature = feature;
							map.addPopup(popup);
						}
					});
				}

				function onFeatureUnselect(evt) {
					feature = evt.feature;
					if (feature.popup) {
						popup.feature = null;
						map.removePopup(feature.popup);
						feature.popup.destroy();
						feature.popup = null;
					}
				}

				<?php  echo "var javazoom= ".$this->zoom.";";?>
				<?php $curlonglat = $app->input->get('longlat', '', 'raw');
				if($curlonglat =="") {
					echo "var curlonglat= '77.941406518221,21.676757633686';";
				}else{
					echo "var curlonglat= '".$app->input->get('longlat','77.941406518221,21.676757633686', 'raw')."';";
				} ?>

				//alert(curlonglat);
				var longlatsegment=curlonglat.split(",");

			 	<?php if($this->geometry!="0") {?>
				   	map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
					map.zoomTo(6);
				<?php } else {?>
				   	map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
					map.zoomTo(4);
				<?php } ?>
				//map.events.register("click", map, regionclick);

				function stateCqlFilter(){
					<?php if($this->state!="all") {
						//$stateArray=explode(",",JRequest::GetVar('state'));
						$statetojavascript=str_replace(",","','",$this->state);
						echo " return \"name in ('".$statetojavascript."')\";";
					} ?>
				}

				function districtCqlFilter() {
					<?php if($this->district != "all" && $this->district != "") {
						//$stateArray=explode(",",JRequest::GetVar('district'));
						if(strlen($this->UnselectedDistrict) > $this->district) {
							$statetojavascript = $this->district;
							//if(substr_count($statetojavascript,"362")){
								//$statetojavascript=$statetojavascript.",363";
								//echo "<pre>Inside";
						 	//}
						 	//echo $statetojavascript;exit;
							$statetojavascript = str_replace(",", "','", $statetojavascript);
							echo " return \"".$this->districtsearchvariable." in ('".$statetojavascript."') \";";

						}else{
							$statetojavascript=$this->UnselectedDistrict;
							$statetojavascript=str_replace(",","','",$statetojavascript);
							echo " return \"".$this->districtsearchvariable." not in ('".$statetojavascript."') \";";
						}
					}else if($this->district == "all"){
						echo " return  \"state in ('".$statetojavascript."')\";";
					} ?>
				}

				function cityCqlFilter(){
					<?php  if($this->town!="all" && $this->town!="" ){
						//$stateArray=explode(",",JRequest::GetVar('town'));
						$statetojavascript = $this->town;
						$statetojavascript = str_replace(",","','",$statetojavascript);
						echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
					}

					if($this->town == "all" ){
						//$stateArray=explode(",",JRequest::GetVar('town'));
						echo " return  \"state in ('".$statetojavascript."')\";";
					} ?>
				}

				function urbanCqlFilter(){
					<?php  if($this->urban != "all" && $this->urban != ""){
						//$stateArray=explode(",",JRequest::GetVar('state'));
						$statetojavascript = $this->urban;
						$statetojavascript = str_replace(",","','",$statetojavascript);
						echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
					}

					if($this->urban == "all" ){
						//$stateArray=explode(",",JRequest::GetVar('town'));
						echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
					} ?>
				}

				function changeAttrOnZoom(javazoom,response){
					/*JQuery.ajax({
						method:"GET",
						url:"index.php?option=com_mica&view=showresults&task=getAttribute&zoom="+parseInt(javazoom),
						success:function(data)
						{  	if(data==-1)
							{
								window.location ='index.php?option=com_user&view=login';
							}
							else if(data==-2)
							{
								var confirmaction=confirm("You haven't select Attributes or your membership has been expired n.Do you want to select/upgrade plan for process further?");
								if(confirmaction==1)
								{
									window.location ='index.php?option=com_acctexp&task=showSubscriptionPlans';
								}
								else
								{

								}
							}
							else
							{	//alert(data);
								if(oldzoom !=javazoom)
								{
								//$("#ajaxattribute").html(data);
								oldzoom=javazoom;
								}
								if(response)

								JQuery("#avail_attribute").html("");
								JQuery("#thematic_attribute").html("");
									var avail_attribute="";
									JQuery('input.statetotal_attributes1[type=checkbox]').each(function () {
									avail_attribute +="<option value='"+JQuery(this).val()+"'>"+JQuery(this).val()+"</option>";
									});
									JQuery("#avail_attribute").html(avail_attribute);
									JQuery("#thematic_attribute").html(avail_attribute);
							}
						}
					});*/
					onFeatureSelect(response.responseText);
				}

				function addCustomAttr(attrname,attributevale){
					JQuery.ajax({
						url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
						method  : 'GET',
						success : function(data){
							window.location='index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
						}
					});
				}

				function reloadCustomAttr(){
					JQuery.ajax({
						url     : "index.php?option=com_mica&task=showresults.getCustomAttr",
						method  : 'GET',
						success : function(data){
							JQuery("#addcustomattribute").html(data);
						}
					});
				}

				JQuery(".delcustom").live("click",function(){
					var myid    = JQuery(this).attr('id');
					var segment = myid.split("_");
					JQuery.ajax({
						url     : "index.php?option=com_mica&task=showresults.deleteCustomAttr&id="+segment[2],
						method  : 'GET',
						success : function(data){
							JQuery("#"+myid).parent("tr").remove();
						}
					});
				});
			</script>
		</div>
	</div>
</div>
<div id="light" class="white_content" style="display: none;">
	<!--Loading Please wait....
	<a href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a>-->
	<div class="divclose">
		<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X" />
		</a>
	</div>

	<script type="text/javascript">
		var lastchar = '';
		function moverightarea(){
			var val      = document.getElementById('custom_attribute').innerHTML;
			var selected = 0;
			for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
				var avail_attribute = document.micaform1.avail_attribute;
				if(document.micaform1.avail_attribute[i].selected){
					selected = 1;
					if(lastchar=='attr'){
						alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
					}else{
						document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
						lastchar = 'attr';
					}
				}
			}
			if(selected==0){
				alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
			}
		}

		function addplussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"+";
				lastchar = 'opr';
			}
		}

		function addminussign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"-";
				lastchar = 'opr';
			}
		}

		function addmultiplysign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"*";
				lastchar = 'opr';
			}
		}

		function adddivisionsign(){
			var val = document.getElementById('custom_attribute').innerHTML;
			if(lastchar=='opr'){
				alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			}else{
				document.getElementById('custom_attribute').innerHTML = val+"/";
				lastchar = 'opr';
			}
		}

		function addLeftBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+"(";
			checkIncompleteFormula();
		}

		function addRightBraket(){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+")";
			checkIncompleteFormula();
		}

		function addPersentage(){
			var val = document.getElementById('custom_attribute').innerHTML;
			//if(lastchar=='opr'){
				//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
			//}else{
				document.getElementById('custom_attribute').innerHTML = val+"%";
				lastchar = 'opr';
			//}
		}

		function addNumeric(){
			var val       = document.getElementById('custom_attribute').value;
			var customval = document.getElementById('custom_numeric').value;
			document.getElementById('custom_attribute').innerHTML = val+customval;
			lastchar = '';
		}

		function checkIncompleteFormula(){
			var str   = document.getElementById('custom_attribute').innerHTML;
			var left  = str.split("(");
			var right = str.split(")");

			if(left.length==right.length){
				document.getElementById('custom_attribute').style.color="black";
			}else{
				document.getElementById('custom_attribute').style.color="red";
			}
		}

		function Click(val1){
			var val = document.getElementById('custom_attribute').innerHTML;
			document.getElementById('custom_attribute').innerHTML = val+val1;
			checkIncompleteFormula();
		}

		function checkfilledvalue(){
			var tmp1 = document.getElementById('custom_attribute').innerHTML;
			var tmp2 = document.getElementById('new_name').value;
			var flg  = 0;
			var len1 = parseInt(tmp1.length) - 1;
			var len2 = parseInt(tmp2.length) - 1;
			if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
				flg = 1;
			}

			if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){
				alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
			}else if(lastchar=='opr' && flg == 0){
				alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
			}else{
				if(!validateFormula()){
					return false;
				}
				addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
			}
		}

		function undofield(){
			document.getElementById('custom_attribute').innerHTML = '';
			lastchar = '';
		}

		JQuery("#editvariable").live("click",function(){
			JQuery('#save').attr("id","updatecustom");
			JQuery('#updatecustom').attr("onclick","javascript:void(0)");
			is_updatecustom=1;
			JQuery('#updatecustom').attr("value","Update");
		});

		JQuery(".customedit").live('click',function(){
			if(is_updatecustom == 1){
				//alert(JQuery(this).find(":selected").text());
				JQuery('#new_name').val(JQuery(this).find(":selected").text());
				JQuery('#new_name').attr('disabled', true);
				oldattrval=JQuery(this).find(":selected").val();
				JQuery('#oldattrval').val(oldattrval);
				JQuery('textarea#custom_attribute').text("");
				lastchar = '';
				moverightarea();
			}
		});
	</script>

	<form name="micaform1" id="micaform1" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="get">
		<table cellspacing="5" cellpadding="0" border="0" width="100%">
			<tr>
				<td colspan="2" align="center">
					<div class="poptitle aa"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" >
					<?php /*<a href="javascript:void(0);" id="editvariable"><img src="< ?php echo JUri::base();? >components/com_mica/images/edit.png" alt="Edit Custom Variable" title="Edit Custom Variable"/>< ?php //echo JText::_('EDIT_CUSTOM_ATTRIBUTE_LABEL');? ></a>*/ ?>
				</td>
				<?php /*<td colspan="2" align="center" ><a href="javascript:void(0);" id="deletevariable">< ?php echo JText::_('DELETE_CUSTOM_ATTRIBUTE_LABEL');? ></a></td>*/ ?>
			</tr>
			<tr>
				<td colspan="2">
					<b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox">
				</td>
			</tr>
			<tr>
				<td width="68%">
					<br><b><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></b>
				</td>
				<td align="right" width="32%">
					<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox">
					<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="button" style="width: 77px;" value="Add Value" onclick="addNumeric();">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2">
					<table cellspacing="0" cellpadding="0" border="1" width="100%">
						<tr valign="top">
							<td width="25%">
								<select name="avail_attribute" id="avail_attribute" size="10" class="avial_select">
									<optgroup label="Default Variable" class="defaultvariable">
										<?php $attr= $this->themeticattribute;//themeticattribute
										$eachattr=explode(",",$attr);
										foreach($eachattr as $eachattrs){
											echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
										} ?>
									</optgroup>
									<?php /*
									<optgroup label="Custom Variable" class="customedit">
										<?php $attr= $this->customattributelib;
										$eachattr=explode(",",$attr);
										$eachattr=array_filter($eachattr);
										foreach($eachattr as $eachattrs){
											$eachattrs=explode(":",$eachattrs);
											echo "<option value='".$eachattrs[1]."'>".JTEXT::_($eachattrs[0])."</option>";
										} ?>
									</optgroup> */ ?>
									<?php /*
									<optgroup label="Custom Library Variable" class="defaultvariablelib">
										<?php $attr= $this->customattributelib;
										$eachattr=explode(",",$attr);
										$eachattr=array_filter($eachattr);
										foreach($eachattr as $eachattrs){
											$eachattrs=explode(":",$eachattrs);
											echo "<option value='".$eachattrs[1]."'>".JTEXT::_($eachattrs[0])."</option>";
										} ?>
									</optgroup>*/ ?>
								</select>
							</td>
							<td width="2%"></td>
							<td width="37%">
								<table  border="1" align="center" cellpadding="0" cellspacing="0" class="cal_t">
									<tr>
										<td colspan="5">
											<input type="button" title="<?php echo JText::_('MOVE_RIGHT_TIP');?>" name="moveright" id="moveright" class="button" style="width: 98%" value=">>" onclick="moverightarea();">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="button"  value="+" onclick="addplussign();">
										</td>
									   	<td width="60">
									   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="button"  value="-" onclick="addminussign();">
									   	</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="button"  value="*" onclick="addmultiplysign();">
										</td>
									   	<td width="60">
									   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="button"  value="/" onclick="adddivisionsign();">
									   	</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="button"  value="%" onclick="addPersentage();">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="button">
										</td>
										<td width="60">
											<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="button">
										</td>
									   	<td width="60">
									   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="button">
									   	</td>
										<td width="60">
											<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="button">
										</td>
									 	<td width="60">
									 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="button">
									 	</td>
									</tr>
									<tr>
										<td width="60">
											<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="button">
										</td>
										<td width="60">
											<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="button">
										</td>
										<td width="60">
											<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="button">
										</td>
										<td width="60">
											<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="button">
										</td>
										<td width="60">
											<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="button">
										</td>
									</tr>
									<tr>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="button"  value="(" onclick="addLeftBraket();">
										</td>
										<td width="60">
											<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="button"  value=")" onclick="addRightBraket();">
										</td>
									</tr>
								</table>
							</td>
						 	<td width="2%"></td>
							<td width="32%">
								<div class="t_area_t">
									<textarea name="custom_attribute" id="custom_attribute" rows="10" cols="30" readonly="readonly" class="inputbox"></textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="5" >
								<div  class="frontbutton" >
									<input type="button" name="save" id="save" class="button" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
								</div>
								<div class="frontbutton" >
									<input type="button" name="clear" id="clear" class="button" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="undofield();">
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</div>

<div id="light2" class="white_content2" style="display: none;">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="closePopup();"><img src="media/system/images/closebox.jpeg" alt="X" /></a>
	</div>
	<div class="themeconent"></div>
</div>

<div style="display: none" id="menu">
	<ol style="width:100px;border:1px solid black;">
		<li><a id="compare" href="javascript:void();">Compare</a></li>
	</ol>
</div>

<div id="fade" class="black_overlay"></div>

<input type="hidden" id="statecompare" 		value="<?php echo $app->input->get('state', '', 'raw');?>" />
<input type="hidden" id="districtcompare" 	value="<?php echo $app->input->get('district', '', 'raw');?>" />
<input type="hidden" id="towncompare" 		value="<?php echo $app->input->get('town', '', 'raw');?>" />
<input type="hidden" id="urbancompare" 		value="<?php echo $app->input->get('urban', '', 'raw');?>" />
<input type="hidden" id="comparedata" 		value="1" />
<input type="hidden" id="oldattrval" 		value="" / >

<script  type="text/javascript">
	JQuery(document).ready(function(){
		var myid = JQuery.cookie("tab");

		//Edited rajesh 28/01/2013
		jQuery('#tq').mouseover(function(){jQuery('#tq').show();});
		jQuery('#tq').click(function(){jQuery('#tq').show();});
		jQuery('#tq').mouseout(function() {jQuery('#tq').css('display','none');});
		jQuery('#gis').mouseover(function(){jQuery('#tq').show();});
		jQuery('#gis').mouseout(function() {jQuery('#tq').css('display','none');});
		//Edited ends

		if(typeof(myid) == "object"){
			JQuery(".toggle1#text").click();
			JQuery("#alldata").click();
		}else if(typeof(myid)=="text"){
			JQuery(".toggle1#text").click();
			JQuery("#alldata").click();
		}else{
			JQuery(".toggle1#"+myid).click();
		}

		JQuery("#vwtoggle").click(function(){
			if(JQuery(this).html() == "+"){
				JQuery(this).html("-");
			}else{
				JQuery(this).html("+");
			}
			JQuery("#vw").click();
		});

		<?php if( $_SERVER['HTTP_REFERER'] != JUri::base().substr($_SERVER['REQUEST_URI'],1) && $_SERVER['HTTP_REFERER'] != "") { ?>
			//JQuery(".toggle#vw").click();
		<?php } else{ ?>
			//JQuery(".toggle#matrix").click();
		<?php } ?>

		if(myid == "tq"){
			JQuery(".toggle1#gis").click();
		}

		JQuery('#jsscrollss1').jScrollPane({verticalArrowPositions: 'os',horizontalArrowPositions: 'os',autoReinitialise: true,autoReinitialiseDelay:1000});
		//var pageno      = 2;
		//var contentPane = JQuery("#jsscrollss").data('jsp').getContentPane();

		<?php /* jquery pagination for data
			JQuery(".loadanother").live("mouseover",function(){
				if(pageno > 13){
					return false;
				}
				var totaldist=JQuery("#district").val().split(",");
				var totalpage=totaldist.length/50;

				if(totalpage < pageno){
					return false;
				}
				JQuery(".jspContainer").append("<div id='loading' style='background-color: yellow;position: absolute;text-align: center;width: 99%;height:50px;margin-top: 250px;padding-top:25px;'>Loading....</div>");
				JQuery(".loadanother").removeClass('loadanother');

				JQuery.ajax({
					url     :"index.php?option=com_mica&view=showresults&page="+pageno,
					success :function(data){
						if(data.length >150){
							if(pageno <= 13){
								JQuery("#loading").remove();
								var $response=JQuery(data);
								var filteredhtml=$response.filter("tr:not(.firsttableheader,.secondtableheader,.thirdtableheader)");
								JQuery("#datatable").append(filteredhtml);pageno++;
								JQuery("#jsscrollss").data('jsp').reinitialise();
							}else{
								clearInterval(lodedpage);
								JQuery("#jsscrollss").data('jsp').reinitialise();
							}
						}else{
							pageno=14;
							JQuery(".loadanother").removeClass('loadanother');
							JQuery("#jsscrollss").data('jsp').reinitialise();
						}
					}
				});

			});
		*/ ?>
	});
</script>

</div>
