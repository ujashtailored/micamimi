<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/pdf.php' );


/**
 * Summery Front controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerSummeryfront extends MicaController
{
	//Custom Constructor
	var $tomcat_url             = "";
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $subdistrictsearchvariable ="Sub_District_Code";

	function __construct($default = array())
	{
		parent::__construct($default);

		$this->_table_prefix = '#__mica_';
		//$u    = JURI::getInstance( JURI::base() );
		//$this->tomcat_url="http://".$u->_host."/tomcat/geoserver/india/wms";
		//$this->tomcat_url = "http://http://162.144.250.77:8080/geoserver/india/wms";

		//DEFINE('TOMCAT_URL',$this->tomcat_url);
		if (!defined('TOMCAT_URL'))
		{
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcat_url);
		}

		$app     = JFactory::getApplication();
		$session = JFactory::getSession();

		$user = JFactory::getUser();
		if ($user->id == 0) {
			$session->set('user_id',null);
			JFactory::getApplication()->redirect(JURI::base().'index.php?option=com_users&view=login', $error, 'error' ); //redirect to login page
		}
		else{
			$session->set('user_id', $user->id);
		}

		$reset = $app->input->get('reset', 0, 'int');
		if($reset == 0){
			$session->set('oldattributes', $session->get('attributes'));
		}else{
			$session->set('oldattributes',"");
			//$session->set('villagesattributes',null);
			$session->set('summeryattributes',null);
			//$session->set('villagestate',null);
			//$session->set('villagedistrict',null);
			$session->set('urban',null);
			$session->set('summarystate',null);
			$session->set('summarydistrict',null);
			$session->set('summarySubDistrict',null);
			$session->set('urban',null);
			// Comment this code
			//$session->set('activeworkspace',null);
			$session->set('attrname',null);
			$session->set('customattribute',null);
			$session->set('m_type',null);
			$session->set('activetable',null);
			$session->set('activedata',null);
			$session->set('composite',null);
			$session->set('m_type_rating',null);
			$session->set('customformula',null);
			$session->set('Groupwisedata',null);
			$session->set('popupactivedata',null);
			//$session->set('themeticattribute',null);
			$session->set('activenamevariable',null);
			$session->set('activesearchvariable',null);
			$session->set('dataof',null);
		}

		//////.//////////// Workspace ///////////////////////
		if($session->get('activeworkspace') == ""){
			$result = workspaceHelper::loadWorkspace(1);
			if(!empty($result)){
				$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));

			}else{
				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
				//$app->redirect("index.php?option=com_mica&view=summeryresults&Itemid=188");
			}

		}else if($session->get('is_default') == 1){

			workspaceHelper::updateWorkspace($innercall);

		}else{

			$app->input->set('activeworkspace', $session->get('activeworkspace'));
			//$this->saveSldToDataBase("1","1");
		}

		$state = $app->input->get('summarystate', '', 'raw');

		$stats = array();
		foreach($state as $eachstat){
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('summarystate',$state);
		}

		$sessionstate = $session->get('summarystate');

		$app->input->set('summarystate', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}

		$session->set('summarystate', $sessionstate);

		$session->set('summarystate', $sessionstate);

		// Set  District in Session
		$district = $app->input->get('summarydistrict', '', 'raw');
		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('summarydistrict', $district);
		}

		$sessiondistrict = $session->get('summarydistrict');

		if($sessiondistrict != ""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}

		$app->input->set('summarydistrict',$sessiondistrict);
		$session->set('summarydistrict',$sessiondistrict);

		// Set  summarySubDistrict in Session
		$subDistrict = $app->input->get('summarySubDistrict', '', 'raw');
		if(is_array($subDistrict)){
			$subDistrict = implode(",", $subDistrict);
			$app->input->set('summarySubDistrict', $subDistrict);
		}

		$sessionSubDistrict = $session->get('summarySubDistrict');

		if($sessionSubDistrict != ""){
			$addtosession1   = ($sessionSubDistrict.",".$subDistrict);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessionSubDistrict = implode(",",$mytmp);
		}else{
			$sessionSubDistrict = $subDistrict;
		}

		$app->input->set('summarySubDistrict',$sessionSubDistrict);
		$session->set('summarySubDistrict',$sessionSubDistrict);
		// end Sub District in Session

		// Comment Code on 1-04-2018 added code above for subDistrict because not getting sub district in session
		// Set Sub District in Session
		/*$subDistrict = $app->input->get('summarySubDistrict', '', 'raw');
		if(is_array($subDistrict)){
			$subDistrict = implode(",", $subDistrict);
			$app->input->set('summarySubDistrict', $subDistrict);
		}

		$session->set('summarySubDistrict',$subDistrict);*/
		// end Sub District in Session

		$m_type = $app->input->get('m_type', '', 'raw');
		//$session_mtype=($session->get('m_type'));
		if(!is_array($m_type)){
			$m_type = $session->get('m_type');
		}else{
			$session->set('m_type', $m_type);
			$m_type = $session->get('m_type');
		}

		$villages    = $app->input->get('villages', '', 'raw');
		$sessiontown = $session->get('villages');
		if($sessiontown != ""){
			$addtosession = ($sessiontown.",".$villages);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",",$mytmp);
		}else{
			$sessiontown = $villages;
		}
		$session->set('villages',$sessiontown);
		$app->input->set('villages',$sessiontown);
		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');
		if($sessionurban !="" )
		{
			$addtosession = ($sessionurban.",".$urban);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",",$mytmp);
			$session->set('dataof',"Urban");
		}
		else
		{
			$sessionurban = $urban;
		}
		$session->set('urban',$sessionurban);
		$app->input->set('urban',$sessionurban);

		if($sessiontown != ""){
			$app->input->set('db_table','Villages');
			$zoom = 9;//JRequest::setVar('db_table','Villages');
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$zoom = 8;//JRequest::setVar('db_table','Urban');
		}else if($sessiondistrict != ""){
			$app->input->set('db_table','District');
			$zoom = 7;//JRequest::setVar('db_table','District');
		}else{
			$app->input->set('db_table','State');
			$zoom = 6;//JRequest::setVar('db_table','State');
		}

		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			//$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}

		$composite = $app->input->get('composite', '', 'raw');
		if(!empty($composite)){
			$session->set('composite', $app->input->get('composite', '', 'raw'));
			//$attributes = JRequest::getVar('attributes');
			$composite = array_merge(array("MPI"), $composite);
		}else{
			$session->clear('composite');
		}

		$attributes = $app->input->get('attributes', '', 'raw');

		if(!empty($attributes)){
			$composite = $session->get('composite');
		    //echo  '<br/>==<br/>'.implode(",",$composite);
		    //echo  '<br/>==<br/>'.implode(",",$attributes);
			//$attributes = JRequest::getVar('attributes');
			if(!empty($composite)){
				$attributes = array_merge(array("MPI"),$attributes,$composite);
			}else{
				$attributes = array_merge(array("MPI"),$attributes);
			}
			//echo  '<br/>==<br/>'.implode(",",$attributes);
		}
		//echo "a<pre>";print_r($attributes);exit;
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}

		$sessionattr = $session->get('summeryattributes');
		if($sessionattr != ""){
			if($app->input->get('summeryattributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('summeryattributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);

		$app->input->set('composite', $session->get('composite'));
		$app->input->set('m_type_rating', $session->get('m_type_rating'));
		$app->input->set('summeryattributes', $session->get('summeryattributes'));
		$app->input->set('summarystate', $session->get('summarystate'));
		$app->input->set('m_type', $session->get('m_type'));
		$app->input->set('preselected', $session->get('summarydistrict'));

		$msg = $app->input->get('msg', '', 'raw');
		if($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}else if($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items            = $this->get('StateItems');
		$this->composite_attr         = $this->get('compositeAttr');
		$this->typesArray             = $this->get('AttributeType');
		$this->industrylevelgroups    = $this->get('IndustryLevelGroups');
		$this->state                  = $sessionstate;
		$this->district               = $sessiondistrict;
		$this->town                   = $sessiontown;
		$this->urban                  = $sessionurban;
		$this->m_type                 = $m_type;
		$this->themeticattribute      = $session->get('themeticattribute');
		$this->user                   = JFactory::getUser();
		$this->userid                 = $this->user->id;
		$this->activeworkspace        = $session->get('activeworkspace');
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;
		$this->zoom                   = $zoom;
		$this->customattributelib = $session->get('customattributelib');
	}

	/**
	 * A task to return Sub-district data for AJAX.
	 *
	 * @return  void
	 */
	public function getSubDistrict()
	{
		$subdistrictsearchvariable = "Sub_District_Code";
		$districtIds               = $_GET['summarydistrict'];
		$session                   = JFactory::getSession();
		$preselectedDistricts      = $session->get('summarySubDistrict');

		if (!is_array($preselectedDistricts))
		{
			if(empty($preselectedDistricts))
				$preselectedDistricts =[];
			else
				$preselectedDistricts = explode(',', $preselectedDistricts);
		}

		if ($districtIds == 'null')
		{
			$districtIds = $session->get('summarydistrict');
		}

		$db = JFactory::getDBO();

		if (!empty($districtIds))
		{
			$districtIds = explode(",", $districtIds);
			$districtIds = implode(",", array_filter($districtIds));
			$districtIds = str_replace(",", "','", $districtIds);

			$inCoumnName = "'" . $districtIds . "'";

			 /*$query = "SELECT " . $subdistrictsearchvariable . ", Sub_District_Code, Sub_District_Name, district, district_name, State_name, india_information_OGR_FID
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('india_information_OGR_FID') . " IN (" . $inCoumnName . ")
				GROUP BY " . $db->quoteName('Sub_District_Code') . "
				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('Sub_District_Name') . " ASC ";*/
			$query = "SELECT DISTINCT " . $subdistrictsearchvariable . ", Sub_District_Code, Sub_District_Name, district, district_name, State_name, india_information_OGR_FID
				FROM " . $db->quoteName('villages_master') . "
				WHERE " . $db->quoteName('india_information_OGR_FID') . " IN (" . $inCoumnName . ") ";
				//GROUP BY " . $db->quoteName('Sub_District_Code') . "
				//ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('Sub_District_Name') . " ASC ";


			$db->setQuery($query);

			try
			{
				$villageData = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$villageData = array();
			}
		}
		else
		{
			$villageData = array();
		}

		//$gpbydist = array();

		// foreach ($villageData as $eachdist)
		// {
		// 	$gpbydist[$eachdist->district_name ." - ".$eachdist->State_name][] = $eachdist;
		// }

		// Array sort
		//ksort($gpbydist);

		//$villageData = $gpbydist;
		$returndata  = '';
		$x           = 0;
		$returndata1= [];
		$groupdistrict1 =[];

		if (count($villageData))
		{
			//$groupdistrict.='<div class="subdistrictslider">';
			foreach ($villageData as $index => $val)
			{
				$key    = $val->district_name ." - ".$val->State_name;
				$result = mb_substr(strtoupper($key), 0, 2);
				$final  =str_replace(' ','',$key);
				//group data
				$returndata1[$key]['group']= '<li class="district_group" id='.$result.'><label class="villages_label">'.$key.'</label><div class="fright"><input type="checkbox" class="allcheck" id="subdistrictlabel_allcheck-'.$final.'"><label>&nbsp;</label></div></li>';

				// round with short name
				$groupdistrict1[$key]="<div><span class='tooltip-txt-box'><a href=#".$result.">".$result."</a><span class='tooltiptext'>".$key."</span></span></div>";

				$class=strtolower(str_replace(" ", "_", $key));
				$class=strtolower(str_replace("&", "_", $class));

				//for ($i = 0; $i < count($val); $i++)
				{


					if(count($preselectedDistricts)>0)
					{
						if (in_array($val->$subdistrictsearchvariable, $preselectedDistricts))
						{
							$selected = "checked";
						}
					}
					else
					{
						$selected = "";
					}

					$returndata1[$key]['data'][$val->Sub_District_Name.'-'.$x]= '<li class="'.$class.'">
							<input type="checkbox" class="subdistrict_checkbox subdistrictlabel_checkbox-'.$key.'" name="summarySubDistrict[]" value="'.$val->Sub_District_Code.'" '.$selected.' id="subdistrict_check_'.$x.'" >
							<label for="subdistrict_check_'.$x.'">'.$val->Sub_District_Name.'</label></li>';
							$x++;
				}
			}
			//$groupdistrict.= '</div>';
		}
		else
		{
			//$returndata .= '<ul class="list1 villagelist"></ul>';
		}
		//$returndata .= '</ul>';

		foreach ($returndata1 as $key => $value) {
			ksort($value['data']);
			# code...
		}
		ksort($returndata1);
		$returndata = implode('', array_map(function ($entry) {
			  return $entry['group']. implode('', $entry['data']);
			}, $returndata1));

		$groupdistrict.='<div class="subdistrictslider">'.implode('', $groupdistrict1).'</div>';

		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $returndata . "split" . $groupdistrict."split".$getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
		exit;
	}

	/**
	 * A task to return data for AJAX.
	 */
	public function getsecondlevel()
	{
		$districtsearchvariable = $this->districtsearchvariable;
		$db                     = JFactory::getDBO();
		$stat                   = $this->input->get('stat', '', 'raw');
		$stat                   = explode(",",$stat);
		$session                = JFactory::getSession();
		$preselected            = $session->get("summarydistrict");
		$preselected =  empty($preselected) ? [] : explode(",", $preselected);

		foreach ($stat as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}

		$query = "SELECT state_code,LOWER(name) as name FROM rail_state";

		$db->setQuery($query);

		try {
			$state_codes = $db->loadAssocList();
		}
		catch (Exception $e) {
		}

		$stat = implode(",", $stats);
		$stat = str_replace(",", "','", $stat);
		$stat = "'" . $stat . "'";

		if ($stat)
		{
			/*$query = "SELECT ".$districtsearchvariable.", distshp, state
				FROM ".$db->quoteName('india_information')."
				WHERE ".$db->quoteName('state')." IN (".$stat.")
					AND ".$db->quoteName('OGR_FID')." != ".$db->quote(490)."
				GROUP BY ".$db->quoteName('distshp').", ".$db->quoteName('state')."
				ORDER BY ".$db->quoteName('state').", ".$db->quoteName('distshp')." ASC ";*/

				//india_information_OGR_FID
			$query = "SELECT DISTINCT district as OGR_FID, District_name as distshp, State_name as state, india_information_OGR_FID as districtId
				FROM ".$db->quoteName('villages_master')."
				WHERE ".$db->quoteName('State_name')." IN (".$stat.") ";
				//GROUP BY ".$db->quoteName('District_name').", ".$db->quoteName('District_name')."
				//ORDER BY ".$db->quoteName('State_name').", ".$db->quoteName('distshp')." ASC ";


			$db->setQuery($query);

			try
			{
				$district_data = $db->loadObjectList();
			}

			catch (Exception $e)
			{
				$district_data = array();
			}
		}
		else
		{
			$district_data = array();
		}

		$state    = array();
		$gpbydist = array();

		// foreach ($district_data as $eachdist)
		// {

		// 	$gpbydist[$eachdist->state][] = $eachdist;
		// }

		//$district_data = $gpbydist;
		$returndata  = '';
		$x           = 0;
		$returndata1 = [];
		$groupstate  ='';
		$groupstate1  =[];

		if (count($district_data))
		{



			foreach ($district_data as $index=>$val)
			{
				$key =$val->state;

				//$returndata .= '<optgroup label="'.$key.'">';
				$state_id = array_search(strtolower($key), array_column($state_codes, 'name'));
				$returndata1[$key]['group']='<li class="district_group" id='.$state_codes[$state_id]['state_code'].'><label class="district_label">'.$key.'</label><div class="fright"><input type="checkbox" class="allcheck" id="districtlabel_allcheck-'.$state_id.'"><label>&nbsp;</label></div></li>';

				$groupstate1[$key]="<div><span class='tooltip-txt-box'><a href=#".$state_codes[$state_id]['state_code'].">".strtoupper($state_codes[$state_id]['state_code'])."</a><span class='tooltiptext'>".$state_codes[$state_id]['name']."</span></span></div>";
				$class=strtolower(str_replace(" ", "_", $key));
				$class=strtolower(str_replace("&", "_", $class));

				//for ($i = 0; $i < count($val); $i++)
				{

					/*if (in_array($val[$i]->$districtsearchvariable, $preselected)){$selected1="checked";}else{$selected1="";}

					$returndata.='<li class="'.$class.'"><input type="checkbox" class="district_checkbox districtlabel_checkbox-'.$state_id.'" name="summarydistrict[]" value="'.$val[$i]->$districtsearchvariable.'" '.$selected1.' id="summarydistrict_'.$x.'" ><label for="summarydistrict_'.$x.'">'.$val[$i]->distshp.'</label></li>';
							$x++;
					*/
					if (!empty($preselected))
					{
						if (in_array($val->districtId, $preselected))
							{$selected1="checked";}
					}
					else
						{$selected1="";}

					$returndata1[$key]['data'][$val->distshp.'-'.$x]='<li class="'.$class.'"><input type="checkbox" class="district_checkbox districtlabel_checkbox-'.$state_id.'" name="summarydistrict[]" value="'.$val->districtId.'" '.$selected1.' id="summarydistrict_'.$x.'" ><label for="summarydistrict_'.$x.'">'.$val->distshp.'</label></li>';
							$x++;
				}


			}


		}
		else
		{

			//$returndata.='<ul class="list1 districtlist"></ul>';
		}
		//$returndata.='</ul>';


        // echo "<pre/>";print_r($returndata);exit;
		/*$getUrbanAgglormationNames=$this->getUrbanAgglormationNamesByStateNames($stat);
		$getUATownNamesByStateNames=$this->getUATownNamesByStateNames($stat);*/

		foreach ($returndata1 as $key => $value) {
			ksort($value['data']);
		}
		ksort($returndata1);
		$returndata = implode('', array_map(function ($entry) {
			  return $entry['group']. implode('', $entry['data']);
			}, $returndata1));

		$groupstate.='<div class="districtslider">'.implode('', $groupstate1).'</div>';
		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";

		echo $returndata."split".$groupstate."split".$getUrbanAgglormationNames."split".$getUATownNamesByStateNames;exit;
	}
	function getUrbanAgglormationNamesByStateNames($state)
	{
		$urbansearchvariable = $this->urbansearchvariable;
		$db                  = JFactory::getDBO();

		if ($state)
		{
			$query = "SELECT ".$urbansearchvariable.",place_name
				FROM ".$db->quoteName('jos_mica_urban_agglomeration')."
				WHERE ".$db->quoteName('place_name')." LIKE '%".$state."%'";
			$db->setQuery($query);
			$urban_data = $db->loadObjectList();
		}
		else
		{
			$urban_data = array();
		}

		$returndata = '';

		if (count($urban_data))
		{
			$returndata .= '<select name="urban" id="urban" class="inputbox" onchange="geturban(this.value)" >';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';

			for ($i=0;$i<count($urban_data);$i++)
			{
				$returndata .= '<option value="'.$urban_data[$i]->$urbansearchvariable.'">'.$urban_data[$i]->place_name.'</option>';
			}

			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="urban" id="urban" class="inputbox" ></select>';
		}

		return $returndata;
	}

	function getUATownNamesByStateNames($state)
	{
		$townsearchvariable = $this->townsearchvariable;
		$db = JFactory::getDBO();

		if ($state)
		{
			$query = "SELECT ".$this->townsearchvariable.",place_name
				FROM ".$db->quoteName('my_table')."
				WHERE ".$db->quoteName('state')." LIKE '%".$state."%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}
		else
		{
			$town_data = array();
		}

		$returndata = '';

		if (count($town_data))
		{
			$returndata .= '<select name="villages" id="villages" class="inputbox" onchange="gettown(this.value)" >';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';

			for ($i=0;$i<count($town_data);$i++)
			{
				$returndata .= '<option value="'.$town_data[$i]->$townsearchvariable.'">'.$town_data[$i]->place_name.'</option>';
			}

			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="villages" id="villages" class="inputbox" onchange="gettown(this.value)"></select>';
		}

		return $returndata;
	}

	function gettown()
	{
		$db       = JFactory::getDBO();
		$stat     = $this->input->get('stat', '', 'raw');
		$district = $this->input->get('district', '', 'raw');

		if ($stat && $district)
		{
			$query = "SELECT id,TownName
				FROM ".$db->quoteName('#__mica_towns')."
				WHERE ".$db->quoteName('StateName')." LIKE '%".$stat."%'
					AND ".$db->quoteName('DistrictName')." LIKE '%".$district."%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}
		else
		{
			$town_data = array();
		}

		$returndata = '';

		if (count($town_data))
		{
			$returndata .= '<select name="villages" id="villages" class="inputbox" onchange="showhidetype(this.value)">';
			$returndata .= '<option value="all">'.JText::_('ALL').'</option>';

			for ($i=0;$i<count($town_data);$i++)
			{
				$returndata .= '<option value="'.$town_data[$i]->TownName.'">'.$town_data[$i]->TownName.'</option>';
			}

			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="villages" id="villages" class="inputbox" onchange="showhidetype(this.value)">
							<option value="">'.JText::_('PLEASE_SELECT').'</option>
							</select>';
		}

		echo $returndata;exit;
	}

	function getStateField()
	{
		$state      = $this->input->get('state', '', 'raw');
		$db         = JFactory::getDBO();
		$query      = "SELECT * FROM ".$db->quoteName('#__mica_state')." WHERE ".$db->quoteName('StateName')." LIKE '%".$state."%' ";
		$db->setQuery($query);
		$state_data = $db->loadObjectList();
		$returnable = array();

		foreach ($state_data as $key=>$val)
		{
			foreach ($val as $key1=>$val1)
			{
				$returnable[$key1]=$val1;
			}
		}

		echo json_encode($state_data);exit;
	}

	function getDistrictField()
	{
		$stat  = $this->input->get('state', '', 'raw');
		$dist  = $this->input->get('dist', '', 'raw');
		$db    = JFactory::getDBO();
		$query = "SELECT * FROM  ".$db->quoteName('#__mica_district_r_u')."
			WHERE ".$db->quoteName('StateName')." LIKE '%".$stat."%'
				AND ".$db->quoteName('DistrictName')." LIKE '%".$dist."%' ";
		$db->setQuery($query);
		$district_data = $db->loadObjectList();

		$returnable = array();

		foreach ($district_data as $key=>$val)
		{
			foreach ($val as $key1=>$val1)
			{
				$returnable[$key1] = $val1;
			}
		}

		echo json_encode($district_data );exit;
	}

	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcat_url = $db->loadResult();
	}
}
