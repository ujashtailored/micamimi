<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

/**
 * MICA Front controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerMysubscription extends MicaController
{
	/**
	 *
	 */
	public function updatePassword()
	{
		$password1 = $this->input->get('password','','raw');
		$password2 = $this->input->get('password2','','raw');

		$user = JFactory::getUser();
		$msg  = JText::_('PASSWORDS_DO_NOT_MATCH');
		if($password1 == $password2 && $password1 != ""){
			$salt     = JUserHelper::genRandomPassword(32);
			$crypt    = JUserHelper::getCryptedPassword($password1, $salt);
			$password = $crypt.':'.$salt;

			$db = JFactory::getDBO();
			$query 	= "UPDATE ".$db->quoteName('#__users')."
				SET ".$db->quoteName('password')." = ".$db->quote($password)."
				WHERE ".$db->quoteName('id')." = ".$db->quote($user->id)."
					AND ".$db->quoteName('block')." = ".$db->quote(0);
			$db->setQuery($query);
			$db->execute();
			$msg = JText::_('PASSWORD_RESET_SUCCESS');
		}

		$return = str_replace(array('"', '<', '>', "'"), '', @$_SERVER['HTTP_REFERER']);
		if (empty($return) || !JURI::isInternal($return)) {
			$return = JURI::base();
		}
		$this->setRedirect(JRoute::_($return, false), $msg, 'error');
		return false;
	}

}
