<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.helper');

class workspaceHelper
{
	function saveWorkspace($innercall = null)
	{
		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();

		$name    = $app->input->get("name", '', 'raw');
		$default = $app->input->get("default", '', 'raw');
		$view    = $app->input->getCmd('view');

		$urban  = $session->get('urban');
		$dataof = $session->get('dataof');

		if ($view == 'villageshowresults')
		{
			$sessionattr     = $session->get('villagesattributes');
			$m_type          = $session->get('villagem_type');
			$state           = $session->get('villagestate');
			$district        = $session->get('villagedistrict');
			$subdistrict     = $session->get('villageSubDistrict');
			$villages        = $session->get('villages_villages');
			$dataAttribute   = 'villagesattributes';
			$datastate       = "villagestate";
			$datadistrict    = "villagedistrict";
			$subdatadistrict = "villageSubDistrict";
			$datavillages    = "villages_villages";
			$datam_type      = "villagem_type";

			$data = array(
				$dataAttribute   => $sessionattr,
				$datastate       => $state,
				$datadistrict    => $district,
				$subdatadistrict => $subdistrict,
				"urban"          => $urban,
				$datavillages    => $villages,
				$datam_type      => $m_type,
				'dataof'         => "villages"
			);
		}
		elseif ($view == 'summaryshowresults')
		{
			$sessionattr     = $session->get('summeryattributes');
			$state           = $session->get('summarystate');
			$district        = $session->get('summarydistrict');
			$subdistrict     = $session->get('summarySubDistrict');
			$dataAttribute   = 'summeryattributes';
			$datastate       = "summarystate";
			$datadistrict    = "summarydistrict";
			$datasubdistrict = "summarySubDistrict";

			$data = array(
				$dataAttribute   => $sessionattr,
				$datastate       => $state,
				$datadistrict    => $district,
				$datasubdistrict => $subdistrict,
				'dataof'         => "villageSummary"
			);
		}
		else if($view == 'showresults')
		{
			$sessionattr = $session->get('attributes');
			$m_type      = $session->get('m_type');
			$state       = $session->get('state');
			$district    = $session->get('district');
			$dataAttribute = "attribute";
			$datastate     = "state";
			$datadistrict  = "district";
			$datam_type    = "m_type";

			$data = array(
				$dataAttribute => $sessionattr,
				$datastate     => $state,
				$datadistrict  => $district,
				"urban"        => $urban,
				$datam_type    => $m_type,
				'dataof'       => $dataof
			);
		}

		//$data = mysql_real_escape_string(serialize($data));
		$data = serialize($data);

		$default = $app->input->get("default", 0);
		if($default == 1){
			$default = 1;
		}else{
			$default = 0;
		}

		if($user->id == 0){
			return -1;
		}

		$query = "INSERT INTO ".$db->quoteName('#__mica_user_workspace')."
			(".$db->quoteName('userid').", ".$db->quoteName('name').", ".$db->quoteName('data').", ".$db->quoteName('is_default').", ".$db->quoteName('create_date').")
			VALUES ( ".$db->quote($user->id).", ".$db->quote($name).", ".$db->quote($data).", ".$db->quote($default).", now() )";

		$db->setQuery($query);
		$db->execute();

		if($innercall == null){
			exit;
		}else{

			$id = $db->insertid();
			$session->set('activeworkspace',$id);

			$customattribute = $session->get('customattribute');
			$customattribute = explode(",",$customattribute);
			$customattribute = array_filter($customattribute);

			$activetable = $session->get("activetable");
			foreach($customattribute as $row){
				$seg   = explode(":",$row);
				$query = "INSERT INTO ".$db->quoteName('#__mica_user_custom_attribute')."
					(".$db->quoteName('profile_id').", ".$db->quoteName('name').", ".$db->quoteName('attribute').")
					VALUES ( ".$db->quote($id).", ".$db->quote($seg[0]).", ".$db->quote($seg[1])." ) ";
				$db->setQuery($query);
				$db->execute();

				$query = "INSERT INTO ".$db->quoteName('#__mica_user_custom_attribute_lib')."
					(".$db->quoteName('profile_id').", ".$db->quoteName('name').", ".$db->quoteName('attribute').", ".$db->quoteName('tables').")
					VALUES (".$db->quote($id).", ".$db->quote($seg[0]).", ".$db->quote($seg[1]).", ".$db->quote($activetable)." ) ";
				$db->setQuery($query);
				$db->execute();
			}
			return $id;
		}

		return true;
	}

	/**
	 *
	 */
	public static function loadWorkspace($isguest = null) {
		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$view    = $app->input->getCmd('view');
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();

		if ($isguest != null)
		{
			$session->set('urban',null);

			if ($view == 'villageshowresults')
			{
				$session->set('villagesattributes', null);
				$session->set('villagestate', null);
				$session->set('villagedistrict', null);
				$session->set('villageSubDistrict', null);
				$session->set('villages_villages', null);
				$session->set('villagem_type', null);
			}
			elseif ($view == 'summaryshowresults')
			{
				$session->set('summaryattributes', null);
				$session->set('summarystate', null);
				$session->set('summarydistrict', null);
				$session->set('summarySubDistrict', null);
			}
			else
			{
				$session->set('attributes', null);
				$session->set('state', null);
				$session->set('district', null);
				$session->set('villages', null);
				$session->set('m_type', null);
			}
			$session->set('attrname',null);
			$session->set('activeworkspace',null);
		}

		$workspaceid = $app->input->get("workspaceid", '', 'raw');

		if ($user->id == 0)
		{
			return -1;
		}

		if($workspaceid == ""){
			$query = "SELECT *
				FROM ".$db->quoteName('#__mica_user_workspace')."
				WHERE ".$db->quoteName('userid')." = ".$db->quote($user->id)."
					AND ".$db->quoteName('is_default')." = ".$db->quote(1);
		}else{
			$query = "SELECT *
				FROM ".$db->quoteName('#__mica_user_workspace')."
				WHERE ".$db->quoteName('userid')." = ".$db->quote($user->id)."
					AND ".$db->quoteName('id')." = ".$db->quote($workspaceid);
		}

		$db->setQuery($query);

		return $db->loadAssocList();
	}


	public static function updateWorkspace(){
		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$view    = $app->input->getCmd('view');

		$urban  = $session->get('urban');
		$dataof = $session->get('dataof');

		if ($view == 'villageshowresults' || $view == 'villagefront')
		{
			$sessionattr   = $session->get('villagesattributes');
			$state         = $session->get('villagestate');
			$m_type        = $session->get('villagem_type');
			$district      = $session->get('villagedistrict');
			$subdistrict      = $session->get('villageSubDistrict');
			$villages      = $session->get('villages_villages');
			$dataAttribute = 'villagesattributes';
			$datastate     = "villagestate";
			$datadistrict  = "villagedistrict";
			$subdatadistrict  = "villageSubDistrict";
			$datavillages  = "villages_villages";
			$datam_type    = "villagem_type";

			$data = array(
				$dataAttribute => $sessionattr,
				$datastate     => $state,
				$datadistrict  => $district,
				$subdatadistrict  => $subdistrict,
				"urban"        => $urban,
				$datavillages  => $villages,
				$datam_type    => $m_type,
				'dataof'       => "villages"
			);
		}
		else if($view == 'summaryshowresults' || $view == 'summeryfront')
		{
			$sessionattr     = $session->get('summeryattributes');
			$state           = $session->get('summarystate');
			$district        = $session->get('summarydistrict');
			$subdistrict     = $session->get('summarySubDistrict');
			$dataAttribute   = 'summeryattributes';
			$datastate       = "summarystate";
			$datadistrict    = "summarydistrict";
			$datasubdistrict = "summarySubDistrict";

			$data = array(
				$dataAttribute   => $sessionattr,
				$datastate       => $state,
				$datadistrict    => $district,
				$datasubdistrict => $subdistrict,
				'dataof'         => "villageSummary",
			);
		}
		else if($view == 'showresults' || $view == 'micafront')
		{
			$sessionattr   = $session->get('attributes');
			$state         = $session->get('state');
			$m_type        = $session->get('m_type');
			$district      = $session->get('district');
			$villages      = $session->get('villages');
			$dataAttribute = "attribute";
			$datastate     = "state";
			$datadistrict  = "district";
			$datam_type    = "m_type";

			$data = array(
				$dataAttribute => $sessionattr,
				$datastate     => $state,
				$datadistrict  => $district,
				"urban"        => $urban,
				$datam_type    => $m_type,
				'dataof'       => $dataof
			);
		}

		$activetable = $session->get("activetable");
		//$data = mysql_real_escape_string(serialize($data));
		//$data = $db->escape(serialize($data));
		if ($data) {

			$data = serialize($data);

			$workspaceid  = $session->get('activeworkspace');
			$gusetprofile = $session->get('is_default');
			$name         = $app->input->get("name", '', 'raw');

			if( $gusetprofile ==1){
				$name = "name = 'Guest' ,";
			}else if($name!=""){
				$name = "name = '".$name."' ,";
			}else{
				$name = "";
			}

			if($user->id == 0){
				return -1;
			}

			$query = "UPDATE ".$db->quoteName('#__mica_user_workspace')."
				SET  ".$name." data = ".$db->quote($data)." ,
					modify_date = now()
				WHERE ".$db->quoteName('userid')." = ".$db->quote($user->id)."
					AND ".$db->quoteName('id')." = ".$db->quote($workspaceid);
			$db->setQuery($query);
			$db->execute();

			$id          = $app->input->get("id", '', 'raw');
			$workspaceid = $session->get('activeworkspace');
			$attributes  = $session->get('customattribute');
			if(strlen($attributes) != 0){
				$attributes = explode(",",$attributes);
				$attributes = array_filter($attributes);
				foreach($attributes as $eachcustom){
					$count       = 0;
					$eachcustom1 = explode(":", $eachcustom);
					$query       = "SELECT count(*) FROM ".$db->quoteName('#__mica_user_custom_attribute')."
						WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
							AND ".$db->quoteName('name')." = ".$db->quote($eachcustom1[0]);
					$db->setQuery($query);
					$count = $db->loadResult();

					if($count == 0){
						$queryAttr = "INSERT into ".$db->quoteName('#__mica_user_custom_attribute_lib')."
							(profile_id,name,attribute,tables) VALUES (".$db->quote($workspaceid).", ".$db->quote($eachcustom1[0]).", ".$db->quote($eachcustom1[1]).", ".$db->quote($activetable)." ) ";
							$db->setQuery($queryAttr);
							$db->execute();

						$query = "INSERT INTO ".$db->quoteName('#__mica_user_custom_attribute')."
							(profile_id,name,attribute) VALUES ( ".$db->quote($workspaceid).", ".$db->quote($eachcustom1[0]).", ".$db->quote($eachcustom1[1])." ) ";
						$db->setQuery($query);
						$db->execute();
					}else{
						$query = "UPDATE ".$db->quoteName('#__mica_user_custom_attribute')."
								SET ".$db->quoteName('attribute')." = ".$db->quote($eachcustom1[1])."
							WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
								AND ".$db->quoteName('name')." = ".$db->quote($eachcustom1[0]);
						$db->setQuery($query);
						$db->execute();

						$query = "UPDATE ".$db->quoteName('#__mica_user_custom_attribute_lib')."
								SET ".$db->quoteName('attribute')." = ".$db->quote($eachcustom1[1])."
							WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
								AND ".$db->quoteName('name')." LIKE ".$db->quote($eachcustom1[0])."
								AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
						$db->setQuery($query);
						$db->execute();
					}
				}
			}else{
				//$query="UPDATE #__mica_user_custom_attribute SET deleted = 1 where profile_id = ".$workspaceid;
				$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute')."
					WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid);
				$db->setQuery($query);
				$db->execute();
			}
			//$result=workspaceHelper::loadWorkspace();
		}
		return true;
	}

	/**
	 *
	 */
	public function deleteWorkspace(){
		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$view    = $app->input->getCmd('view');

		if ($view == 'villageshowresults')
		{
			$session->set('villagesattributes', null);
			$session->set('villagestate', null);
			$session->set('villagem_type', null);
			$session->set('villagedistrict', null);
			$session->set('villageSubDistrict', null);
			$session->set('villages_villages', null);
		}
		else if ($view == 'summaryshowresults')
		{
			$session->set('summaryattributes', null);
			$session->set('summarystate', null);
			$session->set('summarydistrict', null);
			$session->set('summarySubDistrict', null);
		}
		else if($view == 'showresults')
		{
			$session->set('attributes', null);
			$session->set('state', null);
			$session->set('m_type', null);
			$session->set('district', null);
			$session->set('villages', null);
		}

		$session->set('urban', null);
		$session->set('activeworkspace', null);
		$session->set('activetable', null);
		$session->set('activedata', null);

		if($user->id == 0){
			return -1;
		}

		$workspaceid = $app->input->get("workspaceid", '', 'raw');

		$query =" DELETE FROM ".$db->quoteName('#__mica_user_workspace')."
			WHERE ".$db->quoteName('userid')." = ".$db->quote($user->id)."
				AND ".$db->quoteName('id')." = ".$db->quote($workspaceid);
		$db->setQuery($query);
		$db->execute();exit;
	}


	/**
	 *
	 */

	function unsetDefault(){
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		if($user->id == 0){
			return -1;
		}

		$query = "UPDATE ".$db->quoteName('#__mica_user_workspace')."
				SET ".$db->quoteName('is_default')." = ".$db->quote(0)."
			WHERE ".$db->quoteName('userid')." = ".$db->quote($id);
		$db->setQuery($query);
		$db->execute();
		return true;
	}

}
