var isDatatableLoad = 0;
var isGraphFilterDataLoad = 0;

function loadGis(type = null) {
	jQuery('#map').html("");

	var data = $('#micaform').serialize();
	JQuery.ajax({
	    url     : "index.php?option=com_mica&task=summeryresults.getGISDataajax&type="+type,
	    method  : 'POST',
	    async   : true,
	    data    : data,
	    beforeSend: function() {
	      jQuery('#main_loader').show();
	    },
	    success : function(data){
	    	$("#apply_chnages_val").val(0);
	    	isGISLoad = 1;
	        //JQuery("#gis").load(" #gis > *");
	        jQuery('#main_loader').hide();
	        JQuery("#gis").show();
	        JQuery("#result_table").hide();
	        JQuery("#graph").hide();
	        JQuery("#quartiles").hide();
	        JQuery("#potentiometer").hide();

	        JQuery("#light_thematic").load(" #light_thematic > *");
	        JQuery("#thematic_attribute").load(" #thematic_attribute > *");

	        latestData = JQuery.parseJSON(data);

	        tomcaturl              = latestData.gis_tomcaturl;
	        mainzoom               = latestData.gis_zoom;
	        javazoom               = latestData.gis_zoom;
	        userid                 = latestData.gis_userid;
	        activeworkspace        = latestData.gis_activeworkspace;
	        mainGeometry           = latestData.gis_geometry;
	        mainlonglat            = latestData.gis_longlat;
	        mainState              = latestData.gis_state;
	        mainDistrict           = latestData.gis_district;
	        mainUnselectedDistrict = latestData.gis_UnselectedDistrict;
	        mainTown               = latestData.gis_town;
	        mainUrban              = latestData.gis_urban;
	        var sldlinkDistricts   = latestData.gis_slddistrict;

	        OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
	            defaultHandlerOptions: {
	                'single'         : true,
	                'double'         : false,
	                'pixelTolerance' : 0,
	                'stopSingle'     : true,
	                'stopDouble'     : false
	            },
	            initialize: function(options) {
	                this.handlerOptions = OpenLayers.Util.extend(
	                    {}, this.defaultHandlerOptions
	                );

	                OpenLayers.Control.prototype.initialize.apply(
	                    this, arguments
	                );

	                this.handler = new OpenLayers.Handler.Click(
	                    this, {
	                        'click':this.getfeaturenfo,'dblclick': this.onDblclick
	                    }, this.handlerOptions
	                );
	            },
	            onDblclick: function(e) {
	                JQuery(".olPopup").css({"display":"none"});
	                JQuery("#featurePopup_close").css({"display":"none"});
	                //changeAttrOnZoom(map.zoom);
	            },
	            getfeaturenfo :function(e) {
					coordinates = e;
					var params  = {
	                    REQUEST       : "GetFeatureInfo",
	                    projection    : "EPSG:4326",
	                    EXCEPTIONS    : "application/vnd.ogc.se_xml",
	                    BBOX          : map.getExtent().toBBOX(10),
	                    SERVICE       : "WMS",
	                    INFO_FORMAT   : 'text/html',
	                    QUERY_LAYERS  : selectlayer(map.zoom),
	                    FEATURE_COUNT : 6,
	                    Layers        : selectlayer(map.zoom),
	                    WIDTH         : map.size.w,
	                    HEIGHT        : map.size.h,
	                    X             : parseInt(e.xy.x),
	                    Y             : parseInt(e.xy.y),
	                    CQL_FILTER    : selectfilter(),
	                    srs           : map.layers[0].params.SRS
	                };

	                // handle the wms 1.3 vs wms 1.1 madness
	                if(map.layers[0].params.VERSION == "1.3.0") {
	                    params.version = "1.3.0";
	                    params.i       = e.xy.x;
	                    params.j       = e.xy.y;
	                } else {
	                    params.version = "1.1.1";
	                    params.y       = parseInt(e.xy.y);
	                    params.x       = parseInt(e.xy.x);
	                }
	                OpenLayers.loadURL(tomcaturl, params, this, setHTML, setHTML);
	                var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
	                //lonLat.transform(map.displayProjection,map.getProjectionObject());
	                //map.setCenter(lonLat, map.zoom);
	                map.panTo(lonLat);
	                //map.addControl(ovControl);
	                //OpenLayers.Event.stop(e);
	            }
	        });

	        var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
	        var options = {
	            controls      : [],
	            maxExtent     : bounds,
	            projection    : "EPSG:4326",
	            maxResolution : 'auto',
	            zoom          : mainzoom,
	            units         : 'degrees'
	        };

	        map = new OpenLayers.Map('map', options);

	        /*land = new OpenLayers.Layer.WMS("State Boundaries",
	            tomcaturl,
	            {
	                Layer       : 'india:rail_state',
	                transparent : true,
	                format      : 'image/png',
	                CQL_FILTER  : stateCqlFilter(),
	                SLD         : sldlinkStateBoundaries
	            },
	            {
	                isBaseLayer: false
	            }
	        );*/

	        opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
	            tomcaturl,
	            {
	                Layers           : 'india:rail_state',
	                transparent      : false,
	                format           : 'image/png',
	                styles           : "dummy_state",
	                transitionEffect : 'resize'
	            },
	            {
	                isBaseLayer: true
	            }
	        );

	        var districts = new OpenLayers.Layer.WMS("districts",
	            tomcaturl,
	            {
	                Layer       : 'india:india_information',
	                transparent : true,
	                format      : 'image/png',
	                CQL_FILTER  : districtCqlFilter(),
	                SLD         : sldlinkDistricts
	            },
	            {
	                isBaseLayer: false
	            },
	            {
	                transitionEffect: 'resize'
	            }
	        );

	        /*cities = new OpenLayers.Layer.WMS("Cities",
	            tomcaturl,
	            {
	                Layer       : 'india:my_table',
	                transparent : true,
	                format      : 'image/png',
	                CQL_FILTER  : cityCqlFilter(),
	                SLD         : sldlinkCities
	            },
	            {
	                isBaseLayer : false
	            },
	            {
	                transitionEffect : 'resize'
	            }
	        );

	        urban = new OpenLayers.Layer.WMS("Urban",
	            tomcaturl,
	            {
	                Layer       : 'india:jos_mica_urban_agglomeration',
	                transparent : true,
	                format      : 'image/png',
	                CQL_FILTER  : urbanCqlFilter(),
	                SLD         : sldlinkUrban
	            },
	            {
	                isBaseLayer: false
	            },
	            {
	                transitionEffect: 'resize'
	            }
	        );*/

	        if(mainzoom==5 || mainzoom==6 ){
                map.addLayers([land]);
            }else if(mainzoom==7){
                map.addLayers([districts]);
                districts.redraw(true);
            }else if(mainzoom==8){
                map.addLayers([urban]);
            }else{
                map.addLayers([cities]);
            }

	        map.div.oncontextmenu = function noContextMenu(e) {
	            if (OpenLayers.Event.isRightClick(e)){
	                displaymenu(e);
	            }
	            // return false; //cancel the right click of brower
	        };

	        //map.addControl(exportMapControl);
	        map.addControl(new OpenLayers.Control.PanZoomBar({
	            position : new OpenLayers.Pixel(5, 15)
	        }));

	        map.addControl(new OpenLayers.Control.Navigation({
	            dragPanOptions: {enableKinetic: true}
	        }));

	        //map.addControl(new OpenLayers.Control.Scale($('scale')));
	        map.addControl(new OpenLayers.Control.Attribution());
	        //map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
	        //map.addControl(new OpenLayers.Control.LayerSwitcher());
	        //alert(bounds);

	        var click = new OpenLayers.Control.Click();
	        map.addControl(click);
	        click.activate();
	        map.zoomTo(mainzoom);

	        if(mainGeometry != 0 && mainGeometry != "")
	        {
	            var format    = new OpenLayers.Format.WKT();
	            var feature   = format.read(mainGeometry);
	            var homepoint = feature.geometry.getCentroid();
	        }

	        map.addLayers([opverviewland]);
	        //if (!map.getCenter(bounds))
	            //map.zoomToMaxExtent();
	        map.zoomToExtent(bounds);
	        //map.zoomToMaxExtent();

	        //map.zoomTo(mainzoom);

	        if(mainlonglat == "")
	        {
	            var curlonglat= '77.941406518221,21.676757633686';
	        }
	        else
	        {
	            var curlonglat = mainlonglat;
	        }

	        //alert(curlonglat);
	        var longlatsegment=curlonglat.split(",");
	        if(mainGeometry !="0")
	        {
	            map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
	            map.zoomTo(6);
	        }
	        else
	        {
	            map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
	            map.zoomTo(4);
	        }


            /*totalthemecount     = latestData.gis_totalthemecount;
            usedlevel           = latestData.gis_usedlevel;
            havingthematicquery = latestData.gis_havingthematicquery;

            JQuery("#light_thematic").load(" #light_thematic > *",function(){
                if(typeof(usedlevel) != "undefined"){
                    if (usedlevel != null) {
                        var ulevel=usedlevel.split(",");
                        if(ulevel.length == 3){
                            JQuery("#themeconent").css({"display":"none"});
                        }
                    }
                    getColorPallet_v2();
                }
            });*/

            if(type == "thematic") {
                totalthemecount     = latestData.gis_totalthemecount;
                usedlevel           = latestData.gis_usedlevel;
                havingthematicquery = latestData.gis_havingthematicquery;

                JQuery("#light_thematic").load(" #light_thematic > *",function(){
                    if(typeof(usedlevel) != "undefined"){
                        if (usedlevel != null) {
                            var ulevel=usedlevel.split(",");
                            if(ulevel.length == 3){
                                JQuery("#themeconent").css({"display":"none"});
                            }
                        }
                        getColorPallet();
                    }
                });
            }else {
                JQuery("#light_thematic").load(" #light_thematic > *",function(){
                    if(typeof(usedlevel) != "undefined"){
                        if (usedlevel != null) {

                            var ulevel=usedlevel.split(",");
                            if(ulevel.length == 3){
                                JQuery("#themeconent").css({"display":"none"});
                            }
                        }
                        getColorPallet();
                    }
                });
            }
	    }
	});
}

function selectlayer(cuurzoom){
	if(javazoom==5 || javazoom==6){
		return "rail_state";
	}else if(javazoom==7){
		return "india_information";
	}else  if(javazoom==8){
		return "jos_mica_urban_agglomeration";
	}else{
		return "my_table";
	}
}

function setHTML(response){
	changeAttrOnZoom(map.zoom,response);
};

function selectfilter(){
	if(javazoom==5 || javazoom==6 ){
		return stateCqlFilter();
	}else if(javazoom==7){
		return districtCqlFilter();
	}else  if(javazoom==8){
		return urbanCqlFilter();
	}else{
		return cityCqlFilter();
	}
}

function displaycombo(value) {
	if ($("#state").val() == "all")
	{
		alert("Please Select State First");
		document.getElementById('dataof').selectedIndex = 0;
		document.getElementById("summarydistrict").selectedIndex = 0;
		return false;
	}
	else
	{
		var allVals = [];
		var slvals  = [];
        $('.state_checkbox:checked').each(function() {
            slvals.push($(this).val())
        });
        selected = slvals.join(',');
        getfrontdistrict(selected);

        var allVals = [];
		var slvals  = [];
        $('.district_checkbox:checked').each(function() {
            slvals.push($(this).val())
        });
        selected = slvals.join(',');
        getSubDistrict(selected);
	}

	if (value == "summarydistrict")
	{
		$(".districtspan").css({
			"display": "block"
		});
		$(".townspan").css({
			"display": "none"
		});
		$(".urbanspan").css({
			"display": "none"
		});
	}
}

function variable_sortListDir() {
	var list, i, switching, b, shouldSwitch, dir, switchcount = 0;

	list      = $('.variablelist');
	switching = true;
	dir       = "desc";
	header    = $('.variable_label');
    for(z=0; z<(header.length); z++)
   	{
		var inner_class = $(header[z]).text();
		inner_class     = inner_class.split(' ').join('_');
		inner_class     = inner_class.toLowerCase();
		switching       = true;
		dir             = dir;

	    while (switching) {
	        switching = false;

	        a = $('.'+inner_class);

	        for (i = 0; i < (a.length - 2); i++) {
	            shouldSwitch = false;

	            if (dir == "asc") {
	                if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {
	                    shouldSwitch = true;
	                    break;
	                }
	            } else if (dir == "desc") {
	                if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {
	                    shouldSwitch = true;
	                    break;
	                }
	            }
	        }
	        if (shouldSwitch) {
                var other = $(a[i]);
                $(a[i+1]).after(other.clone());
                other.after($(a[i+1])).remove();
                switching = true;

	            switchcount++;
	        } else {
	            if (switchcount == 0 && dir == "desc") {
	                dir = "asc";
	                switching = true;
	            }
	        }
	    }
    }
}

/* bar chart, line chart , radar chart*/
function loadCharts(data=null, selectedDistrict=null, selectedVariable=null, initialGraph=0, selectedSecondLevelVariable=null, selectedSecondLevelDistrict=null, isfillter=0) {

	// Check filter variable selected or not
    if(selectedDistrict == null && selectedVariable == null){
        var districtVals                       = [];
        var chartvariablesVals                 = [];
        $('.chartstates_checkbox:checked').each(function() {
            districtVals.push($(this).val());
        });
        selectedDistrict = districtVals.join(',');

        $('.chartvariables_checkbox:checked').each(function() {
            chartvariablesVals.push($(this).val());
        });
        selectedVariable = chartvariablesVals.join(',');
    }

    var isSubDist = $(".issubdist").attr('data-issubdist');

	if(data==null)
    var data = $('#micaform').serialize();
    data = data+"&selectedDistrict="+selectedDistrict+"&selectedVariable="+selectedVariable+"&initialGraph="+initialGraph+"&selectedSecondLevelVariable="+selectedSecondLevelVariable+"&isSubDist="+isSubDist+"&isfillter="+isfillter;
    if(isfillter == 1){
    	initialGraph=1;
    }
	var isVillageData = jQuery('#isVillageDataGet').val();
	if(isVillageData == 1){
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=villageshowresults.getGraphajax",
			method  : 'post',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				if (chart1 != undefined) {
	                chart1.destroy();
	            }

	            isGraphLoad = 1;
	            jQuery('#main_loader').hide();
				JQuery("#gis").hide();
				JQuery("#graph").show();
				JQuery("#result_table").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();
				JQuery('#villagedistictdata').hide();

				/*------------------bakground coclor------------*/
	            var backgroundColor = 'white';
	            Chart.plugins.register({
	                beforeDraw: function(c) {
	                    var ctx = c.chart.ctx;
	                    ctx.fillStyle = backgroundColor;
	                    ctx.fillRect(0, 0, c.chart.width, c.chart.height);
	                }
	            });
	            /*--------------bakground coclor end------------*/

				var chartType              = document.getElementById("chartype").value;
				var ctx                    = document.getElementById('myChart').getContext('2d');
				var datasets               = [];
				var secondlevelDatasetList = [];
				var result                 = JQuery.parseJSON(data);
				// Removed Second Level Data from other graph
	            if($('#chartype option:selected').text() != "Secondary Axis")
	            {
	                delete result.secondlevelDatasets;
	            }
				var color                    = Chart.helpers.color;
				//var colorcode                = ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
				var labels                   = [];
				var joinedClassesmain        = [];
				var i                        = 0, col = 0;
				//var keys                   = JQuery.map(result, function(element,index) {return index.split(' ').join('\n'); });
				var keys                     = JQuery.map(result, function(element,index) { var indexRes = index.split("~~"); var index = indexRes[0]; return index });
				venn_sets                    = [];
				venn_sets_main               = result;
				venn_selected_districts      = [];
				var joinedClasses            = new Array();
				var secondLevelDistrictLabel = [];

				var ict_unit = [];
	            var efficiency = [];
	            var colorcode = [];

	            var dynamicColors = function() {
	                var r = Math.floor(Math.random() * 255);
	                var g = Math.floor(Math.random() * 255);
	                var b = Math.floor(Math.random() * 255);
	                return "rgb(" + r + "," + g + "," + b + ")";
	            };

	            for (var d in data) {
	                ict_unit.push("ICT Unit " + data[d].ict_unit);
	                efficiency.push(data[d].efficiency);
	                colorcode.push(dynamicColors());
	            }

				JQuery.each(result,function(key,value){
					JQuery.each(value,function(key1,value1){
						if(i==0)
						{
							labels.push(key1);
							venn_selected_districts.push(key1);
							distrcitname = key1.split("-");
							distrcitname = distrcitname[0];
							if(venn_sets.length < 5)
							{
								secondLevelDistrictLabel.push(distrcitname.trim());
								//venn_sets.push ({'title':distrcitname.trim() + ' '  + value1, 'views':parseFloat(value1)});
								venn_sets.push ({'title':distrcitname.trim(),'views':parseFloat(value1)});
								venn_title = key;

								if(col==0){
									venn_selected_vars.push(key);
								}
							}
						}

						if (joinedClasses[key1] === undefined)
						{
							joinedClasses[key1]= new Array();
						}
	                    joinedClasses[key1][key]=value1;// vaule define
	                    col++;

	                });
					i++;
				});

				var i = 0;
				//var statecity = '';
				//statecity = '<ul id ="statesForGraph" class="list1">';
	            JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
					var res       = value.split("~~");
					var cityname  = res[0];
					var citycode  = res[1];
					/*var container = $('#cblist');
	            	statecity += "<li>"+
	            	'<input type = "checkbox" class="chartstates_checkbox" '+
	            	' id="chartstates_' + citycode +'" value="'+ citycode+ '" checked/>'+
	            	'<label for="chartstates_' + citycode +'">'+ cityname+ "</label>"+
	            	"</li>";*/
					var newres = Object.keys(joinedClasses[value]).map(function(k) {
	            		return [k, joinedClasses[value][k]];
	            	});

					var joinedClassesmain = [];

	            	$.each(newres, function (key1,value1) {
						joinedClassesmain.push(value1[1]);
					});

					var variable1 = [];
					var newhtml   = '';
					//newhtml       = '<ul id ="variablesForGraph" class="list1">';

	                $.each(newres, function (key1,value1) {

	               		variable1=value1[0];

	              /* 	newhtml += "<li>"+
	               	'<input type = "checkbox" class="chartvariables_checkbox" '+
	               	' id="chartvariables_' + variable1.replace(/ /g,"_") +'" value="'+ variable1+ '" checked/>'+
	               	'<label for="chartvariables_' + variable1.replace(/ /g,"_") + '">'+ variable1+ "</label>"+
	               	"</li>";*/

	               });
	               /*newhtml +='</ul>';
	               $('#variabelist').html(newhtml);*/

	                datasetValues[citycode] = joinedClassesmain;

	                if (chartType == 'bar') {
	                    if (initialGraph == 1) {
			                if(i<5)
			               	{
				                datasets.push({
				                    'label':cityname, //city name should come here
				                    'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
				                    'lineTension': 0,
				                    'fill': false,
				                    'borderColor':colorcode[i],
				                    'borderWidth':1,
				                    'data':joinedClassesmain,
				                    'id':citycode,
				                    'id1':variable1
				                });
				            }
			            }else{
			            	datasets.push({
			                    'label':cityname, //city name should come here
			                    'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
			                    'lineTension': 0,
			                    'fill': false,
			                    'borderColor':colorcode[i],
			                    'borderWidth':1,
			                    'data':joinedClassesmain,
			                    'id':citycode,
			                    'id1':variable1
			                });
			            }
			        }else{
	                    if(i<5)
	                    {
	                    	datasets.push({
			                    'label':cityname, //city name should come here
			                    'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
			                    'lineTension': 0,
			                    'fill': false,
			                    'borderColor':colorcode[i],
			                    'borderWidth':1,
			                    'data':joinedClassesmain,
			                    'id':citycode,
			                    'id1':variable1
			                });
			            }
			        }
	                //if (i == 5) {return false;}
	                i++;
	            });
	            //statecity +='</ul>';

	            //$('#cblist').html(statecity);
	          	var formatsetkys = [];
	            $.each(keys,function(index, el){
					formatsetkys.push(el.replace(/ /g, "\n"));
	            });

	           // statecity = '';

	           	newlabel = [];
	            originalLabels = keys;
	            keys.forEach(function(e,i){
	                data = e.replace(/(\s[^\s]*)\s/g,"$1--");
	                rres = data.split("--");
	                newlabel.push(rres);
	            });

	            var displaySecondLevel = false;
	            if($('#chartype option:selected').text() == "Secondary Axis")
	            {
	                $.each(result.secondlevelDatasets, function (secondlevelDatasetsKey,value) {
	                	if(secondlevelDatasetsKey<5)
	               		{
							var valueLabel           = value.label.split("~~");
							var secondlevelLabelDisp = valueLabel[0];
							var secondlevelVariable  = valueLabel[1];

		                    secondlevelDatasetList.push({
		                        'label':secondlevelLabelDisp, //city name should come here
		                        'backgroundColor':color(colorcode[secondlevelDatasetsKey]).alpha(0.5).rgbString(),
		                        'borderColor':colorcode[secondlevelDatasetsKey],
		                        'borderWidth':1,
		                        'data':value.data
		                    });

		                    if (selectedSecondLevelVariable != null) {
		                        //secondLevelchartvariableName = selectedSecondLevelVariable.replace(/_/g," ");
		                        secondLevelchartvariableNameArray = selectedSecondLevelVariable.split(",");

		                        var indexoflabels = $.inArray(secondlevelVariable, secondLevelchartvariableNameArray);

		                        if (indexoflabels !== -1) {
		                        	displaySecondLevel = true;
		                            secondlevelDatasetList[secondlevelDatasetsKey].yAxisID = "y-axis-2";
		                        }
		                        else{
		                            secondlevelDatasetList[secondlevelDatasetsKey].yAxisID = "y-axis-1";
		                        }
		                    }
		                }
	                });

	                chartconfig = {
	                    type: 'bar',
	                    data: {
	                        labels:secondLevelDistrictLabel,
	                        datasets: secondlevelDatasetList
	                    },
	                    options: {
	                        legend: {
	                            display: true,
	                        	onHover: function(event, legendItem) {
	                                document.getElementById("myChart").style.cursor = 'pointer';
	                            }
	                        },
	                        scales: {
	                            yAxes: [{
	                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
	                                display: true,
	                                position: "left",
	                                id: "y-axis-1",
	                            }, {
	                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
	                                display: displaySecondLevel,
	                                position: "right",
	                                id: "y-axis-2",
	                                gridLines: {
	                                    drawOnChartArea: true
	                                },
	                                afterBuildTicks: function(scale) {
	                                  scale.ticks = updateChartTicks(scale);
	                                    return;
	                                  },
	                                  beforeUpdate: function(oScale) {
	                                    return;
	                                  },
	                                  ticks: {
	                                    beginAtZero:true,
	                                    maxTicksLimit: 10,
	                                  }
	                            }],
	                            xAxes: [{
	                                ticks: {
	                                    maxTicksLimit: 8,
	                                    autoSkip: false
	                                },
	                                maxBarThickness: 50,
	                            }]
	                        }
	                    },
	                };
	            }
	            else{
		            chartconfig = {
		            	type: chartType,
		                // The data for our dataset
		                data: {
		                	labels: newlabel,
		                	datasets: datasets
		                },
		                options: {
		                	legend: {
		                		display: true,
		                		onHover: function(event, legendItem) {
	                                document.getElementById("myChart").style.cursor = 'pointer';
	                            }
		                	},
		                	scales: {
		                		yAxes: [{
		                            ticks: {
		                                beginAtZero: true
		                            }
		                        }],
		                		xAxes: [{
		                			ticks: {
		                				maxTicksLimit: 8,
		                                autoSkip: false,
		                                /*maxRotation: 360,
	                                	minRotation: 360*/
		                			},
		                            gridLines: {
						                offsetGridLines: true
						            },
	                            	maxBarThickness: 50,
		                		}]
		                	},
	                        animation: {
	                            duration: 0,
	                            onComplete: function () {

	                            	// render the value of the chart above the bar
	                            	var ctx = this.chart.ctx;
									ctx.textAlign = 'center';
									ctx.textBaseline = 'bottom';

									// Need to have datasetIndex here too
									this.data.datasets.forEach(function (dataset, datasetIndex) {

										// Check if the dataset is visible before drawing the value
										if (this.isDatasetVisible(datasetIndex)) {
										    for (var i = 0; i < dataset.data.length; i++) {
										      	var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
										      	scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
										      	ctx.fillStyle = '#444';
										      	var y_pos = model.y+1; //model.y - 5;

										      	if ((scale_max - model.y) / scale_max >= 0.93)
										        	y_pos = model.y + 20;

										      	ctx.fillText(dataset.data[i]+""+"", model.x, y_pos);
										    }
										}
									}, this);
	                            }
	                        }
		                },
		    		};
		    	}

	            chart1 = new Chart(ctx, chartconfig);

	          /*  // Selected 5 Distict data default
	            $('.chartstates_checkbox').click();
	            $('.chartstates_checkbox:eq( 0 )').click();
	            $('.chartstates_checkbox:eq( 1 )').click();
	            $('.chartstates_checkbox:eq( 2 )').click();
	            $('.chartstates_checkbox:eq( 3 )').click();
	            $('.chartstates_checkbox:eq( 4 )').click();

	            // Selected 5 Variable data default
	            $('.chartvariables_checkbox').click();
	            $('.chartvariables_checkbox:eq( 0 )').click();
	            $('.chartvariables_checkbox:eq( 1 )').click();
	            $('.chartvariables_checkbox:eq( 2 )').click();
	            $('.chartvariables_checkbox:eq( 3 )').click();
	            $('.chartvariables_checkbox:eq( 4 )').click();*/

	            chart_selected_vars = [];
	            $.each($(".chartvariables_checkbox:checked"), function(){
	           		 chart_selected_vars.push($(this).val());
	        	});
	        	 chart_selected_districts = [];
	            $.each($(".chartstates_checkbox:checked"), function(){
	           		 chart_selected_districts.push($(this).val());
	        	});

	        	if($('#chartype').val() =='bubble'){
	                $('#venn').html('<svg></svg>');
	                Bubblechartload();
	            }
	        }
    	});
	}else{
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getGraphajax",
			method  : 'post',
			async   : true,
			data    : data,
			beforeSend: function() {
				jQuery('#main_loader').show();
			},
			success : function(data){
				if (chart1 != undefined) {
	                chart1.destroy();
	            }

	            isGraphLoad = 1;

				jQuery('#main_loader').hide();
				JQuery("#gis").hide();
				JQuery("#graph").show();
				JQuery("#result_table").hide();
				JQuery("#quartiles").hide();
				JQuery("#potentiometer").hide();
				JQuery('#villagedistictdata').hide();

				/*------------------bakground coclor------------*/
	            var backgroundColor = 'white';
	            Chart.plugins.register({
	                beforeDraw: function(c) {
	                    var ctx = c.chart.ctx;
	                    ctx.fillStyle = backgroundColor;
	                    ctx.fillRect(0, 0, c.chart.width, c.chart.height);
	                }
	            });
	            /*--------------bakground coclor end------------*/

				var chartType              = document.getElementById("chartype").value;
				var ctx                    = document.getElementById('myChart').getContext('2d');
				var datasets               = [];
				var secondlevelDatasetList = [];
				var result                 = JQuery.parseJSON(data);
				// Removed Second Level Data from other graph
	            if($('#chartype option:selected').text() != "Secondary Axis")
	            {
	                delete result.secondlevelDatasets;
	            }
				var color                    = Chart.helpers.color;
				//var colorcode                = ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
				var labels                   = [];
				var joinedClassesmain        = [];
				var i                        = 0, col = 0;
				//var keys                   = JQuery.map(result, function(element,index) {return index.split(' ').join('\n'); });
				var keys                     = JQuery.map(result, function(element,index) { var indexRes = index.split("~~"); var index = indexRes[0]; return index });
				venn_sets                    = [];
				venn_sets_main               = result;
				venn_selected_districts      = [];
				var joinedClasses            = new Array();
				var secondLevelDistrictLabel = [];

				var ict_unit = [];
	            var efficiency = [];
	            var colorcode = [];

	            var dynamicColors = function() {
	                var r = Math.floor(Math.random() * 255);
	                var g = Math.floor(Math.random() * 255);
	                var b = Math.floor(Math.random() * 255);
	                return "rgb(" + r + "," + g + "," + b + ")";
	            };

	            for (var d in data) {
	                ict_unit.push("ICT Unit " + data[d].ict_unit);
	                efficiency.push(data[d].efficiency);
	                colorcode.push(dynamicColors());
	            }

				JQuery.each(result,function(key,value){
					JQuery.each(value,function(key1,value1){
						if(i==0)
						{
							labels.push(key1);
							venn_selected_districts.push(key1);
							distrcitname = key1.split("-");
							distrcitname = distrcitname[0];
							if(venn_sets.length < 5)
							{
								secondLevelDistrictLabel.push(distrcitname.trim());
								//venn_sets.push ({'title':distrcitname.trim() + ' '  + value1, 'views':parseFloat(value1)});
								venn_sets.push ({'title':distrcitname.trim(),'views':parseFloat(value1)});
								venn_title = key;

								if(col==0){
									venn_selected_vars.push(key);
								}
							}
						}

						if (joinedClasses[key1] === undefined)
						{
							joinedClasses[key1]= new Array();
						}
	                    joinedClasses[key1][key]=value1;// vaule define
	                    col++;

	                });
					i++;
				});

				var i = 0;
				//var statecity = '';
				//statecity = '<ul id ="statesForGraph" class="list1">';
	            JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
					var res       = value.split("~~");
					var cityname  = res[0];
					var citycode  = res[1];
					/*var container = $('#cblist');
	            	statecity += "<li>"+
	            	'<input type = "checkbox" class="chartstates_checkbox" '+
	            	' id="chartstates_' + citycode +'" value="'+ citycode+ '" checked/>'+
	            	'<label for="chartstates_' + citycode +'">'+ cityname+ "</label>"+
	            	"</li>";*/
					var newres = Object.keys(joinedClasses[value]).map(function(k) {
	            		return [k, joinedClasses[value][k]];
	            	});

					var joinedClassesmain = [];

	            	$.each(newres, function (key1,value1) {
						joinedClassesmain.push(value1[1]);
					});

					var variable1 = [];
					var newhtml   = '';
					//newhtml       = '<ul id ="variablesForGraph" class="list1">';

	                $.each(newres, function (key1,value1) {

	               		variable1=value1[0];

	              /* 	newhtml += "<li>"+
	               	'<input type = "checkbox" class="chartvariables_checkbox" '+
	               	' id="chartvariables_' + variable1.replace(/ /g,"_") +'" value="'+ variable1+ '" checked/>'+
	               	'<label for="chartvariables_' + variable1.replace(/ /g,"_") + '">'+ variable1+ "</label>"+
	               	"</li>";*/

	               });
	               /*newhtml +='</ul>';
	               $('#variabelist').html(newhtml);*/

	                datasetValues[citycode] = joinedClassesmain;

	                if (chartType == 'bar') {
	                    if (initialGraph == 1) {
			                if(i<5)
			               	{
				                datasets.push({
				                    'label':cityname, //city name should come here
				                    'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
				                    'lineTension': 0,
				                    'fill': false,
				                    'borderColor':colorcode[i],
				                    'borderWidth':1,
				                    'data':joinedClassesmain,
				                    'id':citycode,
				                    'id1':variable1
				                });
				            }
			            }else{
			            	datasets.push({
			                    'label':cityname, //city name should come here
			                    'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
			                    'lineTension': 0,
			                    'fill': false,
			                    'borderColor':colorcode[i],
			                    'borderWidth':1,
			                    'data':joinedClassesmain,
			                    'id':citycode,
			                    'id1':variable1
			                });
			            }
			        }else{
	                    if(i<5)
	                    {
	                    	datasets.push({
			                    'label':cityname, //city name should come here
			                    'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
			                    'lineTension': 0,
			                    'fill': false,
			                    'borderColor':colorcode[i],
			                    'borderWidth':1,
			                    'data':joinedClassesmain,
			                    'id':citycode,
			                    'id1':variable1
			                });
			            }
			        }
	                //if (i == 5) {return false;}
	                i++;
	            });
	            //statecity +='</ul>';

	            //$('#cblist').html(statecity);
	          	var formatsetkys = [];
	            $.each(keys,function(index, el){
					formatsetkys.push(el.replace(/ /g, "\n"));
	            });

	           // statecity = '';

	           	newlabel = [];
	            originalLabels = keys;
	            keys.forEach(function(e,i){
	                data = e.replace(/(\s[^\s]*)\s/g,"$1--");
	                rres = data.split("--");
	                newlabel.push(rres);
	            });

	            var displaySecondLevel = false;
	            if($('#chartype option:selected').text() == "Secondary Axis")
	            {
	                $.each(result.secondlevelDatasets, function (secondlevelDatasetsKey,value) {
	                	if(secondlevelDatasetsKey<5)
	               		{
							var valueLabel           = value.label.split("~~");
							var secondlevelLabelDisp = valueLabel[0];
							var secondlevelVariable  = valueLabel[1];

		                    secondlevelDatasetList.push({
		                        'label':secondlevelLabelDisp, //city name should come here
		                        'backgroundColor':color(colorcode[secondlevelDatasetsKey]).alpha(0.5).rgbString(),
		                        'borderColor':colorcode[secondlevelDatasetsKey],
		                        'borderWidth':1,
		                        'data':value.data
		                    });

		                    if (selectedSecondLevelVariable != null) {
		                        //secondLevelchartvariableName = selectedSecondLevelVariable.replace(/_/g," ");
		                        secondLevelchartvariableNameArray = selectedSecondLevelVariable.split(",");

		                        var indexoflabels = $.inArray(secondlevelVariable, secondLevelchartvariableNameArray);

		                        if (indexoflabels !== -1) {
		                        	displaySecondLevel = true;
		                            secondlevelDatasetList[secondlevelDatasetsKey].yAxisID = "y-axis-2";
		                        }
		                        else{
		                            secondlevelDatasetList[secondlevelDatasetsKey].yAxisID = "y-axis-1";
		                        }
		                    }
		                }
	                });

	                chartconfig = {
	                    type: 'bar',
	                    data: {
	                        labels:secondLevelDistrictLabel,
	                        datasets: secondlevelDatasetList
	                    },
	                    options: {
	                        legend: {
	                            display: true,
	                        	onHover: function(event, legendItem) {
	                                document.getElementById("myChart").style.cursor = 'pointer';
	                            }
	                        },
	                        scales: {
	                            yAxes: [{
	                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
	                                display: true,
	                                position: "left",
	                                id: "y-axis-1",
	                            }, {
	                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
	                                display: displaySecondLevel,
	                                position: "right",
	                                id: "y-axis-2",
	                                gridLines: {
	                                    drawOnChartArea: true
	                                },
	                                afterBuildTicks: function(scale) {
	                                  scale.ticks = updateChartTicks(scale);
	                                    return;
	                                  },
	                                  beforeUpdate: function(oScale) {
	                                    return;
	                                  },
	                                  ticks: {
	                                    beginAtZero:true,
	                                    maxTicksLimit: 10,
	                                  }
	                            }],
	                            xAxes: [{
	                                ticks: {
	                                    maxTicksLimit: 8,
	                                    autoSkip: false
	                                },
	                                maxBarThickness: 50,
	                            }]
	                        }
	                    },
	                };
	            }
	            else{
		            chartconfig = {
		            	type: chartType,
		                // The data for our dataset
		                data: {
		                	labels: newlabel,
		                	datasets: datasets
		                },
		                options: {
		                	legend: {
		                		display: true,
		                		onHover: function(event, legendItem) {
	                                document.getElementById("myChart").style.cursor = 'pointer';
	                            }
		                	},
		                	scales: {
		                		yAxes: [{
		                            ticks: {
		                                beginAtZero: true
		                            }
		                        }],
		                		xAxes: [{
		                			ticks: {
		                				maxTicksLimit: 8,
		                                autoSkip: false,
		                                /*maxRotation: 360,
	                                	minRotation: 360*/
		                			},
		                            gridLines: {
						                offsetGridLines: true
						            },
	                            	maxBarThickness: 50,
		                		}]
		                	},
	                        animation: {
	                            duration: 0,
	                            onComplete: function () {

	                            	// render the value of the chart above the bar
	                            	var ctx = this.chart.ctx;
									ctx.textAlign = 'center';
									ctx.textBaseline = 'bottom';

									// Need to have datasetIndex here too
									this.data.datasets.forEach(function (dataset, datasetIndex) {

										// Check if the dataset is visible before drawing the value
										if (this.isDatasetVisible(datasetIndex)) {
										    for (var i = 0; i < dataset.data.length; i++) {
										      	var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
										      	scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
										      	ctx.fillStyle = '#444';
										      	var y_pos = model.y+1; //model.y - 5;

										      	if ((scale_max - model.y) / scale_max >= 0.93)
										        	y_pos = model.y + 20;

										      	ctx.fillText(dataset.data[i]+""+"", model.x, y_pos);
										    }
										}
									}, this);
	                            }
	                        }
		                },
		    		};
		    	}

	            chart1 = new Chart(ctx, chartconfig);

	          /*  // Selected 5 Distict data default
	            $('.chartstates_checkbox').click();
	            $('.chartstates_checkbox:eq( 0 )').click();
	            $('.chartstates_checkbox:eq( 1 )').click();
	            $('.chartstates_checkbox:eq( 2 )').click();
	            $('.chartstates_checkbox:eq( 3 )').click();
	            $('.chartstates_checkbox:eq( 4 )').click();

	            // Selected 5 Variable data default
	            $('.chartvariables_checkbox').click();
	            $('.chartvariables_checkbox:eq( 0 )').click();
	            $('.chartvariables_checkbox:eq( 1 )').click();
	            $('.chartvariables_checkbox:eq( 2 )').click();
	            $('.chartvariables_checkbox:eq( 3 )').click();
	            $('.chartvariables_checkbox:eq( 4 )').click();*/

	            chart_selected_vars = [];
	            $.each($(".chartvariables_checkbox:checked"), function(){
	           		 chart_selected_vars.push($(this).val());
	        	});
	        	 chart_selected_districts = [];
	            $.each($(".chartstates_checkbox:checked"), function(){
	           		 chart_selected_districts.push($(this).val());
	        	});

	        	if($('#chartype').val() =='bubble'){
	                $('#venn').html('<svg></svg>');
	                Bubblechartload();
	            }
	        }
    	});
	}
}

var updateChartTicks = function(scale) {
	var incrementAmount = 0;
	var previousAmount  = 0;
	var newTicks        = [];
	newTicks            = scale.ticks;

	for (x=0;x<newTicks.length;x++) {
    	incrementAmount = (previousAmount - newTicks[x]);
    	previousAmount = newTicks[x];
  	}

  	if (newTicks.length > 2) {
    	if (newTicks[0] - newTicks[1] != incrementAmount) {
      		newTicks[0] = newTicks[1] + incrementAmount;
    	}
  	}

 	return newTicks;
};

function getfrontdistrict(val) {
	if(val == "" ){
		$('.district_checkbox').removeAttr('checked');
		$('.subdistrict_checkbox').removeAttr('checked');
		$('.allcheck').removeAttr('checked');
		getAttribute(9);
		return false;
	}else if(val=="all"){
		document.getElementById('summarydistrict').selectedIndex=0;
		document.getElementById('urban').selectedIndex=0;
		document.getElementById('villages').selectedIndex=0;

		$('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({'display':'none'});
		  getAttribute(9);
		return false;
	}else{
		 getAttribute(9);

		$(".distattrtypeselection").css({"display":"none"});
	}

	$('.distattrtypeselection').css({'display':'none'});
 	$("#main_loader").show();

	$.ajax({
		url     : "index.php?option=com_mica&task=summeryfront.getsecondlevel&stat="+val+"&preselected="+preselecteddata,
		method  : 'post',
		success : function(combos){
			var segment = combos.split("split");
			JQuery(".districtlist").html(segment[0]);
			JQuery("#statecode").html(segment[1]);

			if(preselecteddata != ""){
				getAttribute(9, "district");
			}
			var dataof       = JQuery("#dataof").val();
			if(dataof == "ua" || dataof == "UA"){
				dataof="urban";
			}
			$('.district_checkbox').each(function () {

			    var state_code = $(this).attr('data-id');
			    var checked_len = $('.districtlabel_checkbox-'+state_code+':checked').length ;
			    var total_len = $('.districtlabel_checkbox-'+state_code).length ;

			    if(checked_len == total_len) {
			        $('#districtlabel_allcheck-'+state_code).prop('checked',true);
			    }
			})

			if ($('.districtNamegrp:checked').length == $('.districtNamegrp').length)
			{
			    // Cheked District checkbox
			    $('#district_allcheck').prop('checked', true);
			} else {
			    $('#district_allcheck').prop('checked', false);
			}

			checkvalidation();
			getSubDistrict();
			$("#main_loader").hide();
		}
	});
}

function Bubblechartload() {

	var venn_titleLabel   = venn_title.split("~~");
	var VariableTitleDisp = venn_titleLabel[0];

	var chart = bubbleChart().width(760).height(500).unitName("").title(VariableTitleDisp);
	d3.select('#venn').datum(venn_sets).call(chart);
}


function saveSLD(){
	var formula     = JQuery("#thematic_attribute").val();
	//var condition = JQuery("#condition").val();
	var limit       = JQuery("#no_of_interval").val();
	var level       = JQuery("#level").val();

	if(formula=="")
	{
		alert("Please Select Variable");
		return false;
	}
	else if(limit=="")
	{
		alert("Please Select Interval");
		return false;
	}

	var from  ="";
	var to    ="";
	var color ="";
	for(var i=1;i<=limit;i++)
	{
		from +=JQuery("#from"+i).val()+",";
		to   +=JQuery("#to"+i).val()+",";

		if(level=="0" || level=="")
		{
			color +=rgb2hex(JQuery("#color"+i).css("background-color"))+",";
		}
		else
		{
			color +=i+",";
		}
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=summeryresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
		method  : 'GET',
		success : function(data){
			//window.location.href  = window.location;
			JQuery("#gis_script").load(" #gis_script > *");
            loadGis('thematic');
            document.getElementById('light_thematic').style.display = 'none';
            document.getElementById('light2').style.display = 'none';
            document.getElementById('fade').style.display   = 'none';
		}
	});
}

    JQuery(document).ready(function(){
    	$("#apply_chnages_val").val(0);

    	$('#district_tab').click(function(e){
		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		JQuery('#state_tab :selected').each(function(i, selected) {
			myurl +=JQuery(selected).val()+",";
			zoom=5;
		});
	});

	$('#variables_tab').click(function(e){
		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}

		if($('.district_checkbox:checked').length == 0){
			alert(districtalert);
			return false;
		}
	});
	$('#sub_district_tab').click(function(e){
		if($('.state_checkbox:checked').length == 0){
			alert(statealert);
			return false;
		}
	});


	if(typeof(preselected) != "undefined")
	{
		displaycombo();
	}

	$(document).on('change','.allcheck',function(){
		/* Act osn the event */
		if($(this).is('#district_allcheck')) {
            if($('#district_allcheck').prop("checked")) {
                $('.districtNamegrp').prop("checked",true);
                $('.district_checkbox').prop("checked",true);
                getSubDistrict();
            } else  {
                $('.districtNamegrp').prop("checked",false);
                $('.district_checkbox').prop("checked",false);
            }
        } else {

			var id      = $(this).attr('id');
			var input   = id.split('_');

			$("." +input[0]+"_checkbox").prop("checked",$(this).prop("checked"));

			if(input[0]=="state")
			{
				var allVals = [];
				var slvals  = [];
				$('.state_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');
				if (selected) {
					$("#statetext").attr('style','color:"#ba171b"');
				} else {
					$("#statetext").attr('style','color: "black"');
				}
				getfrontdistrict(selected);
				checkvalidation();
			}
			else if(input[0]=="district" || input[0]=='districtlabel')
			{
				if(input[0]=='districtlabel')
	            {
	                //For Checkbox of each district inside district tab
	                var statecode = input[1].split('-');
	                $("." + input[0] + "_checkbox-"+ statecode[1]).prop("checked", $(this).prop("checked"));
	            }

				var allVals = [];
				var slvals  = [];
				$('.district_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');

				if(selected) {
					$("#districttext").attr('style','color:"#ba171b"');
				} else {
					$("#districttext").attr('style','color: "black"');
				}
				getSubDistrict(selected);
				checkvalidation();
			}
			else if(input[0] == "subdistrict"  || input[0] == "subdistrictlabel")
			{
				if(input[0]=='subdistrictlabel')
	            {
	                //For Checkbox of each district inside district tab
	                var statecode = input[1].split('-');
	                $("." + input[0] + "_checkbox-"+ statecode[1]).prop("checked", $(this).prop("checked"));
	            }

				var allVals = [];
				var slvals  = [];

				$('.subdistrict_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');

				if(selected) {
					$("#subdistricttext").attr('style','color:"#ba171b"');
				} else {
					$("#subdistricttext").attr('style','color: "black"');
				}

				checkvalidation();
			}
			else if(input[0] == "variable" || input[0] == "variablelabel")
			{
				if(input[0]=='variablelabel')
				{
					//For Checkbox of each district inside district tab
	                var statecode = input[1].split('-');
	                console.log(statecode);
	                $("." + input[0] + "_checkbox-"+ statecode[1]).prop("checked", $(this).prop("checked"));
				}

				var allVals = [];
				var slvals  = [];

				$('.variable_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');
				if(selected) {
					$("#variabletext").attr('style','color:"#ba171b"');
				} else {
					$("#variabletext").attr('style','color: "black"');
				}
				checkvalidation();
			}

			else if(input[0] == "metervariables")
			{
				var allVals = [];
				var slvals  = [];

				$('.metervariables_checkbox:checked').each(function() {
					slvals.push($(this).val())
				})
				selected = slvals.join(',');
			}

			if($('.districtNamegrp:checked').length == $('.districtNamegrp').length ) {
				$('#district_allcheck').prop('checked',true);
			} else {
				$('#district_allcheck').prop('checked',false);
			}
		}
	});

	$('#district_tab').click(function(e) {
		$('#state_tab :selected').each(function(i, selected) {
			myurl += $(selected).val() + ",";
			zoom = 9;
		});
	});

	//For check all function to work well with all other checkboxes:- start
	$('.leftcontainer').on("change",'.list1 input[type=checkbox]', function(){
		var classname =$(this).attr('class');
		//classname = classname.split(" ");
		if(classname.indexOf("_checkbox")!=-1)
		{
			end    = classname.indexOf("_checkbox");
			start  = classname.indexOf(" ")!=-1?classname.indexOf(" "):0 ;
			result = classname.substring(start, end).trim();
		}

        if($(this).prop('checked')==false)
        {
			$('#'+result+'_allcheck').prop('checked', false); //works with mpi_allcheck, swcs_checkbox, swcs_allcheck
		}
		else
		{
			if($('.'+result+'list input[type=checkbox]:checked').length == $('.'+result+'list input[type=checkbox]').length)
			{
				$('#'+result+'_allcheck').prop('checked', true);
			}
			else{
				$('#'+result+'_allcheck').prop('checked', false);
			}
		}
	});
	//For check all function to work well with all other checkboxes:- END\

	$(document).on("click", ".filterspeed", function() {
	    //var selection     = $(this).val();
	    var selection = $(this).data('val');
	    filterspeed_radio = selection;


	    if (selection == "0") {
	        //Layer

	        $('.sfilter').addClass('layer');
			$('.sfilter').removeClass('Variable');
	        $('#meterdistrict_allcheck').hide();
	        $('#metervariables_allcheck').show();
	        $('.meterdistrict_checkbox').data('multiple', 1);
	        $('.metervariables_checkbox').data('multiple', 0);
	        $('.metervariables_checkbox').removeAttr('checked');
	        $('.meterdistrict_checkbox').removeAttr('checked');
	        $('.metervariables_checkbox').prop('checked', false);
	        $('.meterdistrict_checkbox').prop('checked', false);
	        console.log(district_checked_layer);
	        $('.meterdistrict_checkbox').each(function() {
	            if ($.inArray($(this).val(), district_checked_layer) != -1)
	                $(this).attr('checked', true);
	        });
	        $('.metervariables_checkbox').each(function() {
	            if ($.inArray($(this).val(), variable_checked_layer) != -1)
	                $(this).attr('checked', true);
	        });
	         $('.filterbylabel').html('<b>Filter By Layer</b>');

	    } else {
	        //Variable

	        $('.sfilter').removeClass('layer');
	        $('.sfilter').addClass('Variable');
	         $('#metervariables_allcheck').hide();
	         $('#meterdistrict_allcheck').show();
	        $('.filterbylabel').html('<b>Filter By Variable</b>');
			$('.meterdistrict_checkbox').data('multiple', 0);
	        $('.metervariables_checkbox').data('multiple', 1);
	        $('.metervariables_checkbox').prop('checked', false);
	        $('.meterdistrict_checkbox').prop('checked', false);
	        $('.metervariables_checkbox').removeAttr('checked');
	        $('.meterdistrict_checkbox').removeAttr('checked');
	        console.log(district_checked_district);
	        $('.meterdistrict_checkbox').each(function() {
	            if (jQuery.inArray($(this).val(), district_checked_district) != -1)
	                $(this).attr('checked', true);

	        });
	        $('.metervariables_checkbox').each(function() {
	            if (jQuery.inArray($(this).val(), variable_checked_district) != -1)
	                $(this).attr('checked', true);
	        })
	    }

	    $(".speed").css({
	        "display": "block"
	    });
	    showhidePopup('variablefiltetr');
	});

    $(document).on("click",'.closeme1',function(){
    	showhidePopup($(this).data('me'),'hide');
   		// onclick="document.getElementById('variablefiltetr').style.display='none';document.getElementById('fade').style.display='none';"
	});

     function showhidePopup(popupid,action='show')
    {
        if(action =="show")
        {
            $('#'+popupid).show();
            $('#fade').show();
        }
        else
        {
            $('#'+popupid).hide();
            $('#fade').hide();
        }
    }

	///this is not needed now because we have converted radio into buttons
	$(document).on('click', '#showspeed', function(){
		if($('.metervariables_checkbox:checked').length == 0){
			alert(metervariabel);
			return false;
		}

		if($('.meterdistrict_checkbox:checked').length == 0){
			alert(meterdistrict);
			return false;
		}

		showhidePopup('variablefiltetr','hide');

		var speedvar    = new Array();
		var speedregion = new Array();
	    $(".metervariables_checkbox:checked").each(function() {
			speedvar.push($(this).val());
		});
		$(".meterdistrict_checkbox:checked").each(function() {
			speedregion.push($(this).val());
		});

		if($('.meterdistrict_checkbox').data('multiple')==1)
		{
			//district
			//	$('.metervariables_checkbox').data('multiple',0);
			variable_checked_layer = speedvar;
			district_checked_layer = speedregion;
			console.log(speedvar,"layer");
			console.log(speedregion,"layer");
			$('.filterbylabel').html('<b>Filter By Layer: </b><div class="poptitle">'+speedregion+'</div>');
		}
		else
		{
			//layer
			variable_checked_district = speedvar;
			district_checked_district = speedregion;
			console.log(speedvar,"variabel");
			console.log(speedregion,"variabel");
			 //bc = speedvar.replace(/_/g,' ');
			$('.filterbylabel').html('<b>Filter By variable: </b><div class="poptitle"></div>');

		}

		var selectedvar,selectedregion ;
		selectedvar    = speedvar.join(',') ;
		selectedregion = speedregion.join(',') ;

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getsmeter&region="+selectedregion+"&speedvar="+selectedvar+"&filterspeed="+filterspeed_radio,
			type    : 'GET',
			beforeSend: function() {
	                $('#main_loader').show();
	            },
			success: function(data) {
				$('#main_loader').hide();
				$("#spedometer_region").html("");
				$("#spedometer_region").html(data);
				showhidePopup('variablefiltetr','hide');
				initspeed();
				$("#spedometer_region").css({
					"width": "915px"
				});
				$("#spedometer_region").css({
					//"overflow": "auto"
				});
			}
		});
	});

    $(document).on("click", "#speedfiltershow" , function() {
        $("#speedfiltershow").css({
            "display": "none"
        });
        $(".sfilter").css({
            "display": "block"
        });
        $("#spedometer_region").css({
            "width": "681px"
        });
        $("#spedometer_region").css({
            "overflow": "auto"
        });
    });

    if(typeof(usedlevel) != "undefined"){
        var ulevel = usedlevel.split(",");
        if(ulevel.length == 3){
            JQuery("#themeconent").css({"display":"none"});
        }
        getColorPallet();
    }

    var graphdist = $("#district").val();

    var minval        = "";
    var maxval        = "";
    var colorselected = "";
    var edittheme     = 0;

     $(".hideothers").css({
        "display": "none"
    });

    if (typeof(havingthematicquery) != "undefined") {
        var singlequery = havingthematicquery.split(",");
        $("#thematic_attribute option").each(function() {
            for (var i = 0; i <= singlequery.length; i++) {
                if (singlequery[i] == $(this).val()) {
                    $(this).attr("disabled", true);
                }
            }
        });
    }

	//for search START
	$('.searchtop').keyup(function(event) {
		var input, filter, a, id;
		var rows_selected=[];
		id     = $(this).attr('id');
		input  = id.split('_');
		filter = this.value.toUpperCase();

		$("."+ input[0] +"list li").each(function(){
			a = $('label',this).text();

			if (a.toUpperCase().indexOf(filter) > -1)
			{
			 if($.inArray(a, rows_selected) !== -1)
			 {
			 $(row).find('input[type="checkbox"]').prop('checked', true);
       		 $(row).addClass('selected');
       		 }
			$(this).show();
			}
			else
			{
				$(this).hide();
			}
		});
	});
	//for search END

	$('.state_checkbox').click(function(e) {
		var slvals = [];
		$('.state_checkbox:checked').each(function() {
			slvals.push($(this).val());
		});

		selected = slvals.join(',');
		getfrontdistrict(selected);
		checkvalidation();
	});

	$('#leftcontainer').on("click",'.district_checkbox',function(e) {
	    var slvals = [];
	    $('.district_checkbox:checked').each(function() {
	        slvals.push($(this).val())
	    });
	    selected = slvals.join(',');
	    getAttribute(selected);

	    var state_code = $(this).attr('data-id');
        var checked_len = $('.districtlabel_checkbox-'+state_code+':checked').length ;
        var total_len = $('.districtlabel_checkbox-'+state_code).length ;

        if(checked_len == total_len) {
            $('#districtlabel_allcheck-'+state_code).prop('checked',true);
        } else if ( checked_len != total_len) {
        	$('#districtlabel_allcheck-'+state_code).prop('checked',false);
        }

        if($('.districtNamegrp:checked').length == $('.districtNamegrp').length ) {
        	$('#district_allcheck').prop('checked',true);
        } else if ($('.districtNamegrp:checked').length != $('.districtNamegrp').length){
        	$('#district_allcheck').prop('checked',false);
        }
    });

    $(document).on("click",'.subdistrict_checkbox',function() {

	   var data_id = $(this).data('id');
	   var checked_len = $('.subdistrictid_checkbox-'+data_id+':checked').length;
	   var total_len = $('.subdistrictid_checkbox-'+data_id).length;

	   if( checked_len == total_len ) {

	   		$('.subdistrictlabel_allcheck-'+data_id).prop('checked',true);
	   } else if ( checked_len != total_len){

	   		$('.subdistrictlabel_allcheck-'+data_id).prop('checked',false);
	   }

	   if($('.subdistrictNameGrp:checked').length ==  $('.subdistrictNameGrp').length ) {

	   	$('#subdistrict_allcheck').prop('checked',true);
	   } else if ($('.subdistrictNameGrp:checked').length !=  $('.subdistrictNameGrp').length ){

	   	$('#subdistrict_allcheck').prop('checked',false);
	   }

    });

    $(document).on('click','#subdistrict_allcheck',function() {
    	if ($(this).prop('checked')) {
    		$('.subdistrictNameGrp').prop('checked',true);
    		$('.subdistrict_checkbox').prop('checked',true);
    	} else {
    		$('.subdistrictNameGrp').prop('checked',false);
    		$('.subdistrict_checkbox').prop('checked',false);
    	}
    });

    $(document).on('click','.subdistrictNameGrp',function(){
    	var data_attr = $(this).data('attr');
    	var checked_len = $('.subdistrictlabel_allcheck-'+data_attr+':checked').length;
    	var total_len = $('.subdistrictlabel_allcheck-'+data_attr).length;

    	if($(this).prop('checked')) {
    		$('.subdistrictid_checkbox-'+data_attr).prop('checked',true);
    	} else {
    		$('.subdistrictid_checkbox-'+data_attr).prop('checked',false);
    	}

    	if ($('.subdistrict_checkbox:checked').length == $('.subdistrict_checkbox').length ) {
    		$('#subdistrict_allcheck').prop('checked',true);
    	} else if ($('.subdistrict_checkbox:checked').length != $('.subdistrict_checkbox').length ) {
    		$('#subdistrict_allcheck').prop('checked',false);
    	}

    });


    $(document).on('click','.variable_checkbox',function () {
    	var data_id =  $(this).data('id');
    	var checked_len = $('.variablelabel_checkbox-'+data_id+':checked').length;
    	var total_len = $('.variablelabel_checkbox-'+data_id).length;

    	if ( checked_len == total_len) {
    		$('#variablelabel_allcheck-'+data_id).prop('checked',true);
    	} else if (checked_len != total_len) {
    		$('#variablelabel_allcheck-'+data_id).prop('checked',false);
    	}

    	if ($('.var_allcheck:checked').length == $('.var_allcheck').length)	{
    		$('#variable_allcheck').prop('checked',true);
    	} else if ($('.var_allcheck:checked').length != $('.var_allcheck').length) {
    		$('#variable_allcheck').prop('checked',false);
    	}

    });

    $(document).on('click','.var_allcheck',function () {
    	var data_attr = $(this).data('attr');
    	if($(this).prop('checked') ) {
    		$('.variablelabel_checkbox-'+data_attr).prop('checked',true);
    	}

    	if ($('.var_allcheck:checked').length == $('.var_allcheck').length)	{
    		$('#variable_allcheck').prop('checked',true);
    	} else {
    		$('#variable_allcheck').prop('checked',false);
    	}
    });

    $(document).on('click','#variable_allcheck', function () {
    	if($(this).prop('checked')) {
    		$('.var_allcheck').prop('checked',true);
    		$('.variable_checkbox').prop('checked',true);
    	} else {
    		$('.var_allcheck').prop('checked',false);
    		$('.variable_checkbox').prop('checked',false);
    	}
    });

	JQuery('.distattrtypeselection').css({'display':'none'});
	JQuery('.districtspan,.urbanspan,.townspan').css({'display':'none'});
	JQuery('.group').css({'float':'left'});
	JQuery('.createneworkspace').css({'display':'none'});
	JQuery('.createnewworkspace').css({'display':'none'});

	//getAttribute(9);

	$(document).on('click', '#enablelegend', function(){
		chart1.options.legend.display = true;
		chart1.update();

		if (document.getElementById('enablelegend').checked) {
			chart1.options.legend.display = true;
			chart1.update();
		}
		else {
			chart1.options.legend.display = false;
			chart1.update();
		}
	});

		JQuery("#attr").live("click",function(){
		JQuery("#statetotal").toggle();
	});

	$(document).on('click', '#createworkspace', function(){

		var workspacename = JQuery("#new_w_txt").val();
		if(workspacename == ""){
			alert("Please Enter workspace name");
			return false;
		}

		$.ajax({
			url: "index.php?option=com_mica&task=summeryresults.saveWorkspace&name=" + workspacename+"&view=summaryshowresults",
			method: 'GET',
			success: function(data) {
				$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
				$("#activeworkspacename").text(workspacename);
				$("#workspacceedit").hide();
				$("#fade").hide();
			}
		});
	});

	JQuery("#updateworkspace").live("click",function(){
		var workspacename = JQuery("#new_w_txt").val();
		var workspaceid   = JQuery("#profile").val();

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid+"&view=summaryshowresults",
			method  : 'GET',
			success : function(data){
				$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
				$("#activeworkspacename").text(workspacename);
				$("#workspacceedit").hide();
				$("#fade").hide();

			}
		});
	});

	JQuery("#deleteworkspace").live("click",function(){
		if(!confirm("Are you Sure to Delete Workspace?")){
			return false;
		}

		var workspaceid = JQuery("#profile").val();
		var workspacename = JQuery("#new_w_txt").val();
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.deleteWorkspace&view=summaryshowresultse&name="+workspacename+"&workspaceid="+workspaceid,
			method  : 'GET',
			success : function(data){
				$("#workspacceedit").load(location.href + " #workspacceedit>*", "");
				$("#activeworkspacename").text(workspacename);
				$("#workspacceedit").hide();
				$("#fade").hide();

			}
		});
	});

 	$(document).on("click","#filterChartButton", function(){
 		if ( isGraphFilterDataLoad == 0) {
	        var checkedValue = [];
	        var inputElements = document.getElementsByClassName('chartstates_checkbox');

	        for(var i=0; inputElements[i]; ++i){
	              if(inputElements[i].checked){
	                   checkedValue[i] = inputElements[i].value;
	              }
	        }
	        checkedValue = checkedValue.filter(function(v){return v!==''});
	        var checkedDistValue = checkedValue.join(',');
	        var isVillageData = jQuery('#isVillageDataGet').val();

	        // Graph filter district
	        if(isVillageData == 1){
	        	JQuery.ajax({
	        		//https://www.mica-mimi.in/index.php?option=com_mica&task=villageshowresults.getSelectedDistListForGraph&checkedDistValue=
			        url     : "index.php?option=com_mica&task=villageshowresults.getSelectedDistListForGraph&checkedDistValue="+checkedDistValue,
			        method  : 'GET',
			        beforeSend: function() {
						jQuery('#main_loader').show();
						$('#cblist').html('');
					},
			        success : function(data){
			            $('#cblist').html(data);
			            isGraphFilterDataLoad = 1;
			            jQuery('#main_loader').hide();
			        }
			    });
	        }
	        else{
	        	JQuery.ajax({
			        url     : "index.php?option=com_mica&task=summeryresults.getSelectedDistListForGraph&checkedDistValue="+checkedDistValue,
			        method  : 'GET',
			        beforeSend: function() {
						jQuery('#main_loader').show();
						$('#cblist').html('');
					},
			        success : function(data){
			            $('#cblist').html(data);
			            isGraphFilterDataLoad = 1;
			            jQuery('#main_loader').hide();
			        }
			    });

	        }

	        var checkedVarValue = [];
	        var inputElements = document.getElementsByClassName('chartvariables_checkbox');

	        for(var i=0; inputElements[i]; ++i){
	              if(inputElements[i].checked){
	                   checkedVarValue[i] = inputElements[i].value;
	              }
	        }
	        checkedVarValue = checkedVarValue.filter(function(v){return v!==''});
	        var checkedVarValue = checkedVarValue.join(',');

	        // Graph filter variables
	        JQuery.ajax({
	            url     : "index.php?option=com_mica&task=summeryresults.getSelectedVarListForGraph&checkedVarValue="+checkedVarValue,
	            method  : 'GET',
	            beforeSend: function() {
					jQuery('#main_loader').show();
					$('#variabelist').html('');
				},
	            success : function(data){
	                $('#variabelist').html(data);
	                isGraphFilterDataLoad = 1;
	                //jQuery('#main_loader').hide();
	            }
	        });
	    }
	    else{
	    	$('#cblist').show();
	    	$('#variabelist').show();
	    }
    });

	// Graph Apply Changes Filter
	$(document).on("click","#graph_apply_changes", function(){
	    var districtVals                       = [];
	    var chartvariablesVals                 = [];
	    var chartSecondLevelchartvariablesVals = [];
	    var chartSecondLevelchartDistrictsVals = [];
	    var checkedelements                    = [];

	    $('.chartstates_checkbox:checked').each(function() {
	        districtVals.push($(this).val());
	        checkedelements.push($(this).val());
	    });
	    districtSelected = districtVals.join(',');

	    $('.chartvariables_checkbox:checked').each(function() {
	        chartvariablesVals.push($(this).val());
	        checkedelements.push($(this).val());
	    });
	    chartvariablesValsSelected = chartvariablesVals.join(',');

	    $('.secondLevelchartvariables_checkbox:checked').each(function() {
	        chartSecondLevelchartvariablesVals.push($(this).val())
	    });
	    chartSecondLevelchartvariablesValsSelected = chartSecondLevelchartvariablesVals.join(',');

	    $('.secondLevelchartdistrict_checkbox:checked').each(function() {
	        chartSecondLevelchartDistrictsVals.push($(this).val())
	    });
	    chartSecondLevelchartDistrictValsSelected = chartSecondLevelchartDistrictsVals.join(',');

	    chart1.destroy();

	    if($('#chartype').val() != 'bar'){
            if (checkedelements.length >= 7) {
                alert("Variable + District Total Should be less then 6");
                loadCharts();
                return false;
            }
        }

	    loadCharts(null, districtSelected, chartvariablesValsSelected, 0, chartSecondLevelchartvariablesValsSelected, chartSecondLevelchartDistrictValsSelected,isFillter=1);

	    $('#graphfunction').hide();
	    $('#fade').hide();
	});

	JQuery("#submit").live("click",function(){
		if(JQuery("#comparedata").val() == 1){
			var statecompare    = JQuery("#statecompare").val();
			var districtcompare = JQuery("#districtcompare").val();
			var towncompare     = JQuery("#towncompare").val();
			var urbancompare    = JQuery("#urbancompare").val();

			if(urbancompare != ""){
				if(urbancompare == "all"){
					alert("All UA already Selected");
					return false;
				}
				var urban=JQuery("#urban").val();
				if(urban == ""){
					alert("Please Select UA to compare");
					return false;
				}else if(urban == "all"){
					alert("You can not select All UA to compare");
					return false;
				}
			}else if(towncompare != ""){
				if(towncompare == "all"){
					alert("All Towns already Selected");
					return false;
				}
				var villages=JQuery("#villages").val();
				if(villages == ""){
					alert("Please Select Villages to compare");
					return false;
				}else if(villages == "all"){
					alert("You can not select All Villages to compare");
					return false;
				}
			}
			else if(districtcompare!="")
			{
				if(districtcompare=="all")
				{
					alert("All Districts already Selected");
					return false;
				}
				var dist = JQuery("#summarydistrict").val();
				if(dist == ""){
					alert("Please Select District to compare");
					return false;
				}
				else if(dist=="all")
				{
					alert("You can not select All District to compare");
					return false;
				}
			}
			else if(statecompare!="")
			{
				if(statecompare=="all")
				{
					alert("All States already Selected");
					return false;
				}
				var stateval=JQuery("#summarystate").val();
				if(stateval=="")
				{
					alert("Please Select State to compare");
					return false;
				}
				else if(stateval=="all")
				{
					alert("You can not select All State to compare");
					return false;
				}
			}
			return true;

		}else{
			var resetmtype      = 0;
			var checked         = 0;
			var dataof          = JQuery("#dataof").val();
			var districtcompare = JQuery("#summarydistrict").val();
			if(dataof=="District"){
				if(JQuery("#summarydistrict").val()==""){
					alert("Please Select District First");
					return false;
				}
			}
			else if(dataof=="UA")
			{
				if(JQuery("#urban").val()=="")
				{
					alert("Please Select UA First");
					return false;
				}
			}
			else if(dataof=="Villages")
			{
				if(JQuery("#Villages").val()=="")
				{
					alert("Please Select Villages First");
					return false;
				}

			}

			JQuery("input:checkbox:checked").each(function(){
				var chk = JQuery(this).attr("checked");
				if(chk=="checked"){
					checked=1;
				}
			});

			if(checked==0){
				alert("Please select variable first");
				return false;
			}else{
				if(typeof(districtcompare)=="undefined" || districtcompare==""){
					//JQuery("#m_type").remove();
					//alert(JQuery("#m_type").val());
					return true;
				}
			}
		}
	});

	$(document).on('keyup','#no_of_interval',function(e){
		var str="";
		if(e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105){
			if(typeof(edittheme)=="undefined"){
				edittheme=0;
			}
			createTable(edittheme);
		}
	});

	$('body').on('click', '.simpleColorChooser', function() {
        var value = $('.simple_color').val();
        value     = value.replace("#", "");
        var steps = $("#no_of_interval").val();

        $.ajax({
            url: "index.php?option=com_mica&task=summeryresults.getColorGradiant&value=" + encodeURIComponent(value) + "&steps=" + parseInt(steps++),
            method: 'GET',
            success: function(data) {
                colorselected = data;
                var segment   = data.split(",");
                segment       = segment.reverse();

                for (var i = 1; i <= segment.length; i++) {
                    var ncol = "#"+JQuery.trim(segment[i]);
                    JQuery("#color"+i).fadeIn(1000,JQuery("#color"+i).css({"background-color":ncol}));
                    /*$("#color" + (i)).fadeIn(1000, $("#color" + (i)).css({
                        "background-color": "#" + segment[i]
                    }));*/
                }
            }
        });
    });

    $('body').on('click', 'a.deletegrp', function() {
        var level    = $(this).attr("class");
        var getlevel = level.split(" ");
        var getdelid = $(this).attr("id");
        var segment  = getdelid.split("del_");

        $.ajax({
            url: "index.php?option=com_mica&task=summeryresults.deletethematicQueryToSession&formula=" + encodeURIComponent(segment[1]) + "&level=" + getlevel[1],
            method: 'GET',
            success: function(data) {
                loadGis('thematic');
                document.getElementById('light_thematic').style.display = 'none';
            }
        });
    });

    $('body').on('click', 'a.edittheme', function() {
        document.getElementById('light_thematic').style.display = 'none';
        edittheme = 1;
        var cnt   = $("#themeconent").html();

        $("#themeconent").html("");
        $(".themeconent").html(cnt);
        $('.simpleColorContainer').remove();

        getColorPallet();

        var classname = $(this).attr("class");
        var myid      = this.id;
        var getcount  = classname.split(" ");
        var seg       = myid.split("__");

        $("#thematic_attribute").val(seg[0]);
        $("#level").val(seg[1]);

        $("#thematic_attribute").change();
        $("#no_of_interval").val(getcount[1]);
        createTable(edittheme);
        $("#no_of_interval").val(getcount[1]);
        $(".range_" + seg[1]).each(function(i) {
            i++;
            rangeid = ($(this).attr("id"));
            var mylimit = rangeid.split("-");
            $("#from" + i).val(mylimit[0]);
            $("#to" + i).val(mylimit[1]);
        });

        var endcolor = "";
        if (seg[1] == "0") {
            for (var i = 1; i <= getcount[1]; i++) {
                $("#color" + (i)).css({
                    "background-color": "" + $(".col_" + i).css("background-color")
                });
                if (getcount[1] == i) {
                    $(".simpleColorDisplay").css({
                        "background-color": $(".col_" + i).css("background-color")
                    });
                }
                //rangeid=$(".range"+i).attr("id");
            }
            //rangeid=$(".range"+i).attr("id");
        }
        //  $("#thematicquerypopup").click();
        thematicquerypopup(edittheme);
    });

	$('body').on('click', 'a.edittheme', function() {
		document.getElementById('light_thematic').style.display = 'none';
		edittheme=1;
		var cnt = JQuery("#themeconent").html();

		JQuery("#themeconent").html("");
		JQuery(".themeconent").html(cnt);
		JQuery('.simpleColorContainer').remove();

		getColorPallet();

		var classname = JQuery(this).attr("class");
		var myid      = this.id;
		var getcount  = classname.split(" ");
		var seg       = myid.split("__");

		JQuery("#thematic_attribute").val(seg[0]);
		JQuery("#level").val(seg[1]);

		JQuery("#thematic_attribute").change();
		JQuery("#no_of_interval").val(getcount[1]);
		createTable(edittheme);
		JQuery("#no_of_interval").val(getcount[1]);
		JQuery(".range_"+seg[1]).each(function(i){
			i++;
			rangeid     = (JQuery(this).attr("id"));
			var mylimit = rangeid.split("-");
			JQuery("#from"+i).val(mylimit[0]);
			JQuery("#to"+i).val(mylimit[1]);
		});

		var endcolor = "";
		if(seg[1] == "0"){
			for(var i=1;i<=getcount[1];i++){
				JQuery("#color"+(i)).css({"background-color":""+JQuery(".col_"+i).css("background-color")});
				if(getcount[1] == i){
					JQuery(".simpleColorDisplay").css({"background-color":JQuery(".col_"+i).css("background-color")});
				}
				//rangeid=JQuery(".range"+i).attr("id");
			}
		}
		//	JQuery("#thematicquerypopup").click();
		thematicquerypopup();
	});

	$(document).on("click", "#updatecustom", function() {
        var new_name   = $('#new_name').val();
        var oldattrval = $('#oldattrval').val();

        if ($("#new_name").val() == "") {
            alert("Please Select Custom Variable First!!");
            return false;
        }

        if (!validateFormula_v2()) {
            return false;
        }

        var attributevale = $('textarea#custom_attribute').text();
        $.ajax({
            url: "index.php?option=com_mica&task=summeryresults.updateCustomAttr&attrname=" + new_name + "&attributevale=" + encodeURIComponent(attributevale) + "&oldattrval=" + encodeURIComponent(oldattrval),
            method: 'GET',
            success: function(data)
            {
                $('#customlist').removeClass('active');
                $('#custom_manage').addClass('active');
                $('#custom_edit').removeClass('active');
                $('#home').removeClass('in active');
                $('#menu1').addClass('in active');
                $('#menu2').removeClass('in active');
                $('a.getmanagedata').trigger("click");

                alert("Custom Variable Updated Suceessfully.");
                //$("#light").hide();
                //getDataNew();
                getAttribute();
            }
        });
    });

	$(".full-data-view").on("#deletevariable", "click", function() {
		var new_name      = JQuery('.customedit').find(":selected").text();
		var attributevale = JQuery('textarea#custom_attribute').text();

		if(new_name=="" && attributevale==""){
			alert("Please Select Custom Variable");
			return false;
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.deleteCustomAttr&attrname="+new_name+"&attributevale="+attributevale,
			method  : 'GET',
			success : function(data){
				//alert(data);
				window.location='index.php?option=com_mica&view=summeryresults&Itemid=108';
				//window.location='index.php?option=com_mica&view=summeryresults&Itemid=108';
			}
		});
	});

	$(".full-data-view").on(".variablegrp", "click", function() {
		var myid = JQuery(this).attr("id");
		var dis  = JQuery('.'+myid).css("display");

		JQuery(".hideothers").css({"display":"none"});
		JQuery('.variablegrp').addClass("deactive");
		JQuery(this).addClass("active");
		JQuery(this).removeClass("deactive");

		if(dis=="block"){
			JQuery('.'+myid).css("display","none");
			JQuery(this).addClass("deactive");
			JQuery(this).removeClass("active");
		}else{
			JQuery('.'+myid).css("display","block");
			JQuery(this).addClass("active");
			JQuery(this).removeClass("deactive");
		}
	});

	$(".full-data-view").on(".hovertext", "mouseover", function(e) {
		var allclass  = JQuery(this).attr("class");
		var segment   = allclass.replace("hovertext ","");
		var x         = e.pageX - this.offsetLeft;
		var y         = e.pageY - this.offsetTop;
		var popuphtml ="<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>"+segment+"</div>";
		JQuery(this).append(popuphtml);
	});

	$(".full-data-view").on(".hovertext", "click", function(e) {
		JQuery(this).parent().prev().find("input").prop("checked",true);//,true);
	});

	$(".full-data-view").on(".hovertext", "mouseout", function(e) {
		JQuery("#popupattr").remove();
	});

	$("#tabvariables").on(".customvariablecheckbox", "click", function() {
		var check = (JQuery(this).attr("checked"));

		if(check == "checked"){
			JQuery("#new_name").val(JQuery(this).attr("id"));
			JQuery("#custom_attribute").text(JQuery(this).val());
			JQuery("#save").click();
			//addCustomVariable(JQuery(this).val(),JQuery(this).attr("id"));
		}else{
			window.location = "index.php?option=com_mica&task=summeryresults.deleteattribute&attr="+JQuery(this).attr("id");
		}
	});

	JQuery("li #menu100").parent().attr("class","active");

	$(".contenttoggle").css({
        "display": "none"
    });

	$(document).on("click", ".customedit", function(){
		JQuery('#save').attr("id","updatecustom");
		JQuery('#updatecustom').attr("onclick","javascript:void(0)");
		JQuery('#updatecustom').attr("value","Update");

		JQuery('#new_name').val(JQuery(this).attr("id"));
		JQuery('#new_name').attr('disabled', true);
		JQuery('.aa').html("Edit");

		oldattrval=JQuery(this).attr("value");
		JQuery('#oldattrval').val(oldattrval);
		JQuery('textarea#custom_attribute').text(JQuery(this).attr("value"));
		lastchar = '';
		//moverightarea();
		//JQuery('#new_name').val(JQuery(this).prev().attr("id"));
		//JQuery('#custom_attribute').val(JQuery(this).prev().val());
		document.getElementById('light').style.display='block';
		document.getElementById('fade').style.display='block';
	});

	JQuery(document).on('click', '#closeextra', function(){

		JQuery('#updatecustom').attr("id","save");
		JQuery('#save').attr("onclick","checkfilledvalue();");
		JQuery('#save').attr("value","Save");
		JQuery('#new_name').val("");
		JQuery('#new_name').attr('disabled', false);
		JQuery('.aa').html("Add New");
	});

	JQuery(".deletecustomvariable").live("click",function(){
		var names = new Array();
		var i     = 0;
		JQuery(".dcv:checked").each(function(i){
			names.push(JQuery(this).attr("id"));
		});
		window.location="index.php?option=com_mica&task=summeryresults.deleteCustomVariableFromLib&attrname="+names;
	});

	$(document).on("click", "#fullscreen, #fullscreen1", function() {
        var fromthematic = $("#fullscreen").attr("fromthematic");

        if (fromthematic == 1) {
            popped = open(siteurl + 'index.php?option=com_mica&view=summeryfullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
        } else {
            popped = open(siteurl + 'index.php?option=com_mica&view=summeryfullmap&Itemid=108&tmpl=component', 'MapWin');
        }

        popped.document.body.innerHTML = "<div id='map' style='height:" + $(window).height() + "px;width:" + $(window).width() + "px;'></div>";
    });

	$(document).on('click', '.fullscreeniconoff', function()
	{
		$("#fullscreentable").removeClass("fullscreentable");
		$("#matrixclose").remove();
		$(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix' onclick='downloadQuartiles()';/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");
		//add by ujash
		$(this).val('Full Screen');
		//end
		$(this).removeClass("fullscreeniconoff");
		$(this).addClass("fullscreenicon");
		$("#fade").css({
			"display": "none"
		});

		$("#tablescroll").removeAttr("width");
        $("#tablescroll").removeAttr("overflow");
        $("#tablescroll").removeAttr("height");
	});

	$(document).on('click', '#matrix', function(){
		if(JQuery("#fullscreentable").html().length < 100){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=summeryresults.getMaxForMatrix",
				method  : 'POST',
				success : function(data){
					JQuery("#fullscreentable").html(data);
				}
			});
		}
	});

	// GIS Data Show
    $(document).on('click', '#gisdata', function(){
        if (isGISLoad == 1) {
            $("#result_table").hide();
            $("#graph").hide();
            $("#gis").show();
            $("#quartiles").hide();
            $("#potentiometer").hide();
            $('#villagedistictdata').hide();
        }else{
            loadGis('gis');
        }
    });

	// Graph Data Show
    $(document).on('click', '#loadchart', function(){
        //if (isGraphLoad == 1) {
            $("#result_table").hide();
            $("#gis").hide();
            $("#graph").show();
            $("#quartiles").hide();
            $("#potentiometer").hide();
            $('#villagedistictdata').hide();
        //}else{
        	loadCharts(null, null, null, 1, null, null);
        //}
    });

    $(document).on('click', '#tabledata', function(){
    	 if (isDatatableLoad == 1) {
    	 	$('#isVillageDataGet').val(0);
    	 	isGraphFilterDataLoad = 0;
            $("#result_table").show();
            $("#gis").hide();
            $("#graph").hide();
            $("#quartiles").hide();
            $("#potentiometer").hide();
            $('#villagedistictdata').hide();
        }else{
            getDataNew();
        }
    });

    //End
	$(document).on('click', '#showchart', function(){
		var checkedelements = new Array();
		var checkeddist     = new Array();
		var checkevar       = new Array();
		JQuery("#selectdistrictgraph").find(".districtchecked").each(function(){
			if(JQuery(this).attr("checked")){
				checkedelements.push(this.val);
				checkeddist.push(JQuery(this).val());
			}
		});

		JQuery("#light1007").find(".variablechecked").each(function(){
			if(JQuery(this).attr("checked")){
				checkedelements.push(JQuery(this).val());
				checkevar.push(JQuery(this).val());
			}
		});

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=summeryresults.getGraph",
			type    : 'POST',
			data    : "dist="+checkeddist+"&attr="+checkevar,
			success : function(data){
				var segments = data.split("<->");
				datastr    = segments[0];
				grpsetting = segments[1];
				JQuery("#chartype").change();
			}
		});
	});

	});