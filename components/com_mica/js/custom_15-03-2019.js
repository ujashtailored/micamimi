var filterspeed_radio;
var chartconfig ;
var chart1;
var venn_sets                 = [];
var venn_sets_main            = [];
var chart_selected_vars       = [];
var district_checked_layer    = [];
var variable_checked_layer    = [];
var district_checked_district = [];
var variable_checked_district = [];
var venn_selected_vars        = [];
var venn_selected_districts   = [];
var edittheme                 = 0;
var originalLabels            = [];
var venn_title ;

function getfrontdistrict_v2(val) {
    if (val == "") {
        $('.level2').css({
            'display': 'none'
        });

        getAttribute_v2(5, "state");

        $(".distattrtypeselection").css({
            "display": "none"
        });

        return false;
    }
    else if (val == "all")
    {
        document.getElementById('district').selectedIndex = 0;
        document.getElementById('urban').selectedIndex = 0;
        document.getElementById('town').selectedIndex = 0;

        $('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({
            'display': 'none'
        });

        getAttribute_v2(5, "state");

        return false;
    }
    else
    {
        getAttribute_v2(5, "state");

        $(".distattrtypeselection").css({
            "display": "none"
        });
    }

    $("#main_loader").show();

    $('.distattrtypeselection').css({
        'display': 'none'
    });

    $.ajax({
        url: "index.php?option=com_mica&task=micafront.getsecondlevel&stat=" + val + "&preselected=" + preselecteddata,
        method: 'post',
        success: function(data)
        {
            var segment = data.split("split");

            $(".districtlist").html(segment[0]);
            $("#statecode").html(segment[1]);

            if (preselecteddata != "")
            {
                getAttribute_v2(5, "district");
            }

            var dataof = $("#dataof").val();
            if (typeof(preselecteddataof) != "undefined")
            {
                var selectedtype = preselecteddataof.toLowerCase();
                if (selectedtype == "ua" || selectedtype == "UA")
                {
                    selectedtype = "urban";
                }
            }

            if (dataof == "ua" || dataof == "UA")
            {
                dataof = "urban";
            }
            checkvalidation();
            $("#main_loader").hide();
        }
    });
}

function getdistrict_v2(val) {
    if (val == "") {
        $('.level2').css({
            'display': 'none'
        });
        getAttribute_v2(5, "state");
        return false;
    } else if (val == "all") {
        $('.level2').css({
            'display': 'none'
        });
        getAttribute_v2(5, "state");
        return false;
    } else {
        getAttribute_v2(5, "state");
    }

    $('.distattrtypeselection').css({
        'display': 'none'
    });

    $.ajax({
        url: "index.php?option=com_mica&&task=micafront.getsecondlevel&stat=" + val,
        method: 'post',
        success: function(combos) {
            $('.level2').css({
                'display': 'block'
            });
            var segment = combos.split("split");
            $("#districtspan").html(segment[0]);
            $("#villagespan").html(segment[0]);
        }
    });
}

function getdistrictattr_v2(val){
    var val = $('.district_checkbox').val();
    $("#loaderdistrict").show();

    if (val == "")
    {
        $('.townspan').css({
            'display': 'block'
        });
        $('.urbanspan').css({
            'display': 'block'
        });
        $('.distattrtypeselection').css({
            'display': 'none'
        });
        resetCombo('district');

        getAttribute_v2(5, "state");
    }
    else
    {
        getAttribute_v2(5, "district");

        $("#loaderdistrict").hide();
    }
}

// AJAX IN STATE
function getAttribute_v2(javazoom, type){
    var allVals = [];
    var slvals = [];
    $('.district_checkbox:checked').each(function() {
        slvals.push($(this).val())
    })

    selected = slvals.join(',');

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getAttribute&zoom=" + parseInt(javazoom) + "&type=" + type + "&district=" + selected,
        method: 'GET',
        async: true,
        success: function(data) {
            var segment = data.split("split");

            $("#variables").html(segment[0]);
            $("#variableshortcode").html(segment[1]);
            $("#CustomVariable").html(segment[2]);
            $(".hideothers").css({
                "display": "none"
            });
        }
    });
}

/*
Load Charts
 */
function loadCharts(data=null, selectedDistrict=null, selectedVariable=null, initialGraph=0, selectedSecondLevelVariable=null, selectedSecondLevelDistrict=null){

    // Check filter variable selected or not
    if(selectedDistrict == null && selectedVariable == null){
        var districtVals                       = [];
        var chartvariablesVals                 = [];
        $('.chartstates_checkbox:checked').each(function() {
            districtVals.push($(this).val());
        });
        selectedDistrict = districtVals.join(',');

        $('.chartvariables_checkbox:checked').each(function() {
            chartvariablesVals.push($(this).val());
        });
        selectedVariable = chartvariablesVals.join(',');
    }

    if(data==null)
    var data = $('#micaform').serialize();
    data = data+"&selectedDistrict="+selectedDistrict+"&selectedVariable="+selectedVariable+"&initialGraph="+initialGraph+"&selectedSecondLevelVariable="+selectedSecondLevelVariable;

    JQuery.ajax({
        url     : "index.php?option=com_mica&task=showresults.getGraphajax",
        method  : 'post',
        async   : true,
        data    : data,
        beforeSend: function() {
            jQuery('#main_loader').show();
        },
        success : function(data){
            if (chart1 != undefined) {
                chart1.destroy();
            }

            jQuery('#main_loader').hide();
            JQuery("#gis").hide();
            JQuery("#graph").show();
            JQuery("#result_table").hide();
            JQuery("#quartiles").hide();
            JQuery("#potentiometer").hide();

            /*------------------bakground coclor------------*/
            var backgroundColor = 'white';
            Chart.plugins.register({
                beforeDraw: function(c) {
                    var ctx = c.chart.ctx;
                    ctx.fillStyle = backgroundColor;
                    ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                }
            });
            /*--------------bakground coclor end------------*/

            var chartType              = document.getElementById("chartype").value;
            var ctx                    = document.getElementById('myChart').getContext('2d');
            var datasets               = [];
            var secondlevelDatasetList = [];
            var i                      = 0;
            var result                 = JQuery.parseJSON(data);
            // Removed Second Level Data from other graph
            if($('#chartype option:selected').text() != "Second Level Chart")
            {
                delete result.secondlevelDatasets;
            }
            var color                    = Chart.helpers.color;
            var colorcode                = ["rgba(255, 99, 132)", "rgba(54, 162, 235)", "rgba(255, 206, 86)", "rgba(75, 192, 192)", "rgba(255, 159, 64)"];
            var labels                   = [];
            var joinedClassesmain        = [];
            var i                        = 0, col = 0;
            //var keys                   = JQuery.map(result, function(element,index) {return index}).splice(0,5);
            var keys                     = JQuery.map(result, function(element,index) {return index });
            venn_sets                    = [];
            venn_sets_main               = result;
            venn_selected_districts      = [];
            var joinedClasses            = new Array();
            var secondLevelDistrictLabel = [];

            JQuery.each(result,function(key,value){
                JQuery.each(value,function(key1,value1){

                    if(i==0)
                    {
                        labels.push(key1);
                        venn_selected_districts.push(key1);
                        distrcitname = key1.split("-");
                        distrcitname = distrcitname[0];
                        if(venn_sets.length < 5)
                        {
                            secondLevelDistrictLabel.push(distrcitname.trim());
                            venn_sets.push ({'title':distrcitname.trim(), 'views':parseFloat(value1)});
                            venn_title = key;
                            if(col==0){
                                venn_selected_vars.push(key);
                            }
                        }
                    }
                    if (joinedClasses[key1] === undefined)
                    {
                        joinedClasses[key1]= new Array();
                    }
                    joinedClasses[key1][key] = value1;// vaule define
                    col++;
                });
                    i++;
            });

            var i = 0;
            //var statecity = '';
            //statecity = '<ul id ="statesForGraph" class="list1">';
            JQuery.each(labels,function(key,value){//value:- cityname,joinedClasses[value]:-var+value
                var res       = value.split("~~");
                var cityname  = res[0];
                var citycode  = res[1];

                /*var container = $('#cblist');
                statecity += "<li>"+
                '<input type = "checkbox" class="chartstates_checkbox" '+
                ' id="chartstates_' + citycode +'" value="'+ citycode+ '" checked/>'+
                '<label for="check">'+ cityname+ "</label>"+
                "</li>";

                 $('<label />', { 'for': 'check', text: cityname }).appendTo(container);*/
                var newres = Object.keys(joinedClasses[value]).map(function(k) {
                    return [k, joinedClasses[value][k]];
                });

                var joinedClassesmain = [];
                $.each(newres, function (key1,value1) {
                    joinedClassesmain.push(value1[1]);
                });

                var variable1   = [];
                var newhtml     = '';
                //newhtml       = '<ul id ="variablesForGraph" class="list1">';

                $.each(newres, function (key1,value1) {
                    variable1 = value1[0];
                    /*newhtml += "<li>"+
                        '<input type = "checkbox" class="chartvariables_checkbox" '+
                                    ' id="chartvariables_' + variable1 +'" value="'+ variable1+ '"/>'+
                        '<label for="chartvariables_' + variable1 + '">'+ variable1+ "</label>"+
                         "</li>";*/
                });

                //newhtml +='</ul>';

                //$('#variabelist').html(newhtml);

                datasetValues[citycode] = joinedClassesmain;

                if(i<5)
                {
                    datasets.push({
                        'label':cityname, //city name should come here
                        'backgroundColor':color(colorcode[i]).alpha(0.5).rgbString(),
                        'borderColor':colorcode[i],
                        'borderWidth':1,
                        'data':joinedClassesmain,
                        'id':citycode,
                        'id1':variable1
                    });
                }

                i++;
            });

            newlabel = [];
            originalLabels = keys;
            keys.forEach(function(e,i){
                data = e.replace(/(\s[^\s]*)\s/g,"$1--");
                rres = data.split("--");
                newlabel.push(rres);
            });

            console.log(datasets);
            var displaySecondLevel = false;
            if($('#chartype option:selected').text() == "Second Level Chart")
            {
                $.each(result.secondlevelDatasets, function (secondlevelDatasetsKey,value) {
                    secondlevelDatasetList.push({
                        'label':value.label, //city name should come here
                        'backgroundColor':color(colorcode[secondlevelDatasetsKey]).alpha(0.5).rgbString(),
                        'borderColor':colorcode[secondlevelDatasetsKey],
                        'borderWidth':1,
                        'data':value.data
                    });

                    if (selectedSecondLevelVariable != null) {

                        secondLevelchartvariableName = selectedSecondLevelVariable.replace(/_/g," ");
                        secondLevelchartvariableNameArray = secondLevelchartvariableName.split(",");

                        var indexoflabels = $.inArray(value.label, secondLevelchartvariableNameArray);

                        if (indexoflabels !== -1) {
                            displaySecondLevel = true;
                            secondlevelDatasetList[secondlevelDatasetsKey].yAxisID = "y-axis-2";
                        }
                        else{
                            secondlevelDatasetList[secondlevelDatasetsKey].yAxisID = "y-axis-1";
                        }
                    }
                });

                chartconfig = {
                    type: 'bar',
                    data: {
                        labels:secondLevelDistrictLabel,
                        datasets: secondlevelDatasetList
                    },
                    options: {
                        legend: {
                            display: true,
                        },
                        scales: {
                            yAxes: [{
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                position: "left",
                                id: "y-axis-1",
                            }, {
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: displaySecondLevel,
                                position: "right",
                                id: "y-axis-2",
                                gridLines: {
                                    drawOnChartArea: true
                                },
                                afterBuildTicks: function(scale) {
                                  scale.ticks = updateChartTicks(scale);
                                    return;
                                  },
                                  beforeUpdate: function(oScale) {
                                    return;
                                  },
                                  ticks: {
                                    beginAtZero:true,
                                    maxTicksLimit: 10,
                                  }
                            }],
                            xAxes: [{
                                ticks: {
                                    maxTicksLimit: 8,
                                    autoSkip: false
                                   // display:false
                                }
                            }]
                        }
                    },
                };
            }
            else{
                chartconfig = {
                    type: chartType,
                    // The data for our dataset
                    data: {
                        labels:newlabel,
                        datasets: datasets
                    },
                    options: {
                        legend: {
                            display: true,
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    maxTicksLimit: 8,
                                    autoSkip: false
                                   // display:false
                                }
                            }]
                        }
                    },
                };
            }

            chart1 = new Chart(ctx, chartconfig);

            // Selected 5 Distict data default
            /*$('.chartstates_checkbox').click();
            $('.chartstates_checkbox:eq( 0 )').click();
            $('.chartstates_checkbox:eq( 1 )').click();
            $('.chartstates_checkbox:eq( 2 )').click();
            $('.chartstates_checkbox:eq( 3 )').click();
            $('.chartstates_checkbox:eq( 4 )').click();*/

            // Selected 5 Variable data default
            // $('.chartvariables_checkbox').click();
            // $('.chartvariables_checkbox:eq( 0 )').click();
            // $('.chartvariables_checkbox:eq( 1 )').click();
            // $('.chartvariables_checkbox:eq( 2 )').click();
            // $('.chartvariables_checkbox:eq( 3 )').click();
            // $('.chartvariables_checkbox:eq( 4 )').click();

            chart_selected_vars = [];
            $.each($(".chartvariables_checkbox:checked"), function(){
                chart_selected_vars.push($(this).val());
            });

            chart_selected_districts = [];
            $.each($(".chartstates_checkbox:checked"), function(){
                chart_selected_districts.push($(this).val());
            });

            if($('#chartype').val() =='bubble'){
                $('#venn').html('<svg></svg>');
                Bubblechartload();
            }
        }
    });
}

var updateChartTicks = function(scale) {
	var incrementAmount = 0;
	var previousAmount  = 0;
	var newTicks        = [];
	newTicks            = scale.ticks;

	for (x=0;x<newTicks.length;x++) {
    	incrementAmount = (previousAmount - newTicks[x]);
    	previousAmount = newTicks[x];
  	}

  	if (newTicks.length > 2) {
    	if (newTicks[0] - newTicks[1] != incrementAmount) {
      		newTicks[0] = newTicks[1] + incrementAmount;
    	}
  	}

 	return newTicks;
};

function gettown_v2(val) {

    $('.distattrtypeselection').css({
        'display': 'none'
    });

    $('.districtspan').css({
        'display': 'none'
    });

    $('.urbanspan').css({
        'display': 'none'
    });

    if (val == "")
    {
        $('.districtspan').css({
            'display': 'block'
        });

        $('.urbanspan').css({
            'display': 'block'
        });
    }
    getAttribute_v2(8, "town");
}

function geturban_v2(val) {
    $('.htitle').css({
        'width': '150px'
    });

    $('.distattrtypeselection').css({
        'display': 'none'
    });

    if (val == "") {
        $('.districtspan').css({
            'display': 'block'
        });

        $('.townspan').css({
            'display': 'block'
        });
    }
    getAttribute_v2(8, "urban");
}

function getvalidation_v2() {
    var myurl = "";
    var zoom = 5;
    if (document.getElementById('state').value == '') {
        return false;
    } else {
        myurl = "state=";
        $('#state :selected').each(function(i, selected) {
            myurl += $(selected).val() + ",";
            zoom = 5;
        });

        myurl += "&district=";
        $('#district :selected').each(function(i, selected) {
            myurl += $(selected).val() + ",";
            zoom = 6;
        });
        myurl += "&attributes=";

        $('input.statetotal_attributes[type=checkbox]').each(function() {
            if (this.checked) {
                myurl += $(this).val() + ",";
            }
        });
        $("#zoom").val(zoom);
        return false;
    }
}

function checkRadio_v2(frmName, rbGroupName) {
    var radios = document[frmName].elements[rbGroupName];
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            return true;
        }
    }
    return false;
}

function changeWorkspace(value) {

    if (value == 0)
    {
        $('.createneworkspace').css({
            'display': 'block'
        });

        $('.createnewworkspace').css({
            'display': ''
        });
        $('.thematiceditoption').css('display', 'block');
    }
    else
    {
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid=" + value + "&tmpl=component",
            method: 'GET',
            success: function(data) {
                result = $.parseJSON(data);

                $("#workspacceedit").load(location.href + " #workspacceedit>*", "");

                $('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
                    getAttribute_v2(5, "district");

                    if (typeof(preselected) != "undefined") {
                        displaycombo_v2();
                    }
                });

                $("#activeworkspacename").html(result[0].name);
                $("#workspacceedit").hide();
                $("#fade").hide();
                $("#default").show();
                //update form fields  as per selected workspace id
                setTimeout(function() {
                    jQuery('.scrollbar-inner').scrollbar();
                    //getDataNew();
                }, 1000);
            }
        });
    }
}

function getMinmaxVariable_v2(value) {
    $(".minmaxdisplay").html("Please Wait...");
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getMinMax&value=" + encodeURIComponent(value),
        method: 'GET',
        success: function(data) {
            var segment = data.split(",");
            minval = segment[0];
            maxval = segment[1];
            $(".minmaxdisplay").html("<b>MIN :</b>" + segment[0] + "<b><br/>MAX :</b>" + segment[1] + "");
            $("#maxvalh").val(segment[1]);
            $("#minvalh").val(segment[0]);
        }
    });
}

function createTable_v2(edittheme) {
    var maxval      = $("#maxvalh").val();
    var minval      = $("#minvalh").val();
    var level       = $("#level").val();
    var str         = "";
    var diff        = maxval - minval;
    var max         = minval;
    var disp        = 0;
    var setinterval = diff / ($("#no_of_interval").val());
    setinterval     = setinterval.toFixed(2);
    start           = minval;

    if ($("#no_of_interval").val() > 5) {
        alert("Interval must be less then 5");
        return false;
    }

    var colorselected = "";
    if (edittheme != 1 && usedlevel.indexOf("0") == -1) {
        str += '<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
        $(".colorhide").css({
            "display": ""
        });
        colorselected = 1;
    } else if (edittheme == 1 && level == "0") {
        str += '<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
        $(".colorhide").css({
            "display": ""
        });
        colorselected = 1;
    } else {
        str += '<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Pin</th></tr></thead>';
        $(".colorhide").css({
            "display": "none"
        });
        colorselected = 0;
    }

    for (var i = 1; i <= $("#no_of_interval").val(); i++) {
        if ((edittheme != 1 && usedlevel.indexOf("0") == -1)) {
            end   = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str   += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"></td>	</tr>';
            start = end;
        } else if (edittheme != 1 && usedlevel.indexOf("0") != -1 && level != "0") {

            if (usedlevel.indexOf("1") == -1) {
                level = 1;
                $("#level").val(1);
            } else {
                level = 2;
                $("#level").val(2);
            }

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"><img src="' + siteurl + '/components/com_mica/maps/img/layer' + level + '/pin' + i + '.png" /></td>	</tr>';
            start = end;

        } else if (edittheme == 1 && level != "0") {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"><img src="' + siteurl + '/components/com_mica/maps/img/layer' + level + '/pin' + i + '.png" /></td>	</tr>';
            start = end;

        } else if (edittheme == 1 && usedlevel.indexOf("0") != -1 && level == "0") {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ><td id="color' + i + '"></td>	</tr>';
            start = end;

        } else {

            end = (parseFloat(setinterval) + parseFloat(start)).toFixed(2);
            str += '<tr><td><input type="text" name="from1" id="from' + i + '" class="inputbox inputboxrange" value="' + start + '"></td><td><input type="text" name="to[]" id="to' + i + '" class="inputbox inputboxrange" value="' + end + '"></td ></tr>';
            start = end;

        }
    }

    $("#displayinterval").html(str);

    if (colorselected == 1) {
        $('.simpleColorChooser').click();
        //colorChooser();
    }
    //return str;
}

function getColorPallet_v2() {
    $('.simple_color').simpleColor({
        cellWidth: 9,
        cellHeight: 9,
        border: '1px solid #333333',
        buttonClass: 'colorpickerbutton'
    });
}

function saveSLD(){
    var formula     = $("#thematic_attribute").val();
    var limit       = $("#no_of_interval").val();
    var level       = $("#level").val();

    if(formula == "")
    {
        alert("Please Select Variable");
        return false;
    }
    else if(limit == "")
    {
        alert("Please Select Interval");
        return false;
    }

    var from  ="";
    var to    ="";
    var color ="";

    for(var i=1;i<=limit;i++)
    {
        from +=$("#from"+i).val()+",";
        to   +=$("#to"+i).val()+",";

        if(level=="0" || level=="")
        {
            color +=rgb2hex($("#color"+i).css("background-color"))+",";
        }
        else
        {
            color +=i+",";
        }
    }

    $.ajax({
        url     : "index.php?option=com_mica&task=showresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
        method  : 'GET',
        success : function(data){
            JQuery("#gis_script").load(" #gis_script > *");
            loadGis('thematic');
            document.getElementById('light_thematic').style.display = 'none';
            document.getElementById('light2').style.display = 'none';
            document.getElementById('fade').style.display   = 'none';
            // For Themetic query selected view show in fullscreen
            JQuery("#fullscreen").attr("fromthematic",1);
        }
    });
}

function rgb2hex(rgb) {
    if (typeof(rgb) == "undefined") {
        return "ffffff";
    }

    if (rgb.search("rgb") == -1)
    {
        return rgb;
    }
    else
    {
        rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);

        function hex(x)
        {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
}

function thematicquerypopup_v2(){
    if (totalthemecount == 3 && (typeof(edittheme) == "undefined" || (edittheme) == 0)) {
        alert("You Can Select maximum 3 Thematic Query for Single Workspace");
        return false;
    }
    else
    {
        $("#light2").fadeIn();
        $("#fade").fadeIn();
    }
}

function closePopup_v2() {
    edittheme = 0;
    document.getElementById('light2').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    $("#displayinterval").html("");

    //$("#thematic_attribute").val(0).attr("selected");
    document.getElementById("thematic_attribute").selectedIndex = 0;
    $("#no_of_interval").val("");
    $(".minmaxdisplay").html("");

    if ($(".themeconent").html().length > 10) {
        $("#themeconent").html($(".themeconent").html());
        $('.simpleColorContainer').remove();
        getColorPallet_v2();
        $(".simpleColorDisplay").css({
            "background-color": "#FFFFCC"
        });
    }
}

function validateFormula(){
    var assignformula = "";
    var formula       = $('textarea#custom_attribute').text();

    $("#avail_attribute option").each(function() {
        var myclass = $(this).parent().attr("class");
        if ($(this).val() == formula.trim() && myclass != "defaultvariablelib") {
            assignformula = "You have already assign '" + formula + "' value to '" + $(this).val() + "' Variable";
        }
    });

    if (assignformula != "") {
        alert(assignformula);
        return false;
    }
    return true;
}

function downloadMapPdf_v2() {
    var start         = map.layers;
    var selectedlayer = "";

    if (javazoom == 5 || javazoom == 6) {
        selectedlayer = "india:rail_state";
    } else if (javazoom == 7) {
        selectedlayer = "india:india_information";
    } else if (javazoom == 8) {
        selectedlayer = "india:jos_mica_urban_agglomeration";
    } else {
        selectedlayer = "india:my_table";
    }

    var baselayer     = "india:rail_state";
    var buildrequest  = "";
    var buildrequest1 = "";

    for (x in start) {
        for (y in start[x].params) {
            if (start[x].params.LAYER == selectedlayer) {
                if (y == "FORMAT") {
                    buildrequest1 += (y + "EQT" + encodeURIComponent(start[x].params[y])) + "AND";
                } else {
                    buildrequest += (y + "EQT" + encodeURIComponent(start[x].params[y])) + "AND";
                }
            }
        }
    }

    buildrequest  = buildrequest + "FORMATEQTimage/png";
    buildrequest1 = buildrequest1 + "FORMATEQTimage/png";
    var finalurl  = buildrequest + encodeURIComponent("ANDBBOXEQT" + map.getExtent().toBBOX() + "ANDWIDTHEQT925ANDHEIGHTEQT650");
    var finalurl1 = buildrequest1 + encodeURIComponent("ANDBBOXEQT" + map.getExtent().toBBOX() + "ANDWIDTHEQT925ANDHEIGHTEQT650");

    window.open("index.php?option=com_mica&task=showresults.exportMap&mapparameter=" + finalurl + "&baselayer=" + finalurl1);
}

function exportImage_v2(name){
    var store = '0'; // '1' to store the image on the server, '0' to output on browser
    var imgtype = 'jpeg'; // choose among 'png', 'jpeg', 'jpg', 'gif'
    var opt = 'index.php?option=com_mica&task=showresults.amExport&store=' + store + '&name=' + name + '&imgtype=' + imgtype;
    var flashMovie = document.getElementById('chartdiv');

    $("#exportchartbutton").html("Please Wait..."); //"Please Wait..."

    data = flashMovie.exportImage_v2(opt);
    flashMovie.amReturnImageData_v2("chartdiv", data);
}


function amReturnImageData_v2(chartidm, data) {
    var onclick = "exportImage($('#chartype option:selected').text())";
    var button = '<input type="button" name="downloadchart" onclick="' + onclick + '" class="button" value="Export">';
    var previoushtml = $("#exportchartbutton").html(button);
}

function preloader_v2() {
    var preloaderstr = "<div id='facebook' ><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><div id='block_4' class='facebook_block'></div><div id='block_5' class='facebook_block'></div><div id='block_6' class='facebook_block'></div></div>";
}

function toggleCustomAction_v2(ele, action) {

    if (action != "") {
        $(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("checked", action);
    } else {
        $(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').removeAttr("checked");
    }

    $(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').click();
}

function getRemove_v2(cls, a) {
    $.ajax({
        url: "index.php?option=com_mica&task=showresults.deleteattribute&attr=" + a + "&chkcls=" + cls,
        method: 'GET',
        async: true,
        success: function(data) {
            if (a == "MPI") {
                $(".mpi_checkbox").prop("checked", false);
            } else {
                value = a.replace(/ /g, "_");
                // Removed custom variable from datatable
                $("input[name='custom_variable["+ a +"]']").prop('checked',false);
                $(".variable_checkbox[value='" + a + "']").prop("checked", false);
                $(".swcs_checkbox[value=Rural_Score_" + value + "]").prop("checked", false);
                $(".swcs_checkbox[value=Urban_Score_" + value + "]").prop("checked", false);
                $(".swcs_checkbox[value=Total_Score_" + value + "]").prop("checked", false);
            }
            getDataNew();
        }
    });
}

function displaycombo_v2(value){
    if ($("#state").val() == "all") {
        alert("Please Select State First");
        document.getElementById('dataof').selectedIndex = 0;
        document.getElementById("district").selectedIndex = 0;
        return false;
    }
    else
    {
        var allVals = [];
        var slvals = [];
        $('.state_checkbox:checked').each(function() {
            slvals.push($(this).val())
        });
        selected = slvals.join(',');
        getfrontdistrict_v2(selected);
    }

    if (value == "District")
    {
        $(".districtspan").css({
            "display": "block"
        });
        $(".townspan").css({
            "display": "none"
        });
        $(".urbanspan").css({
            "display": "none"
        });
    }
}

function sortListDir(id){
    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list      = document.getElementById(id);
    switching = true;

    //Set the sorting direction to ascending:
    dir = "asc";

    //Make a loop that will continue until no switching has been done:
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        b         = list.getElementsByTagName("LI");
        text      = list.getElementsByTagName("label");

        //Loop through all list-items:
        for (i = 0; i < (text.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*check if the next item should switch place with the current item,
				    based on the sorting direction (asc or desc):*/

            if (dir == "asc") {
                if (text[i].innerHTML.toLowerCase() > text[i + 1].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically lower than current item,
				          mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (text[i].innerHTML.toLowerCase() < text[i + 1].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically higher than current item,
				          mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
        }

        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
				      and mark that a switch has been done:*/
            b[i].parentNode.insertBefore(b[i + 1], b[i]);
            switching = true;
            //Each time a switch is done, increase switchcount by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
				      set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir       = "desc";
                switching = true;
            }
        }
    }
}

function variable_sortListDir()
{
    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list      = $('.variablelist');
    switching = true;
    dir       = "desc";

    header = $('.variable_label');
    for(z=0; z<(header.length); z++)
    {
        var inner_class = $(header[z]).text();
        inner_class     = inner_class.split(' ').join('_');
        inner_class     = inner_class.toLowerCase();
        switching       = true;
        dir             = dir;

        while (switching)
        {
            switching = false;
            a = $('.'+inner_class);
            for (i = 0; i < (a.length - 2); i++) {
                shouldSwitch = false;

                if (dir == "asc") {
                    if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase())
                    {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if (dir == "desc")
                {
                    if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase())
                    {
                        shouldSwitch = true;
                        break;
                    }
                }
            }

            if (shouldSwitch)
            {
                var other = $(a[i]);
                $(a[i+1]).after(other.clone());
                other.after($(a[i+1])).remove();
                switching = true;

                switchcount++;
            }
            else
            {
                if (switchcount == 0 && dir == "desc")
                {
                    dir = "asc";
                    switching = true;
                }
            }
        }
    }
}

function swsc_sortListDir()
{
    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list      = $('.variablelist');
    switching = true;
    dir1      = "asc";

    header = $('.swsc_label');
    for(z=0; z<(header.length); z++)
    {
        var inner_class = $(header[z]).text();
        inner_class     = inner_class.split(' ').join('__');
        inner_class     = inner_class.toLowerCase();
        switching       = true;
        dir1            = dir1;

        while (switching) {

            switching = false;

            a = $('.'+inner_class);

            for (i = 0; i < (a.length - 1); i++) {
                shouldSwitch = false;

                if (dir1 == "asc") {
                    if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

                        shouldSwitch = true;
                        break;
                    }
                } else if (dir1 == "desc") {

                    if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

                        shouldSwitch = true;
                        break;
                    }
                }
            }

            if (shouldSwitch)
            {
                var other = $(a[i]);
                $(a[i+1]).after(other.clone());
                other.after($(a[i+1])).remove();
                switching = true;

                switchcount++;
            }
            else
            {
                if (switchcount == 0 && dir1 == "asc")
                {
                    dir1 = "desc";
                    switching = true;
                }
            }
        }
    }
}

function district_sortListDir()
{

    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list      = $('.variablelist');
    switching = true;
    dir2      = "desc";

    header =$('.district_label');
    for(z=0; z<(header.length); z++)
    {
        var inner_class = $(header[z]).text();
        inner_class     = inner_class.split(' ').join('_');
        inner_class     = inner_class.split('&').join('_');
        inner_class     = inner_class.toLowerCase();
        switching       = true;
        dir2            = dir2;

        while (switching)
        {
            switching = false;

            a = $('.'+inner_class);
            for (i = 0; i < (a.length - 1); i++) {
                shouldSwitch = false;

                if (dir2 == "asc") {
                    if ($(a[i]).find('label').text().toLowerCase() > $(a[i+1]).find('label').text().toLowerCase()) {

                        shouldSwitch = true;
                        break;
                    }
                } else if (dir2 == "desc") {

                    if ($(a[i]).find('label').text().toLowerCase() < $(a[i+1]).find('label').text().toLowerCase()) {

                        shouldSwitch = true;
                        break;
                    }
                }
            }

            if (shouldSwitch)
            {
                var other = $(a[i]);
                $(a[i+1]).after(other.clone());
                other.after($(a[i+1])).remove();
                switching = true;

                switchcount++;
            } else {

                if (switchcount == 0 && dir2 == "desc") {
                    dir2 = "asc";
                    switching = true;
                }
            }
        }
    }
}

function getvalidation(num){
    var myurl = "";
    myurl     = "state=";
    myurl     += $("#state").val();
    myurl     += "&district=";
    myurl     += $("#district").val();
    myurl     += "&attributes=";
    var attr  = "";

    $('#result_table').find('.statetotal_attributes' + num).each(function(key, value) {
        $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
    });
    getData();

    return true;
}

function getindlvlgrpvar(){
    var groupid = $('input[name=indlvlgrps]:checked').val();

    $.ajax({
        url: "index.php?option=com_mica&task=micafront.getIndLvlGrp",
        type: 'POST',
        data: "groupid=" + groupid,
        success: function(data, textStatus, xhr) {
            var data = $.parseJSON(data);
            $('.variable_checkbox').each(function(index, el) {
                if ($('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"))) {
                    $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked", false));
                }
            });

            $('.type_checkbox').each(function(index, el) {
                if ($('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"))) {
                    $('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked", false));
                }
            });

            if (data['variables'] != 'false') {
                $('.variable_checkbox').each(function(index, el)
                {
                    if ($.inArray($(this).val(), data['variables']) !== -1) {
                        $('.variable_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
                    }
                });
                $('.variable_checkbox').val(data['variables']);
            }

            if (data['type'] != 'false')
            {
                $('.type_checkbox').each(function(index, el) {
                    if ($.inArray($(this).val(), data['type']) !== -1)
                    {
                        $('.type_checkbox[value=' + $(this).val() + ']').prop("checked", $(this).prop("checked"));
                    }
                });
                $('.type_checkbox').val(data['type']);
            }
            checkvalidation();
        }
    });
}

function viewRecordPlanLimit()
{
    JQuery.ajax({
        url     : "index.php?option=com_mica&task=showresults.viewRecordPlanLimit",
        method  : 'GET',
        async   : false,
        success : function(data)
        {
            jQuery("#viewRecordPlanLimit").val(data);
        }
    });
}


// edited by salim STARTED 25-10-2018
function getDataNew(){

    // Check View Record Data as per plan
    viewRecordPlanLimit();
    var allow_view = JQuery("#viewRecordPlanLimit").val();

    // If limit is over then get O value
    if (allow_view == 0) {
        alert("Your Plan view limit is over. Please contact to MICA team.");
        return false;
    }


    apply_chng = document.getElementById("apply_chnages_val").value;
    var data   = $('#micaform').serialize();

    $.ajax({
        url: "index.php?option=com_mica&task=showresults.getDataajax",
        method: 'POST',
        async: false,
        data: data,
        beforeSend: function() {
            $('#main_loader').show();
        },
        success: function(data) {
            $("#apply_chnages_val").val(0);
            flag = true;
            $("#result_table").show();
            $("#graph").hide();
            $("#quartiles").hide();
            $("#potentiometer").hide();
            $("#gis").hide();
            $("#default3").hide();
            $(".alldata").html(data);

            //loadCharts();
            var graphdist = "";
            $('.district_checkbox:checked').each(function(i, selected) {

                graphdist += $(this).val() + ",";
            });
            graphdist = graphdist.slice(0, -1);
            $.ajax({
                url: "index.php?option=com_mica&task=showresults.getDistrictlist",
                type: 'POST',
                data: "dist=" + graphdist,
                success: function(data) {
                    $('#main_loader').hide();
                    $("#result_table").find("#selectdistrictgraph").html(data);
                    $("#speedfiltershow").find("#speed_variable").html(data);
                }
            });

            $('#main_loader').hide();
            $("#default").hide();
            $("#default1").hide();
            $("#light").hide();
            $("#applyChangesInitial").prop('btn-disabled', false);
            $("#apply_chnages").removeAttr('btn-disabled');
        }
    });
}

function userDownloadLimit()
    {
        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.downloadAllowedPlanLimit",
            method  : 'GET',
            async   : false,
            success : function(data)
            {
                jQuery("#downloadAllowedPlanLimit").val(data);
            }
        });
    }


function downloadAction() {
    // Check View Record Data as per plan
    userDownloadLimit();
    var allow_download = JQuery("#downloadAllowedPlanLimit").val();

    // If limit is over then get O value
    if (allow_download == 0) {
        alert("Your Plan download limit is over. Please contact to MICA team.");
        return false;
    }
    else{
        window.location = 'index.php?option=com_mica&task=showresults.exportexcel';
    }
}

function loadGis(type)
{
    jQuery('#map').html("");
    //JQuery("#map").load(" #map > *");
    var data = $('#micaform').serialize();
    JQuery.ajax({
        //url     : "index.php?option=com_mica&view=showresults&Itemid="+<?php echo $itemid; ?>+"&tmpl=component",
        url     : "index.php?option=com_mica&task=showresults.getGISDataajax&type="+type,
        method  : 'POST',
        async   : true,
        data    : data,
        beforeSend: function() {
          jQuery('#main_loader').show();
        },
        success : function(data){
            $("#apply_chnages_val").val(0);
            //JQuery("#gis").load(" #gis > *");
            jQuery('#main_loader').hide();
            JQuery("#gis").show();
            JQuery("#result_table").hide();
            JQuery("#graph").hide();
            JQuery("#quartiles").hide();
            JQuery("#potentiometer").hide();

            JQuery("#light_thematic").load(" #light_thematic > *");
            JQuery("#thematic_attribute").load(" #thematic_attribute > *");

            latestData = JQuery.parseJSON(data);

            tomcaturl              = latestData.gis_tomcaturl;
            mainzoom               = latestData.gis_zoom;
            javazoom               = latestData.gis_zoom;
            userid                 = latestData.gis_userid;
            activeworkspace        = latestData.gis_activeworkspace;
            mainGeometry           = latestData.gis_geometry;
            mainlonglat            = latestData.gis_longlat;
            mainState              = latestData.gis_state;
            mainDistrict           = latestData.gis_district;
            mainUnselectedDistrict = latestData.gis_UnselectedDistrict;
            mainTown               = latestData.gis_town;
            mainUrban              = latestData.gis_urban;
            var sldlinkDistricts   = latestData.gis_slddistrict;

            OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
                defaultHandlerOptions: {
                    'single'         : true,
                    'double'         : false,
                    'pixelTolerance' : 0,
                    'stopSingle'     : true,
                    'stopDouble'     : false
                },
                initialize: function(options) {
                    this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );

                    OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    );

                    this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click':this.getfeaturenfo,'dblclick': this.onDblclick
                        }, this.handlerOptions
                    );
                },
                onDblclick: function(e) {
                    JQuery(".olPopup").css({"display":"none"});
                    JQuery("#featurePopup_close").css({"display":"none"});
                    //changeAttrOnZoom(map.zoom);
                },
                getfeaturenfo :function(e) {
                    coordinates = e;
                    var params  = {
                        REQUEST       : "GetFeatureInfo",
                        projection    : "EPSG:4326",
                        EXCEPTIONS    : "application/vnd.ogc.se_xml",
                        BBOX          : map.getExtent().toBBOX(10),
                        SERVICE       : "WMS",
                        INFO_FORMAT   : 'text/html',
                        QUERY_LAYERS  : selectlayer(map.zoom),
                        FEATURE_COUNT : 6,
                        Layers        : selectlayer(map.zoom),
                        WIDTH         : map.size.w,
                        HEIGHT        : map.size.h,
                        X             : parseInt(e.xy.x),
                        Y             : parseInt(e.xy.y),
                        CQL_FILTER    : selectfilter(),
                        srs           : map.layers[0].params.SRS
                    };

                    // handle the wms 1.3 vs wms 1.1 madness
                    if(map.layers[0].params.VERSION == "1.3.0") {
                        params.version = "1.3.0";
                        params.i       = e.xy.x;
                        params.j       = e.xy.y;
                    } else {
                        params.version = "1.1.1";
                        params.y       = parseInt(e.xy.y);
                        params.x       = parseInt(e.xy.x);
                    }
                    OpenLayers.loadURL(tomcaturl, params, this, setHTML, setHTML);
                    var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
                    //lonLat.transform(map.displayProjection,map.getProjectionObject());
                    //map.setCenter(lonLat, map.zoom);
                    map.panTo(lonLat);
                    //map.addControl(ovControl);
                    //OpenLayers.Event.stop(e);
                }
            });

            var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
            var options = {
                controls      : [],
                maxExtent     : bounds,
                projection    : "EPSG:4326",
                maxResolution : 'auto',
                zoom          : mainzoom,
                units         : 'degrees'
            };

            map = new OpenLayers.Map('map', options);

            /*land = new OpenLayers.Layer.WMS("State Boundaries",
                tomcaturl,
                {
                    Layer       : 'india:rail_state',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : stateCqlFilter(),
                    SLD         : sldlinkStateBoundaries
                },
                {
                    isBaseLayer: false
                }
            );*/

            opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
                tomcaturl,
                {
                    Layers           : 'india:rail_state',
                    transparent      : false,
                    format           : 'image/png',
                    styles           : "dummy_state",
                    transitionEffect : 'resize'
                },
                {
                    isBaseLayer: true
                }
            );

            districts = new OpenLayers.Layer.WMS("districts",
                tomcaturl,
                {
                    Layer       : 'india:india_information',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : districtCqlFilter(),
                    SLD         : sldlinkDistricts
                },
                {
                    isBaseLayer: false
                },
                {
                    transitionEffect: 'resize'
                }
            );

            /*cities = new OpenLayers.Layer.WMS("Cities",
                tomcaturl,
                {
                    Layer       : 'india:my_table',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : cityCqlFilter(),
                    SLD         : sldlinkCities
                },
                {
                    isBaseLayer : false
                },
                {
                    transitionEffect : 'resize'
                }
            );*/

            /*urban = new OpenLayers.Layer.WMS("Urban",
                tomcaturl,
                {
                    Layer       : 'india:jos_mica_urban_agglomeration',
                    transparent : true,
                    format      : 'image/png',
                    CQL_FILTER  : urbanCqlFilter(),
                    SLD         : sldlinkUrban
                },
                {
                    isBaseLayer: false
                },
                {
                    transitionEffect: 'resize'
                }
            );*/

            if(mainzoom==5 || mainzoom==6 ){
                map.addLayers([land]);
            }else if(mainzoom==7){
                map.addLayers([districts]);
                districts.redraw(true);
            }else if(mainzoom==8){
                map.addLayers([urban]);
            }else{
                map.addLayers([cities]);
            }

            map.div.oncontextmenu = function noContextMenu(e) {
                if (OpenLayers.Event.isRightClick(e)){
                    displaymenu(e);
                }
                // return false; //cancel the right click of brower
            };

            //map.addControl(exportMapControl);
            map.addControl(new OpenLayers.Control.PanZoomBar({
                position : new OpenLayers.Pixel(5, 15)
            }));

            map.addControl(new OpenLayers.Control.Navigation({
                dragPanOptions: {enableKinetic: true}
            }));

            //map.addControl(new OpenLayers.Control.Scale($('scale')));
            map.addControl(new OpenLayers.Control.Attribution());
            //map.addControl(new OpenLayers.Control.MousePosition({element: $('location')}));
            //map.addControl(new OpenLayers.Control.LayerSwitcher());
            //alert(bounds);

            var click = new OpenLayers.Control.Click();
            map.addControl(click);
            click.activate();
            map.zoomTo(mainzoom);

            if(mainGeometry != 0 && mainGeometry != "")
            {
                var format    = new OpenLayers.Format.WKT();
                var feature   = format.read(mainGeometry);
                var homepoint = feature.geometry.getCentroid();
            }

            map.addLayers([opverviewland]);
            //if (!map.getCenter(bounds))
                //map.zoomToMaxExtent();
            map.zoomToExtent(bounds);
            //map.zoomToMaxExtent();

            //map.zoomTo(mainzoom);

            if(mainlonglat == "")
            {
                var curlonglat= '77.941406518221,21.676757633686';
            }
            else
            {
                var curlonglat = mainlonglat;
            }

            var longlatsegment=curlonglat.split(",");

            if(mainGeometry !="0")
            {
                map.setCenter(new OpenLayers.LonLat(homepoint.x, homepoint.y),javazoom );
                map.zoomTo(6);
            }
            else
            {
                map.setCenter(new OpenLayers.LonLat(longlatsegment[0],longlatsegment[1]),javazoom );
                map.zoomTo(4);
            }

            /*totalthemecount     = latestData.gis_totalthemecount;
            usedlevel           = latestData.gis_usedlevel;
            havingthematicquery = latestData.gis_havingthematicquery;

            JQuery("#light_thematic").load(" #light_thematic > *",function(){
                if(typeof(usedlevel) != "undefined"){
                    if (usedlevel != null) {
                        var ulevel=usedlevel.split(",");
                        if(ulevel.length == 3){
                            JQuery("#themeconent").css({"display":"none"});
                        }
                    }
                    getColorPallet_v2();
                }
            });*/

            if(type == "thematic") {
                totalthemecount     = latestData.gis_totalthemecount;
                usedlevel           = latestData.gis_usedlevel;
                havingthematicquery = latestData.gis_havingthematicquery;

                JQuery("#light_thematic").load(" #light_thematic > *",function(){
                    if(typeof(usedlevel) != "undefined"){
                        if (usedlevel != null) {
                            var ulevel=usedlevel.split(",");
                            if(ulevel.length == 3){
                                JQuery("#themeconent").css({"display":"none"});
                            }
                        }
                        getColorPallet_v2();
                    }
                });
            }else {
                JQuery("#light_thematic").load(" #light_thematic > *",function(){
                    if(typeof(usedlevel) != "undefined"){
                        if (usedlevel != null) {

                            var ulevel=usedlevel.split(",");
                            if(ulevel.length == 3){
                                JQuery("#themeconent").css({"display":"none"});
                            }
                        }
                        getColorPallet_v2();
                    }
                });
            }
        }
    });
}

function checkvalidation(){
	var statecheck = false, districtcheck = false, typecheck = false, variablecheck = false;

    //check for state selected or not
	if ($('.state_checkbox:checked').length == 0) {
        $("#statetext").attr("style", "color:black");
        statecheck = false;
    }
    else
    {
   		$("#statetext").attr("style", "color:#ba171b");//red
        $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
        statecheck = true;
    }

    // check for district selected or not
    if ($('.district_checkbox:checked').length == 0) {
        $("#districttext").attr("style", "color:black");
        districtcheck = false;
    }
    else
    {
    	$("#districttext").attr("style", "color:#ba171b");
        $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
        districtcheck = true;
    }

    // check for type selected or not
    if ($('.type_checkbox:checked').length == 0) {
        $("#typetext").attr("style", "color:black");
        $("#default").show();
        typecheck = false;
    }
    else
    {
    	$("#typetext").attr("style", "color:#ba171b");
        $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
        typecheck = true;
    }

    if ($('.variable_checkbox:checked').length == 0) {
        $("#variabletext").attr("style", "color:black");
        variablecheck =  false;
    }
    else
    {
    	$("#variabletext").attr("style", "color:#ba171b");
        $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
         variablecheck = true;

    }
    // check for Industry Level selected or not
    if ($('.industryLevel_checkbox:checked').length > 0) {
        $("#typetext").attr("style", "color:#ba171b");
        $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
        typecheck = true;

        $("#variabletext").attr("style", "color:#ba171b");
        $("#applyChangesInitial").removeAttr('disabled').removeClass('btn-disabled');
        variablecheck = true;
    }

    if( variablecheck == true)
    {
    	$("#apply_chnages").removeAttr('disabled').removeClass('btn-disabled');
    }
    else
    {
    	$("#apply_chnages").attr('disabled','disabled').addClass('btn-disabled');
    }

    if(flag == true)
    {
        $("#default").hide();
        $("#tabledata").attr('disabled','disabled').addClass('btn-disabled');
        $("#loadchart").attr('disabled','disabled').addClass('btn-disabled');
        $("#gisdata").attr('disabled','disabled').addClass('btn-disabled');
        $("#matrix").attr('disabled','disabled').addClass('btn-disabled');
        $("#pmdata").attr('disabled','disabled').addClass('btn-disabled');
        $("#default1").show();
    }
    else
    {
        $("#default").show();
    }
}

function Bubblechartload() {
    var chart = bubbleChart().width(760).height(500).unitName("").title(venn_title);
    d3.select('#venn').datum(venn_sets).call(chart);

    d3.selectAll("body .venn-circle text") .style("font-size", "24px") .style("font-style", "italic");
}

$(document).ready(function()
{
    $("#apply_chnages_val").val(0);

    $("input[type='checkbox']").change(function () {
        $(this).siblings('ul')
            .find("input[type='checkbox']")
            .prop('checked', this.checked);
    });

    //START for Tab validation
    $('#district_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        JQuery('#state_tab :selected').each(function(i, selected) {
            myurl +=JQuery(selected).val()+",";
            zoom=5;
        });
    });

    $('#industry_level_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        if($('.district_checkbox:checked').length == 0){
            alert(districtalert);
            return false;
        }
    });

    $('#type_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        if($('.district_checkbox:checked').length == 0){
            alert(districtalert);
            return false;
        }
    });

    $('#variable_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        if($('.district_checkbox:checked').length == 0){
            alert(districtalert);
            return false;
        }

        if($('.type_checkbox:checked').length == 0){
            alert(typealert);
            return false;
        }
    });

    $('#mpi_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        if($('.district_checkbox:checked').length == 0){
            alert(districtalert);
            return false;
        }

        if($('.type_checkbox:checked').length == 0){
            alert(typealert);
            return false;
        }

    });

    $('#swcs_tab').click(function(e){
        if($('.state_checkbox:checked').length == 0){
            alert(statealert);
            return false;
        }

        if($('.district_checkbox:checked').length == 0){
            alert(districtalert);
            return false;
        }

        if($('.type_checkbox:checked').length == 0){
            alert(typealert);
            return false;
        }
    });
    //END for Tab validation

    if(typeof(usedlevel) != "undefined"){
        var ulevel = usedlevel.split(",");
        if(ulevel.length == 3){
            JQuery("#themeconent").css({"display":"none"});
        }
        getColorPallet_v2();
    }

    var graphdist = $("#district").val();
    if (typeof(preselected) != "undefined") {
        displaycombo_v2();
    }

    var minval        = "";
    var maxval        = "";
    var colorselected = "";
    var edittheme     = 0;
    var newDataset    = new Object();

    $(".hideothers").css({
        "display": "none"
    });

    if (typeof(havingthematicquery) != "undefined") {
        var singlequery = havingthematicquery.split(",");
        $("#thematic_attribute option").each(function() {
            for (var i = 0; i <= singlequery.length; i++) {
                if (singlequery[i] == $(this).val()) {
                    $(this).attr("disabled", true);
                }
            }
        });
    }

    $('#variablespan').on('change', '.variable_checkbox', function() {
        checkvalidation();
    });

    //Check all For District,State and Variables
    $(document).on('click','.allcheck',function(event) {
        var id    = $(this).attr('id');
        var input = id.split('_');
        $("." + input[0] + "_checkbox").prop("checked", $(this).prop("checked"));

        if (input[0] == "state") { ///For State
            var allVals = [];
            var slvals  = [];
            $('.state_checkbox:checked').each(function() {
                slvals.push($(this).val())
            })
            selected = slvals.join(',');
            if (selected) {
                $("#statetext").attr('style','color:"#ba171b"');
            } else {
                $("#statetext").attr('style','color: "black"');
            }
            getfrontdistrict_v2(selected);
            checkvalidation();
        } else if (input[0] == "district" || input[0]=='districtlabel') { //For District

            if(input[0]=='districtlabel')
            {
                //For Checkbox of each district inside district tab
                var statecode = input[1].split('-');
                $("." + input[0] + "_checkbox-"+ statecode[1]).prop("checked", $(this).prop("checked"));
            }

            var allVals = [];
            var slvals  = [];
            $('.district_checkbox:checked').each(function() {
                slvals.push($(this).val())
            });

            selected = slvals.join(',');
             if (selected) {
                $("#districttext").attr('style','color:"#ba171b"');
            } else {
                $("#districttext").attr('style','color: "black"');
            }

            getdistrictattr_v2(selected);
            checkvalidation();
        } else if (input[0] == "variable" || input[0]=='variablelabel') { // For Variables

            if(input[0]=='variablelabel')
            {
                //For Checkbox of each district inside district tab
                var statecode = input[1].split('-');
                console.log( input[0]);
                console.log(statecode)

                $("." + input[0] + "_checkbox-"+ statecode[1]).prop("checked", $(this).prop("checked"));
            }

            var allVals = [];
            var slvals  = [];
            $('.variable_checkbox:checked').each(function() {
                slvals.push($(this).val())
            });
            selected = slvals.join(',');
            if(selected) {
                $("#variabletext").attr('style','color:"#ba171b"');
            } else {
                $("#variabletext").attr('style','color: "black"');
            }
            checkvalidation();
        }
        else if(input[0] == "metervariables")
        {
            var allVals = [];
            var slvals  = [];

            $('.metervariables_checkbox:checked').each(function() {
                slvals.push($(this).val())
            })
            selected = slvals.join(',');
        }
         else if(input[0] =="meterdistrict")
        {
        var allVals = [];
        var slvals  = [];

        $('.meterdistrict_checkbox:checked').each(function() {
            slvals.push($(this).val())
        })
        selected = slvals.join(',');
        }
    });

    $("#matrix").click(function() {
        var data = $('#micaform').serialize();
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.getMaxForMatrix",
            method: 'POST',
            async: true,
            data: data,
            beforeSend: function() {
                $('#main_loader').show();
            },
            success: function(data) {
                $("#apply_chnages_val").val(0);
                $('#main_loader').hide();
                $("#quartiles").show();
                $("#result_table").hide();
                $("#graph").hide();
                $("#potentiometer").hide();
                $("#gis").hide();
                $("#fullscreentable").html(data);
            }
        });
    });

    $("#pmdata").click(function() {
        $("#potentiometer").show();
        $("#result_table").hide();
        $("#graph").hide();
        $("#quartiles").hide();
        $("#gis").hide();
    });

    $(document).on("click", "#createworkspace" , function() {
        var workspacename = $("#new_w_txt").val();
        if (workspacename == "") {
            alert("Please Enter workspace name");
            return false;
        }

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.saveWorkspace&name=" + workspacename,
            method: 'GET',
            success: function(data) {
                $("#workspacceedit").load(location.href + " #workspacceedit>*", "");
                $("#activeworkspacename").text(workspacename);
                  $("#workspacceedit").hide();
                  $("#fade").hide();
            }
        });
    });

    $(document).on("click", "#deleteworkspace" , function() {

        if (!confirm("Are you Sure to Delete Workspace?")) {
            return false;
        }

        var workspaceid = $("#profile").val();
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteWorkspace&workspaceid=" + workspaceid,
            method: 'GET',
            success: function(data) {
                alert("Workspace deleted successfully.");
                $("#workspacceedit").load(location.href + " #workspacceedit>*", "");
                $("#activeworkspacename").text(workspacename);
                $("#workspacceedit").hide();
                $("#fade").hide();
            }
        });

    });

    $(document).on("click","#updateworkspace", function(){

        var workspacename = $("#new_w_txt").val();
        var workspaceid   = $("#profile").val();

        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid,
            method  : 'GET',
            success : function(data){
                $("#workspacceedit").load(location.href + " #workspacceedit>*", "");
                $("#activeworkspacename").text(workspacename);
                $("#workspacceedit").hide();
                $("#fade").hide();
            }
        });
    });

    $(document).on("click","#filterChartButton", function(){
        var checkedValue = [];
        var inputElements = document.getElementsByClassName('chartstates_checkbox');
        for(var i=0; inputElements[i]; ++i){
            if(inputElements[i].checked){
                checkedValue[i] = inputElements[i].value;
            }
        }
        checkedValue = checkedValue.filter(function(v){return v!==''});
        var checkedDistValue = checkedValue.join(',');

        // Graph filter district
        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.getSelectedDistListForGraph&checkedDistValue="+checkedDistValue,
            method  : 'GET',
            success : function(data){
                $('#cblist').html(data);
            }
        });

        var checkedVarValue = [];
        var inputElements = document.getElementsByClassName('chartvariables_checkbox');
        for(var i=0; inputElements[i]; ++i){
              if(inputElements[i].checked){
                   checkedVarValue[i] = inputElements[i].value;
              }
        }

        checkedVarValue     = checkedVarValue.filter(function(v){return v!==''});
        var checkedVarValue = checkedVarValue.join(',');

        // Graph filter variables
        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.getSelectedVarListForGraph&checkedVarValue="+checkedVarValue,
            method  : 'GET',
            success : function(data){
                $('#variabelist').html(data);
            }
        });
    });

    // Graph Apply Changes Filter
    $(document).on("click","#graph_apply_changes", function(){
        var districtVals                       = [];
        var chartvariablesVals                 = [];
        var chartSecondLevelchartvariablesVals = [];
        var chartSecondLevelchartDistrictsVals = [];
        var checkedelements                    = [];

        $('.chartstates_checkbox:checked').each(function() {
            districtVals.push($(this).val());
            checkedelements.push($(this).val());
        });
        districtSelected = districtVals.join(',');

        $('.chartvariables_checkbox:checked').each(function() {
            chartvariablesVals.push($(this).val());
            checkedelements.push($(this).val());
        });
        chartvariablesValsSelected = chartvariablesVals.join(',');

        $('.secondLevelchartvariables_checkbox:checked').each(function() {
            chartSecondLevelchartvariablesVals.push($(this).val())
        });
        chartSecondLevelchartvariablesValsSelected = chartSecondLevelchartvariablesVals.join(',');

        $('.secondLevelchartdistrict_checkbox:checked').each(function() {
            chartSecondLevelchartDistrictsVals.push($(this).val())
        });
        chartSecondLevelchartDistrictValsSelected = chartSecondLevelchartDistrictsVals.join(',');

        chart1.destroy();

        if (checkedelements.length > 10) {
            alert("Variable + District Total Should be less then 10");
            return false;
        }

        loadCharts(null, districtSelected, chartvariablesValsSelected, 0, chartSecondLevelchartvariablesValsSelected, chartSecondLevelchartDistrictValsSelected);

        $('#graphfunction').hide();
        $('#fade').hide();
    });

    $(document).on("click", ".filterspeed", function() {
        //var selection     = $(this).val();
        var selection     = $(this).data('val');
        filterspeed_radio = selection;

        if (selection == "0") {
            //Layer
            $('#meterdistrict_allcheck').hide();
            $('#metervariables_allcheck').show();
            $('.meterdistrict_checkbox').data('multiple', 1);
            $('.metervariables_checkbox').data('multiple', 0);
            $('.metervariables_checkbox').removeAttr('checked');
            $('.meterdistrict_checkbox').removeAttr('checked');
             $('.metervariables_checkbox').prop('checked', false);
            $('.meterdistrict_checkbox').prop('checked', false);

            $('.meterdistrict_checkbox').each(function() {
                if ($.inArray($(this).val(), district_checked_layer) != -1){
                    $(this).attr('checked', true);
                }
            });

            $('.metervariables_checkbox').each(function() {
                if ($.inArray($(this).val(), variable_checked_layer) != -1){
                    $(this).attr('checked', true);
                }
            });

            var district_checked_layerDispl = "";
            if (district_checked_layer.length > 0) {
                district_checked_layerDispl = district_checked_layer[0].replace("%20", " ");
            }

            $('.filterbylabel').html('<b>Filter By Layer: </b>'+district_checked_layerDispl);

        } else {
            //Variable
            $('#metervariables_allcheck').hide();
            $('#meterdistrict_allcheck').show();
            $('.filterbylabel').html('<b>Filter By Variable</b>');
            $('.meterdistrict_checkbox').data('multiple', 0);
            $('.metervariables_checkbox').data('multiple', 1);
            $('.metervariables_checkbox').removeAttr('checked');
            $('.meterdistrict_checkbox').removeAttr('checked');
             $('.metervariables_checkbox').prop('checked', false);
            $('.meterdistrict_checkbox').prop('checked', false);

            $('.meterdistrict_checkbox').each(function() {
                if (jQuery.inArray($(this).val(), district_checked_district) != -1){
                    $(this).attr('checked', true);
                }
            });

            $('.metervariables_checkbox').each(function() {
                if (jQuery.inArray($(this).val(), variable_checked_district) != -1){
                    $(this).attr('checked', true);
                }
            });
        }

        $(".speed").css({
            "display": "block"
        });
        showhidePopup('variablefiltetr');
    });

    $(document).on("click",'.closeme1',function(){
        showhidePopup($(this).data('me'),'hide');
        // onclick="document.getElementById('variablefiltetr').style.display='none';document.getElementById('fade').style.display='none';"
    });

    function showhidePopup(popupid, action = 'show')
    {
        if(action =="show")
        {
            $('#'+popupid).show();
            $('#fade').show();
        }
        else
        {
            $('#'+popupid).hide();
            $('#fade').hide();
        }

    }

    ///this is not needed now because we have converted radio into buttons
    $(document).on('click', '#showspeed', function(){

        if($('.metervariables_checkbox:checked').length == 0){
            alert(metervariabel);
            return false;
        }

        if($('.meterdistrict_checkbox:checked').length == 0){
            alert(meterdistrict);
            return false;
        }

        showhidePopup('variablefiltetr','hide');

        var speedvar    = new Array();
        var speedregion = new Array();

        $(".metervariables_checkbox:checked").each(function() {
            speedvar.push($(this).val());
        });

        $(".meterdistrict_checkbox:checked").each(function() {
            speedregion.push($(this).val());
        });
        if($('.meterdistrict_checkbox').data('multiple')==1)
        {
            //district
            //  $('.metervariables_checkbox').data('multiple',0);
            variable_checked_layer = speedvar;
            district_checked_layer = speedregion;
            $('.filterbylabel').html('<b>Filter By Layer: </b><div class="poptitle">'+speedregion[0].replace("%20", " ")+'</div>');
        }
        else
        {
            //layer
            variable_checked_district = speedvar;
            district_checked_district = speedregion;
            $('.filterbylabel').html('<b>Filter By variabel: </b><div class="poptitle">'+speedregion[0].replace("%20", " ")+'</div>');
        }

        var selectedvar,selectedregion ;
        selectedvar      = speedvar.join(',') ;
        selectedregion   = speedregion.join(',') ;
        var variabelname = selectedvar.replace(" ","_");

        JQuery.ajax({
             url: "index.php?option=com_mica&task=showresults.getsmeter&region=" + selectedregion + "&speedvar=" + variabelname + "&filterspeed=" + filterspeed_radio,
            type    : 'GET',
            beforeSend: function() {
                $('#main_loader').show();
            },
            success: function(data) {
                $('#main_loader').hide();
                $("#spedometer_region").html("");
                $("#spedometer_region").html(data);
                //showhidePopup('variablefiltetr','hide');
                initspeed();
                $("#spedometer_region").css({
                    "width": "915px"
                });
                $("#spedometer_region").css({
                    //"overflow": "auto"
                });
            }
        });
    });

    $(document).on("click", "#speedfiltershow" , function() {
        $("#speedfiltershow").css({
            "display": "none"
        });
        $(".sfilter").css({
            "display": "block"
        });
        $("#spedometer_region").css({
            "width": "681px"
        });
        $("#spedometer_region").css({
            "overflow": "auto"
        });
    });

    $('.leftcontainer').on("change", '.list1 input[type=checkbox]', function() {
        var classname = $(this).attr('class');

        //classname = classname.split(" ");
        if (classname.indexOf("_checkbox") != -1) {

            end = classname.indexOf("_checkbox");
            start = classname.indexOf(" ") != -1 ? classname.indexOf(" ") : 0;
            result = classname.substring(start, end).trim();
         }
        if ($(this).prop('checked') == false)
        {
            $('#' + result + '_allcheck').prop('checked', false); //works with mpi_allcheck, swcs_checkbox, swcs_allcheck

        }
        else
        {
            if ($('.' + result + 'list input[type=checkbox]:checked').length == $('.' + result + 'list input[type=checkbox]').length)
            {
                $('#' + result + '_allcheck').prop('checked', true);
            }
            else
            {
                $('#' + result + '_allcheck').prop('checked', false);
            }
        }
    });
    //For check all function to work well with all other checkboxes:- END

    //for search START
    $('.searchtop').keyup(function(event) {
        var input, filter, a, id;
        id = $(this).attr('id');
        input = id.split('_');
        filter = this.value.toUpperCase();

        $("." + input[0] + "list li").each(function() {
            a = $('label', this).text();
            if (a.toUpperCase().indexOf(filter) > -1)
            {
                $(this).show();
            }
            else
            {
                $(this).hide();
            }
        });
    });

    //checkbox checked apply chnges button hide show
    $('#district_tab').click(function(e) {
        $('#state_tab :selected').each(function(i, selected) {
            myurl += $(selected).val() + ",";
            zoom = 5;
        });
    });

    $('.state_checkbox').click(function(e) {
        var slvals = [];
        $('.state_checkbox:checked').each(function() {
            slvals.push($(this).val())
        })
        selected = slvals.join(',');
        getfrontdistrict_v2(selected);
        checkvalidation();
    });

    $('#leftcontainer').on("click",'.district_checkbox',function(e) {
        var slvals = [];
        $('.district_checkbox:checked').each(function() {
            slvals.push($(this).val())
        })
        selected = slvals.join(',');
        getAttribute_v2(selected);
        checkvalidation();
    });

    $('#leftcontainer').on("click",'.type_checkbox, .industryLevel_checkbox, .mpi_checkbox, .swcs_checkbox',function(e) {
        checkvalidation();
    });

    ///////Chart functionalities
   /* $('#graph').on('click', '.chartvariables_checkbox', function()
    {
        var variabelname  = $(this).val();
        //var indexoflabels = $.inArray(variabelname, chart1.data.labels);
        var indexoflabels = $.inArray(variabelname, originalLabels);



        if (indexoflabels != -1) {
            if (document.getElementById('chartvariables_' + variabelname).checked)
            {
            }
            else
            {
                console.log("I reached here",)
                //var templabel = chart1.data.labels.splice(indexoflabels, 1);
                var templabel = originalLabels.splice(indexoflabels, 1);

                trashedLabels.push(templabel[0]);
                //console.log(trashedLabels);

                chart1.data.datasets.forEach(function(val, index)
                {
                    var citycode = val.id;
                    var tempdata = val.data.splice(indexoflabels, 1);

                    if (trashedData[citycode] == undefined)
                        trashedData[citycode] = [];
                    trashedData[citycode][templabel[0]] = tempdata[0];
                });
            }
        }
        else
        {
             console.log("I reached here trasjed");

            var indexoflabels_t = $.inArray(variabelname, trashedLabels);
            console.log(indexoflabels_t);
            if (indexoflabels_t != -1)
            {
                data=variabelname.replace(/(\s[^\s]*)\s/g,"$1--");
                rres = data.split("--");
                chart1.data.labels.push(rres);

                chart1.data.datasets.forEach(function(val, index) {
                    tempdata = trashedData[val.id][variabelname];
                    val.data.push(tempdata);
                });
            }
        }
        chart1.update();
    });*/

   /* $('#graph').on('click', '.chartstates_checkbox', function() {
        var state_id = $(this).val();
        chart1.data.datasets.forEach(function(val, index) {
            if (val.id == state_id) {
                if (document.getElementById('chartstates_' + state_id).checked) {
                    if (val._meta[0] != undefined) {
                        val._meta[0].hidden = false;
                        return;
                    }
                } else {
                    if (val._meta[0] != undefined) {
                        val._meta[0].hidden = true;
                        return;
                    }
                }
            }
            chart1.update();
        })
    });*/

    // Added Second Level graph Variable
    $('#secondLevelGraphvariable').on('click', function() {
        var newhtml   = '';
        newhtml       = '<ul id ="secondLevelVariablesForGraph" class="list1">';

        var checkedValue = [];
        var inputElements = document.getElementsByClassName('secondLevelchartvariables_checkbox');
        for(var i=0; inputElements[i]; ++i){
            if(inputElements[i].checked){
                checkedValue[i] = inputElements[i].value;
            }
        }

        $.each($(".chartvariables_checkbox:checked"), function(){
            // For checked checkbox
            checked = "";
            if(jQuery.inArray($(this).val(), checkedValue) !== -1){
                checked = "checked";
            }else{
                checked = "";
            }

            var secondLevelchartvariableName = $(this).val();
            secondLevelchartvariableName = secondLevelchartvariableName.replace(/_/g," ");

            newhtml += "<li>"+
            '<input type = "checkbox" class="secondLevelchartvariables_checkbox" '+
                        ' id="secondLevelchartvariables_' + $(this).val() +'" value="'+ $(this).val()+ '" '+checked+'/>'+
            '<label for="secondLevelchartvariables_' + $(this).val() + '">'+ secondLevelchartvariableName +"</label>"+
             "</li>";
        });

        newhtml +='</ul>';

        $('#secondLevelVariabelist').html(newhtml);
    });

    // Added Second Level graph District
    $('#secondLevelGraphDistrict').on('click', function() {
        var newhtml   = '';
        newhtml       = '<ul id ="secondLevelDistrictsForGraph" class="list1">';

        var checkedValue = [];
        var inputElements = document.getElementsByClassName('secondLevelchartdistrict_checkbox');
        for(var i=0; inputElements[i]; ++i){
            if(inputElements[i].checked){
                checkedValue[i] = inputElements[i].value;
            }
        }

        $.each($(".chartstates_checkbox:checked"), function(){
            // For checked checkbox
            checked = "";
            if(jQuery.inArray($(this).attr("data-districtName"), checkedValue) !== -1){
                checked = "checked";
            }else{
                checked = "";
            }

            //var secondLevelchartDistrictName = $(this).val();
            //secondLevelchartDistrictName     = secondLevelchartDistrictName.replace(/_/g," ");

            newhtml += "<li>"+
            '<input type = "checkbox" class="secondLevelchartdistrict_checkbox" '+
                        ' id="secondLevelchartdistrict' + $(this).attr("data-districtName") +'" value="'+ $(this).attr("data-districtName")+ '" '+checked+'/>'+
            '<label for="secondLevelchartdistrict' + $(this).attr("data-districtName") + '">'+ $(this).attr("data-districtName") +"</label>"+
             "</li>";
        });

        newhtml +='</ul>';

        $('#secondLevelDistrictlist').html(newhtml);
    });

    $('#graph').on('click', '.secondLevelchartvariables_checkbox', function(){
        var variabelnameOrg  = $(this).val();

        // Replace space to underscore in string
        variabelname = variabelnameOrg.replace(/_/g, ' ');

        var indexoflabels = $.inArray(variabelname, chart1.data.labels);

        if (indexoflabels !== -1) {
            if (document.getElementById('secondLevelchartvariables_' + variabelnameOrg).checked) {
                chart1.data.datasets[indexoflabels]['_meta'][0].yAxisID = "y-axis-2";
                //chart1.data.datasets[indexoflabels]['_meta'][0]['data'][indexoflabels]['_yScale'].id = "y-axis-2";
            }
            else{
                chart1.data.datasets[indexoflabels]['_meta'][0].yAxisID = "y-axis-1";
            }
        }
        chart1.update();
    });

    $(document).on("change",'#chartype',function(){
        $('#secondLevelGraphvariable').css({
            'display': 'none'
        });
        if (chart1){
            chart1.destroy();
        }
        type = $(this).val();
        if(type!="bubble" )
        {
            if ($('#chartype option:selected').text() == "Second Level Chart"){
                $('#secondLevelGraphvariable').css({
                    'display': 'block'
                });
            }

            $('#myChart').show();
            $('#venn').hide();
            $('#venn_label').hide();
            var ctx = document.getElementById("myChart").getContext("2d");

            // Distroyed chart Object
            var temp  = jQuery.extend(true, {}, chartconfig);
            temp.type = type;
            chart1   = new Chart(ctx, temp);

            ///change the checkbox selection of variable popup
            $('.chartvariables_checkbox').prop("checked",false);
            $.each(chart_selected_vars,function(key,val){
                $('#chartvariables_'+ val.replace(/ /g,"_")).prop("checked",true);
            });
        }
        else if(type == "bubble")
        {
            //display bubble chart
            $('#myChart').hide();
            $('#venn').show();
            $('#venn_label').show();

            venn_sets=[];
            $('.chartvariables_checkbox').prop("checked",false);
            $.each(venn_selected_vars,function(k,val){
                JQuery.each(venn_sets_main,function(key,value){
                    JQuery.each(value,function(key1,value1){

                        if(val.replace(/ /g,"_")==key.replace(/ /g,"_"))
                        {
                            distrcitname = key1.split("-");
                            distrcitname = distrcitname[0];
                            if(venn_sets.length < 5)
                            {
                                venn_sets.push ({'title':distrcitname.trim(), 'views':parseFloat(value1)});
                                venn_title = key;
                            }
                        }
                    });
                });
                $('#chartvariables_'+ val.replace(/ /g,"_")).prop("checked",true);
            });

            // var chart = venn.VennDiagram();
            // d3.select("#venn").datum(venn_sets).call(chart);
            if(venn_title!=""){
                Bubblechartload();
            }
        }
    });

    $(document).on('change','.chartvariables_checkbox',function(){
        if($('#chartype').val() == 'bubble'){
            $('.chartvariables_checkbox').prop("checked",false);
            $(this).prop("checked",true);
        }
    });

    $(document).on('keyup','#no_of_interval',function(e){
        var str = "";
        if (e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105) {
            if (typeof(edittheme) == "undefined") {
                edittheme = 0;
            }
            createTable_v2(edittheme);
        }
    });

    $("li #menu100").parent().attr("class", "active");

    $(".contenttoggle").css({
        "display": "none"
    });

    $("#result_table").on(".toggle", "click", function() {
        var myclass = $(this).attr("class");
        $(".toggle").css({
            "border-right": "1px solid #0154A1"
        });

        $(this).prev("span").css({
            "border-right": "1px solid white"
        });

        var myclasssplit = myclass.split(" ");
        if (typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active")
        {
            $(".toggle").removeClass("active");
            $(".contenttoggle").slideUp();
            return false;
        }

        var myid = $(this).attr("id");
        $(".toggle").removeClass("active");
        $(this).addClass("active");
        $(".contenttoggle").slideUp();
        $("." + myid).slideDown();
    });

    $("#result_table").on(".toggle1", "click", function() {
        $(".toggle1").css({
            "border-right": "1px solid #0154A1"
        });

        $(this).prev("span").css({
            "border-right": "none"
        });

        $(".toggle1").last("span").css({
            "border-right": "white"
        })

        var myclass      = $(this).attr("class");
        var myclasssplit = myclass.split(" ");

        if (typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active") {
            $(".toggle1").removeClass("active");
            $(".contenttoggle1").slideUp();
            $(".toggle1").css({
                "border-right": "1px solid #0154A1"
            });
            $(".toggle1").last("span").css({
                "border-right": "white"
            })
            return false;
        }

        var myid = $(this).attr("id");
        $(".toggle1").removeClass("active");
        $(this).addClass("active");
        $(this).css({
            "border-right": "1px solid #0154A1"
        });
        $(".contenttoggle1").slideUp();
        $("." + myid).slideDown();
        $.cookie("tab", myid);

        if (myid == "text") {
            $('#jsscrollss').not(".firsttableheader,.secondtableheader,.thirdtableheader").jScrollPane({
                verticalArrowPositions: 'os',
                horizontalArrowPositions: 'os'
            });
        }
    });

    $("#result_table").on("click",".toggle2",  function() {
        var myclass      = $(this).attr("class");
        var myclasssplit = myclass.split(" ");

        if (typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active") {
            $(".toggle2").removeClass("active");
            $(".contenttoggle2").css({
                "display": "none"
            });
            return false;
        }
        var myid = $(this).attr("id");

        $(".toggle2").removeClass("active");
        $(this).addClass("active");
        $(".contenttoggle2").css({
            "display": "none"
        });
        $("." + myid).css({
            "display": "block"
        });
    });

    if ($('.customvariablecheckbox').prop('checked') == true){
        $("#applyChangesInitial").prop('btn-disabled', false);
        $("#applyChangesInitial").removeAttr('disabled');
    }

    $(".full-data-view").on("#submit", "click", function() {
        if ($("#comparedata").val() == 1)
        {
            var statecompare = $("#statecompare").val();
            var districtcompare = $("#districtcompare").val();
            var towncompare = $("#towncompare").val();
            var urbancompare = $("#urbancompare").val();

            if (urbancompare != "") {
                if (urbancompare == "all") {
                    alert("All UA already Selected");
                    return false;
                }
                var urban = $("#urban").val();
                if (urban == "") {
                    alert("Please Select UA to compare");
                    return false;
                } else if (urban == "all") {
                    alert("You can not select All UA to compare");
                    return false;
                }
            }
            else if (towncompare != "")
            {
                if (towncompare == "all")
                {
                    alert("All Towns already Selected");
                    return false;
                }

                var town = $("#town").val();
                if (town == "")
                {
                    alert("Please Select Town to compare");
                    return false;
                }
                else if (town == "all")
                {
                    alert("You can not select All Town to compare");
                    return false;
                }
            }
            else if (districtcompare != "")
            {
                if (districtcompare == "all") {
                    alert("All Districts already Selected");
                    return false;
                }
                var dist = $("#district").val();
                if (dist == "")
                {
                    alert("Please Select District to compare");
                    return false;
                }
                else if (dist == "all")
                {
                    alert("You can not select All District to compare");
                    return false;
                }
            }
            else if (statecompare != "")
            {
                if (statecompare == "all")
                {
                    alert("All States already Selected");
                    return false;
                }

                var stateval = $("#state").val();
                if (stateval == "")
                {
                    alert("Please Select State to compare");
                    return false;
                }
                else if (stateval == "all")
                {
                    alert("You can not select All State to compare");
                    return false;
                }
            }
            return true;
        }
        else
        {
            var resetmtype      = 0;
            var checked         = 0;
            var dataof          = $("#dataof").val();
            var districtcompare = $("#district").val();

            if (dataof == "District") {
                if ($("#district").val() == "") {
                    alert("Please Select District First");
                    return false;
                }
            } else if (dataof == "UA") {
                if ($("#urban").val() == "") {
                    alert("Please Select UA First");
                    return false;
                }
            } else if (dataof == "Town") {
                if ($("#Town").val() == "") {
                    alert("Please Select Town First");
                    return false;
                }
            }

            $("input:checkbox:checked").each(function() {
                var chk = $(this).attr("checked");
                if (chk == "checked") {
                    checked = 1;
                }
            });

            if (checked == 0) {
                alert("Please select variable first");
                return false;
            } else {
                if (typeof(districtcompare) == "undefined" || districtcompare == "") {
                    return true;
                }
            }
        }
    });

    $('body').on('click', '.simpleColorChooser', function() {
        var value = $('.simple_color').val();
        value     = value.replace("#", "");
        var steps = $("#no_of_interval").val();

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.getColorGradiant&value=" + encodeURIComponent(value) + "&steps=" + parseInt(steps++),
            method: 'GET',
            success: function(data) {
                colorselected = data;
                var segment   = data.split(",");
                segment       = segment.reverse();

                for (var i = 1; i <= segment.length; i++) {
                        var ncol = "#"+JQuery.trim(segment[i]);
                        JQuery("#color"+i).fadeIn(1000,JQuery("#color"+i).css({"background-color":ncol}));
                        /*$("#color" + (i)).fadeIn(1000, $("#color" + (i)).css({
                            "background-color": "#" + segment[i]
                        }));*/
                }
            }
        });
    });

    $('body').on('click', 'a.deletegrp', function() {
        var level    = $(this).attr("class");
        var getlevel = level.split(" ");
        var getdelid = $(this).attr("id");
        var segment  = getdelid.split("del_");

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deletethematicQueryToSession&formula=" + encodeURIComponent(segment[1]) + "&level=" + getlevel[1],
            method: 'GET',
            success: function(data) {
                loadGis('thematic');
                document.getElementById('light_thematic').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            }
        });
    });

    $('body').on('click', 'a.edittheme', function() {
        document.getElementById('light_thematic').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        edittheme = 1;
        var cnt   = $("#themeconent").html();

        $("#themeconent").html("");
        $(".themeconent").html(cnt);
        $('.simpleColorContainer').remove();

        getColorPallet_v2();

        var classname = $(this).attr("class");
        var myid      = this.id;
        var getcount  = classname.split(" ");
        var seg       = myid.split("__");

        $("#thematic_attribute").val(seg[0]);
        $("#level").val(seg[1]);

        $("#thematic_attribute").change();
        $("#no_of_interval").val(getcount[1]);
        createTable_v2(edittheme);
        $("#no_of_interval").val(getcount[1]);
        $(".range_" + seg[1]).each(function(i) {
            i++;
            rangeid = ($(this).attr("id"));
            var mylimit = rangeid.split("-");
            $("#from" + i).val(mylimit[0]);
            $("#to" + i).val(mylimit[1]);
        });

        var endcolor = "";
        if (seg[1] == "0") {
            for (var i = 1; i <= getcount[1]; i++) {
                $("#color" + (i)).css({
                    "background-color": "" + $(".col_" + i).css("background-color")
                });
                if (getcount[1] == i) {
                    $(".simpleColorDisplay").css({
                        "background-color": $(".col_" + i).css("background-color")
                    });
                }
                //rangeid=$(".range"+i).attr("id");
            }
            //rangeid=$(".range"+i).attr("id");
        }
        //  $("#thematicquerypopup").click();
        thematicquerypopup_v2(edittheme);
    });

    $(document).on("click", "#updatecustom", function() {
        var new_name   = $('#new_name').val();
        var oldattrval = $('#oldattrval').val();

        if ($("#new_name").val() == "") {
            alert("Please Select Custom Variable First!!");
            return false;
        }

        if (!validateFormula()) {
            return false;
        }

        var attributevale = $('textarea#custom_attribute').text();
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.updateCustomAttr&attrname=" + new_name + "&attributevale=" + encodeURIComponent(attributevale) + "&oldattrval=" + encodeURIComponent(oldattrval),
            method: 'GET',
            success: function(data)
            {
                $('#customlist').removeClass('active');
                $('#custom_manage').addClass('active');
                $('#custom_edit').removeClass('active');
                $('#home').removeClass('in active');
                $('#menu1').addClass('in active');
                $('#menu2').removeClass('in active');
                $('a.getmanagedata').trigger("click");

                alert("Custom Variable Updated Suceessfully.");
                //$("#light").hide();
                //getDataNew();
                getAttribute_v2();
            }
        });
    });

    $(".full-data-view").on("#deletevariable", "click", function() {
        var new_name      = $('.customedit').find(":selected").text();
        var attributevale = $('textarea#custom_attribute').text();

        if (new_name == "" && attributevale == "")
        {
            alert("Please Select Custom Variable");
            return false;
        }

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteCustomAttr&attrname=" + new_name + "&attributevale=" + attributevale,
            method: 'GET',
            success: function(data) {
                window.location = 'index.php?option=com_mica&view=showresults&Itemid=108';
            }
        });
    });

    $(".full-data-view").on(".variablegrp", "click", function() {
        var myid = $(this).attr("id");
        var dis  = $('.' + myid).css("display");

        $(".hideothers").css({
            "display": "none"
        });

        $('.variablegrp').addClass("deactive");
        $(this).addClass("active");
        $(this).removeClass("deactive");

        if (dis == "block") {
            $('.' + myid).css("display", "none");
            $(this).addClass("deactive");
            $(this).removeClass("active");
        } else {
            $('.' + myid).css("display", "block");
            $(this).addClass("active");
            $(this).removeClass("deactive");
        }
    });

    $(".full-data-view").on(".hovertext", "mouseover", function(e) {
        var allclass  = $(this).attr("class");
        var segment   = allclass.replace("hovertext ", "");
        var x         = e.pageX - this.offsetLeft;
        var y         = e.pageY - this.offsetTop;
        var popuphtml = "<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>" + segment + "</div>";

        $(this).append(popuphtml);
    });

    $(".full-data-view").on(".hovertext", "click", function(e) {
        $(this).parent().prev().find("input").prop("checked", true); //,true);
    });

    $(".full-data-view").on(".hovertext", "mouseout", function(e) {
        $("#popupattr").remove();
    });

    $("#tabvariables").on(".customvariablecheckbox", "click", function() {
        var check = ($(this).attr("checked"));

        if (check)
        {
            $("#default1").show();
            document.getElementById("applyChangesInitial").style.pointerEvents = "auto";
            document.getElementById("applyChangesInitial").style.cursor = "pointer";
            $("#light").hide();
        }
        else {
            $.ajax({
                url: "index.php?option=com_mica&task=showresults.deleteattribute&attr=" + $(this).attr("id"),
                method: 'GET',
                success: function(data) {
                    getAttribute_v2(5);
                    $("#default1").show();
                    $("#light").hide();
                }
            });
        }
    });

    $(document).on("click", ".customedit", function(){
        $('#save').attr("id", "updatecustom");
        $('#updatecustom').attr("onclick", "javascript:void(0)");
        $('#updatecustom').attr("value", "Update");

        JQuery('#new_name').val(JQuery(this).attr("id"));
        $('#new_name').attr('disabled', true);
        $('.aa').html("Edit");

        oldattrval = $(this).attr("value");
        $('#oldattrval').val(oldattrval);
        $('textarea#custom_attribute').text($(this).attr("value"));
        lastchar = '';

        $('#menu1').removeClass('in active');
        $('#menu2').addClass('in active');
        $('#custom_manage').removeClass('active');
        $('#custom_edit').addClass('active');
        $('#custom_edit a').text('Edit');
        $('#menu2 .poptitle').text('Edit');
        $('#light').css('display','block');
        //$('#custom_manage a').removeAttr("href");
        //$('#customlist a').removeAttr("href");
        $('.custom_edit_button').removeClass('col-md-6').addClass('col-md-4');
        r = $('<input type="button" name="cancel" id="cancel" class="btn btn-primary btn-bottom custom_cancel_btn" value="Cancel" >');
        $(".custom_cancel").append(r);
    });

    $(document).on("click", ".custom_cancel_btn" , function(){
        $('#custom_manage a').attr("href", "#menu1");
        $('#customlist a').attr("href", "#home");
        $('#menu2').removeClass('in active');
        $('#menu1').addClass('in active');
        $('#custom_edit').removeClass('active');
        $('#custom_manage').addClass('active');
        $('#custom_edit a').text('Add New');
        $('#menu2 .poptitle').text('Add New');
        $('#light').css('display','block');
        $('.custom_edit_button').removeClass('col-md-4').addClass('col-md-6');

        // Remove cancel button
        $('#cancel').remove();
    });

    $(document).on("click", "#custom_edit" , function(){
        if($('#custom_edit a').text() == "Add New"){
            $('#updatecustom').attr("value", "Save");
            $('#updatecustom').attr("id", "save");
            $('#save').attr("onclick", "checkfilledvalue()");

            // Clear Previouse Value
            $("#new_name").val("");
            $("#custom_numeric").val("");
            $("#custom_attribute").val("");
            $('#new_name').attr('disabled', false);

            $('#cancel').remove();
        }

        if($('#custom_edit a').text() == "Edit"){
            oldattrval = $(this).attr("value");
            $('#oldattrval').val(oldattrval);
            $('textarea#custom_attribute').text($(this).attr("value"));
            lastchar = '';

            r = $('<input type="button" name="cancel" id="cancel" class="btn btn-primary btn-bottom custom_cancel_btn" value="Cancel" >');
            $(".custom_cancel").append(r);
        }
    });

    $(".full-data-view").on("#closeextra", "click", function() {
        $('#updatecustom').attr("id", "save");
        $('#save').attr("onclick", "checkfilledvalue();");
        $('#save').attr("value", "Save");
        $('#new_name').val("");
        $('#new_name').attr('disabled', false);
        $('.aa').html("Add New");
    });

    $(document).on("click", ".deletecustomvariable", function(){
        var names = new Array();
        var i     = 0;

        JQuery(".dcv:checked").each(function(i){
            names.push(JQuery(this).attr("id"));
        });

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.deleteCustomVariableFromLib&attrname=" + names,
            type: 'GET',
            success: function(data) {
                var response = JSON.parse(data);
                if (response.success == 1) {
                    getAttribute_v2(5);
                    //getDataNew();
                    $('#customlist').removeClass('active');
                    $('#custom_manage').addClass('active');
                    $('#custom_edit').removeClass('active');
                    $('#home').removeClass('in active');
                    $('#menu1').addClass('in active');
                    $('#menu2').removeClass('in active');
                    $('a.getmanagedata').trigger("click");
                    alert("Custom Variable Delete Suceessfully.");
                }
            }
        });
    });

    $(document).on("click", "#fullscreen, #fullscreen1", function() {
        var fromthematic = $("#fullscreen").attr("fromthematic");

        if (fromthematic == 1) {
            popped = open(siteurl + 'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
        } else {
            popped = open(siteurl + 'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component', 'MapWin');
        }

        popped.document.body.innerHTML = "<div id='map' style='height:" + $(window).height() + "px;width:" + $(window).width() + "px;'></div>";
    });

    $(document).on("click", ".fullscreeniconoff", function(){
        $("#fullscreentable").removeClass("fullscreentable");
        $("#matrixclose").remove();
        $(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");

        $(this).removeClass("fullscreeniconoff");
        $(this).addClass("fullscreenicon");
        $("#fade").css({
            "display": "none"
        });
        $("#tablescroll").removeAttr("width");
        $("#tablescroll").removeAttr("overflow");
        $("#tablescroll").removeAttr("height");
    });

    $(document).on("click", ".fullscreenicon", function(){
        $("#fullscreentable").addClass("fullscreentable");
        $(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'></td></tr></table>");
        $(this).addClass("fullscreeniconoff");
        $(this).removeClass("fullscreenicon");
        var toappend = '<div class="divclose" id="matrixclose" style="text-align:right;"><a href="javascript:void(0);" onclick="$(\'.fullscreeniconoff\').click();"><img src="' + siteurl + '/media/system/images/closebox.jpeg" alt="X"></a></div>';
        var html     = $("#fullscreentable").html();
        $("#fullscreentable").html(toappend + "<div id='tablescroll'>" + html + "</div>");
        $("#fadefade").css({
            "display": "block"
        });
        $("#tablescroll").css({
            "width": "100%"
        });
        $("#tablescroll").css({
            "height": "100%"
        });
        $("#tablescroll").css({
            "overflow": "auto"
        });
    });

    $(document).on("click", "#downmatrix", function(){
         window.location.href = "index.php?option=com_mica&task=showresults.downloadMatrix&rand=" + Math.floor(Math.random() * 100000);
    });

    $(".full-data-view").on("#speedometer", "click", function() {
        $.ajax({
            url: "index.php?option=com_mica&task=showresults.speedometer",
            method: 'POST',
            success: function(data) {
                $("#speed_region").html(data);
            }
        });
    });

    $(".full-data-view").on("#showchart", "click", function() {
        var checkedelements = new Array();
        var checkeddist     = new Array();
        var checkevar       = new Array();

        $("#selectdistrictgraph").find(".districtchecked").each(function() {
            if ($(this).attr("checked")) {
                checkedelements.push(this.val);
                checkeddist.push($(this).val());
            }
        });

        $("#light1007").find(".variablechecked").each(function() {
            if ($(this).attr("checked")) {
                checkedelements.push($(this).val());
                checkevar.push($(this).val());
            }
        });

        if (checkedelements.length > 15) {
            alert("Variable + District Total Should be less then 15");
            return false;
        }

        $.ajax({
            url: "index.php?option=com_mica&task=showresults.getGraph",
            type: 'POST',
            data: "dist=" + checkeddist + "&attr=" + checkevar,
            success: function(data) {
                var segments = data.split("<->");
                datastr      = segments[0];
                grpsetting   = segments[1];
                $("#chartype").change();
            }
        });
    });

    // Hide Popup via esc key
    $(document).keyup(function(e){
        if (e.keyCode == "27") {
            document.getElementById('light_thematic').style.display = 'none';
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }
    });

    var is_updatecustom = 0;
    $(".full-data-view").on(".deleterow", "click", function() {

        var value = $(this).val();
        var ans   = confirm("Are you Sure to delete Variable " + value);

        if (ans == 1) {
            $.ajax({
                url: "index.php?option=com_mica&task=showresults.deleteattribute&attr=" + value,
                type: 'GET',
                success: function(data) {
                    $("#" + value).fadeOut();
                    window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
                }
            });
        }
    });

    $(".full-data-view").on(".deletecustom", "click", function() {
        var value = $(this).val();
        var ans   = confirm("Are you Sure to delete Custom Variable " + value);
        if (ans == 1) {
            $.ajax({
                url: "index.php?option=com_mica&task=showresults.deleteCustomAttribute&attr=" + value,
                type: 'GET',
                success: function(data) {
                    $("#" + value).fadeOut();
                    window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
                }
            });
        }
    });

    $(".full-data-view").on(".removecolumn", "click", function() {
        var value   = $(this).val();
        var segment = value.split("`");
        var ans     = confirm("Are you Sure to delete  " + segment[2]);

        if (ans == 1) {
            $.ajax({
                url: "index.php?option=com_mica&task=showresults.deleteVariable&table=" + segment[0] + "&value=" + segment[1],
                type: 'GET',
                success: function(data) {
                    //$(this).fadeOut();
                    window.location = 'index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>';
                }
            });
        }
    });

    var compare = 1;
    $(".full-data-view").on("#district_tab", "click", function() {
        $(document).ready(function() {
            $('.slider').slick({
                dots: false,
                vertical: true,
                slidesToShow: 11,
                slidesToScroll: 1,
                verticalSwiping: true,
                arrrow: true,
            });
        });
    });

    ///Get Potentiometer Data
    $(document).on('click', '#pmdata', function(){
        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.getSelectedVarListForSpeedoMeter",
            method  : 'POST',
            success : function(htmldata){
                $('#potentiolMeter').html(htmldata);

                var selectedregion, selectedvar;
                selectedvar    = $('.metervariables_checkbox:first').val();
                selectedregion = $('.meterdistrict_checkbox:first').val();
                selectedregion = selectedregion.replace("%20", " ");

                $('.filterbylabel').html('<b>Filter By Layer: </b>'+selectedregion);
                var variabelname = selectedvar.replace(" ","_");

                JQuery.ajax({
                   url: "index.php?option=com_mica&task=showresults.getsmeter&region=" + selectedregion + "&speedvar=" + variabelname + "&filterspeed=" + filterspeed_radio,
                    type    : 'GET',
                    beforeSend: function() {
                            $('#main_loader').show();
                        },
                    success: function(data) {
                        $('#main_loader').hide();
                        $("#spedometer_region").html("");
                        $("#spedometer_region").html(data);
                        //showhidePopup('variablefiltetr','hide');
                        initspeed();
                        $("#spedometer_region").css({
                            "width": "915px"
                        });
                        $("#spedometer_region").css({
                            "overflow": "auto"
                        });
                     }
                });
            }
        });
    });

    $(document).on('click','.metervariables_checkbox, .meterdistrict_checkbox',function(event) {
        if($(this).data('multiple')==1)
        {
            var $box = $(this);
            if ($box.is(":checked"))
            {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            }
            else
            {
                $box.prop("checked", false);
            }
         }
    });

    // Custom Data get selected variable list
    $(document).on('click', '#custom_btn', function(){
        $('#customlist').addClass('active');
        $('#custom_manage').removeClass('active');
        $('#custom_edit').removeClass('active');
        $('#home').addClass('in active');
        $('#menu1').removeClass('in active');
        $('#menu2').removeClass('in active');
        JQuery.ajax({
            url     : "index.php?option=com_mica&task=showresults.getSelectedVarListForCustom",
            method  : 'POST',
            success : function(data){
                $('#avail_attribute_for_custom').html(data);
            }
        });
    });

    $(document).on('click', '#apply_chnages', function(){
        $("#apply_chnages_val").val(1);
        usedlevel = '';
        getDataNew();
        $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
        $("#loadchart").removeAttr('disabled').removeClass('btn-disabled');
        $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
        $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
        $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
    });

    $(document).on('click', '#applyChangesInitial', function(){
        $("#apply_chnages_val").val(1);
        usedlevel = '';
        if ($('.variable_checkbox:checked').length == 0 || $('.district_checkbox:checked').length == 0 || $('.state_checkbox:checked').length == 0 || $('.type_checkbox:checked').length == 0)
        {
            checkvalidation();
            $("#default1").hide();
            $("#default").show();
        }
        else
        {
            getDataNew();
            $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
            $("#graphdata").removeAttr('disabled').removeClass('btn-disabled');
            $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
            $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
            $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
        }
        $("#tabledata").removeAttr('disabled').removeClass('btn-disabled');
        $("#loadchart").removeAttr('disabled').removeClass('btn-disabled');
        $("#gisdata").removeAttr('disabled').removeClass('btn-disabled');
        $("#matrix").removeAttr('disabled').removeClass('btn-disabled');
        $("#pmdata").removeAttr('disabled').removeClass('btn-disabled');
    });


    $(document).on('click', '.sidebar-toggle-box', function(){
          $('.explore-data').toggleClass( "expand" );
    });

    $(document).on('click', '.left-section .nav-tabs a', function(){
          $('.explore-data').removeClass( "expand" );
    });

    var rightSecHeight = jQuery('.full-data-view').innerHeight() - '100';
    jQuery('#map').css('height', rightSecHeight)
    //console.log(rightSecHeight);

});




