<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Villagefront view class for Mica.
 *
 * @since  1.6
 */
class MicaViewVillagefront extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$app     = JFactory::getApplication();
		$session = JFactory::getSession();
		$reset   = $app->input->get('reset', 0, 'int');

		if($reset == 0){
			$session->set('oldattributes', $session->get('villagesattributes'));
		}else{
			$session->set('oldattributes', "");
			$session->set('villagesattributes', null);
			$session->set('villagestate', null);
			$session->set('villagedistrict', null);
			$session->set('urban', null);
			$session->set('villages_villages', null);
			$session->set('activeworkspace', null);
			$session->set('attrname', null);
			$session->set('customattribute', null);
			$session->set('villagem_type', null);
			$session->set('activetable', null);
			$session->set('activedata' ,null);
			$session->set('village_composite', null);
			$session->set('village_m_type_rating', null);
			$session->set('customformula', null);
			$session->set('Groupwisedata', null);
		}

		// Need to clear session varialbles of district view in village view
		$session->set('attributes', null);
		$session->set('state', null);
		$session->set('district', null);
		$session->set('m_type', null);
		$session->set('indlvlgrps', null);
		$session->set('composite', null);
		$session->set('m_type_rating', null);
		// End

		$app->input->set('village_composite', $session->get('village_composite'));
		$app->input->set('village_m_type_rating', $session->get('village_m_type_rating'));
		$app->input->set('villagesattributes', $session->get('villagesattributes'));
		$app->input->set('villagestate', $session->get('villagestate'));
		$app->input->set('villagem_type', $session->get('villagem_type'));
		$app->input->set('preselected', $session->get('villagedistrict'));
		$app->input->set('villages_villages', $session->get('villages_villages'));

		$msg = $app->input->get('msg', '', 'raw');

		if ($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}elseif ($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}elseif ($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items    = $this->get('StateItems');
		$this->composite_attr = $this->get('compositeAttr');
		$this->typesArray     = $this->get('AttributeType');

		return parent::display($tpl);
	}
}
