<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
/**
 * Micafront view class for Mica.
 *
 * @since  1.6
 */
class MicaViewMicafront extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{


		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}

		if(!defined('TOMCAT_STYLE_ABS_PATH'))
		{
			DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/micamimi18/");
			//DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles");
		}

		$app     = JFactory::getApplication();
		$session = JFactory::getSession();

		$reset = $app->input->get('reset', 0, 'int');
		if($reset == 0){
			$session->set('oldattributes', $session->get('attributes'));
		}else{
			$session->set('oldattributes', "");
			$session->set('attributes', null);
			$session->set('state', null);
			$session->set('district', null);
			$session->set('urban', null);
			$session->set('town', null);
			$session->set('activeworkspace', null);
			$session->set('attrname', null);
			$session->set('customattribute', null);
			$session->set('m_type', null);
			$session->set('activetable', null);
			$session->set('activedata' ,null);
			$session->set('composite', null);
			$session->set('m_type_rating', null);
			$session->set('customformula', null);
			$session->set('Groupwisedata', null);
		}

		//////.//////////// Workspace ///////////////////////
		if($session->get('activeworkspace') == ""){

			$result = workspaceHelper::loadWorkspace(1);
			//echo "<pre/>";print_r($result);exit;

	    	if(!empty($result)){
	    		// /echo "<pre/>";print_r($session->set('activeworkspace',$result[0]['id']));exit;
	    		$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				//die($app);

				cssHelper::flushOrphandSld($session->get('activeworkspace'));

	    	}else{

				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
				//$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188");
	    	}

		}else if($session->get('is_default') == 1){

	    	workspaceHelper::updateWorkspace($innercall);

	    }else{


    	    $app->input->set('activeworkspace', $session->get('activeworkspace'));
			//$this->saveSldToDataBase("1","1");
	    }




	    //////.//////////// Attributes/ Variables ///////////////////////
	   /* $composite = $app->input->get('composite', '', 'raw');
		if(!empty($composite)){
			$session->set('composite', $app->input->get('composite', '', 'raw'));
			//$attributes = JRequest::getVar('attributes');
			$composite = array_merge(array("MPI"), $composite);
		}else{
			$session->clear('composite');
		}


		$attributes = $app->input->get('attributes', '', 'raw');
		if(!empty($attributes)){
			$composite = $session->get('composite');
		    //echo  '<br/>==<br/>'.implode(",",$composite);
		    //echo  '<br/>==<br/>'.implode(",",$attributes);
			//$attributes = JRequest::getVar('attributes');
			if(!empty($composite)){
				$attributes = array_merge(array("MPI"),$attributes,$composite);
			}else{
				$attributes = array_merge(array("MPI"),$attributes);
			}
			//echo  '<br/>==<br/>'.implode(",",$attributes);
		}
		//echo "a<pre>";print_r($attributes);exit;
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('attributes');
		if($sessionattr != ""){
			if($app->input->get('attributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('attributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);

		$state_items = $this->get('StateItems');

		$this->dataof                 = $dataof;
		$this->state_items            = $state_items;
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;

		$this->Itemid = $app->input->get('Itemid', 188);

		$customattribute  = $session->get('customattribute');
		$new_name         = $app->input->get('new_name', array(), 'array');
		$custom_attribute = $app->input->get('custom_attribute', array(), 'array');

		$customdata = $this->get('CustomData');

		$this->get('customattrlib');
		$this->customattributelib = $session->get('customattributelib');

		$this->customattribute    = $customattribute;*/


		///////////////////////////////////////



		$app->input->set('composite', $session->get('composite'));
		$app->input->set('m_type_rating', $session->get('m_type_rating'));
		$app->input->set('attributes', $session->get('attributes'));
		$app->input->set('state', $session->get('state'));
		$app->input->set('m_type', $session->get('m_type'));
		$app->input->set('preselected', $session->get('district'));

		$msg = $app->input->get('msg', '', 'raw');
		if($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}else if($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items    = $this->get('StateItems');
		$this->composite_attr = $this->get('compositeAttr');
		$this->typesArray     = $this->get('AttributeType');



		return parent::display($tpl);
	}

	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcaturl = $db->loadResult();
	}

}
