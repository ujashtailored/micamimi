<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Dashboard model.
 *
 * @since  1.6
 */
class MicaModelDashboard extends JModelLegacy
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Fetch user Account innformations.
	 */
	public function getAccount(){
		$db   = $this->getDBO();
		$user = JFactory::getUser();

		//$QUERY = "SELECT *, a.params as par
		$QUERY = "SELECT a.id, a.title, b.plan_subscription_from_date, b.plan_subscription_to_date
			FROM ".$db->quoteName('#__osmembership_plans')." AS a
			INNER JOIN ".$db->quoteName('#__osmembership_subscribers')." AS b
				ON 		".$db->quoteName('a.id')." = ".$db->quoteName('b.plan_id')."
					AND ".$db->quoteName('b.user_id')." = ".$db->quote($user->id)."
			WHERE ".$db->quoteName('a.published')." = ".$db->quote(1);
		$db->setQuery($QUERY);
		$user->plandetails = $db->loadObject();

		return $user;
		/*
			$name          =$user->name;
			$institutename =$user->institute_name;
			$address       =$user->address;
			$contactperson =$user->contactperson;
			$designation   =$user->designation;
			$contactnumber =$user->contactnumber;
			$mobile        =$user->mobile;
			$paymentoption =$user->paymentoption;
			$chequeno      =$user->chequeno;
			$bankname      =$user->bankname;
			$date          =$user->date;
			$whywnattobuy  =$user->whywnattobuy;
		*/
		//return array($currentplan,$name,$institutename,$address,$contactperson,$designation,$contactnumber,$mobile);
	}

	/**
	 *
	 */
	public function getWorkspace(){
		$db    = $this->getDBO();
		$query = "SELECT name, id, create_date ,modify_date
			FROM ".$db->quoteName('#__mica_user_workspace')."
			WHERE ".$db->quoteName('userid')." = ".$db->quote(JFactory::getUser()->get('id'))."
				AND ".$db->quoteName('is_default')." <> ".$db->quote(1);
		$db->setQuery($query);
		$result = $db->loadAssocList();
		return $result;
	}

	function getGroup(){
		$db = $this->getDBO();

		$query = "SELECT  * FROM ".$db->quoteName('#__mica_group_field')." allfields
			JOIN ".$db->quoteName('#__mica_group')." grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
			WHERE ".$db->quoteName('grp.publish')." = 1 ";
		$db->setQuery($query);
		$list=$db->loadAssocList();

		$change_group_name = 1;
		foreach($list as $each){
			$todisplay[$each['group']]=$each['groupid'];

		}
		return $todisplay;
	}


	/**
	 *
	 */
	public function getCreatedAccount(){
		$db = $this->getDBO();
		$db->setQuery("SELECT count(*) FROM ".$db->quoteName('#__mica_adduser')." WHERE ".$db->quoteName('login_userid')." = ".$db->quote(JFactory::getUser()->get('id')));
		return (int) $db->loadResult();
	}

	/**
	 *
	 */
	public function getCreatedAccountFromAEC(){
		$db = $this->getDBO();

		$now = $db->quote(JFactory::getDate('now')->format('Y-m-d'));
		$SQL = "SELECT b.usercreation_limit
			FROM ".$db->quoteName('#__osmembership_plans')." AS a
			INNER JOIN ".$db->quoteName('#__osmembership_subscribers')." AS b ON ".$db->quoteName('a.id')." = ".$db->quoteName('b.plan_id')."
			WHERE ".$db->quoteName('b.user_id')." = ".$db->quote(JFactory::getUser()->get('id'))."
				AND ".$db->quoteName('b.published')." = 1
				AND (	".$db->quoteName('a.lifetime_membership')." = ".$db->quote(1)."
						OR (DATEDIFF(".$now.", ".$db->quoteName('from_date').") >= -1 AND DATE(".$db->quoteName('to_date').") >= ".$now.")
				)
			ORDER BY ".$db->quoteName('to_date')." DESC ";
		$db->setQuery($SQL);
		return (int) $db->loadResult();
	}

	/**
	 * Returns parent account infomration if logged in user is child account.
	 */
	public function getParentinfo(){
		$user = JFactory::getUser();
		$db   = $this->getDBO();
		$sql  = "SELECT a.* FROM ".$db->quoteName('#__users')." AS a
			INNER JOIN ".$db->quoteName('#__mica_adduser')." AS muser
				ON 		".$db->quoteName('muser.login_userid')." = ".$db->quoteName('a.id')."
					AND ".$db->quoteName('muser.created_userid')." = ".$db->quote($user->get('id'))."
			GROUP BY ".$db->quoteName('a.id')."
			ORDER BY ".$db->quoteName('a.name')." DESC ";
		$db->setQuery($sql);
		$parent = $db->loadObject();

		$groups = array_values($user->groups);
		if (count($groups) > 0 && $parent != null) {
			$db->setQuery("SELECT title FROM ".$db->quoteName('#__usergroups')." WHERE ".$db->quoteName('id')." = ".$db->quote($groups[0]));
			$parent->groupname = $db->loadResult();
		}
		return $parent;
	}

	/**
	 * Returns parent account Subscription infomration if logged in user is child account.
	 */
	public function getParentsubscriptioninfo(){
		$parent = $this->getParentinfo();
		$plandetails = null;
		if ($parent != null) {
			$db   = $this->getDBO();
			$QUERY = "SELECT a.id, a.title, b.plan_subscription_from_date, b.plan_subscription_to_date
				FROM ".$db->quoteName('#__osmembership_plans')." AS a
				INNER JOIN ".$db->quoteName('#__osmembership_subscribers')." AS b
					ON 		".$db->quoteName('a.id')." = ".$db->quoteName('b.plan_id')."
						AND ".$db->quoteName('b.user_id')." = ".$db->quote($parent->id)."
				WHERE ".$db->quoteName('a.published')." = ".$db->quote(1);
			$db->setQuery($QUERY);
			$plandetails = $db->loadObject();
		}
		return $plandetails;
	}

	/**
	 *
	 */
	public function isAllowedAdd() {
		$db = $this->getDBO();
		$isAllowedQry = "SELECT ipaddress FROM ".$db->quoteName('#__users')." WHERE ".$db->quoteName('id')." = ".$db->quote(JFactory::getUser()->get('id'));
		$db->setQuery($isAllowedQry);
		return $db->loadResult();
	}
}
