<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/helpers/Workspace.php';
require_once JPATH_COMPONENT . '/helpers/css.php';

/**
 * MICA Villages Showresults model.
 *
 * @since  1.6
 */
class MicaModelVillageShowresults extends JModelLegacy
{

	var $_data                  = null;
	var $_total                 = null;
	var $_pagination            = null;
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $ruralprefix            = "Rural_";
	var $urnbanprefix           = "Urban_";
	var $totalprefix            = "Total_";

	function __construct(){
		parent::__construct();

		$app  = JFactory::getApplication();
		$user = JFactory::getUser();
		if($user->id == 0){
			$app->redirect("index.php");exit;
		}
		$this->_table_prefix = '#__mica_';
		$limit               = $app->input->get('limit','limit', 25, 0);
		//$limitstart        = $mainframe->getUserStateFromRequest($context.'limitstart','limitstart',0);
		//$limitstart        = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$jlimitstart         = $app->input->get('limitstart');
		if(!$jlimitstart)	$limitstart=0;

		$this->setState('limit',1);
		$page = ($app->input->get("limitstart") != "") ? $app->input->get("limitstart") : 0;
		$statingpage = null;
		$this->setState('limitstart', $page);
		//$this->setState('limitstart',$limitstart);
	}

	/**
	 * A function called from view to get states.
	 */
	public function getStateItems(){
		$res = null;
		$db    = $this->getDBO();
		$query = "SELECT id, name FROM ".$db->quoteName('rail_state')." GROUP BY ".$db->quoteName('name');
		$db->setQuery($query);
		try {
			$res = $db->loadObjectList();
		} catch (Exception $e) {

		}
		return $res;
	}

	/**
	 *
	 */
	public function getAttributeCheck($db_table, $attributes, $m_type = null){
		$app     = JFactory::getApplication();
		$session =JFactory::getSession();
		$db      = $this->getDBO();

		$query = "DESCRIBE ".$db_table;
		$db->setQuery($query);
		$fields = $db->loadObjectList();

		//$m_types=$session->get('m_type');
		foreach($fields as $eachfield){
			$tablefield[] = $eachfield->Field;
		}

		if(!is_array($attributes)){
			$attributes=explode(",",$attributes);
		}
		$attrstr=array();

		//foreach($m_types as $m_type)
		{
			foreach($attributes as $eachattr)
			{
				$status=0;
				if($m_type=="Total" && $eachattr!="MPI" && $eachattr!="Score")
				{
					if(in_array($this->totalprefix.$eachattr,$tablefield))
					{
						//$attrstr[]=$this->totalprefix.$eachattr;
						$status=1;
					}

				}
				if($m_type=="Urban" && $eachattr!="MPI"  && $eachattr!="Score")
				{
					if(in_array($this->urnbanprefix.$eachattr,$tablefield))
					{
						//$attrstr[]=$this->urnbanprefix.$eachattr;
						$status=1;
					}
				}
				if($m_type=="Rural" && $eachattr!="MPI"  && $eachattr!="Score")
				{
					if(in_array($this->ruralprefix.$eachattr,$tablefield))
					{
						//$attrstr[]=$this->ruralprefix.$eachattr;
						$status=1;
					}
				}
				if($eachattr=="MPI")
				{
					$m_type_rating = $app->input->get("village_m_type_rating", array("Total"), 'raw');
					foreach($m_type_rating as $eachprefix)
					{
						if(in_array($eachprefix."_".$eachattr,$tablefield))
						{
							//$attrstr[]=$eachprefix."_".$eachattr;
							$status=1;
						}
					}

				}

				if($status==0)
				{
					if(in_array($eachattr,$tablefield))
					{
					$attrstr[]=$eachattr;
					}
				}
			}
		}

		//echo implode(",",$attrstr);exit;
		return implode(",",$attrstr);
	}

	/**
	 * A function called from view.
	 */
	public function getCustomData($infotype = null, $pagefromcontroller = null)
	{
		$session      = JFactory::getSession();
		$app          = JFactory::getApplication();
		$db           = $this->getDBO();
		$states       = $session->get('villagestate');
		$mystates     = explode(",",$states);
		$mystates     = array_filter($mystates);
		$statesinsert = implode("','",$mystates);
		$district     = $session->get('villagedistrict');
		$villages     = $session->get('villages_villages');
		$urban        = $session->get('urban');
		$m_types      = $session->get('villagem_type');
		$m_types      = !empty($m_types) ? $m_types : $app->input->get('villagem_type', 'array', array());
		$attributes   = $session->get('villagesattributes');
		$attributes   = explode(",",$attributes);

		//if(substr_count($district,"362")) { $district=$district.",363"; }
		$mytmp = array_filter($attributes);

		if ($pagefromcontroller != "")
		{
			$page        = 13;
			$statingpage = 0;
		}
		else
		{
			$page        = ($app->input->get("limitstart") != "" ) ? $app->input->get("limitstart") : 0;
			$statingpage = null;
			//$this->setState('limitstart',$page); $this->setState('limit',13);
		}

		$db_table   = "";
		$attributes = array();
		$attribute  = "";

		// if ($villages == "" && $urban == "")
		if (!empty($villages))
		{
			//if($district!="")
			$db_table = "villages";
		 	// else
		 	// 	$db_table="rail_state";

			// foreach($m_types as $m_type)
			// {
			// 	foreach($mytmp as $eachvar)
			// 	{
			// 		if ($m_type == "Total" )
			// 		{
			// 			if (strpos($eachvar, $this->urnbanprefix) === false && strpos($eachvar, $this->ruralprefix) === false &&  strpos($eachvar, $this->totalprefix) === false)
			// 			{
			// 				$attributes[] = $this->totalprefix . $eachvar;
			// 			}
			// 			else
			// 			{
			// 				$attributes[] = $eachvar;
			// 			}
			// 		}
			// 		elseif ($m_type == "Rural")
			// 		{
			// 	 		//  $db_table="india_information";
			// 			if (strpos($eachvar, $this->urnbanprefix) === false && strpos($eachvar, $this->ruralprefix) === false && strpos($eachvar, $this->totalprefix) === false)
			// 			{
			// 	 	  		$attributes[] = $this->ruralprefix . $eachvar;
			// 			}
			// 			else
			// 			{
			// 				$attributes[] = $eachvar;
			// 			}
			// 	 	}
			// 	 	elseif ($m_type == "Urban")
			// 	 	{
			// 			// $db_table     = "india_information";
			// 			// $attributes[] = $this->urbanprefix.$eachvar;
			// 			if (strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false &&  strpos($eachvar, $this->totalprefix)=== false)
			// 			{
			// 	 	  		$attributes[] = $this->urnbanprefix . $eachvar;
			// 			}
			// 			else
			// 			{
			// 			 	$attributes[] = $eachvar;
			// 			}
			// 	 	}
			// 	 	else
			// 	 	{
			// 			// $attributes[] = implode(",",$mytmp);
			// 	 	}
			// 	}
			// }
      $attributes=$mytmp;
		}

		$attributes = implode(",", $attributes);

		$session->set('themeticattribute', $attributes);

		if (trim($district) == null || trim($district) == ",")
		{
			if ($states != "all," && $states != ""  && $states != "all")
			{
				$mystates = explode(",",$states);
				$mystates = array_filter($mystates);
				$mystates = implode("','",$mystates);
				$where    = "WHERE ".$db->quoteName('name')." IN ('".$mystates."') ";

				if ($session->get('state_orgfid') != "")
				{
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND ".$this->statesearchvariable." IN ('".$mystates1."')";
				}
			}
			else
			{
				$mystates = "1=1";
				$where    = "";

				if ($session->get('state_orgfid') != "")
				{
					$state_orgfid = $session->get('state_orgfid');

					// Condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " WHERE " . $this->statesearchvariable . " IN ('" . $mystates1 . "') ";
				}
			}

			$query = "SELECT " . $attributes . " , " . $this->statesearchvariable . " , name
				FROM " . $db->quoteName('rail_state')."
				" . $where . "
				GROUP BY " . $db->quoteName('name') . "
				ORDER BY " . $db->quoteName('name');
			$db->setQuery($query);

			$session->set('activetable', "rail_state");
			$session->set('activenamevariable', "name");
			$session->set('activesearchvariable', $this->statesearchvariable);

			if ($infotype != "")
			{
				//$session->set('popupactivedata',$db->loadObjectList());
			}
			else
			{
				//$session->set('activedata',$db->loadObjectList());
			}

			try
			{
				$this->_data = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$this->_data = NULL;
			}

			return $this->_data;
		}
		else
		{
			/*if ($district != "all," && $district != "all")
			{
				$states   = explode(",", $states);
				$states   = array_filter($states);
				$states   = implode("','", $states);
				$district = explode(",", $district);
				$district = array_filter($district);
				$district = implode("','", $district);

				$where    = "WHERE " . $this->districtsearchvariable . " IN ('" . $district . "') AND " . $db->quoteName('state') . " IN ('" . $states . "') ";

				if ($session->get('district_orgfid') != "")
				{
					$state_orgfid = $session->get('district_orgfid');

					// Condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND " . $this->districtsearchvariable . " IN ('" . $mystates1 . "') ";
				}
			}
			else
			{
				$states = explode(",", $states);
				$states = array_filter($states);
				$states = implode("','", $states);
				$where  = "WHERE 1 = 1 AND " . $db->quoteName('state') . " IN ( '" . $states . "')";

				if ($session->get('district_orgfid') != "")
				{
					$state_orgfid = $session->get('district_orgfid');

					// Condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND " . $this->districtsearchvariable . " IN ('" . $mystates1 . "')";
				}
			}*/

			if (!is_array($villages)) {
				$villages = explode(',', $villages);
			}

			$villages = implode("','", $villages);

			$where  = "WHERE 1 = 1 AND " . $db->quoteName($this->districtsearchvariable) . " IN ( '" . $villages . "')";

			/*$query = "SELECT " . $attributes . " ," . $this->districtsearchvariable . ", concat(place_name,'-',district_name) as name, state_name as state, district_name as distshp
				FROM " . $db->quoteName('villages') . "
				" . $where . "
					AND " . $db->quoteName('district_name') . " <> ''
					AND " . $db->quoteName('place_name') . " <> ''
				ORDER BY " . $db->quoteName('state_name') . " ASC
				LIMIT " . (($statingpage === null) ? (($page) * 50) : $statingpage) . "," . (($statingpage === null) ? 50 : 650);*/

			$task = $app->input->getCmd('task');
			$limitCondition = "";

			if ($task != 'exportexcel') {
				$limitCondition = "LIMIT " . (($statingpage === null) ? (($page) * 50) : $statingpage) . "," . (($statingpage === null) ? 50 : 650);
			}

			$query = "SELECT " . $attributes . " ," . $this->districtsearchvariable . ", concat(place_name,'-',district_name) as name, state_name as state, district_name as distshp
				FROM " . $db->quoteName('villages') . "
				" . $where . "
					AND " . $db->quoteName('district_name') . " <> ''
					AND " . $db->quoteName('place_name') . " <> ''
				ORDER BY " . $db->quoteName('state_name') . " ASC " . $limitCondition;
				;
			$db->setQuery($query);

			$session->set('activetable', "villages");
			$session->set('activenamevariable', "place_name");
			$session->set('activesearchvariable', $this->districtsearchvariable);

			/*if ($infotype != ""){
				//$session->set('popupactivedata',$db->loadObjectList());
			}else{
				//$session->set('activedata',$db->loadObjectList());
			}*/

			try
			{
				$this->_data = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$this->_data = NULL;
			}

			return $this->_data;
		}
	}


	/**
	 * function called from controller.
	 */
	public function getMaxForMatrix(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$table         = $session->get('activetable');
		$variable      = $session->get('themeticattribute');
		$variable      = explode(",",$variable);
		$maxValsSelect = "";
		foreach($variable as $eachvariable){
			$maxValsSelect[] = "MAX(" . $eachvariable . ") AS '" . $eachvariable . "'";
		}
		$maxValsSelect = implode(" , ",$maxValsSelect);

		// Complete the query
 		$maxValsQuery = "SELECT $maxValsSelect FROM $table LIMIT 0,1";
		$db->setQuery($maxValsQuery);
		return $db->loadAssocList();
	}

	/**
	 * [getGraph description]
	 *
	 * @return  [type]  [description]
	 */
	public function getGraph()
	{
		$session    = JFactory::getSession();
		$states     = $session->get('villagestate');
		$district   = $session->get('villagedistrict');
		$villages   = $session->get('villages_villages');
		$urban      = $session->get('urban');
		$attributes = $session->get('villagesattributes');

		$attributes      = explode(",", $attributes);
		$attributes      = array_filter($attributes);
		$excludeVarArray = array(
		    'Agricultural_Commodities_First',
		    'Agricultural_Commodities_Second',
		    'Agricultural_Commodities_Third',
		    'Manufacturers_Commodities_First',
		    'Manufacturers_Commodities_Second',
		    'Manufacturers_Commodities_Third',
		    'Handicrafts_Commodities_First',
		    'Handicrafts_Commodities_Second',
		    'Handicrafts_Commodities_Third',
			'Agricultural_Marketing_Society_Status_A1NA2',
			'MandisRegular_Market_Status_A1NA2',
			'Public_Distribution_System_PDS_Shop_Status_A1NA2',
			'Weekly_Haat_Status_A1NA2',
			'CinemaVideo_Hall_Status_A1NA2',
			'Community_Centre_withwithout_TV_Status_A1NA2',
			'Daily_Newspaper_Supply_Status_A1NA2',
			'Mobile_Phone_Coverage_Status_A1NA2',
			'Public_Library_Status_A1NA2',
			'Sports_ClubRecreation_Centre_Status_A1NA2',
			'Sports_Field_Status_A1NA2',
			'Tractors_Status_A1NA2',
			'Commercial_Bank_Status_A1NA2'
		);


		$attributes = array_values(array_diff($attributes, $excludeVarArray));
		$attr       = array();
		$i          = 0;

		foreach ($this->getCustomData(1, 1) as $key => $val)
		{
			foreach ($val as $ek => $ev)
			{
				if ($ev == "")
				{
					$ev = "0";
				}

				foreach ($attributes as $eachattr)
				{
					if ($ek == $eachattr)
					{
						$attr[$ek][] = $ev;
					}

					if ($ek == $this->urnbanprefix.$eachattr)
					{
						$attr[JTEXT::_($this->urnbanprefix . $eachattr)][] = $ev;
					}

					if ($ek == $this->totalprefix . $eachattr)
					{
						$attr[JTEXT::_($this->totalprefix . $eachattr)][] = $ev;
					}

					if ($ek == $this->ruralprefix . $eachattr)
					{
						$attr[JTEXT::_($this->ruralprefix . $eachattr)][] = $ev;
					}

					if ($ek == "name")
					{
						$attrs['name'][$i] = $ev;
					}
				}
				if($i == 5){break;}
			}
			if($i == 5){break;}
			$i++;
		}

		$customattribute = $session->get('customattribute');
		$eachcustomattr  = array();
		$customattribute = explode(",",$customattribute);
		$customattribute = array_filter($customattribute);
		$z = 0;

		foreach ($attrs['name'] as $eachname)
		{
			foreach ($customattribute as $eachcustom)
			{
				$eachcustom = explode(":", $eachcustom);
				$eachcustomattr[$eachcustom[0]][$z] = $this->getCustomAttributeValue($eachcustom[1], $this->_data,$eachname);
				if ($z == 10){break;}
			}

			if($z==5){break;}
			$z++;
		}

		$attr          = array_merge($attr, $eachcustomattr);
		$attr          = array_slice($attr, 0, 10);
		$arraytoreturn = "";
		$lastkey       = count(($attr));
		$z             = 1;

		foreach ($attr as $key => $val)
		{
			if ($z !== $lastkey)
			{
				$arraytoreturn .= JTEXT::_($key) . ";" . implode(";", $val) . "sln";
			}
			else
			{
				$arraytoreturn .= JTEXT::_($key) . ";" . implode(";", $val);
			}

			$z++;
		}

		return $arraytoreturn;
	}

	/**
	 * [getGraphSettings description]
	 *
	 * @return  [type]  [description]
	 */
	public function getGraphSettings()
	{
		$session       = JFactory::getSession();
		$states        = $session->get('villagestate');
		$district      = $session->get('villagedistrict');
		$villages      = $session->get('villages_villages');
		$urban         = $session->get('urban');

		//$attributes  = $session->get('villagesattributes');
		$data_settings = "";
		/*
			if($villages!=""){
				$i=0;
				foreach($this->_data as $eachdata){
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
					$data_settings .= "</graph>";
					$i++;
				}
			}else if($urban!=""){
				$i = 0;
				foreach($this->_data as $eachdata)
				{
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
					$data_settings .= "</graph>";
					$i++;
				}
			}
		*/

		if(trim($district) == ","  || strlen($district) == 0){
			$i = 0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
				$data_settings .= "</graph>";
				$i++;
				if($i == 6){
					break;
				}
			}
		}else{
			$i = 0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
				$data_settings .= "</graph>";
				$i++;
				if($i == 6){
					break;
				}
			}
		}

		/*$session = JFactory::getSession();
		$attrnamesession=$session->get('customattribute');
		if(strlen($attrnamesession)>0)
		{
			$attrnamesession=explode(",",$attrnamesession);
			$attrnamesession=array_filter($attrnamesession);
			foreach($attrnamesession as $eachattr)
			{
				$name=explode(":",$eachattr);
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".$name[0]."]]></title>";
				$data_settings .= "</graph>";
				$i++;

			}

		}*/
		return $data_settings;
	}

	function scorevariablename($srprefix,$key,$type){
		foreach($srprefix as $eachprefix){
			if(strpos($key,$eachprefix) !== false){
				if($type == 1){
					return  str_replace($eachprefix,"",$key);
				}else{
					return str_replace("_","",$eachprefix);
				}
			}
		}
	}

	public function getDisplayGroup()
	{
		$task            = JFactory::getApplication()->input->getCmd('task');
		$session         = JFactory::getSession();
		$db              = JFactory::getDBO();
		$urban           = $session->get('urban');
		$villages        = $session->get('villages_villages');
		$district        = $session->get('villagedistrict');
		$state           = $session->get('villagestate');
		$attributes      = $session->get('villagesattributes');
		$attributes      = explode(",", $attributes);
		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		$eachcustomattr  = array();

		foreach($customattribute as $eachcustom){
			$eachcustom                     = explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];
		}

		$eachcustomattr = array_filter($eachcustomattr);

		if($villages != ""){
			$db_table = "villages";
			$session->set('dataof',"Villages");
		}else if($district != ""){
			$db_table = "district";
			$session->set('dataof',"District");
		}else if($urban != ""){
			$db_table = "urban";
			$session->set('dataof',"UA");
		}else{
			$db_table = "state";
		}

		$query ="SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
				INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
			WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
				AND ".$db->quoteName('grp.publish')." = ".$db->quote(1)."
			ORDER BY ".$db->quoteName('grp.ordering').", ".$db->quoteName('allfields.field')." ASC ";
		$db->setQuery($query);
		$list = $db->loadAssocList();

		$change_group_name = 1;
		$addedarray        = array();

		foreach($list as $each){
			foreach($this->_data as $togrp){
				//Edited by Dave, condition to check if its Diu, then ommit from the text display list.
				//if($togrp->OGR_FID == 363)
					//continue;

				foreach($togrp as $key => $val){
					if ($task == 'exportexcel') {
						$statename = $togrp->OGR_FID . '_' . $togrp->name;
					}
					else{
						$statename = $togrp->name;
					}

					if(($each['field'] == $key || $this->urnbanprefix.$each['field'] == $key || $this->totalprefix.$each['field'] == $key || $this->ruralprefix.$each['field'] == $key) || ($each['group'] == "Score" && $each['field'] == $key) ){

						foreach($attributes as $eachsessionattr){
							if(trim($each['group']) == "Score" && $eachsessionattr == $key){

								$srprefix     = "Rural_";
								$suprefix     = "Urban_";
								$stprefix     = "Total_";
								$sprefixarray = array("Rural_","Urban_","Total_");

								if((strpos($key,$srprefix)==0) || (strpos($key,$suprefix)==0) || (strpos($key,$stprefix)==0)){
									$todisplay[$statename][JTEXT::_($each['group'])][JTEXT::_($this->scorevariablename($sprefixarray,$eachsessionattr,1))][$this->scorevariablename($sprefixarray,$eachsessionattr,0)] = array($this->scorevariablename($sprefixarray,$eachsessionattr,0) => $val);
									$icon[$each['group']] = $each['icon'];//$addedarray[]=$this->scorevariablename($sprefixarray,$eachsessionattr,1);
								}
							}else if(strstr($each['field'], $eachsessionattr) && trim($each['group']) != "Score"){
								$segment = explode($eachsessionattr,$key);

								if($segment[1] == "" && $segment[0].$each['field'] == $key){
									$todisplay[$statename][JTEXT::_($each['group'])][JTEXT::_($eachsessionattr)][] = array(str_replace("_","",$segment[0])=>$val);
									$icon[$each['group']] = $each['icon'];
									$addedarray[]         = $segment[0];
								}
							}

							if(count($eachcustomattr) != 0){
								foreach($eachcustomattr as $keys => $vals){
									//if(stristr($vals,$eachsessionattr))
									{
										$ev = $this->getCustomAttributeValue($vals, $this->_data, $statename);
										$todisplay[$statename]['Custom Variable'][$keys][$vals] = array($keys => $ev);
									}
								}
							}
							//ksort($todisplay[$statename][$each['group']][$eachsessionattr],SORT_STRING);
						}
					}
				}
				//$todisplay[$statename]=array_reverse($todisplay[$statename]);
				//break;
			}
		}

		return array($todisplay, $icon);
	}

	function countkeys($data){
		$i=0;
		foreach($data as $eachdata){
			$i = $i + count($eachdata);
		}
		return $i;
	}

	function getNewAttributeTable_cont()
	{
		$gpdata=$this->getDisplayGroup();
		$groupwisedata=$gpdata[0];
		$icon=$gpdata[1];
		$segmentrow=0;
		foreach($icon as $ikey=>$ival)
		{
			foreach($groupwisedata as $key=>$val)
			{
				$newgroupwisedata[$key][$ikey]=array_pop(array_values($val[$ikey]));
				$segmentrow=count(array_pop(array_values($val[$ikey])));
			}
		}
		//echo "<pre>";echo $segmentrow;
		$totalrows= count($newgroupwisedata)*$segmentrow;
		$totalcolumns=count($icon);

		$session = JFactory::getSession();
		$activetable=$session->get("activetable");

		if($activetable  == "india_information"){
			$datatablename = "District Name";
		}

		if($activetable  == "rail_state"){
			$datatablename = "State Name";
		}

		if($activetable == "jos_mica_urban_agglomeration"){
			$datatablename = "UA Name";
		}

		if($activetable == "villages"){
			$datatablename = "City Name";
		}

		/*$customattribute=$session->get('customattribute');
		$customattribute=explode(",",$customattribute);
		foreach($customattribute as $eachcustom)
		{
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]]=$eachcustom[1];

		}*/

		$c           = array_keys($icon);
		$headernames = implode("</td><td width='200'>", $c);
		$str         = "<tr class='firsttableheader'><td width='200'>" . $datatablename . "</td><td width='200'>" . $headernames . "</td></tr>";
		$i           = 0;

		foreach ($newgroupwisedata as $key => $val)
		{
			if ($i <= $totalcolumns)
				{
					$str .= "<tr ><td width='200'>".$key."</td>";
				}
				else
				{
					// $newgroupwisedata
					$i = 0;
				}

			foreach($icon as $k=>$v)
			{
				$av = array_values($val[$k][$i]);
				$str .="<td width='200'>".$av[0]."</td>";
			}

			$str .="</tr>";
		}

		return $str;
	}


	/**
	 * A function called from view.
	 */
	//getNewAttributeTableold currently unused
	public function getNewAttributeTable()
	{
		$session       = JFactory::getSession();
		$gpdata        = $this->getDisplayGroup();
		$groupwisedata = $gpdata[0];
		$icon          = $gpdata[1];

		$activetable   = $session->get("activetable");
		switch ($activetable) {
			case 'india_information':
				$datatablename = "District Name";
				break;

			case 'rail_state':
				$datatablename = "State Name";
				break;

			case 'jos_mica_urban_agglomeration':
				$datatablename = "UA Name";
				break;

			case 'villages':
				$datatablename = "City Name";
				break;

			case 'india_information':
				$datatablename = "District Name";
				break;
		}

		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		foreach($customattribute as $eachcustom){
			$eachcustom                     = explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];
		}

		$i=1; $j=0; $k=0; $l=0;
		$headerspan = 0;
		//$groupwisedata=array_reverse($groupwisedata);
		foreach($groupwisedata as $mainname => $data){
			$header[$i]     = "<td >".$mainname."</td>";
			$grpspan        = 0;
			$rearrangeddata = array();
			$tmp            = "";
			$tmp1           = "";

			foreach($data as $grpname => $grpdata){
				if($grpname == "Mkt Potential Index"){
					$tmp = $grpdata;
				}else if($grpname == "Composite Score" ||$grpname == "Score"){
					$tmp1 = $grpdata;
				}else{
					$rearrangeddata[$grpname] = $grpdata;
				}
				$rea++;
			}

			$Market  = array("Mkt Potential Index" => $tmp);
			$Market1 = array("Composite Score" => $tmp1);
			$data    = array_merge($Market, $Market1, $rearrangeddata);

			foreach($data as $grpname => $grpdata){
				//$headerspan +=$this->getColspan($grpdata,2);
				foreach($grpdata as $variablegrp => $variabledata){
					$grpspan = 0;
					$vargrpname[$i][$k] = "<td colspan='".count($variabledata)."'><span class=''>".$variablegrp."<a href='index.php?option=com_mica&task=villageshowresults.deleteattribute&attr=".$variablegrp."' class='right_arrow'>X</a></span></td>";

					foreach($variabledata as $eachvariabledata){
						foreach($eachvariabledata as $key => $value){
							$mygrpvariablename[$i][$k][$l] = "<td>".$key."</td>";
							$mygrpdata[$i][$k][$l]         = "<td>".$value."</td>";
							$grpspan++;$headerspan++;
						}

						$img = "";
						if($icon[$grpname]!=""){
							$img = "<img src='".JUri::base()."components/com_mica/images/".$icon[$grpname]."'/>";
						}
						$grpheader[$i][$j] = "<td colspan='".$this->countkeys($grpdata)."'><div class='rightgrptext'>".$grpname."</div></td>";//<div class='leftgrpimg'>".$img."</div>
						$l++;
					}
					$k++;
				}
				$j++;
			}
			$i++;
		}
		//$mygrpvariablename=array_map('array_filter', $mygrpvariablename);


		//$str = "<tr class='firsttableheader'><td width='200'>".$datatablename."</td>";//<tr><td colspan='".(($headerspan)+1)."'>Data</td></tr>
		$str = "<tr class='firsttableheader'><td width='200'></td>";
		foreach($grpheader as $inserheader){
			foreach($inserheader as $inserheaderval){
				$str .= $inserheaderval == '-' ? '' : $inserheaderval;
			}
			break;
		}

		$str .="</tr>";
		$str .="<tr class='secondtableheader'><td></td>";
		foreach($vargrpname as $eachgrpname){
			foreach($eachgrpname as $name){
				$str .= $name;
			}
			break;
		}
		$str .="</tr>";

		$a = array_pop(array_unique(array_pop(array_values(array_unique($mygrpvariablename)))));
		$a = array_filter(array_values($a));
		if(strlen(str_replace("<td></td>","",$a[0]))==0){
			$skip = 1;
		}else{
			$skip = 0;
		}

		if($skip == 0){
			$str .="<tr class='thirdtableheader'><td ></td>";
			foreach($mygrpvariablename as $variablenamegrp){
				foreach($variablenamegrp as $varname){
					foreach($varname as $each){
						$str .= $each;
					}
				}
				break;
			}
			$str .="</tr>";
		}

		$lastelements = 0;
		foreach($mygrpdata as $key=>$gdata){
			$loadclass = ($lastelements >=39)?"loadanother":"";
			$str      .= "<tr class='".$loadclass."'>".$header[$key];
			foreach($gdata as $data){
				foreach($data as $d){
					$str .=($d=="")?"N/A":$d;
				}
			}
			$str .="</tr>";
			$lastelements++;
		}

		return $str;
	}


	function hasValues($input, $deepCheck = true) {
	    foreach($input as $value) {
	        if(is_array($value) && $deepCheck) {
	            if($this->hasValues($value, $deepCheck))
	                return true;
	        }
	        elseif(!empty($value) && !is_array($value))
	            return true;
	    }
	    return false;
	}

	/**
	 *
	 */
	public function getColspan($groupwisedata, $onlevel){
		$keys = array_keys($groupwisedata);
		$vals = array_values($groupwisedata);

		if($onlevel==3){
			return count($keys);
		}else if($onlevel==2){
			return count($keys)*count($vals[0]);
		}else if($onlevel==1){
			return  $this->getColspan($groupwisedata,2)+$this->getColspan($groupwisedata,3);
		}
	}

	function getAttributeTable()
	{

		$session = JFactory::getSession();
		$m_type=$session->get('m_type');
		$states=$session->get('states');
		$district=$session->get('villagedistrict');

		$customattribute=$session->get('customattribute');
		$customattribute=explode(",",$customattribute);
		foreach($customattribute as $eachcustom)
		{
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]]=$eachcustom[1];

		}
		$compareflag=0;
		if(count($this->_data) ==0)
		{
			return "No Data Available";
		}
		else if(count($this->_data) >1)
		{
			$compareflag=1;
		}
		$heads=array();
		if(1 != 1)
		{
			if(trim($district)=="")
			{	$i=0;
				foreach($this->_data as $key=>$val)
				{      foreach($val as $ek=>$ev)
					{
					if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
						{
							$heads[$ek][]=$ev;
						}
						else
						{
							$deleteindex[]=$ev;

						}


					}
				}
			}
			else
			{
				$i=0;
				foreach($this->_data as $key=>$val)
				{      foreach($val as $ek=>$ev)
					{

					if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
						{
							$heads[$ek][]=$ev;
						}
						else
						{
							$deleteindex[]=$ev;

						}

					}
				}
			}


			$heads=array_reverse($heads);
			$heads=array_filter($heads);
				$heads=array_merge($heads,$eachcustomattr);
			$deleteindex=array_reverse($deleteindex);
			$mykeys=array_keys($heads);
				$eachcustomattr=array_filter($eachcustomattr);


			$head="";
			$i=0;
			$data="";
			foreach($mykeys as $eachkey)
			{
				if($eachkey=="name")
				{
					$head .="<th>State Name</th>";
				}
				else if($eachkey=="distshp")
				{
				$head .="<th>District Name</th>";
				}
				else if($eachkey=="place_name")
				{
					$head .="<th>Villages Name</th>";
				}
				else if($eachkey=="UA_name")
				{
					$head .="<th>Urban Name</th>";
				}
				else
				{
					$head .="<th>".$eachkey."</th>";
				}


			}
			 $head ="<tr>".$head."</tr>";

			foreach($this->_data as $eachkey)
			{
				$data .="<tr>";
				for($i=0;$i<count($mykeys);$i++)
				{
					if(!empty($eachcustomattr[$mykeys[$i]]) && isset($eachcustomattr[$mykeys[$i]]))
					{
					$ev=$this->getCustomAttributeValue($eachkey->$mykeys[$i],$this->_data);
					$data .="<td>".JTEXT::_($ev)."</td>";

					}
					else if(is_object($eachkey))
					{
					$data .="<td>".JTEXT::_($mykeys[$i])."</td>";
					}
				}
				$data .="</tr>";
			}
		}
		else
		{

		foreach($this->_data as $key=>$val)
				{
					foreach($val as $ek=>$ev)
					{
						if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
						{
							$heads[$ek][]=$ev;
						}
						else
						{
							$deleteindex[]=$ev;

						}

					}
				}
				//echo "<pre>";
				//print_R($this->_data)
				$heads=array_reverse($heads);
				//$deleteindex=array_reverse($deleteindex);
				$mykeys=array_keys($heads);
				$data ="";
				$j=0;
				$heads=array_merge($heads,$eachcustomattr);
				$heads=array_filter($heads);

		     	foreach($heads as $ek=>$ev)
					{	$originalname[]=$ek;
						if($ek=="name")
						{
							$ek="State Name";
						}
						else if($ek=="distshp")
						{
							$ek="District Name";
						}
						else if($ek=="place_name")
						{
							$ek="Villages Name";
						}
						else if($ek=="UA_Name")
						{
							$ek="Urban Name";
						}
						else
						{
							$ek =$ek;
						}

						if(!empty($eachcustomattr[$ek]))
							{
							$ev=$this->getCustomAttributeValue($eachcustomattr[$ek],$this->_data);
							$rowclass="deletecustom";
							}
						else
						{
							$rowclass="deleterow";
						}
						if($j!=0)
						{
						$todisplay ="<td ><input type='checkbox' class='".$rowclass."' value='".$originalname[$j]."' /></td>";
						}
						else
						{
							$todisplay="<td >Delete</td>";
						}
						$data .="<tr id='".$originalname[$j]."'>".$todisplay."<td>".$ek."</td>";


						$i=0;

						foreach($ev as $k=>$v)
						{

							if($i==0)
							{
								if($j == 0)
								{
									$myid="id = '".$v."'";
									$myclass="class='".$ek." '";
									$deletedcolumn="<input type='checkbox' id='removecolumn' class ='removecolumn' value='".$originalname[$j]."`".$deleteindex[$k]."`".$v."' />";
								}
								else
								{
									$myid="id = ''";
									$myclass="";
									$deletedcolumn="";
								}


							}
							else
							{
								if($j==0)
								{
									$myid="id = '".$v."'";
								}
								else
								{
									$myid="id = ''";
								}
								$myclass="class = ''";

							}

							$data .="<td ".$myclass." ".$id.">".$deletedcolumn.JTEXT::_($v)."</td>";

						}
						$data .="</tr>";
						$j++;
					}

		}
		return $head.$data;

	}

	/**
	 *
	 */
	function getCustomAttributeValue($name,$data,$identifier=null){
		$session = JFactory::getSession();
		$db      = $this->getDbo();

		$returnable = array();
		$i          = 0;

		$name1 =str_replace("+","",$name);
		$name1 =str_replace("*","",$name1);
		$name1 =str_replace("-","",$name1);
		$name1 =str_replace("/","",$name1);
		$name1 =str_replace("%","",$name1);
		$name1 =str_replace("SIN(","",$name1);
		$name1 =str_replace("COS(","",$name1);
		$name1 =str_replace("TAN(","",$name1);
		$name1 =str_replace("LOG(","",$name1);
		$name1 =str_replace("LOG10(","",$name1);
		$name1 =str_replace("SQRT(","",$name1);
		$name1 =str_replace("EXP(","",$name1);
		$name1 =str_replace("^","",$name1);
		$name1 =str_replace("EXP(","",$name1);
		$name1 =str_replace("LN(","",$name1);
		$name1 =str_replace("PI(","",$name1);
		$name1 =str_replace(")","",$name1);
		$name1 =str_replace("(","",$name1);
		$name1 =str_replace(range(0,9),'',$name1);

		//$name1 = str_replace(",,",",",$name1);
		$chkval  = explode(" ",$name1);
		$chkval  = array_filter($chkval);
		$field   = implode(",",$chkval);

		$table          = $session->get("activetable");
		$searchvariable = $session->get("activenamevariable");

		//foreach($data as $ed)
		{
			$dupname    = $name;
			$identifier = explode("-",$identifier);

			$query = "SELECT ".$field." FROM ".$table." WHERE ".$searchvariable." LIKE '".$identifier[0]."'";
			$db->setQuery($query);
			try {
				$list  = $db->loadObjectList();
			} catch (Exception $e) {
				$list  = NULL;
			}

			$list = array_pop($list);

			//if($ed->name==$identifier)
			{
				foreach($list as $k => $v){
					//echo $dupname."<br /><br />";
					$dupname        = str_replace($k,$v,$dupname);
					$returnable[$i] = $dupname;
				}
			}
			$i++;
		}
		$i=0;

		//echo "<pre />";print_r($returnable);
		foreach($returnable as $reeval)
		{
			//echo 'aaa'.$reeval."<br />";
			$result="";
			if(strstr($reeval,"^")) {
				$cExplode = str_replace(" ","",$reeval);
				$cExplode = explode("^",$cExplode);
				$result   = pow($cExplode[0],$cExplode[1]);
				//@eval('$result = '.$formulaNew.';');
			}else if(strstr($reeval,"%")) {
				//$perExplode = str_replace("(","",$reeval);
				//$perExplode = str_replace(")","",$perExplode);
				$perExplode     = str_replace(" ","",$reeval);
				$perExplodeData = explode("%",$perExplode);
				$formulaNew     = "(".$perExplodeData[0].")/100";
				@eval('$result = '.$formulaNew.';');
			}else if(strstr($reeval,"LN(")){
				$lnexplode =explode('LN(',$reeval);
				$test      = explode(")",$lnexplode[1]);
				//$result  =pow(10,$test[0]);
				$result    =number_format(log($test[0]),5);
			}else if(strstr($reeval,"LOG(")){
				$lnexplode1 =explode('LOG(',$reeval);
				$test1      = explode(")",$lnexplode1[1]);
				//$result   =pow(10,$test[0]);
				$resultTmp  =log($test1[0],10);
				$result     = number_format($resultTmp,5);
			}else{
				//echo $reeval."<br />";
				@eval('$result = '.$reeval.';');
			}

			$returnables[$i]=  $result;
			$i++;
		}

		//echo $returnables[0];exit;
		return $returnables[0];
	}

	function getWorkspace(){
		return workspaceHelper::loadWorkspace();
	}

	function updateWorkspace(){
		return workspaceHelper::updateWorkspace();
	}

	function deleteWorkspace(){
		return workspaceHelper::deleteWorkspace();
	}

	function saveWorkspace(){
		return workspaceHelper::saveWorkspace();
	}

	function unsetDefault(){
		return workspaceHelper::unsetDefault();
	}

	function getSldToDataBase()
  	{
  		$session=JFactory::getSession();
  		$activeworkspace=$session->get('activeworkspace');
  		$sld=cssHelper::getLayerProperty($activeworkspace,1);
		echo $sld;
		exit;
  	}

  	function getthematicQueryFromDb(){
		$db      = $this->getDbo();
		$session = JFactory::getSession();

  		$activeworkspace=$session->get('activeworkspace');
		$query  = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		//cont..
		/*$customformulafromsession=$session->get('customformula',$attributes);
		$customformula=$formula.":".$from."->".$to."->".$color."|";
		$customformulafromsession = $customformulafromsession.$customformula;
		$session->set('customformula',$customformulafromsession);*/
  	}

  	function getSldLevel(){
  		$db      = $this->getDbo();
		$session = JFactory::getSession();

  		$activeworkspace=$session->get('activeworkspace');

		$query  = "SELECT level FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$activeworkspace."
			GROUP BY level";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$str   = "";
		$level = "0,1,2";
		foreach($result as $eachlevel){
			$level =str_replace($eachlevel->level.",","",$level);
		}
		return $level;
  	}

  	/**
  	 * A function called from view.
  	 */
	public function getBreadCrumb()
	{
		$session  =JFactory::getSession();
		$urban    = $session->get('urban');
		$villages = $session->get('villages_villages');
		$m_type   = $session->get('villagem_type');
		$district = $session->get('villagedistrict');
		$state    = $session->get('villagestate');
		$dataof   = $session->get('dataof');

		$statecount = explode(",", $state);
		$separator  = "<span class='ja-pathway-text'>&nbsp; &gt; &nbsp;</span>";//JHTML::_('image.site', 'arrow.png');
		$breadcrumb = "<div class='breadcrumb ' id='ja-pathway'><span class='ja-pathway-text'>Your Query :</span>";

		if(count($statecount) == 1){
			$breadcrumb .="<span class='ja-pathway-text'>State(".$state.")</span>".$separator;
			if($dataof == "District"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == $district){
						$selected = $eachdata->OGR_FID."&villagem_type=".$m_type;
						$name     = $eachdata->name;
						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}
						break;

					}else{

						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}

						if($district == "all"){
							$name     = "All";
							$selected = "all&villagem_type=".$m_type;
						}else{
							$name     = "Compare";
							$selected = "all&villagem_type=".$m_type;
						}
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>District(".$name.")</span>".$separator;
				//$breadcrumb .="<span class='ja-pathway-text'>Type(".ucfirst($m_typedisp).")</span>";
			}

			if($dataof == "Villages"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == $villages){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($villages == "all"){
							$name = "All";
						}else{
							$name = "Compare";
						}
						$selected = "all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Villages(".$name.")</span>";
			}

			if($dataof == "UA" || $dataof == "Urban"){
				$dataof = "UA";
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID==trim($urban)){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($urban=="all"){
							$name="All";
						}else{
							$name="Compare";
						}
						$selected="all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Urban(".$name.")</span>";
			}
			$selected=$district;

		}else{
			$breadcrumb .="<span class='ja-pathway-text'>State</span>".$separator;
			if($dataof=="District"){
				$breadcrumb .="<span class='ja-pathway-text'>District</span>".$separator;
				//$breadcrumb .="<span class='ja-pathway-text'>".ucfirst($m_type)."</span>";
			}

			if($dataof=="Villages"){
				$breadcrumb .="<span class='ja-pathway-text'>Villages</span>";
			}

			if($dataof=="UA"){
				$breadcrumb .="<span class='ja-pathway-text'>Urban</span>";
			}
			$selected=$district;
		}

		$ac .="<div class='modifystruct'><div class='ja-pathway-text' id='activeworkspacename' style='float: left;padding-right: 4px;font-weight: bold;font-size:12px;'></div>";
		$ac .="<a class='ja-pathway-text' href='index.php?option=com_mica&view=villagefront'>Modify</a></div>";
		$breadcrumb .="</div>";
		return $breadcrumb."~~~".$ac;
	}

	/**
	 * A function called from view.
	 */
	public function getcustomattrlib(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		// $activetable = $session->get("activetable");
		$activetable = "villages";

		$query = "SELECT name, attribute
			FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get("activeworkspace"))."
				AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
		$db->setQuery($query);
		$allcostomattr = $db->loadAssocList();

		$str = "";
		foreach($allcostomattr as $eachcustomattr){
			$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
			$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
		}
		$session->set('customattributelib',$str);
	}

	/**
	 * a function called from view.
	 */
  	public function getInitialGeometry(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$state = $session->get('villagestate');
		$state = explode(",",$state);
		$state = array_filter($state);
		$state = implode("','",$state);

		if($state != "all" && substr_count($state,",") == 0){
			//$where="1=1";
			//$query="select AsText(GROUP_CONCAT(india_state)) as poly from rail_state where ".$where;
			$query = "SELECT AsText((india_state)) as poly
				FROM ".$db->quoteName('rail_state')."
				WHERE ".$db->quoteName('name')." IN ('".$state."') ";
		}else{
			return 0;
		}

		$db->setQuery($query);
		$allvertex = $db->loadAssocList();

		$str       = "";
		$allvertex = array_reverse($allvertex);
		foreach($allvertex as $eachvertex){
			//$str[] = $eachvertex['AsText(india_state)'];
		 	return $eachvertex['poly'];
		}
		return implode(",", $str);
	}

	/**
	 *
	 */
	public function getPagination(){
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			$app = JFactory::getApplication();
		    jimport('joomla.html.pagination');
		    $this->_pagination = new JPagination(ceil(count($this->_data)/50), ( ($app->input->get("limitstart") != "") ? $app->input->get("limitstart") : 0), 1 );
		}
		return $this->_pagination;
	}

	/**
	 * A function called from view.
	 */
	public function getUnselectedDistrict(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$district = $session->get('villagedistrict');
		$explode_district = explode(",", $district);

		if (count($explode_district) > 0 && strlen($explode_district[0]) > 0 ) {
			$query    = "SELECT OGR_FID FROM ".$db->quoteName('india_information')."
				WHERE OGR_FID NOT IN (".$district.")";
			$db->setQuery($query);
			try {
				$return = $db->loadAssocList();
			} catch (Exception $e) {
				$return = NULL;
			}

			foreach($return as $each){
				$returnable[]=$each['OGR_FID'];
			}
			return implode(",",$returnable);
		}else{
			return '';
		}
	}

}
