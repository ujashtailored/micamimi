<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Base controller class for Mica.
 *
 * @since  1.5
 */
class MicaController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe URL parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController  This object to support chaining.
	 *
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Get the document object.
		$document = JFactory::getDocument();

		// Set the default view name and format from the Request.
		$vName   = $this->input->getCmd('view', 'micafront');
		$vFormat = $document->getType();
		$lName   = $this->input->getCmd('layout', 'default');

		if ($view = $this->getView($vName, $vFormat))
		{
			// Do any specific processing by view.
			switch ($vName)
			{
				case 'dashboard':
				case 'fullmap':
				case 'showresults':
					// The user is a guest, load the registration model and show the registration page.
					$model = $this->getModel($vName);
					break;
				case 'villageshowresults':
					// The user is a guest, load the registration model and show the registration page.
					$model = $this->getModel($vName);
					break;
				case 'villagefront':
					// The user is a guest, load the registration model and show the registration page.
					$model = $this->getModel($vName);
					break;
				case 'summeryresults':
					// The user is a guest, load the registration model and show the registration page.
					$model = $this->getModel($vName);
					break;
				case 'summeryfront':
					// The user is a guest, load the registration model and show the registration page.
					$model = $this->getModel($vName);
					break;

				// Handle the default views.
				default:
				case 'micafront':
					$model = $this->getModel('micafront');
					break;
			}

			// Push the model into the view (as default).
			$view->setModel($model, true);
			$view->setLayout($lName);

			// Push document object into the view.
			$view->document = $document;

			$view->display();
		}
	}
}
