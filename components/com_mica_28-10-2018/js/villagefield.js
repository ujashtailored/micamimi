var filterspeed_radio;
JQuery(document).ready(function(){
	JQuery('.distattrtypeselection').css({'display':'none'});
	JQuery('.districtspan,.urbanspan,.townspan').css({'display':'none'});
	JQuery('.group').css({'float':'left'});
	JQuery('.createneworkspace').css({'display':'none'});
	JQuery('.createnewworkspace').css({'display':'none'});

	/*if(typeof(choosenjq) != "undefined" && typeof(multiselect) != "undefined" ){
		choosenjq("#village_composite").multiselect({selectedList: 1,noneSelectedText: 'Composite Score'});
		choosenjq("#rating").multiselect({selectedList: 1,noneSelectedText: 'Market Potential Index'});
		choosenjq("#uachecked").multiselect({multiple: false,header: 'Please Select',noneSelectedText: 'Please Select',selectedList: 1});
	}*/

	if(typeof(usedlevel) != "undefined"){
		var ulevel=usedlevel.split(",");
		if(ulevel.length == 3){
			JQuery("#themeconent").css({"display":"none"});
		}
		getColorPallet();
	}

	getAttribute(9);

	if(typeof(preselected) != "undefined"){
		//JQuery('#dataof').change();
		displaycombo();
		getVariableList();
		//choosenjq("#village_composite").multiselect({selectedList: 1,noneSelectedText: 'Composite Score'});
		//choosenjq("#rating").multiselect({selectedList: 1,noneSelectedText: 'Market Potential Index'});
		// choosenjq("#m_type").multiselect({header: '&nbsp;&nbsp;Select Type',noneSelectedText: '&nbsp;&nbsp;Select Type',selectedList: 1});
		//choosenjq("#uachecked").multiselect({multiple: false,header: '&nbsp;&nbsp;Please Select',noneSelectedText: '&nbsp;&nbsp;Please Select',selectedList: 1});
	}
	var minval        = "";
	var maxval        = "";
	var colorselected = "";
	var edittheme     = 0;

	JQuery(".hideothers").css({"display":"none"});
	//JQuery('#attr').click();

	/*var singlequery = havingthematicquery.split(",");
	JQuery("#thematic_attribute option").each(function(){
		for(var i=0;i<=singlequery.length;i++){
			if(singlequery[i]==JQuery(this).val()){
				JQuery(this).attr("disabled",true);
			}
		}
	});*/
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villagefront.getsecondlevelvillage&villagedistrict="+JQuery("#villagedistrict").val(),
		method  : 'post',
		success : function(combos){
			var segment = combos.split("split");
			JQuery("#villagespan").html(segment[0]);
			//choosenjq("select").multiselect({selectedList: 1});
			var dataof = JQuery("#dataof").val();

			if(dataof == "ua" || dataof == "UA"){
				dataof="urban";
			}
		}
	});

});

function getfrontdistrict(val){
	//var val = JQuery("#villagestate").val();

	if(val == "" ){
		JQuery('.level2').css({'display':'none'});
		getAttribute(9);
		//document.getElementById('m_type').selectedIndex=0;
		JQuery(".distattrtypeselection").css({"display":"none"});
		return false;
	}
	else if(val=="all"){
		document.getElementById('villagedistrict').selectedIndex=0;
		document.getElementById('urban').selectedIndex=0;
		document.getElementById('villages_villages').selectedIndex=0;
		//document.getElementById('m_type').selectedIndex=0;
		JQuery('.districtspan,.urbanspan,.townspan,.distattrtypeselection').css({'display':'none'});
		getAttribute(9);
		return false;
	}
	else{
		getAttribute(9);
		//document.getElementById('m_type').selectedIndex=0;
		JQuery(".distattrtypeselection").css({"display":"none"});
	}

	JQuery('.distattrtypeselection').css({'display':'none'});

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villagefront.getsecondlevel&stat="+val+"&preselected="+preselecteddata,
		method  : 'post',
		success : function(combos){
			var segment = combos.split("split");
			JQuery(".districtlist").html(segment[0]);
			//JQuery("#urbanspan").html(segment[1]);
			//JQuery("#townspan").html(segment[2]);
			if(preselecteddata != ""){
				getAttribute(9);
			}

			//choosenjq("select").multiselect({selectedList: 1});
			//choosenjq("#villagedistrict").multiselect({multiple: false, header:false});
			var dataof       = JQuery("#dataof").val();
			/*var selectedtype = preselecteddataof.toLowerCase();
			if(selectedtype == "ua" || selectedtype == "UA"){
				selectedtype="urban";
			}*/

			if(dataof == "ua" || dataof == "UA"){
				dataof="urban";
			}
			$( "#loader").hide();
			/*if(typeof(preselecteddata)!="undefined" && dataof.toLowerCase()==selectedtype){
				JQuery('#'+selectedtype).val(preselecteddata).attr("select","selected");
				JQuery('#'+selectedtype).change();
				alert("in1");
				eval("get"+selectedtype(preselecteddata));
			}*/
		}
	});
}

function getdistrict(val){
	if(val == "" ){
		JQuery('.level2').css({'display':'none'});
		getAttribute(9);
		return false;
	}
	else if(val == "all"){
		JQuery('.level2').css({'display':'none'});
		getAttribute(9);
		return false;
	}
	else{
		getAttribute(9);
	}
	JQuery('.distattrtypeselection').css({'display':'none'});

	JQuery.ajax({
		url     : "index.php?option=com_mica&&task=villagefront.getsecondlevel&stat="+val,
		method  : 'post',
		success : function(combos){
			JQuery('.level2').css({'display':'block'});
			var segment = combos.split("split");
			JQuery("#districtspan").html(segment[0]);
			// choosenjq("select").chosen();
			//choosenjq("#villagedistrict").multiselect({multiple:false});
		}
	});
}

function displaycombo(value){
	if(JQuery("#villagestate").val()=="all"){
		alert("Please Select State First");
		document.getElementById('dataof').selectedIndex=0;
		document.getElementById("villagedistrict").selectedIndex=0;
		return false;
	}
	else{
		getfrontdistrict(JQuery("#villagestate").val());
	}

	if(value=="District"){
		JQuery(".districtspan").css({"display":"block"});
		JQuery(".townspan").css({"display":"none"});
		JQuery(".urbanspan").css({"display":"none"});
	}/*else if(value=="Villages"){
		JQuery(".districtspan").css({"display":"none"});
		JQuery(".townspan").css({"display":"block"});
		JQuery(".urbanspan").css({"display":"none"});
		JQuery(".distattrtypeselection").css({"display":"none"});
		document.getElementById("district").selectedIndex=0;
		document.getElementById('m_type').selectedIndex=0;
		JQuery(".distattrtypeselection").css({"display":"none"});
	}else if(value=="UA"){
		JQuery(".districtspan").css({"display":"none"});
		JQuery(".townspan").css({"display":"none"});
		document.getElementById('m_type').selectedIndex=0;
		JQuery(".distattrtypeselection").css({"display":"none"});
		JQuery(".urbanspan").css({"display":"block"});
		document.getElementById("district").selectedIndex=0;
	}else{
		JQuery(".districtspan").css({"display":"none"});
		JQuery(".townspan").css({"display":"none"});
		JQuery(".urbanspan").css({"display":"none"});
		JQuery(".distattrtypeselection").css({"display":"none"});
		document.getElementById('m_type').selectedIndex=0;
	}*/
}

function gettown(val){
	//JQuery('.htitle').css({'width':'150px'});
	JQuery('.distattrtypeselection').css({'display':'none'});
	JQuery('.districtspan').css({'display':'none'});
	JQuery('.urbanspan').css({'display':'none'});
	if(val == ""){
		JQuery('.districtspan').css({'display':'block'});
		JQuery('.urbanspan').css({'display':'block'});
	}
	getAttribute(9);
}

function geturban(val){
	JQuery('.htitle').css({'width':'150px'});
	JQuery('.distattrtypeselection').css({'display':'none'});
	if(val == ""){
		JQuery('.districtspan').css({'display':'block'});
		JQuery('.townspan').css({'display':'block'});
	}
	getAttribute(9);
}

function resetCombo(selectedid){
	//document.getElementById(selectedid).selectedIndex = 0;
}

function getdistrictattr(val){
	//alert(val);
	//var val=JQuery('.district_checkbox').val();
	//var val=JQuery("#villagedistrict").val();

	var val = JQuery('input[name=villagedistrict]:checked').val();

	if(val==""){
		JQuery('.townspan').css({'display':'block'});
		JQuery('.urbanspan').css({'display':'block'});
		JQuery('.distattrtypeselection').css({'display':'none'});
		resetCombo('district');
		getAttribute(9);
	}
	else{
		getAttribute(9);

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=villagefront.getsecondlevelvillage&villagedistrict="+val,
			method  : 'post',
			success : function(combos){
				var segment = combos.split("split");
				JQuery(".villagelist").html(segment[0]);
				//choosenjq("select").multiselect({selectedList: 1});
				var dataof = JQuery("#dataof").val();

				if(dataof == "ua" || dataof == "UA"){
					dataof="urban";
				}
			}
		});
	}
	//JQuery('.htitle').css({'width':'150px'});
}

function getvalidation(){
	var myurl = "";
	var zoom  = 5;
	if(document.getElementById('villagestate').value==''){
		return false;
	}
	else{
		myurl = "villagestate=";
		JQuery('#villagestate :selected').each(function(i, selected) {
			myurl +=JQuery(selected).val()+",";
			zoom=5;
		});

		myurl +="&villagedistrict=";
		JQuery('#villagedistrict :selected').each(function(i, selected) {
			myurl += JQuery(selected).val()+",";
			zoom  = 6;
		});
		myurl +="&villagesattributes=";

		JQuery('input.statetotal_attributes[type=checkbox]').each(function () {
			if(this.checked){
				myurl +=JQuery(this).val()+",";
			}
		});
		JQuery("#zoom").val(zoom);
		//window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=villageshowresults&Itemid=108";
		return false;
	}
}

function checkRadio(frmName,rbGroupName){
	var radios = document[frmName].elements[rbGroupName];
	for(var i=0;i<radios.length;i++){
		if(radios[i].checked){
	   		return true;
	  	}
	}
	return false;
}

function getVariableList(){
	var grplist = JQuery("#variablegrp").val();
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.getAttribute&zoom=9&type=&grplist="+grplist,
		method  : 'GET',
		async   : true,
		success : function(data){
			JQuery("#grplist").html(data);
			//choosenjq("#villagesattributes").multiselect({selectedList: 1, noneSelectedText: 'Select Variable '});
		}
	});
}

function getAttribute(javazoom,type){
	JQuery.ajax({
		url     :"index.php?option=com_mica&task=villageshowresults.getAttribute&zoom="+parseInt(javazoom)+"&type=",
		method  : 'GET',
		async   : true,
		success : function(data){
			JQuery("#statetotal").html(data);
			JQuery(".hideothers").css({"display":"none"});
			JQuery(".variablegrp").addClass("deactive");
			//choosenjq("#variablegrp").multiselect({selectedList: 1,noneSelectedText: 'Select Variable Group'});
			getVariableList();
		}
	});
}

function filterAttributes(value){
	var i = 0;

	JQuery(".statetotalunselectall").click();
	if(value == "Urban"){
		JQuery(".hideurban").css({"display":"block"});
		JQuery(".hiderural").css({"display":"none"});
		JQuery(".hidetotal").css({"display":"none"});
	}

	if(value == "Rural"){
		JQuery(".hideurban").css({"display":"none"});
		JQuery(".hiderural").css({"display":"block"});
		JQuery(".hidetotal").css({"display":"none"});
	}

	if(value == "Total"){
		JQuery(".hideurban").css({"display":"block"});
		JQuery(".hiderural").css({"display":"block"});
		JQuery(".hidetotal").css({"display":"block"});
	}
}

JQuery("#attr").live("click",function(){
	JQuery("#statetotal").toggle();
});

function changeWorkspace(value){
	if(value == 0){
		JQuery('.createneworkspace').css({'display':'block'});
		JQuery('.createnewworkspace').css({'display':''});
		JQuery('.thematiceditoption').css('display','block');
	}
	else{
		window.location = "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.loadWorkspace&workspaceid="+value;
	}
}

JQuery("#createworkspace").live("click",function(){
	var workspacename = JQuery("#new_w_txt").val();
	if(workspacename == ""){
		alert("Please Enter workspace name");
		return false;
	}
	window.location = "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.saveWorkspace&name="+workspacename;
});

JQuery("#updateworkspace").live("click",function(){
	var workspacename = JQuery("#new_w_txt").val();
	var workspaceid   = JQuery("#profile").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.updateWorkspace&name="+workspacename+"&workspaceid="+workspaceid,
		method  : 'GET',
		success : function(data){
			window.location = 'index.php?option=com_mica&view=villageshowresults&Itemid=108&msg=1';
		}
	});
});

JQuery("#deleteworkspace").live("click",function(){
	if(!confirm("Are you Sure to Delete Workspace?")){
		return false;
	}

	var workspaceid = JQuery("#profile").val();
	JQuery.ajax({
		url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.deleteWorkspace&workspaceid="+workspaceid,
		method  : 'GET',
		success : function(data){
			window.location = 'index.php?option=com_mica&view=villagefront&Itemid=100&msg=-1';
		}
	});
});

JQuery("#submit").live("click",function(){
	if(JQuery("#comparedata").val() == 1){
		var statecompare    = JQuery("#statecompare").val();
		var districtcompare = JQuery("#districtcompare").val();
		var towncompare     = JQuery("#towncompare").val();
		var urbancompare    = JQuery("#urbancompare").val();

		if(urbancompare != ""){
			if(urbancompare == "all"){
				alert("All UA already Selected");
				return false;
			}
			var urban=JQuery("#urban").val();
			if(urban == ""){
				alert("Please Select UA to compare");
				return false;
			}
			else if(urban == "all"){
				alert("You can not select All UA to compare");
				return false;
			}
		}
		else if(towncompare != ""){
			if(towncompare == "all"){
				alert("All Towns already Selected");
				return false;
			}
			var villages=JQuery("#villages_villages").val();
			if(villages == ""){
				alert("Please Select Villages to compare");
				return false;
			}
			else if(villages == "all"){
				alert("You can not select All Villages to compare");
				return false;
			}
		}
		else if(districtcompare!="") {
			if(districtcompare=="all") {
				alert("All Districts already Selected");
				return false;
			}
			var dist = JQuery("#villagedistrict").val();
			if(dist == ""){
				alert("Please Select District to compare");
				return false;
			}
			else if(dist=="all") {
				alert("You can not select All District to compare");
				return false;
			}
		}
		else if(statecompare!="") {
			if(statecompare=="all") {
				alert("All States already Selected");
				return false;
			}
			var stateval=JQuery("#villagestate").val();
			if (stateval=="") {
				alert("Please Select State to compare");
				return false;
			}
			else if(stateval=="all") {
				alert("You can not select All State to compare");
				return false;
			}
		}
		return true;
	}
	else {
		var resetmtype      = 0;
		var checked         = 0;
		var dataof          = JQuery("#dataof").val();
		var districtcompare = JQuery("#villagedistrict").val();
		if (dataof=="District") {
			if (JQuery("#villagedistrict").val()=="") {
				alert("Please Select District First");
				return false;
			}
		}
		else if (dataof=="UA") {
			if (JQuery("#urban").val()=="") {
				alert("Please Select UA First");
				return false;
			}
		}
		else if (dataof=="Villages") {
			if (JQuery("#villages_villages").val()=="") {
				alert("Please Select Villages First");
				return false;
			}
		}

		JQuery("input:checkbox:checked").each(function() {
			var chk = JQuery(this).attr("checked");
			if (chk=="checked") {
				checked=1;
			}
		});

		if (checked==0) {
			alert("Please select variable first");
			return false;
		}
		else {
			if (typeof(districtcompare)=="undefined" || districtcompare=="") {
				//JQuery("#m_type").remove();
				return true;
			}
		}
	}
});

function checkAllgrp(grpid){
	JQuery('.statetotal_attributes'+grpid).each(function(index){
		JQuery(this).attr('checked','checked');
	});
}

function uncheckAllgrp(grpid){
	JQuery('.statetotal_attributes'+grpid).each(function(index){
		JQuery(this).removeAttr('checked','');
	});
}

function getMinmaxVariable(value){
	JQuery(".minmaxdisplay").html("Please Wait...");
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.getMinMax&value="+encodeURIComponent(value),
		method  : 'GET',
		success : function(data){
			var segment = data.split(",");
			minval = segment[0];
			maxval = segment[1];
			JQuery(".minmaxdisplay").html("<b>MIN :</b>"+segment[0]+"<b><br/>MAX :</b>"+segment[1]+"");
			JQuery("#maxvalh").val(segment[1]);
			JQuery("#minvalh").val(segment[0]);
		}
	});
}

JQuery("#no_of_interval").live("keyup",function(e){
	var str="";
	if(e.which >= 48 && e.which <= 57 || e.which >= 96 && e.which <= 105){
		if(typeof(edittheme)=="undefined"){
			edittheme=0;
		}
		createTable(edittheme);
	}
});

function createTable(edittheme){
	var maxval      = JQuery("#maxvalh").val();
	var minval      = JQuery("#minvalh").val();
	var level       = JQuery("#level").val();
	var str         = "";
	var diff        = maxval-minval;
	var max         = minval;
	var disp        = 0;
	var setinterval = diff/(JQuery("#no_of_interval").val());
		setinterval = setinterval.toFixed(2);
	start = minval;
	if(JQuery("#no_of_interval").val()>5){
		alert("Interval must be less then 5");
		return false;
	}
	var colorselected="";
	if (edittheme!=1 && usedlevel.indexOf("0")==-1) {
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
		JQuery(".colorhide").css({"display":""});
		colorselected=1;
	}
	else if(edittheme==1 && level=="0") {
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Color</th></tr></thead>';
		JQuery(".colorhide").css({"display":""});colorselected=1;
	}
	else {
		str +='<thead><tr><th align="center">From</th><th align="center">To</th><th align="center">Pin</th></tr></thead>';
		JQuery(".colorhide").css({"display":"none"});colorselected=0;
	}

	for (var i=1;i<=JQuery("#no_of_interval").val();i++) {
		if ((edittheme!=1 && usedlevel.indexOf("0")==-1)) {
			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
			start = end;
		}
		else if(edittheme!=1 && usedlevel.indexOf("0")!=-1 && level!="0") {
			if (usedlevel.indexOf("1")==-1) {
				level=1;JQuery("#level").val(1);
			}
			else {
				level=2; JQuery("#level").val(2);
			}
			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
			start = end;
		}
		else if(edittheme==1 && level!="0") {
			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"><img src="'+siteurl+'/components/com_mica/maps/img/layer'+level+'/pin'+i+'.png" /></td>	</tr>';
			start = end;
		}
		else if(edittheme==1 && usedlevel.indexOf("0")!=-1 && level=="0") {
			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ><td id="color'+i+'"></td>	</tr>';
			start = end;
		}
		else{
			end   = (parseFloat(setinterval)+parseFloat(start)).toFixed(2);
			str   += '<tr><td><input type="text" name="from1" id="from'+i+'" class="inputbox inputboxrange" value="'+start+'"></td><td><input type="text" name="to[]" id="to'+i+'" class="inputbox inputboxrange" value="'+end+'"></td ></tr>';
			start = end;
		}
	}

	JQuery("#displayinterval").html(str);
	if(colorselected == 1){
		JQuery('.simpleColorChooser').click();
	}
	//return str;
}

JQuery('.simpleColorChooser').live("click",function(){
	var value = JQuery('.simple_color').val();
		value = value.replace("#","");
	var steps = JQuery("#no_of_interval").val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.getColorGradiant&value="+encodeURIComponent(value)+"&steps="+parseInt(steps++),
		method  : 'GET',
		success : function(data){
			colorselected = data;
			var segment = data.split(",");
				segment = segment.reverse();
			for(var i=1;i<=segment.length;i++){
				JQuery("#color"+(i)).fadeIn(1000,JQuery("#color"+(i)).css({"background-color":"#"+segment[i]}));
			}
		}
	});
});

function getColorPallet(){
	JQuery('.simple_color').simpleColor({
		cellWidth   : 9,
		cellHeight  : 9,
		border      : '1px solid #333333',
		buttonClass : 'colorpickerbutton'
	});
}

JQuery("#savesld").live("click",function(){
	var formula     = JQuery("#thematic_attribute").val();
	//var condition = JQuery("#condition").val();
	var limit       = JQuery("#no_of_interval").val();
	var level       = JQuery("#level").val();
	if(formula=="") {
		alert("Please Select Variable");
		return false;
	}
	else if(limit=="") {
		alert("Please Select Interval");
		return false;
	}

	var from  ="";
	var to    ="";
	var color ="";
	for(var i=1;i<=limit;i++) {
		from +=JQuery("#from"+i).val()+",";
		to   +=JQuery("#to"+i).val()+",";

		if(level=="0" || level=="") {
			color +=rgb2hex(JQuery("#color"+i).css("background-color"))+",";
		}
		else {
			color +=i+",";
		}
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.addthematicQueryToSession&formula="+encodeURIComponent(formula)+"&from="+from+"&to="+to+"&color="+color+"&level="+level,
		method  : 'GET',
		success : function(data){
			window.location.href  =window.location;
		}
	});
});

JQuery(".deletegrp").live("click",function(){
	var level    = JQuery(this).attr("class");
	var getlevel = level.split(" ");
	var getdelid = JQuery(this).attr("id");
	var segment  = getdelid.split("del_");

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.deletethematicQueryToSession&formula="+encodeURIComponent(segment[1])+"&level="+getlevel[1],
		method  : 'GET',
		success : function(data){
			window.location = '';
		}
	});
});

function rgb2hex(rgb) {
	if (typeof(rgb)=="undefined") {
		return "ffffff";
	}

    if (rgb.search("rgb") == -1){
        return rgb;
    }
    else {
		rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
		return  hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
}

JQuery(".edittheme").live("click",function(){
	edittheme=1;
	var cnt = JQuery("#themeconent").html();

	JQuery("#themeconent").html("");
	JQuery(".themeconent").html(cnt);
	JQuery('.simpleColorContainer').remove();

	getColorPallet();

	var classname = JQuery(this).attr("class");
	var myid      = this.id;
	var getcount  = classname.split(" ");
	var seg       = myid.split("__");
	//JQuery("#thematicquerypopup").click();
	JQuery("#thematic_attribute").val(seg[0]);
	JQuery("#level").val(seg[1]);

	JQuery("#thematic_attribute").change();
	JQuery("#no_of_interval").val(getcount[1]);
	createTable(edittheme);
	JQuery("#no_of_interval").val(getcount[1]);
	JQuery(".range_"+seg[1]).each(function(i){
		i++;
		rangeid     = (JQuery(this).attr("id"));
		var mylimit = rangeid.split("-");
		JQuery("#from"+i).val(mylimit[0]);
		JQuery("#to"+i).val(mylimit[1]);
	});

	var endcolor = "";
	if(seg[1] == "0"){
		for(var i=1;i<=getcount[1];i++){
			JQuery("#color"+(i)).css({"background-color":""+JQuery(".col_"+i).css("background-color")});
			if(getcount[1] == i){
				JQuery(".simpleColorDisplay").css({"background-color":JQuery(".col_"+i).css("background-color")});
			}
			//rangeid=JQuery(".range"+i).attr("id");
		}
	}
	//	JQuery("#thematicquerypopup").click();
	thematicquerypopup();
});

function thematicquerypopup(){
	if (totalthemecount==3 && (typeof(edittheme)=="undefined" || (edittheme)==0)) {
		alert("You Can Select maximum 3 Thematic Query for Single Workspace");
		return false;
	}
	else {
		//document.getElementById('light2').style.display='block';
		//document.getElementById('fade').style.display='block';
		JQuery("#light2").fadeIn();
		JQuery("#fade").fadeIn();
	}
}

JQuery("#updatecustom").live("click",function(){
	var new_name   = JQuery('#new_name').val();
	var oldattrval = JQuery('#oldattrval').val();
	//alert(new_name);
	//alert(oldattrval);
	if(JQuery("#new_name").val()==""){
		alert("Please Select Custom Variable First!!");
		return false;
	}

	if(!validateFormula()){
		return false;
	}

	var attributevale=JQuery('textarea#custom_attribute').text();
	//alert(attributevale);
	JQuery.ajax({
		url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.updateCustomAttr&attrname="+new_name+"&attributevale="+encodeURIComponent(attributevale)+"&oldattrval="+encodeURIComponent(oldattrval),
		method  : 'GET',
		success : function(data){
			//JQuery("#closeextra").click();
			//reloadCustomAttr();
			window.location='index.php?option=com_mica&view=villageshowresults&Itemid=108';
		}
	});
});

JQuery("#deletevariable").live("click",function(){
	var new_name      = JQuery('.customedit').find(":selected").text();
	var attributevale = JQuery('textarea#custom_attribute').text();
	if (new_name == "" && attributevale == "") {
		alert("Please Select Custom Variable");
		return false;
	}

	JQuery.ajax({
		url     : "index.php?option=com_mica&view=villageshowresults&task=villageshowresults.deleteCustomAttr&attrname="+new_name+"&attributevale="+attributevale,
		method  : 'GET',
		success : function(data){
			//alert(data);
			window.location='index.php?option=com_mica&view=villageshowresults&Itemid=108';
			//window.location='index.php?option=com_mica&view=villageshowresults&Itemid=108';
		}
	});
});

function closePopup(){
	edittheme = 0;
	document.getElementById('light2').style.display = 'none';
	document.getElementById('fade').style.display   = 'none';
	JQuery("#displayinterval").html("");

	//JQuery("#thematic_attribute").val(0).attr("selected");
	document.getElementById("thematic_attribute").selectedIndex = 0;
	JQuery("#no_of_interval").val("");
	JQuery(".minmaxdisplay").html("");
	if(JQuery(".themeconent").html().length>10){
		JQuery("#themeconent").html(JQuery(".themeconent").html());
		JQuery('.simpleColorContainer').remove();
		getColorPallet();
		JQuery(".simpleColorDisplay").css({"background-color":"#FFFFCC"});
	}
}

function validateFormula(){
	var assignformula = "";
	var formula       = JQuery('textarea#custom_attribute').text();

	JQuery("#avail_attribute option").each(function(){
		//alert(JQuery(this).val());
		//alert(formula);
		var myclass=JQuery(this).parent().attr("class");
		if(JQuery(this).val()==formula.trim() && myclass!="defaultvariablelib"){
			assignformula ="You have already assign '"+formula+"' value to '"+JQuery(this).val()+"' Variable";
		}
	});

	if(assignformula!=""){
		alert(assignformula);
		return false;
	}
	return true;
}

function downloadMapPdf(){
	//alert(tomcaturl);
	var start         = map.layers;
	var selectedlayer = "";

	if(javazoom==5 || javazoom==6) {
		selectedlayer ="india:rail_state";
	}
	else if(javazoom==7) {
		selectedlayer = "india:india_information";
	}
	else if(javazoom==8) {
		selectedlayer = "india:jos_mica_urban_agglomeration";
	}
	else {
		selectedlayer = "india:villages";
	}

	var baselayer     = "india:rail_state";
	var buildrequest  = "";
	var buildrequest1 = "";
	for (x in start) {
		for (y in start[x].params) {
			if (start[x].params.LAYER==selectedlayer) {
				if (y=="FORMAT") {
					buildrequest1 += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
				}
				else {
					buildrequest += (y+"EQT"+encodeURIComponent(start[x].params[y]))+"AND";
				}
			}
		}
	}

	buildrequest=buildrequest+"FORMATEQTimage/png";
	buildrequest1=buildrequest1+"FORMATEQTimage/png";
	//alert(tomcaturl+"?"+buildrequest+"&BBOX="+map.getExtent().toBBOX()+"&WIDTH=800&HEIGHT=600");
	var finalurl = buildrequest+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
	var finalurl1 = buildrequest1+encodeURIComponent("ANDBBOXEQT"+map.getExtent().toBBOX()+"ANDWIDTHEQT925ANDHEIGHTEQT650");
	window.open("index.php?option=com_mica&task=villageshowresults.exportMap&mapparameter="+finalurl+"&baselayer="+finalurl1);
}

function exportMap() {
	// set download url (toDataURL() requires the use of a proxy)
    // OpenLayers.Util.getElement("downloadLink").href = map.toDataURL();
}

//var previoushtml="";
function exportImage(name) {
    // exporting
	var store       = '0'; // '1' to store the image on the server, '0' to output on browser
	//var name      = '001'; // name of the image
	var imgtype     = 'jpeg'; // choose among 'png', 'jpeg', 'jpg', 'gif'
	var opt         = 'index.php?option=com_mica&task=villageshowresults.amExport&store='+store+'&name='+name+'&imgtype='+imgtype;
	var flashMovie  = document.getElementById('chartdiv');
	// previoushtml =JQuery("#exportchartbutton").html();//"Please Wait..."

	JQuery("#exportchartbutton").html("Please Wait...");//"Please Wait..."
	data=  flashMovie.exportImage(opt);
	flashMovie.amReturnImageData("chartdiv",data);
}

function amReturnImageData(chartidm,data){
	var onclick      = "exportImage(JQuery('#chartype option:selected').text())";
	var button       = '<input type="button" name="downloadchart" onclick="'+onclick+'" class="button" value="Export">';
	var previoushtml = JQuery("#exportchartbutton").html(button);
}

function downloadAction(){
	window.location='index.php?option=com_mica&task=villageshowresults.exportexcel';
	//	var table=JQuery("#alldata").html();
	//	JQuery.ajax({
	//		type:"POST",
	//		data : "table="+encodeURIComponent(table),
	//		url:"index.php?option=com_mica&view=villageshowresults&task=exportexcel",
	//		success:function(data)
	//		{
	//		window.open(data);
	//
	//		}
	//	});
}

JQuery(".variablegrp").live("click",function(){
	var myid = JQuery(this).attr("id");
	var dis  = JQuery('.'+myid).css("display");

	JQuery(".hideothers").css({"display":"none"});
	JQuery('.variablegrp').addClass("deactive");
	JQuery(this).addClass("active");
	JQuery(this).removeClass("deactive");

	if (dis=="block") {
		JQuery('.'+myid).css("display","none");
		JQuery(this).addClass("deactive");
		JQuery(this).removeClass("active");
	}
	else {
		JQuery('.'+myid).css("display","block");
		JQuery(this).addClass("active");
		JQuery(this).removeClass("deactive");
	}
});

function preloader(){
	var preloaderstr="<div id='facebook' ><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><div id='block_4' class='facebook_block'></div><div id='block_5' class='facebook_block'></div><div id='block_6' class='facebook_block'></div></div>";
}

JQuery(".hovertext").live("mouseover",function(e){
	var allclass  =JQuery(this).attr("class");
	var segment   =allclass.replace("hovertext ","");
	var x         = e.pageX - this.offsetLeft;
	var y         = e.pageY - this.offsetTop;
	var popuphtml ="<div  id='popupattr' style='position: absolute;z-index: 15000;background-color: #FFE900;border: 1px solid gray;padding:5px;'>"+segment+"</div>";
	JQuery(this).append(popuphtml);
});

JQuery(".hovertext").live("click",function(e){
	JQuery(this).parent().prev().find("input").prop("checked",true);//,true);
});

JQuery(".hovertext").live("mouseout",function(e){
	JQuery("#popupattr").remove();
});

JQuery(".customvariablecheckbox").live("click",function(){
	var check = (JQuery(this).attr("checked"));

	if (check == "checked") {
		JQuery("#new_name").val(JQuery(this).attr("id"));
		JQuery("#custom_attribute").text(JQuery(this).val());
		JQuery("#save").click();
		//addCustomVariable(JQuery(this).val(),JQuery(this).attr("id"));
	}
	else {
		window.location = "index.php?option=com_mica&task=villageshowresults.deleteattribute&attr="+JQuery(this).attr("id");
	}
});

/*function fixheader()
{
	var tableOffset = JQuery("#datatable").offset().top;
	var $header = JQuery(".firsttableheader,.secondtableheader,.thirdtableheader").clone();
	var $fixedHeader = JQuery("#header-fixed").append($header);

	JQuery("#datatable").scroll(function() {
	    var offset = $(this).scrollTop();

	    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
	        $fixedHeader.show();
	    }
	    else if (offset < tableOffset) {
	        $fixedHeader.hide();
	    }
	});​
}*/

JQuery(document).ready(function(){
	JQuery("li #menu100").parent().attr("class","active");
});

function toggleCustomAction(ele, action){
	if(action != ""){
		//JQuery(ele).prev().attr("checked",action);
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("checked",action);
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').click();
	}else{
		//JQuery(ele).prev().removeAttr("checked");
		JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').removeAttr("checked");
		var names = new Array();
		names.push(JQuery(ele).parent().siblings('.customvariable_name').find('.customvariablecheckbox').attr("id"));
		window.location="index.php?option=com_mica&task=villageshowresults.deleteCustomVariableFromLib&attrname="+names;
	}
	//JQuery(ele).prev().click();
}

JQuery(".customedit").live("click", function(){
	JQuery('#save').attr("id","updatecustom");
	JQuery('#updatecustom').attr("onclick","javascript:void(0)");
	JQuery('#updatecustom').attr("value","Update");

	JQuery('#new_name').val(JQuery(this).attr("id"));
	JQuery('#new_name').attr('disabled', true);
	JQuery('.aa').html("Edit");

	oldattrval=JQuery(this).attr("value");
	JQuery('#oldattrval').val(oldattrval);
	JQuery('textarea#custom_attribute').text(JQuery(this).attr("value"));
	lastchar = '';
	//moverightarea();
	//JQuery('#new_name').val(JQuery(this).prev().attr("id"));
	//JQuery('#custom_attribute').val(JQuery(this).prev().val());
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block';
});

JQuery("#closeextra").live("click",function(){
	JQuery('#updatecustom').attr("id","save");
	JQuery('#save').attr("onclick","checkfilledvalue();");
	JQuery('#save').attr("value","Save");
	JQuery('#new_name').val("");
	JQuery('#new_name').attr('disabled', false);
	JQuery('.aa').html("Add New");
});

JQuery(".deletecustomvariable").live("click",function(){
	var names = new Array();
	var i     = 0;
	JQuery(".dcv:checked").each(function(i){
		names.push(JQuery(this).attr("id"));
	});
	window.location="index.php?option=com_mica&task=villageshowresults.deleteCustomVariableFromLib&attrname="+names;
});

JQuery("#fullscreen, #fullscreen1").live("click",function(){
	var fromthematic  =	JQuery("#fullscreen").attr("fromthematic");
	/*JQuery("#map").addClass("fullscreen");
	JQuery(this).css({"display":"none"});
	JQuery("#fullscreenoff").fadeIn();
	JQuery("#map").css({"width":JQuery(window).width()-20,"height":JQuery(window).height()-20});
	map.updateSize();*/

	if (fromthematic==1) {
		popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component&fromthematic=1', 'MapWin');
	}
	else {
		popped = open(siteurl+'index.php?option=com_mica&view=fullmap&Itemid=108&tmpl=component', 'MapWin');
	}
	popped.document.body.innerHTML = "<div id='map' style='height:"+JQuery(window).height()+"px;width:"+JQuery(window).width()+"px;'></div>";
});

JQuery(".fullscreeniconoff").live("click",function(){
	JQuery("#fullscreentable").removeClass("fullscreentable");
	JQuery("#matrixclose").remove();
	JQuery(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input type='button' value='Full Screen' name='fullscreen' class='frontbutton' style='margin-right: 3px;'></td></tr></table>");

	JQuery(this).removeClass("fullscreeniconoff");
	JQuery(this).addClass("fullscreenicon");
	JQuery("#fade").css({"display":"none"});
	JQuery("#tablescroll").removeAttr("width");
	JQuery("#tablescroll").removeAttr("overflow");
	JQuery("#tablescroll").removeAttr("height");
});

JQuery(".fullscreenicon").live("click",function(){
	JQuery("#fullscreentable").addClass("fullscreentable");
	JQuery(this).html("<table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'></td></tr></table>");
	JQuery(this).addClass("fullscreeniconoff");
	JQuery(this).removeClass("fullscreenicon");
	var toappend='<div class="divclose" id="matrixclose" style="text-align:right;"><a href="javascript:void(0);" onclick="JQuery(\'.fullscreeniconoff\').click();"><img src="'+siteurl+'/media/system/images/closebox.jpeg" alt="X"></a></div>';
	var html=JQuery("#fullscreentable").html();
	JQuery("#fullscreentable").html(toappend+"<div id='tablescroll'>"+html+"</div>");
	JQuery("#fade").css({"display":"block"});
	JQuery("#tablescroll").css({"width":"100%"});
	JQuery("#tablescroll").css({"height":"100%"});
	JQuery("#tablescroll").css({"overflow":"auto"});
});

JQuery(".filterspeed").live("click",function(){
	var selection     =JQuery(this).val();
	filterspeed_radio = selection;
	if (selection == "0") {
		JQuery("#speed_variable").attr("multiple","true");
		JQuery("#speed_variable").removeClass("inputbox");

		JQuery("#speed_variable").removeClass("chzn-done");
		JQuery("#speed_region").removeClass("chzn-done");

		JQuery("#speed_region").removeAttr("multiple");
		JQuery("#speed_variable_chzn").remove();
		JQuery("#speed_region_chzn").remove();
		//choosenjq("#speed_region").chosen({max_selected_options: 1});
		//choosenjq("#speed_variable").chosen({max_selected_options: 5});
		JQuery("#speed_region").addClass("inputbox");
	}
	else {
		JQuery("#speed_variable").removeAttr("multiple");
		JQuery("#speed_variable").addClass("inputbox");
		JQuery("#speed_region").attr("multiple","true");

		JQuery("#speed_variable").removeClass("chzn-done");
		JQuery("#speed_region").removeClass("chzn-done");

		JQuery("#speed_variable_chzn").remove();
		JQuery("#speed_region_chzn").remove();

		JQuery("#speed_region").removeClass("inputbox");
		//choosenjq("#speed_region").chosen({max_selected_options: 5});
		//choosenjq("#speed_variable").chosen({max_selected_options: 1});
	}

	JQuery(".speed").css({"display":"block"});
	//choosenjq("#speed_variable").val('').trigger("liszt:updated");
	//choosenjq("#speed_region").val('').trigger("liszt:updated");
});

JQuery("#showspeed").live("click",function(){
	var speedvar    =new Array();
	var speedregion =new Array();
	if (typeof(JQuery("#speed_variable").attr("multiple")) == "string") {
		JQuery("#speed_variable").each(function(){
			speedvar.push(encodeURIComponent(JQuery(this).val()));
		});
		speedregion.push(encodeURIComponent(JQuery("#speed_region").val()));
	}
	else {
		speedvar.push(encodeURIComponent(JQuery("#speed_variable").val()));
		JQuery("#speed_region").each(function(){
			speedregion.push(encodeURIComponent(JQuery(this).val()));
		});
	}
	//var filterspeed = JQuery('input:radio[name=filter]:checked]').val();

	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.getsmeter&region="+speedregion+"&speedvar="+speedvar+"&filterspeed="+filterspeed_radio,
		type    : 'GET',
		success : function(data){
			JQuery("#spedometer_region").html("");
			JQuery("#spedometer_region").html(data);
			initspeed();
			JQuery("#speedfiltershow").css({"display":"block"});
			JQuery(".sfilter").css({"display":"none"});
			JQuery("#spedometer_region").css({"width":"915px"});
			JQuery("#spedometer_region").css({"overflow":"auto"});
		}
	});
});

JQuery("#speedfiltershow").live("click",function(){
	JQuery("#speedfiltershow").css({"display":"none"});
	JQuery(".sfilter").css({"display":"block"});
	JQuery("#spedometer_region").css({"width":"681px"});
	JQuery("#spedometer_region").css({"overflow":"auto"});
});

JQuery("#downmatrix").live("click",function(){
	window.location.href="index.php?option=com_mica&task=villageshowresults.downloadMatrix&rand="+Math.floor(Math.random()*100000);
});

JQuery("#matrix").live("click",function(){
	if (JQuery("#fullscreentable").html().length < 100) {
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=villageshowresults.getMaxForMatrix",
			method  : 'POST',
			success : function(data){
				JQuery("#fullscreentable").html(data);
			}
		});
	}
});

JQuery("#speedometer").live("click",function(){
	JQuery.ajax({
		url    :"index.php?option=com_mica&task=villageshowresults.speedometer",
		method : 'POST',
		success:function(data){
			JQuery("#speed_region").html(data);
		}
	});
});

JQuery(document).ready(function(){

	var graphVillages = JQuery("#villages_villages").val();
	JQuery.ajax({
		url     : "index.php?option=com_mica&task=villageshowresults.getVillagelist",
		type    : 'POST',
		data    : "villages_villages="+graphVillages,
		success : function(data){
			JQuery("#selectdistrictgraph").html(data);
		}
	});

	JQuery("#showchart").click(function(event) {
		var checkedelements = [];
		var checkedvillages = [];
		var checkevar       = [];

		JQuery("#selectdistrictgraph").find(".districtchecked").each(function(){
			if (JQuery(this).attr("checked")) {
				checkedelements.push(this.val);
				checkedvillages.push(JQuery(this).val());
			}
		});

		JQuery("#light1007").find(".variablechecked").each(function(){
			if (JQuery(this).attr("checked")) {
				checkedelements.push(JQuery(this).val());
				checkevar.push(JQuery(this).val());
			}
		});

		if (checkedelements.length >15) {
			alert("Variable + District Total Should be less then 15");
			return false;
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=villageshowresults.getGraph",
			type    : 'POST',
			data    : "villages_villages="+checkedvillages+"&attr="+checkevar,
			success : function(data){
				var segments = data.split("<->");
				datastr    = segments[0];
				grpsetting = segments[1];
				JQuery("#chartype").change();
			}
		});
	});
});