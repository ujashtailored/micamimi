<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

/**
 * MICA Front controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerMicafront extends MicaController
{
	//Custom Constructor
	var $tomcat_url             = "";
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";

	function __construct($default = array()){
		parent::__construct($default);

		$this->_table_prefix = '#__mica_';
		//$u    = JURI::getInstance( JURI::base() );
	 	//$this->tomcat_url="http://".$u->_host."/tomcat/geoserver/india/wms";
		$this->tomcat_url = "http://http://162.144.250.77:8080/geoserver/india/wms";
		DEFINE('TOMCAT_URL',$this->tomcat_url);
	}

	/*function getsecondlevel(){
		$districtsearchvariable=$this->districtsearchvariable;
		$stat = $this->input->get('stat','');
		$session=JFactory::getSession();
		$session->set('state',$stat);

		$mystates=explode(",",$stat);

		$db = JFactory::getDBO();
		foreach($mystates as $eachstate)
		{
			if($stat){
				$query = 'SELECT '.$districtsearchvariable.',distshp FROM india_information WHERE state in (select state from india_information where OGR_FID ='.$eachstate.')';
				$db->setQuery($query);
				$district_data[$eachstate] = $db->loadObjectList();
			}else{
				$district_data[$eachstate] = array();
			}
		}
		$returndata = '';
		$returndata .= "<span style='float:left;width:100%;'>";
		foreach($district_data as $key=>$val)
		{	$returndata .= "<span style='float:left;width:".(100/count($district_data))."%;'>";
			if(count($val)){
				$returndata .= '<select name="district" id="district" class="inputbox-mainscr" onchange="getdistrictattr(this.value)"  multiple >
							   	';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option>
			<option value="all">'.JText::_('ALL').'</option>';
				for($i=0;$i<count($val);$i++){
					$returndata .= '<option value="'.$val[$i]->$districtsearchvariable.'">'.$val[$i]->distshp.'</option>';
				}

				$returndata .= '</select>';
			}else{
				$returndata .= '<select name="district" id="district" class="inputbox-mainscr" onchange="getdistrict(this.value)">

							   	</select>';
			}
		$returndata .= "</span>";
		}
		$returndata .= "</span>";
		$getUrbanAgglormationNames=$this->getUrbanAgglormationNamesByStateNames($district_data);
		//$getUATownNamesByStateNames=$this->getUATownNamesByStateNames($mystates);
		 echo $returndata."split".$getUrbanAgglormationNames."split".$getUATownNamesByStateNames;
		exit;
	}
	function getUrbanAgglormationNamesByStateNames($state)
	{
		$urbansearchvariable=$this->urbansearchvariable;
		$db = JFactory::getDBO();

		foreach($state as $eachstate)
		{
			if($eachstate){
				$query = 'SELECT '.$urbansearchvariable.',place_name FROM jos_mica_urban_agglomeration WHERE place_name like "%'.$eachstate[0]->distshp.'%"';
				$db->setQuery($query);
				$urban_data[$eachstate] = $db->loadObjectList();
			}else{
				$urban_data[$eachstate] = array();
			}
		}
			$returndata = '';
			$returndata .= "<span style='float:left;width:100%;'>";
		foreach($urban_data as $key=>$val)
		{$returndata .= "<span style='float:left;width:".(100/count($district_data))."%;'>";
			if(count($val)){
				$returndata .= '<select name="urban" id="urban" class="inputbox-mainscr" onchange="geturban(this.value)" multiple>
							   	';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';
				for($i=0;$i<count($val);$i++){
					$returndata .= '<option value="'.$val[$i]->$urbansearchvariable.'">'.$val[$i]->place_name.'</option>';
				}

				$returndata .= '</select>';
			}else{
				$returndata .= '<select name="urban" id="urban" class="inputbox-mainscr" >

							   	</select>';
			}
		$returndata .= "</span>";
		}
		$returndata .= "</span>";
		return $returndata;
	}*/


	/**
	 * A task to return data for AJAX.
	 */
	public function getsecondlevel(){
		$districtsearchvariable = $this->districtsearchvariable;
		$stat                   = $this->input->get('stat','','raw');
		//$preselected          = $this->input->get('preselected','');

		$stat = explode(",",$stat);

		$session     = JFactory::getSession();
		$preselected = $session->get("district");
		$preselected = explode(",",$preselected);
		foreach($stat as $eachstat){
			$stats[] = base64_decode($eachstat);
		}

		$stat = implode(",",$stats);
		$stat = str_replace(",","','",$stat);
		$stat = "'".$stat."'";

		$db = JFactory::getDBO();
		if($stat){
			$query = "SELECT ".$districtsearchvariable.", distshp, state
				FROM ".$db->quoteName('india_information')."
				WHERE ".$db->quoteName('state')." IN (".$stat.")
					AND ".$db->quoteName('OGR_FID')." != ".$db->quote(490)."
				GROUP BY ".$db->quoteName('distshp').", ".$db->quoteName('state')."
				ORDER BY ".$db->quoteName('state').", ".$db->quoteName('distshp')." ASC ";
			$db->setQuery($query);
			try {
				$district_data = $db->loadObjectList();
			} catch (Exception $e) {
				$district_data = array();
			}

		}else{
			$district_data = array();
		}

		$state    = array();
		$gpbydist = array();
		foreach($district_data as $eachdist){
			$gpbydist[$eachdist->state][]=$eachdist;
		}

		$district_data = $gpbydist;
		$returndata    = '';
		if(count($district_data)){
			$returndata .= '<select name="district[]" id="district" class="inputbox" onchange="getdistrictattr(this.value)"  multiple="multiple" >';

			foreach($district_data as $key=>$val){
				$returndata .= '<optgroup label="'.$key.'">';
				for($i = 0; $i < count($val); $i++){
					if(in_array($val[$i]->$districtsearchvariable,$preselected)){$selected="selected";}else{$selected="";}
					$returndata .= '<option value="'.$val[$i]->$districtsearchvariable.'" '.$selected.'>'.$val[$i]->distshp.'</option>';
				}
				$returndata .= '</optgroup>';
			}
			$returndata .= '</select>';
		}else{
			$returndata .= '<select name="district" id="district" class="inputbox" onchange="getdistrict(this.value)"></select>';
		}

		/*$getUrbanAgglormationNames=$this->getUrbanAgglormationNamesByStateNames($stat);
		$getUATownNamesByStateNames=$this->getUATownNamesByStateNames($stat);*/
		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $returndata."split".$getUrbanAgglormationNames."split".$getUATownNamesByStateNames;exit;
	}


	function getUrbanAgglormationNamesByStateNames($state){
		$urbansearchvariable=$this->urbansearchvariable;

		$db = JFactory::getDBO();
		if($state){
			$query = "SELECT ".$urbansearchvariable.",place_name
				FROM ".$db->quoteName('jos_mica_urban_agglomeration')."
				WHERE ".$db->quoteName('place_name')." LIKE '%".$state."%'";
			$db->setQuery($query);
			$urban_data = $db->loadObjectList();
		}else{
			$urban_data = array();
		}

		$returndata = '';
		if(count($urban_data)){
			$returndata .= '<select name="urban" id="urban" class="inputbox" onchange="geturban(this.value)" >';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';
			for($i=0;$i<count($urban_data);$i++){
				$returndata .= '<option value="'.$urban_data[$i]->$urbansearchvariable.'">'.$urban_data[$i]->place_name.'</option>';
			}
			$returndata .= '</select>';
		}else{
			$returndata .= '<select name="urban" id="urban" class="inputbox" ></select>';
		}
		return $returndata;
	}


	function getUATownNamesByStateNames($state){
		$townsearchvariable = $this->townsearchvariable;

		$db = JFactory::getDBO();
		if($state){
			$query = "SELECT ".$this->townsearchvariable.",place_name
				FROM ".$db->quoteName('my_table')."
				WHERE ".$db->quoteName('state')." LIKE '%".$state."%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}else{
			$town_data = array();
		}

		$returndata = '';
		if(count($town_data)){
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="gettown(this.value)" >';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';
			for($i=0;$i<count($town_data);$i++){
				$returndata .= '<option value="'.$town_data[$i]->$townsearchvariable.'">'.$town_data[$i]->place_name.'</option>';
			}
			$returndata .= '</select>';
		}else{
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="gettown(this.value)"></select>';
		}
		return $returndata;
	}

	function gettown(){
		$db = JFactory::getDBO();

		$stat     = $this->input->get('stat','','raw');
		$district = $this->input->get('district','','raw');

		if($stat && $district){
			$query = "SELECT id,TownName
				FROM ".$db->quoteName('#__mica_towns')."
				WHERE ".$db->quoteName('StateName')." LIKE '%".$stat."%'
					AND ".$db->quoteName('DistrictName')." LIKE '%".$district."%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}else{
			$town_data = array();
		}

		$returndata = '';
		if(count($town_data)){
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="showhidetype(this.value)">';
			$returndata .= '<option value="all">'.JText::_('ALL').'</option>';
			for($i=0;$i<count($town_data);$i++){
				$returndata .= '<option value="'.$town_data[$i]->TownName.'">'.$town_data[$i]->TownName.'</option>';
			}
			$returndata .= '</select>';
		}else{
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="showhidetype(this.value)">
						   	<option value="">'.JText::_('PLEASE_SELECT').'</option>
						   	</select>';
		}
		echo $returndata;exit;
	}

	function getStateField(){
		$state = $this->input->get('state','','raw');

		$db = JFactory::getDBO();
		$query = "SELECT * FROM ".$db->quoteName('#__mica_state')." WHERE ".$db->quoteName('StateName')." LIKE '%".$state."%' ";
		$db->setQuery($query);
		$state_data = $db->loadObjectList();

		$returnable = array();
		foreach($state_data as $key=>$val){
			foreach($val as $key1=>$val1){
				$returnable[$key1]=$val1;
			}
		}
		echo json_encode($state_data);exit;
	}

	function getDistrictField(){
		$stat = $this->input->get('state','','raw');
		$dist = $this->input->get('dist','','raw');

		$db = JFactory::getDBO();
		$query = "SELECT * FROM  ".$db->quoteName('#__mica_district_r_u')."
			WHERE ".$db->quoteName('StateName')." LIKE '%".$stat."%'
				AND ".$db->quoteName('DistrictName')." LIKE '%".$dist."%' ";
		$db->setQuery($query);
		$district_data = $db->loadObjectList();

		$returnable = array();
		foreach($district_data as $key=>$val){
			foreach($val as $key1=>$val1){
				$returnable[$key1]=$val1;
			}
		}
		echo json_encode($district_data );exit;
	}

}
