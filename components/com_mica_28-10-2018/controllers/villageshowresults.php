<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

// DEVNOTE: import CONTROLLER object class
require_once JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers/Workspace.php';
require_once JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers/css.php';
require_once JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers/pdf.php';

/**
 * VillageShowresults controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerVillageShowresults extends MicaController
{
	// Custom Constructor
	var $tomcat_url             = "";

	var $townsearchvariable     = "OGR_FID";

	var $statesearchvariable    = "OGR_FID";

	var $urbansearchvariable    = "OGR_FID";

	var $districtsearchvariable = "OGR_FID";

	var $ruralprefix            = "Rural_";

	var $urnbanprefix           = "Urban_";

	var $totalprefix            = "Total_";

	var $_modal                 = "";

	/**
	 * [__construct description]
	 *
	 * @param   array  $default  [description]
	 */
	function __construct($default = array())
	{
		parent::__construct($default);

		if (!defined('TOMCAT_URL'))
		{
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcat_url);
		}

		// DEFINE('TOMCAT_STYLE_ABS_PATH', "/usr/share/tomcat7/webapps/geoserver/data/www/styles/");
		// DEFINE('TOMCAT_STYLE_ABS_PATH', "/home/mimi/apache-tomcat-7-0-26/webapps/geoserver/data/www/styles/");
		if (!defined('TOMCAT_STYLE_ABS_PATH'))
		{
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH', JPATH_SITE . "/geoserver/data/www/styles/");
		}

		$this->_table_prefix = '#__mica_';
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'VillageShowresults', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * ajax task.
	 *
	 * @return  void
	 */
	public function deleteattribute()
	{
		$session       = JFactory::getSession();
		$removeattr    = $this->input->get('attr', '', 'raw');
		$removeattrs[] = "rural_" . str_replace(" ", "_", strtolower($removeattr));
		$removeattrs[] = "urban_" . str_replace(" ", "_", strtolower($removeattr));
		$removeattrs[] = "total_" . str_replace(" ", "_", strtolower($removeattr));
		$removeattrs[] = str_replace(" ", "_", strtolower($removeattr));
		$removeattr    = str_replace(" ", "_", $removeattr);
		$attributes    = $session->get('villagesattributes');
		$original      = strlen($attributes);

		foreach ($removeattrs as $removeattr)
		{
			$attributes = str_ireplace($removeattr, "", $attributes);
		}


		$attributes = explode(",", $attributes);
		$attributes = array_filter($attributes);
		$attributes = implode(",", $attributes);
		$removed    = strlen($attributes);

		if ($original == $removed)
		{
			$this->deleteCustomAttribute();
		}

		$session->set('villagesattributes', $attributes);
		$attributes = $session->get('villagesattributes');

		$app = JFactory::getApplication();
		$app->redirect("index.php?option=com_mica&view=villageshowresults&Itemid=188");
		exit;
	}

	/**
	 * an ajax task.
	 *
	 * @return  void
	 */
	public function getsmeter()
	{
		$session            = JFactory::getSession();
		$db                 = JFactory::getDBO();
		$region             = urldecode($this->input->get("region", '', 'raw'));
		$filterspeed        = $this->input->get("filterspeed", '', 'raw');
		$speedvar           = urldecode($this->input->get("speedvar", '', 'raw'));
		$activetable        = $session->get("activetable");
		$activenamevariable = $session->get("activenamevariable");
		$speedvar           = explode(",", $speedvar);

		/*foreach($speedvar as $eachvar){
			$maxspeedvar[] ="(select max(".$eachvar.") from ".$activetable.") as max".$eachvar;
			$minspeedvar[] ="(select min(".$eachvar.") from ".$activetable.") as min".$eachvar;
		}
		$maxspeedvar = implode(",",$maxspeedvar);
		$minspeedvar = implode(",",$minspeedvar);
		$speedvar    = implode(",",$speedvar);*/

		// Edited rajesh 30-01-2013
		// For maximum value

		foreach ($speedvar as $eachvar)
		{
			$maxquery  = "SELECT " . $eachvar . " FROM " . $activetable . " WHERE " . $eachvar . " IS NOT NULL AND " . $eachvar . ">0 ORDER BY " . $eachvar . "+0 DESC LIMIT 1";
			$db->setQuery($maxquery);
			$maxresult = $db->loadResult();

			if (empty($maxresult))
			{
				$maxresult = 0;
			}

			$maxspeedvar[] = $maxresult . " as max" . $eachvar;
			$minquery      = "SELECT " . $eachvar . " FROM " . $activetable . " where " . $eachvar . " IS NOT NULL AND " . $eachvar . ">0 ORDER BY " . $eachvar . "+0 ASC LIMIT 1";
			$db->setQuery($minquery);
			$minresult = $db->loadResult();

			if (empty($minresult))
			{
				$minresult = 0;
			}

			$minspeedvar[] = $minresult . " as min" . $eachvar;
		}

		$maxspeedvar = implode(",", $maxspeedvar);
		$minspeedvar = implode(",", $minspeedvar);
		$speedvar    = implode(",", $speedvar);
		// Edited ends

		if (count(explode(",", $region)) == 1 && $filterspeed == 0)
		{
			// $region    = explode("-",$region);
			// $region    = $region[0];
			$temp_region = $region;
			$dash_pos    = strrpos($region, "-");

			if ($dash_pos === false)
			{
				// Note: three equal signs not found...
			}
			else
			{
				$region = substr($region, 0, $dash_pos);
			}

			if ($region == 'Diu Daman')
			{
				$region = 'Diu+Daman';
			}

			$state = substr($temp_region, $dash_pos + 1);
			$query = "SELECT " . $activenamevariable . " as name," . $speedvar . "," . $maxspeedvar . "," . $minspeedvar . "
				FROM " . $activetable . "
				WHERE " . $activenamevariable . " LIKE '" . $region . "'
					AND " . $db->quoteName('district_name') . " LIKE '" . $state . "'";

					// Group by ".$activenamevariable;
		}
		else
		{
			$regionss    = explode(",", $region);
			$state_array = array();

			foreach ($regionss as $eachregions)
			{
				$temp_region = $eachregions;
				$dash_pos    = strrpos($eachregions, "-");

				if ($dash_pos === false)
				{
					// Note: three equal signs not found...
				}
				else
				{
					$new_region = substr($eachregions, 0, $dash_pos);
				}

				if ($new_region == 'Diu Daman')
				{
					$new_region = 'Diu+Daman';
				}

				$state_array[] = substr($temp_region, $dash_pos + 1);

				$sapregion[] = $new_region;
			}

			$regions = implode(",", $sapregion);
			$region  = str_replace(",", "','", $regions);
			$query   = "SELECT " . $activenamevariable . " as name," . $speedvar . " ,";
			$query   .= $maxspeedvar . "," . $minspeedvar . " ";
			$query   .= " , district_name FROM " . $activetable . " WHERE " . $activenamevariable . " IN ('" . $region . "') ";

			// Group by ".$activenamevariable;
		}

		$db->setQuery($query);
		$list = $db->loadAssocList();

		if (count(explode(",", $region)) == 1 && $filterspeed == 0)
		{
			$indexkey = 0;

			foreach ($list[0] as $key => $val)
			{
				if (isset($list[0]["min" . $key]))
				{
					if ($list[0]["min" . $key] > 0)
					{
						$per = ($list[0][$key] * 100) / ($list[0]["max" . $key] - $list[0]["min" . $key]);
						$per = round($per);
					}
					else
					{
						$per = 0;
					}

					// $op[$key]           = $per;
					$op[$indexkey]['name'] = $key;
					$op[$indexkey]['data'] = $per;
					$indexkey++;
				}
			}
		}
		else
		{
			$indexkey = 0;

			foreach ($list as $eachlist)
			{
				foreach ($eachlist as $key => $val)
				{
					if (isset($eachlist["min" . $key]))
					{
						if (in_array($eachlist['district_name'], $state_array))
						{
							if ($eachlist["min" . $key] > 0)
							{
								$per = ($eachlist[$key] * 100) / ($eachlist["max" . $key] - $eachlist["min" . $key]);
								$per = round($per);
							}
							else
							{
								$per = 0;
							}

							$op[$indexkey]['name'] = $eachlist['name'] . ' - ' . $eachlist['district_name'];
							$op[$indexkey]['data'] = $per;
							$indexkey++;
						}
					}
				}
			}
		}

		if (count($op) > 0)
		{
			$script = "<script type='text/javascript'>
				function initspeed(){";
					$search_chars = array(" ", "&", "+");

				for ($l = 0; $l < count($op); $l++)
				{
					$key    = $op[$l]['name'];
					$val    = $op[$l]['data'];
					$script .= "	JQuery('#" . $l . str_replace($search_chars, "_", $key) . "').speedometer();
								JQuery('#" . $l . str_replace($search_chars, "_", $key) . "').speedometer({ percentage: " . $val . " || 0 });";
				}

			$script .= " }
			</script>";
		}

		$div .= "<table><tr>";

		if (count($op) > 0)
		{
			for ($l = 0; $l < count($op); $l++)
			{
				$key = $op[$l]['name'];
				$val = $op[$l]['data'];

				// $div .='<td ><div class="speed_m_div" id="'.str_replace(" ","_",$key).'" style="float:right;">'.array($key=>$val).'</div></td>';
				$tmpjtext = array($key => $val);

				$div .= '<td style="text-align:center;" ><div style="text-align:center;" class="speed_m_div" id="' . $l . str_replace($search_chars, "_", $key) . '" style="float:right;">' . $val . '</div></td>';
			}
		}

		$div .= "</tr><tr>";

		for ($l = 0; $l < count($op); $l++)
		{
			$key = $op[$l]['name'];
			$val = $op[$l]['data'];
			$div .= '<td style="text-align:center;">' . JText::_($key) . '</td>';
		}

		$div .= "</tr></table>";
		echo $opstring = $javascriptstring . $script . $div;
		exit;
	}

	/**
	 * Ajax task.
	 *
	 * @return  void
	 */
	public function deleteCustomAttribute()
	{
		$removeattr = $this->input->get('attr', '', 'raw');
		$this->input->set('attrname', $removeattr);

		$this->deleteCustomAttr(1);
		$this->deletethematicQueryToSession(1);

		return true;
		/*$session = JFactory::getSession();
		$attributes=$session->get('customattribute');
		$attributes=explode(",",$attributes);
		foreach($attributes as $k=>$eachattr)
		{	if(strstr($eachattr,$removeattr))
			{
				unset($attributes[$k]);
			}

		}

		$attributes=array_filter($attributes);
		$attributes=implode(",",$attributes);
		$session->set('customattribute',$attributes);*/
		//return true;
	}

	/**
	 * [deleteCustomVariableFromLib description]
	 *
	 * @return  void
	 */
	function deleteCustomVariableFromLib()
	{
		$session     = JFactory::getSession();
		$db          = JFactory::getDbo();
		$workspaceid = $session->get('activeworkspace');
		$activetable = $session->get('activetable');
		$name        = $this->input->get("attrname", '', 'raw');
		$name        = explode(",", $name);

		foreach ($name as $eachvariable)
		{
			$this->input->set('attr', $eachvariable);
			$this->deleteCustomAttribute();
			$query = "DELETE FROM " . $db->quoteName('#__mica_user_custom_attribute') . "
				WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($workspaceid) . "
					AND " . $db->quoteName('name') . " LIKE " . $db->quote($eachvariable);
			$db->setQuery($query);
			$db->execute();
			$query = "DELETE FROM " . $db->quoteName('#__mica_user_custom_attribute_lib') . "
				WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($workspaceid) . "
					AND " . $db->quoteName('name') . " LIKE " . $db->quote($eachvariable) . "
					AND " . $db->quoteName('tables') . " LIKE " . $db->quote($activetable);
			$db->setQuery($query);
			$db->execute();
		}

		$app = JFactory::getApplication();
		$app->redirect("index.php?option=com_mica&view=villageshowresults&Itemid=188");
	}

	/**
	 * ajax task.
	 *
	 * @return  void
	 */
	public function deleteVariable()
	{
		$session = JFactory::getSession();
		$table   = $this->input->get('table', '', 'raw');
		$value   = $this->input->get('value', '', 'raw');

		if ($table == "name")
		{
			$state      = $session->get('villagestate');
			$attributes = str_replace($value, "", $state);
			$attributes = explode(",", $attributes);
			$attributes = array_filter($attributes);
			$attributes = implode(",", $attributes);
			$session->set('villagestate', $attributes);

		}
		elseif ($table == "distshp")
		{
			$district   = $session->get('villagedistrict');
			$attributes = str_replace($value, "", $district);
			$attributes = explode(",", $attributes);
			$attributes = array_filter($attributes);
			$attributes = implode(",", $attributes);
			$session->set('villagedistrict', $attributes);

		}
		elseif ($table == "place_name")
		{
			$villages   = $session->get('villages_villages');
			$attributes = str_replace($value, "", $villages);
			$attributes = explode(",", $attributes);
			$attributes = array_filter($attributes);
			$attributes = implode(",", $attributes);
			$session->set('villages_villages', $attributes);

		}
		elseif ($table == "UA_Name")
		{
			$urban      = $session->get('urban');
			$attributes = str_replace($value, "", $urban);
			$attributes = explode(",", $attributes);
			$attributes = array_filter($attributes);
			$attributes = implode(",", $attributes);
			$session->set('urban', $attributes);
		}

		exit;
	}

	/**
	 * Internal function fetches URL.
	 *
	 * @return void;
	 */
	function getTomcatUrl()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM " . $db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcat_url = $db->loadResult();
	}

	/**
	 * AJAX task.
	 *
	 * @return  void
	 */
	public function popupAttributes()
	{
		$session    = JFactory::getSession();
		$attributes = $session->get('villagesattributes');
		$villages   = $session->get('villages_villages');
		$urban      = $session->get('urban');
		$id         = $this->input->get("id", '', 'raw');
		$id         = trim($id);
		$zoom       = $this->input->get("zoom", '', 'raw');

		if ($zoom == 5 || $zoom == 6)
		{
			$searchvariable  = $this->districtsearchvariable;

			// $district      = $session->get('district');
			// $session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid', $district_orgfid . "," . $id);
			$sld = $this->sldPopup($searchvariable, $id, "india_information", "distshp");
		}
		elseif ($zoom == 7)
		{
			$searchvariable = $this->districtsearchvariable;

			// $district      = $session->get('district');
			// $session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid', $district_orgfid . "," . $id);
			$sld = $this->sldPopup($searchvariable, $id, "india_information", "distshp");

		}
		elseif ($zoom == 8)
		{
			$searchvariable  = $this->districtsearchvariable;

			// $district      = $session->get('district');
			// $session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid', $district_orgfid . "," . $id);
			$sld = $this->sldPopup($searchvariable, $id, "india_information", "distshp");
		}
		else
		{
			$searchvariable  = $this->districtsearchvariable;

			// $district      = $session->get('district');
			// $session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid', $district_orgfid . "," . $id);
			$sld = $this->sldPopup($searchvariable, $id, "india_information", "distshp");
		}

		$session->set('state_orgfid', null);
		$session->set('district_orgfid', null);
		$session->set('town_orgfid', null);
		$session->set('urban_orgfid', null);
		$str    = "<div ><table class='popupmaintable ' cellspacing='0' cellpadding='0' border='1'>";
		$header = "";
		$sld    = array_reverse($sld);

		if (count($sld) > 0)
		{
			$header .= "<table class='popupheader'><tr>";
			$header .= "<td class='popupleft'>Name</td>";
			$header .= "<td class='popupright'>" . $sld[0]["text"] . "</td>";
			$header .= "</tr></table>";

			foreach ($sld as $eachsld)
			{
				if ($eachsld['custom_name'] == "")
				{
					$name = $eachsld['custom_formula'];
				}
				else
				{
					$name = $eachsld['custom_name'];
				}

				$str .= "<tr >";
				$str .= "<td >" . JTEXT::_($name) . "</td>";
				$str .= "<td >" . $eachsld['custom_value'] . "</td>";

				if ($eachsld['level'] != 0)
				{
					$color = str_replace("#", "", $eachsld['color']);
					$str .= "<td ><img src='" . JURI::base() . "components/com_mica/maps/img/layer" . $eachsld['level'] . "/pin" . $color . ".png' /></td>";
				}
				else
				{
					$str .= "<td style='background:" . $eachsld['color'] . ";'></td>";
				}

				$str .= "</tr>";
			}
		}
		else
		{
			$model  = $this->getModel();
			$data   = $model->getCustomData(1, 1);
			$header = "";

			foreach ($data as $basekey => $object)
			{
				foreach ($object as $key => $val)
				{
					if ($object->$searchvariable == $id && $key != $searchvariable && $key != "state" && $key != "distshp")
					{
						if (strstr($key, "name"))
						{
							$header .= "<table class='popupheader'><tr>";
							$header .= "<td class='popupleft'>" . JTEXT::_($key) . "</td>";
							$header .= "<td class='popupright'>" . $val . "</td>";
							$header .= "</tr></table>";
						}
						else
						{
							$str .= "<tr >";
							$str .= "<td class='popupleft'>" . JTEXT::_($key) . "</td>";
							$str .= "<td class='popupright'>" . $val . "</td>";
							$str .= "</tr>";
						}
					}
				}
			}
		}

		$str .= "</table></div>";
		echo $header . $str;
		exit;
	}

	/**
	 * [sldPopup description]
	 *
	 * @param   [type]  $searchvarable       [description]
	 * @param   [type]  $searchvalue         [description]
	 * @param   [type]  $basetable           [description]
	 * @param   [type]  $searchtextvariable  [description]
	 *
	 * @return  [type]                       [description]
	 */
	function sldPopup ($searchvarable, $searchvalue, $basetable, $searchtextvariable)
	{
		$session         = JFactory::getSession();
		$db              = JFactory::getDBO();
		$activeworkspace = $session->get('activeworkspace');
		$finalarray      = array();

		$query = "SELECT sld.*, sld.layerfill, t1." . $searchtextvariable . " , t2.*
			FROM " . $db->quoteName('#__mica_map_sld') . " AS sld ,
				" . $db->quoteName($basetable) . " AS t1 ,
				" . $db->quoteName('#__mica_sld_legend') . " AS t2
			WHERE " . $db->quoteName('sld.text') . " = " . $db->quoteName("t1." . $searchtextvariable) . "
				AND " . $db->quoteName("t1." . $searchvarable) . " = " . $db->quote($searchvalue) . "
				AND " . $db->quoteName('profile_id') . " = " . $db->quote($activeworkspace) . "
				AND " . $db->quoteName('t2.workspace_id') . " = " . $db->quoteName('profile_id') . "
				AND " . $db->quoteName('sld.layerfill') . " = " . $db->quoteName('t2.color') . "
			GROUP BY " . $db->quoteName('t2.level') . " DESC ";
		$db->setQuery($query);
		$list = $db->loadAssocList();

		foreach ($list as $eacharray)
		{
			$query = "SELECT name
				FROM " . $db->quoteName('#__mica_user_custom_attribute') . "
				WHERE " . $db->quoteName('attribute') . " = " . $db->quote($eacharray['custom_formula']) . "
					AND " . $db->quoteName('profile_id') . " = " . $db->quote($eacharray['profile_id']);
			$db->setQuery($query);
			$customformulaname = $db->loadResult();

			if ($customformulaname == '')
			{
				$customname = array("custom_name" => "");
			}
			else
			{
				$customname = array("custom_name" => $customformulaname);
			}

			$eacharray = array_merge($customname, $eacharray);

			$query = "SELECT (" . $eacharray['custom_formula'] . ") as customval FROM " . $basetable . " WHERE " . $searchvarable . "=" . $searchvalue;
			$db->setQuery($query);
			$customformulavalue = $db->loadResult();

			$customvalue  = array("custom_value" => $customformulavalue);
			$eacharray    = array_merge($customvalue, $eacharray);
			$finalarray[] = $eacharray;
		}

		return $finalarray;
	}

	/**
	 * A task to fetch attributes from AJAX call.
	 *
	 * @return  void
	 */
	public function getAttribute()
	{
		$zoom = $this->input->get('zoom', '', 'raw');
		$type = $this->input->get('type', '', 'raw');
		$user = JFactory::getUser();

		if ($user->id <= 0)
		{
			echo - 1;
			exit;
		}

		echo $values = $this->getPlanAttr($zoom, $type);
		exit;
	}

	/**
	 * [getPlanAttr description]
	 *
	 * @param   [type]  $zoom  [description]
	 * @param   [type]  $type  [description]
	 *
	 * @return  [type]         [description]
	 */
	function getPlanAttr($zoom, $type = null)
	{
		$prefix = 0;

		if ($type == null)
		{
			if ($zoom == 5)
			{
				$db_table = "State";
				$prefix   = 1;
			}
			elseif ($zoom > 5 && $zoom <= 7)
			{
				$db_table = "District";
				$prefix   = 1;
			}
			else
			{
				$db_table = "villages";
			}
		}
		else
		{
			$db_table = "district";
		}

		$userAttr = $this->userSelectedAttr($db_table);

		if ($db_table == "state")
		{
			$prefix = 1;
		}
		elseif ($db_table == "district")
		{
			$prefix = 1;
		}
		else
		{
			$prefix = 0;
		}

		if ($userAttr == -2)
		{
			return -2;
		}

		$str       = "";
		$todisplay = array();
		$db        = JFactory::getDBO();

		// Edited by date on 21/01/2013
		// $query="select * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid where allfields.table like '".$db_table."' and grp.publish =1 order by grp.group,allfields.field ASC";
		$query = "SELECT * FROM " . $db->quoteName('#__mica_group_field') . " AS allfields
			INNER JOIN " . $db->quoteName('#__mica_group') . " AS grp ON " . $db->quoteName('grp.id') . " = " . $db->quoteName('allfields.groupid') . "
			WHERE " . $db->quoteName('allfields.table') . " LIKE " . $db->quote($db_table) . "
				AND " . $db->quoteName('grp.publish') . " = " . $db->quote(1) . "
			ORDER BY " . $db->quoteName('grp.group') . ", " . $db->quoteName('allfields.field') . " ASC";
		$db->setQuery($query);
		$list = $db->loadAssocList();

		foreach ($list as $each)
		{
			if ($each['group'] != "Mkt Potential Index" && $each['group'] != "Score")
			{
				if ($prefix == 1)
				{
					// $each['field']               = str_replace($this->ruralprefix, "", $each['field']);
					// $each['field']               = str_replace($this->urnbanprefix, "", $each['field']);
					// $each['field']               = str_replace($this->totalprefix, "", $each['field']);
					$todisplay[$each['group']][] = $each['field'];
				}
				else
				{
					$todisplay[$each['group']][] = $each['field'];
				}

				$icon[$each['group']] = $each['icon'];
			}
			else
			{
				if ($prefix == 1)
				{
					// $each['field'] = str_replace($this->ruralprefix, "", $each['field']);
					// $each['field'] = str_replace($this->urnbanprefix, "", $each['field']);
					// $each['field'] = str_replace($this->totalprefix, "", $each['field']);
					$todisplay1[$each['group']][] = $each['field'];
				}
				else
				{
					$todisplay1[$each['group']][] = $each['field'];
				}

				$icon[$each['group']] = $each['icon'];
			}
		}

		$todisplay = $todisplay;

		foreach ($todisplay as $eachgrp => $vals)
		{
			$todisplay[$eachgrp] = array_unique($todisplay[$eachgrp]);
		}

		foreach ($userAttr as $each)
		{
			if ($prefix == 1)
			{
				// $each = str_replace($this->ruralprefix, "", $each);
				// $each = str_replace($this->urnbanprefix, "", $each);
				// $each = str_replace($this->totalprefix, "", $each);
				$userAttr1[] = $each;
			}
			else
			{
				$userAttr1[] = $each;
			}
		}

		$userAttr      = array_unique($userAttr1);
		$session       = JFactory::getSession();
		$oldattr       = $session->get('villagesattributes');
		$oldattr_array = explode(",", strtolower($oldattr));

		$urban         = "";
		$villages      = array();
		$other         = "";
		$str           = "";
		$i             = 0;
		$str1          = "";
		$other         = array();
		$urban         = array();
		$total         = array();
		$grpname       = array();
		$header        = "";
		$div           = "";
		$returndata    = "";

		if ($this->input->get('grplist', '', 'raw') == "")
		{
			//$returndata .= '<select name="villagesattributes[]" id="villagesattributes" class="inputbox"   multiple="multiple" >';
			$returndata.='<ul class="list1 districtlist">';

			foreach ($todisplay as $keys => $district_data)
			{
				$keys = $keys == '-' ? "" : $keys;

				//$returndata .= '<optgroup label="' . $keys . '">';
				$returndata .='<li class="variable_group"><label>'.$keys.'</label></li>';

				foreach ($district_data as $key => $eachattr)
				{
					if ($eachattr == "Rating" || $eachattr == "MPI" || $eachattr == "Total_Geographical_Area_in_Hectares" || $eachattr == 'Nearest_Town_Name')
					{
						//$checked = "selected" . " disabled";
						$checked = "checked" . " disabled";
					}
					elseif (in_array(strtolower($eachattr), $oldattr_array))
					{
						//$checked = "selected";
						$checked = "checked";
					}
					else
					{
						$checked = "";
					}

					// $returndata .= '<option value="'.$eachattr.'" '.$checked.'>'.JTEXT::_($eachattr).'</option>';
					//$returndata .= '<option value="' . $eachattr . '" ' . $checked . '>' . JTEXT::_($eachattr) . '</option>';

					$returndata .= '<li><input type="checkbox" class="variable_checkbox" name="villagesattributes[]" value="'.$eachattr.'" '.$checked.' id="variable_check_'.$x.'">
						<label for="variable_check_'.$x.'">'.JTEXT::_($eachattr).'</label></li>';
						$x++;
				}

				//$returndata .= '</optgroup >';
			}

			//$returndata .= '</select>';

			$returndata .= '</ul>';

			return $returndata;
		}
		else
		{
			$grplist    = $this->input->get('grplist', '', 'raw');
			$grplist    = explode(",", $grplist);
			$returndata .= '<select name="villagesattributes[]" id="villagesattributes" class="inputbox" multiple="multiple" >';

			foreach ($todisplay as $key => $district_data)
			{
				if (in_array($key, $grplist))
				{
					$returndata .= '<optgroup label="' . $key . '">';

					foreach ($district_data as $key => $eachattr)
					{
						if ($eachattr == "Rating" || $eachattr == "MPI")
						{
							$checked = "selected";
						}
						elseif (in_array(strtolower($eachattr), $oldattr_array))
						{
							$checked = "selected";
						}
						else
						{
							$checked = "";
						}

						// Edited by dave 21/01/2013
						// $returndata .= '<option value="'.$eachattr.'" '.$checked.'>'.JTEXT::_($eachattr).'</option>';
						$returndata .= '<option value="' . $eachattr . '" ' . $checked . '>' . JTEXT::_($eachattr) . '</option>';
					}

					$returndata .= '</optgroup >';
				}
			}

			$returndata .= '</select>';

			return $returndata;
		}

		foreach ($todisplay as $key => $val)
		{
			$grpname[] = $key;
			$str[$i]   = "";

			if ($icon[$grpname[$i]] != "")
			{
				$img = $icon[$grpname[$i]];
			}
			else
			{
				$img = "default.png";
			}

			$header .= "<li ><a href='#" . str_replace(" ", "_", trim($grpname[$i])) . "grp'><div class='grpimg'><img src='" . JUri::base() . "components/com_mica/images/" . $img . "'/></div><div class='grptitle'>" . $grpname[$i] . "</div></a></li>";

			// $oldattr=explode(",",$oldattr);
			foreach ($userAttr as $eachattr)
			{
				if (in_array($eachattr, $val))
				{
					if ($eachattr == "Rating" || $eachattr == "MPI")
					{
						$checked = "checked";
					}
					elseif (in_array(strtolower($eachattr), $oldattr_array))
					{
						$checked = "selected";
					}
					else
					{
						$checked = "";
					}

					$other[$i][] = '
					<div class="variablechkbox ">
					<input type="checkbox" name="villagesattributes[]" class="statetotal_attributes hideurban ' . str_replace(" ", "_", $key) . '" value="' . $eachattr . '" ' . $checked . '>
					</div>
					<div class="variabletext hideurban"><span class="hovertext ' . JTEXT::_(trim($eachattr)) . '">' . ucfirst(strtolower($eachattr)) . '</span></div>';
				}
			}

			$i++;
		}

		$str1 .= '<div class="maintableattr">
			<div class="left">
			<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(statetotal_attributes);">' . JText::_('SELECT_ALL_LABEL') . '</a> / <a class="statetotalunselectall" onclick="uncheckall(statetotal_attributes);" href="javascript:void(0);">' . JText::_("UNSELECT_ALL_LABEL") . '</a>
			</div></div>';

		$finalstr   = "";
		$town1      = "";
		$urban1     = "";
		$others1    = "";
		$mystrarray = array();
		$total1     = array();

		for ($i = 0; $i < count($str); $i++)
		{
			$finalstr = "";
			$town1    = "";
			$urban1   = "";
			$others1  = "";
			$total1   = "";

			for ($j = 0; $j < count($other[$i]); $j++)
			{
				if (($j % 3) == 0)
				{
					$others1 .= "</div><div class='maintableattr'>";
				}

				$others1 .= $other[$i][$j];
			}

			$othersss = "<div id='" . str_replace(" ", "_", trim($grpname[$i])) . "grp' class='singlegrp'>";
			$others1  = $othersss . "<div>" . $others1;

			/*$finalstr .=$str[$i];
			$finalstr .="</div><div class='maintableattr ' >".$urban1."";
			$finalstr .="</div><div class='maintableattr '>".$total1."</div>";
			$others1  .="</div>";*/

			$finalstr .= $others1 . "";
			$finalstr .= '<div class="maintableattr">
				<div class="selectallfront">
				<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(\'' . str_replace(" ", "_", trim($grpname[$i])) . '\');">' . JText::_('SELECT_ALL_LABEL') . '</a> / <a class="statetotalunselectall" onclick="uncheckall(\'' . str_replace(" ", "_", trim($grpname[$i])) . '\');" href="javascript:void(0);">' . JText::_("UNSELECT_ALL_LABEL") . '</a>
				</div></div></div></div>';
			$mystrarray[$i] = $finalstr;
		}

		return "<div id='tabs'><ul>" . $header . "</ul>" . implode("", $mystrarray) . "</div></div></div>";
	}

	/**
	 * [userSelectedAttr description]
	 *
	 * @param   [type]  $db_table  [description]
	 *
	 * @return  [type]             [description]
	 */
	function userSelectedAttr($db_table)
	{
		$db    = JFactory::getDBO();
		$query = " SELECT plan_id
			FROM " . $db->quoteName('#__osmembership_subscribers') . " AS a
			WHERE " . $db->quoteName('plan_subscription_status') . " =  " . $db->quote(1) . "
				AND " . $db->quoteName('user_id') . " = " . $db->quote(JFactory::getUser()->id);
		$db->setQuery($query);
		$plan_id = (int) $db->loadResult();

		 $query = "SELECT attribute FROM " . $db->quoteName("#__mica_user_attribute") . "
			WHERE " . $db->quoteName('aec_plan_id') . " = " . $db->quote($plan_id) . "
				AND " . $db->quoteName('dbtable') . " LIKE " . $db->quote($db_table);
		$db->setQuery($query);
		$row = $db->loadResult();
		$attr = explode(",", $row);

		return $attr;
	}

	/**
	 * ajax task.
	 *
	 * @return  void
	 */
	public function AddCustomAttr()
	{
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$session = JFactory::getSession();

		if ($user->id == 0)
		{
			return -1;
		}

		$attributevale = $this->input->get('attributevale', '', 'raw');
		$attrname      = $this->input->get('attrname', '', 'raw');
		/*
			$query = "SELECT count(id) FROM ".$this->_table_prefix."user_profile WHERE user_id = ".$user->id.";";
			$db->setQuery($query);
			$count = $db->loadResult();
			if($count==0)
			{
				return -2;
			}
			$query="SELECT id FROM ".$this->_table_prefix."user_profile WHERE user_id = ".$user->id;
			$db->setQuery($query);
			$db->query();
			$row = $db->loadRow();
			$query="INSERT into ".$this->_table_prefix."user_custom_attribute (profile_id,name,attribute) VALUES (".$row[0].",'".$attrname."','".$attributevale."') ";
			$db->setQuery($query);
			$db->query();
		*/

		$attrnamesession        = $session->get('customattribute');

		// $attrname             = $attrname."`".$attrnamesession;
		// $attributevalesession = $session->get('attributevale');
		$attributevale          = $attrname . ":" . $attributevale . "," . $attrnamesession;
		$session->set('customattribute', $attributevale);
		workspaceHelper::updateWorkspace();

		// $session->set('attributevale',$attributevale);
		exit;
	}

	/**
	 * ajax task.
	 *
	 * @return  void
	 */
	public function getCustomAttr()
	{
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$session = JFactory::getSession();

		if ($user->id == 0)
		{
			return -1;
		}

		$query = "SELECT count(id) FROM " . $db->quoteName('#__user_profile') . " WHERE " . $db->quoteName('user_id') . " = " . $db->quote($user->id);
		$db->setQuery($query);
		$count = $db->loadResult();

		if ($count == 0)
		{
			return -2;
		}

		$query = "SELECT id FROM " . $db->quoteName('#__user_profile') . " WHERE " . $db->quoteName('user_id') . " = " . $db->quote($user->id);
		$db->setQuery($query);
		$row = $db->loadResult();

		$query = "SELECT * FROM " . $db->quoteName('#__user_custom_attribute') . " WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($row);
		$db->setQuery($query);
		$attr = $db->loadAssocList();
		$str = "";

		foreach ($attr as $eachattr)
		{
			$str .= '
			<tr >
				<td align="left" valign="top"><input type="checkbox" name="customattributes[]" class="customtotal_attributes" id="d' . $eachattr['id'] . '" value="' . $eachattr['attribute'] . '" checked="checked"></td>
				<td align="left" valign="top" class="d' . $eachattr['id'] . '">' . $eachattr['name'] . '</td>
				<td id="del_custom_' . $eachattr['id'] . '" class="delcustom"> X </td>
			</tr>';
		}

		echo $str;
		exit;
	}

	/**
	 * An ajax task
	 *
	 * @return  void
	 */
	public function updateCustomAttr()
	{
		$session       = JFactory::getSession();
		$attributevale = $this->input->get('attributevale', '', 'raw');
		$oldattrval    = $this->input->get('oldattrval', '', 'raw');
		$attrname      = $this->input->get('attrname', '', 'raw');

		$attributes = $session->get('customattribute');
		$attributes = str_replace($oldattrval, $attributevale, $attributes);
		$session->set('customattribute', $attributes);

		$activeworkspace          = $session->get('activeworkspace');
		$customformulafromsession = $session->get('customformula');
		$customformulafromsession = str_replace($oldattrval, $attributevale, $customformulafromsession);
		$session->set('customformula', $customformulafromsession);

		workspaceHelper::updateWorkspace();
		exit;
	}

	/**
	 * An ajax task
	 *
	 * @param   [type]  $innercall  [description]
	 *
	 * @return  void
	 */
	public function deleteCustomAttr($innercall = null)
	{
		/*$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$id=$this->input->get("id", '', 'raw');
		if($user->id == 0){
			return -1;
		}
		$query="DELETE FROM ".$this->_table_prefix."user_custom_attribute WHERE id = ".$id;
		$db->setQuery($query);
		$db->query();
		echo 1;*/

		$session    = JFactory::getSession();
		$attrname   = $this->input->get('attrname', '', 'raw');
		$attributes = $session->get('customattribute');
		$attributes = explode(",", $attributes);
		$val        = array();

		foreach ($attributes as $k => $eachattr)
		{
			$nameandattr = explode(":", $eachattr);

			if ($nameandattr[0] == "" || $nameandattr[0] == $attrname)
			{
				unset($attributes[$k]);
				$val[] = $nameandattr[1];
			}
		}

		$attributes = array_filter($attributes);
		$attributes = implode(",", $attributes);

		$session->set('customattribute', $attributes);
		$customformulafromsession = $session->get('customformula');
		$geteachformula           = explode("|", $customformulafromsession);

		foreach ($geteachformula as $key => $eachtocheck)
		{
			$singleformula = explode(":", $eachtocheck);

			if (in_array($singleformula[0], $val))
			{
				unset($geteachformula[$key]);
				$segment = explode("->", $singleformula[1]);

				$this->input->set('level', end($segment));
			}
		}

		$customformulafromsession = implode("|", $geteachformula);
		$session->set('customformula', $customformulafromsession);

		// echo $customformulafromsession;exit;
		workspaceHelper::updateWorkspace();

		if ($innercall != null)
		{
			return true;
		}
		else
		{
			exit;
		}
	}

	/**
	 * A task for page.
	 *
	 * @param   [type]  $msg  [description]
	 *
	 * @return  void
	 */
	public function loadWorkspace($msg = null)
	{
		$app    = JFactory::getApplication();
		$result = workspaceHelper::loadWorkspace();
		$user   = JFactory::getUser();

		if ($user->id == 0)
		{
			return -1;
		}

		if (!empty($result) && count($result) > 0)
		{
			$session = JFactory::getSession();
			$db      = JFactory::getDBO();
			$restore = unserialize($result[0]['data']);
			$session->set('villagesattributes', $restore['villagesattributes']);
			$session->set('villagem_type', $restore['villagem_type']);
			$session->set('villagestate', $restore['villagestate']);
			$session->set('villagedistrict', $restore['villagedistrict']);
			$session->set('urban', $restore['urban']);
			$session->set('villages_villages', $restore['villages_villages']);
			$session->set('is_default', $result[0]['is_default']);

			$workspaceid = $this->input->get("workspaceid", '', 'raw');
			$session->set('gusetprofile', '');
			$session->set('activeworkspace', $workspaceid);

			$query = "SELECT count(*)
				FROM " . $db->quoteName('#__mica_user_custom_attribute_lib') . "
				WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($workspaceid);
			$db->setQuery($query);
			$count = $db->loadResult();

			if ($count != 0)
			{
				$query = " SELECT name, attribute
					FROM " . $db->quoteName("#__mica_user_custom_attribute") . "
					WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($workspaceid);
				$db->setQuery($query);
				$allcostomattr = $db->loadAssocList();
				$str = "";

				foreach ($allcostomattr as $eachcustomattr)
				{
					$eachcustomattr['attribute'] = str_replace("'", " ", $eachcustomattr['attribute']);
					$str .= $eachcustomattr['name'] . ":" . $eachcustomattr['attribute'] . ",";
				}

				$attributes = $session->set('customattribute', $str);
				$query = "SELECT name, attribute
					FROM " . $db->quoteName('#__mica_user_custom_attribute_lib') . "
					WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($session->get('activeworkspace'));
				$db->setQuery($query);
				$allcostomattr = $db->loadAssocList();
				$str = "";

				foreach ($allcostomattr as $eachcustomattr)
				{
					$eachcustomattr['attribute'] = str_replace("'", " ", $eachcustomattr['attribute']);
					$str .= $eachcustomattr['name'] . ":" . $eachcustomattr['attribute'] . ",";
				}

				$session->set('customattributelib', $str);
				$session->set('customattribute', $str);
			}
			else
			{
				$session->set('customattribute', null);
				$session->set('customattributelib', null);
			}
		}

		if ($msg == "insert")
		{
			$msg = "&msg=0";
			$this->saveSldToDataBase("1");
			$session->set('customformula', null);

			// $session->set('customformula',null);
			// $this->saveSldToDataBase("1");
		}
		else
		{
			$msg = "";
		}

		$app->redirect("index.php?option=com_mica&view=villageshowresults&Itemid=188" . $msg);
	}

	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function updateWorkspace()
	{
		$updatedworkspace = workspaceHelper::updateWorkspace();
		$this->saveSldToDataBase("1", "1");
		exit;
	}

	/**
	 * An ajax task
	 *
	 * @return  void
	 */
	public function deleteWorkspace()
	{
		$db              = JFactory::getDBO();
		$session         = JFactory::getSession();
		$activeworkspace = $session->get('activeworkspace');
		$query           = "DELETE FROM " . $db->quoteName('#__mica_user_custom_attribute') . "
			WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($activeworkspace);
		$db->setQuery($query);
		$db->execute();

		workspaceHelper::deleteWorkspace();
		exit;
	}

	/**
	 * An ajax task
	 *
	 * @return  void
	 */
	public function saveWorkspace()
	{
		$id = workspaceHelper::saveWorkspace(true);
		$this->input->set('workspaceid', $id);
		$this->loadWorkspace("insert");
	}

	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function getMinMax()
	{
		$session     = JFactory::getSession();
		$db          = JFactory::getDBO();
		$value       = $this->input->get('value', '', 'raw');
		$activetable = $session->get('activetable');
		$activedata  = $session->get('villagedistrict');
		$activedata  = explode(",", $activedata);
		$ogrfid      = array();

		foreach ($activedata as $eachdata)
		{
			$ogrfid[] = $eachdata;
		}

		$result = array();

		if ($value != '')
		{
			$query = "SELECT (" . $value . ") as mymin, (" . $value . ") as mymax
				FROM " . $activetable . "
				WHERE " . $db->quoteName('OGR_FID') . " IN (" . implode(",", $ogrfid) . ")";
			$db->setQuery($query);
			$result = $db->loadObjectList();
		}

		$min       = min($result);
		$max       = max($result);
		$results[] = (int) ($min->mymin);
		$results[] = (int) ($max->mymax);

		echo implode(",", $results);
		exit;
	}

	/**
	 * An ajax task.
	 *
	 * @return  void-
	 */
	public function getColorGradiant()
	{
		$value         = $this->input->get('value', '', 'raw');
		$steps         = $this->input->get('steps', '', 'raw');

		$theColorBegin = (isset($value)) ? hexdec("#" . $value) : 0x000000;
		$theColorEnd   = 0xffffff;
		$theNumSteps   = (isset($_REQUEST['steps'])) ? intval($steps) : 16;

		$theR0         = ($theColorBegin & 0xff0000) >> 16;
		$theG0         = ($theColorBegin & 0x00ff00) >> 8;
		$theB0         = ($theColorBegin & 0x0000ff) >> 0;

		$theR1         = ($theColorEnd & 0xff0000) >> 16;
		$theG1         = ($theColorEnd & 0x00ff00) >> 8;
		$theB1         = ($theColorEnd & 0x0000ff) >> 0;
		$str           = array();

		for ($i = 0; $i <= $theNumSteps; $i++)
		{
			$theR    = $this->interpolate($theR0, $theR1, $i, $theNumSteps);
			$theG    = $this->interpolate($theG0, $theG1, $i, $theNumSteps);
			$theB    = $this->interpolate($theB0, $theB1, $i, $theNumSteps);
			$theVal  = ((($theR << 8) | $theG) << 8) | $theB;
			$str[$i] = dechex($theVal);
		}

		echo implode(",", $str);
		exit;
	}

	/**
	 * [interpolate description]
	 *
	 * @param   [type]  $pBegin  [description]
	 * @param   [type]  $pEnd    [description]
	 * @param   [type]  $pStep   [description]
	 * @param   [type]  $pMax    [description]
	 *
	 * @return  [type]           [description]
	 */
	function interpolate($pBegin, $pEnd, $pStep, $pMax)
	{
		if ($pBegin < $pEnd)
		{
			return (($pEnd - $pBegin) * ($pStep / $pMax)) + $pBegin;
		}
		else
		{
			return (($pBegin - $pEnd) * (1 - ($pStep / $pMax))) + $pEnd;
		}
	}

	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function addthematicQueryToSession()
	{
		$session = JFactory::getSession();
		$user    = JFactory::getUser();

		if ($user->id == 0)
		{
			return -1;
		}

		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');

		if ($level == "")
		{
			$level = $this->getSldLevel();
			$level = explode(",", $level);
			$level = $level[0];
		}

		$from  = $this->input->get('from', '', 'raw');
		$to    = $this->input->get('to', '', 'raw');
		$color = $this->input->get('color', '', 'raw');

		$customformulafromsession = $session->get('customformula');

		$geteachformula = explode("|", $customformulafromsession);

		foreach ($geteachformula as $key => $eachtocheck)
		{
			$singleformula = explode(":", $eachtocheck);

			if ($singleformula[0] == $formula || $singleformula[0] == "")
			{
				unset($geteachformula[$key]);
			}
		}

		$customformulafromsession = implode("|", $geteachformula);
		$customformula            = "|" . $formula . ":" . $from . "->" . $to . "->" . $color . "->" . $level;
		$customformulafromsession = $customformulafromsession . $customformula;
		$customformulafromsession = explode(":", $customformulafromsession);
		$customformulafromsession = array_filter($customformulafromsession);
		$customformulafromsession = array_unique($customformulafromsession);
		$customformulafromsession = implode(":", $customformulafromsession);
		$session->set('customformula', $customformulafromsession);

		// Edited
		$session->set('fromthematic', 1);

		// Edited ends
		echo $customformulafromsession;
		exit;
	}

	/**
	 * An ajax task., internal as well.
	 *
	 * @param   [type]  $innercall  [description]
	 *
	 * @return  void
	 */
	public function deletethematicQueryToSession($innercall = null)
	{
		$session = JFactory::getSession();
		$user    = JFactory::getUser();
		$db      = JFactory::getDBO();

		if ($user->id == 0)
		{
			return -1;
		}

		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');

		/*$from  =$this->input->get('from', '', 'raw');
		$to    =$this->input->get('to', '', 'raw');
		$color =$this->input->get('color', '', 'raw');*/

		$customformulafromsession = $session->get('customformula', $attributes);
		$geteachformula           = explode("|", $customformulafromsession);

		foreach ($geteachformula as $key => $eachtocheck)
		{
			$singleformula = explode(":", $eachtocheck);

			if ($singleformula[0] == $formula)
			{
				unset($geteachformula[$key]);
			}
		}

		$customformulafromsession   = implode("|", $geteachformula);

/*		$customformula            = $formula.":".$from."->".$to."->".$color."|";
		$customformulafromsession = $customformulafromsession.$customformula;*/

		$customformulafromsession   = explode(":", $customformulafromsession);
		$customformulafromsession   = array_filter($customformulafromsession);
		$customformulafromsession   = array_unique($customformulafromsession);
		$customformulafromsession   = implode(":", $customformulafromsession);

		// echo $customformulafromsession;
		$session->set('customformula', $customformulafromsession);

		$workspaceid = $session->get('activeworkspace');
		$query = "DELETE FROM " . $db->quoteName('#__mica_sld_legend') . "
			WHERE " . $db->quoteName('workspace_id') . " = " . $db->quote($workspaceid) . "
				AND " . $db->quoteName('level') . " = " . $db->quote($level);
		$db->setQuery($query);
		$db->execute();

		$query = "DELETE FROM " . $db->quoteName('#__mica_map_sld') . "
			WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($workspaceid) . "
				AND " . $db->quoteName('level') . " = " . $db->quote($level);
		$db->setQuery($query);
		$db->execute();

		if ($innercall != null)
		{
			return true;
		}
		else
		{
			exit;
		}
	}

	/**
	 * [saveSldToDataBase description]
	 *
	 * @param   [type]  $innercall  [description]
	 * @param   [type]  $isUpdate   [description]
	 *
	 * @return  [type]              [description]
	 */
	private function saveSldToDataBase($innercall = null, $isUpdate = null)
	{
		cssHelper::saveSldToDatabase($innercall, $isUpdate);
	}

	/**
	 * [getSldLevel description]
	 *
	 * @return  [type]  [description]
	 */
	function getSldLevel()
	{
		$db              = JFactory::getDBO();
		$session         = JFactory::getSession();
		$activeworkspace = $session->get('activeworkspace');
		$query           = "SELECT level FROM #__mica_sld_legend where workspace_id = " . $activeworkspace . " group by level";
		$db->setQuery($query);
		$result          = $db->loadObjectList();

		$str   = "";
		$level = "0,1,2";

		foreach ($result as $eachlevel)
		{
			$level = str_replace($eachlevel->level . ", ", "", $level);
		}

		return $level;
	}

	/**
	 * Ajax request to export chat in PDF.
	 *
	 * @return  void
	 */
	public function amExport()
	{
		$imgtype = $this->input->get('imgtype', '', 'raw');
		$name    = $this->input->get('name', '', 'raw');

		if (!$imgtype)
		{
			// To be sure that the parameters are correctly forwarded
			echo "no image type";
			exit;
		}

		// Set image quality (from 0 to 100, not applicable to gif)
		$imgquality = 100;

		// Get data from $_POST or $_GET ?
		$data       = &$_POST;

		// Get image dimensions
		$width  = (int) $data['width'];
		$height = (int) $data['height'];

		// Create image object
		$img    = imagecreatetruecolor($width, $height);

		// Populate image with pixels
		for ($y = 0; $y < $height; $y++)
		{
			// Innitialize
			$x   = 0;

			// Get row data
			$row = explode(',', $data['r' . $y]);

			// Place row pixels
			$cnt = sizeof($row);

			for ($r = 0; $r < $cnt; $r++)
			{
				// Get pixel(s) data
				$pixel    = explode(':', $row[$r]);

				// Get color
				$pixel[0] = str_pad($pixel[0], 6, '0', STR_PAD_LEFT);

				$cr_e     = '0x' . strtoupper(substr($pixel[0], 0, 2));
				$cg_e     = '0x' . strtoupper(substr($pixel[0], 2, 2));
				$cb_e     = '0x' . strtoupper(substr($pixel[0], 4, 2));

				// VALORI DECIMALI

				/*
					$cr = hexdec(substr($pixel[0], 0, 2));
					$cg = hexdec(substr($pixel[0], 2, 2));
					$cb = hexdec(substr($pixel[0], 4, 2));
				*/

				// Allocate color
				$color  = imagecolorallocatealpha($img, hexdec($cr_e), hexdec($cg_e), hexdec($cb_e), '0');

				// Place repeating pixels
				$repeat = isset($pixel[1]) ? (int) $pixel[1] : 1;

				for ($c = 0; $c < $repeat; $c++)
				{
					// Place pixel
					imagesetpixel($img, $x, $y, $color);

					// Iterate column
					$x++;
				}
			}
		}

		// Relative or absolute path to server; check permissions to write on that dir!
		$path = JPATH_BASE . "/userchart/001." . $imgtype;

		switch ($imgtype)
		{
			case 'png':
				imagepng($img, $path) or die("Unable to create the file");
				break;
			case 'jpeg':
				imagejpeg($img, $path) or die("Unable to create the file");
				break;
			case 'jpg':
				imagejpeg($img, $path) or die("Unable to create the file");
				break;
			case 'gif':
				imagegif($img, $path) or die("Unable to create the file");
				break;
		}

		$logo      = JPATH_BASE . "/templates/mica/images/logo_pdf_chart.png";
		$pdfhelper = new pdfHelper();
		$pdfhelper->newPage();
		$pdfhelper->addHeader("", $logo);
		$pdfhelper->addImage($path);
		$pdfhelper->setOutput($name . ".pdf");
		exit;

		/*	    // set proper content type
			    $ch=fopen($path);
			    $content=fread($ch,filesize($path));
			   $ch1=fopen('/var/www/micamap/'.$name.".pdf",'w');
			   //write image to file
			   fwrite($ch1,@readfile($path),filesize($path));
			   fclose($ch1);
			   imagedestroy($img);
			    header('Content-type: application/pdf');
			    header('Content-Disposition: attachment; filename="'.$name.'.pdf"');
			    header("Content-Transfer-Encoding: binary");
			    header('Content-Length: ' . filesize($path));
				@readfile('/var/www/micamap/'.$name.".pdf");


			exit;*/
	}

	/**
	 * A task for excel export.
	 *
	 * @return  void
	 */
	public function exportexcel()
	{
		ini_set('memory_limit','1024M');
		$session = JFactory::getSession();
		$model   = $this->getModel();
		$model->getCustomData(1, 1);

		// $model->getDisplayGroup();
		$groupwisedata = $model->getDisplayGroup();

		// $groupwisedata=$session->get('Groupwisedata');
		$Market  = array();
		$Market1 = array();

		foreach ($groupwisedata as $grp => $grpdatas)
		{
			foreach ($grpdatas as $grpname => $grpdata)
			{
				if ($grpname == "Mkt Potential Index")
				{
					$tmp[$grpname] = $grpdata;
				}
				elseif ($grpname == "Score" || $grpname == "Composite Score" )
				{
					$tmp1[$grpname] = $grpdata;
				}
				else
				{
					$rearrangeddata[$grpname] = $grpdata;
				}
			}

			$Market  = $tmp;
			$Market1 = $tmp1;

			if (!is_array($tmp1))
			{
				$groupwisedatas[$grp] = is_array($Market) ? array_merge($Market, $rearrangeddata) : $rearrangeddata;
			}
			else
			{
				$groupwisedatas[$grp] = array_merge($Market, $Market1, $rearrangeddata);
			}
		}

		// array_pop($groupwisedatas);

		$i = 0;
		$j = 0;
		$k = 0;
		$l = 0;
		$headerspan = 0;

		foreach ($groupwisedatas[1] as $mainname => $data)
		{
			$header[$i] = $mainname;

			foreach ($data as $grpname => $grpdata)
			{
				$grpheader[$i][$j] = JTEXT::_($grpname) . str_repeat("\t", ($model->getColspan($grpdata, 2)));
				$headerspan += $model->getColspan($grpdata, 2);

				foreach ($grpdata as $variablegrp => $variabledata)
				{
					$vargrpname[$i][$k] = $variablegrp . str_repeat("\t", ($model->getColspan($variabledata, 3)));

					foreach ($variabledata as $eachvariabledata)
					{
						foreach ($eachvariabledata as $key => $value)
						{
							$mygrpvariablename[$i][$k][$l] = JTEXT::_($key) . "\t";
							$mygrpdata[$i][$k][$l]         = $value . "\t";
						}

						$l++;
					}

					$k++;
				}

				$j++;
			}

			$i++;
		}

		// $op=$session->get('state');
		$op = "State";

		if ($session->get('villages_villages') != "")
		{
			$m_type = $session->get('villagem_type');

			if ($m_type == "Total")
			{
				$m_type = "All";
			}

			$op .= "->Villages";
		}
		elseif ($session->get('urban') != "")
		{
			$op .= "->Urban Agglomeration ";
		}
		elseif ($session->get('villagedistrict') != "")
		{
			$m_type = $session->get('villagem_type');

			if ($m_type == "Total")
			{
				$m_type = "All";
			}

			$op .= "-> District ";
		}

		$str = str_repeat("\t", ($model->getColspan($headerspan, 2) / 2)) . "Data of " . $op . str_repeat("\t", ($model->getColspan($headerspan, 2) / 2)) . "\n";
		$str .= "\t";

		foreach ($grpheader as $inserheader)
		{
			foreach ($inserheader as $inserheaderval)
			{
				$str .= $inserheaderval;
			}

			break;
		}

		$str .= "\n";
		$str .= "\t";

		foreach ($vargrpname as $eachgrpname)
		{
			foreach ($eachgrpname as $name)
			{
				$str .= $name;
			}

			break;
		}

		$str .= "\n";
		$str .= "\t";

		foreach ($mygrpvariablename as $variablenamegrp)
		{
			foreach ($variablenamegrp as $varname)
			{
				foreach ($varname as $each)
				{
					$str .= $each;
				}
			}

			break;
		}

		$str .= "\n";

		foreach ($mygrpdata as $key => $gdata)
		{
			$str .= $header[$key] . "\t";

			foreach ($gdata as $data)
			{
				foreach ($data as $d)
				{
					$str .= $d;
				}
			}

			$str .= "\n";
		}

		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=datasheet.xls');
		echo $str;
		exit;
	}

	/**
	 * [exportMap description]
	 *
	 * @return  [type]  [description]
	 */
	function exportMap()
	{
		$session         = JFactory::getSession();
		$db              = JFactory::getDBO();
		$activeworkspace = $session->get('activeworkspace');
		$query           = " SELECT * FROM " . $db->quoteName('#__mica_sld_legend') . "
			WHERE " . $db->quoteName('workspace_id') . " = " . $db->quote($activeworkspace) . "
			ORDER BY " . $db->quoteName('level') . "," . $db->quoteName('range_from') . " ASC";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$id     = array();
		$level  = array();
		$i      = 1;
		$j      = 1;
		$html   = "";

		foreach ($result as $range)
		{
			if ($range->level == "0")
			{
				$str = '<td bgcolor="' . $range->color . '" width="15" > </td>';
			}
			else
			{
				$pin      = str_replace("#", "", $range->color);
				$pinimage = JPATH_BASE . '/components/com_mica/maps/img/layer' . trim($range->level) . '/pin' . $pin . '.png';

				// $pinimage='../maps/img/layer'.trim($range->level).'/pin'.$pin.'.png';
				$str = '<td ><img src="' . $pinimage . '" alt="pin" border="1" width="20"/></td>';
			}

			$grouping[$range->custom_formula][] = '<tr><td  width="100" >' . $range->range_from . ' - ' . $range->range_to . "</td>" . $str . "</tr>";

			// $id[]=$range->custom_formula;
			// $grouping[$range->custom_formula]=$range->level;
			$level[$range->custom_formula][] = $range->level;
			$i++;
			$j++;
		}

		$i            = 0;
		$range        = array();
		$str          = array();
		$totalcount   = 0;
		$l            = 0;
		$tojavascript = array();

		foreach ($grouping as $key => $val)
		{
			$grname         = $this->getCustomAttrName($key, $activeworkspace);
			$grpname[]      = $grname;
			$str[]          = implode(" ", $val);
			$ranges[]       = count($val);
			$levelunique[]  = $level[$key][$l];
			$tojavascript[] = $key;
			$l++;
		}

		$tojavascript = implode(",", $tojavascript);

		// $str = "";
		$op = $session->get('villagestate');

		if ($session->get('villagedistrict') != "")
		{
			$m_type = $session->get('villagem_type');

			/*if(is_array($m_type))
			{
				$m_type = implode(",",$m_type);
			}*/

			if ($m_type == "Total")
			{
				$m_type = "All";
			}

			$op .= "-> District ";

			// $op .="-> District (".$m_type.")";
		}
		elseif ($session->get('urban') != "")
		{
			$op .= "->Urban Agglomeration ";
		}
		elseif ($session->get('villages_villages') != "")
		{
			$op .= "->Villages";
		}

		$html = '';

		if ($grpname != "")
		{
			for ($i = 0; $i < count($grpname); $i++)
			{
				$html .= '<td><table  cellspacing="5" cellpadding="0"   border="0"><tr><td class="range"  colspan="2"><b>' . $grpname[$i][0] . '</b></td></tr>' . $str[$i] . '</table></td>';
			}
		}
		else
		{
			// $html .='<tr><td >{mapimage}</td></tr>';
		}

		$mapparameter  = rawurlencode($this->input->get('mapparameter', '', 'raw'));
		$mapparameter1 = rawurlencode($this->input->get('baselayer', '', 'raw')) . "&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A4326&FORMAT=image%2Fpng&LAYERS=india%3Arail_state&TRANSPARENT=true&styles=dummy_state&WIDTH=925&HEIGHT=650";
		$makeurl       = "";
		$mapparameter  = str_replace("EQT", "=", $mapparameter);
		$mapparameter  = str_replace("AND", "&", $mapparameter);
		$mapparameter  = str_replace('\n', " ", $mapparameter);
		$mapparameter  = $this->tomcat_url . "?" . $mapparameter;

		$mapparameter1 = str_replace("EQT", "=", $mapparameter1);
		$mapparameter1 = str_replace("AND", "&", $mapparameter1);

		$mapparameter1 = str_replace('\n', " ", $mapparameter1);
		$mapparameter1 = $this->tomcat_url . "?" . $mapparameter1;

		// $image= getimagesize($mapparameter);
		$layer1    = JPATH_BASE . "/userchart/map.png";
		$baselayer = JPATH_BASE . "/userchart/base.png";
		$finalop   = JPATH_BASE . "/userchart/final.png";

		file_put_contents($layer1, file_get_contents($mapparameter));
		file_put_contents($baselayer, file_get_contents($mapparameter1));

		$dest = imagecreatefrompng($baselayer);
		$src  = imagecreatefrompng($layer1);
		imagecopy($dest, $src, 0, 0, 0, 0, imagesx($src), imagesy($src));
		imagepng($dest, $finalop);
		imagedestroy($dest);

		// file_put_contents($finalop, $final_img);
		require_once JPATH_BASE . '/components/com_mica/helpers/tcpdf/tcpdf.php';
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		ob_clean();

		// Set default header data
		$logo = "logo_pdf.png";
		$pdf->SetHeaderData($logo, '30', '');
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		// Set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

		// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

		// Set image scale factor
		$pdf->SetFont('times', '', 8);
		$y = $pdf->getY();
		$pdf->SetFillColor(255, 255, 0);

		// Set color for text
		$pdf->SetTextColor(0, 0, 0);

		$pdf->AddPage();
		$rowspan = (count($result) + count($grpname)) + 2;

		$mapimage = '<img src="' . JPATH_BASE . '/userchart/final.png" border="0"/>';
		$head = "";
$html = <<<EOD
<table><tr><td ><b>Data of $op</b></td></tr>
<tr><td >$mapimage</td></tr>
<tr><td >
<table width="500"><tr>$html</tr></table>
</td></tr>
</table>
EOD;

		// Place image relative to end of HTML
		$pdf->writeHTML($html, true, false, false, false, '');

		/*$pdf->writeHTMLCell(PDF_MARGIN_LEFT, $y, PDF_MARGIN_LEFT, $y, $tbl, '', 1, 0, true, 'R', true);
		$x =$pdf->GetX();
		$y =$pdf->GetY();
		$pdf->SetXY($x, $y);
		$pdf->Image(JUri ::base().'userchart/final.png');
		$pdf->writeHTML($html,true, true, true, true, '');*/
		$pdf->lastPage();
		$pdf->Output('map.pdf', 'D');
		exit;

		/* $pdfhelper = new pdfHelper();
		$pdfhelper->newPage();
		$pdfhelper->addHeader("MIMI",$logo);
		$pdfhelper->addImage($finalop,60,30,650,650,'PNG');
		$pdfhelper->addHtml(5,$html);

		$pdfhelper->setOutput("map.pdf");*/
	}

	/**
	 * [getCustomAttrName description]
	 *
	 * @param   [type]  $name             [description]
	 * @param   [type]  $activeworkspace  [description]
	 *
	 * @return  [type]                    [description]
	 */
	function getCustomAttrName($name,$activeworkspace)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT name, attribute
			FROM " . $db->quoteName('#__mica_user_custom_attribute') . "
			WHERE " . $db->quoteName('profile_id') . " = " . $db->quote($activeworkspace) . "
				AND " . $db->quoteName('attribute') . " LIKE '" . $name . "'";
		$db->setQuery($query);
		$result = $db->loadAssoc();

		if (count($result) == 0)
		{
			return array($name, $name);
		}
		else
		{
			return array($result['name'], $result['attribute']);
		}
	}

	/**
	 * A task page.
	 *
	 * @return  void
	 */
	public function downloadMatrix()
	{
		$session      = JFactory::getSession();
		$model        = $this->getModel();
		$MaxForMatrix = $model->getMaxForMatrix();
		$data         = $model->getCustomData(1, 1);
		$customdata   = $data;

		$keys         = array_keys($MaxForMatrix[0]);
		$vals         = array_values($MaxForMatrix[0]);

		$excludeVarArray = array(
		    'Agricultural_Commodities_First',
		    'Agricultural_Commodities_Second',
		    'Agricultural_Commodities_Third',
		    'Manufacturers_Commodities_First',
		    'Manufacturers_Commodities_Second',
		    'Manufacturers_Commodities_Third',
		    'Handicrafts_Commodities_First',
		    'Handicrafts_Commodities_Second',
		    'Handicrafts_Commodities_Third'
		);

		$statusVarArray = array(
			'Agricultural_Marketing_Society_Status_A1NA2',
			'MandisRegular_Market_Status_A1NA2',
			'Public_Distribution_System_PDS_Shop_Status_A1NA2',
			'Weekly_Haat_Status_A1NA2',
			'CinemaVideo_Hall_Status_A1NA2',
			'Community_Centre_withwithout_TV_Status_A1NA2',
			'Daily_Newspaper_Supply_Status_A1NA2',
			'Mobile_Phone_Coverage_Status_A1NA2',
			'Public_Library_Status_A1NA2',
			'Sports_ClubRecreation_Centre_Status_A1NA2',
			'Sports_Field_Status_A1NA2',
			'Tractors_Status_A1NA2',
			'Commercial_Bank_Status_A1NA2'
		);

		$powerSupplyVarArray = array(
		 	'Power_Supply_For_Agriculture_Use_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_Agriculture_Use_Winter_OctMarchperdayinhrs',
		    'Power_Supply_For_All_Users_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_All_Users_Winter_OctMarch_perdayinhrs',
		    'Power_Supply_For_Commercial_Use_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_Commercial_Use_Winter_OctMarch_perdayinhrs',
		    'Power_Supply_For_Domestic_Use_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_Domestic_Use_Winter_OctMarch_perdayinhrs'
	    );

		$keys = array_values(array_diff($keys, $excludeVarArray));
		// $width =1024/count($keys)+1;
		// $width =round($width);
		$width = 150;
		$table = '<table cellpadding="0" cellspacing="0" border="0">';
		$table .= '<tr bgcolor="#0154A1" ><th width="' . $width . '">Legends</th><th width="' . $width . '">Variable Name</th>';

		foreach ($keys as $eachkey)
		{
			$table .= '<th width="' . $width . '">' . JTEXT::_($eachkey) . '</th>';
		}

		$table .= '</tr>';

		$i = 0;

		// $classarray = array("#f39685","#b8413d","#a61b18","#7f0301","");
		// $classarray   = array("#FFD600","#4DAF4A","#377EB8","#D7191C");
		$classarray    = array("#FFD600","#4DAF4A","#377EB8","#D7191C","", "", "#4DAF5A","#377EB7","#D7192C","");

		// $classname  = array("#f39685"=>"Low","#b8413d"=>"Average","#a61b18"=>"Medium","#7f0301"=>"High",""=>"");
		//$classname    = array("#FFD600" => "Low", "#4DAF4A" => "Medium", "#377EB8" => "High", "#D7191C" => "Very High");
		$classname     = array(
			"#FFD600" => "Low",
			"#4DAF4A" => "Medium",
			"#377EB8" => "High",
			"#D7191C" => "Very High",
			""    => "",
			""    => "",
			"#4DAF5A" => "Blanks",
			"#377EB7" => "1",
			"#D7192C" => "2"
		);

		foreach ($customdata as $eachdata)
		{
			$table .= '<tr>';

			if (isset($classarray[$i]) && $classarray[$i] != "")
			{
				$table .= '
					<td width="' . $width . '">
						<table cellspacing="6" cellpadding="4">
							<tr>
								<td bgcolor="' . $classarray[$i] . '">' . $classname[$classarray[$i]] . '</td>
							</tr>
						</table>
					</td>';
			}
			else
			{
				$table .= '<td width="' . $width . '"></td>';
			}

			$i++;
			$table .= '<td width="' . $width . '" >' . JText::_($eachdata->name) . '</td>';

			foreach ($eachdata as $key => $val)
			{
				$q  = $MaxForMatrix[0][$key] / 4;
				$q1 = $q + $q;
				$q2 = $q + $q + $q;
				$q4 = $MaxForMatrix[0][$key];

				if (!in_array($key, $statusVarArray) && !in_array($key, $powerSupplyVarArray))
				{
					if (in_array($key, $keys) && $key != "name")
					{
						if ($val <= $q)
						{
							$class = "#FFD600";
						}
						elseif ($val >= $q && $val < $q1)
						{
							$class = "#4DAF4A";
						}
						elseif ($val >= $q1 && $val < $q2)
						{
							$class = "#377EB8";
						}
						elseif ($val >= $q2 && $val <= $q4)
						{
							$class = "#D7191C";
						}

						if ($val == 'N/a')
						{
							// $class="#000000";
							$table .= '<td bgcolor="' . $class . '" width="' . $width . '" style="border:1px solid #FFFFFF;">' . $val . '</td>';
						}
						else
						{
							$table .= '<td bgcolor="' . $class . '" width="' . $width . '" style="border:1px solid #FFFFFF;"></td>';
						}

						// $table .= '<td bgcolor="'.$class.'" width="'.$width.'" style="border:1px solid #FFFFFF;"></td>';//
					}
				}
				elseif (in_array($key, $statusVarArray))
				{
					if ($MaxForMatrix[0][$key] == '1')
					{
						$class = "#377EB7";
						$table .= '<td bgcolor="' . $class . '" width="' . $width . '" style="border:1px solid #FFFFFF;"></td>';
					}
					elseif ($MaxForMatrix[0][$key] == '2')
					{
						$class = "#D7192C";
						$table .= '<td bgcolor="' . $class . '" width="' . $width . '" style="border:1px solid #FFFFFF;"></td>';
					}
					else
					{
						$class = "#4DAF5A";
						$table .= '<td bgcolor="' . $class . '" width="' . $width . '" style="border:1px solid #FFFFFF;"></td>';
					}
				}
				elseif (in_array($key, $powerSupplyVarArray))
				{
					if ($MaxForMatrix[0][$key] > '24')
					{
						$class = "#FFD600";
						$table .= '<td bgcolor="' . $class . '" width="' . $width . '" style="border:1px solid #FFFFFF;"></td>';
					}
				}
			}

			$table .= "</tr>";
		}

		if (count($eachdata) <= 3 && $i <= 3)
		{
			for ($i = 3; $i < count($classarray); $i++)
			{
				$table .= '<tr>
					<td width="' . $width . '">
						<table>
							<tr>
								<td bgcolor="' . $classarray[$i] . '">' . $classname[$classarray[$i]] . '</td>
							</tr>
						</table>
					</td>';

				$td    = '<td width="' . $width . '"></td>';
				$table .= str_repeat($td, count($this->customdata[0]));
				$table .= '</tr>';
			}
		}

		$table .= '</table>';

		/*require_once(JPATH_BASE.'/components/com_mica/helpers/tcpdf/tcpdf.php');
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'C2', true, 'UTF-8', true);
		ob_clean();

		// set default header data
		$logo="logo_pdf.png";
		$pdf->SetHeaderData($logo, '30', '');
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

		// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->SetFont('times', '', 8);
		$pdf->AddPage();
		$pdf->SetFillColor(255, 255, 0);
		// set color for text
		$pdf->SetTextColor(0, 0, 0);
		$pdf->writeHTML($table, true, false, true, false, '');
		$pdf->lastPage();
		$pdf->Output('matrix.pdf', 'D');*/
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=datasheet.xls');
		echo $table;
		exit;
	}

	/**
	 * A task for page.
	 *
	 * @return  void
	 */
	public function setFieldVal()
	{
		$session     = JFactory::getSession();
		$activetable = $session->get("activetable");
		$id          = $this->input->get('id', '', 'raw');

		if ($activetable == "india_information")
		{
			$dist = $session->get("villagedistrict");
			$session->set('villagedistrict', str_replace($id, "", $dist));
			$dist = $session->get("villagedistrict");
		}

		if ($activetable == "rail_state")
		{
			$state = $session->get("villagestate");
			$session->set('villagestate', str_replace($id, "", $state));
			$dist  = $session->get("villagestate");
		}

		if ($activetable == "jos_mica_urban_agglomeration")
		{
			$urban = $session->get("urban");
			$session->set('urban', str_replace($id, "", $urban));
			$dist  = $session->get("urban");
		}

		if ($activetable == "villages")
		{
			$villages = $session->get("villages_villages");
			$session->set('villages_villages', str_replace($id, "", $villages));
			$dist = $session->get("villages_villages");
		}

		$app = JFactory::getApplication();
		$app->redirect("index.php?option=com_mica&view=villageshowresults&Itemid=188");
	}

	/**
	 * An ajax task.
	 *
	 * @param   [type]  $innercall  [description]
	 *
	 * @return  vaoid
	 */
	public function getMaxForMatrix($innercall = null)
	{
		$session         = JFactory::getSession();
		$db              = JFactory::getDBO();
		$table           = $session->get('activetable');
		$variable        = $session->get('themeticattribute');
		$variable        = explode(",", $variable);
		$maxValsSelect   = "";
		$excludeVarArray = array(
		    'Agricultural_Commodities_First',
		    'Agricultural_Commodities_Second',
		    'Agricultural_Commodities_Third',
		    'Manufacturers_Commodities_First',
		    'Manufacturers_Commodities_Second',
		    'Manufacturers_Commodities_Third',
		    'Handicrafts_Commodities_First',
		    'Handicrafts_Commodities_Second',
		    'Handicrafts_Commodities_Third'
		);

		$statusVarArray = array(
			'Agricultural_Marketing_Society_Status_A1NA2',
			'MandisRegular_Market_Status_A1NA2',
			'Public_Distribution_System_PDS_Shop_Status_A1NA2',
			'Weekly_Haat_Status_A1NA2',
			'CinemaVideo_Hall_Status_A1NA2',
			'Community_Centre_withwithout_TV_Status_A1NA2',
			'Daily_Newspaper_Supply_Status_A1NA2',
			'Mobile_Phone_Coverage_Status_A1NA2',
			'Public_Library_Status_A1NA2',
			'Sports_ClubRecreation_Centre_Status_A1NA2',
			'Sports_Field_Status_A1NA2',
			'Tractors_Status_A1NA2',
			'Commercial_Bank_Status_A1NA2'
		);

		$powerSupplyVarArray = array(
		 	'Power_Supply_For_Agriculture_Use_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_Agriculture_Use_Winter_OctMarchperdayinhrs',
		    'Power_Supply_For_All_Users_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_All_Users_Winter_OctMarch_perdayinhrs',
		    'Power_Supply_For_Commercial_Use_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_Commercial_Use_Winter_OctMarch_perdayinhrs',
		    'Power_Supply_For_Domestic_Use_Summer_AprilSept_perdayinhrs',
		    'Power_Supply_For_Domestic_Use_Winter_OctMarch_perdayinhrs'
	    );

		$variable = array_values(array_diff($variable, $excludeVarArray));

		foreach ($variable as $eachvariable)
		{
			$maxValsSelect[] = "MAX(CAST(" . $eachvariable . " as UNSIGNED)) AS '" . $eachvariable . "'";
		}

		$maxValsSelect = implode(" , ", $maxValsSelect);

		// Complete the query
		$maxValsQuery = "SELECT $maxValsSelect FROM $table LIMIT 0, 1";
		$db->setQuery($maxValsQuery);

		if ($innercall == 1)
		{
			return $db->loadAssocList();
		}

		$MaxForMatrix = $db->loadAssocList();
		$model        = $this->getModel();
		$data         = $model->getCustomData(1, 1);
		$customdata   = $data;

		echo '<table cellpadding="0" cellspacing="0" >';
		$keys  = array_keys($MaxForMatrix[0]);
		$vals  = array_values($MaxForMatrix[0]);
		$width = 1024 / count($keys) + 1;

		// Kamlesh check here
		echo "<tr id='matrixhead'><td width='" . $width . "px'>Legends</td><td width='" . $width . "px'>Variable Name</td>";

		foreach ($keys as $eachkey)
		{
			echo "<td width='" . $width . "px'><a href='javascript:void();' title='" . JTEXT::_($eachkey) . "' style='color:white !important;'>" . substr(JTEXT::_($eachkey), 0, 10) . "</td>";
		}

		echo "</tr>";
		$i = 0;

		// $classarray = array("#f39685","#b8413d","#a61b18","#7f0301","");
		$classarray    = array("#FFD600","#4DAF4A","#377EB8","#D7191C","#4DAF5A","#377EB7","#D7192C","");

		// $classname  = array("#f39685"=>"Low","#b8413d"=>"Average","#a61b18"=>"Medium","#7f0301"=>"High",""=>"");
		$classname     = array("#FFD600" => "Low","#4DAF4A" => "Medium","#377EB8" => "High","#D7191C" => "Very High","#4DAF5A" => "Blanks","#377EB7" => "1","#D7192C" => "2","" => "");

		// $classarray = array("#ECE0BA","#CC9966","#89570F","#5C3B0F","");
		// $classname  = array("#ECE0BA"=>"Low","#CC9966"=>"Average","#89570F"=>"Medium","#5C3B0F"=>"High",""=>"");

		foreach ($customdata as $eachdata)
		{
			echo "<tr>";

			if (isset($classarray[$i]) && $classarray[$i] != "")
			{
				echo "<td width='" . $width . "px' >" . $classname[$classarray[$i]] . "<div  style='float:right;width:10px;height:10px;margin-right: 2px;background-color:" . $classarray[$i] . ";'></td>";
			}
			else
			{
				if ($i == 8)
				{
					echo "<td width='" . $width . "px' class='frontbutton'></td>";
				}
				elseif ($i == 7)
				{
					echo "<td width='" . $width . "px' class=''><table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton ' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input style='margin-right: 3px;' type='button' class='frontbutton fullscreenicon' name='fullscreen' value='Full Screen'/></td></tr></table></td>";
				}
				else
				{
					echo "<td width='" . $width . "px'></td>";
				}
			}

			$i++;
			echo "<td width='" . $width . "px' >" . JText::_($eachdata->name) . "</td>";

			foreach ($eachdata as $key => $val)
			{
				if (!in_array($key, $statusVarArray) && !in_array($key, $powerSupplyVarArray))
				{
					$q  = $MaxForMatrix[0][$key] / 4;
					$q1 = $q + $q;
					$q2 = $q + $q + $q;
					$q4 = $MaxForMatrix[0][$key];

					if (in_array($key, $keys) && $key != "name")
					{
						if ($val <= $q)
						{
							$class = "#FFD600";
						}
						elseif ($val >= $q && $val < $q1)
						{
							$class = "#4DAF4A";
						}
						elseif ($val >= $q1 && $val < $q2)
						{
							$class = "#377EB8";
						}
						elseif ($val >= $q2 && $val <= $q4)
						{
							$class = "#D7191C";
						}

						if ($val == 'N/a')
						{
							// $class="#000000";
							echo '<td  width="' . $width . 'px" style="background-color:' . $class . ';border:1px solid #FFFFFF;text-align:center;" >' . $val . '</td>';
						}
						else
						{
							echo '<td  width="' . $width . 'px" style="background-color:' . $class . ';border:1px solid #FFFFFF;" ></td>';
						}
					}
				}
				elseif (in_array($key, $statusVarArray))
				{
					if ($MaxForMatrix[0][$key] == '1')
					{
						$class = "#377EB7";
						echo '<td  width="' . $width . 'px" style="background-color:' . $class . ';border:1px solid #FFFFFF;" ></td>';
					}
					elseif ($MaxForMatrix[0][$key] == '2')
					{
						$class = "#D7192C";
						echo '<td  width="' . $width . 'px" style="background-color:' . $class . ';border:1px solid #FFFFFF;" ></td>';
					}
					else
					{
						$class = "#4DAF5A";
						echo '<td  width="' . $width . 'px" style="background-color:' . $class . ';border:1px solid #FFFFFF;" ></td>';
					}
				}
				elseif (in_array($key, $powerSupplyVarArray))
				{
					if ($MaxForMatrix[0][$key] > '24')
					{
						$class = "#FFD600";
						echo '<td  width="' . $width . 'px" style="background-color:' . $class . ';border:1px solid #FFFFFF;" ></td>';
					}
				}
			}

			echo "</tr>";
		}

		if (count($eachdata) <= 3 && $i <= 3)
		{
			for ($i = count($eachdata); $i <= count($classarray); $i++)
			{
				echo "<tr>";

				if ($i == 5)
				{
					echo "<td width='" . $width . "px' class='frontbutton'></td>";
				}
				elseif ($i == 4)
				{
					echo "<td width='" . $width . "px' class=''><table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton ' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input style='margin-right: 3px;' type='button' class='frontbutton fullscreenicon' name='fullscreen' value='Full Screen'/></td></tr></table></td>";
				}
				else
				{
					echo "<td width='" . $width . "px' >" . $classname[$classarray[$i]] . "<div class='' style='float:right;width:10px;height:10px;margin-right: 2px;background-color:" . $classarray[$i] . ";'></td>";
				}

				$td = "<td width='" . $width . "px'></td>";
				echo str_repeat($td, count($customdata[0]));
				echo "</tr>";
			}
		}

		echo '</td></tr></table>';
		exit;
	}

	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function speedometer()
	{
		$model = $this->getModel();
		$data  = $model->getCustomData(1, 1);
		$str   = "";

		foreach ($data as $eachdata)
		{
			$str .= '<option value=' . str_replace(" ", "%20", $eachdata->name) . '>' . $eachdata->name . '</option>';
		}

		echo $str;
		exit;
	}

	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function getGraph()
	{
		$db       = JFactory::getDBO();
		$villages = $this->input->getString('villages_villages');
		$attr     = $this->input->getString('attr');

		$query    = " SELECT " . $attr . "  FROM " . $db->quoteName('villages') . " WHERE " . $db->quoteName('OGR_FID') . " IN (" . $villages . ")";
		$db->setQuery($query);

		try
		{
			$attr = $db->loadObjectList();
		}
		catch (Exception $e)
		{
			$attr = null;
		}

		foreach ($attr as $eachattr)
		{
			foreach ($eachattr as $key => $val)
			{
				$attrs[JTEXT::_($key)][] = $val;
			}
		}

		// $attr=array_merge($attr,$eachcustomattr);
		$arraytoreturn = "";
		$lastkey       = count($attr);
		$z             = 1;

		foreach ($attrs as  $key => $val)
		{
			if ($z !== $lastkey)
			{
				$arraytoreturn .= JTEXT::_($key) . ";" . implode(";", $val) . "sln";
			}
			else
			{
				$arraytoreturn .= JTEXT::_($key) . ";" . implode(";", $val);
			}

			$z++;
		}

		$i     = 0;
		$query = "SELECT concat(place_name,'-',district_name) as name, OGR_FID FROM " . $db->quoteName('villages') . " WHERE " . $db->quoteName('OGR_FID') . " IN (" . $villages . ")";
		$db->setQuery($query);
		$dist = $db->loadObjectList();

		foreach ($dist as $eachdata)
		{
			$data_settings .= "<graph gid='" . $i . "'>";
			$data_settings .= "<title><![CDATA[" . JTEXT::_($eachdata->name) . "]]></title>";
			$data_settings .= "</graph>";
			$i++;

			if ($i == 15)
			{
				break;
			}
		}

		echo $arraytoreturn . "<->" . $data_settings;
		exit;
	}

	/**
	 * A task to provide district datas as options for select list.( AJAX call)
	 *
	 * @return  void
	 */
	public function getDistrictlist()
	{
		$dist         = $this->input->get('dist', '', 'raw');
		$dist_explode = explode(",", $dist);
		$list         = array();

		if (count($dist_explode) > 0  && $dist_explode[0] > 0)
		{
			$db    = JFactory::getDBO();
			$query = "SELECT CONCAT(distshp,'-',state) as name , OGR_FID
				FROM " . $db->quoteName('india_information') . "
				WHERE " . $db->quoteName('OGR_FID') . " IN (" . $dist . ")
				GROUP BY " . $db->quoteName('distshp') . "
				ORDER BY " . $db->quoteName('distshp') . " ASC";
			$db->setQuery($query);
			$list = $db->loadObjectList();
		}

		$options = "";

		foreach ($list as $eachlist)
		{
			$options .= "<li width='100px;float:left;'> <input type='checkbox' class='districtchecked' value='" . $eachlist->OGR_FID . "' />" . $eachlist->name . "</li>";
		}

		echo $options;
		exit;
	}

	/**
	 * A task to provide district datas as options for select list.( AJAX call)
	 *
	 * @return  void
	 */
	public function getVillagelist()
	{
		$villages = $this->input->get('villages_villages', '', 'raw');
		$villages_explode = explode(",", $villages);
		$list         = array();

		if (count($villages_explode) > 0  && $villages_explode[0] > 0)
		{
			$db    = JFactory::getDBO();
			$query = "SELECT CONCAT(place_name,'-',district_name) as name , OGR_FID
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('OGR_FID') . " IN (" . $villages . ")
				GROUP BY " . $db->quoteName('place_name') . "
				ORDER BY " . $db->quoteName('place_name') . " ASC";
			$db->setQuery($query);
			$list = $db->loadObjectList();
		}

		$options = "";

		foreach ($list as $eachlist)
		{
			$options .= "<li width='100px;float:left;'> <input type='checkbox' class='districtchecked' value='" . $eachlist->OGR_FID . "' />" . $eachlist->name . "</li>";
		}

		echo $options;
		exit;
	}

	/**
	 * [forceFileDownload description]
	 *
	 * @return  [type]  [description]
	 */
	function forceFileDownload()
	{
		$user = JFactory::getUser();

		if ($user->id != 0)
		{
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			$path = JURI::root() . 'components/com_mica/images/MIMI Urban Agglomeration.xls';
			header("Content-disposition: attachment; filename=\"MIMI Urban Agglomeration.xls\"");
			header("Location: $path");
		}
	}

	/**
	 * Let you download spreadsheet of Villages data file.
	 *
	 * @return  void
	 */
	public function TownforceFileDownload()
	{
		$user = JFactory::getUser();

		if ($user->id != 0)
		{
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			$path = JURI::root() . 'components/com_mica/images/MIMI Towns.xlsx';
			header("Content-disposition: attachment; filename=\"MIMI Towns.xlsx\"");
			header("Location: $path");
		}
	}

	/**
	 * Let you download spreadsheet of Villages Definition data file.
	 *
	 * @return  void
	 */
	public function TownsDefinitionforceFileDownload()
	{
		$user = JFactory::getUser();

		if ($user->id != 0)
		{
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			$path = JURI::root() . 'components/com_mica/images/MiMi-TownsDefinition.xlsx';
			header("Content-disposition: attachment; filename=\"MiMi-TownsDefinition.xlsx\"");
			header("Location: $path");
		}
	}
}
