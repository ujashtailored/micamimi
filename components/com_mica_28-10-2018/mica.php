<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$user = JFactory::getUser();
if($user->get('id') <= 0 || $user->get('id') == ""){
	JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_users&view=login', false));
	return false;
}


JLoader::register('MicaHelperRoute', JPATH_COMPONENT . '/helpers/route.php');

$controller = JControllerLegacy::getInstance('Mica');
$controller->execute(JFactory::getApplication()->input->get('task', 'display'));
$controller->redirect();
