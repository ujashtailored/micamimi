<?php defined('_JEXEC') or die('Restricted access');?>

<?php 
if(isset($this->message)){
	//JError
}
$user=JFactory::getUser();
?>
<script type="text/javascript">
	function validate_form(form){
		var ps 	= form.password;
		var vps = form.verifypassword;
		
		var ret = '';
		var all_empty = 0;
		for(var i=0;i<ps.length;i++){
			str		= ps[i].value;
			str 	= str.replace(/^\s+|\s+$/g,'');

			vstr	= vps[i].value; 
			vstr 	= vstr.replace(/^\s+|\s+$/g,'');
			
			if(	( str != '' || str != null) && (str.length > 0)){
				all_empty++;
				if(str != vstr){
					ps[i].style.border 	= "1px solid red";
					vps[i].style.border = "1px solid red";
					ret = 'error';
				}else{
					ps[i].style.border 	= "none";
					vps[i].style.border = "none";
				}	
			}else{
				ps[i].style.border 	= "none";
				vps[i].style.border = "none";
			}	
		}

		if(ret != 'error' && all_empty > 0){
			form.submit();
		}	
	}	
</script>
<form class="adminform" name="adminform" method="post" id="adminform" action="">
	<h1><?php echo JText::_('Edit Sub Users') ?></h1>
	<div class="childusers">
		<table cellpadding="3" cellspacing="0" border="0" width="100%" >
			<tr>
				<td>
					<div id="error_message"></div>
				</td>
			</tr>
		</table>	
		<table cellpadding="3" cellspacing="0" border="" width="100%" class="mimi_table">
			<tr align="left">
				<th>Name</th>
			    <th>Email</th>
			    <th>Password</th>
			    <th>Verify Password</th>
			</tr>
            <tr>
            	<td><strong><?php echo $user->name; ?></strong></td>
                <td><strong><?php echo $user->email; ?></strong></td>
                <td></td>
                <td></td>
            </tr>
			<?php for($i=0;$i<count($this->CreatedAccount);$i++) { ?>
			<tr>
				<td><?php echo $this->CreatedAccount[$i]->name; ?></td>
			    <td><?php echo $this->CreatedAccount[$i]->email; ?></td>
			    <td><input type="password" class="inputbox" name="password[<?php echo $this->CreatedAccount[$i]->id; ?>]" 			id="password" /></td>
			    <td><input type="password" class="inputbox" name="verifypassword[<?php echo $this->CreatedAccount[$i]->id; ?>]" 		id="verifypassword" /></td>
			</tr>
			<?php } ?>
			
			<tr align="right">
				<td colspan="4" align="right"><div style="width:100%; text-align:right;"><input type="button" id="button" value="Save" name="button"	onclick="validate_form(this.form);" /></div></td>
			</tr>
		</table>
	</div>
	<input type="hidden" name="option"	value="com_adduserfrontend" />
	<input type="hidden" name="view"	value="childusers" 			/>
	<input type="hidden" name="task"	value="save_changes" 		/>
</form>