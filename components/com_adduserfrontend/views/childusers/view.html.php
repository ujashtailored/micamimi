<?php
/**
 * Joomla! 3.0 component Add user Frontend
 *
 * @version $Id: view.html.php 2014-08-24 23:00:13 svn $
 * @author Kim Pittoors
 * @package Joomla
 * @subpackage Add user Frontend
 * @license GNU/GPL
 *
 * Add users to Community builder on the frontend
 *
* @Copyright Copyright (C) 2009 - 2014 - Kim Pittoors - www.joomlacy.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');
/**
 * HTML View class for the Add user Frontend component
 */
class AdduserfrontendViewChildusers extends JViewLegacy {
	function display($tpl = null) {

        if (JRequest::getVar('task') == 'save_changes'){
			$not_matched = $this->get('SaveChanges');

			$message = JText::_('Saved successfully');
			if (count($not_matched) > 0){
				$not_matched_names = array();
				foreach ($not_matched as $key => $value) {
					$not_matched_names[] = JFactory::getUser($value)->get('name');
				}
				$message = JText::_('Passwords and Verify Passwords did not matched for these Users : ').implode(" , ", $not_matched_names);
			}
			JFactory::getApplication('site')->redirect('index.php?option=com_adduserfrontend&view=childusers',$message);
		}

		$this->assignRef('CreatedAccount', $this->get('CreatedAccount'));
		parent::display($tpl);
    }
}
?>
