<?php
#die('pankaj');
/**
 * @package    Joomla.Site
 *
 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
 */
/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
	if(!preg_match('/www/', $_SERVER['HTTP_HOST']))
	{
		header('Location: https://www.'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
	} else {
		header('Location: '.str_replace('www.www.', 'www.', 'https://www.'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']));
	}
}*/
/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
	
	if(preg_match('/demo/', $_SERVER['HTTP_HOST']))
	{
		header('Location: http://demo.mica-mimi.in'.$_SERVER['REQUEST_URI']);
	}
}*/

if(preg_match('/demo/', $_SERVER['HTTP_HOST']))
{
	header('Location: http://demo.mica-mimi.in'.$_SERVER['REQUEST_URI']);
}
else{

if(!preg_match('/www/', $_SERVER['HTTP_HOST']))
{
	header('Location: https://www.'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
} else {
	 /*$txt = "user id date";
	 $myfile = file_put_contents('logs_salim.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
	die("I am in");	*/

define('JOOMLA_MINIMUM_PHP', '5.3.10');

if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
{
	die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
}

// Saves the start time and memory usage.
$startTime = microtime(1);
$startMem  = memory_get_usage();

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php'))
{
	include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', __DIR__);
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';

// Set profiler start time and memory usage and mark afterLoad in the profiler.
JDEBUG ? JProfiler::getInstance('Application')->setStart($startTime, $startMem)->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');

// Execute the application.
$app->execute();
}
}

